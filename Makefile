SHELL := /bin/sh

.PHONY: dependencies
# installs go dependency binaries
dependencies:
	go install github.com/swaggo/swag/cmd/swag@latest
	go install github.com/sqlc-dev/sqlc/cmd/sqlc@latest
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
	go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

.PHONY: proto
# generate proto definitions
proto:
	@buf generate
	@buf generate --template api/grpc_runner/v1/grpc_runner.buf.gen.yaml --path api/grpc_runner/v1/grpc_runner.proto

.PHONY: sqlc
# generate queries based on the sqlc definitions
sqlc:
	@go run github.com/sqlc-dev/sqlc/cmd/sqlc generate ./...

.PHONY: test
# test with coverage and reports
test:
	@go test -parallel 25 -v -cover ./... | go run github.com/jstemmer/go-junit-report@latest > report.xml

.PHONY: test/report
# test with report
test/report:
	@go test -parallel 25 -v ./... 2>&1 | go run github.com/jstemmer/go-junit-report@latest > report.xml

.PHONY: build/web
# build web
build/web:
	@npm run build --prod --prefix web

.PHONY: binary/anime-pack
# builds just api binary which assumes the frontend is built
binary/anime-pack:
	@scripts/build anime-pack;

.PHONY: build/anime-pack
# builds the api which runs all the steps required
build/anime-pack:
	make build/web;
	make binary/anime-pack;

.PHONY: build/migrate
# build the migrations
build/migrate:
	@scripts/build migrate;

.PHONY: build/anime-pack-runner
# build the runner
build/anime-pack-runner :
	@scripts/build anime-pack-runner;

.PHONY: swagger
# generate swagger docs for the api
swagger:
	@go run github.com/swaggo/swag/cmd/swag init \
		-g 'register.go' \
		-d './internal/controllers/api' \
		--parseDependency \
		--parseInternal \
		--generatedTime \
		--propertyStrategy camelcase

.PHONY: help
# show help
help:
	@echo ''
	@echo 'Usage:'
	@echo ' make [target]'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\/_0-9]+:/ { \
	helpMessage = match(lastLine, /^# (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 2, RLENGTH); \
			printf "\033[36m%-32s\033[0m %s\n", helpCommand,helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
