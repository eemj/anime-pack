// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             (unknown)
// source: api/grpc_runner/v1/grpc_runner.proto

package grpc_runner_v1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	RunnerService_Auth_FullMethodName      = "/grpc_runner_v1.RunnerService/Auth"
	RunnerService_Stream_FullMethodName    = "/grpc_runner_v1.RunnerService/Stream"
	RunnerService_Operation_FullMethodName = "/grpc_runner_v1.RunnerService/Operation"
)

// RunnerServiceClient is the client API for RunnerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RunnerServiceClient interface {
	Auth(ctx context.Context, in *AuthRequest, opts ...grpc.CallOption) (*AuthResponse, error)
	Stream(ctx context.Context, in *StreamRequest, opts ...grpc.CallOption) (*StreamResponse, error)
	Operation(ctx context.Context, opts ...grpc.CallOption) (RunnerService_OperationClient, error)
}

type runnerServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRunnerServiceClient(cc grpc.ClientConnInterface) RunnerServiceClient {
	return &runnerServiceClient{cc}
}

func (c *runnerServiceClient) Auth(ctx context.Context, in *AuthRequest, opts ...grpc.CallOption) (*AuthResponse, error) {
	out := new(AuthResponse)
	err := c.cc.Invoke(ctx, RunnerService_Auth_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *runnerServiceClient) Stream(ctx context.Context, in *StreamRequest, opts ...grpc.CallOption) (*StreamResponse, error) {
	out := new(StreamResponse)
	err := c.cc.Invoke(ctx, RunnerService_Stream_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *runnerServiceClient) Operation(ctx context.Context, opts ...grpc.CallOption) (RunnerService_OperationClient, error) {
	stream, err := c.cc.NewStream(ctx, &RunnerService_ServiceDesc.Streams[0], RunnerService_Operation_FullMethodName, opts...)
	if err != nil {
		return nil, err
	}
	x := &runnerServiceOperationClient{stream}
	return x, nil
}

type RunnerService_OperationClient interface {
	Send(*OperationRequest) error
	Recv() (*OperationResponse, error)
	grpc.ClientStream
}

type runnerServiceOperationClient struct {
	grpc.ClientStream
}

func (x *runnerServiceOperationClient) Send(m *OperationRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *runnerServiceOperationClient) Recv() (*OperationResponse, error) {
	m := new(OperationResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// RunnerServiceServer is the server API for RunnerService service.
// All implementations must embed UnimplementedRunnerServiceServer
// for forward compatibility
type RunnerServiceServer interface {
	Auth(context.Context, *AuthRequest) (*AuthResponse, error)
	Stream(context.Context, *StreamRequest) (*StreamResponse, error)
	Operation(RunnerService_OperationServer) error
	mustEmbedUnimplementedRunnerServiceServer()
}

// UnimplementedRunnerServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRunnerServiceServer struct {
}

func (UnimplementedRunnerServiceServer) Auth(context.Context, *AuthRequest) (*AuthResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Auth not implemented")
}
func (UnimplementedRunnerServiceServer) Stream(context.Context, *StreamRequest) (*StreamResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Stream not implemented")
}
func (UnimplementedRunnerServiceServer) Operation(RunnerService_OperationServer) error {
	return status.Errorf(codes.Unimplemented, "method Operation not implemented")
}
func (UnimplementedRunnerServiceServer) mustEmbedUnimplementedRunnerServiceServer() {}

// UnsafeRunnerServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RunnerServiceServer will
// result in compilation errors.
type UnsafeRunnerServiceServer interface {
	mustEmbedUnimplementedRunnerServiceServer()
}

func RegisterRunnerServiceServer(s grpc.ServiceRegistrar, srv RunnerServiceServer) {
	s.RegisterService(&RunnerService_ServiceDesc, srv)
}

func _RunnerService_Auth_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AuthRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RunnerServiceServer).Auth(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RunnerService_Auth_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RunnerServiceServer).Auth(ctx, req.(*AuthRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RunnerService_Stream_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StreamRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RunnerServiceServer).Stream(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RunnerService_Stream_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RunnerServiceServer).Stream(ctx, req.(*StreamRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _RunnerService_Operation_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(RunnerServiceServer).Operation(&runnerServiceOperationServer{stream})
}

type RunnerService_OperationServer interface {
	Send(*OperationResponse) error
	Recv() (*OperationRequest, error)
	grpc.ServerStream
}

type runnerServiceOperationServer struct {
	grpc.ServerStream
}

func (x *runnerServiceOperationServer) Send(m *OperationResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *runnerServiceOperationServer) Recv() (*OperationRequest, error) {
	m := new(OperationRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// RunnerService_ServiceDesc is the grpc.ServiceDesc for RunnerService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RunnerService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "grpc_runner_v1.RunnerService",
	HandlerType: (*RunnerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Auth",
			Handler:    _RunnerService_Auth_Handler,
		},
		{
			MethodName: "Stream",
			Handler:    _RunnerService_Stream_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Operation",
			Handler:       _RunnerService_Operation_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "api/grpc_runner/v1/grpc_runner.proto",
}
