<template>
  <div class="anime-edit__wrapper">
    <div class="details__wrapper">
      <loader v-if="details.loading && !details.error" :centered="true" />
      <div
        class="details-container"
        v-if="!(details.loading && details.error) && details.data"
      >
        <banner
          :id="details.data.id"
          :banner="details.data.banner"
          :disabled="false"
          :view="true"
        />
        <div class="header">
          <div class="overlap-banner">
            <img
              class="poster"
              :src="details.data.poster.name"
              :alt="`${details.data.title} Poster`"
            />
          </div>
          <div class="summary">
            <h1 class="title">{{ details.data.title }}</h1>
            <h2 class="subtitle native" v-if="details.data.title_native">
              {{ details.data.title_native }}
            </h2>
            <p class="description" v-html="details.data.description" />
          </div>
          <preferences
            v-if="
              !(preferences.details.loading && preferences.details.error) &&
                preferences.details.data
            "
            :details="preferences.details.data"
            @change-automatic-downloads="handleAutomaticDownloadChange"
            @change-perform-checksum="handlePerformChecksumChange"
          />
        </div>
        <div class="episodes__wrapper">
          <loader v-if="episodes.loading && !episodes.error" />
          <div
            class="actions flex flex-justify-content-start flex-row"
            v-if="!(episodes.loading && episodes.error) && episodes.data"
          >
            <button class="btn" @click="selectPriorityBest">
              <span>Pick the Best</span>
            </button>
            <button
              class="btn download"
              @click="handleDownloadClick(payload)"
              v-if="websocket && payload && payload.length > 0"
            >
              <download-icon />
            </button>
            <button
              class="btn download"
              @click="handleCancelClick(payload)"
              v-if="websocket && hasProgress && payload && payload.length > 0"
            >
              <close-icon />
            </button>
            <button
              class="btn clear"
              @click="handleClearClick"
              v-if="payload && payload.length > 0"
            >
              <erase-icon />
            </button>
          </div>
          <div
            class="episodes-container"
            v-if="!(episodes.loading && episodes.error) && episodes.data"
          >
            <div
              class="item"
              v-for="(episode, index) of episodes.data"
              :key="episode.name"
            >
              <episode-item
                :episode="episode"
                :progress="episodeProgress[index]"
                :connected="websocket"
                v-model:releaseGroup="episodeForm[index].rg"
                v-model:quality="episodeForm[index].q"
                v-model:prioritize="episodeForm[index].p"
                @click-download="handleDownloadClick"
                @click-cancel="handleCancelClick"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import {
  computed, defineComponent, onMounted, ref, watch, Ref,
} from 'vue';
import { useStore } from 'vuex';
import { useRoute, useRouter } from 'vue-router';
import { pickBest } from '@/adapter/helpers';

import { RootState } from '@/shared/types/state';

import Loader from '@/components/Loader.vue';
import Banner from '@/components/Anime/Banner.vue';
import Preferences from '@/components/Anime/Preferences.vue';
import EpisodeItem from '@/components/Anime/EpisodeItem.vue';
import DownloadIcon from '@/components/Icons/DownloadIcon.vue';
import EraseIcon from '@/components/Icons/EraseIcon.vue';
import CloseIcon from '@/components/Icons/CloseIcon.vue';

import {
  Payload,
  Opcode,
  DataActivityDownload,
  DataActivityCancel,
} from '@/shared/types/protocol';
import { UpdatePreferencesPayload } from '@/store/modules/anime/preferences';

interface TitleAnimeEpisodeForm {
  rg: number;
  q: number;
  p?: boolean;
}

export default defineComponent({
  components: {
    Loader,
    Banner,
    Preferences,
    EpisodeItem,
    DownloadIcon,
    EraseIcon,
    CloseIcon,
  },

  setup() {
    const store = useStore<RootState>();
    const router = useRouter();
    const route = useRoute();

    const details = computed(() => store.state.anime.details);
    const episodes = computed(() => store.state.anime.episodes);
    const preferences = computed(() => store.state.anime.preferences);
    const websocket = computed(() => store.state.health.websocket);

    const downloadPriority = computed(() => store.state.settings.downloadPriority);

    const episodeProgress = computed(() => store.getters.episodeProgress);
    const hasProgress = computed(() => store.getters.hasProgress);

    const getDetails = async (id: number) => store.dispatch('getDetails', id);
    const getEpisodes = async (id: number) => store.dispatch('getEpisodes', id);
    const getPreferences = async (id: number) => store.dispatch('getPreferences', id);
    const updatePreferences = async (preference: UpdatePreferencesPayload) => store.dispatch('updatePreferences', preference);
    const sendSocket = async (payload: Payload) => store.dispatch('sendSocket', payload);

    let id = Number(route.params?.id);

    if (Number.isNaN(id)) {
      router.push('/404');
      return {};
    }

    watch(
      () => details.value,
      (state) => {
        if (state.error) {
          router.push('/404');
        } else if (state.data?.title) {
          document.title = `Anime Pack | Edit ${state.data.title}`;
        }
      },
      {
        deep: true,
        immediate: true,
      },
    );

    onMounted(async () => {
      if (details.value.data?.id !== id) {
        await Promise.all([
          getDetails(id),
          getEpisodes(id),
          getPreferences(id),
        ]);
      }
    });

    router.afterEach(async (to, from) => {
      if (to.params?.id !== from.params?.id) {
        id = Number(to.params?.id);
        if (!Number.isNaN(id)) {
          await Promise.all([
            getDetails(id),
            getEpisodes(id),
            getPreferences(id),
          ]);
        }
      }
    });

    const episodeForm: Ref<TitleAnimeEpisodeForm[]> = ref([]);

    watch(
      () => episodes.value.data,
      (data) => {
        if (data) {
          episodeForm.value = data.map(() => ({
            rg: -1,
            q: -1,
            p: undefined,
          }));
        }
      },
      {
        deep: true,
        immediate: true,
      },
    );

    const payload = computed(() => (episodes.value.data
      ? episodeForm.value
        .map((form, index) => {
          if (
            form.rg >= 0
                && form.q >= 0
                && episodes.value.data
                && index < episodes.value.data.length
          ) {
            const { releaseGroups } = episodes.value.data[index];
            if (form.rg <= releaseGroups.length - 1) {
              const { qualities } = releaseGroups[form.rg];
              if (form.q <= qualities.length) {
                return {
                  q: qualities[form.q].id,
                  rg: releaseGroups[form.rg].id,
                  d: releaseGroups[form.rg].episodeID,
                  p: !form.p ? undefined : form.p,
                };
              }
            }
          }

          return null;
        })
        .filter((form) => form !== null)
      : []));

    const selectPriorityBest = () => {
      if (episodes.value.data) {
        episodes.value.data.forEach((episode, index) => {
          const best = pickBest(episode, downloadPriority.value);
          episodeForm.value[index].rg = best.releaseGroupIndex;
          episodeForm.value[index].q = best.qualityIndex;
        });
      }
    };

    const handleDownloadClick = (items: DataActivityDownload) => sendSocket({
      op: Opcode.ActivityDownload,
      d: items,
    });

    const handleCancelClick = (items: DataActivityCancel) => sendSocket({
      op: Opcode.ActivityCancel,
      d: items,
    });

    const handleAutomaticDownloadChange = async (value: boolean) => {
      await updatePreferences({ automaticDownloads: value });
    };

    const handlePerformChecksumChange = async (value: boolean) => {
      await updatePreferences({ performChecksum: value });
    };

    const handleClearClick = () => {
      if (payload.value.length && episodes.value.data) {
        episodeForm.value = episodes.value.data.map(() => ({ rg: -1, q: -1 }));
      }
    };

    return {
      details,
      episodes,
      preferences,
      websocket,

      episodeProgress,
      hasProgress,

      payload,
      episodeForm,
      selectPriorityBest,

      handleClearClick,
      handleDownloadClick,
      handleCancelClick,
      handleAutomaticDownloadChange,
      handlePerformChecksumChange,
    };
  },
});
</script>

<style lang="scss" scoped>
@import '@/assets/scss/variables.scss';

div.anime-edit__wrapper {
  div.details-container div.header {
    display: grid;
    padding: 0 100px;
    grid-column-gap: 30px;
    grid-template-columns: 220px auto 220px;

    @media only screen and (max-width: 950px) {
      grid-template-columns: auto;
    }

    div.summary {
      h1.title {
        margin-bottom: 0;
      }

      h2.subtitle {
        margin: 10px 0 0 0;
        font-weight: 600;
        font-style: italic;
        font-size: 16px;
      }
    }

    div.overlap-banner {
      margin-top: -75px;
      text-align: center;
      z-index: 1;
      position: relative;

      img.poster {
        max-width: 220px;
        outline: 0;
        border: 0;
        cursor: pointer;
        border-radius: 2px;
      }
    }
  }

  div.episodes__wrapper {
    margin: 25px;

    div.actions {
      box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.25);
      background-color: var(--color-dark-3);
      height: 25px;
      margin: 25px 0;
      padding: 5px 0;

      button.btn {
        span {
          padding: 10px;
          font-family: 'Roboto Condensed', sans-serif;
          font-size: 14px;
        }
      }
    }

    div.episodes-container {
      display: grid;
      grid-gap: 25px;
      justify-content: center;
      width: 100%;
      grid-template-columns: repeat(auto-fill, 285px);
    }
  }
}
</style>
