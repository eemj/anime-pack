import { Nullable } from '@/shared/types/generic';
import { ObjectDirective, DirectiveBinding } from 'vue';

const CLASS_PLACEHOLDER = 'placeholder';
const TAG_NAME_DIV = 'DIV';
const TAG_NAME_IMG = 'IMG';

const lazy = (): ObjectDirective => {
  const observer = new IntersectionObserver(
    (entries) => {
      entries
        .filter((entry) => entry.isIntersecting)
        .forEach((entry) => {
          const value = (entry.target as HTMLElement).dataset.src;

          let image: Nullable<HTMLImageElement> = new Image();

          image.onerror = () => {
            if (image) {
              image.onerror = null;
            }

            entry.target.classList.remove(CLASS_PLACEHOLDER);
            observer.unobserve(entry.target);

            image = null;
          };

          image.onload = () => {
            if (image) {
              image.onload = null;
            }

            switch (entry.target.tagName) {
              case TAG_NAME_DIV:
                // eslint-disable-next-line no-param-reassign
                (entry.target as HTMLElement).style.backgroundImage = `url('${value}')`;
                break;
              case TAG_NAME_IMG:
                if (value) {
                  // eslint-disable-next-line no-param-reassign
                  (entry.target as HTMLImageElement).src = value;
                }
                break;
              default:
                break;
            }

            entry.target.classList.remove(CLASS_PLACEHOLDER);
            observer.unobserve(entry.target);

            image = null;
          };

          if (value && image.src !== value) {
            image.src = value;
          }
        });
    },
    { root: null, threshold: 0 },
  );

  return {
    mounted(el: HTMLElement) {
      if (el.dataset.src && el.dataset.src !== '') {
        observer.observe(el);
      }
    },

    updated(el: HTMLElement, { value }: DirectiveBinding<string>) {
      if (value && value !== '' && el.dataset.src !== value) {
        // eslint-disable-next-line no-param-reassign
        el.dataset.src = value;
        observer.observe(el);
      }
    },

    beforeMount(el: HTMLElement, { value }: DirectiveBinding<string>) {
      if (value && value !== '') {
        // eslint-disable-next-line no-param-reassign
        el.dataset.src = value;
      }
    },

    unmounted() {
      observer.disconnect();
    },
  };
};

export default lazy;
