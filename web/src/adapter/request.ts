import { ActionContext } from 'vuex';
import { APIResponse } from '../shared/types/api';

export enum HttpMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
  HEAD = 'HEAD'
}

const applicationJson = 'application/json';

const headers = {
  Accept: applicationJson,
  'Content-Type': applicationJson,
  'Accept-Encoding': 'gzip', // Persist the gzip encoding...
};

export async function request<T>(
  path: string,
  method: HttpMethod,
  body?: string,
): Promise<APIResponse<T>> {
  const response = await window.fetch(
    path.startsWith('/') ? path : `/${path}`,
    {
      headers,
      method,
      body,
    },
  );

  try {
    const apiResponse: APIResponse<T> = await response.json();
    return apiResponse;
  } catch {
    return { data: {} } as APIResponse<T>;
  }
}

export async function context<S, R, B>(
  ctx: ActionContext<S, R>,
  apiCall: Promise<APIResponse<B>>,
  pendingMutation: string,
  failureMutation: string,
  successMutation: string,
): Promise<void> {
  ctx.commit(pendingMutation);

  try {
    const response = await apiCall;

    if (response.error) {
      ctx.commit(failureMutation, response.error);
      return Promise.reject(response.error);
    }

    if (response.data) {
      if (response.page) {
        ctx.commit(successMutation, response);
      } else {
        ctx.commit(successMutation, response.data);
      }

      return Promise.resolve();
    }
  } catch (err) {
    const errMessage = (err instanceof Error ? (err as Error).message : err);
    ctx.commit(failureMutation, errMessage);
    return Promise.reject(errMessage);
  }

  return Promise.resolve();
}
