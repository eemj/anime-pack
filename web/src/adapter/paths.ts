import { stringify } from 'query-string';
import {
  AnimeFilterQuery,
  PageQuery,
  ThirdPartyQuery,
  TitleQuery,
  AnimeEpisodeLatestFilterQuery,
} from './query';

const apiVersion = 1;
const context = `/api/v${apiVersion}`;

const createQueryFrom = (obj?: object | string): string => {
  if (
    typeof obj !== 'string'
    && obj
    && Object.getOwnPropertyNames(obj).length > 0
  ) {
    return `?${stringify(obj as Record<string, unknown>, {
      skipEmptyString: true,
      skipNull: true,
    })}`;
  }
  return obj as string;
};

export default {
  // Anime
  Anime: (query: AnimeFilterQuery) => `${context}/anime${createQueryFrom(query)}`,
  AnimeId: (id: number) => `${context}/anime/${id}`,
  AnimeIdPreferences: (id: number) => `${context}/anime/${id}/preferences`,
  AnimeIdEpisode: (id: number) => `${context}/anime/${id}/episode`,
  AnimeIdEpisodeName: (id: number, episode: string) => `${context}/anime/${id}/episode/${episode}`,
  AnimeThirdParty: (query: ThirdPartyQuery) => `${context}/anime/third-party${createQueryFrom(query)}`,
  AnimeIndexes: () => `${context}/anime/index`,
  AnimeIndex: (index: string) => `${context}/anime/index/${index}`,
  AnimeLatest: (query: PageQuery) => `${context}/anime/latest${createQueryFrom(query)}`,
  AnimeEpisodeLatest: (query: AnimeEpisodeLatestFilterQuery) => `${context}/anime/episode/latest${createQueryFrom(query)}`,
  AnimeSimulcast: () => `${context}/anime/simulcast`,

  // Title
  Title: (query: TitleQuery) => `${context}/title${createQueryFrom(query)}`,
  TitleId: (id: number) => `${context}/title/${id}`,

  // Format
  Format: () => `${context}/format`,

  // Genre
  Genre: () => `${context}/genre`,
  GenreId: (id: number) => `${context}/genre/${id}`,

  // Season
  Season: () => `${context}/season`,
  SeasonYear: () => `${context}/season-year`,

  // Stauts
  Status: () => `${context}/status`,

  // Studio
  Studio: () => `${context}/studio`,
  StudioId: (id: number) => `${context}/studio/${id}`,

  // Jobs
  Jobs: () => `${context}/jobs`,
  JobsId: (id: string) => `${context}/jobs/${id}`,
  JobsStart: (id: string) => `${context}/jobs/${id}/start`,
  JobsStop: (id: string) => `${context}/jobs/${id}/stop`,
  JobsDisable: (id: string) => `${context}/jobs/${id}/disable`,

  // Runners
  Runners: () => `${context}/runner`,
  RunnerIdent: (ident: string) => `${context}/runner/${ident}`,

  // WebSocket
  Socket: () => '/ws',
};
