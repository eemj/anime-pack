import { AnimeEpisode } from '@/shared/types/episode';

export const hasVideo = (episode: AnimeEpisode) => !!episode.releaseGroups.find(
  (group) => !!group.qualities.find(
    (quality) => quality.video !== undefined && quality.thumbnail !== undefined,
  ),
);

export const pickBest = (episode: AnimeEpisode, downloadPriority: Record<string, number>) => {
  const grouped = episode.releaseGroups.map((group, index) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const quality = group.qualities
      .map(({ height }, qindex) => ({ height, index: qindex }))
      .sort((a, b) => b.height - a.height)
      .shift()!;

    return {
      quality,
      name: group.name,
      index,
    };
  });

  const bestQuality = grouped
    .flatMap((group) => group.quality.height)
    .sort((a, b) => b - a)
    .shift();

  const sortedGroup = grouped
    .filter((group) => group.quality?.height === bestQuality)
    .sort(
      (a, b) => (downloadPriority[a.name] ?? Number.MAX_VALUE)
          - downloadPriority[b.name] ?? Number.MAX_VALUE,
    )
    .shift();

  return {
    releaseGroupIndex: sortedGroup?.index ?? 0,
    qualityIndex: sortedGroup?.quality?.index ?? 0,
  };
};

export default {};
