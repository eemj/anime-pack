import type {
  Anime,
  AnimeFormat,
  AnimeLatest,
  AnimeSeason,
  AnimeSimulcast,
  AnimeStatus,
  Genre,
  Preferences,
  Studio,
} from '@/shared/types/anime';
import type { Job } from '@/shared/types/jobs';
import type { APIResponse } from '@/shared/types/api';
import type { AnimeEpisode, AnimeEpisodeLatest } from '@/shared/types/episode';
import type { Title } from '@/shared/types/title';
import { Runner } from '@/shared/types/runner';
import type {
  AnimeFilterQuery,
  PageQuery,
  PreferenceUpdateBody,
  ThirdPartyQuery,
  TitleQuery,
  TitleReviewBody,
  AnimeEpisodeLatestFilterQuery,
} from './query';
import paths from './paths';
import { HttpMethod, request } from './request';

export const DEFAULT_PAGE = 1;
export const DEFAULT_ELEMENTS = 20;
export const MAX_ELEMENTS = 100;
export const DEFAULT_PAGE_QUERY: PageQuery = {
  page: DEFAULT_PAGE,
  elements: DEFAULT_ELEMENTS,
};

export class Adapter {
  static getTitle(query: TitleQuery): Promise<APIResponse<Title[]>> {
    return request<Title[]>(paths.Title(query), HttpMethod.GET);
  }

  static getTitleId(id: number): Promise<APIResponse<Title>> {
    return request<Title>(paths.TitleId(id), HttpMethod.GET);
  }

  static deleteTitleId(id: number): Promise<APIResponse<void>> {
    return request<void>(paths.TitleId(id), HttpMethod.DELETE);
  }

  static patchTitleId(
    id: number,
    body: TitleReviewBody,
  ): Promise<APIResponse<Title>> {
    return request<Title>(
      paths.TitleId(id),
      HttpMethod.PATCH,
      JSON.stringify(body),
    );
  }

  static getAnime(query: AnimeFilterQuery): Promise<APIResponse<Anime[]>> {
    return request<Anime[]>(paths.Anime(query), HttpMethod.GET);
  }

  static getAnimeId(id: number): Promise<APIResponse<Anime>> {
    return request<Anime>(paths.AnimeId(id), HttpMethod.GET);
  }

  static getAnimeLatest(query: PageQuery): Promise<APIResponse<AnimeLatest[]>> {
    return request<AnimeLatest[]>(paths.AnimeLatest(query), HttpMethod.GET);
  }

  static getAnimeIdPreferences(id: number): Promise<APIResponse<Preferences>> {
    return request<Preferences>(paths.AnimeIdPreferences(id), HttpMethod.GET);
  }

  static getAnimeEpisodeLatest(
    query: AnimeEpisodeLatestFilterQuery,
  ): Promise<APIResponse<AnimeEpisodeLatest[]>> {
    return request<AnimeEpisodeLatest[]>(
      paths.AnimeEpisodeLatest(query),
      HttpMethod.GET,
    );
  }

  static patchAnimeIdPreferences(
    id: number,
    body: PreferenceUpdateBody,
  ): Promise<APIResponse<Preferences>> {
    return request<Preferences>(
      paths.AnimeIdPreferences(id),
      HttpMethod.PATCH,
      JSON.stringify(body),
    );
  }

  static getAnimeIndexes(): Promise<APIResponse<string[]>> {
    return request<string[]>(paths.AnimeIndexes(), HttpMethod.GET);
  }

  static getAnimeIndex(index: string): Promise<APIResponse<Anime[]>> {
    return request<Anime[]>(
      paths.AnimeIndex(window.encodeURIComponent(index)),
      HttpMethod.GET,
    );
  }

  static getAnimeIdEpisode(id: number): Promise<APIResponse<AnimeEpisode[]>> {
    return request<AnimeEpisode[]>(paths.AnimeIdEpisode(id), HttpMethod.GET);
  }

  static getAnimeIdEpisodeName(
    id: number,
    episode: string,
  ): Promise<APIResponse<AnimeEpisode>> {
    return request<AnimeEpisode>(
      paths.AnimeIdEpisodeName(id, episode),
      HttpMethod.GET,
    );
  }

  static getAnimeThirdParty(
    query: ThirdPartyQuery,
  ): Promise<APIResponse<Anime>> {
    return request<Anime>(paths.AnimeThirdParty(query), HttpMethod.GET);
  }

  static getAnimeSimulcast(): Promise<APIResponse<AnimeSimulcast[]>> {
    return request<AnimeSimulcast[]>(paths.AnimeSimulcast(), HttpMethod.GET);
  }

  static getFormat(): Promise<APIResponse<AnimeFormat[]>> {
    return request<AnimeFormat[]>(paths.Format(), HttpMethod.GET);
  }

  static getSeason(): Promise<APIResponse<AnimeSeason[]>> {
    return request<AnimeSeason[]>(paths.Season(), HttpMethod.GET);
  }

  static getSeasonYear(): Promise<APIResponse<number[]>> {
    return request<number[]>(paths.SeasonYear(), HttpMethod.GET);
  }

  static getStatus(): Promise<APIResponse<AnimeStatus[]>> {
    return request<AnimeStatus[]>(paths.Status(), HttpMethod.GET);
  }

  static getGenre(): Promise<APIResponse<Genre[]>> {
    return request<Genre[]>(paths.Genre(), HttpMethod.GET);
  }

  static getStudio(): Promise<APIResponse<Studio[]>> {
    return request<Studio[]>(paths.Studio(), HttpMethod.GET);
  }

  static getJob(): Promise<APIResponse<Job[]>> {
    return request<Job[]>(paths.Jobs(), HttpMethod.GET);
  }

  static stopJob(id: string): Promise<APIResponse<void>> {
    return request<void>(paths.JobsStop(id), HttpMethod.DELETE);
  }

  static disableJob(id: string): Promise<APIResponse<void>> {
    return request<void>(paths.JobsDisable(id), HttpMethod.DELETE);
  }

  static startJob(id: string): Promise<APIResponse<void>> {
    return request<void>(paths.JobsStart(id), HttpMethod.PUT);
  }

  static getJobId(id: string): Promise<APIResponse<Job>> {
    return request<Job>(paths.JobsId(id), HttpMethod.GET);
  }

  static getRunners(): Promise<APIResponse<Runner[]>> {
    return request<Runner[]>(paths.Runners(), HttpMethod.GET);
  }

  static getRunner(ident: string): Promise<APIResponse<Runner[]>> {
    return request<Runner[]>(paths.RunnerIdent(ident), HttpMethod.GET);
  }

  static toggleRunnerState(ident: string): Promise<APIResponse<Runner[]>> {
    return request<Runner[]>(paths.RunnerIdent(ident), HttpMethod.PUT);
  }
}

export default Adapter;
