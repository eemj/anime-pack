import { AnimeFormat, AnimeSeason, AnimeStatus } from '@/shared/types/anime';

export enum OrderByDirection {
  Descending = 'desc',
  Ascending = 'asc',
}

export enum AnimeEpisodeLatestOrderByQuery {
  Relevance = '',
  AnimeScore = 'score',
  Size = 'size',
  VideoDuration = 'duration',
  VideoCreatedAt = 'createdAt',
}

export enum AnimeLatestOrderByQuery {
  Relevance = '',
  ID = 'id',
  Score = 'score',
  CreatedAt = 'createdAt',
}

export enum AnimeOrderByQuery {
  Relevance = '',
  Title = 'title',
  Score = 'score',
  Year = 'year',
  NextAiringDate = 'nextAiringDate',
  StartDate = 'startDate',
}

export enum TitleOrderByQuery {
  Relevance = '',
  ID = 'id',
  CreatedAt = 'createdAt'
}

export interface PageQuery {
  page: number;
  elements: number;
}

export interface AnimeLatestQuery extends PageQuery {
  statuses?: AnimeStatus[];
  formats?: AnimeFormat[];
  favourite?: boolean;
  orderBy?: AnimeLatestOrderByQuery;
  direction?: OrderByDirection;
}

export interface TitleQuery extends PageQuery {
  reviewed?: boolean;
  isDeleted?: boolean;
  orderBy?:TitleOrderByQuery;
  direction?: OrderByDirection;
}

export interface AnimeFilterQuery extends PageQuery {
  ids?: number[];
  title?: string;
  genreIDs?: number[];
  studioIDs?: number[];
  formats?: AnimeFormat[];
  seasons?: AnimeSeason[];
  statuses?: AnimeStatus[];
  seasonYear?: number;
  favourite?: boolean;
  orderBy?: AnimeOrderByQuery;
  direction?: OrderByDirection;
}

export interface AnimeEpisodeLatestFilterQuery extends PageQuery {
  format?: AnimeStatus;
  status?: AnimeFormat;
  favourite?: boolean;
  orderBy?: AnimeEpisodeLatestOrderByQuery;
  direction?: OrderByDirection;
}

export interface ThirdPartyQuery {
  idAniList?: number;
  idMAL?: number;
}

export interface TitleReviewBody {
  reviewed?: boolean;
  idAniList?: number;
  idMAL?: number;
}

export interface PreferenceUpdateBody {
  performChecksum?: boolean;
  automaticDownloads?: boolean;
  favourite?: boolean;
}
