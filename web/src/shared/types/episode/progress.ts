import { Nullable } from '@/shared/types/generic';
import { DataProgressItem } from '@/shared/types/protocol';

export interface AnimeEpisodeProgress {
  readonly rg: string;
  readonly h: number;
  readonly pg: Nullable<DataProgressItem>;
}
