import { Episode, Image, Video } from '@/shared/types/anime';

export interface AnimeEpisodeReleaseGroupQuality {
  id: number;
  height: number;
  video?: Video;
  thumbnail?: Image;
}

export interface AnimeEpisodeLatestReleaseGroupQuality {
  readonly id: number;
  readonly height: number;
  readonly video: Video;
  readonly thumbnail: Image;
}

export interface AnimeEpisodeLatestReleaseGroup {
  readonly episodeID: number;
  readonly id: number;
  readonly name: string;
  readonly qualities: AnimeEpisodeLatestReleaseGroupQuality[];
}

export interface AnimeEpisodeReleaseGroup {
  episodeID: number;
  id: number;
  name: string;
  qualities: AnimeEpisodeReleaseGroupQuality[];
}

export interface AnimeEpisode {
  name: string;
  releaseGroups: AnimeEpisodeReleaseGroup[];
  createdAt: Date;
}

export interface AnimeEpisodeLatest {
  readonly id: number;
  readonly title: string;
  readonly titleEnglish?: string;
  readonly titleNative?: string;
  readonly titleRomaji?: string;
  readonly score: number;
  readonly episode: Episode;
  readonly poster: Image;
  readonly banner: Image;
  readonly releaseGroups: AnimeEpisodeLatestReleaseGroup[];
}
