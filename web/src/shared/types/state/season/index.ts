import { FetchState, Nullable } from '@/shared/types/generic';
import { AnimeSeason } from '../../anime';

export type SeasonState = FetchState<Nullable<AnimeSeason[]>, string>;

export enum SeasonMutations {
  SEASON_SUCCESS = 'SEASON_SUCCESS',
  SEASON_PENDING = 'SEASON_PENDING',
  SEASON_FAILURE = 'SEASON_FAILURE',
}

export type SeasonYearState = FetchState<Nullable<number[]>, string>;

export enum SeasonYearMutations {
  SEASON_YEAR_SUCCESS = 'SEASON_YEAR_SUCCESS',
  SEASON_YEAR_PENDING = 'SEASON_YEAR_PENDING',
  SEASON_YEAR_FAILURE = 'SEASON_YEAR_FAILURE',
}
