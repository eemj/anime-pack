import { FetchState, Nullable, EmptyFetchState } from '@/shared/types/generic';
import { Page } from '@/shared/types/api';
import { Title } from '@/shared/types/title';
import { Anime } from '@/shared/types/anime';
import { TitleReviewBody } from '@/adapter/query';

export interface TitleUpdatePayload {
  id: number;
  body: TitleReviewBody;
}

export interface TitleAddAnimePayload {
  id: number;
  anime: Anime;
}

export interface TitleDetailsSubState extends FetchState<Nullable<Title[]>, string> {
  page: Nullable<Page>;
}

export interface TitleUpdateSubState extends EmptyFetchState<string> {
  updated: number;
}

export interface TitleState {
  details: TitleDetailsSubState;
  update: TitleUpdateSubState;
  delete: EmptyFetchState<string>;
}

export enum TitleMutations {
  TITLE_SUCCESS = 'TITLE_SUCCESS',
  TITLE_PENDING = 'TITLE_PENDING',
  TITLE_FAILURE = 'TITLE_FAILURE',

  UPDATE_TITLE_SUCCESS = 'UPDATE_TITLE_SUCCESS',
  UPDATE_TITLE_PENDING = 'UPDATE_TITLE_PENDING',
  UPDATE_TITLE_FAILURE = 'UPDATE_TITLE_FAILURE',

  DELETE_TITLE_SUCCESS = 'DELETE_TITLE_SUCCESS',
  DELETE_TITLE_PENDING = 'DELETE_TITLE_PENDING',
  DELETE_TITLE_FAILURE = 'DELETE_TITLE_FAILURE',

  ADD_ANIME_TITLE = 'ADD_ANIME_TITLE'
}
