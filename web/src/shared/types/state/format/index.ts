import { FetchState, Nullable } from '@/shared/types/generic';
import { AnimeFormat } from '../../anime';

export type FormatState = FetchState<Nullable<AnimeFormat[]>, string>;

export enum FormatMutations {
    FORMAT_SUCCESS = 'FORMAT_SUCCESS',
    FORMAT_PENDING = 'FORMAT_PENDING',
    FORMAT_FAILURE = 'FORMAT_FAILURE',
}
