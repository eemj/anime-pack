import { AnimeSimulcast } from '@/shared/types/anime';
import { FetchState, Nullable } from '@/shared/types/generic';

export enum AnimeSimulcastMutations {
  SIMULCAST_PENDING = 'SIMULCAST_PENDING',
  SIMULCAST_SUCCESS = 'SIMULCAST_SUCCESS',
  SIMULCAST_FAILURE = 'SIMULCAST_FAILURE',
}

export type AnimeSimulcastState = FetchState<Nullable<AnimeSimulcast[]>, string>;
