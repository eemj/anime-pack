import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';
import { Page } from '@/shared/types/api';

export enum AnimeFavouriteMutations {
  FAVOURITE_PENDING = 'FAVOURITE_PENDING',
  FAVOURITE_SUCCESS = 'FAVOURITE_SUCCESS',
  FAVOURITE_FAILURE = 'FAVOURITE_FAILURE'
}

export interface AnimeFavouriteState
  extends FetchState<Nullable<Anime[]>, string> {
  page: Nullable<Page>;
}
