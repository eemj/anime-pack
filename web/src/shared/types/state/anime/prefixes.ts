import { Nullable, FetchState, EmptyFetchState } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';

export interface AnimePrefixesPayload {
  prefix: string;
  anime: Anime[];
}

export type AnimePrefixOptionsSubState = EmptyFetchState<string>;

export interface AnimePrefixesTree<T> {
  [key: string]: Nullable<T>;
}

export type AnimePrefixAnimeSubState = FetchState<
  AnimePrefixesTree<Nullable<Anime[]>>,
  string
>;

export interface AnimePrefixesSubState {
  options: AnimePrefixOptionsSubState;
  anime: AnimePrefixAnimeSubState;
}
