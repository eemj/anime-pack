import { EmptyFetchState, FetchState, Nullable } from '@/shared/types/generic';
import { Preferences } from '@/shared/types/anime';

export enum AnimePreferencesMutations {
  PREFERENCE_PENDING = 'PREFERENCE_PENDING',
  PREFERENCE_SUCCESS = 'PREFERENCE_SUCCESS',
  PREFERENCE_FAILURE = 'PREFERENCE_FAILURE',
  UPDATE_PREFERENCE_PENDING = 'UPDATE_PREFERENCE_PENDING',
  UPDATE_PREFERENCE_SUCCESS = 'UPDATE_PREFERENCE_SUCCESS',
  UPDATE_PREFERENCE_FAILURE = 'UPDATE_PREFERENCE_FAILURE'
}

export interface AnimePreferencesState {
  details: FetchState<Nullable<Preferences>, string>;
  update: EmptyFetchState<string>;
}
