import {
  DynamicElements, FetchState, Nullable, Paginated,
} from '@/shared/types/generic';
import { AnimeLatest } from '@/shared/types/anime';
import { AnimeLatestQuery } from '@/adapter/query';

export enum AnimeLatestMutations {
  LATEST_PENDING = 'LATEST_PENDING',
  LATEST_SUCCESS = 'LATEST_SUCCESS',
  LATEST_FAILURE = 'LATEST_FAILURE',
}

export interface AnimeLatestState
  extends FetchState<Nullable<AnimeLatest[]>, string>,
    DynamicElements,
    Paginated {
  query: AnimeLatestQuery;
}
