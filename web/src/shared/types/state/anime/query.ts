import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';

export enum AnimeQueryMutations {
  QUERY_PENDING = 'QUERY_PENDING',
  QUERY_SUCCESS = 'QUERY_SUCCESS',
  QUERY_FAILURE = 'QUERY_FAILURE'
}

export type AnimeQueryState = FetchState<Nullable<Anime>, string>;
