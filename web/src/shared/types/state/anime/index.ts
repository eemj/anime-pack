import { AnimeActivityState } from '@/shared/types/state/anime/activity';
import { AnimeLatestState } from '@/shared/types/state/anime/latest';
import { AnimeFavouriteState } from '@/shared/types/state/anime/favourite';
import { AnimeQueryState } from '@/shared/types/state/anime/query';
import { AnimeDetailsState } from '@/shared/types/state/anime/details';
import { AnimeEpisodeState } from '@/shared/types/state/anime/episodes';
import { AnimePreferencesState } from '@/shared/types/state/anime/preferences';
import { AnimeIndexesState } from '@/shared/types/state/anime/indexes';
import { AnimeSearchState } from '@/shared/types/state/anime/search';
import { AnimeAiringState } from '@/shared/types/state/anime/airing';
import { AnimeEpisodeLatestState } from '@/shared/types/state/anime/episodelatest';
import { AnimeSimulcastState } from './simulcast';

export interface AnimeState {
  query: AnimeQueryState;
  latest: AnimeLatestState;
  activity: AnimeActivityState;
  search: AnimeSearchState;
  details: AnimeDetailsState;
  episodes: AnimeEpisodeState;
  episodeLatest: AnimeEpisodeLatestState;
  preferences: AnimePreferencesState;
  favourite: AnimeFavouriteState;
  indexes: AnimeIndexesState;
  airing: AnimeAiringState;
  simulcast: AnimeSimulcastState;
}
