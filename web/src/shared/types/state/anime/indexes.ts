import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';

export enum AnimeIndexesMutations {
  INDEXES_PENDING = 'INDEXES_PENDING',
  INDEXES_SUCCESS = 'INDEXES_SUCCESS',
  INDEXES_FAILURE = 'INDEXES_FAILURE',

  INDEX_PENDING = 'INDEX_PENDING',
  INDEX_SUCCESS = 'INDEX_SUCCESS',
  INDEX_FAILURE = 'INDEX_FAILURE'
}

export interface AnimeIndex {
  [key: string]: Nullable<Anime[]>;
}

export interface AnimeIndexesState {
  indexes: FetchState<Nullable<string[]>, string>;
  index: FetchState<AnimeIndex, string>;
}
