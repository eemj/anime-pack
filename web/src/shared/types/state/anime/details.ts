import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';

export enum AnimeDetailsMutations {
  DETAILS_PENDING = 'DETAILS_PENDING',
  DETAILS_SUCCESS = 'DETAILS_SUCCESS',
  DETAILS_FAILURE = 'DETAILS_FAILURE'
}

export type AnimeDetailsState = FetchState<Nullable<Anime>, string>;
