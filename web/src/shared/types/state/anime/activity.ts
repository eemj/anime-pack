import { Image } from '@/shared/types/anime';
import { Nullable, FetchState } from '@/shared/types/generic';
import { DataActivityDifferences, DataActivityProgress } from '@/shared/types/protocol';

export enum AnimeActivityMutations {
  ACTIVITY_DATA_UPDATE = 'ACTIVITY_DATA_UPDATE',
  ACTIVITY_DETAILS_PENDING = 'ACTIVITY_DETAILS_PENDING',
  ACTIVITY_DETAILS_FAILURE = 'ACTIVITY_DETAILS_FAILURE',
  ACTIVITY_DETAILS_SUCCESS = 'ACTIVITY_DETAILS_SUCCESS',
  ACTIVITY_DIFFERENCE_UPDATE = 'ACTIVITY_DIFFERENCE_UPDATE',
  ACTIVITY_FETCH_PROGRESS = 'ACTIVITY_FETCH_PROGRESS',
  ACTIVITY_FETCH_DETAILS = 'ACTIVITY_FETCH_DETAILS'
}

export type AnimeActivityDifferenceData = DataActivityDifferences;

export type AnimeActivityData = DataActivityProgress;

export interface AnimeActivityDetail {
  id: number;
  title: string;
  poster: Image;
}

export type AnimeActivityDetails = Record<number, Nullable<AnimeActivityDetail>>;

export type AnimeActivityState = {
  tree: AnimeActivityData;
  details: FetchState<AnimeActivityDetails, string>;
};
