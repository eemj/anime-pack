import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';
import { PaginatedData } from '@/shared/types/api';
import { AnimeFilterQuery } from '@/adapter/query';

export enum AnimeSearchMutations {
  SEARCH_PENDING = 'SEARCH_PENDING',
  SEARCH_SUCCESS = 'SEARCH_SUCCESS',
  SEARCH_FAILURE = 'SEARCH_FAILURE',
  SEARCH_CLEAR = 'SEARCH_CLEAR',

  SEARCH_TITLE_UPDATE = 'SEARCH_TITLE_UPDATE',
}

export interface AnimeSearchState
  extends FetchState<Nullable<PaginatedData<Anime[]>>, string> {
  query: AnimeFilterQuery;
}
