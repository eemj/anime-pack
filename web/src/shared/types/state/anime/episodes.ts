import { FetchState, Nullable } from '@/shared/types/generic';
import { AnimeEpisode } from '@/shared/types/episode';

export enum AnimeEpisodesMutations {
  EPISODES_PENDING = 'EPISODES_PENDING',
  EPISODES_SUCCESS = 'EPISODES_SUCCESS',
  EPISODES_FAILURE = 'EPISODES_FAILURE',

  EPISODE_UPDATE_METADATA = 'EPISODE_UPDATE_METADATA',
}

export interface GetEpisodesQuery {
  id: number;
}

export type AnimeEpisodeState = FetchState<Nullable<AnimeEpisode[]>, string>;
