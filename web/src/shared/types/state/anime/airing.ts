import { FetchState, Nullable } from '@/shared/types/generic';
import { Anime } from '@/shared/types/anime';
import { Page } from '@/shared/types/api';

export enum AnimeAiringMutations {
  AIRING_PENDING = 'AIRING_PENDING',
  AIRING_SUCCESS = 'AIRING_SUCCESS',
  AIRING_FAILURE = 'AIRING_FAILURE'
}

export interface AnimeAiringState
  extends FetchState<Nullable<Anime[]>, string> {
  page: Nullable<Page>;
}
