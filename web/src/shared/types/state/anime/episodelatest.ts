import { AnimeEpisodeLatest } from '../../episode';
import {
  FetchState, Nullable, Paginated,
} from '../../generic';

export enum AnimeEpisodeLatestMutations {
  ANIME_EPISODE_SUCCESS = 'ANIME_EPISODE_SUCCESS',
  ANIME_EPISODE_PENDING = 'ANIME_EPISODE_PENDING',
  ANIME_EPISODE_FAILURE = 'ANIME_EPISODE_FAILURE',
}

export type AnimeEpisodeLatestState =
  FetchState<Nullable<AnimeEpisodeLatest[]>, string>
  & Paginated;
