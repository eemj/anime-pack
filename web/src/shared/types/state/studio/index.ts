import { Studio } from '@/shared/types/anime';
import { FetchState, Nullable } from '@/shared/types/generic';

export type StudioState = FetchState<Nullable<Studio[]>, string>;

export enum StudioMutations {
    STUDIO_SUCCESS = 'STUDIO_SUCCESS',
    STUDIO_PENDING = 'STUDIO_PENDING',
    STUDIO_FAILURE = 'STUDIO_FAILURE',
}
