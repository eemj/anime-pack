import { Genre } from '@/shared/types/anime';
import { FetchState, Nullable } from '@/shared/types/generic';

export type GenreState = FetchState<Nullable<Genre[]>, string>;

export enum GenreMutations {
    GENRE_SUCCESS = 'GENRE_SUCCESS',
    GENRE_PENDING = 'GENRE_PENDING',
    GENRE_FAILURE = 'GENRE_FAILURE',
}
