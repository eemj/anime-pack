import { FetchState, Nullable } from '@/shared/types/generic';
import { AnimeStatus } from '../../anime';

export type StatusState = FetchState<Nullable<AnimeStatus[]>, string>;

export enum StatusMutations {
    STATUS_SUCCESS = 'STATUS_SUCCESS',
    STATUS_PENDING = 'STATUS_PENDING',
    STATUS_FAILURE = 'STATUS_FAILURE',
}
