export interface DimensionUpdate {
  width?: number;
  height?: number;
}

export interface Dimension {
  height: number;
  width: number;
}

export interface PaletteColor {
  name: string;
  hex: string;
}

export enum SettingMutations {
  SETTING_CREATE_THEME = 'SETTING_CREATE_THEME',
  SETTING_APPLY_THEME = 'SETTING_APPLY_THEME',
  SETTING_SAVE_THEME = 'SETTING_SAVE_THEME',

  SETTING_DIMENSION_UPDATE = 'SETTING_DIMENSION_UPDATE',

  SETTING_TOGGLE_DARK_MODE = 'SETTING_TOGGLE_DARK_MODE',
  SETTING_TOGGLE_QUICK_SEARCH = 'SETTING_TOGGLE_QUICK_SEARCH',

  SETTING_LOAD_PRIORITY = 'SETTING_LOAD_PRIORITY',
}

export interface SettingState {
  darkMode: boolean;
  palette: PaletteColor[];
  dimension: Dimension;
  quickSearch: boolean;
  downloadPriority: Record<string, number>;
  hex: string;
}
