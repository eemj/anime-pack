import { AnimeState } from './anime';
import { RouteState } from './route';
import { HealthState } from './health';
import { TitleState } from './title';
import { StatusState } from './status';
import { SeasonState, SeasonYearState } from './season';
import { FormatState } from './format';
import { StudioState } from './studio';
import { GenreState } from './genre';
import { SettingState } from './settings';
import { JobsState } from './jobs';
import { RunnerState } from './runner';

export interface RootState {
  anime: AnimeState;
  route: RouteState;
  health: HealthState;
  title: TitleState;
  status: StatusState;
  season: SeasonState;
  seasonYear: SeasonYearState;
  format: FormatState;
  genre: GenreState;
  studio: StudioState;
  settings: SettingState;
  jobs: JobsState;
  runner: RunnerState;
}
