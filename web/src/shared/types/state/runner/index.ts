import { EmptyFetchState, FetchState } from '@/shared/types/generic';
import { Runner } from '@/shared/types/runner';

export interface RunnerState {
  runners: FetchState<Runner[], string>;
  update: EmptyFetchState<string>;
}

export enum RunnerMutations {
  RUNNER_SUCCESS = 'RUNNER_SUCCESS',
  RUNNER_PENDING = 'RUNNER_PENDING',
  RUNNER_FAILURE = 'RUNNER_FAILURE',

  STATE_RUNNER_SUCCESS = 'STATE_RUNNER_SUCCESS',
  STATE_RUNNER_PENDING = 'STATE_RUNNER_PENDING',
  STATE_RUNNER_FAILURE = 'STATE_RUNNER_FAILURE',
}
