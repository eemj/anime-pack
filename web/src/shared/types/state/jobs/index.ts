import { EmptyFetchState, FetchState, Nullable } from '../../generic';
import { Job, JobState } from '../../jobs';

export enum JobsMutation {
  JOBS_PENDING = 'JOBS_PENDING',
  JOBS_SUCCESS = 'JOBS_SUCCESS',
  JOBS_FAILURE = 'JOBS_FAILURE',

  JOBS_UPDATE_STATUS = 'JOBS_UPDATE_STATUS'
}

export const JobActionMutation = {
  JOB_STATE_PENDING(state: JobState) {
    return `JOB_STATE_${state}_PENDING`;
  },
  JOB_STATE_SUCCESS(state: JobState) {
    return `JOB_STATE_${state}_SUCCESS`;
  },
  JOB_STATE_FAILURE(state: JobState) {
    return `JOB_STATE_${state}_FAILURE`;
  },
};

export type JobAction = EmptyFetchState<string>;

export interface JobsState extends FetchState<Nullable<Job[]>, string> {
  actions: Record<Job['id'], JobAction>;
}
