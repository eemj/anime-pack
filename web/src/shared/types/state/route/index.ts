export interface RouteState {
  name: string;
}

export enum RouteMutations {
  ROUTE_UPDATE = 'ROUTE_UPDATE',
}
