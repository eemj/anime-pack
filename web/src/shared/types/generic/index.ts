import { Page } from '@/shared/types/api';

export type Nullable<T> = T | null;

export interface DynamicElements {
  elements: Nullable<number>;
}

export interface Paginated {
  page: Nullable<Page>;
}

export interface EmptyFetchState<E> {
  error: Nullable<E>;
  loading: boolean;
}

export interface FetchState<D, E> extends EmptyFetchState<E> {
  data: D;
}

export interface NullableTree<T> {
  [key: string]: Nullable<T>;
}
