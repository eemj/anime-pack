export enum JobState {
    ENABLE = 'ENABLE',
    DISABLE = 'DISABLE',
    ACTIVE = 'ACTIVE',
    PASSIVE = 'PASSIVE'
}

export const JobStates = [
  JobState.ACTIVE,
  JobState.DISABLE,
  JobState.ENABLE,
  JobState.PASSIVE,
];

export interface Job {
    readonly id: string;
    readonly name: string;
    readonly state: JobState;
    readonly period: number;
    readonly lastActivity: Date;
    readonly startedAt?: Date;
    readonly endedAt?: Date;
}
