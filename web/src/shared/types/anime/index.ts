export enum AnimeProvider {
  ANILIST = 'ANILIST',
  MYANIMELIST = 'MYANIMELIST'
}

export enum AnimeFormat {
  TV = 'TV',
  TV_SHORT = 'TV_SHORT',
  OVA = 'OVA',
  MOVIE = 'MOVIE',
  SPECIAL = 'SPECIAL',
  ONA = 'ONA',
  MUSIC = 'MUSIC',
}

export enum AnimeStatus {
  FINISHED = 'FINISHED',
  RELEASING = 'RELEASING',
  NOTYETRELEASED = 'NOT_YET_RELEASED',
  CANCELLED = 'CANCELLED',
  HIATUS = 'HIATUS',
}

export enum AnimeSeason {
  WINTER = 'WINTER',
  FALL = 'FALL',
  SUMMER = 'SUMMER',
  SPRING = 'SPRING',
}

export interface Image {
  readonly id: number;
  readonly name: string;
  readonly hash?: string;
}

export interface Bot {
  readonly id: number;
  readonly name: string;
}

export interface ReleaseGroup {
  readonly id: number;
  readonly name: string;
}

export interface Quality {
  readonly id: number;
  readonly height: number;
}

export interface Video {
  readonly path: string;
  readonly duration: number;
  readonly size: number;
  readonly crc32: string;
  readonly thumbnail: Image;
  readonly quality?: Quality;
}

export interface Preferences {
  readonly automaticDownloads: boolean;
  readonly performChecksum: boolean;
  readonly favourite: boolean;
}

export interface Genre {
  readonly id: number;
  readonly name: string;
}

export interface Studio {
  readonly id: number;
  readonly name: string;
}

export interface Episode {
  readonly id: number;
  readonly name: string;
}

export interface AnimeLatest {
  readonly id: number;
  readonly idMAL: number;
  readonly titleEnglish?: string;
  readonly titleRomaji?: string;
  readonly titleNative?: string;
  readonly title: string;
  readonly colour?: string;
  readonly poster: Image;
  readonly episode: Episode;
}

export enum Weekday {
  Sunday = 0,
  Monday = 1,
  Tuesday = 2,
  Wednesday = 3,
  Thursday = 4,
  Friday = 5,
  Saturday = 6,
}

export interface AnimeSimulcast {
  readonly id: number;
  readonly idMAL: number;
  readonly titleEnglish?: string;
  readonly titleRomaji?: string;
  readonly titleNative?: string;
  readonly title: string;
  readonly score: number;
  readonly description: string;
  readonly colour?: string;
  readonly poster: Image;
  readonly banner?: Image;
  readonly format: string;
  readonly season?: string;
  readonly year?: number;
  readonly nextAiringDate: Date;
  readonly weekDay: number;
  readonly genres: Genre[];
  readonly studios: Studio[];
}

export interface ExtendedAnimeSimulcast extends AnimeSimulcast {
  readonly elapsedMs: number;
  readonly duration: string;
}

export interface Anime {
  readonly id: number;
  readonly idMAL: number;
  readonly score: number;
  readonly description: string;
  readonly startDate?: string | Date;
  readonly endDate?: string | Date;
  readonly nextAiringDate?: string | Date;
  readonly titleEnglish?: string;
  readonly titleRomaji?: string;
  readonly titleNative?: string;
  readonly title: string;
  readonly format: AnimeFormat;
  readonly status?: AnimeStatus;
  readonly poster: Image;
  readonly banner?: Image;
  readonly colour?: string;
  readonly genres: Genre[];
  readonly studios?: Studio[];
  readonly season?: AnimeSeason;
  readonly year?: number;

  readonly preferences?: Preferences;
}
