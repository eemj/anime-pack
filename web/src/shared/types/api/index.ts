export interface PageRequest {
  page: number;
  elements: number;
}

export interface Page {
  current: number;
  total: number;
  elements: number;
  remaining: number;
  hasNextPage: boolean;
}

export interface PaginatedData<T> {
  page: Page;
  data: T;
}

export interface APIResponse<T> {
  data?: T;
  error?: string;
  code?: number;
  page?: Page;
}

// TODO: migrate everything to this
export type AsyncResponse<T> = Promise<APIResponse<T>>;
