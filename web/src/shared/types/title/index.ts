import { Anime, AnimeProvider } from '@/shared/types/anime';

export interface Title {
  readonly id: number;
  readonly name: string;
  readonly reviewed: boolean;
  readonly seasonNumber?: string;

  readonly filenames?: string[];
  readonly anime?: Anime[];
}

export interface TitleReview extends Title {
  provider?: AnimeProvider;
  providerId?: number;
  previewFilenames: boolean;
}
