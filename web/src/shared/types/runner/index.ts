export enum RunnerState {
  UNKNOWN = 'UNKNOWN',
  WAIT = 'WAIT',
  PAUSE = 'PAUSE',
  RUN = 'RUN'
}

export const RunnerStates = [
  RunnerState.UNKNOWN,
  RunnerState.WAIT,
  RunnerState.PAUSE,
  RunnerState.RUN,
];

export interface RunnerTransfer {
  id: number;
  title: string;
  episode: string;
  height: number;
  releaseGroup: string;
}

export interface Runner {
  readonly transfer?: RunnerTransfer;
  readonly alias?: string;
  readonly user: string;
  readonly hostname: string;
  readonly ident: string;
  readonly docker: boolean;
  readonly ms: number;
  state: RunnerState;
}

export type Runners = Runner[];

export interface ToggleRunner {
  readonly state: RunnerState;
}
