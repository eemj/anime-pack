import { Nullable } from '@/shared/types/generic';

export enum Opcode {
  Acknowledged = 0,
  Exception = 1,

  ActivityProgress = 10,
  ActivityMetadata = 11,
  ActivityDownload = 12,
  ActivityCancel = 13,
  ActivityDifference = 14,
  ActivityGetProgress = 15,

  StatusIRC = 20,
  StatusJob = 21
}

export type DataAcknowledge = Record<string, never>;

export enum ProgressType {
  Metadata = 'metadata',
  Download = 'download',
  Upload = 'upload',
  Transcode = 'transcode',
}

export interface DataExceptionMetadata {
  t: string;
  e: string;
  h: number;
  rg: string;
  th: string;
  crc32: string;
  dur: number;
  sz: number;
}

export interface DataException {
  m: string;
}

export interface DataProgressItem {
  typ: ProgressType;
  pct: number;
  sp: number;
  // eslint-disable-next-line camelcase
  cur_sz?: number;
  sz?: number;
  // eslint-disable-next-line camelcase
  cur_dur?: number;
  dur?: number;
}

export interface DataActivityProgress {
  [key: string]: {
    d: number;
    rg: {
      [key: string]: {
        [key: string]: {
          [key: number]: Nullable<DataProgressItem>;
        };
      };
    };
  };
}

export interface DataDownloadCancelItem {
  d: number;
  q: number;
  rg: number;
}

export interface DataDownloadItem extends DataDownloadCancelItem {
  p?: boolean;
}

export enum DataJobState {
  ENABLE = 'ENABLE',
  DISABLE = 'DISABLE',
  PASSIVE = 'PASSIVE',
  ACTIVE = 'ACTIVE'
}

export interface DataStatusJob {
  d: string;
  s: DataJobState;
  la: Date;
  na?: Date;
  sat?: Date;
  eat?: Date;
  p: number;
}

export type DataActivityDownload = DataDownloadItem[];

export type DataActivityCancel = DataDownloadCancelItem[];

export interface DataStatusIRC {
  c: boolean;
}

export type DataActivityGetProgress = Record<string, never>;

export enum DifferenceToken {
  UNDEFINED = '?',
  ADD = '+',
  REMOVE = '-',
  MODIFY = '.',
}

export interface Anime {
  d: number;
  t: string;
  e: string;
  h: number;
  rg: string;
}

export interface DataActivityDifference {
  tkn: DifferenceToken;
  an: Anime;
  pg: DataProgressItem;
}

export type DataActivityDifferences = DataActivityDifference[];

export type Data =
  | DataAcknowledge
  | DataException
  | DataExceptionMetadata
  | DataActivityDownload
  | DataActivityCancel
  | DataActivityProgress
  | DataActivityGetProgress
  | DataActivityDifferences
  | DataStatusJob
  | DataStatusIRC;

export interface Payload {
  op: Opcode;
  d: Data;
}
