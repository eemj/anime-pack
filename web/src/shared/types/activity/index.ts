import { Nullable } from '@/shared/types/generic';
import { DataProgressItem } from '@/shared/types/protocol';

export interface AnimeActivityEpisode {
  name: string;
  group: string;
  height: number;
  progress: Nullable<DataProgressItem>;
}

export interface AnimeActivitySummary {
  inProgress: number;
  totalPercentage: number;
}

export interface AnimeActivity {
  id: number;
  title: string;
  episodes: AnimeActivityEpisode[];
  summary: AnimeActivitySummary;
}
