function chunk<T>(array: T[], size: number) {
  return Array.from(
    { length: Math.ceil(array.length / size) },
    (_, index) => array.slice(index * size, index * size + size),
  );
}

export default { chunk };
