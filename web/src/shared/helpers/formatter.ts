import { DataProgressItem, ProgressType } from '@/shared/types/protocol';
import { Nullable } from '@/shared/types/generic';
import { AnimeStatus, AnimeFormat } from '@/shared/types/anime';

import dayjs from 'dayjs';
import dayjsDuration from 'dayjs/plugin/duration';
import dayjsUtc from 'dayjs/plugin/utc';
import dayjsRelativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(dayjsDuration);
dayjs.extend(dayjsRelativeTime);
dayjs.extend(dayjsUtc);

const UNITS = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
const UNITS_LENGTH = UNITS.length;
const SIZE_THRESHOLD = 1024;
const DECIMAL_POINTER = 2;
const RATE = 10 ** DECIMAL_POINTER;

export const capitalize = (phrase: string): string => {
  if (!phrase) return '';

  if (phrase.length === 1) {
    return phrase.toUpperCase();
  }

  return `${phrase.substring(0, 1).toUpperCase()}${phrase
    .substring(1, phrase.length)
    .toLowerCase()}`;
};

export const pad = (number: number, by: number) =>
  String(Math.floor(number)).padStart(by, '0');

export const format = (phrase?: AnimeFormat): string => {
  if (!phrase) return '';

  if (
    phrase === AnimeFormat.TV
    || phrase === AnimeFormat.OVA
    || phrase === AnimeFormat.ONA
  ) {
    return phrase?.toString();
  }

  if (phrase === AnimeFormat.TV_SHORT) {
    return 'TV Short';
  }

  return capitalize(phrase);
};

export const status = (phrase?: AnimeStatus): string => {
  if (phrase) {
    return phrase.split('_').map(capitalize).join(' ');
  }
  return '';
};

export const date = (timestamp: string | Date): string => {
  const d = timestamp instanceof Date ? timestamp : new Date(timestamp);

  return d.getTime() === 0 || d.getTime() < 0
    ? '?'
    : `${d.toDateString().split(' ')[1]} ${d.getDate()}, ${d.getFullYear()}`;
};

export const duration = (msec: number): string =>
  `${pad(msec / 3600, 2)}:${pad((msec / 60) % 60, 2)}:${pad(
    msec % 60,
    2,
  )}.${pad(msec / 1000, 3)}`;

export const season = (phrase: string) => capitalize(phrase);

export const release = (seasonName?: string, seasonYear?: number) => {
  if (seasonName && seasonYear) {
    return `${season(seasonName)} ${seasonYear}`;
  }
  if (seasonName && !seasonYear) {
    return `${season(seasonName)} ?`;
  }
  if (!seasonName && seasonYear) {
    return `? ${seasonYear}`;
  }
  return '?';
};

export const filesize = (bytes: number): string => {
  if (Math.abs(bytes) < SIZE_THRESHOLD) {
    return `${bytes}B`;
  }

  let u = -1;

  do {
    // eslint-disable-next-line no-param-reassign
    bytes /= SIZE_THRESHOLD;
    ++u;
  } while (
    Math.round(Math.abs(bytes) * RATE) / RATE >= SIZE_THRESHOLD
    && u < UNITS_LENGTH - 1
  );

  return `${bytes.toFixed(DECIMAL_POINTER)} ${UNITS[u]}`;
};

export const speed = (progress: Nullable<DataProgressItem>): string => {
  if (progress === null) {
    return '';
  }

  if (
    [
      ProgressType.Metadata,
      ProgressType.Download,
      ProgressType.Upload,
    ].includes(progress.typ)
  ) {
    return `${(progress.sp / 1024 / 1024).toFixed(DECIMAL_POINTER)}MB/s`;
  }

  return `${progress.sp}x`;
};

export const percentage = (value: Nullable<number>): string =>
  `${(value === null ? 0 : value).toFixed(2)}%`;

export const goDuration = (value: number, delimiter = ''): string => {
  const elapsed = dayjs.duration(value, 'milliseconds');

  const hours = elapsed.hours();
  const minutes = elapsed.minutes();
  const seconds = elapsed.seconds();
  const milliseconds = elapsed.milliseconds();

  return [
    `${hours > 0 ? `${hours}h` : ''}`,
    `${minutes > 0 ? `${minutes}m` : ''}`,
    `${seconds > 0 ? `${seconds}s` : ''}`,
    `${milliseconds > 0 ? `${milliseconds}ms` : ''}`,
  ]
    .join(delimiter)
    .trim();
};

export interface HumanDuration {
  readonly elapsedMs: number;
  readonly duration: string;
}

const pluralize = (value: number, text: string) =>
  `${value} ${text}${value === 1 ? '' : 's'}`;

export const humanDuration = (timestamp: Date): HumanDuration => {
  const now = dayjs.utc();

  let current = dayjs.utc(timestamp);
  let diff = current.diff(now);

  if (diff <= 0) {
    // Add 7 days, if we're negative.
    current = current.add(dayjs.duration({ weeks: 1 }));
    diff = current.diff(now);
  }

  const elapsed = dayjs.duration(diff);

  const days = elapsed.days();
  const hours = elapsed.hours();

  if (days > 0 && hours >= 0) {
    return {
      elapsedMs: elapsed.asMilliseconds(),
      duration: `${pluralize(days, 'day')}, ${pluralize(hours, 'hour')}`,
    };
  }

  const minutes = elapsed.minutes();

  if (hours > 0 && minutes >= 0) {
    return {
      elapsedMs: elapsed.asMilliseconds(),
      duration: `${pluralize(hours, 'hour')}, ${pluralize(minutes, 'minute')}`,
    };
  }

  const seconds = elapsed.seconds();

  return {
    elapsedMs: elapsed.asMilliseconds(),
    duration: `~ ${pluralize(minutes, 'minute')}, ${pluralize(seconds, 'second')}`,
  };
};
