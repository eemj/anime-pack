/* eslint-disable import/prefer-default-export */

import { reactive, watch } from 'vue';
import { DEFAULT_ELEMENTS, MAX_ELEMENTS } from '@/adapter';
import { useStore } from 'vuex';

/**
 * Represents the parameters used for calculating dynamic elements.
 * @interface
 */
export interface DynamicElementParam {
  /**
   * The width of the element (including padding and margins).
   * @type {number}
   * @description Please ensure this width takes into consideration any padding and
   * margins of the element.
   */
  readonly elementWidth: number;

  /**
   * The number of rows.
   * @type {number}
   */
  readonly rows: number;
}

export function useDynamicElements(param: DynamicElementParam): { elements: number } {
  const store = useStore();
  const elementsState = reactive<{ elements: number }>({
    elements: DEFAULT_ELEMENTS,
  });

  watch(
    () => store.state.settings.dimension.width,
    (newPageWidth) => {
      const computedElements = Math.floor((newPageWidth / param.elementWidth) * param.rows);
      elementsState.elements = Math.max(Math.min(computedElements, MAX_ELEMENTS), DEFAULT_ELEMENTS);
    },
    { immediate: true },
  );

  return elementsState;
}
