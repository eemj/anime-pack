import {
  ref, Ref, onMounted, onUnmounted,
} from 'vue';

export interface Scroll {
  x: Ref<number>;
  y: Ref<number>;
}

export default function useScroll(): Scroll {
  const x = ref(window.scrollX);
  const y = ref(window.scrollY);
  const update = () => {
    x.value = window.scrollX;
    y.value = window.scrollY;
  };

  onMounted(() => {
    window.addEventListener('scroll', update);
  });

  onUnmounted(() => {
    window.removeEventListener('scroll', update);
  });

  return { x, y };
}
