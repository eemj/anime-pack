import {
  ref, onMounted, onUnmounted, Ref,
} from 'vue';

export interface WindowSize {
  width: Ref<number>;
  height: Ref<number>;
}

export default function useWindowSize(): WindowSize {
  const width = ref(window.innerWidth);
  const height = ref(window.innerHeight);
  const update = () => {
    if (window) {
      width.value = window.innerWidth;
      height.value = window.innerHeight;
    }
  };

  onMounted(() => {
    window.addEventListener('resize', update, { passive: true });
  });

  onUnmounted(() => {
    window.removeEventListener('resize', update);
  });

  update();

  return {
    width,
    height,
  };
}
