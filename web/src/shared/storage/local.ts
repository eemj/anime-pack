import { Nullable } from '@/shared/types/generic';

function first<T>(key: string): Nullable<T> {
  const value = window.localStorage.getItem(key);
  return value !== null ? (JSON.parse(value) as T) : null;
}

export default { first };
