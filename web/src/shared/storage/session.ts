export interface Expiry<T> {
  value: T;
  expiresIn: Date;
}

function getKeys() {
  const keys = [];
  let key: string | null = '';
  for (let index = 0; ; index++) {
    key = window.sessionStorage.key(index);
    if (key === null) {
      break;
    }
    keys.push(key);
  }
  return keys;
}

function getItems() {
  return getKeys()
    .map((key) => window.sessionStorage.getItem(key))
    .map((value) => (value !== null ? JSON.parse(value) : value))
    .filter((value) => value !== null && 'expiresIn' in value);
}

function first<T>(key: string): Expiry<T> | null {
  const value = window.sessionStorage.getItem(key);
  return value !== null ? JSON.parse(value) : null;
}

function store<T>(key: string, value: T): Expiry<T> {
  const item = first(key);
  const now = new Date();

  if (item !== null) {
    if (item.expiresIn < now) {
      return item as Expiry<T>;
    }

    window.sessionStorage.removeItem(key);
  }

  const expiry = { value, expiresIn: new Date(now.getTime() + 3600000) };
  window.sessionStorage.setItem(key, JSON.stringify(expiry));
  return expiry;
}

export default {
  getKeys,
  getItems,
  first,
  store,
};
