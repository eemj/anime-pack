import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';

const Home = () => import(/* webpackChunkName: "home" */ '@/views/Home.vue');
const Activity = () => import(/* webpackChunkName: "activity" */ '@/views/Activity.vue');
const Titles = () => import(/* webpackChunkName: "titles" */ '@/views/Titles.vue');
const Anime = () => import(/* webpackChunkName: "anime" */ '@/views/Anime.vue');
const AnimeEdit = () => import(/* webpackChunkName: "anime" */ '@/views/AnimeEdit.vue');
const Favourite = () => import(/* webpackChunkName: "favourite" */ '@/views/Favourite.vue');
const Index = () => import(/* webpackChunkName: "index" */ '@/views/Index.vue');
const NotFound = () => import(/* webpackChunkName: "not_found" */ '@/views/NotFound.vue');
const Airing = () => import(/* webpackChunkName: "airing" */ '@/views/Airing.vue');
const Episode = () => import(/* webpackChunkName: "episode" */ '@/views/Episode.vue');
const AdvancedSearch = () => import(/* webpackChunkName: "advanced_search" */ '@/views/AdvancedSearch.vue');
const EpisodeLatest = () => import(/* webpackChunkName: "episode_latest" */ '@/views/EpisodeLatest.vue');
const Jobs = () => import(/* webpackChunkName: "jobs" */ '@/views/Jobs.vue');
const Simulcast = () => import(/* webpackChunkName: "simulcast" */ '@/views/Simulcast.vue');
const Runners = () => import(/* webpackChunkName: "runner" */ '@/views/Runners.vue');

export enum RouteName {
  HOME = 'home',
  ACTIVITY = 'activity',
  TITLES = 'titles',
  INDEX = 'index',
  ADVANCED_SEARCH = 'advanced_search',
  FAVOURITE = 'favourite',
  AIRING = 'airing',
  ANIME_EDIT = 'anime_edit',
  ANIME_DETAILS = 'anime_details',
  NOT_FOUND = 'not_found',
  EPISODE = 'episode',
  EPISODE_LATEST = 'episode_latest',
  JOBS = 'jobs',
  SIMULCAST = 'simulcast',
  RUNNERS = 'runners'
}

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      title: 'Anime Pack | Home',
    },
  },
  {
    path: '/activity',
    name: 'activity',
    component: Activity,
    meta: {
      title: 'Anime Pack | Activity',
    },
  },
  {
    path: '/titles',
    name: 'titles',
    component: Titles,
    meta: {
      title: 'Anime Pack | Titles',
    },
  },
  {
    path: '/anime/index/:index',
    name: 'index',
    component: Index,
    meta: {
      title: 'Anime Pack | Index',
    },
  },
  {
    path: '/anime/search',
    name: 'advanced_search',
    component: AdvancedSearch,
    meta: {
      title: 'Anime Pack | Advanced Search',
    },
  },
  {
    path: '/anime/favourite',
    name: 'favourite',
    component: Favourite,
    meta: {
      title: 'Anime Pack | Favourite',
    },
  },
  {
    path: '/anime/airing',
    name: 'airing',
    component: Airing,
    meta: {
      title: 'Anime Pack | Airing',
    },
  },
  {
    path: '/anime/:id',
    name: 'anime_details',
    component: Anime,
    meta: {
      title: 'Anime Pack | Loading..',
    },
  },
  {
    path: '/anime/:id/edit',
    name: 'anime_edit',
    component: AnimeEdit,
    meta: {
      title: 'Anime Pack | Loading..',
    },
  },
  {
    path: '/anime/:id/episode/:episode',
    name: 'episode',
    component: Episode,
    meta: {
      title: 'Anime Pack | Loading..',
    },
  },
  {
    path: '/latest/episode',
    name: 'episode_latest',
    component: EpisodeLatest,
    meta: {
      title: 'Anime Pack | Latest Episodes',
    },
  },
  {
    path: '/jobs',
    name: 'jobs',
    component: Jobs,
    meta: {
      title: 'Anime Pack | Jobs',
    },
  },
  {
    path: '/simulcast',
    name: 'simulcast',
    component: Simulcast,
    meta: {
      title: 'Anime Pack | Simulcast',
    },
  },
  {
    path: '/runners',
    name: 'runners',
    component: Runners,
    meta: {
      title: 'Anime Pack | Runners',
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'not_found',
    component: NotFound,
    meta: {
      title: 'Anime Pack | Not Found',
    },
  },
];

const history = createWebHashHistory(process.env.BASE_URL);

const router = createRouter({ history, routes });

router.afterEach((to) => {
  document.title = String(to.meta?.title || 'Anime Pack');
});

export default router;
