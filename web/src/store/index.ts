import { createStore, createLogger } from 'vuex';
import { RootState } from '@/shared/types/state';
import { Router } from 'vue-router';

import createRouterPlugin from '@/store/plugins/router';
import createWebSocketPlugin from '@/store/plugins/websocket';

import anime from '@/store/modules/anime';
import route from '@/store/modules/route';
import health from '@/store/modules/health';
import title from '@/store/modules/title';
import format from '@/store/modules/format';
import genre from '@/store/modules/genre';
import season from '@/store/modules/season';
import seasonYear from '@/store/modules/season/year';
import status from '@/store/modules/status';
import studio from '@/store/modules/studio';
import settings from '@/store/modules/settings';
import jobs from '@/store/modules/jobs';
import runner from '@/store/modules/runner';

const createPlugins = (router: Router) => {
  const plugins = [createWebSocketPlugin(), createRouterPlugin(router)];

  if (process.env.NODE_ENV === 'development' || 'webpackHotUpdate' in window) {
    plugins.push(createLogger());
  }

  return plugins;
};

const store = (router: Router) => createStore<RootState>({
  plugins: createPlugins(router),
  modules: {
    anime,
    route,
    health,
    title,
    format,
    genre,
    season,
    seasonYear,
    status,
    studio,
    settings,
    jobs,
    runner,
  },
});

export default store;
