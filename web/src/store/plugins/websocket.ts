/* tslint:disable:no-console */
import { Store } from 'vuex';
import extensions from '@/shared/helpers/extensions';
import {
  DataActivityDifferences,
  DataException,
  Opcode,
  type DataActivityProgress,
  type DataDownloadItem,
  type DataExceptionMetadata,
  type DataStatusJob,
  type Payload,
} from '@/shared/types/protocol';
import paths from '@/adapter/paths';
import { RootState } from '@/shared/types/state';
import { AnimeActivityMutations } from '@/shared/types/state/anime/activity';
import { HealthMutations } from '@/shared/types/state/health';
import { RouteMutations } from '@/shared/types/state/route';

const URI = `${window.location.protocol.replace('http', 'ws')}//${
  window.location.host
}${paths.Socket()}`;

const CHUNK_OFFSET = 10;

const createWebSocket = (store: Store<RootState>, socket?: WebSocket): Promise<WebSocket> =>
  new Promise((resolve) => {
    const retryConnection = () => {
      window.setTimeout(async () => {
        try {
          socket = await createWebSocket(store, socket);
        } catch (error) {
          console.error('[WebSocket] connection failed:', error);
          retryConnection(); // Retry connection if it fails
        }
      }, 1000); // Retry after 1 second
    };

    const connectWebSocket = (): WebSocket => {
      socket = new WebSocket(URI, ['anime-pack.v1']);

      socket.addEventListener('close', (event: CloseEvent) => {
        if (store?.state?.health.websocket) {
          store.commit(HealthMutations.WEBSOCKET_DISCONNECTED);
        }

        if (event.code > 0) {
          console.error('[WebSocket] connection closed (%d), reason (%s)', event.code, event.reason);
        } else {
          console.info('[WebSocket] connection closed (%d)', event.code);
        }

        // Shouldn't trigger..
        if (socket?.readyState === WebSocket.OPEN) {
          socket?.close();
        }

        retryConnection();
      });

      socket.addEventListener('open', () => {
        console.info('[WebSocket] connection established');
      });

      socket.addEventListener('error', (error: Event) => {
        console.error('[WebSocket]', error, JSON.stringify(error));
      });

      socket.addEventListener('message', (message: MessageEvent) => {
        const payload = JSON.parse(message.data) as Payload;

        switch (payload.op) {
          case Opcode.Exception:
            {
              const exception = (payload.d as DataException).m;
              if (exception) {
                console.error(exception); // TODO: Move this to a notification popup or something...
              }
            }
            break;
          case Opcode.Acknowledged:
            store.commit(HealthMutations.WEBSOCKET_CONNECTED);
            store.commit(AnimeActivityMutations.ACTIVITY_FETCH_PROGRESS);
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            resolve(socket!);
            break;
          case Opcode.ActivityDifference:
            store.dispatch(
              'applyActivityDifference',
              payload.d as DataActivityDifferences,
            );
            break;
          case Opcode.ActivityProgress:
            store.dispatch(
              'updateActivityData',
              payload.d as DataActivityProgress,
            );
            break;
          case Opcode.ActivityMetadata:
            store.dispatch('episodeMetadata', payload.d as DataExceptionMetadata);
            break;
          case Opcode.StatusJob:
            store.dispatch('updateJobStatus', payload.d as DataStatusJob);
            break;
          default:
            break;
        }
      });

      return socket;
    };

    socket = connectWebSocket();
  });

const createWebSocketPlugin = () => {
  let socket: WebSocket | undefined;

  return async (store: Store<RootState>) => {
    console.info('[WebSocket] injecting store plugin');

    if (socket === undefined) {
      socket = await createWebSocket(store, socket);
    }

    store.subscribe(async (mutation) => {
      if (
        mutation.type !== HealthMutations.WEBSOCKET_SEND
          && mutation.type !== AnimeActivityMutations.ACTIVITY_FETCH_PROGRESS
          && mutation.type !== RouteMutations.ROUTE_UPDATE
      ) {
        return;
      }

      if (socket && socket.readyState !== WebSocket.OPEN) {
        socket = await createWebSocket(store);
      }

      const { payload } = mutation;

      switch (mutation.type) {
        case HealthMutations.WEBSOCKET_SEND:
          // we'll send out the items in chunks if they're 'Download' or
          // 'DownloadCancel' opcodes
          if (
            payload.op === Opcode.ActivityDownload
              || payload.op === Opcode.ActivityCancel
          ) {
            const { d } = payload;

            try {
              await Promise.all(
                extensions.chunk<DataDownloadItem>(d, CHUNK_OFFSET).map(
                  (chunk: DataDownloadItem[], index: number): Promise<void> =>
                    new Promise((resolve, reject) => {
                      try {
                        socket?.send(
                          JSON.stringify({
                            op: payload.op,
                            d: chunk,
                          }),
                        );

                        console.info('[WebSocket] sent (chunk:%d) %o', index, chunk);

                        resolve();
                      } catch (e) {
                        reject(e);
                      }
                    }),
                ),
              );
            } catch (e) {
              console.error('[WebSocket] send exception', e);
            }
          }
          break;

        case AnimeActivityMutations.ACTIVITY_FETCH_PROGRESS:
          socket?.send(
            JSON.stringify({
              op: Opcode.ActivityGetProgress,
              d: {},
            } as Payload),
          );
          break;

        default:
          break;
      }
    });
  };
};

export default createWebSocketPlugin;
