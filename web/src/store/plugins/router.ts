import { Router } from 'vue-router';
import { Store } from 'vuex';
import { RootState } from '@/shared/types/state';
import { RouteMutations } from '@/shared/types/state/route';

const createRouterPlugin = (router: Router) => (store: Store<RootState>) => {
  router.afterEach((to) => {
    store.commit(RouteMutations.ROUTE_UPDATE, to.name);
  });
};

export default createRouterPlugin;
