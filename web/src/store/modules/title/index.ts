import { MutationTree, ActionTree, ActionContext } from 'vuex';
import { RootState } from '@/shared/types/state';
import {
  TitleState,
  TitleMutations,
  TitleAddAnimePayload,
  TitleUpdatePayload,
} from '@/shared/types/state/title';
import { Title } from '@/shared/types/title';
import { APIResponse } from '@/shared/types/api';
import { Anime } from '@/shared/types/anime';
import { context } from '@/adapter/request';
import adapter, { DEFAULT_ELEMENTS } from '@/adapter';

const threshold = 4;

type Context = ActionContext<TitleState, RootState>;

const defaultState: TitleState = {
  update: {
    loading: false,
    error: null,
    updated: 0,
  },
  delete: {
    loading: false,
    error: null,
  },
  details: {
    loading: false,
    error: null,
    data: null,
    page: null,
  },
};

const mutations: MutationTree<TitleState> = {
  [TitleMutations.TITLE_PENDING](state: TitleState) {
    state.details.loading = true;
    state.details.error = null;
    state.details.data = null;
  },
  [TitleMutations.TITLE_FAILURE](state: TitleState, error: string) {
    state.details.loading = false;
    state.details.error = error;
    state.details.data = null;
  },
  [TitleMutations.TITLE_SUCCESS](
    state: TitleState,
    { data, page }: APIResponse<Title[]>,
  ) {
    state.details.loading = false;
    state.details.error = null;

    if (data && page) {
      state.details.data = data;
      state.details.page = page;
    }
  },

  [TitleMutations.UPDATE_TITLE_PENDING](state: TitleState) {
    state.update.loading = true;
    state.update.error = null;
  },
  [TitleMutations.UPDATE_TITLE_FAILURE](state: TitleState, error: string) {
    state.update.loading = false;
    state.update.error = error;
  },
  [TitleMutations.UPDATE_TITLE_SUCCESS](state: TitleState, data: Title) {
    state.update.loading = false;
    state.update.error = null;

    state.update.updated += 1;
    if (state.update.updated === threshold) {
      state.update.updated = 0;
    }

    if (state.details.data) {
      const index = state.details.data.findIndex(
        (title: Title) => title.id === data.id,
      );

      if (index >= 0) {
        state.details.data.splice(index, 1);
      }
    }
  },

  [TitleMutations.DELETE_TITLE_PENDING](state: TitleState) {
    state.delete.loading = true;
    state.delete.error = null;
  },
  [TitleMutations.DELETE_TITLE_FAILURE](state: TitleState, error: string) {
    state.delete.loading = false;
    state.delete.error = error;
  },
  [TitleMutations.DELETE_TITLE_SUCCESS](state: TitleState, index: number) {
    state.delete.loading = false;
    state.delete.error = null;

    if (state.details.data) {
      state.details.data.splice(index, 1);
    }
  },

  [TitleMutations.ADD_ANIME_TITLE](
    state: TitleState,
    payload: TitleAddAnimePayload,
  ) {
    if (state.details.data) {
      const titleIndex = state.details.data.findIndex(
        (title: Title) => title.id === payload.id,
      );

      if (titleIndex >= 0) {
        if (state.details.data[titleIndex].anime) {
          const animeIndex = state.details.data[titleIndex].anime?.findIndex(
            (anime: Anime) => anime.id === payload.anime.id,
          ) ?? -1;

          if (animeIndex === -1) {
            state.details.data[titleIndex].anime?.push(payload.anime);
          }
        } else {
          state.details.data[titleIndex] = {
            ...state.details.data[titleIndex],
            anime: [payload.anime],
          };
        }
      }
    }
  },
};

const actions: ActionTree<TitleState, RootState> = {
  addTitleAnime(ctx: Context, payload: TitleAddAnimePayload) {
    ctx.commit(TitleMutations.ADD_ANIME_TITLE, payload);
  },

  async getTitles(ctx: Context, page: number): Promise<void> {
    if (ctx.state.details.loading || ctx.state.update.loading) {
      return;
    }

    await context<TitleState, RootState, Title[]>(
      ctx,
      adapter.getTitle({
        page,
        isDeleted: false,
        reviewed: false,
        elements: DEFAULT_ELEMENTS,
      }),
      TitleMutations.TITLE_PENDING,
      TitleMutations.TITLE_FAILURE,
      TitleMutations.TITLE_SUCCESS,
    );
  },

  async deleteTitle(ctx: Context, id: number): Promise<void> {
    if (
      ctx.state.details.loading
      || ctx.state.update.loading
      || ctx.state.details.data === null
    ) {
      return;
    }

    const index = ctx.state.details.data?.findIndex(
      (title: Title) => title.id === id,
    );

    if (index >= 0) {
      ctx.commit(TitleMutations.DELETE_TITLE_PENDING);

      const response = await adapter.deleteTitleId(id);

      if (response.error && response.code) {
        ctx.commit(TitleMutations.DELETE_TITLE_FAILURE, response.error);
      } else {
        ctx.commit(TitleMutations.DELETE_TITLE_SUCCESS, index);
      }
    }
  },

  async updateTitle(ctx: Context, payload: TitleUpdatePayload): Promise<void> {
    if (
      ctx.state.details.loading
      || ctx.state.update.loading
      || ctx.state.details.data === null
    ) {
      return;
    }

    if (ctx.state.details.data) {
      const index = ctx.state.details.data.findIndex(
        (title: Title) => title.id === payload.id,
      );

      if (index >= 0) {
        context<TitleState, RootState, Title>(
          ctx,
          adapter.patchTitleId(payload.id, payload.body),
          TitleMutations.UPDATE_TITLE_PENDING,
          TitleMutations.UPDATE_TITLE_FAILURE,
          TitleMutations.UPDATE_TITLE_SUCCESS,
        ).then(() => {
          // Check the update threshold.. if we exceeded we'll get more items..
          if (ctx.state.update.updated >= threshold && ctx.state.details.page) {
            ctx.dispatch('getTitles', ctx.state.details.page?.current);
          }
        });
      }
    }
  },
};

export default {
  state: defaultState,
  mutations,
  actions,
};
