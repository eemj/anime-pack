import adapter from '@/adapter';
import { Runner, RunnerState as RunnerStateEnum } from '@/shared/types/runner';
import { RootState } from '@/shared/types/state';
import { RunnerState, RunnerMutations } from '@/shared/types/state/runner';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

type Context = ActionContext<RunnerState, RootState>;

const defaultState: RunnerState = {
  runners: {
    loading: false,
    data: [],
    error: null,
  },
  update: {
    loading: false,
    error: null,
  },
};

interface StateRunnerSuccess {
  readonly ident: string;
  readonly state: RunnerStateEnum;
}

const mutations: MutationTree<RunnerState> = {
  [RunnerMutations.RUNNER_SUCCESS]({ runners }: RunnerState, data: Runner[]) {
    runners.loading = false;
    runners.error = null;
    runners.data = data;
  },
  [RunnerMutations.RUNNER_PENDING]({ runners }: RunnerState) {
    runners.loading = true;
    runners.error = null;
    runners.data = [];
  },
  [RunnerMutations.RUNNER_FAILURE]({ runners }: RunnerState, error: string) {
    runners.loading = false;
    runners.error = error;
    runners.data = [];
  },

  [RunnerMutations.STATE_RUNNER_SUCCESS](
    state: RunnerState,
    data: StateRunnerSuccess,
  ) {
    state.update.loading = false;
    state.update.error = null;

    const runner = state.runners.data?.find((r) => r.ident === data.ident);

    if (runner) {
      runner.state = data.state;
    }
  },
  [RunnerMutations.STATE_RUNNER_PENDING]({ update }: RunnerState) {
    update.loading = true;
    update.error = null;
  },
  [RunnerMutations.STATE_RUNNER_FAILURE](
    { update }: RunnerState,
    error: string,
  ) {
    update.loading = false;
    update.error = error;
  },
};

const actions: ActionTree<RunnerState, RootState> = {
  async getRunners(ctx: Context) {
    if (ctx.state.runners.loading) {
      return;
    }

    ctx.commit(RunnerMutations.RUNNER_PENDING);

    const response = await adapter.getRunners();

    if (response.error && response.code) {
      ctx.commit(RunnerMutations.RUNNER_FAILURE, response.error);
    } else if (response.data) {
      ctx.commit(RunnerMutations.RUNNER_SUCCESS, response.data);
    }
  },

  async toggleRunner(ctx: Context, ident: string) {
    if (ctx.state.update.loading) {
      return;
    }

    ctx.commit(RunnerMutations.STATE_RUNNER_PENDING);

    const response = await adapter.toggleRunnerState(ident);

    if (response.error && response.code) {
      ctx.commit(RunnerMutations.STATE_RUNNER_FAILURE, response.error);
    } else if (response.data) {
      ctx.commit(RunnerMutations.STATE_RUNNER_SUCCESS, {
        ident,
        state: response.data,
      });
    }
  },
};

const stateModule: Module<RunnerState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
