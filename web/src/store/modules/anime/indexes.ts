import {
  AnimeIndexesState,
  AnimeIndexesMutations,
} from '@/shared/types/state/anime/indexes';
import {
  ActionTree, Module, MutationTree, ActionContext,
} from 'vuex';
import { Anime } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { context } from '@/adapter/request';
import adapter from '@/adapter';

type Context = ActionContext<AnimeIndexesState, RootState>;

const defaultState: AnimeIndexesState = {
  indexes: {
    data: null,
    loading: false,
    error: null,
  },
  index: {
    data: {},
    loading: false,
    error: null,
  },
};

const mutations: MutationTree<AnimeIndexesState> = {
  [AnimeIndexesMutations.INDEXES_PENDING](state: AnimeIndexesState) {
    state.indexes.loading = true;
    state.indexes.error = null;
    state.indexes.data = null;
  },
  [AnimeIndexesMutations.INDEXES_SUCCESS](
    state: AnimeIndexesState,
    data: string[],
  ) {
    state.indexes.loading = false;
    state.indexes.error = null;
    state.indexes.data = data;
    state.indexes.data.forEach((index) => {
      state.index.data[index] = null;
    });
  },
  [AnimeIndexesMutations.INDEXES_FAILURE](
    state: AnimeIndexesState,
    error: string,
  ) {
    state.indexes.loading = false;
    state.indexes.error = error;
    state.indexes.data = null;
  },

  [AnimeIndexesMutations.INDEX_PENDING](state: AnimeIndexesState) {
    state.index.loading = true;
    state.index.error = null;
  },
  [AnimeIndexesMutations.INDEX_SUCCESS](state: AnimeIndexesState, data: Anime[]) {
    state.index.loading = false;
    state.index.error = null;

    // Determine what index we received..
    let index = '#';

    if (data?.length > 0) {
      const char = data[0].title.charAt(0).toUpperCase();
      index = char.match(/[A-Z]/i) ? char : '#';
    }

    state.index.data[index] = data;
  },
  [AnimeIndexesMutations.INDEX_FAILURE](
    state: AnimeIndexesState,
    error: string,
  ) {
    state.index.loading = true;
    state.index.error = error;
  },
};

const actions: ActionTree<AnimeIndexesState, RootState> = {
  async getIndexes(ctx: Context) {
    if (ctx.state.indexes.loading) {
      return;
    }

    await context<AnimeIndexesState, RootState, string[]>(
      ctx,
      adapter.getAnimeIndexes(),
      AnimeIndexesMutations.INDEXES_PENDING,
      AnimeIndexesMutations.INDEXES_FAILURE,
      AnimeIndexesMutations.INDEXES_SUCCESS,
    );
  },

  async getIndex(ctx: Context, index: string) {
    if (ctx.state.index.loading || ctx.state.index.data[index]) {
      return;
    }

    await context<AnimeIndexesState, RootState, Anime[]>(
      ctx,
      adapter.getAnimeIndex(index),
      AnimeIndexesMutations.INDEX_PENDING,
      AnimeIndexesMutations.INDEX_FAILURE,
      AnimeIndexesMutations.INDEX_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeIndexesState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default stateModule;
