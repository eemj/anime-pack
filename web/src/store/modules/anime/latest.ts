import {
  ActionTree, MutationTree, Module, ActionContext,
} from 'vuex';
import { AnimeLatestState, AnimeLatestMutations } from '@/shared/types/state/anime/latest';
import { AnimeLatest } from '@/shared/types/anime';
import { APIResponse } from '@/shared/types/api';
import { RootState } from '@/shared/types/state';
import adapter, { DEFAULT_ELEMENTS, DEFAULT_PAGE } from '@/adapter';
import {
  AnimeLatestOrderByQuery,
  AnimeLatestQuery,
  OrderByDirection,
} from '@/adapter/query';

type Context = ActionContext<AnimeLatestState, RootState>;

const DEFAULT_QUERY: AnimeLatestQuery = {
  page: DEFAULT_PAGE,
  elements: DEFAULT_ELEMENTS,
  direction: OrderByDirection.Descending,
  orderBy: AnimeLatestOrderByQuery.CreatedAt,
};

const defaultState: AnimeLatestState = {
  loading: false,
  error: null,
  data: null,
  query: DEFAULT_QUERY,
  page: null,
  elements: null,
};

const mutations: MutationTree<AnimeLatestState> = {
  [AnimeLatestMutations.LATEST_PENDING](state: AnimeLatestState, query: AnimeLatestQuery) {
    state.loading = true;
    state.error = null;
    state.data = null;
    state.page = null;
    state.query = query;
  },
  [AnimeLatestMutations.LATEST_SUCCESS](
    state: AnimeLatestState,
    { data, page }: APIResponse<AnimeLatest[]>,
  ) {
    state.loading = false;
    state.error = null;

    if (page && data) {
      state.data = data;
      state.page = page;
    }
  },
  [AnimeLatestMutations.LATEST_FAILURE](state: AnimeLatestState, error: string) {
    state.loading = false;
    state.data = null;
    state.page = null;
    state.error = error;
  },
};

const actions: ActionTree<AnimeLatestState, RootState> = {
  async getLatest(ctx: Context, query: AnimeLatestQuery) {
    if (ctx.state.loading) {
      return;
    }

    ctx.commit(AnimeLatestMutations.LATEST_PENDING, query);

    try {
      const response = await adapter.getAnimeLatest(query);
      if (response.error) {
        ctx.commit(AnimeLatestMutations.LATEST_FAILURE, response.error);
      }
      if (response.data) {
        ctx.commit(AnimeLatestMutations.LATEST_SUCCESS, response);
      }
    } catch (err) {
      const errMessage = err instanceof Error ? (err as Error).message : err;
      ctx.commit(AnimeLatestMutations.LATEST_FAILURE, errMessage);
    }
  },
};

const stateModule: Module<AnimeLatestState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default stateModule;
