import { Adapter } from '@/adapter';
import { PageQuery } from '@/adapter/query';
import { context } from '@/adapter/request';
import { PaginatedData } from '@/shared/types/api';
import { AnimeEpisodeLatest } from '@/shared/types/episode';
import { RootState } from '@/shared/types/state';
import { AnimeEpisodeLatestMutations, AnimeEpisodeLatestState } from '@/shared/types/state/anime/episodelatest';
import {
  Module, ActionTree, MutationTree, ActionContext,
} from 'vuex';

type Context = ActionContext<AnimeEpisodeLatestState, RootState>;

const defaultState: AnimeEpisodeLatestState = {
  data: null,
  error: null,
  loading: false,
  page: null,
};

const mutations: MutationTree<AnimeEpisodeLatestState> = {
  [AnimeEpisodeLatestMutations.ANIME_EPISODE_SUCCESS](
    state: AnimeEpisodeLatestState,
    data: PaginatedData<AnimeEpisodeLatest[]>,
  ) {
    state.error = null;
    state.loading = false;
    state.data = data.data;
    state.page = data.page;
  },
  [AnimeEpisodeLatestMutations.ANIME_EPISODE_PENDING](state: AnimeEpisodeLatestState) {
    state.error = null;
    state.loading = false;
    state.data = null;
  },
  [AnimeEpisodeLatestMutations.ANIME_EPISODE_FAILURE](
    state: AnimeEpisodeLatestState,
    error: string,
  ) {
    state.error = error;
    state.data = null;
    state.page = null;
  },
};

const actions: ActionTree<AnimeEpisodeLatestState, RootState> = {
  async getEpisodesLatest(ctx: Context, query: PageQuery) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeEpisodeLatestState, RootState, AnimeEpisodeLatest[]>(
      ctx,
      Adapter.getAnimeEpisodeLatest(query),
      AnimeEpisodeLatestMutations.ANIME_EPISODE_PENDING,
      AnimeEpisodeLatestMutations.ANIME_EPISODE_FAILURE,
      AnimeEpisodeLatestMutations.ANIME_EPISODE_SUCCESS,
    );
  },
};

const module: Module<AnimeEpisodeLatestState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default module;
