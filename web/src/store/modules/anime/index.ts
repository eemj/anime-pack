import { Module, GetterTree, ModuleTree } from 'vuex';
import { AnimeState } from '@/shared/types/state/anime';
import { RootState } from '@/shared/types/state';
import { AnimeEpisodeProgress } from '@/shared/types/episode/progress';

import details from '@/store/modules/anime/details';
import episodes from '@/store/modules/anime/episodes';
import episodeLatest from '@/store/modules/anime/episodelatest';
import activity from '@/store/modules/anime/activity';
import query from '@/store/modules/anime/query';
import favourite from '@/store/modules/anime/favourite';
import indexes from '@/store/modules/anime/indexes';
import search from '@/store/modules/anime/search';
import preferences from '@/store/modules/anime/preferences';
import latest from '@/store/modules/anime/latest';
import airing from '@/store/modules/anime/airing';
import simulcast from '@/store/modules/anime/simulcast';

const getters: GetterTree<AnimeState, RootState> = {
  hasProgress(state: AnimeState): boolean {
    if (
      state.details.data
      && state.episodes.data
      && Object.keys(state.activity.tree).length > 0
      && state.details.data.title in state.activity.tree
    ) {
      const { title } = state.details.data;

      return state.episodes.data.some((episode) => {
        const data = state.activity.tree[title];

        return Object.keys(data.rg)
          .filter((g) => data.rg[g] && episode.name in data.rg[g])
          .some((g) => {
            const qualities = data.rg[g][episode.name];

            return Object.keys(qualities)
              .map(Number)
              .some((height: number) => {
                const progression = qualities[height];

                return (progression?.pct || -1) > 0;
              });
          });
      });
    }

    return false;
  },

  episodeProgress(state: AnimeState): AnimeEpisodeProgress[][] {
    if (
      state.details.data
      && state.episodes.data
      && Object.keys(state.activity.tree).length > 0
      && state.details.data.title in state.activity.tree
    ) {
      const { title } = state.details.data;

      return state.episodes.data.map((episode) => {
        const data = state.activity.tree[title];
        const rgs: AnimeEpisodeProgress[] = [];

        Object.keys(data.rg)
          .filter((g) => data.rg[g] && episode.name in data.rg[g])
          .forEach((g) => {
            const qualities = data.rg[g][episode.name];

            rgs.push(
              ...Object.keys(qualities)
                .map(Number)
                .map(
                  (height: number): AnimeEpisodeProgress => ({
                    rg: g,
                    h: height,
                    pg: qualities[height],
                  }),
                ),
            );
          });

        return rgs;
      });
    }

    return new Array(state.episodes.data?.length || 0);
  },
};

const modules: ModuleTree<RootState> = {
  details,
  episodes,
  episodeLatest,
  activity,
  query,
  favourite,
  indexes,
  search,
  preferences,
  latest,
  airing,
  simulcast,
};

const stateModule: Module<AnimeState, RootState> = {
  getters,
  modules,
};

export default stateModule;
