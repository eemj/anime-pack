import {
  MutationTree, ActionTree, Module, ActionContext,
} from 'vuex';
import {
  AnimeSearchState,
  AnimeSearchMutations,
} from '@/shared/types/state/anime/search';
import type { Anime } from '@/shared/types/anime';
import type { RootState } from '@/shared/types/state';
import type { PaginatedData } from '@/shared/types/api';
import type { AnimeFilterQuery } from '@/adapter/query';
import { context } from '@/adapter/request';
import adapter, { DEFAULT_PAGE_QUERY } from '@/adapter';

type Context = ActionContext<AnimeSearchState, RootState>;

const defaultState: AnimeSearchState = {
  loading: false,
  error: null,
  data: null,
  query: { ...DEFAULT_PAGE_QUERY },
};

const mutations: MutationTree<AnimeSearchState> = {
  [AnimeSearchMutations.SEARCH_TITLE_UPDATE](state: AnimeSearchState, title: string) {
    state.query = Object.freeze({ ...state.query, title });
  },

  [AnimeSearchMutations.SEARCH_CLEAR](state: AnimeSearchState) {
    state.data = null;
    state.error = null;
    state.query = { ...DEFAULT_PAGE_QUERY };
    state.loading = false;
  },
  [AnimeSearchMutations.SEARCH_PENDING](state: AnimeSearchState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [AnimeSearchMutations.SEARCH_FAILURE](
    state: AnimeSearchState,
    error: string,
  ) {
    state.error = error;
    state.loading = false;
    state.data = null;
  },
  [AnimeSearchMutations.SEARCH_SUCCESS](
    state: AnimeSearchState,
    data: PaginatedData<Anime[]>,
  ) {
    state.data = Object.freeze(data);
    state.error = null;
    state.loading = false;
  },
};

const actions: ActionTree<AnimeSearchState, RootState> = {
  setSearchPhrase(ctx: Context, phrase: string) {
    ctx.commit(AnimeSearchMutations.SEARCH_TITLE_UPDATE, phrase);
  },

  async quickSearchAnime(ctx: Context) {
    if (ctx.state.loading && ctx.state.query?.title) {
      return;
    }

    const title = (ctx.state.query?.title || '').trim();

    if (!title.length) {
      return;
    }

    await context<AnimeSearchState, RootState, Anime[]>(
      ctx,
      adapter.getAnime({
        page: 1,
        elements: 20,
        title,
      }),
      AnimeSearchMutations.SEARCH_PENDING,
      AnimeSearchMutations.SEARCH_FAILURE,
      AnimeSearchMutations.SEARCH_SUCCESS,
    );
  },

  async searchAnime(ctx: Context, query: AnimeFilterQuery) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeSearchState, RootState, Anime[]>(
      ctx,
      adapter.getAnime(query),
      AnimeSearchMutations.SEARCH_PENDING,
      AnimeSearchMutations.SEARCH_FAILURE,
      AnimeSearchMutations.SEARCH_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeSearchState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
