import {
  AnimeFavouriteState,
  AnimeFavouriteMutations,
} from '@/shared/types/state/anime/favourite';
import {
  MutationTree, Module, ActionTree, ActionContext,
} from 'vuex';
import { Anime } from '@/shared/types/anime';
import { APIResponse } from '@/shared/types/api';
import { RootState } from '@/shared/types/state';
import { context } from '@/adapter/request';
import adapter from '@/adapter';
import { PageQuery } from '@/adapter/query';

type Context = ActionContext<AnimeFavouriteState, RootState>;

const defaultState: AnimeFavouriteState = {
  loading: false,
  error: null,
  data: null,
  page: null,
};

const mutations: MutationTree<AnimeFavouriteState> = {
  [AnimeFavouriteMutations.FAVOURITE_PENDING](state: AnimeFavouriteState) {
    state.loading = false;
    state.error = null;
    state.page = null;
    state.data = null;
  },
  [AnimeFavouriteMutations.FAVOURITE_SUCCESS](
    state: AnimeFavouriteState,
    { data, page }: APIResponse<Anime[]>,
  ) {
    state.loading = false;
    state.error = null;

    if (data && page) {
      state.data = data;
      state.page = page;
    }
  },
  [AnimeFavouriteMutations.FAVOURITE_FAILURE](
    state: AnimeFavouriteState,
    error: string,
  ) {
    state.loading = false;
    state.data = null;
    state.page = null;
    state.error = error;
  },
};

const actions: ActionTree<AnimeFavouriteState, RootState> = {
  async getFavourite(ctx: Context, query: PageQuery) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeFavouriteState, RootState, Anime[]>(
      ctx,
      adapter.getAnime({ ...query, favourite: true }),
      AnimeFavouriteMutations.FAVOURITE_PENDING,
      AnimeFavouriteMutations.FAVOURITE_FAILURE,
      AnimeFavouriteMutations.FAVOURITE_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeFavouriteState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
