import {
  type ActionContext,
  type ActionTree,
  type GetterTree,
  type Module,
  type MutationTree,
} from 'vuex';
import adapter, { MAX_ELEMENTS } from '@/adapter';
import extensions from '@/shared/helpers/extensions';
import { AnimeActivity, AnimeActivityEpisode } from '@/shared/types/activity';
import { Anime } from '@/shared/types/anime';
import { DifferenceToken } from '@/shared/types/protocol';
import { RootState } from '@/shared/types/state';
import {
  type AnimeActivityData,
  type AnimeActivityDifferenceData,
  type AnimeActivityState,
  AnimeActivityMutations,
} from '@/shared/types/state/anime/activity';
import { APIResponse } from '@/shared/types/api';
import { RouteName } from '@/router';
import { AnimeOrderByQuery, OrderByDirection } from '@/adapter/query';

type Context = ActionContext<AnimeActivityState, RootState>;

const defaultState: AnimeActivityState = {
  tree: {},
  details: {
    loading: false,
    error: null,
    data: {},
  },
};

const getters: GetterTree<AnimeActivityState, RootState> = {
  activityList(state: AnimeActivityState): AnimeActivity[] {
    return Object.keys(state.tree)
      .filter((title) => state.tree[title].d in state.details.data)
      .map((title): AnimeActivity => {
        const episodes: AnimeActivityEpisode[] = [];

        Object.keys(state.tree[title].rg).forEach((group) => {
          Object.keys(state.tree[title].rg[group]).forEach((episode) => {
            Object.keys(state.tree[title].rg[group][episode]).forEach(
              (quality) => {
                const height = Number(quality);
                episodes.push({
                  group,
                  height,
                  name: episode,
                  progress: state.tree[title].rg[group][episode][height],
                });
              },
            );
          });
        });

        episodes.sort((a, b) => {
          if (a.progress && b.progress) {
            if (a.progress.pct > b.progress.pct) {
              return -1;
            }

            if (a.progress.pct < b.progress.pct) {
              return 1;
            }

            return 0;
          }

          if (a.progress && !b.progress) {
            return -1;
          }

          return 1;
        });

        const summary = {
          inProgress: episodes.filter((e) => e.progress)?.length ?? 0,
          totalPercentage: episodes
            .map(({ progress }) => progress?.pct ?? 0)
            .reduce((previous, next) => previous + next, 0),
        };

        return {
          title,
          episodes,
          summary,
          id: state.tree[title].d,
        };
      })
      .sort((a, b) => b.summary.totalPercentage - a.summary.totalPercentage);
  },
};

const actions: ActionTree<AnimeActivityState, RootState> = {
  async getActivityProgress(ctx: Context) {
    ctx.commit(AnimeActivityMutations.ACTIVITY_FETCH_PROGRESS);
  },

  async cacheActivityDetails(ctx: Context, ids: number[]) {
    ctx.commit(AnimeActivityMutations.ACTIVITY_DETAILS_PENDING);

    const futures = await Promise.allSettled(
      extensions
        .chunk<number>(ids, MAX_ELEMENTS)
        .map((idsChunk) => adapter.getAnime({
          ids: idsChunk,
          elements: idsChunk.length < MAX_ELEMENTS ? idsChunk.length : MAX_ELEMENTS,
          page: 1,
          orderBy: AnimeOrderByQuery.Relevance,
          direction: OrderByDirection.Ascending,
        })),
    );

    const errorEncountered = futures.find(
      ({ status, ...future }) =>
        status === 'rejected'
        || (future as PromiseFulfilledResult<APIResponse<Anime[]>>).value.error,
    );

    if (errorEncountered) {
      ctx.commit(
        AnimeActivityMutations.ACTIVITY_DETAILS_FAILURE,
        (errorEncountered as PromiseFulfilledResult<APIResponse<Anime[]>>)
          ?.value?.error || 'something_went_wrong',
      );
    } else {
      const data = futures
        .filter(({ status }) => status !== 'rejected')
        .flatMap((future) =>
          (
            future as PromiseFulfilledResult<APIResponse<Anime[]>>
          ).value.data?.map((anime) => ({
            id: anime.id,
            poster: anime.poster,
            title: anime.title,
          })));

      ctx.commit(
        AnimeActivityMutations.ACTIVITY_DETAILS_SUCCESS,
        // Only store the required information
        data,
      );
    }
  },

  async fetchActivityDetails(ctx: Context) {
    const ids = Object.keys(ctx.state.tree)
      .map((title) => ctx.state.tree[title].d)
      .filter((id) => !(id in ctx.state.details.data));

    if (
      ctx.rootState.anime.details?.data?.id
      && !(ctx.rootState.anime.details.data.id in ctx.state.details.data)) {
      const stateIndex = ids.findIndex((id) => id === ctx.rootState.anime.details?.data?.id);

      if (stateIndex >= 0) {
        ctx.commit(
          AnimeActivityMutations.ACTIVITY_DETAILS_SUCCESS,
          {
            id: ctx.rootState.anime.details.data.id,
            title: ctx.rootState.anime.details.data.title,
            poster: ctx.rootState.anime.details.data.poster,
          },
        );

        ids.splice(stateIndex, 1);
      }

      await ctx.dispatch('cacheActivityDetails', ids);
    }
  },

  async updateActivityData(ctx: Context, data: AnimeActivityData) {
    if (ctx.rootState.route.name !== RouteName.ACTIVITY && data !== null) {
      const ids: number[] = [];

      Object.keys(data)
        .map((title) => Number(data[title].d))
        .filter((v, i, a) => a.indexOf(v) === i)
        .forEach((id: number) => {
          if (!(id in ctx.state.details.data)) {
            // If we have the details already in our state, use that instead of
            // creating a new request.
            if (
              ctx.rootState.anime.details?.data?.id
              && ctx.rootState.anime.details?.data?.id === id
            ) {
              ctx.commit(AnimeActivityMutations.ACTIVITY_DETAILS_SUCCESS, [
                {
                  id: ctx.rootState.anime.details.data.id,
                  title: ctx.rootState.anime.details.data.title,
                  poster: ctx.rootState.anime.details.data.poster,
                },
              ]);
              return;
            }

            ids.push(id);
          }
        });

      if (ids.length > 0) {
        await ctx.dispatch('cacheActivityDetails', ids);
      }
    }

    ctx.commit(AnimeActivityMutations.ACTIVITY_DATA_UPDATE, data);
  },

  async applyActivityDifference(
    ctx: Context,
    data: AnimeActivityDifferenceData,
  ) {
    const ids = [...new Set(data.map(({ an: { d } }) => d)).values()].filter(
      (d) => !(d in ctx.state.details.data),
    );

    if (ids.length > 0) {
      await ctx.dispatch('cacheActivityDetails', ids);
    }

    ctx.commit(AnimeActivityMutations.ACTIVITY_DIFFERENCE_UPDATE, data);
  },
};

const mutations: MutationTree<AnimeActivityState> = {
  [AnimeActivityMutations.ACTIVITY_DIFFERENCE_UPDATE](
    state: AnimeActivityState,
    payload: AnimeActivityDifferenceData,
  ) {
    payload.forEach(({ tkn, an, pg }) => {
      switch (tkn) {
        case DifferenceToken.ADD:
        case DifferenceToken.MODIFY:
          if (!(an.t in state.tree)) {
            state.tree[an.t] = {
              d: an.d,
              rg: { [an.rg]: { [an.e]: { [an.h]: pg } } },
            };
          } else if (!(an.rg in state.tree[an.t].rg)) {
            state.tree[an.t].rg[an.rg] = { [an.e]: { [an.h]: pg } };
          } else if (!(an.e in state.tree[an.t].rg[an.rg])) {
            state.tree[an.t].rg[an.rg][an.e] = { [an.h]: pg };
          } else {
            state.tree[an.t].rg[an.rg][an.e][an.h] = pg;
          }
          break;
        case DifferenceToken.REMOVE:
          {
            const element = state.tree?.[an.t]?.rg?.[an.rg]?.[an.e]?.[an.h];

            if (element === undefined) break;

            // - Title -> Release Groups -> Episode -> Quality = Progress

            // Delete the quality
            delete state.tree[an.t].rg[an.rg][an.e][an.h];

            // Do we have any qualities in our episodes? Delete
            if (Object.keys(state.tree[an.t].rg[an.rg][an.e]).length <= 1) {
              delete state.tree[an.t].rg[an.rg][an.e];
            }

            // Do we have any episodes in our release group? Delete
            if (Object.keys(state.tree[an.t].rg[an.rg]).length <= 1) {
              delete state.tree[an.t].rg[an.rg];
            }

            // Do we have any release groups in our anime? Delete
            if (Object.keys(state.tree[an.t].rg).length === 0) {
              delete state.tree[an.t];

              // Remove it from the details data.
              if (an.d in state.details.data) {
                delete state.details.data[an.d];
              }
            }
          }
          break;
        default:
          // eslint-disable-next-line no-console
          console.warn(`activity: unexpected token '${tkn}'`);
          break;
      }
    });
  },

  [AnimeActivityMutations.ACTIVITY_DATA_UPDATE](
    state: AnimeActivityState,
    payload: AnimeActivityData,
  ) {
    const ids = Object.keys(payload).map((title) => Number(payload[title].d));

    ids.forEach((id: number) => {
      if (!(id in state.details.data)) {
        state.details.data[id] = null;
      }
    });

    Object.keys(state.details.data)
      .map(Number)
      .forEach((id: number) => {
        if (!ids.includes(id)) {
          delete ids[id];
        }
      });

    state.tree = payload;
  },

  [AnimeActivityMutations.ACTIVITY_FETCH_PROGRESS]() {
    /* Websocket Event Trigger */
  },

  [AnimeActivityMutations.ACTIVITY_DETAILS_PENDING](state: AnimeActivityState) {
    state.details.loading = true;
    state.details.error = null;
  },
  [AnimeActivityMutations.ACTIVITY_DETAILS_SUCCESS](
    state: AnimeActivityState,
    animes: Anime[],
  ) {
    state.details.loading = false;
    state.details.error = null;

    animes.forEach((anime) => {
      if (anime.id) {
        state.details.data[anime.id] = anime;
      }
    });
  },
  [AnimeActivityMutations.ACTIVITY_DETAILS_FAILURE](
    state: AnimeActivityState,
    error: string,
  ) {
    state.details.error = error;
    state.details.loading = false;
  },
};

const stateModule: Module<AnimeActivityState, RootState> = {
  state: defaultState,
  getters,
  actions,
  mutations,
};

export default stateModule;
