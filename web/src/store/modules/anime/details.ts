import {
  AnimeDetailsState,
  AnimeDetailsMutations,
} from '@/shared/types/state/anime/details';
import {
  ActionTree, ActionContext, MutationTree, Module,
} from 'vuex';
import { Anime } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import adapter from '@/adapter';
import { context } from '@/adapter/request';

type Context = ActionContext<AnimeDetailsState, RootState>;

const defaultState: AnimeDetailsState = {
  loading: false,
  error: null,
  data: null,
};

const actions: ActionTree<AnimeDetailsState, RootState> = {
  async getDetails(ctx: Context, id: number) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeDetailsState, RootState, Anime>(
      ctx,
      adapter.getAnimeId(id),
      AnimeDetailsMutations.DETAILS_PENDING,
      AnimeDetailsMutations.DETAILS_FAILURE,
      AnimeDetailsMutations.DETAILS_SUCCESS,
    );
  },
};

const mutations: MutationTree<AnimeDetailsState> = {
  [AnimeDetailsMutations.DETAILS_PENDING](state: AnimeDetailsState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [AnimeDetailsMutations.DETAILS_SUCCESS](
    state: AnimeDetailsState,
    data: Anime,
  ) {
    state.loading = false;
    state.error = null;
    state.data = Object.freeze(data);
  },
  [AnimeDetailsMutations.DETAILS_FAILURE](
    state: AnimeDetailsState,
    error: string,
  ) {
    state.data = null;
    state.loading = false;
    state.error = error;
  },
};

const stateModule: Module<AnimeDetailsState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default stateModule;
