/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  ActionTree, MutationTree, Module, ActionContext,
} from 'vuex';
import {
  AnimeEpisodeState,
  AnimeEpisodesMutations,
} from '@/shared/types/state/anime/episodes';
import { AnimeEpisode } from '@/shared/types/episode';
import { RootState } from '@/shared/types/state';
import { DataExceptionMetadata } from '@/shared/types/protocol';
import { context } from '@/adapter/request';
import adapter from '@/adapter';

type Context = ActionContext<AnimeEpisodeState, RootState>;

const defaultState: AnimeEpisodeState = {
  loading: false,
  error: null,
  data: null,
};

const actions: ActionTree<AnimeEpisodeState, RootState> = {
  async getEpisodes(ctx: Context, id: number) {
    if (ctx.state.loading) return;

    await context<AnimeEpisodeState, RootState, AnimeEpisode[]>(
      ctx,
      adapter.getAnimeIdEpisode(id),
      AnimeEpisodesMutations.EPISODES_PENDING,
      AnimeEpisodesMutations.EPISODES_FAILURE,
      AnimeEpisodesMutations.EPISODES_SUCCESS,
    );
  },

  episodeMetadata(
    { rootState, commit }: Context,
    payload: DataExceptionMetadata,
  ) {
    if (rootState.anime.details.data?.title === payload.t) {
      commit(AnimeEpisodesMutations.EPISODE_UPDATE_METADATA, payload);
    }
  },
};

const mutations: MutationTree<AnimeEpisodeState> = {
  [AnimeEpisodesMutations.EPISODES_PENDING](state: AnimeEpisodeState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [AnimeEpisodesMutations.EPISODES_SUCCESS](
    state: AnimeEpisodeState,
    data: AnimeEpisode[],
  ) {
    state.data = data;
    state.loading = false;
    state.error = null;
  },
  [AnimeEpisodesMutations.EPISODES_FAILURE](
    state: AnimeEpisodeState,
    error: string,
  ) {
    state.data = null;
    state.loading = false;
    state.error = error;
  },
  [AnimeEpisodesMutations.EPISODE_UPDATE_METADATA](
    state: AnimeEpisodeState,
    payload: DataExceptionMetadata,
  ) {
    const episode = state.data?.find(({ name }) => name === payload.e);
    const group = episode?.releaseGroups.find(
      ({ name }) => name === payload.rg,
    );
    const quality = group?.qualities.find(({ height }) =>
      (payload.h > 0 ? height === payload.h : height === undefined));

    if (quality) {
      quality.thumbnail = {
        id: 0,
        name: payload.th,
      };
    }
  },
};

const stateModule: Module<AnimeEpisodeState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default stateModule;
