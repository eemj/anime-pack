import {
  MutationTree, ActionTree, Module, ActionContext,
} from 'vuex';
import {
  AnimeAiringState,
  AnimeAiringMutations,
} from '@/shared/types/state/anime/airing';
import { Anime, AnimeStatus } from '@/shared/types/anime';
import { APIResponse } from '@/shared/types/api';
import { RootState } from '@/shared/types/state';
import adapter, { DEFAULT_ELEMENTS } from '@/adapter';
import { context } from '@/adapter/request';
import { AnimeOrderByQuery, OrderByDirection, PageQuery } from '@/adapter/query';

type Context = ActionContext<AnimeAiringState, RootState>;

const defaultState: AnimeAiringState = {
  loading: false,
  error: null,
  data: null,
  page: null,
};

const mutations: MutationTree<AnimeAiringState> = {
  [AnimeAiringMutations.AIRING_PENDING](state: AnimeAiringState) {
    state.loading = true;
    state.error = null;
    state.page = null;
    state.data = null;
  },
  [AnimeAiringMutations.AIRING_SUCCESS](
    state: AnimeAiringState,
    { page, data }: APIResponse<Anime[]>,
  ) {
    state.loading = false;
    state.error = null;

    if (data && page) {
      state.data = data;
      state.page = page;
    }
  },
  [AnimeAiringMutations.AIRING_FAILURE](
    state: AnimeAiringState,
    error: string,
  ) {
    state.error = error;
    state.loading = false;
    state.data = null;
    state.page = null;
  },
};

const actions: ActionTree<AnimeAiringState, RootState> = {
  async getAiring(ctx: Context, query: PageQuery) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeAiringState, RootState, Anime[]>(
      ctx,
      adapter.getAnime({
        ...query,
        statuses: [AnimeStatus.RELEASING],
        elements: DEFAULT_ELEMENTS,
        orderBy: AnimeOrderByQuery.NextAiringDate,
        direction: OrderByDirection.Ascending,
      }),
      AnimeAiringMutations.AIRING_PENDING,
      AnimeAiringMutations.AIRING_FAILURE,
      AnimeAiringMutations.AIRING_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeAiringState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
