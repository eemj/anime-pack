import {
  MutationTree, ActionTree, Module, ActionContext,
} from 'vuex';
import {
  AnimeQueryState,
  AnimeQueryMutations,
} from '@/shared/types/state/anime/query';
import { Anime } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import adapter from '@/adapter';
import { ThirdPartyQuery } from '@/adapter/query';
import { context } from '@/adapter/request';

type Context = ActionContext<AnimeQueryState, RootState>;

const defaultState: AnimeQueryState = {
  loading: false,
  error: null,
  data: null,
};

const mutations: MutationTree<AnimeQueryState> = {
  [AnimeQueryMutations.QUERY_PENDING](state: AnimeQueryState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [AnimeQueryMutations.QUERY_SUCCESS](state: AnimeQueryState, data: Anime) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [AnimeQueryMutations.QUERY_FAILURE](state: AnimeQueryState, error: string) {
    state.data = null;
    state.loading = false;
    state.error = error;
  },
};

const actions: ActionTree<AnimeQueryState, RootState> = {
  resetQuery(ctx: Context) {
    ctx.commit(AnimeQueryMutations.QUERY_SUCCESS, null);
  },

  async getQuery(ctx: Context, query: ThirdPartyQuery) {
    if (
      ctx.state.loading
      || (query.idAniList && ctx.state.data?.id === query.idAniList)
      || (query.idMAL && ctx.state.data?.id === query.idMAL)
    ) {
      return;
    }

    await context<AnimeQueryState, RootState, Anime>(
      ctx,
      adapter.getAnimeThirdParty(query),
      AnimeQueryMutations.QUERY_PENDING,
      AnimeQueryMutations.QUERY_FAILURE,
      AnimeQueryMutations.QUERY_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeQueryState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
