import { context } from '@/adapter/request';
import { AnimeSimulcast } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { AnimeSimulcastMutations, AnimeSimulcastState } from '@/shared/types/state/anime/simulcast';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';
import adapter from '@/adapter';

type Context = ActionContext<AnimeSimulcastState, RootState>;

const defaultState: AnimeSimulcastState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<AnimeSimulcastState> = {
  [AnimeSimulcastMutations.SIMULCAST_PENDING](state: AnimeSimulcastState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [AnimeSimulcastMutations.SIMULCAST_SUCCESS](state: AnimeSimulcastState, data: AnimeSimulcast[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [AnimeSimulcastMutations.SIMULCAST_FAILURE](state: AnimeSimulcastState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<AnimeSimulcastState, RootState> = {
  async getSimulcast(ctx: Context) {
    if (ctx.state.loading) {
      return;
    }

    await context<AnimeSimulcastState, RootState, AnimeSimulcast[]>(
      ctx,
      adapter.getAnimeSimulcast(),
      AnimeSimulcastMutations.SIMULCAST_PENDING,
      AnimeSimulcastMutations.SIMULCAST_FAILURE,
      AnimeSimulcastMutations.SIMULCAST_SUCCESS,
    );
  },
};

const stateModule: Module<AnimeSimulcastState, RootState> = {
  state: defaultState,
  actions,
  mutations,
};

export default stateModule;
