import {
  MutationTree, ActionTree, Module, ActionContext,
} from 'vuex';
import {
  AnimePreferencesState,
  AnimePreferencesMutations,
} from '@/shared/types/state/anime/preferences';
import { Preferences } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { context } from '@/adapter/request';
import adapter from '@/adapter';
import { PreferenceUpdateBody } from '@/adapter/query';

export interface UpdatePreferencesPayload {
  favourite?: boolean;
  automaticDownloads?: boolean;
  performChecksum?: boolean;
}

type Context = ActionContext<AnimePreferencesState, RootState>;

const defaultState: AnimePreferencesState = {
  details: {
    loading: false,
    error: null,
    data: null,
  },
  update: {
    loading: false,
    error: null,
  },
};

const mutations: MutationTree<AnimePreferencesState> = {
  [AnimePreferencesMutations.PREFERENCE_PENDING](state: AnimePreferencesState) {
    state.details.loading = true;
    state.details.data = null;
    state.details.error = null;
  },
  [AnimePreferencesMutations.PREFERENCE_SUCCESS](
    state: AnimePreferencesState,
    data: Preferences,
  ) {
    state.details.loading = false;
    state.details.data = data;
    state.details.error = null;
  },
  [AnimePreferencesMutations.PREFERENCE_FAILURE](
    state: AnimePreferencesState,
    error: string,
  ) {
    state.details.loading = false;
    state.details.data = null;
    state.details.error = error;
  },

  [AnimePreferencesMutations.UPDATE_PREFERENCE_PENDING](
    state: AnimePreferencesState,
  ) {
    state.update.loading = true;
    state.update.error = null;
  },
  [AnimePreferencesMutations.UPDATE_PREFERENCE_SUCCESS](
    state: AnimePreferencesState,
    payload: PreferenceUpdateBody,
  ) {
    state.update.loading = false;
    state.update.error = null;

    if (state.details.data) {
      const { automaticDownloads, performChecksum, favourite } = state.details.data;

      state.details.data = {
        automaticDownloads: payload.automaticDownloads ?? automaticDownloads,
        favourite: payload.favourite ?? favourite,
        performChecksum: payload.performChecksum ?? performChecksum,
      };
    }
  },
  [AnimePreferencesMutations.UPDATE_PREFERENCE_FAILURE](
    state: AnimePreferencesState,
    error: string,
  ) {
    state.update.loading = false;
    state.update.error = error;
  },
};

const actions: ActionTree<AnimePreferencesState, RootState> = {
  async getPreferences(ctx: Context, id: number) {
    if (ctx.state.details.loading) {
      return;
    }

    await context<AnimePreferencesState, RootState, Preferences>(
      ctx,
      adapter.getAnimeIdPreferences(id),
      AnimePreferencesMutations.PREFERENCE_PENDING,
      AnimePreferencesMutations.PREFERENCE_FAILURE,
      AnimePreferencesMutations.PREFERENCE_SUCCESS,
    );
  },

  async updatePreferences(ctx: Context, payload: PreferenceUpdateBody) {
    if (
      (!ctx.rootState.anime?.details?.loading
        && ctx.rootState.anime?.details?.data
        && ctx.state.details.loading)
      || ctx.state.update.loading
    ) {
      return;
    }

    if (ctx.rootState.anime.details?.data?.id) {
      const id = ctx.rootState.anime.details?.data?.id;

      ctx.commit(AnimePreferencesMutations.UPDATE_PREFERENCE_PENDING);

      try {
        const response = await adapter.patchAnimeIdPreferences(id, payload);

        if (response.error) {
          ctx.commit(AnimePreferencesMutations.UPDATE_PREFERENCE_FAILURE, response.error);
        } else if (response.data) {
          ctx.commit(AnimePreferencesMutations.UPDATE_PREFERENCE_SUCCESS, payload);
        }
      } catch (err) {
        ctx.commit(
          AnimePreferencesMutations.UPDATE_PREFERENCE_FAILURE,
          (err instanceof Error) ? err.message : err,
        );
      }
    }
  },
};

const stateModule: Module<AnimePreferencesState, RootState> = {
  state: defaultState,
  mutations,
  actions,
};

export default stateModule;
