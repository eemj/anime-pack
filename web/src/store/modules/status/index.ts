import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { AnimeStatus } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { StatusMutations, StatusState } from '@/shared/types/state/status';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

const key = '_statuses';

const defaultState: StatusState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<StatusState> = {
  [StatusMutations.STATUS_PENDING](state: StatusState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [StatusMutations.STATUS_SUCCESS](state: StatusState, data: AnimeStatus[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [StatusMutations.STATUS_FAILURE](state: StatusState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<StatusState, RootState> = {
  async getStatuses(ctx: ActionContext<StatusState, RootState>) {
    let statuses = session.first<AnimeStatus[]>(key);

    if (statuses === null || statuses.expiresIn < new Date()) {
      ctx.commit(StatusMutations.STATUS_PENDING);

      const response = await Adapter.getStatus();

      if (response.data) {
        const value = response.data;

        ctx.commit(StatusMutations.STATUS_SUCCESS, value);

        if (statuses == null) {
          statuses = { value, expiresIn: new Date() };
        }

        session.store<AnimeStatus[]>(key, statuses.value);
      } else {
        ctx.commit(StatusMutations.STATUS_FAILURE, response.error);
      }
    } else {
      ctx.commit(StatusMutations.STATUS_SUCCESS, statuses.value);
    }

    return statuses?.value;
  },
};

const module: Module<StatusState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
