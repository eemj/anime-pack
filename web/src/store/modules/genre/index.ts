import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { Genre } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { GenreMutations, GenreState } from '@/shared/types/state/genre';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

const key = '_genres';

const defaultState: GenreState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<GenreState> = {
  [GenreMutations.GENRE_PENDING](state: GenreState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [GenreMutations.GENRE_SUCCESS](state: GenreState, data: Genre[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [GenreMutations.GENRE_FAILURE](state: GenreState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<GenreState, RootState> = {
  async getGenres(ctx: ActionContext<GenreState, RootState>) {
    let genres = session.first<Genre[]>(key);

    if (genres === null || genres.expiresIn < new Date()) {
      ctx.commit(GenreMutations.GENRE_PENDING);

      const response = await Adapter.getGenre();

      if (response.data) {
        ctx.commit(GenreMutations.GENRE_SUCCESS, response.data);

        if (genres == null) {
          genres = { value: response.data, expiresIn: new Date() };
        }

        session.store<Genre[]>(key, genres.value);
      } else {
        ctx.commit(GenreMutations.GENRE_FAILURE, response.error);
      }
    } else {
      ctx.commit(GenreMutations.GENRE_SUCCESS, genres.value);
    }

    return genres?.value;
  },
};

const module: Module<GenreState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
