import {
  ActionContext, MutationTree, ActionTree, Payload,
} from 'vuex';
import { RootState } from '@/shared/types/state';
import { HealthState, HealthMutations } from '@/shared/types/state/health';

type Context = ActionContext<HealthState, RootState>;

const mutations: MutationTree<HealthState> = {
  [HealthMutations.WEBSOCKET_CONNECTED](state: HealthState) {
    state.websocket = true;
  },
  [HealthMutations.WEBSOCKET_DISCONNECTED](state: HealthState) {
    state.websocket = false;
  },
  [HealthMutations.IRC_CONNECTED](state: HealthState) {
    state.irc = true;
  },
  [HealthMutations.IRC_DISCONNECTED](state: HealthState) {
    state.irc = false;
  },
  [HealthMutations.WEBSOCKET_SEND]() {
    // empty.
  },
};

const actions: ActionTree<HealthState, RootState> = {
  sendSocket(ctx: Context, payload: Payload) {
    ctx.commit(HealthMutations.WEBSOCKET_SEND, payload);
  },
};

const defaultState: HealthState = {
  websocket: false,
  irc: false,
};

export default {
  state: defaultState,
  mutations,
  actions,
};
