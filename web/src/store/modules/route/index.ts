import { MutationTree } from 'vuex';
import { RouteState, RouteMutations } from '@/shared/types/state/route';

const defaultState: RouteState = {
  name: 'home',
};

const mutations: MutationTree<RouteState> = {
  [RouteMutations.ROUTE_UPDATE](state: RouteState, name: string) {
    state.name = name;
  },
};

export default {
  mutations,
  state: defaultState,
};
