import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { Studio } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { StudioMutations, StudioState } from '@/shared/types/state/studio';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

const key = '_studios';

const defaultState: StudioState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<StudioState> = {
  [StudioMutations.STUDIO_PENDING](state: StudioState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [StudioMutations.STUDIO_SUCCESS](state: StudioState, data: Studio[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [StudioMutations.STUDIO_FAILURE](state: StudioState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<StudioState, RootState> = {
  async getStudios(ctx: ActionContext<StudioState, RootState>) {
    let studios = session.first<Studio[]>(key);

    if (studios === null || studios.expiresIn < new Date()) {
      ctx.commit(StudioMutations.STUDIO_PENDING);

      const response = await Adapter.getStudio();

      if (response.data) {
        ctx.commit(StudioMutations.STUDIO_SUCCESS, response.data);

        if (studios == null) {
          studios = { value: response.data, expiresIn: new Date() };
        }

        session.store<Studio[]>(key, studios.value);
      } else {
        ctx.commit(StudioMutations.STUDIO_FAILURE, response.error);
      }
    } else {
      ctx.commit(StudioMutations.STUDIO_SUCCESS, studios.value);
    }

    return studios?.value;
  },
};

const module: Module<StudioState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
