import { ActionContext, ActionTree, MutationTree } from 'vuex';
import { Job, JobState, JobStates } from '@/shared/types/jobs';
import { RootState } from '@/shared/types/state';
import { JobActionMutation, JobsMutation, JobsState } from '@/shared/types/state/jobs';
import adapter from '@/adapter';
import { context } from '@/adapter/request';
import { DataStatusJob } from '@/shared/types/protocol';

type Context = ActionContext<JobsState, RootState>;

interface JobTargetAction {
  jobId: string;
  error?: string;
}

const mutations: MutationTree<JobsState> = {
  [JobsMutation.JOBS_PENDING](state: JobsState) {
    state.loading = true;
    state.error = null;
  },
  [JobsMutation.JOBS_FAILURE](state: JobsState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = [];
  },
  [JobsMutation.JOBS_SUCCESS](state: JobsState, jobs: Job[]) {
    state.loading = false;
    state.error = null;
    state.data = jobs;

    jobs.forEach((job) => {
      state.actions[job.id] = {
        error: null,
        loading: false,
      };
    });
  },
  [JobsMutation.JOBS_UPDATE_STATUS](state: JobsState, status: DataStatusJob) {
    if (status.d in state.actions) {
      const jobIndex = state.data?.findIndex((job) => job.id === status.d);

      if (state.data && jobIndex && jobIndex >= 0) {
        state.data[jobIndex] = {
          ...state.data[jobIndex],
          lastActivity: status.la,
          startedAt: status.sat,
          endedAt: status.eat,
          period: status.p,
          state: status.s.toString() as JobState,
        };
      }
    }
  },
};

// Create a mutation for every job state that's supported.
JobStates.forEach((jobState: string) => {
  mutations[JobActionMutation.JOB_STATE_FAILURE(jobState as JobState)] = (
    state: JobsState,
    { jobId, error }: JobTargetAction,
  ) => {
    if (!(jobId in state.actions)) {
      return;
    }

    if (error !== undefined) {
      state.actions[jobId].error = error;
    }

    state.actions[jobId].loading = false;
  };
  mutations[JobActionMutation.JOB_STATE_SUCCESS(jobState as JobState)] = (
    state: JobsState,
    jobId: string,
  ) => {
    if (!(jobId in state.actions)) {
      return;
    }

    state.actions[jobId].loading = false;
    state.actions[jobId].error = null;
  };
  mutations[JobActionMutation.JOB_STATE_PENDING(jobState as JobState)] = (
    state: JobsState,
    jobId: string,
  ) => {
    if (!(jobId in state.actions)) {
      return;
    }

    state.actions[jobId].loading = false;
    state.actions[jobId].error = null;
  };
});

const actions: ActionTree<JobsState, RootState> = {
  async getJob(ctx: Context) {
    if (ctx.state.loading) {
      return;
    }

    await context<JobsState, RootState, Job[]>(
      ctx,
      adapter.getJob(),
      JobsMutation.JOBS_PENDING,
      JobsMutation.JOBS_FAILURE,
      JobsMutation.JOBS_SUCCESS,
    );
  },

  async getJobId(ctx: Context, jobId: string) {
    if (ctx.state.loading) {
      return;
    }

    try {
      const response = await adapter.getJobId(jobId);

      if (response.error) {
        ctx.commit(JobsMutation.JOBS_FAILURE, response.error);
      } else if (response.data) {
        // Get the current list of jobs, but remove the one we're retrieving.
        const currentJobs = (ctx.rootState.jobs.data || [])
          .filter((job) => job.id !== jobId);

        // Since the data returned successfully, append the job.
        currentJobs.push(response.data);

        ctx.commit(JobsMutation.JOBS_SUCCESS, currentJobs);
      }
    } catch (err) {
      if (err instanceof Error) {
        ctx.commit(JobsMutation.JOBS_FAILURE, err.message);
      } else {
        ctx.commit(JobsMutation.JOBS_FAILURE, err);
      }
    }
  },

  async startJobId(ctx: Context, jobId: string) {
    if (!(jobId in ctx.state.actions) || ctx.state.actions[jobId].loading) {
      return;
    }

    ctx.commit(JobActionMutation.JOB_STATE_PENDING(JobState.ACTIVE), jobId);

    const response = await adapter.startJob(jobId);

    if (!response.error) {
      ctx.commit(JobActionMutation.JOB_STATE_SUCCESS(JobState.ACTIVE), jobId);
    } else {
      ctx.commit(JobActionMutation.JOB_STATE_FAILURE(JobState.ACTIVE), {
        jobId,
        error: response.error,
      });
    }
  },

  async stopJobId(ctx: Context, jobId: string) {
    if (!(jobId in ctx.state.actions) || ctx.state.actions[jobId].loading) {
      return;
    }

    ctx.commit(JobActionMutation.JOB_STATE_PENDING(JobState.PASSIVE), jobId);

    const response = await adapter.stopJob(jobId);

    if (!response.error) {
      ctx.commit(JobActionMutation.JOB_STATE_SUCCESS(JobState.PASSIVE), jobId);
    } else {
      ctx.commit(JobActionMutation.JOB_STATE_FAILURE(JobState.PASSIVE), {
        jobId,
        error: response.error,
      });
    }
  },

  async disableJobId(ctx: Context, jobId: string) {
    if (!(jobId in ctx.state.actions) || ctx.state.actions[jobId].loading) {
      return;
    }

    ctx.commit(JobActionMutation.JOB_STATE_PENDING(JobState.DISABLE), jobId);

    const response = await adapter.disableJob(jobId);

    if (!response.error) {
      ctx.commit(JobActionMutation.JOB_STATE_SUCCESS(JobState.DISABLE), jobId);
    } else {
      ctx.commit(JobActionMutation.JOB_STATE_FAILURE(JobState.DISABLE), {
        jobId,
        error: response.error,
      });
    }
  },

  updateJobStatus(ctx: Context, status: DataStatusJob) {
    ctx.commit(JobsMutation.JOBS_UPDATE_STATUS, status);
  },
};

const defaultState = {
  loading: false,
  data: null,
  error: null,
  actions: {},
};

const module = {
  mutations,
  actions,
  state: defaultState,
};

export default module;
