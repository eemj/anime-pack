import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { RootState } from '@/shared/types/state';
import { SeasonYearMutations, SeasonYearState } from '@/shared/types/state/season';
import {
  ActionContext,
  ActionTree,
  Module,
  MutationTree,
} from 'vuex';

const key = '_season-years';

const defaultState: SeasonYearState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<SeasonYearState> = {
  [SeasonYearMutations.SEASON_YEAR_PENDING](state: SeasonYearState) {
    state.loading = false;
    state.error = null;
    state.data = null;
  },
  [SeasonYearMutations.SEASON_YEAR_SUCCESS](state: SeasonYearState, data: number[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [SeasonYearMutations.SEASON_YEAR_FAILURE](state: SeasonYearState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<SeasonYearState, RootState> = {
  async getSeasonYears(ctx: ActionContext<SeasonYearState, RootState>) {
    let seasonYears = session.first<number[]>(key);

    if (seasonYears === null || seasonYears.expiresIn < new Date()) {
      ctx.commit(SeasonYearMutations.SEASON_YEAR_PENDING);

      const response = await Adapter.getSeasonYear();

      if (response.data) {
        const value = response.data;

        ctx.commit(SeasonYearMutations.SEASON_YEAR_SUCCESS, value);

        if (seasonYears === null) {
          seasonYears = { value, expiresIn: new Date() };
        }

        session.store<number[]>(key, seasonYears.value);
      } else {
        ctx.commit(SeasonYearMutations.SEASON_YEAR_FAILURE, response.error);
      }
    } else {
      ctx.commit(SeasonYearMutations.SEASON_YEAR_SUCCESS, seasonYears.value);
    }

    return seasonYears?.value;
  },
};

const module: Module<SeasonYearState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
