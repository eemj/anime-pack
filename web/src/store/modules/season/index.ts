import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { AnimeSeason } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { SeasonMutations, SeasonState } from '@/shared/types/state/season';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

const key = '_seasons';

const defaultState: SeasonState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<SeasonState> = {
  [SeasonMutations.SEASON_PENDING](state: SeasonState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [SeasonMutations.SEASON_SUCCESS](state: SeasonState, data: AnimeSeason[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [SeasonMutations.SEASON_FAILURE](state: SeasonState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<SeasonState, RootState> = {
  async getSeasons(ctx: ActionContext<SeasonState, RootState>) {
    let seasons = session.first<AnimeSeason[]>(key);

    if (seasons === null || seasons.expiresIn < new Date()) {
      ctx.commit(SeasonMutations.SEASON_PENDING);

      const response = await Adapter.getSeason();

      if (response.data) {
        const value = response.data;

        ctx.commit(SeasonMutations.SEASON_SUCCESS, value);

        if (seasons == null) {
          seasons = { value, expiresIn: new Date() };
        }

        session.store<AnimeSeason[]>(key, seasons.value);
      } else {
        ctx.commit(SeasonMutations.SEASON_FAILURE, response.error);
      }
    } else {
      ctx.commit(SeasonMutations.SEASON_SUCCESS, seasons.value);
    }

    return seasons?.value;
  },
};

const module: Module<SeasonState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
