import { Adapter } from '@/adapter';
import session from '@/shared/storage/session';
import { AnimeFormat } from '@/shared/types/anime';
import { RootState } from '@/shared/types/state';
import { FormatMutations, FormatState } from '@/shared/types/state/format';
import {
  ActionContext, ActionTree, Module, MutationTree,
} from 'vuex';

const key = '_formats';

const defaultState: FormatState = {
  data: null,
  error: null,
  loading: false,
};

const mutations: MutationTree<FormatState> = {
  [FormatMutations.FORMAT_PENDING](state: FormatState) {
    state.loading = true;
    state.error = null;
    state.data = null;
  },
  [FormatMutations.FORMAT_SUCCESS](state: FormatState, data: AnimeFormat[]) {
    state.loading = false;
    state.error = null;
    state.data = data;
  },
  [FormatMutations.FORMAT_FAILURE](state: FormatState, error: string) {
    state.loading = false;
    state.error = error;
    state.data = null;
  },
};

const actions: ActionTree<FormatState, RootState> = {
  async getFormats(ctx: ActionContext<FormatState, RootState>) {
    let formats = session.first<AnimeFormat[]>(key);

    if (formats === null || formats.expiresIn < new Date()) {
      ctx.commit(FormatMutations.FORMAT_PENDING);

      const response = await Adapter.getFormat();

      if (response.data) {
        const value = response.data;

        ctx.commit(FormatMutations.FORMAT_SUCCESS, value);

        if (formats == null) {
          formats = { value, expiresIn: new Date() };
        }

        session.store<AnimeFormat[]>(key, formats.value);
      } else {
        ctx.commit(FormatMutations.FORMAT_FAILURE, response.error);
      }
    } else {
      ctx.commit(FormatMutations.FORMAT_SUCCESS, formats.value);
    }

    return formats?.value;
  },
};

const module: Module<FormatState, RootState> = {
  namespaced: false,
  state: defaultState,
  mutations,
  actions,
};

export default module;
