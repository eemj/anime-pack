import tc2, { Instance } from 'tinycolor2';
import { RootState } from '@/shared/types/state';
import { ActionContext, ActionTree, MutationTree } from 'vuex';
import {
  DimensionUpdate,
  PaletteColor,
  SettingMutations,
  SettingState,
} from '@/shared/types/state/settings';
import * as config from '@/config.json';

const { primaryColour, downloadPriority } = config;

type Context = ActionContext<SettingState, RootState>;

const THEME_KEY = '_theme';

function createPalette(hex: string): PaletteColor[] {
  return tc2(hex)
    .monochromatic(15)
    .map((color: Instance, offset: number) => {
      const number = (offset + 1) * 100;
      const name = number >= 1e3 ? `A${number % 1e3}`.padEnd(4, '0') : number.toString();
      return {
        name,
        hex: color.toHexString(),
      };
    });
}

const defaultState: SettingState = {
  darkMode: true,
  downloadPriority,
  palette: createPalette(primaryColour),
  quickSearch: false,
  dimension: {
    width: window.innerWidth,
    height: window.innerHeight,
  },
  hex: primaryColour,
};

const actions: ActionTree<SettingState, RootState> = {
  updateDimension(ctx: Context, update: DimensionUpdate) {
    ctx.commit(SettingMutations.SETTING_DIMENSION_UPDATE, update);
  },

  applyTheme(ctx: Context) {
    const hex = window.localStorage.getItem(THEME_KEY);

    if (hex && hex !== primaryColour) {
      ctx.commit(SettingMutations.SETTING_CREATE_THEME, hex);
    } else if (!hex) {
      window.localStorage.setItem(THEME_KEY, ctx.state.hex);
    }

    ctx.commit(SettingMutations.SETTING_APPLY_THEME);
  },

  toggleQuickSearch(ctx: Context) {
    ctx.commit(SettingMutations.SETTING_TOGGLE_QUICK_SEARCH);
  },
};

const mutations: MutationTree<SettingState> = {
  [SettingMutations.SETTING_CREATE_THEME](state: SettingState, hex: string) {
    state.hex = hex;
    state.palette = createPalette(hex);
  },
  [SettingMutations.SETTING_APPLY_THEME](state: SettingState) {
    state.palette.forEach((color: PaletteColor) => {
      document.body.style.setProperty(
        `--color-primary-${color.name}`,
        color.hex,
      );
    });
  },

  [SettingMutations.SETTING_DIMENSION_UPDATE](
    state: SettingState,
    update: DimensionUpdate,
  ) {
    if (update.width) state.dimension.width = update.width;
    if (update.height) state.dimension.height = update.height;
  },

  [SettingMutations.SETTING_TOGGLE_DARK_MODE](state: SettingState): void {
    state.darkMode = !state.darkMode;
  },
  [SettingMutations.SETTING_TOGGLE_QUICK_SEARCH](state: SettingState): void {
    state.quickSearch = !state.quickSearch;
  },
};

export default {
  state: defaultState,
  mutations,
  actions,
};
