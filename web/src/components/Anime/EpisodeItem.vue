<template>
  <div class="episode flex flex-align-items-center">
    <div class="thumbnail__wrapper">
      <div
        v-lazy="thumbnail ? thumbnail.name : ''"
        :class="[
          'thumbnail',
          'flex',
          'flex-justify-content-center',
          'flex-align-items-center',
          !thumbnail && 'placeholder',
        ]"
      >
        <div class="overlay" @click="handleEpisodeClick" v-if="thumbnail">
          <play-button-o-icon />
        </div>
      </div>
    </div>

    <div class="label__wrapper">
      <p class="label">{{ episode.name }}</p>
    </div>

    <div class="progress__wrapper flex" v-if="progress">
      <div class="bar" :style="{ width }"></div>
      <div class="details">
        <div class="item flex" v-for="item in progress" :key="item">
          <p class="release-group" v-if="item.rg">
            <strong>[{{ item.rg }}]</strong>
          </p>
          <p class="height" v-if="item.h">
            <i>{{ item.h }}p</i>
          </p>
          <p class="percentage" v-if="item.pg !== null && item.pg.pct >= 0">
            {{ percentage(item.pg.pct) }}
          </p>
          <p class="speed" v-if="item.pg !== null && item.pg.sp >= 0">
            {{ speed(item.pg) }}
          </p>
        </div>
      </div>
    </div>

    <div
      class="actions__wrapper flex"
      v-if="hasAnyChoice && (releaseGroup !== null && quality !== null)"
    >
      <div
        class="actions flex flex-justify-content-center flex-align-items-center"
      >
        <select
          class="release-group flex-1"
          name="releaseGroup"
          :value="releaseGroup"
          @change="handleGroupChange"
        >
          <option value="-1" class="hide">Group</option>
          <option
            v-for="(sub, index) in episode.releaseGroups"
            :value="String(index)"
            :key="sub"
          >
            {{ sub.name }}
          </option>
        </select>

        <select
          class="quality flex-1"
          name="quality"
          :value="quality"
          :disabled="releaseGroup < 0"
          @change="handleQualityChange"
        >
          <template v-if="releaseGroup < 0">
            <option value="-1" class="hide">Quality</option>
          </template>
          <template v-else>
            <option value="-1" class="hide">Quality</option>
            <option
              v-for="(qual, index) in episode.releaseGroups[releaseGroup]
                .qualities"
              :value="index"
              :key="qual"
            >
              {{ qual.height !== undefined ? `${qual.height}p` : "N/A" }}
            </option>
          </template>
        </select>

        <button
          class="btn icon"
          @click="handleClick"
          :disabled="releaseGroup < 0 || quality < 0"
          v-if="connected"
          aria-label="Download / Click"
        >
          <close-icon v-if="hasProgress" />
          <download-icon v-else />
        </button>

        <button
          class="btn icon"
          @click="handlePrioritizeChange"
          v-if="
            !hasProgress && connected && (releaseGroup >= 0 || quality >= 0)
          "
          aria-label="Prioritize"
        >
          <zap-icon :class="['zap', { active: !!prioritize }]" />
        </button>

        <transition name="fade">
          <button
            class="btn icon"
            @click="handleClear"
            v-if="releaseGroup >= 0 || quality >= 0"
            aria-label="Clear"
          >
            <erase-icon />
          </button>
        </transition>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import {
  computed, ComputedRef, defineComponent, PropType,
} from 'vue';
import { AnimeEpisode } from '@/shared/types/episode';

import { percentage, speed } from '@/shared/helpers/formatter';

import DownloadIcon from '@/components/Icons/DownloadIcon.vue';
import EraseIcon from '@/components/Icons/EraseIcon.vue';
import CloseIcon from '@/components/Icons/CloseIcon.vue';
import PlayButtonOIcon from '@/components/Icons/PlayButtonOIcon.vue';
import { AnimeEpisodeProgress } from '@/shared/types/episode/progress';
import ZapIcon from '../Icons/ZapIcon.vue';

export enum EpisodeItemEmit {
  UPDATE_RELEASE_GROUP = 'update:releaseGroup',
  UPDATE_QUALITY = 'update:quality',
  UPDATE_PRIORITIZE = 'update:prioritize',
  CLICK_DOWNLOAD = 'click-download',
  CLICK_CANCEL = 'click-cancel',
  CLICK_EPISODE = 'click-episode',
}

export default defineComponent({
  name: 'episode-item',

  components: {
    DownloadIcon,
    EraseIcon,
    CloseIcon,
    PlayButtonOIcon,
    ZapIcon,
  },

  props: {
    episode: {
      required: true,
      type: Object as PropType<AnimeEpisode>,
    },
    progress: {
      required: false,
      type: Object as PropType<AnimeEpisodeProgress[]>,
    },
    releaseGroup: {
      required: false,
      default: null,
      type: Number,
    },
    quality: {
      required: false,
      default: null,
      type: Number,
    },
    prioritize: {
      required: false,
      default: false,
      type: Boolean,
    },

    connected: {
      required: false,
      type: Boolean,
      default: true,
    },
  },

  emits: [
    EpisodeItemEmit.UPDATE_RELEASE_GROUP,
    EpisodeItemEmit.UPDATE_QUALITY,
    EpisodeItemEmit.UPDATE_PRIORITIZE,

    EpisodeItemEmit.CLICK_DOWNLOAD,
    EpisodeItemEmit.CLICK_CANCEL,
    EpisodeItemEmit.CLICK_EPISODE,
  ],

  setup(props, context) {
    const thumbnail = computed(
      () =>
        props.episode.releaseGroups
          .find((group) => group.qualities.find((quality) => quality.thumbnail))
          ?.qualities.find((quality) => quality.thumbnail)?.thumbnail,
    );

    const hasAnyChoice = computed(() => props.episode.releaseGroups?.length > 0);

    const hasProgress: ComputedRef<boolean> = computed(() => {
      if (
        props?.releaseGroup < 0
        || props?.quality < 0
        || props.progress === undefined
      ) {
        return false;
      }

      const { name, qualities } = props.episode.releaseGroups[props.releaseGroup];

      return props.progress.some(
        (progress) =>
          progress.rg === name
          && (progress.h === 0
            ? qualities[props.quality].height === undefined
            : progress.h === qualities[props.quality].height),
      );
    });

    const handlePrioritizeChange = () => {
      context.emit(EpisodeItemEmit.UPDATE_PRIORITIZE, !props.prioritize);
    };

    const handleQualityChange = (ev: Event) => {
      context.emit(
        EpisodeItemEmit.UPDATE_QUALITY,
        Number((ev.target as HTMLInputElement).value),
      );
    };

    const handleGroupChange = (ev: Event) => {
      context.emit(EpisodeItemEmit.UPDATE_QUALITY, -1);

      const selection = Number((ev.target as HTMLInputElement).value);

      context.emit(EpisodeItemEmit.UPDATE_RELEASE_GROUP, selection);

      // If the quality is exactly 1 there's no other option,
      // Therefore we'll auto-select the 1st option.
      if (
        props?.episode?.releaseGroups?.length > 0
        && props.episode.releaseGroups[selection]
        && props.episode.releaseGroups[selection]?.qualities?.length === 1
      ) {
        context.emit(EpisodeItemEmit.UPDATE_QUALITY, 0);
      }
    };

    const handleClick = () => {
      if (props?.releaseGroup >= 0 && props?.quality >= 0) {
        const releaseGroup = props.episode.releaseGroups[props.releaseGroup];

        context.emit(
          hasProgress.value
            ? EpisodeItemEmit.CLICK_CANCEL
            : EpisodeItemEmit.CLICK_DOWNLOAD,
          [
            {
              d: releaseGroup.episodeID,
              q: releaseGroup.qualities[props.quality].id,
              rg: releaseGroup.id,
              p: !props.prioritize ? undefined : props.prioritize,
            },
          ],
        );
      }
    };

    const handleClear = () => {
      context.emit(EpisodeItemEmit.UPDATE_RELEASE_GROUP, -1);
      context.emit(EpisodeItemEmit.UPDATE_QUALITY, -1);
      context.emit(EpisodeItemEmit.UPDATE_PRIORITIZE, undefined);
    };

    const handleEpisodeClick = () => {
      context.emit(EpisodeItemEmit.CLICK_EPISODE, props.episode.name);
    };

    const width = computed(() => {
      let totalPercentage = 0;
      const size = (props.progress || []).length;

      if (props.progress) {
        props.progress.forEach((progress) => {
          totalPercentage += +(progress.pg?.pct || 0);
        });
      }

      if (size > 0) {
        totalPercentage /= size || 1;
      }

      return `${totalPercentage}%`;
    });

    return {
      percentage,
      speed,
      thumbnail,

      width,
      hasAnyChoice,
      hasProgress,
      handleClick,
      handleClear,
      handleEpisodeClick,
      handleQualityChange,
      handleGroupChange,
      handlePrioritizeChange,
    };
  },
});
</script>

<style lang="scss" scoped>
@import "@/assets/scss/variables.scss";

$height: 130px;
$width: 285px;

div.episode {
  height: $height;
  width: $width;
  cursor: pointer;
  overflow: hidden;
  position: relative;

  div.thumbnail__wrapper {
    position: absolute;
    display: block;
    height: 100%;
    width: 100%;
    overflow: hidden;
    z-index: 1;

    &:hover {
      div.thumbnail {
        transform: scale(1.1);

        div.overlay {
          opacity: 1;
        }
      }
    }

    div.thumbnail {
      position: absolute;
      top: 0;
      right: 0;
      left: 0;
      bottom: 0;
      height: auto;
      width: 100%;
      margin: 0;
      transition: transform 0.25s ease-out;
      background-size: cover;
      background-position: center;

      &.placeholder {
        background-image: radial-gradient(
          ellipse at center,
          rgba(34, 34, 34, 1) 0%,
          rgba(34, 34, 34, 0.63) 37%,
          rgba(34, 34, 34, 0) 100%
        );
        background-position: 0% 0%;
      }

      div.overlay {
        opacity: 0;
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: opacity 0.35s ease-out;
        box-sizing: border-box;

        svg {
          height: 55%;
          width: 55%;

          color: #ffffffcc;
          filter: drop-shadow(0px 1px 0px rgba(0, 0, 0, 0.3));
        }
      }
    }
  }

  div.label__wrapper {
    position: absolute;
    bottom: 0;
    width: 100%;
    z-index: 5;
    pointer-events: none;

    &::before {
      content: "";
      display: block;
    }

    &::after {
      content: "";
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background: linear-gradient(
        to bottom,
        rgba(
            var(--color-dark-1-r),
            var(--color-dark-1-g),
            var(--color-dark-1-b),
            0
          )
          70%,
        rgba(
            var(--color-dark-1-r),
            var(--color-dark-1-g),
            var(--color-dark-1-b),
            0.75
          )
          100%
      );
    }

    p.label {
      position: absolute;
      bottom: 0;
      margin: 5px;
      right: 0;
      color: var(--color-light-6);
      padding: 0 0.2rem;
      font-size: 1.2rem;
      line-height: normal;
      display: inline-block;
      text-shadow: 0 1px 0 rgba(0, 0, 0, 0.25);
      user-select: none;
    }
  }

  div.progress__wrapper {
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 1;
    pointer-events: none;

    div.bar {
      position: absolute;
      height: 100%;
      background: rgb(0, 0, 0);
      background: linear-gradient(
        90deg,
        rgba(0, 0, 0, 0.15) 0%,
        rgba(0, 0, 0, 0.15) 100%
      );
      transition: 1s ease-in width;
      z-index: 2;
    }

    div.details {
      $space: 45px;
      margin-top: $space;
      height: calc(100% - #{$space});
      width: 100%;
      position: relative;
      z-index: 3;
      pointer-events: none;

      div.item {
        padding-left: 10px;
        font-family: "Roboto", sans-serif;
        font-size: 10px;

        p {
          &:last-child {
            text-align: left;
            min-width: 60px;
          }

          margin: 2px;
          text-align: center;
        }
      }
    }
  }

  div.actions__wrapper {
    width: 100%;
    height: 100%;
    z-index: 2;

    div.actions {
      margin: 10px;
      padding: 2px;
      height: 100%;
      max-height: 26px;
      background-color: var(--color-dark-5);
      border-radius: 5px;
      box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.25);

      select {
        color: var(--color-light-4);
        border: none;
        background-color: transparent;
        font-size: 10px;
        font-family: "Roboto", sans-serif;
        font-weight: 400;
        margin: 0 5px;
        width: 100%;
        height: 24px;
        -webkit-appearance: none;
        overflow: hidden;
        text-align: center;
        border-radius: 5px;

        option {
          color: var(--color-dark-1);
        }

        &.release-group {
          min-width: 70px;
        }

        &.quality {
          min-width: 50px;
        }
      }

      button.btn {
        background: transparent;
        box-shadow: none;
      }

      svg {
        height: 18px;
        width: 24px;

        &.zap {
          color: var(--color-primary-A100);

          &.active {
            color: var(--color-primary-A500);
          }
        }
      }
    }
  }
}
</style>
