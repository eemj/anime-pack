import '@/registerServiceWorker';
import '@/assets/scss/index.scss';

import { createApp } from 'vue';

import lazy from '@/directives/lazy';
import App from '@/App.vue';
import router from '@/router';
import createStore from '@/store';

const store = createStore(router);

createApp(App).use(store).use(router).directive('lazy', lazy())
  .mount('#app');
