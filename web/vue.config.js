const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  productionSourceMap: false,
  transpileDependencies: true,
  pwa: {
    name: 'Anime Pack',
    iconPaths: {
      faviconSVG: 'img/icons/favicon.svg',
      favicon32: 'img/icons/favicon-32x32.png',
      favicon16: 'img/icons/favicon-16x16.png',
      appleTouchIcon: 'img/icons/apple-icon-152x152.png',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/icons/mstile-144x144.png',
    },
    themeColor: '#6D0C26',
    appleMobileWebAppCapable: 'yes',
  },
  devServer: {
    port: 3000,
    allowedHosts: 'all',
  },
  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      args[0].title = 'Anime Pack';
      return args;
    });

    config.optimization.splitChunks({
      minSize: 10000,
      maxSize: 350000,
      cacheGroups: {
        // eslint-disable-next-line
        node_vendors: {
          test: /[\\/]node_modules[\\/]/,
          chunks: 'all',
          priority: 1,
        },
      },
    });
  },
});
