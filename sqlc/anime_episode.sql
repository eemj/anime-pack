-- name: FilterAnimeEpisode :many
select
    title_episode.id as title_episode_id,
    title_episode.created_at as title_episode_created_at,
    episode.name as episode_name,
    release_group.id as release_group_id,
    release_group.name as release_group_name,
    xdcc_quality.id as xdcc_quality_id,
    xdcc_quality.height as xdcc_quality_height,
    video_quality.id as video_quality_id,
    video_quality.height as video_quality_height,
    thumbnail.id as thumbnail_id,
    thumbnail.name as thumbnail_name,
    thumbnail.hash as thumbnail_hash,
    video.id as video_id,
    video.path as video_path,
    video.crc32 as video_crc32,
    video.duration as video_duration,
    video.size as video_size,
    video.created_at as video_created_at,
    anime.status as anime_status
from title_anime
join title on title_anime.title_id = title.id
join anime on title_anime.anime_id = anime.id
join title_episode on title.id = title_episode.title_id
join episode on title_episode.episode_id = episode.id
left join xdcc ON title_episode.id = xdcc.title_episode_id
left join release_group ON xdcc.release_group_id = release_group.id
left join video on xdcc.id = video.xdcc_id
left join quality video_quality ON video_quality.id = video.quality_id
left join quality xdcc_quality ON xdcc_quality.id = xdcc.quality_id
left join thumbnail ON video.thumbnail_id = thumbnail.id
where
    -- Defaults
    (title.is_deleted = false and title.reviewed = true) and
    (title_episode.is_deleted = false) and
    (xdcc.is_deleted = false) and

    -- Filter
    (title_anime.anime_id = coalesce(sqlc.arg('anime_id'), title_anime.anime_id)) and
    (title_episode.episode_id = coalesce(sqlc.narg('episode_id'), title_episode.episode_id)) and
    (episode.name = coalesce(sqlc.narg('episode_name'), episode.name));

-- name: CountAnimeEpisodeLatest :one
select count(video.id)
from video
join xdcc ON video.xdcc_id = xdcc.id
join title_episode on xdcc.title_episode_id = title_episode.id
join title_anime on title_anime.title_id = title_episode.title_id
join anime on title_anime.anime_id = anime.id
left join preferences ON anime.id = preferences.anime_id
where
    (preferences.favourite = case when sqlc.narg('favourite')::bool is null then preferences.favourite else sqlc.narg('favourite')::bool end) and
    (anime.status = case when sqlc.narg('status')::anime_status is null then anime.status else sqlc.narg('status')::anime_status end);

-- name: FilterAnimeEpisodeLatest :many
select
    episode.id as episode_id,
    episode.name as episode_name,
    title_episode.id as title_episode_id,
    anime.id as anime_id,
    anime.title as anime_title,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title_romaji as anime_title_romaji,
    anime.score as anime_score,
    poster.id as poster_id,
    poster.hash as poster_hash,
    poster.name as poster_name,
    video.path as video_path,
    video.crc32 as video_crc32,
    video.size as video_size,
    video.duration as video_duration,
    video.created_at as video_created_at,
    thumbnail.id as thumbnail_id,
    thumbnail.hash as thumbnail_hash,
    thumbnail.name as thumbnail_name,
    release_group.id as release_group_id,
    release_group.name as release_group_name,
    xdcc_quality.id as xdcc_quality_id,
    xdcc_quality.id as xdcc_quality_height,
    video_quality.id as video_quality_id,
    video_quality.id as video_quality_height
from video
join xdcc ON video.xdcc_id = xdcc.id
join thumbnail ON video.thumbnail_id = thumbnail.id
join release_group on xdcc.release_group_id = release_group.id
left join quality video_quality ON video_quality.id = video.quality_id
left join quality xdcc_quality ON xdcc_quality.id = xdcc.quality_id
join title_episode on xdcc.title_episode_id = title_episode.id
join episode on title_episode.episode_id = episode.id
join title_anime on title_anime.title_id = title_episode.title_id
join anime on title_anime.anime_id = anime.id
join poster on anime.poster_id = poster.id
left join preferences ON anime.id = preferences.anime_id
where
    (preferences.favourite = case when sqlc.narg('favourite')::bool is null then preferences.favourite else sqlc.narg('favourite')::bool end) and
    (anime.status = case when sqlc.narg('status')::anime_status is null then anime.status else sqlc.narg('status')::anime_status end)
order by
    case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = true) then video.created_at end desc,
    case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = false) then video.created_at end asc,

    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = true) then anime.score end desc,
    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = false) then anime.score end asc,

    case when (sqlc.arg('order_by')::text = 'size' and sqlc.arg('order_by_desc')::bool = true) then video.size end desc,
    case when (sqlc.arg('order_by')::text = 'size' and sqlc.arg('order_by_desc')::bool = false) then video.size end asc,

    case when (sqlc.arg('order_by')::text = 'duration' and sqlc.arg('order_by_desc')::bool = true) then video.duration end desc,
    case when (sqlc.arg('order_by')::text = 'duration' and sqlc.arg('order_by_desc')::bool = false) then video.duration end asc
limit sqlc.arg('limit')::bigint
offset sqlc.arg('offset')::bigint;
