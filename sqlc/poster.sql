-- name: InsertPoster :one
with ins as (
    insert into poster (name, hash, uri)
    values (sqlc.arg('name'), sqlc.arg('hash'), sqlc.arg('uri'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from poster
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: InsertManyPoster :batchmany
with ins as (
    insert into poster (name, hash, uri)
    values (sqlc.arg('name'), sqlc.arg('hash'), sqlc.arg('uri'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from poster
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: CountPoster :one
select count(1)
from poster
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri));

-- name: FilterPoster :many
select *
from poster
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri));

-- name: FirstPoster :one
select *
from poster
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri))
limit 1;

-- name: UpdatePoster :execrows
update poster
set
    name = coalesce(sqlc.narg('name'), name),
    hash = coalesce(sqlc.narg('hash'), hash),
    uri = coalesce(sqlc.narg('uri'), uri)
where id = sqlc.arg('id');

-- name: DeletePoster :execrows
delete from poster where id = sqlc.arg('id');

-- name: DeleteUnusedPosters :execrows
delete from poster where poster.id in (
    select poster.id
    from poster
    left join anime on anime.poster_id = poster.id
    where anime.id is null
);
