-- name: InsertGenre :batchmany
with ins as (
    insert into genre (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from genre
where name = sqlc.arg('name')
limit 1;

-- name: CountGenre :one
select count(1)
from genre
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterGenre :many
select *
from genre
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstGenre :one
select *
from genre
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateGenre :execrows
update genre
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteGenre :execrows
delete from genre where id = any(sqlc.arg('ids')::bigint[]);
