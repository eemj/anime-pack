-- name: InsertRunner :one
with ins as (
    insert into runner (name, "user", ident, hostname, signature)
    values (sqlc.arg('name'), sqlc.arg('user'), sqlc.arg('ident'), sqlc.arg('hostname'), sqlc.arg('signature'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from runner
where ident = sqlc.arg('ident');

-- name: CountRunner :one
select count(1)
from runner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    ("user" = coalesce(sqlc.narg('user'), "user")) and
    (ident = coalesce(sqlc.narg('ident'), ident)) and
    (hostname = coalesce(sqlc.narg('hostname'), hostname)) and
    (signature = coalesce(sqlc.narg('signature'), signature));

-- name: FilterRunner :one
select *
from runner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    ("user" = coalesce(sqlc.narg('user'), "user")) and
    (ident = coalesce(sqlc.narg('ident'), ident)) and
    (hostname = coalesce(sqlc.narg('hostname'), hostname)) and
    (signature = coalesce(sqlc.narg('signature'), signature));

-- name: FirstRunner :one
select *
from runner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    ("user" = coalesce(sqlc.narg('user'), "user")) and
    (ident = coalesce(sqlc.narg('ident'), ident)) and
    (hostname = coalesce(sqlc.narg('hostname'), hostname)) and
    (signature = coalesce(sqlc.narg('signature'), signature))
limit 1;

-- name: UpdateRunner :execrows
update runner
set name = coalesce(sqlc.narg('name'), name),
    "user" = coalesce(sqlc.narg('user'), "user"),
    hostname = coalesce(sqlc.narg('hostname'), hostname)
where id = sqlc.arg('id');

-- name: DeleteRunner :execrows
delete from runner where id = sqlc.arg('id');
