-- name: InsertXDCC :batchexec
insert into xdcc (pack, size, filename, escaped_filename, bot_id, quality_id, release_group_id, title_episode_id)
values (
    sqlc.arg('pack'),
    sqlc.arg('size'),
    sqlc.arg('filename'),
    sqlc.arg('escaped_filename'),
    sqlc.arg('bot_id'),
    sqlc.arg('quality_id'),
    sqlc.arg('release_group_id'),
    sqlc.arg('title_episode_id')
)
on conflict do nothing;

-- name: CountXDCC :one
select count(1)
from xdcc
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (size = coalesce(sqlc.narg('size'), size)) and
    (filename = coalesce(sqlc.narg('filename'), filename)) and
    (escaped_filename = coalesce(sqlc.narg('escaped_filename'), escaped_filename)) and
    (bot_id = coalesce(sqlc.narg('bot_id'), bot_id)) and
    (release_group_id = coalesce(sqlc.narg('release_group_id'), release_group_id)) and
    (title_episode_id = coalesce(sqlc.narg('title_episode_id'), title_episode_id)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: FilterXDCC :many
select
    xdcc.id,
    xdcc.pack,
    xdcc.size,
    xdcc.filename,
    xdcc.escaped_filename,
    xdcc.bot_id,
    xdcc.quality_id,
    xdcc.release_group_id,
    xdcc.title_episode_id,
    xdcc.created_at,
    xdcc.updated_at,
    xdcc.deleted_at,
    xdcc.is_deleted,
    video.id as video_id,
    bot.name as bot_name,
    release_group.name as release_group_name,
    quality.height as quality_height,
    title_episode.title_id as title_episode_title_id,
    title_episode.episode_id as title_episode_episode_id,
    episode.name as episode_name,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year
from xdcc
join bot on xdcc.bot_id = bot.id
join release_group on xdcc.release_group_id = release_group.id
join title_episode on xdcc.title_episode_id = title_episode.id
left join quality on xdcc.quality_id = quality.id
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
left join video on video.xdcc_id = xdcc.id
where
    (xdcc.id = coalesce(sqlc.narg('id'), xdcc.id)) and
    (xdcc.size = coalesce(sqlc.narg('size'), xdcc.size)) and
    (xdcc.filename = coalesce(sqlc.narg('filename'), xdcc.filename)) and
    (escaped_filename = coalesce(sqlc.narg('escaped_filename'), escaped_filename)) and
    (xdcc.bot_id = coalesce(sqlc.narg('bot_id'), xdcc.bot_id)) and
    (xdcc.release_group_id = coalesce(sqlc.narg('release_group_id'), xdcc.release_group_id)) and
    (xdcc.title_episode_id = coalesce(sqlc.narg('title_episode_id'), xdcc.title_episode_id)) and
    (xdcc.is_deleted = coalesce(sqlc.narg('is_deleted'), xdcc.is_deleted)) and
    (title_episode.title_id = coalesce(sqlc.narg('title_id'), title_episode.title_id)) and
    (title.reviewed = coalesce(sqlc.narg('title_reviewed')::bool, title.reviewed)) and
    (title.is_deleted = coalesce(sqlc.narg('title_is_deleted')::bool, title.is_deleted)) and
    (case when sqlc.arg('without_video')::bool then video is null else true end) and
    (case
        when sqlc.arg('with_quality_id')::bool then case
            when sqlc.narg('quality_id')::bigint is null then xdcc.quality_id is null
            else xdcc.quality_id = sqlc.narg('quality_id')::bigint
        end
        else true
    end); 

-- name: FirstXDCC :one
select
    xdcc.id,
    xdcc.pack,
    xdcc.size,
    xdcc.filename,
    xdcc.escaped_filename,
    xdcc.bot_id,
    xdcc.quality_id,
    xdcc.release_group_id,
    xdcc.title_episode_id,
    xdcc.created_at,
    xdcc.updated_at,
    xdcc.deleted_at,
    xdcc.is_deleted,
    video.id as video_id,
    bot.name as bot_name,
    release_group.name as release_group_name,
    quality.height as quality_height,
    title_episode.title_id as title_episode_title_id,
    title_episode.episode_id as title_episode_episode_id,
    episode.name as episode_name,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year
from xdcc
join bot on xdcc.bot_id = bot.id
join release_group on xdcc.release_group_id = release_group.id
join title_episode on xdcc.title_episode_id = title_episode.id
left join quality on xdcc.quality_id = quality.id
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
left join video on video.xdcc_id = xdcc.id
where
    (xdcc.id = coalesce(sqlc.narg('id'), xdcc.id)) and
    (xdcc.size = coalesce(sqlc.narg('size'), xdcc.size)) and
    (xdcc.filename = coalesce(sqlc.narg('filename'), xdcc.filename)) and
    (escaped_filename = coalesce(sqlc.narg('escaped_filename'), escaped_filename)) and
    (xdcc.bot_id = coalesce(sqlc.narg('bot_id'), xdcc.bot_id)) and
    (xdcc.release_group_id = coalesce(sqlc.narg('release_group_id'), xdcc.release_group_id)) and
    (xdcc.title_episode_id = coalesce(sqlc.narg('title_episode_id'), xdcc.title_episode_id)) and
    (xdcc.is_deleted = coalesce(sqlc.narg('is_deleted'), xdcc.is_deleted)) and
    (title_episode.title_id = coalesce(sqlc.narg('title_id'), title_episode.title_id)) and
    (case when sqlc.arg('without_video')::bool then video is null else true end)
limit 1;

-- name: UpdateXDCC :execrows
update xdcc
set 
    pack = coalesce(sqlc.narg('pack'), pack),
    size = coalesce(sqlc.narg('size'), size),
    filename = coalesce(sqlc.narg('filename'), filename),
    escaped_filename = coalesce(sqlc.narg('escaped_filename'), escaped_filename),
    bot_id = coalesce(sqlc.narg('bot_id'), bot_id),
    quality_id = coalesce(sqlc.narg('quality_id'), quality_id),
    release_group_id = coalesce(sqlc.narg('release_group_id'), release_group_id),
    title_episode_id = coalesce(sqlc.narg('title_episode_id'), title_episode_id)
where id = sqlc.arg('id');

-- name: SoftDeleteXDCC :execrows
update xdcc
set deleted_at = timezone('utc'::text, now())
where
    (id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
    (title_episode_id = any(sqlc.narg('title_episode_ids')::bigint[]) or array_length(sqlc.narg('title_episode_ids')::bigint[], 1) is null);

-- name: DeleteXDCC :execrows
delete from xdcc
where
    (id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
    (title_episode_id = any(sqlc.narg('title_episode_ids')::bigint[]) or array_length(sqlc.narg('title_episode_ids')::bigint[], 1) is null);
