-- name: CountTitleReview :one
select count(1)
from title
where
    (title.reviewed = coalesce(sqlc.narg('reviewed'), title.reviewed)) and
    (title.is_deleted = coalesce(sqlc.narg('is_deleted'), title.is_deleted));

-- name: FilterTitleReview :many
with filtered_title as (
    select
        title.id as title_id,
        title.name as title_name,
        title.reviewed as title_reviewed,
        title.created_at as title_created_at,
        title.updated_at as title_updated_at,
        title.deleted_at as title_deleted_at,
        title.is_deleted as title_is_deleted,
        title.season_number as title_season_number,
        title.year as title_year
    from title
    where
        (title.reviewed = coalesce(sqlc.narg('reviewed'), title.reviewed)) and
        (title.is_deleted = coalesce(sqlc.narg('is_deleted'), title.is_deleted))
    limit sqlc.arg('limit')::bigint
    offset sqlc.arg('offset')::bigint
)
select
        filtered_title.title_id,
        filtered_title.title_name,
        filtered_title.title_reviewed,
        filtered_title.title_created_at,
        filtered_title.title_updated_at,
        filtered_title.title_deleted_at,
        filtered_title.title_is_deleted,
        filtered_title.title_season_number,
        filtered_title.title_year,
        anime.id as anime_id,
        anime.id_mal as anime_id_mal,
        anime.start_date as anime_start_date,
        anime.end_date as anime_end_date,
        anime.score as anime_score,
        anime.description as anime_description,
        anime.title_romaji as anime_title_romaji,
        anime.title_english as anime_title_english,
        anime.title_native as anime_title_native,
        anime.title as anime_title,
        anime.poster_id as anime_poster_id,
        anime.banner_id as anime_banner_id,
        anime.colour as anime_colour,
        anime.year as anime_year,
        anime.next_airing_date as anime_next_airing_date,
        anime.created_at as anime_created_at,
        anime.updated_at as anime_updated_at,
        anime.status as anime_status,
        anime.format as anime_format,
        anime.season as anime_season,
        poster.name as poster_name,
        poster.hash as poster_hash,
        poster.uri as poster_uri,
        banner.name as banner_name,
        banner.hash as banner_hash,
        banner.uri as banner_uri,
        xdcc.filename as xdcc_filename
from filtered_title
left join title_anime on filtered_title.title_id = title_anime.title_id
left join title_episode on filtered_title.title_id = title_episode.title_id
left join xdcc on title_episode.id = xdcc.title_episode_id
left join anime on anime.id = title_anime.anime_id
left join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
order by
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = true) then filtered_title.title_id end desc,
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = false) then filtered_title.title_id end desc,

    case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = true) then filtered_title.title_created_at end desc,
    case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = false) then filtered_title.title_created_at end asc;
