-- name: InsertTitleEpisode :one
with ins as (
    insert into title_episode (title_id, episode_id)
    values (sqlc.arg('title_id'), sqlc.arg('episode_id')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from title_episode
where title_id = sqlc.arg('title_id') and episode_id = sqlc.arg('episode_id')
limit 1;

-- name: InsertManyTitleEpisode :batchmany
with ins as (
    insert into title_episode (title_id, episode_id)
    values (sqlc.arg('title_id'), sqlc.arg('episode_id')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from title_episode
where title_id = sqlc.arg('title_id') and episode_id = sqlc.arg('episode_id')
limit 1;

-- name: CountTitleEpisode :one
select count(1)
from title_episode
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (title_id = coalesce(sqlc.narg('title_id'), title_id)) and
    (episode_id = coalesce(sqlc.narg('episode_id'), episode_id)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: FilterTitleEpisode :many
select
    title_episode.id,
    title_episode.title_id,
    title_episode.episode_id,
    title_episode.created_at,
    title_episode.deleted_at,
    title_episode.is_deleted,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year,
    title.reviewed as title_reviewed,
    title.is_deleted as title_is_deleted,
    title.deleted_at as title_deleted_at,
    episode.name as episode_name
from title_episode
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
where
    (title_episode.id = coalesce(sqlc.narg('id'), title_episode.id)) and
    (title_episode.title_id = coalesce(sqlc.narg('title_id'), title_episode.title_id)) and
    (title_episode.episode_id = coalesce(sqlc.narg('episode_id'), title_episode.episode_id)) and
    (title_episode.is_deleted = coalesce(sqlc.narg('is_deleted'), title_episode.is_deleted));

-- name: FirstTitleEpisode :one
select
    title_episode.id,
    title_episode.title_id,
    title_episode.episode_id,
    title_episode.created_at,
    title_episode.deleted_at,
    title_episode.is_deleted,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year,
    title.reviewed as title_reviewed,
    title.is_deleted as title_is_deleted,
    title.deleted_at as title_deleted_at,
    episode.name as episode_name
from title_episode
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
where
    (title_episode.id = coalesce(sqlc.narg('id'), title_episode.id)) and
    (title_episode.title_id = coalesce(sqlc.narg('title_id'), title_episode.title_id)) and
    (title_episode.episode_id = coalesce(sqlc.narg('episode_id'), title_episode.episode_id)) and
    (title_episode.is_deleted = coalesce(sqlc.narg('is_deleted'), title_episode.is_deleted))
limit 1;

-- name: SoftDeleteTitleEpisode :execrows
update title_episode
set deleted_at = timezone('utc'::text, now())
where
    (id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
    (title_id = coalesce(sqlc.narg('title_id'), title_id)) and
    (episode_id = coalesce(sqlc.narg('episode_id'), episode_id));

-- name: DeleteTitleEpisode :execrows
delete from title_episode
where
    (id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
    (title_id = coalesce(sqlc.narg('title_id'), title_id)) and
    (episode_id = coalesce(sqlc.narg('episode_id'), episode_id));
