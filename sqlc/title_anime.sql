-- name: InsertTitleAnime :batchmany
with ins as (
    insert into title_anime (title_id, anime_id)
    values (sqlc.arg('title_id'), sqlc.arg('anime_id')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from title_anime
where title_id = sqlc.arg('title_id') and anime_id = sqlc.arg('anime_id')
limit 1;

-- name: CountTitleAnime :one
select count(1)
from title_anime
where
    (title_id = coalesce(sqlc.narg('title_id'), title_id)) and
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id));

-- name: FilterTitleAnime :many
select
    title_anime.title_id,
    title_anime.anime_id,
    title_anime.created_at,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year,
    title.reviewed as title_reviewed,
    title.is_deleted as title_is_deleted,
    anime.id_mal as anime_id_mal,
    anime.start_date as anime_start_date,
    anime.end_date as anime_end_date,
    anime.score as anime_score,
    anime.description as anime_description,
    anime.title_romaji as anime_title_romaji,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title as anime_title,
    anime.poster_id as anime_poster_id,
    anime.banner_id as anime_banner_id,
    anime.colour as anime_colour,
    anime.year as anime_year,
    anime.next_airing_date as anime_next_airing_date,
    anime.created_at as anime_created_at,
    anime.updated_at as anime_updated_at,
    anime.status as anime_status,
    anime.format as anime_format,
    anime.season as anime_season,
    poster.name as poster_name,
    poster.hash as poster_hash,
    poster.uri as poster_uri,
    banner.name as banner_name,
    banner.hash as banner_hash,
    banner.uri as banner_uri
from title_anime
join anime on title_anime.anime_id = anime.id
join title on title_anime.title_id = title.id
join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
where
    (title_anime.title_id = coalesce(sqlc.narg('title_id'), title_anime.title_id)) and
    (title_anime.anime_id = coalesce(sqlc.narg('anime_id'), title_anime.anime_id));

-- name: FirstTitleAnime :one
select
    title_anime.title_id,
    title_anime.anime_id,
    title_anime.created_at,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year,
    title.reviewed as title_reviewed,
    title.is_deleted as title_is_deleted,
    anime.id_mal as anime_id_mal,
    anime.start_date as anime_start_date,
    anime.end_date as anime_end_date,
    anime.score as anime_score,
    anime.description as anime_description,
    anime.title_romaji as anime_title_romaji,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title as anime_title,
    anime.poster_id as anime_poster_id,
    anime.banner_id as anime_banner_id,
    anime.colour as anime_colour,
    anime.year as anime_year,
    anime.next_airing_date as anime_next_airing_date,
    anime.created_at as anime_created_at,
    anime.updated_at as anime_updated_at,
    anime.status as anime_status,
    anime.format as anime_format,
    anime.season as anime_season,
    poster.name as poster_name,
    poster.hash as poster_hash,
    poster.uri as poster_uri,
    banner.name as banner_name,
    banner.hash as banner_hash,
    banner.uri as banner_uri
from title_anime
join anime on title_anime.anime_id = anime.id
join title on title_anime.title_id = title.id
join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
where
    (title_anime.title_id = coalesce(sqlc.narg('title_id'), title_anime.title_id)) and
    (title_anime.anime_id = coalesce(sqlc.narg('anime_id'), title_anime.anime_id))
limit 1;

-- name: DeleteTitleAnime :execrows
delete from title_anime
where
    title_id = coalesce(sqlc.narg('title_id'), title_id) and
    anime_id = coalesce(sqlc.narg('anime_id'), anime_id);

