-- name: ListAnimeSimulcast :many
select
    anime.id as anime_id,
    anime.id_mal as anime_id_mal,
    anime.start_date as anime_start_date,
    anime.end_date as anime_end_date,
    anime.score as anime_score,
    anime.description as anime_description,
    anime.title_romaji as anime_title_romaji,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title as anime_title,
    anime.status as anime_status,
    anime.format as anime_format,
    anime.poster_id as anime_poster_id,
    anime.banner_id as anime_banner_id,
    anime.season as anime_season,
    anime.year as anime_year,
    anime.colour as anime_colour,
    poster.name as poster_name,
    poster.hash as poster_hash,
    banner.name as banner_name,
    banner.hash as banner_hash,
    simulcast.hour as simulcast_hour,
    simulcast.minute as simulcast_minute,
    simulcast.second as simulcast_second,
    simulcast.weekday as simulcast_weekday,
    genre.id as genre_id,
    genre.name as genre_name,
    studio.id as studio_id,
    studio.name as studio_name
from simulcast
join title_anime on simulcast.title_id = title_anime.title_id
join vw_anime_id_reviewed on title_anime.anime_id = vw_anime_id_reviewed.anime_id
join anime on vw_anime_id_reviewed.anime_id = anime.id
join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
left join anime_genre on anime_genre.anime_id = anime.id
left join genre on anime_genre.genre_id = genre.id
left join anime_studio on anime_studio.anime_id = anime.id
left join studio on anime_studio.studio_id = studio.id
order by
    simulcast.weekday,
    simulcast.hour,
    simulcast.minute,
    simulcast.second;

-- name: TruncateSimulcast :execrows
delete from simulcast;
