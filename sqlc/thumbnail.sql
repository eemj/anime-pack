-- name: InsertThumbnail :one
with ins as (
    insert into thumbnail (name, hash)
    values (sqlc.arg('name'), sqlc.arg('hash'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from thumbnail
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: InsertManyThumbnail :batchmany
with ins as (
    insert into thumbnail (name, hash)
    values (sqlc.arg('name'), sqlc.arg('hash'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from thumbnail
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: CountThumbnail :one
select count(1)
from thumbnail
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash));

-- name: FilterThumbnail :many
select *
from thumbnail
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash));

-- name: FirstThumbnail :one
select *
from thumbnail
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash))
limit 1;

-- name: UpdateThumbnail :execrows
update thumbnail
set
    name = coalesce(sqlc.narg('name'), name),
    hash = coalesce(sqlc.narg('hash'), hash)
where id = sqlc.arg('id');

-- name: DeleteThumbnail :execrows
delete from thumbnail where id = sqlc.arg('id');

-- name: DeleteUnusedThumbnails :execrows
delete from thumbnail where thumbnail.id in (
    select thumbnail.id
    from thumbnail
    left join video on video.thumbnail_id = thumbnail.id
    where video.id is null
);
