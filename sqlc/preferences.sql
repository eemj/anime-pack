-- name: InsertPreferences :one
with ins as (
    insert into preferences (anime_id)
    values (sqlc.arg('anime_id'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from preferences
where anime_id = sqlc.arg('anime_id');

-- name: CountPreferences :one
select count(1)
from preferences
where
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id)) and
    (favourite = coalesce(sqlc.narg('favourite'), favourite)) and
    (automatic_downloads = coalesce(sqlc.narg('automatic_downloads'), automatic_downloads)) and
    (perform_checksum = coalesce(sqlc.narg('perform_checksum'), perform_checksum));

-- name: FilterPreferences :many
select *
from preferences
where
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id)) and
    (favourite = coalesce(sqlc.narg('favourite'), favourite)) and
    (automatic_downloads = coalesce(sqlc.narg('automatic_downloads'), automatic_downloads)) and
    (perform_checksum = coalesce(sqlc.narg('perform_checksum'), perform_checksum));

-- name: FirstPreference :one
select *
from preferences
where
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id)) and
    (favourite = coalesce(sqlc.narg('favourite'), favourite)) and
    (automatic_downloads = coalesce(sqlc.narg('automatic_downloads'), automatic_downloads)) and
    (perform_checksum = coalesce(sqlc.narg('perform_checksum'), perform_checksum))
limit 1;

-- name: UpdatePreferences :execrows
update preferences
set
    favourite = coalesce(sqlc.narg('favourite'), favourite),
    automatic_downloads = coalesce(sqlc.narg('automatic_downloads'), automatic_downloads),
    perform_checksum = coalesce(sqlc.narg('perform_checksum'), perform_checksum)
where anime_id = sqlc.arg('anime_id');

-- name: DeletePreferences :execrows
delete from preferences where anime_id = any(sqlc.arg('anime_ids')::bigint[]);
