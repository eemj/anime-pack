-- name: ListVideoEntries :many
select
    video.id as video_id,
    video.path as video_path,
    video.duration as video_duration,
    video.size as video_size,
    video.crc32 as video_crc32,
    video.thumbnail_id as video_thumbnail_id,
    video.xdcc_id as video_xdcc_id,
    video.quality_id as video_quality_id,
    video.created_at as video_created_at,
    anime.id as anime_id,
    anime.title as anime_title,
    preferences.favourite as preferences_favourite,
    preferences.automatic_downloads as preferences_automatic_downloads,
    preferences.perform_checksum as preferences_perform_checksum,
    episode.name as episode_name,
    quality.height as quality_height,
    release_group.name as release_group_name
from video
join xdcc on video.xdcc_id = xdcc.id
join quality on quality.id = coalesce(xdcc.quality_id, video.quality_id)
join release_group on xdcc.release_group_id = release_group.id
join title_episode on xdcc.title_episode_id = title_episode.id
join episode on title_episode.episode_id = episode.id
join title_anime on title_episode.title_id = title_anime.title_id
join vw_anime_id_reviewed on title_anime.anime_id = vw_anime_id_reviewed.anime_id
join anime on vw_anime_id_reviewed.anime_id = anime.id
join preferences on anime.id = preferences.anime_id;

-- name: InsertVideo :one
insert into video (path, duration, size, crc32, thumbnail_id, xdcc_id, quality_id)
values (
    sqlc.arg('path'),
    sqlc.arg('duration'),
    sqlc.arg('size'),
    sqlc.arg('crc32'),
    sqlc.arg('thumbnail_id'),
    sqlc.arg('xdcc_id'),
    sqlc.narg('quality_id')
)
returning *;

-- name: CountVideo :one
select count(1)
from video
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (duration = coalesce(sqlc.narg('duration'), duration)) and
    (size = coalesce(sqlc.narg('size'), size)) and
    (crc32 = coalesce(sqlc.narg('crc32'), crc32)) and
    (thumbnail_id = coalesce(sqlc.narg('thumbnail_id'), thumbnail_id)) and
    (xdcc_id = coalesce(sqlc.narg('xdcc_id'), xdcc_id)) and
    (quality_id = coalesce(sqlc.narg('quality_id'), quality_id));

-- name: FilterVideo :many
select *
from video
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (duration = coalesce(sqlc.narg('duration'), duration)) and
    (size = coalesce(sqlc.narg('size'), size)) and
    (crc32 = coalesce(sqlc.narg('crc32'), crc32)) and
    (thumbnail_id = coalesce(sqlc.narg('thumbnail_id'), thumbnail_id)) and
    (xdcc_id = coalesce(sqlc.narg('xdcc_id'), xdcc_id)) and
    (quality_id = coalesce(sqlc.narg('quality_id'), quality_id));

-- name: FirstVideo :one
select *
from video
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (duration = coalesce(sqlc.narg('duration'), duration)) and
    (size = coalesce(sqlc.narg('size'), size)) and
    (crc32 = coalesce(sqlc.narg('crc32'), crc32)) and
    (thumbnail_id = coalesce(sqlc.narg('thumbnail_id'), thumbnail_id)) and
    (xdcc_id = coalesce(sqlc.narg('xdcc_id'), xdcc_id)) and
    (quality_id = coalesce(sqlc.narg('quality_id'), quality_id))
limit 1;

-- name: UpdateVideo :execrows
update video
set 
    duration = coalesce(sqlc.narg('duration'), duration),
    size = coalesce(sqlc.narg('size'), size),
    crc32 = coalesce(sqlc.narg('crc32'), crc32),
    thumbnail_id = coalesce(sqlc.narg('thumbnail_id'), thumbnail_id),
    xdcc_id = coalesce(sqlc.narg('xdcc_id'), xdcc_id),
    quality_id = coalesce(sqlc.narg('quality_id'), quality_id)
where id = sqlc.arg('id');

-- name: DeleteVideo :execrows
delete from video where id = sqlc.arg('id');

