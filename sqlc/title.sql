-- name: InsertTitle :one
with ins as (
    insert into title (name, season_number, year, reviewed)
    values (sqlc.arg('name'), sqlc.narg('season_number'), sqlc.narg('year'), sqlc.arg('reviewed'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from title
where
    (name = sqlc.arg('name')) and
    (case when sqlc.narg('season_number')::text is null then season_number is null else season_number = sqlc.narg('season_number')::text end) and
    (case when sqlc.narg('year')::int is null then year is null else year = sqlc.narg('year')::int end)
limit 1;

-- name: InsertManyTitle :batchmany
with ins as (
    insert into title (name, season_number, year, reviewed)
    values (sqlc.arg('name'), sqlc.narg('season_number'), sqlc.narg('year'), sqlc.arg('reviewed'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from title
where
    (name = sqlc.arg('name')) and
    (case when sqlc.narg('season_number')::text is null then season_number is null else season_number = sqlc.narg('season_number')::text end) and
    (case when sqlc.narg('year')::int is null then year is null else year = sqlc.narg('year')::int end)
limit 1;

-- name: CountTitleById :one
select count(1)
from title
where
    (id = sqlc.arg('id')) and
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: CountTitleByNameSeasonNumberYear :one
select count(1)
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted)) and
    (name = sqlc.arg('name')) and
    (case when sqlc.narg('season_number')::text is null then season_number is null else season_number = sqlc.narg('season_number')::text end) and
    (case when sqlc.narg('year')::int is null then year is null else year = sqlc.narg('year')::int end);

-- name: CountTitleByOthers :one
select count(1)
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: FilterTitleById :many
select *
from title
where
    (id = sqlc.arg('id')) and
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: FilterTitleByNameSeasonNumberYear :many
select *
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted)) and
    (name = sqlc.arg('name')) and
    (case when sqlc.narg('season_number')::text is null then season_number is null else season_number = sqlc.narg('season_number')::text end) and
    (case when sqlc.narg('year')::int is null then year is null else year = sqlc.narg('year')::int end);

-- name: FilterTitleByOthers :many
select *
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted));

-- name: FirstTitleById :one
select *
from title
where
    (id = sqlc.arg('id')) and
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted))
limit 1;

-- name: FirstTitleByNameSeasonNumberYear :one
select *
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted)) and
    (name = sqlc.arg('name')) and
    (case when sqlc.narg('season_number')::text is null then season_number is null else season_number = sqlc.narg('season_number')::text end) and
    (case when sqlc.narg('year')::int is null then year is null else year = sqlc.narg('year')::int end)
limit 1;

-- name: FirstTitleByOthers :one
select *
from title
where
    (reviewed = coalesce(sqlc.narg('reviewed'), reviewed)) and
    (is_deleted = coalesce(sqlc.narg('is_deleted'), is_deleted))
limit 1;

-- name: UpdateTitle :execrows
update title
set
    name = coalesce(sqlc.narg('name'), name),
    reviewed = coalesce(sqlc.narg('reviewed'), reviewed),
    season_number = (case when sqlc.narg('season_number')::text is not null then sqlc.narg('season_number')::text else season_number end),
    year = (case when sqlc.narg('year')::int is not null then sqlc.narg('year')::int else year end)
where id = sqlc.arg('id');

-- name: SoftDeleteTitle :execrows
update title
set deleted_at = timezone('utc'::text, now())
where
    (id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null);

-- name: DeleteTitle :execrows
delete from title where id = sqlc.arg('id');
