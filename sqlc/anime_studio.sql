-- name: InsertAnimeStudio :batchmany
with ins as (
    insert into anime_studio (anime_id, studio_id)
    values (sqlc.arg('anime_id'), sqlc.arg('studio_id'))
    on conflict on constraint anime_studio_pkey do nothing
    returning *
)
select *
from ins
union all
select *
from anime_studio
where anime_id = sqlc.arg('anime_id') and
    studio_id = sqlc.arg('studio_id');

-- name: CountAnimeStudio :one
select count(1)
from anime_studio
where
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id)) and
    (studio_id = coalesce(sqlc.narg('studio_id'), studio_id));

-- name: FirstAnimeStudio :one
select
    anime_studio.anime_id,
    anime_studio.studio_id,
    studio.name as studio_name
from anime_studio
join studio on anime_studio.studio_id = studio.id
where
    (anime_studio.anime_id = coalesce(sqlc.narg('anime_id'), anime_studio.anime_id)) and
    (anime_studio.studio_id = coalesce(sqlc.narg('studio_id'), anime_studio.studio_id))
limit 1;

-- name: FilterAnimeStudio :many
select
    anime_studio.anime_id,
    anime_studio.studio_id,
    studio.name as studio_name
from anime_studio
join studio on anime_studio.studio_id = studio.id
where
    (anime_studio.anime_id = coalesce(sqlc.narg('anime_id'), anime_studio.anime_id)) and
    (anime_studio.studio_id = coalesce(sqlc.narg('studio_id'), anime_studio.studio_id));

-- name: DeleteAnimeStudio :execrows
delete from anime_studio
where
    (anime_id = sqlc.arg('anime_id')) and
    (studio_id = coalesce(sqlc.narg('studio_id'), studio_id));
