-- name: InsertQuality :one
with ins as (
    insert into quality (height)
    values (sqlc.arg('height')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from quality
where height = sqlc.arg('height')
limit 1;

-- name: InsertManyQuality :batchmany
with ins as (
    insert into quality (height)
    values (sqlc.arg('height')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from quality
where height = sqlc.arg('height')
limit 1;

-- name: CountQuality :one
select count(1)
from quality
where
    (height = coalesce(sqlc.narg('height'), height)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterQuality :many
select *
from quality
where
    (height = coalesce(sqlc.narg('height'), height)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstQuality :one
select *
from quality
where
    (height = coalesce(sqlc.narg('height'), height)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateQuality :execrows
update quality
set height = coalesce(sqlc.narg('height'), height)
where id = sqlc.arg('id');

-- name: DeleteQuality :execrows
delete from quality where id = any(sqlc.arg('ids')::bigint[]);
