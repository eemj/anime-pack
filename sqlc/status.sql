-- name: InsertStatus :batchmany
with ins as (
    insert into genre (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from status
where name = sqlc.arg('name')
limit 1;

-- name: CountStatus :one
select count(1)
from status
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterStatus :many
select *
from status
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstStatus :one
select *
from status
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateStatus :execrows
update status
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteStatus :execrows
delete from status where id = any(sqlc.arg('ids')::bigint[]);