-- name: CountAnimeLatest :one
select count(1)
from vw_xdcc_title_episode as title_episode
         join title on title_episode.title_id = title.id and not title.is_deleted and title.reviewed
         join title_anime on title_anime.title_id = title.id
         join anime on title_anime.anime_id = anime.id
         left join preferences on anime.id = preferences.anime_id
where
  -- Filters
    (anime.status = any (sqlc.narg('statuses')::anime_status[]) or array_length(sqlc.narg('statuses')::anime_status[], 1) is null)
  and (anime.format = any (sqlc.narg('formats')::anime_format[]) or array_length(sqlc.narg('formats')::anime_format[], 1) is null)
  and (preferences.favourite = case when sqlc.narg('favourite')::bool is null then preferences.favourite else sqlc.narg('favourite')::bool end);

-- name: FilterAnimeLatest :many
select anime.id                 as anime_id,
       anime.id_mal             as anime_id_mal,
       anime.title              as anime_title,
       anime.title_english      as anime_title_english,
       anime.title_native       as anime_title_native,
       anime.title_romaji       as anime_title_romaji,
       anime.colour             as anime_colour,
       anime.format             as anime_format,
       anime.status             as anime_status,
       poster.id                as poster_id,
       poster.name              as poster_name,
       poster.hash              as poster_hash,
       episode.name             as episode_name,
       title_episode.id         as title_episode_id,
       title_episode.created_at as title_episode_created_at,
       preferences.favourite    as preferences_favourite
from vw_xdcc_title_episode as title_episode
         join title on title_episode.title_id = title.id and not title.is_deleted and title.reviewed
         join episode on title_episode.episode_id = episode.id
         join title_anime on title.id = title_anime.title_id
         join anime on title_anime.anime_id = anime.id
         join poster on anime.poster_id = poster.id
         left join preferences on anime.id = preferences.anime_id
where
  -- Filters
    (anime.status = any (sqlc.narg('statuses')::anime_status[]) or array_length(sqlc.narg('statuses')::anime_status[], 1) is null)
  and (anime.format = any (sqlc.narg('formats')::anime_format[]) or array_length(sqlc.narg('formats')::anime_format[], 1) is null)
  and (preferences.favourite = case when sqlc.narg('favourite')::bool is null then preferences.favourite else sqlc.narg('favourite')::bool end)
order by case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = true) then anime.id end desc,
         case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = false) then anime.id end asc,
         case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = true) then title_episode.created_at end desc,
         case when (sqlc.arg('order_by')::text = 'createdAt' and sqlc.arg('order_by_desc')::bool = false) then title_episode.created_at end asc,
         case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = true) then anime.score end desc,
         case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = false) then anime.score end asc
limit sqlc.arg('limit')::bigint
offset sqlc.arg('offset')::bigint;