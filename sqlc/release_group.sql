-- name: InsertReleaseGroup :one
with ins as (
    insert into release_group (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from release_group
where name = sqlc.arg('name')
limit 1;

-- name: InsertManyReleaseGroup :batchmany
with ins as (
    insert into release_group (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from release_group
where name = sqlc.arg('name')
limit 1;

-- name: CountReleaseGroup :one
select count(1)
from release_group
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterReleaseGroup :many
select *
from release_group
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
order by name desc;

-- name: AvailableReleaseGroups :many
select distinct release_group.id, release_group.name
from xdcc
join release_group on xdcc.release_group_id = release_group.id
where xdcc.is_deleted = false
order by release_group.name;

-- name: FirstReleaseGroup :one
select *
from release_group
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateReleaseGroup :execrows
update release_group
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteReleaseGroup :execrows
delete from release_group where id = any(sqlc.arg('ids')::bigint[]);
