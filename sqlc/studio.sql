-- name: InsertStudio :batchmany
with ins as (
    insert into studio (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from studio
where name = sqlc.arg('name')
limit 1;

-- name: CountStudio :one
select count(1)
from studio
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterStudio :many
select *
from studio
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstStudio :one
select *
from studio
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateStudio :execrows
update studio
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteStudio :execrows
delete from studio where id = any(sqlc.arg('ids')::bigint[]);
