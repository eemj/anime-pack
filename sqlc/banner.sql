-- name: InsertBanner :one
with ins as (
    insert into banner (name, hash, uri)
    values (sqlc.arg('name'), sqlc.arg('hash'), sqlc.arg('uri'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from banner
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: InsertManyBanner :batchmany
with ins as (
    insert into banner (name, hash, uri)
    values (sqlc.arg('name'), sqlc.arg('hash'), sqlc.arg('uri'))
    on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from banner
where name = sqlc.arg('name') and hash = sqlc.arg('hash')
limit 1;

-- name: CountBanner :one
select count(1)
from banner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri));

-- name: FilterBanner :many
select *
from banner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri));

-- name: FirstBanner :one
select *
from banner
where
    (id = coalesce(sqlc.narg('id'), id)) and
    (name = coalesce(sqlc.narg('name'), name)) and
    (hash = coalesce(sqlc.narg('hash'), hash)) and
    (uri = coalesce(sqlc.narg('uri'), uri))
limit 1;

-- name: UpdateBanner :execrows
update banner
set
    name = coalesce(sqlc.narg('name'), name),
    hash = coalesce(sqlc.narg('hash'), hash),
    uri = coalesce(sqlc.narg('uri'), uri)
where id = sqlc.arg('id');

-- name: DeleteBanner :execrows
delete from banner where id = sqlc.arg('id');

-- name: DeleteUnusedBanners :execrows
delete from banner where banner.id in (
    select banner.id
    from banner
    left join anime on anime.banner_id = banner.id
    where anime.id is null
);
