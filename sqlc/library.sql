-- name: FilterLibrary :many
select
    xdcc.id as xdcc_id,
    anime.id as anime_id,
    title_episode.id as title_episode_id,
    release_group.name as release_group_name,
    quality.height as quality_height,
    anime.title AS anime_title,
    episode.name AS episode_name
from xdcc
join title_episode on xdcc.title_episode_id = title_episode.id
join title_anime on title_episode.title_id = title_anime.title_id
join anime on title_anime.anime_id = anime.id
join episode on title_episode.episode_id = episode.id
join release_group on xdcc.release_group_id = release_group.id
left join video on xdcc.id = video.xdcc_id
join quality on quality.id = coalesce(video.quality_id, xdcc.quality_id)
where
    (video.path = coalesce(sqlc.narg('video_path'), video.path)) and
    (video.id = coalesce(sqlc.narg('video_id'), video.id)) and
    (xdcc.id = coalesce(sqlc.narg('xdcc_id'), xdcc.id));

-- name: ListReviewedLibrary :many
select
    title.id as title_id,
    title.name as title_name,
    title.reviewed as title_reviewed,
    title.created_at as title_created_at,
    title.updated_at as title_updated_at,
    title.deleted_at as title_deleted_at,
    title.season_number as title_season_number,
    title.year as title_year,
    anime.id as anime_id
from title
join title_anime on title_anime.title_id = title.id
join anime on title_anime.anime_id = anime.id
where title.reviewed = true;

-- name: BestAnimeQualityPerReleaseGroup :many
with release_group_quality as (
    select
        release_group.name as release_group_name,
        max(quality.height) as quality_height
    from xdcc
    join release_group on xdcc.release_group_id = release_group.id
    join quality on xdcc.quality_id = quality.id
    where
        xdcc.is_deleted = false and
        xdcc.title_episode_id in (
            select title_episode.id
            from title_episode
            where
                title_episode.is_deleted = false and
                title_episode.title_id in (
                    select title_anime.title_id
                    from title_anime
                    join title on title_anime.title_id = title.id
                    where
                        title.is_deleted = false and
                        title.reviewed = true and
                        title_anime.anime_id = sqlc.arg('anime_id')
                )
        )
    group by release_group.name
)
select
    release_group.id as release_group_id,
    release_group.name as release_group_name,
    quality.id as quality_id,
    quality.height as quality_height
from release_group_quality
join release_group on release_group_quality.release_group_name = release_group.name
left join quality on release_group_quality.quality_height = quality.height;
