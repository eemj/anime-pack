-- name: InsertAnime :one
with ins as (
    insert into anime (
        id,
        id_mal,
        start_date,
        end_date,
        score,
        description,
        title_romaji,
        title_english,
        title_native,
        title,
        format,
        poster_id,
        banner_id,
        colour,
        season,
        year,
        status,
        next_airing_date
    )
    values (
        sqlc.arg('id'),
        sqlc.narg('id_mal'),
        sqlc.narg('start_date'),
        sqlc.narg('end_date'),
        sqlc.arg('score'),
        sqlc.arg('description'),
        sqlc.narg('title_romaji'),
        sqlc.narg('title_english'),
        sqlc.narg('title_native'),
        sqlc.arg('title'),
        sqlc.arg('format')::anime_format,
        sqlc.arg('poster_id'),
        sqlc.narg('banner_id'),
        sqlc.narg('colour'),
        sqlc.narg('season')::anime_season,
        sqlc.narg('year'),
        sqlc.arg('status')::anime_status,
        sqlc.narg('next_airing_date')
    ) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from anime
where id = sqlc.arg('id')
limit 1;

-- name: CountAnime :one
select count(1)
from anime
where
    (anime.id = coalesce(sqlc.narg('id'), anime.id)) and
    (anime.id_mal = coalesce(sqlc.narg('id_mal'), anime.id_mal)) and
    (anime.start_date = coalesce(sqlc.narg('start_date'), anime.start_date)) and
    (anime.end_date = coalesce(sqlc.narg('end_date'), anime.end_date)) and
    (anime.score = coalesce(sqlc.narg('score'), anime.score)) and
    (anime.description = coalesce(sqlc.narg('description'), anime.description)) and
    (anime.title_romaji = coalesce(sqlc.narg('title_romaji'), anime.title_romaji)) and
    (anime.title_english = coalesce(sqlc.narg('title_english'), anime.title_english)) and
    (anime.title_native = coalesce(sqlc.narg('title_native'), anime.title_native)) and
    (anime.title = coalesce(sqlc.narg('title'), anime.title)) and
    (anime.format = coalesce(sqlc.narg('format'), anime.format)) and
    (anime.poster_id = coalesce(sqlc.narg('poster'), anime.poster_id)) and
    (anime.banner_id = coalesce(sqlc.narg('banner'), anime.banner_id)) and
    (anime.colour = coalesce(sqlc.narg('colour'), anime.colour)) and
    (anime.status = coalesce(sqlc.narg('status'), anime.status)) and
    (anime.year = coalesce(sqlc.narg('year'), anime.year)) and
    (anime.season = coalesce(sqlc.narg('season'), anime.season)) and
    (anime.next_airing_date = coalesce(sqlc.narg('next_airing_date'), anime.next_airing_date));

-- name: FilterAnime :many
with filtered_anime as (
    select
        anime.id as anime_id,
        anime.id_mal as anime_id_mal,
        anime.start_date as anime_start_date,
        anime.end_date as anime_end_date,
        anime.score as anime_score,
        anime.description as anime_description,
        anime.title_romaji as anime_title_romaji,
        anime.title_english as anime_title_english,
        anime.title_native as anime_title_native,
        anime.title as anime_title,
        anime.poster_id as anime_poster_id,
        anime.banner_id as anime_banner_id,
        anime.colour as anime_colour,
        anime.year as anime_year,
        anime.next_airing_date as anime_next_airing_date,
        anime.created_at as anime_created_at,
        anime.updated_at as anime_updated_at,
        anime.status as anime_status,
        anime.format as anime_format,
        anime.season as anime_season,
        poster.name as poster_name,
        poster.hash as poster_hash,
        poster.uri as poster_uri,
        banner.name as banner_name,
        banner.hash as banner_hash,
        banner.uri as banner_uri,
        preferences.anime_id as preferences_anime_id,
        preferences.favourite as preferences_favourite,
        preferences.automatic_downloads as preferences_automatic_downloads,
        preferences.perform_checksum as preferences_perform_checksum
    from anime
    join poster on anime.poster_id = poster.id
    left join banner on anime.banner_id = banner.id
    left join preferences on preferences.anime_id = anime.id
    where
        (anime.id = coalesce(sqlc.narg('id'), anime.id)) and
        (anime.id_mal = coalesce(sqlc.narg('id_mal'), anime.id_mal)) and
        (anime.start_date = coalesce(sqlc.narg('start_date'), anime.start_date)) and
        (anime.end_date = coalesce(sqlc.narg('end_date'), anime.end_date)) and
        (anime.score = coalesce(sqlc.narg('score'), anime.score)) and
        (anime.description = coalesce(sqlc.narg('description'), anime.description)) and
        (anime.title_romaji = coalesce(sqlc.narg('title_romaji'), anime.title_romaji)) and
        (anime.title_english = coalesce(sqlc.narg('title_english'), anime.title_english)) and
        (anime.title_native = coalesce(sqlc.narg('title_native'), anime.title_native)) and
        (anime.title = coalesce(sqlc.narg('title'), anime.title)) and
        (anime.format = coalesce(sqlc.narg('format'), anime.format)) and
        (anime.poster_id = coalesce(sqlc.narg('poster'), anime.poster_id)) and
        (anime.banner_id = coalesce(sqlc.narg('banner'), anime.banner_id)) and
        (anime.colour = coalesce(sqlc.narg('colour'), anime.colour)) and
        (anime.status = coalesce(sqlc.narg('status'), anime.status)) and
        (anime.year = coalesce(sqlc.narg('year'), anime.year)) and
        (anime.season = coalesce(sqlc.narg('season'), anime.season)) and
        (anime.next_airing_date = coalesce(sqlc.narg('next_airing_date'), anime.next_airing_date))
    limit sqlc.arg('limit')::bigint
    offset sqlc.arg('offset')::bigint
)
select
    filtered_anime.anime_id,
    filtered_anime.anime_id_mal,
    filtered_anime.anime_start_date,
    filtered_anime.anime_end_date,
    filtered_anime.anime_score,
    filtered_anime.anime_description,
    filtered_anime.anime_title_romaji,
    filtered_anime.anime_title_english,
    filtered_anime.anime_title_native,
    filtered_anime.anime_title,
    filtered_anime.anime_poster_id,
    filtered_anime.anime_banner_id,
    filtered_anime.anime_colour,
    filtered_anime.anime_year,
    filtered_anime.anime_next_airing_date,
    filtered_anime.anime_created_at,
    filtered_anime.anime_updated_at,
    filtered_anime.anime_status,
    filtered_anime.anime_format,
    filtered_anime.anime_season,
    filtered_anime.poster_name,
    filtered_anime.poster_hash,
    filtered_anime.poster_uri,
    filtered_anime.banner_name,
    filtered_anime.banner_hash,
    filtered_anime.banner_uri,
    filtered_anime.preferences_anime_id,
    filtered_anime.preferences_favourite,
    filtered_anime.preferences_automatic_downloads,
    filtered_anime.preferences_perform_checksum,
    genre.id as genre_id,
    genre.name as genre_name,
    studio.id as studio_id,
    studio.name as studio_name
from filtered_anime
left join anime_genre on anime_genre.anime_id = filtered_anime.anime_id
left join genre on anime_genre.genre_id = genre.id
left join anime_studio on anime_studio.anime_id = filtered_anime.anime_id
left join studio on anime_studio.studio_id = studio.id
order by
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_id end desc,
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_id end asc,

    case when (sqlc.arg('order_by')::text = 'title' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_title end desc,
    case when (sqlc.arg('order_by')::text = 'title' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_title end asc,

    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_score end desc,
    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_score end asc,

    case when (sqlc.arg('order_by')::text = 'nextAiringDate' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_next_airing_date end desc,
    case when (sqlc.arg('order_by')::text = 'nextAiringDate' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_next_airing_date end asc,

    case when (sqlc.arg('order_by')::text = 'startDate' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_start_date end desc,
    case when (sqlc.arg('order_by')::text = 'startDate' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_start_date end asc;

-- name: FirstAnime :one
select *
from anime
where
    (anime.id = coalesce(sqlc.narg('id'), anime.id)) and
    (anime.id_mal = coalesce(sqlc.narg('id_mal'), anime.id_mal)) and
    (anime.start_date = coalesce(sqlc.narg('start_date'), anime.start_date)) and
    (anime.end_date = coalesce(sqlc.narg('end_date'), anime.end_date)) and
    (anime.score = coalesce(sqlc.narg('score'), anime.score)) and
    (anime.description = coalesce(sqlc.narg('description'), anime.description)) and
    (anime.title_romaji = coalesce(sqlc.narg('title_romaji'), anime.title_romaji)) and
    (anime.title_english = coalesce(sqlc.narg('title_english'), anime.title_english)) and
    (anime.title_native = coalesce(sqlc.narg('title_native'), anime.title_native)) and
    (anime.title = coalesce(sqlc.narg('title'), anime.title)) and
    (anime.format = coalesce(sqlc.narg('format'), anime.format)) and
    (anime.poster_id = coalesce(sqlc.narg('poster'), anime.poster_id)) and
    (anime.banner_id = coalesce(sqlc.narg('banner'), anime.banner_id)) and
    (anime.colour = coalesce(sqlc.narg('colour'), anime.colour)) and
    (anime.status = coalesce(sqlc.narg('status'), anime.status)) and
    (anime.year = coalesce(sqlc.narg('year'), anime.year)) and
    (anime.season = coalesce(sqlc.narg('season'), anime.season)) and
    (anime.next_airing_date = coalesce(sqlc.narg('next_airing_date'), anime.next_airing_date))
limit 1;

-- name: UpdateAnime :execrows
update anime
set id_mal = coalesce(sqlc.narg('id_mal'), id_mal),
    start_date = coalesce(sqlc.narg('start_date'), start_date),
    end_date = coalesce(sqlc.narg('end_date'), end_date),
    score = coalesce(sqlc.narg('score'), score),
    description = coalesce(sqlc.narg('description'), description),
    title_romaji = coalesce(sqlc.narg('title_romaji'), title_romaji),
    title_english = coalesce(sqlc.narg('title_english'), title_english),
    title_native = coalesce(sqlc.narg('title_native'), title_native),
    title = coalesce(sqlc.narg('title'), title),
    format = coalesce(sqlc.narg('format'), format),
    poster_id = coalesce(sqlc.narg('poster_id'), poster_id),
    banner_id = coalesce(sqlc.narg('banner_id'), banner_id),
    colour = coalesce(sqlc.narg('colour'), colour),
    season = coalesce(sqlc.narg('season'), season),
    year = coalesce(sqlc.narg('year'), year),
    status = coalesce(sqlc.narg('status'), status),
    next_airing_date = coalesce(sqlc.narg('next_airing_date'), next_airing_date)
where id = sqlc.arg('id');

-- name: DeleteAnime :execrows
delete from anime where id = sqlc.arg('id');
