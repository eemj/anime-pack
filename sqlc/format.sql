-- name: InsertFormat :batchmany
with ins as (
    insert into genre (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from format
where name = sqlc.arg('name')
limit 1;

-- name: CountFormat :one
select count(1)
from format
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterFormat :many
select *
from format
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstFormat :one
select *
from format
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateFormat :execrows
update format
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteFormat :execrows
delete from format where id = any(sqlc.arg('ids')::bigint[]);
