-- name: InsertEpisode :one
with ins as (
    insert into episode (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from episode
where name = sqlc.arg('name')
limit 1;

-- name: InsertManyEpisode :batchmany
with ins as (
    insert into episode (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from episode
where name = sqlc.arg('name')
limit 1;

-- name: CountEpisode :one
select count(1)
from episode
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterEpisode :many
select *
from episode
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstEpisode :one
select *
from episode
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateEpisode :execrows
update episode
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteEpisode :execrows
delete from episode where id = any(sqlc.arg('ids')::bigint[]);
