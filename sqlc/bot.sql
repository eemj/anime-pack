-- name: InsertBot :one
with ins as (
    insert into bot (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from bot
where name = sqlc.arg('name')
limit 1;

-- name: InsertManyBot :batchmany
with ins as (
    insert into bot (name)
    values (sqlc.arg('name')) on conflict do nothing
    returning *
)
select *
from ins
union all
select *
from bot
where name = sqlc.arg('name')
limit 1;

-- name: CountBot :one
select count(1)
from bot
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FilterBot :many
select *
from bot
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id));

-- name: FirstBot :one
select *
from bot
where
    (name = coalesce(sqlc.narg('name'), name)) and
    (id = coalesce(sqlc.narg('id'), id))
limit 1;

-- name: UpdateBot :execrows
update bot
set name = coalesce(sqlc.narg('name'), name)
where id = sqlc.arg('id');

-- name: DeleteBot :execrows
delete from bot where id = any(sqlc.arg('ids')::bigint[]);
