-- name: ListAnimeIndexes :many
select
distinct ascii(upper(substring(title from 1 for 1))) as "ascii"
from vw_anime_id_reviewed
join anime on vw_anime_id_reviewed.anime_id = anime.id
order by 1 asc;

-- name: ListAnimeByIndex :many
select
    anime.id as anime_id,
    anime.id_mal as anime_id_mal,
    anime.start_date as anime_start_date,
    anime.end_date as anime_end_date,
    anime.score as anime_score,
    anime.description as anime_description,
    anime.title_romaji as anime_title_romaji,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title as anime_title,
    anime.poster_id as anime_poster_id,
    anime.banner_id as anime_banner_id,
    anime.colour as anime_colour,
    anime.year as anime_year,
    anime.next_airing_date as anime_next_airing_date,
    anime.created_at as anime_created_at,
    anime.updated_at as anime_updated_at,
    anime.status as anime_status,
    anime.format as anime_format,
    anime.season as anime_season,
    poster.name as poster_name,
    poster.hash as poster_hash,
    poster.uri as poster_uri,
    banner.name as banner_name,
    banner.hash as banner_hash,
    banner.uri as banner_uri
from vw_anime_id_reviewed
join anime on vw_anime_id_reviewed.anime_id = anime.id
join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
where
    (case
        when substring(title from 1 for 1) ~* '^[a-z]' then upper(substring(title from 1 for 1))
        else '#'
    end) = sqlc.arg('index')::char
order by anime.title asc;
