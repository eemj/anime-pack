-- name: InsertAnimeGenre :batchmany
with ins as (
    insert into anime_genre (anime_id, genre_id)
    values (sqlc.arg('anime_id'), sqlc.arg('genre_id'))
    on conflict on constraint anime_genre_pkey do nothing
    returning *
)
select *
from ins
union all
select *
from anime_genre
where anime_id = sqlc.arg('anime_id') and
    genre_id = sqlc.arg('genre_id');

-- name: CountAnimeGenre :one
select count(1)
from anime_genre
where
    (anime_id = coalesce(sqlc.narg('anime_id'), anime_id)) and
    (genre_id = coalesce(sqlc.narg('genre_id'), genre_id));

-- name: FirstAnimeGenre :one
select
    anime_genre.anime_id,
    anime_genre.genre_id,
    genre.name as genre_name
from anime_genre
join genre on anime_genre.anime_id = genre.id
where
    (anime_genre.anime_id = coalesce(sqlc.narg('anime_id'), anime_genre.anime_id)) and
    (anime_genre.genre_id = coalesce(sqlc.narg('genre_id'), anime_genre.genre_id))
limit 1;

-- name: FilterAnimeGenre :many
select
    anime_genre.anime_id,
    anime_genre.genre_id,
    genre.name as genre_name
from anime_genre
join genre on anime_genre.anime_id = genre.id
where
    (anime_genre.anime_id = coalesce(sqlc.narg('anime_id'), anime_genre.anime_id)) and
    (anime_genre.genre_id = coalesce(sqlc.narg('genre_id'), anime_genre.genre_id));

-- name: DeleteAnimeGenre :execrows
delete from anime_genre
where
    (anime_id = sqlc.arg('anime_id')) and
    (genre_id = coalesce(sqlc.narg('genre_id'), genre_id));
