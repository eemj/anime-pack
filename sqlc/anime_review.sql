-- name: ListAnimeReviewStatus :many
select distinct anime.status
from vw_anime_id_reviewed
inner join anime on vw_anime_id_reviewed.anime_id = anime.id
where anime.status <> 'UNDEFINED'::anime_status
order by 1;

-- name: ListAnimeReviewFormat :many
select distinct anime.format
from vw_anime_id_reviewed
inner join anime on vw_anime_id_reviewed.anime_id = anime.id
where anime.format <> 'UNDEFINED'::anime_format
order by 1;

-- name: ListAnimeReviewSeason :many
select distinct anime.season
from vw_anime_id_reviewed
inner join anime on vw_anime_id_reviewed.anime_id = anime.id
where season is not null and season <> 'UNDEFINED'::anime_season
order by 1;

-- name: ListAnimeReviewSearch :many
select ar.anime_id, t.title::text as title
from vw_anime_id_reviewed ar
cross join lateral (
  select distinct titles.title
  from (
    select unnest(array[a.title_romaji, a.title_native, a.title_english, a.title]) as title
    from anime a
    where ar.anime_id = a.id
  ) titles
  where titles.title is not null and titles.title <> ''
) as t
order by t.title;

-- name: ListAnimeReviewSeasonYear :many
select distinct anime.year
from vw_anime_id_reviewed
inner join anime on vw_anime_id_reviewed.anime_id = anime.id
where anime.year is not null and anime.year > 0
order by 1;

-- name: ListAnimeReviewGenre :many
select distinct genre.*
from anime_genre
join genre on anime_genre.genre_id = genre.id
join vw_anime_id_reviewed on vw_anime_id_reviewed.anime_id = anime_genre.anime_id
order by 1;

-- name: ListAnimeReviewStudio :many
select distinct studio.*
from anime_studio
join studio on anime_studio.studio_id = studio.id
join vw_anime_id_reviewed on vw_anime_id_reviewed.anime_id = anime_studio.anime_id
order by 1;

-- name: CountAnimeReview :one
select coalesce(sum(1), 0)::bigint as count
from (
    select count(anime.id)
    from vw_anime_id_reviewed
    join anime on vw_anime_id_reviewed.anime_id = anime.id
    left join anime_genre on vw_anime_id_reviewed.anime_id = anime_genre.genre_id
    left join anime_studio on vw_anime_id_reviewed.anime_id = anime_studio.studio_id
    left join preferences on vw_anime_id_reviewed.anime_id = preferences.anime_id
    where
        (anime.id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
        (anime.status = any(sqlc.narg('statuses')::anime_status[]) or array_length(sqlc.narg('statuses')::anime_status[], 1) is null) and
        (anime.format = any(sqlc.narg('formats')::anime_format[]) or array_length(sqlc.narg('formats')::anime_format[], 1) is null) and
        (anime.season = any(sqlc.narg('seasons')::anime_season[]) or array_length(sqlc.narg('seasons')::anime_season[], 1) is null) and
        (anime.year = case when sqlc.narg('season_year')::bigint is null then anime.year else sqlc.narg('season_year')::bigint end) and
        (anime_genre.genre_id = any(sqlc.narg('genre_ids')::bigint[]) or array_length(sqlc.narg('genre_ids')::bigint[], 1) is null) and
        (anime_studio.studio_id = any(sqlc.narg('studio_ids')::bigint[]) or array_length(sqlc.narg('studio_ids')::bigint[], 1) is null) and
        (preferences.favourite = coalesce(sqlc.narg('favourite'), preferences.favourite))
    group by anime.id
) count_anime;

-- name: FilterAnimeReview :many
with filtered_anime as (
    select
        anime.id as anime_id,
        anime.id_mal as anime_id_mal,
        anime.start_date as anime_start_date,
        anime.end_date as anime_end_date,
        anime.score as anime_score,
        anime.description as anime_description,
        anime.title_romaji as anime_title_romaji,
        anime.title_english as anime_title_english,
        anime.title_native as anime_title_native,
        anime.title as anime_title,
        anime.poster_id as anime_poster_id,
        anime.banner_id as anime_banner_id,
        anime.colour as anime_colour,
        anime.year as anime_year,
        anime.next_airing_date as anime_next_airing_date,
        anime.created_at as anime_created_at,
        anime.updated_at as anime_updated_at,
        anime.status as anime_status,
        anime.format as anime_format,
        anime.season as anime_season,
        preferences.anime_id as preferences_anime_id,
        preferences.favourite as preferences_favourite,
        preferences.automatic_downloads as preferences_automatic_downloads,
        preferences.perform_checksum as preferences_perform_checksum
    from vw_anime_id_reviewed
    join anime on vw_anime_id_reviewed.anime_id = anime.id
    left join anime_genre on vw_anime_id_reviewed.anime_id = anime_genre.anime_id
    left join anime_studio on vw_anime_id_reviewed.anime_id = anime_studio.anime_id
    left join preferences on preferences.anime_id = anime.id
    where
        (anime.id = any(sqlc.narg('ids')::bigint[]) or array_length(sqlc.narg('ids')::bigint[], 1) is null) and
        (anime.status = any(sqlc.narg('statuses')::anime_status[]) or array_length(sqlc.narg('statuses')::anime_status[], 1) is null) and
        (anime.format = any(sqlc.narg('formats')::anime_format[]) or array_length(sqlc.narg('formats')::anime_format[], 1) is null) and
        (anime.season = any(sqlc.narg('seasons')::anime_season[]) or array_length(sqlc.narg('seasons')::anime_season[], 1) is null) and
        (anime.year = case when sqlc.narg('season_year')::bigint is null then anime.year else sqlc.narg('season_year')::bigint end) and
        (anime_genre.genre_id = any(sqlc.narg('genre_ids')::bigint[]) or array_length(sqlc.narg('genre_ids')::bigint[], 1) is null) and
        (anime_studio.studio_id = any(sqlc.narg('studio_ids')::bigint[]) or array_length(sqlc.narg('studio_ids')::bigint[], 1) is null) and
        (preferences.favourite = coalesce(sqlc.narg('favourite'), preferences.favourite))
    group by
        anime.id,
        anime.id_mal,
        anime.start_date,
        anime.end_date,
        anime.score,
        anime.description,
        anime.title_romaji,
        anime.title_english,
        anime.title_native,
        anime.title,
        anime.poster_id,
        anime.banner_id,
        anime.colour,
        anime.year,
        anime.next_airing_date,
        anime.created_at,
        anime.updated_at,
        anime.status,
        anime.format,
        anime.season,
        preferences.anime_id,
        preferences.favourite,
        preferences.automatic_downloads,
        preferences.perform_checksum
    limit sqlc.arg('limit')::bigint
    offset sqlc.arg('offset')::bigint
)
select
    filtered_anime.anime_id,
    filtered_anime.anime_id_mal,
    filtered_anime.anime_start_date,
    filtered_anime.anime_end_date,
    filtered_anime.anime_score,
    filtered_anime.anime_description,
    filtered_anime.anime_title_romaji,
    filtered_anime.anime_title_english,
    filtered_anime.anime_title_native,
    filtered_anime.anime_title,
    filtered_anime.anime_poster_id,
    filtered_anime.anime_banner_id,
    filtered_anime.anime_colour,
    filtered_anime.anime_year,
    filtered_anime.anime_next_airing_date,
    filtered_anime.anime_created_at,
    filtered_anime.anime_updated_at,
    filtered_anime.anime_status,
    filtered_anime.anime_format,
    filtered_anime.anime_season,
    poster.name as poster_name,
    poster.hash as poster_hash,
    poster.uri as poster_uri,
    banner.name as banner_name,
    banner.hash as banner_hash,
    banner.uri as banner_uri,
    filtered_anime.preferences_anime_id,
    filtered_anime.preferences_favourite,
    filtered_anime.preferences_automatic_downloads,
    filtered_anime.preferences_perform_checksum,
    genre.id as genre_id,
    genre.name as genre_name,
    studio.id as studio_id,
    studio.name as studio_name
from filtered_anime
join poster on filtered_anime.anime_poster_id = poster.id
left join banner on filtered_anime.anime_banner_id = banner.id
left join anime_genre on anime_genre.anime_id = filtered_anime.anime_id
left join genre on anime_genre.genre_id = genre.id
left join anime_studio on anime_studio.anime_id = filtered_anime.anime_id
left join studio on anime_studio.studio_id = studio.id
order by
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_id end desc,
    case when (sqlc.arg('order_by')::text = 'id' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_id end asc,

    case when (sqlc.arg('order_by')::text = 'title' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_title end desc,
    case when (sqlc.arg('order_by')::text = 'title' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_title end asc,

    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_score end desc,
    case when (sqlc.arg('order_by')::text = 'score' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_score end asc,

    case when (sqlc.arg('order_by')::text = 'nextAiringDate' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_next_airing_date end desc,
    case when (sqlc.arg('order_by')::text = 'nextAiringDate' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_next_airing_date end asc,

    case when (sqlc.arg('order_by')::text = 'startDate' and sqlc.arg('order_by_desc')::bool = true) then filtered_anime.anime_start_date end desc,
    case when (sqlc.arg('order_by')::text = 'startDate' and sqlc.arg('order_by_desc')::bool = false) then filtered_anime.anime_start_date end asc;

-- name: FirstAnimeReview :many
select
    anime.id as anime_id,
    anime.id_mal as anime_id_mal,
    anime.start_date as anime_start_date,
    anime.end_date as anime_end_date,
    anime.score as anime_score,
    anime.description as anime_description,
    anime.title_romaji as anime_title_romaji,
    anime.title_english as anime_title_english,
    anime.title_native as anime_title_native,
    anime.title as anime_title,
    anime.poster_id as anime_poster_id,
    anime.banner_id as anime_banner_id,
    anime.colour as anime_colour,
    anime.year as anime_year,
    anime.next_airing_date as anime_next_airing_date,
    anime.created_at as anime_created_at,
    anime.updated_at as anime_updated_at,
    anime.status as anime_status,
    anime.format as anime_format,
    anime.season as anime_season,
    poster.name as poster_name,
    poster.hash as poster_hash,
    poster.uri as poster_uri,
    banner.name as banner_name,
    banner.hash as banner_hash,
    banner.uri as banner_uri,
    preferences.anime_id as preferences_anime_id,
    preferences.favourite as preferences_favourite,
    preferences.automatic_downloads as preferences_automatic_downloads,
    preferences.perform_checksum as preferences_perform_checksum,
    genre.id as genre_id,
    genre.name as genre_name,
    studio.id as studio_id,
    studio.name as studio_name
from vw_anime_id_reviewed
join anime on vw_anime_id_reviewed.anime_id = anime.id
join poster on anime.poster_id = poster.id
left join banner on anime.banner_id = banner.id
left join anime_genre on vw_anime_id_reviewed.anime_id = anime_genre.anime_id
left join genre on anime_genre.genre_id = genre.id
left join anime_studio on vw_anime_id_reviewed.anime_id = anime_studio.anime_id
left join studio on anime_studio.studio_id = studio.id
left join preferences on preferences.anime_id = anime.id
where
    (anime.id = sqlc.arg('id'));
