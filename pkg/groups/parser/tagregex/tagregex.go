package tagregex

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
)

const (
	Name = "named_regex"

	tagTitle          = "title"
	tagEpisode        = "episode"
	tagQualityPreset  = "quality_preset"
	tagQualityWidth   = "quality_width"
	tagQualityHeight  = "quality_height"
	tagReleaseType    = "release_type"
	tagVideoCodec     = "video_codec"
	tagVideoBitDepth  = "video_bitdepth"
	tagAudioCodec     = "audio_codec"
	tagReleaseVersion = "release_version"
	tagCRC32          = "crc32"
)

type taggedRegexParser struct {
	group string
	exprs []*regexp.Regexp
}

func (r *taggedRegexParser) parseTags(regex *regexp.Regexp, phrase string) *entry.Entry {
	if !regex.MatchString(phrase) {
		return nil
	}

	_entry := &entry.Entry{ReleaseGroup: r.group, VideoResolution: entry.VideoResolution{}}
	matches := regex.FindStringSubmatch(phrase)
	for index, value := range matches {
		switch strings.ToLower(regex.SubexpNames()[index]) {
		case tagTitle:
			_entry.Title = strings.TrimSpace(value)
		case tagEpisode:
			_entry.Episode = strings.TrimSpace(value)
		case tagQualityHeight:
			_entry.VideoResolution.Height, _ = strconv.Atoi(value)
		case tagQualityWidth:
			_entry.VideoResolution.Width, _ = strconv.Atoi(value)
		case tagReleaseVersion:
			_entry.Version = strings.TrimSpace(value)
		case tagCRC32:
			_entry.CRC32 = value
		}
	}

	return _entry
}

func (r *taggedRegexParser) Parse(name string) *entry.Entry {
	for _, expr := range r.exprs {
		if expr.MatchString(name) {
			if _entry := r.parseTags(expr, name); _entry != nil {
				return _entry
			}
		}
	}

	return nil
}

func (r *taggedRegexParser) Matchers() (matchers []base.Matcher) {
	return []base.Matcher{base.Matcher_NamedRegexp}
}

func (r *taggedRegexParser) Match(name string) bool {
	for _, expr := range r.exprs {
		if expr.MatchString(name) {
			return true
		}
	}

	return false
}

//

func New(group string, exprs []*regexp.Regexp) base.Parser {
	return &taggedRegexParser{group, exprs}
}
