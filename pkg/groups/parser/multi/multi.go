package multi

import (
	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
)

const Name = "multi"

type multiParser struct {
	parsers []base.Parser
}

func (m *multiParser) Parse(name string) *entry.Entry {
	for _, parser := range m.parsers {
		if parser.Match(name) {
			return parser.Parse(name)
		}
	}

	return nil
}

func (m *multiParser) Match(name string) bool {
	for _, parser := range m.parsers {
		if parser.Match(name) {
			return true
		}
	}

	return false
}

func (m *multiParser) Matchers() (matchers []base.Matcher) {
	matchers = make([]base.Matcher, 0)

	for _, parser := range m.parsers {
		if parser != nil {
			matchers = append(matchers, parser.Matchers()...)
		}
	}

	return
}

func New(parsers ...base.Parser) (parser base.Parser) {
	return &multiParser{parsers: parsers}
}
