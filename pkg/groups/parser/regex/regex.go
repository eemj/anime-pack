package regex

import (
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
)

const Name = "regex"

var DefaultMatches = Matches{Title: 1, Episode: 2, Quality: 3, CRC32: 4}

type Matches struct {
	Title   int `json:"title"`
	Episode int `json:"episode"`
	Version int `json:"version"`
	Quality int `json:"quality"`
	CRC32   int `json:"crc32"`
}

type Expression struct {
	Regexp  *regexp.Regexp
	Matches Matches
}

type regexParser struct {
	group string
	exprs []Expression
}

func (r *regexParser) Parse(name string) *entry.Entry {
	for _, expr := range r.exprs {
		if expr.Regexp.MatchString(name) {
			submatches := expr.Regexp.FindStringSubmatch(name)
			ent := &entry.Entry{ReleaseGroup: r.group}
			n := len(submatches)

			if expr.Matches.Title > 0 && n > expr.Matches.Title {
				ent.Title = strings.TrimSpace(submatches[expr.Matches.Title])
			}

			if expr.Matches.Episode > 0 && n > expr.Matches.Episode {
				ent.Episode = strings.TrimSpace(submatches[expr.Matches.Episode])
			}

			if expr.Matches.Version > 0 && n > expr.Matches.Version {
				ent.Version = submatches[expr.Matches.Version]
			}

			if expr.Matches.Quality > 0 && n > expr.Matches.Quality {
				ent.VideoResolution.Height, _ = strconv.Atoi(submatches[expr.Matches.Quality])
			}

			if expr.Matches.CRC32 > 0 && n > expr.Matches.CRC32 {
				ent.CRC32 = strings.ToUpper(submatches[expr.Matches.CRC32])
			}

			return ent
		}
	}

	return nil
}

func (r *regexParser) Match(name string) bool {
	for _, expr := range r.exprs {
		if expr.Regexp.MatchString(name) {
			return true
		}
	}

	return false
}

func (r *regexParser) Matchers() (matchers []base.Matcher) {
	return []base.Matcher{base.Matcher_Regexp}
}

func New(group string, exprs []Expression) base.Parser {
	return &regexParser{group, exprs}
}
