package blacklist

import (
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
)

type blacklist struct {
	group   string
	phrases []string
	parsers []base.Parser
}

func (b *blacklist) Matchers() (matchers []base.Matcher) {
	matchers = append(matchers, base.Matcher_Blacklist)

	for _, parser := range b.parsers {
		for _, parserMatcher := range parser.Matchers() {
			found := false

			for _, matcher := range matchers {
				if parserMatcher == matcher {
					found = true
					break
				}
			}

			if !found {
				matchers = append(matchers, parserMatcher)
			}
		}
	}

	return
}

func (b *blacklist) Group() string { return b.group }

func (b *blacklist) Match(name string) (match bool) {
	isBlacklisted := false

	for _, phrase := range b.phrases {
		if strings.Contains(name, phrase) {
			isBlacklisted = true
			break
		}
	}

	if isBlacklisted {
		return false
	}

	for _, parser := range b.parsers {
		if parser.Match(name) {
			return true
		}
	}

	return
}

func (b *blacklist) Parse(name string) *entry.Entry {
	if b.Match(name) {
		for _, parser := range b.parsers {
			if entry := parser.Parse(name); entry != nil {
				return entry
			}
		}
	}

	return nil
}

func New(group string, phrases []string, parsers ...base.Parser) base.Parser {
	return &blacklist{
		group:   group,
		phrases: phrases,
		parsers: parsers,
	}
}
