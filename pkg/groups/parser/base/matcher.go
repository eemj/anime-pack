package base

type Matcher int

const (
	Matcher_None        Matcher = 0
	Matcher_Regexp      Matcher = 1
	Matcher_NamedRegexp Matcher = 2
	Matcher_Blacklist   Matcher = 3
	Matcher_Anitomy     Matcher = 4
)

func (m Matcher) String() string {
	switch m {
	case Matcher_None:
		return "NONE"
	case Matcher_Regexp:
		return "REGEXP"
	case Matcher_NamedRegexp:
		return "NAMED_REGEXP"
	case Matcher_Blacklist:
		return "BLACKLIST"
	case Matcher_Anitomy:
		return "ANITOMY"
	}
	return ""
}
