package base

import (
	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
)

type Parser interface {
	Parse(name string) *entry.Entry
	Match(name string) bool
	Matchers() (matcher []Matcher)
}
