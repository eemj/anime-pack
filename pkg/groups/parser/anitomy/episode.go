package anitomy

import (
	"strconv"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

func parseEpisodeNumber(episodeNumbers []string) string {
	var episodeNumber string
	if len(episodeNumbers) == 1 {
		episodeNumber = episodeNumbers[0]
	}
	episodeNumber = utils.Unpad(episodeNumber)
	_, err := strconv.ParseFloat(episodeNumber, 64)
	if err != nil {
		return ""
	}
	return episodeNumber
}
