package anitomy

import (
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
)

func parseResolution(resolution string) entry.VideoResolution {
	resolution = strings.ToLower(resolution) //

	index := strings.IndexRune(resolution, 'x')
	if index > -1 {
		// Case: "1280x720"
		width, err := strconv.Atoi(resolution[:index])
		if err != nil {
			return entry.VideoResolution{}
		}
		height, err := strconv.Atoi(resolution[index+1:])
		if err != nil {
			return entry.VideoResolution{}
		}
		return entry.VideoResolution{
			Width:  width,
			Height: height,
		}
	}

	// Case: "720p"
	if len(resolution) >= 2 && resolution[len(resolution)-1] == 'p' {
		height, err := strconv.Atoi(resolution[:len(resolution)-1])
		if err == nil {
			return entry.VideoResolution{Height: height}
		}
	}

	return entry.VideoResolution{}
}
