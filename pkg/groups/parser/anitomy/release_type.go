package anitomy

import "strings"

var allowedAnimeTypes = map[string]bool{
	"MOVIE":    true,
	"OAD":      true,
	"OAV":      true,
	"ONA":      true,
	"OVA":      true,
	"SP":       true,
	"SPECIAL":  true,
	"SPECIALS": true,
	"TV":       true,

	"GEKIJOUBAN": false,
	"ED":         false,
	"ENDING":     false,
	"NCED":       false,
	"NCOP":       false,
	"OP":         false,
	"OPENING":    false,
	"PREVIEW":    false,
	"PV":         false,
}

func areAnimeTypesAllowed(animeTypes []string) bool {
	if len(animeTypes) == 0 {
		return true
	}

	for _, animeType := range animeTypes {
		// Make sure it's always uppercase.
		if !allowedAnimeTypes[strings.ToUpper(animeType)] {
			return false
		}
	}

	return false
}
