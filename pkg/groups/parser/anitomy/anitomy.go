package anitomy

import (
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"gitlab.com/eemj/anitogo"
)

var (
	options = anitogo.Options{
		AllowedDelimiters:  " _.&+,|",
		IgnoredStrings:     []string{},
		ParseEpisodeNumber: true,
		ParseEpisodeTitle:  true,
		ParseFileExtension: true,
		ParseReleaseGroup:  true,
	}

	blacklistPhrases = [...]string{
		// GHOST
		"commentary",
		"menu",
		"bd menu vol",

		// THORA
		"tlnotes",
		"tl_preview",
	}
)

type anitomyParser struct{}

// Match implements base.Parser.
func (p *anitomyParser) Match(name string) bool {
	return p.Parse(name) != nil
}

// Matchers implements base.Parser.
func (*anitomyParser) Matchers() (matcher []base.Matcher) {
	return []base.Matcher{base.Matcher_Anitomy}
}

// Parse implements base.Parser.
func (*anitomyParser) Parse(name string) *entry.Entry {
	elements := anitogo.Parse(name, options)
	if elements == nil {
		return nil
	}

	// If strings.Contains one of the blacklisted phrases,
	// we'll terminate the parsing.
	lowerName := strings.ToLower(name)
	for _, phrase := range blacklistPhrases {
		if strings.Contains(lowerName, phrase) {
			return nil
		}
	}

	release := &entry.Entry{
		Title:        elements.AnimeTitle,
		ReleaseGroup: elements.ReleaseGroup,
	}

	if len(elements.AnimeSeason) > 0 {
		seasonNumber := utils.Unpad(elements.AnimeSeason[0])
		if seasonNumber != "" {
			release.SeasonNumber = &seasonNumber
		}
	}

	// If `AnimeYear` is not empty and `AnimeYear` > 0
	if elements.AnimeYear != "" {
		releaseYear, err := strconv.Atoi(elements.AnimeYear)
		if err == nil && releaseYear > 0 {
			release.Year = &releaseYear
		}
	}

	// if `EpisodeNumber` is empty, fallback to `EpisodeNumberAlt`.
	episodeNumbers := elements.EpisodeNumber
	if len(episodeNumbers) == 0 {
		episodeNumbers = elements.EpisodeNumberAlt
	}
	if len(episodeNumbers) > 0 {
		release.Episode = parseEpisodeNumber(episodeNumbers)
	}

	// Versions.
	versions := elements.ReleaseVersion
	if len(versions) > 0 {
		release.Version = versions[0]
	}

	// If there's no `title`, `release group` or an `episode`, or maybe one of the
	// anime types isn't allowed.. it's a deal breaker.
	if release.Title == "" ||
		release.ReleaseGroup == "" ||
		release.Episode == "" ||
		!areAnimeTypesAllowed(elements.AnimeType) {
		return nil
	}

	// CRC32
	if elements.FileChecksum != "" && len(elements.FileChecksum) == 8 {
		release.CRC32 = strings.ToUpper(elements.FileChecksum) // Make sure it's always uppercase.
	}

	// Video Resolution can be either: 1280x720 / 720p
	if elements.VideoResolution != "" {
		release.VideoResolution = parseResolution(elements.VideoResolution)
	}

	return release
}

func New() base.Parser {
	return &anitomyParser{}
}
