package groups

import (
	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/anitomy"
	"gitlab.com/eemj/anime-pack/pkg/groups/parser/base"
)

var _parser base.Parser = anitomy.New()

func Parse(filename string) *entry.Entry {
	// It would be wise to have a regex fallback,
	// since there are some releases that don't follow the naming
	// standard anitomy defines.
	return _parser.Parse(filename)
}
