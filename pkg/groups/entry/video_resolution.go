package entry

import (
	"fmt"
	"strconv"
)

type VideoResolution struct {
	Height int `json:"height"`
	Width  int `json:"width"`
}

func (r VideoResolution) String() string {
	if r.Width == 0 && r.Height > 0 {
		return (strconv.Itoa(r.Height) + "p")
	}
	if r.Width > 0 && r.Height > 0 {
		return fmt.Sprintf("%dx%d", r.Width, r.Height)
	}
	return ""
}

func (r VideoResolution) IsZero() bool { return r.Height == 0 }
