package entry

type Entry struct {
	ReleaseGroup    string          `json:"group"`
	Title           string          `json:"title"`
	SeasonNumber    *string         `json:"seasonNumber,omitempty"`
	Year            *int            `json:"year,omitempty"`
	Episode         string          `json:"episode"`
	Version         string          `json:"version,omitempty"`
	CRC32           string          `json:"crc32"`
	Extension       string          `json:"extension"`
	VideoResolution VideoResolution `json:"videoResolution"`
}
