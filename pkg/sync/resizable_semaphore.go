package sync

// IMPORTANT:
// This implementation is 99% based on 'https://github.com/marusama/semaphore/blob/master/semaphore.go'
// However, we didn't want the panics, therefore we re-implemented it in our application.

import (
	"context"
	"sync"
	"sync/atomic"
)

const (
	min = 0xFFFFFFFF
	max = 0xFFFFFFFF00000000
)

// ResizableSemaphore counting resizable semaphore synchronization primitive.
// Use the Semaphore to control access to a pool of resources.
// There is no guaranteed order, such as FIFO or LIFO, in which blocked goroutines enter the semaphore.
// A goroutine can enter the semaphore multiple times, by calling the Acquire or TryAcquire methods repeatedly.
// To release some or all of these entries, the goroutine can call the Release method
// that specifies the number of entries to be released.
// Change Semaphore capacity to lower or higher by SetLimit.
type ResizableSemaphore struct {
	state     uint64
	lock      sync.RWMutex
	broadcast chan struct{}
}

// NewResizableSemaphore initializes a new instance of the ResizableSemaphore,
// the limit parameter points to the maximum number of concurrent entries.
func NewResizableSemaphore(limit int) (semaphore *ResizableSemaphore) {
	semaphore = &ResizableSemaphore{
		state:     uint64(limit) << 32,
		broadcast: make(chan struct{}),
	}

	return
}

// Acquire enters the semaphore a specified number of times, blocking only until ctx is done.
// This operation can be cancelled via passed context (but it's allowed to pass ctx='nil').
// Method returns context error (ctx.Err()) if the passed context is cancelled,
// but this behavior is not guaranteed and sometimes semaphore will still be acquired.
func (s *ResizableSemaphore) Acquire(ctx context.Context, n int) (err error) {
	var ctxDoneCh <-chan struct{}

	if ctx != nil {
		ctxDoneCh = ctx.Done()
	}

	for {
		// check if the received context is done
		select {
		case <-ctxDoneCh:
			return ctx.Err()
		default:
		}

		state := atomic.LoadUint64(&s.state)
		count := state & min
		limit := state >> 32

		newCount := count + uint64(n)

		if newCount <= limit {
			if atomic.CompareAndSwapUint64(&s.state, state, limit<<32+newCount) {
				return nil
			}

			continue
		} else {
			s.lock.RLock()
			broadcast := s.broadcast
			s.lock.RUnlock()

			if atomic.LoadUint64(&s.state) != state {
				continue
			}

			select {
			case <-ctxDoneCh:
				return ctx.Err()
			case <-broadcast:
			}
		}
	}

	return
}

// TryAcquire acquires the semaphore without blocking.
// On success, returns true. On failure, returns false and leaves the semaphore unchanged.
func (s *ResizableSemaphore) TryAcquire(n int) bool {
	if n <= 0 {
		return false
	}

	for {
		state := atomic.LoadUint64(&s.state)
		count := state & min
		limit := state >> 32

		newCount := count + uint64(n)

		if newCount <= limit {
			if atomic.CompareAndSwapUint64(&s.state, state, (limit<<32)+newCount) {
				return true
			}

			continue
		}

		return false
	}
}

// Release exits the semaphore a specified number of times
func (s *ResizableSemaphore) Release(n int) {
	if n <= 0 {
		return
	}

	for {
		state := atomic.LoadUint64(&s.state)
		count := state & min

		if count < uint64(n) {
			return
		}

		newCount := count - uint64(n)

		if atomic.CompareAndSwapUint64(
			&s.state, state,
			((state & max) + newCount),
		) {
			newBroadcast := make(chan struct{})
			s.lock.Lock()
			broadcast := s.broadcast
			s.broadcast = newBroadcast
			s.lock.Unlock()
			close(broadcast)

			return
		}
	}
}

// SetLimit changes current semaphore limit in concurrent way.
// It is allowed to change limit many times and it's safe to set limit higher or lower.
func (s *ResizableSemaphore) SetLimit(limit int) {
	for {
		state := atomic.LoadUint64(&s.state)

		if atomic.CompareAndSwapUint64(
			&s.state, state,
			((uint64(limit) << 32) + (state & min)),
		) {
			newBroadcast := make(chan struct{})
			s.lock.Lock()
			broadcast := s.broadcast
			s.broadcast = newBroadcast
			s.lock.Unlock()
			close(broadcast)

			return
		}
	}
}

// GetCount returns current number of occupied entries in semaphore.
func (s *ResizableSemaphore) GetCount() int { return int(atomic.LoadUint64(&s.state) & min) }

// GetLimit returns current semaphore limit.
func (s *ResizableSemaphore) GetLimit() int { return int(atomic.LoadUint64(&s.state) >> 32) }
