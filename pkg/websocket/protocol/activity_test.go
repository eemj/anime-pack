package protocol

import (
	"sort"
	"testing"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

type TestDataActivityDownloadSortScenario struct {
	Incoming DataActivityDownload
	Expected DataActivityDownload
}

func TestDataActivityDownloadSort(t *testing.T) {
	scenarios := []TestDataActivityDownloadSortScenario{{
		Incoming: DataActivityDownload{
			&DataDownloadItem{Prioritize: utils.PointerOf(false), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
			&DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		},
		Expected: DataActivityDownload{
			&DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
			&DataDownloadItem{Prioritize: utils.PointerOf(false), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		},
	}, {
		Incoming: DataActivityDownload{
			&DataDownloadItem{Prioritize: nil, ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
			&DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		},
		Expected: DataActivityDownload{
			&DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
			&DataDownloadItem{Prioritize: nil, ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		},
	}}

	for _, scenario := range scenarios {
		n := len(scenario.Incoming)

		if n != len(scenario.Expected) {
			t.Fatalf("expected both slices to have the same length")
		}

		sort.Sort(scenario.Incoming)

		for index, item := range scenario.Incoming {
			adjacent := scenario.Expected[index]

			if !item.Equals(adjacent) {
				t.Fatalf("(%d/%d) expected '%s' but got '%s'", index, n, item, adjacent)
			}
		}
	}
}

type TestDataActivityDownloadItemEqualsScenario struct {
	Original *DataDownloadItem
	Modified *DataDownloadItem
	IsEqual  bool
}

func TestDataActivityDownloadItemEquals(t *testing.T) {
	scenarios := []TestDataActivityDownloadItemEqualsScenario{{
		Original: &DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		Modified: &DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		IsEqual:  true,
	}, {
		Original: &DataDownloadItem{Prioritize: utils.PointerOf(false), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		Modified: &DataDownloadItem{Prioritize: utils.PointerOf(true), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		IsEqual:  false,
	}, {
		Original: &DataDownloadItem{Prioritize: utils.PointerOf(false), ID: 1, QualityID: utils.PointerOf[int64](1), ReleaseGroupID: 1},
		Modified: &DataDownloadItem{Prioritize: utils.PointerOf(false), ID: 1, QualityID: utils.PointerOf[int64](9), ReleaseGroupID: 1},
		IsEqual:  false,
	}}

	for _, scenario := range scenarios {
		if outcome := scenario.Original.Equals(scenario.Modified); outcome != scenario.IsEqual {
			if scenario.IsEqual {
				t.Fatalf("expected '%s' to not match '%s'", scenario.Original, scenario.Modified)
			} else {
				t.Fatalf("expected '%s' to match '%s'", scenario.Original, scenario.Modified)
			}
		}
	}
}
