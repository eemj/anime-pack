package protocol

import (
	"strconv"
	"strings"
)

type Anime struct {
	ID            int64  `json:"d"`
	Title         string `json:"t"`
	Episode       string `json:"e"`
	QualityHeight int    `json:"h"`
	ReleaseGroup  string `json:"rg"`
}

func (a Anime) String() string {
	sb := new(strings.Builder)

	sb.WriteString("id:")
	sb.WriteString(strconv.FormatUint(uint64(a.ID), 10))
	sb.WriteString(", title:")
	sb.WriteString(a.Title)
	sb.WriteString(", episode:")
	sb.WriteString(a.Episode)
	sb.WriteString(", quality_height:")

	if a.QualityHeight > 0 {
		sb.WriteString(strconv.Itoa(a.QualityHeight))
	} else {
		sb.WriteString("<nil>")
	}

	sb.WriteString(", release_group:")
	sb.WriteString(a.ReleaseGroup)

	return sb.String()
}

func (a Anime) Equal(other Anime) bool {
	return a.ID == other.ID &&
		a.Title == other.Title &&
		a.Episode == other.Episode &&
		a.QualityHeight == other.QualityHeight &&
		a.ReleaseGroup == other.ReleaseGroup
}
