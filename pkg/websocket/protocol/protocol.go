package protocol

import "encoding/json"

type Opcode uint16

const (
	// Bidirectional between the website and the server
	Acknowledged Opcode = 0
	Exception    Opcode = 1

	// Activity
	Activity_Progress    Opcode = 10
	Activity_Metadata    Opcode = 11
	Activity_Download    Opcode = 12
	Activity_Cancel      Opcode = 13
	Activity_Difference  Opcode = 14
	Activity_GetProgress Opcode = 15

	// Status
	Status_IRC Opcode = 20
	Status_Job Opcode = 21
)

type Data interface {
	Validate() error
}

type opcodePayload struct {
	Opcode Opcode `json:"op"`
}

type dataPayload struct {
	Data Data `json:"d,omitempty"`
}

type Payload struct {
	Opcode Opcode `json:"op"`
	Data   Data   `json:"d,omitempty"`
}

func MustBytes[D Data](opcode Opcode, data D) []byte {
	bytes, err := NewBytes[D](opcode, data)
	if err != nil {
		panic(err)
	}
	return bytes
}

func NewBytes[D Data](opcode Opcode, data D) ([]byte, error) {
	return New(opcode, data).
		MarshalJSON()
}

func New[D Data](
	opcode Opcode,
	data D,
) *Payload {
	return &Payload{
		Opcode: opcode,
		Data:   data,
	}
}

func (p Payload) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Opcode Opcode `json:"op"`
		Data   any    `json:"d,omitempty"`
	}{
		Opcode: p.Opcode,
		Data:   p.Data,
	})
}

func (p *Payload) UnmarshalJSON(b []byte) (err error) {
	opcode := new(opcodePayload)

	if err = json.Unmarshal(b, opcode); err != nil {
		return
	}

	p.Opcode = opcode.Opcode

	data := new(dataPayload)

	switch opcode.Opcode {
	case Acknowledged:
		data.Data = new(DataAcknowledged)
	case Exception:
		data.Data = new(DataException)
	case Activity_Progress:
		data.Data = new(DataActivityProgress)
	case Activity_Metadata:
		data.Data = new(DataActivityMetadata)
	case Activity_Download:
		data.Data = new(DataActivityDownload)
	case Activity_Cancel:
		data.Data = new(DataActivityCancel)
	case Activity_Difference:
		data.Data = new(DataActivityDifferences)
	case Activity_GetProgress:
		data.Data = new(DataActivityGetProgress)
	case Status_IRC:
		data.Data = new(DataStatusIRC)
	case Status_Job:
		data.Data = new(DataStatusJob)
	default:
		return ErrUnsupportedOpcode(opcode.Opcode)
	}

	if err = json.Unmarshal(b, data); err != nil {
		return
	}

	p.Data = data.Data

	return
}
