package protocol

import "fmt"

type Error string

func (p Error) Error() string { return string(p) }

const (
	ErrAcknowledgedTimeout = Error("acknowledged timeout")

	ErrIDMustBeGreaterThan0             = Error("`id` must be greater than 0")
	ErrQualityIDMustBeGreaterThan0      = Error("`q` must be greater than 0")
	ErrReleaseGroupIDMustBeGreaterThan0 = Error("`rg` must be greater than 0")

	ErrUnexpectedNilItem = Error("unexpected `nil` item")

	ErrExpectedValidJSON = Error("expected a valid JSON")

	ErrUnexpectedRequestType = Error("unexpected request type")

	ErrTypeIsRequired       = Error("`typ` is required")
	ErrPercentageIsRequired = Error("`pct` is required")

	ErrAddressIsRequired = Error("`addr` (address) is required")
	ErrAddressIsInvalid  = Error("`addr` (address) is not a valid address")

	ErrSizeIsRequired = Error("`sz` (size) is required")
	ErrSizeIsNotValid = Error("`sz` (size) is not valid (>0)")

	ErrNameIsRequired = Error("`n` (name) is required")

	ErrDataIsRequired = Error("`d` (data) is required")

	ErrMessageIsRequired = Error("`m` (message) is required")
	ErrMessageIsEmpty    = Error("`m` (message) is empty")
)

func ErrUnsupportedOpcode(op Opcode) error {
	return Error(fmt.Sprintf("unsupported opcode `%d`", op))
}

func ErrUnsupportedMessageType(messageType string) error {
	return Error(fmt.Sprintf("message type `%s` is not supported", messageType))
}
