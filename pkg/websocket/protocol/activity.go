package protocol

import (
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// ProgressType defines the progression type
type ProgressType string

const (
	// Progress_Download is the type when the file is currently being uploaded
	Progress_Download ProgressType = "download"

	// Progress_Transcode is the type when the file is currently being transcoded
	// (generally from mkv to mp4/ main purpose is to prepare it for the web)
	Progress_Transcode ProgressType = "transcode"

	// Progress_Upload is the type when the runner is uploading the finalized transcode back
	// to the host.
	Progress_Upload ProgressType = "upload"

	// Progress_Metadata is when we receive metadata related information for the file
	// (duration, crc32, thumbnail),
	Progress_Metadata ProgressType = "metadata"
)

// DataActivityProgressItem is used show the progress for the possible progress types.
type DataActivityProgressItem struct {
	Type ProgressType `json:"typ"`

	Percentage float64 `json:"pct"`
	Speed      float64 `json:"sp"`

	// Download/upload properties
	CurrentFilesize *float64 `json:"cur_sz,omitempty"`
	Filesize        *float64 `json:"sz,omitempty"`

	// Transcode properties
	CurrentDuration *float64 `json:"cur_dur,omitempty"`
	Duration        *float64 `json:"dur,omitempty"`
}

func (p DataActivityProgressItem) String() string {
	sb := new(strings.Builder)

	sb.WriteString("type:")
	sb.WriteString(string(p.Type))
	sb.WriteString(", percentage:")
	sb.WriteString(strconv.FormatFloat(p.Percentage, 'f', 8, 64))
	sb.WriteString(", speed:")
	sb.WriteString(strconv.FormatFloat(p.Speed, 'f', 8, 64))

	if p.CurrentFilesize != nil {
		sb.WriteString(", current_filesize:")
		sb.WriteString(strconv.FormatFloat(*p.CurrentFilesize, 'f', 8, 64))
	}

	if p.Filesize != nil {
		sb.WriteString(", filesize:")
		sb.WriteString(strconv.FormatFloat(*p.Filesize, 'f', 8, 64))
	}

	if p.CurrentDuration != nil {
		sb.WriteString(", current_duration:")
		sb.WriteString(strconv.FormatFloat(*p.CurrentDuration, 'f', 8, 64))
	}

	if p.Duration != nil {
		sb.WriteString(", duration:")
		sb.WriteString(strconv.FormatFloat(*p.Duration, 'f', 8, 64))
	}

	return sb.String()
}

func (p DataActivityProgressItem) Validate() error {
	if utils.IsEmpty(string(p.Type)) {
		return ErrTypeIsRequired
	} else if utils.IsEmpty(p.Percentage) {
		return ErrPercentageIsRequired
	}
	return nil
}

type DataActivityGetProgress struct{}

func (d DataActivityGetProgress) Validate() error { return nil }

type DataActivityProgress map[string]struct {
	ID            int64                                                   `json:"d"`
	ReleaseGroups map[string]map[string]map[int]*DataActivityProgressItem `json:"rg"`
}

func (d DataActivityProgress) Validate() error { return nil }

type DataDownloadItem struct {
	ID             int64  `json:"d"`
	QualityID      *int64 `json:"q,omitempty"`
	ReleaseGroupID int64  `json:"rg"`
	Prioritize     *bool  `json:"p,omitempty"`
}

func (d *DataDownloadItem) String() string {
	sb := new(strings.Builder)

	sb.WriteString("id:")
	sb.WriteString(strconv.FormatInt(d.ID, 10))
	sb.WriteString(", ")

	sb.WriteString("qualityID:")

	if d.QualityID == nil {
		sb.WriteString("nil")
	} else {
		sb.WriteString(strconv.FormatInt(*d.QualityID, 10))
	}
	sb.WriteString(", ")

	sb.WriteString("releaseGroupID:")
	sb.WriteString(strconv.FormatInt(d.ReleaseGroupID, 10))
	sb.WriteString(", ")

	sb.WriteString("prioritize:")
	if d.Prioritize == nil {
		sb.WriteString("nil")
	} else {
		sb.WriteString(strconv.FormatBool(*d.Prioritize))
	}

	return sb.String()
}

func (d *DataDownloadItem) Equals(in *DataDownloadItem) bool {
	return d.ID == in.ID &&
		d.ReleaseGroupID == in.ReleaseGroupID &&
		((d.Prioritize != nil && in.Prioritize != nil && *d.Prioritize == *in.Prioritize) ||
			(d.Prioritize == nil && in.Prioritize == nil)) &&
		((d.QualityID != nil && in.QualityID != nil && *d.QualityID == *in.QualityID) ||
			(d.QualityID == nil && in.QualityID == nil))
}

type DataActivityDownload []*DataDownloadItem

func (d DataActivityDownload) Len() int { return len(d) }

func (d DataActivityDownload) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
}

// Less primarily sorts by the priority
func (d DataActivityDownload) Less(i, j int) bool {
	return (d[i].Prioritize != nil && *d[i].Prioritize) &&
		(d[j].Prioritize == nil || (d[j].Prioritize != nil && !*d[j].Prioritize))
}

func (items DataActivityDownload) Validate() error {
	for _, item := range items {
		if item == nil {
			return ErrUnexpectedNilItem
		} else if item.ID < 1 {
			return ErrIDMustBeGreaterThan0
		} else if item.QualityID != nil && *item.QualityID < 1 {
			return ErrQualityIDMustBeGreaterThan0
		} else if item.ReleaseGroupID < 1 {
			return ErrReleaseGroupIDMustBeGreaterThan0
		}
	}
	return nil
}

type DataDownloadCancelItem struct {
	ID             int64  `json:"d"`
	QualityID      *int64 `json:"q,omitempty"`
	ReleaseGroupID int64  `json:"rg"`
}

type DataActivityCancel []*DataDownloadCancelItem

func (items DataActivityCancel) Validate() error {
	for _, item := range items {
		if item == nil {
			return ErrUnexpectedNilItem
		} else if item.ID < 1 {
			return ErrIDMustBeGreaterThan0
		} else if item.QualityID != nil && *item.QualityID < 1 {
			return ErrQualityIDMustBeGreaterThan0
		} else if item.ReleaseGroupID < 1 {
			return ErrReleaseGroupIDMustBeGreaterThan0
		}
	}
	return nil
}

type DataActivityMetadata struct {
	Title        string  `json:"t"`
	Episode      string  `json:"e"`
	Height       int     `json:"h"`
	ReleaseGroup string  `json:"rg"`
	Thumbnail    string  `json:"th"`
	CRC32        string  `json:"crc32"`
	Duration     float64 `json:"dur"`
	Size         int64   `json:"sz"`
	VideoPath    string  `json:"vp"`
}

func (DataActivityMetadata) Validate() error { return nil }
