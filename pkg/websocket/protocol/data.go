package protocol

import (
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

type DataAcknowledged struct{}

func (DataAcknowledged) Validate() error { return nil }

type DataException struct {
	Message string `json:"m"`
}

func (d DataException) Validate() error {
	if utils.IsEmpty(d.Message) {
		return ErrMessageIsRequired
	}
	return nil
}
