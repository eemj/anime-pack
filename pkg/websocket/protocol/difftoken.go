package protocol

import "strings"

type DifferenceToken rune

const (
	DifferenceToken_Undefined DifferenceToken = '?'
	DifferenceToken_Add       DifferenceToken = '+'
	DifferenceToken_Remove    DifferenceToken = '-'
	DifferenceToken_Modify    DifferenceToken = '.'
)

func (d DifferenceToken) MarshalJSON() ([]byte, error) {
	return []byte(`"` + string(d) + `"`), nil
}

func (d *DifferenceToken) UnmarshalJSON(b []byte) error {
	unquoted := strings.Trim(string(b), `"`)
	*d = ParseDifferenceToken(unquoted)
	return nil
}

func ParseDifferenceToken(t string) DifferenceToken {
	switch t {
	case string(DifferenceToken_Add):
		return DifferenceToken_Add
	case string(DifferenceToken_Remove):
		return DifferenceToken_Remove
	case string(DifferenceToken_Modify):
		return DifferenceToken_Modify
	}

	return DifferenceToken_Undefined
}
