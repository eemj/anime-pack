package protocol

import (
	"strconv"
	"strings"
)

type DataActivityDifferences []*DataActivityDifference

func (d DataActivityDifferences) Validate() error {
	return nil
}

func (d DataActivityDifferences) String() string {
	sb := new(strings.Builder)

	for index, difference := range d {
		sb.WriteString(strconv.Itoa(index))
		sb.WriteString(":[")
		sb.WriteString(difference.String())
		sb.WriteString("], ")
	}

	return strings.TrimSuffix(sb.String(), ", ")
}

type DataActivityDifference struct {
	Token    DifferenceToken           `json:"tkn"`
	Anime    Anime                     `json:"an"`
	Progress *DataActivityProgressItem `json:"pg"`
}

func (d DataActivityDifference) Validate() error {
	return nil
}

func (d DataActivityDifference) String() string {
	sb := new(strings.Builder)

	sb.WriteString("token:'")
	sb.WriteRune(rune(d.Token))
	sb.WriteString("'")

	sb.WriteString(", anime:[")
	sb.WriteString(d.Anime.String())
	sb.WriteString("]")

	if d.Progress != nil {
		sb.WriteString(", progress:[")
		sb.WriteString(d.Progress.String())
		sb.WriteString("]")
	}

	return sb.String()
}
