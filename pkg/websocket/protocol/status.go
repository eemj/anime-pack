package protocol

import "time"

type JobState string

const (
	State_Enable  JobState = "ENABLE"
	State_Disable JobState = "DISABLE"
	State_Passive JobState = "PASSIVE"
	State_Active  JobState = "ACTIVE"
)

type DataStatusIRC struct {
	Connected bool `json:"c"`
}

func (DataStatusIRC) Validate() error { return nil }

type DataStatusJob struct {
	State        JobState      `json:"s"`
	ID           string        `json:"d"`
	LastActivity time.Time     `json:"la,omitempty"`
	NextActivity *time.Time    `json:"na,omitempty"`
	StartedAt    *time.Time    `json:"sat,omitempty"`
	EndedAt      *time.Time    `json:"eat,omitempty"`
	Period       time.Duration `json:"p"`
}

func (DataStatusJob) Validate() error {
	return nil
}
