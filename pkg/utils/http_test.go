package utils

import (
	"net"
	"net/http"
	"testing"
)

type TestRealRemoteAddrScenario struct {
	Request *http.Request
}

func TestRealRemoteAddr(t *testing.T) {
	expectedAddr := "203.0.113.195:12313"

	createHTTPRequest := func(key, value string) *http.Request {
		return &http.Request{
			RemoteAddr: expectedAddr,
			Header:     http.Header{key: []string{value}},
		}
	}

	scenarios := []TestRealRemoteAddrScenario{
		{
			Request: createHTTPRequest(
				headerXForwardedFor,
				"203.0.113.195,2001:db8:85a3:8d3:1319:8a2e:370:7348,150.172.238.178",
			),
		},
		{
			Request: createHTTPRequest(headerCFConnectingIP, "203.0.113.195"),
		},
		{
			Request: createHTTPRequest(headerXRealIP, "203.0.113.195"),
		},
		{
			Request: createHTTPRequest(headerTrueClientIP, "203.0.113.195"),
		},
		{
			Request: createHTTPRequest(headerFastlyClientIP, "203.0.113.195"),
		},
	}

	for index, scenario := range scenarios {
		host, port, err := RealRemoteAddr(scenario.Request)
		if err != nil {
			t.Fatalf(
				"unexpected error for scenario '%d' '%s' - %v",
				index,
				expectedAddr,
				err,
			)
		}

		if address := net.JoinHostPort(host, port); address != expectedAddr {
			t.Fatalf(
				"expected '%s' for scenario '%d', but got '%s'",
				expectedAddr,
				index,
				address,
			)
		}
	}
}
