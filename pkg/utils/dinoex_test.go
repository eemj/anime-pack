package utils

import "testing"

type TestScenarioRemoveEvilCharacters struct {
	Phrase         string
	ReplaceSpaces  bool
	ExpectedResult string
}

func TestRemoveEvilCharacters(t *testing.T) {
	scenarios := []TestScenarioRemoveEvilCharacters{
		{
			Phrase:         "Hello world|",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world:",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world?",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world*",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world<",
			ReplaceSpaces:  true,
			ExpectedResult: "Hello_world_",
		},
		{
			Phrase:         "Hello world>",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world/",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world\\",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world\\",
			ReplaceSpaces:  true,
			ExpectedResult: "Hello_world_",
		},
		{
			Phrase:         "Hello world\\",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world\"",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
		{
			Phrase:         "Hello world'",
			ReplaceSpaces:  true,
			ExpectedResult: "Hello_world_",
		},
		{
			Phrase:         "Hello world\u007F",
			ReplaceSpaces:  false,
			ExpectedResult: "Hello world_",
		},
	}

	for _, scenario := range scenarios {
		result := RemoveEvilCharacters(scenario.Phrase, scenario.ReplaceSpaces)
		if result != scenario.ExpectedResult {
			t.Fatalf("'%s'!='%s'", result, scenario.ExpectedResult)
		}
	}
}
