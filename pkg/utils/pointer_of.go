package utils

func PointerOf[T any](t T) *T { return &t }

func ValueOf[T any](t *T) (v T) {
	if t == nil {
		return
	}
	return *t
}
