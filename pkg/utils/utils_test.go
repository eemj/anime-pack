package utils

import "testing"

type EmptyScenario struct {
	Name   string
	In     any
	Result bool
}

func TestEmpty(t *testing.T) {
	t.Parallel()

	scenarios := []EmptyScenario{
		{
			Name:   "uint empty",
			In:     uint(0),
			Result: true,
		},
		{
			Name:   "uint not empty",
			In:     uint(1),
			Result: false,
		},
		{
			Name:   "uint8 empty",
			In:     uint8(0),
			Result: true,
		},
		{
			Name:   "uint8 not empty",
			In:     uint8(1),
			Result: false,
		},
		{
			Name:   "uint16 empty",
			In:     uint16(0),
			Result: true,
		},
		{
			Name:   "uint16 not empty",
			In:     uint16(1),
			Result: false,
		},
		{
			Name:   "uint32 empty",
			In:     uint32(0),
			Result: true,
		},
		{
			Name:   "uint32 not empty",
			In:     uint32(1),
			Result: false,
		},
		{
			Name:   "uint64 empty",
			In:     uint64(0),
			Result: true,
		},
		{
			Name:   "uint64 not empty",
			In:     uint64(1),
			Result: false,
		},
		{
			Name:   "int empty",
			In:     int(0),
			Result: true,
		},
		{
			Name:   "int not empty",
			In:     int(1),
			Result: false,
		},
		{
			Name:   "int8 empty",
			In:     int8(0),
			Result: true,
		},
		{
			Name:   "int8 not empty",
			In:     int8(1),
			Result: false,
		},
		{
			Name:   "int16 empty",
			In:     int16(0),
			Result: true,
		},
		{
			Name:   "int16 not empty",
			In:     int16(1),
			Result: false,
		},
		{
			Name:   "int32 empty",
			In:     int32(0),
			Result: true,
		},
		{
			Name:   "int32 not empty",
			In:     int32(1),
			Result: false,
		},
		{
			Name:   "int64 empty",
			In:     int64(0),
			Result: true,
		},
		{
			Name:   "int64 not empty",
			In:     int64(1),
			Result: false,
		},
	}

	for _, scenario := range scenarios {
		if IsEmpty(scenario.In) != scenario.Result {
			t.Fatalf(
				"expected (%T) '%v' but got '%v' for '%s'",
				scenario.In,
				scenario.Result,
				!scenario.Result,
				scenario.Name,
			)
		}
	}
}

type UnpadScenario struct {
	Name string
	In   string
	Out  string
}

func TestUnpad(t *testing.T) {
	scenarios := []UnpadScenario{
		{
			Name: "unpad 1 digit",
			In:   "1",
			Out:  "1",
		},
		{
			Name: "unpadding 1 digit but '0'",
			In:   "0",
			Out:  "0",
		},
		{
			Name: "unpad 2 digits",
			In:   "02",
			Out:  "2",
		},
		{
			Name: "unpad 2 digits but '0'",
			In:   "00",
			Out:  "0",
		},
		{
			Name: "unpad 3 digits but '7'",
			In:   "007",
			Out:  "7",
		},
		{
			Name: "unpad 4 digits but '7'",
			In:   "0007",
			Out:  "7",
		},
		{
			Name: "unpad 10 digits but '7'",
			In:   "0000000007",
			Out:  "7",
		},
	}

	for _, scenario := range scenarios {
		out := Unpad(scenario.In)

		if out != scenario.Out {
			t.Logf("scenario: '%s'", scenario.Name)
			t.Fatalf("unexpected '%s', expected '%s'", out, scenario.Out)
		}
	}
}

type SurroundWithScenario struct {
	Name string
	In   []string
	Out  string
	With rune
}

func TestSurroundWith(t *testing.T) {
	scenarios := []SurroundWithScenario{
		{
			Name: "single string",
			In:   []string{"a"},
			Out:  "`a`",
			With: '`',
		},
		{
			Name: "multiple strings",
			In:   []string{"a", "b"},
			Out:  "`a`, `b`",
			With: '`',
		},
	}

	for _, scenario := range scenarios {
		out := SurroundWith(scenario.With, scenario.In...)

		if out != scenario.Out {
			t.Logf("scenario: '%s'", scenario.Name)
			t.Fatalf("unexpected '%s', expected '%s'", out, scenario.Out)
		}
	}
}
