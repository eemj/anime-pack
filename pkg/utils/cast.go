package utils

import "fmt"

func TryCast[T any](s interface{}) (T, error) {
	t, ok := s.(T)
	if !ok {
		defaultT := *new(T)
		return defaultT, fmt.Errorf("expected config type `%T` but got `%T`", defaultT, s)
	}
	return t, nil
}
