package utils

import "testing"

type SecondsScenario struct {
	Name      string
	Formatted string
	Seconds   float64
	Seek      float64
}

func TestSecondsToDuration(t *testing.T) {
	t.Parallel()

	scenarios := []SecondsScenario{
		{
			Name:      "80% of 10 minutes",
			Seconds:   10 * 60,
			Seek:      0.8,
			Formatted: "00:08:00.000",
		},
		{
			Name:      "40% of 1 hour 40 minutes 30 seconds 2 milliseconds",
			Seconds:   (1 * 60 * 60) + (40 * 60) + 30,
			Seek:      0.4,
			Formatted: "00:40:12.002",
		},
		{
			Name:      "100% of 23 hours 59 minutes 59 seconds 0 milliseconds",
			Seconds:   (23 * 60 * 60) + (59 * 60) + 59,
			Seek:      1,
			Formatted: "23:59:59.086",
		},
		{
			Name:      "0% of 49 hours 59 minutes 59 seconds 0 milliseconds",
			Seconds:   (49 * 60 * 60) + (59 * 60) + 59,
			Seek:      0,
			Formatted: "00:00:00.000",
		},
	}

	for _, scenario := range scenarios {
		formatted := SecondsToDuration(scenario.Seconds * scenario.Seek)

		if formatted != scenario.Formatted {
			t.Logf("scenario: '%s'", scenario.Name)
			t.Fatalf("unexpected '%s', expected '%s'", formatted, scenario.Formatted)
		}
	}
}
