package utils

import "testing"

type RabinKarpScenario struct {
	Name   string
	Source string
	Target []string
	Found  int
}

func TestRabinKarp(t *testing.T) {
	t.Parallel()

	scenarios := []RabinKarpScenario{
		{
			Name:   "'GEEK FOR GEEKS' should have 'GEEK' twice as a match",
			Source: "GEEK FOR GEEKS",
			Target: []string{"GEEK"},
			Found:  1,
		},
		{
			Name:   "'Boku dake ga Inai Machi' against ['boku','no'] have 1 length as the match",
			Source: "Boku dake ga Inai Machi",
			Target: []string{"boku", "no"},
			Found:  1,
		},
		{
			Name:   "'Boku no Hero Academia' against ['boku','no','hero'] should have 3 length as the match",
			Source: "Boku no Hero Academia",
			Target: []string{"boku", "no", "hero"},
			Found:  3,
		},
	}

	for _, scenario := range scenarios {
		out := RabinKarp(scenario.Source, scenario.Target)

		if out != scenario.Found {
			t.Logf("scenario: '%s'", scenario.Name)
			t.Fatalf("unexpected '%d', expected '%d'", out, scenario.Found)
		}
	}
}
