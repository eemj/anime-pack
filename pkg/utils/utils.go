package utils

import (
	"strconv"
	"strings"
)

func IsEmpty(in any) bool {
	switch v := in.(type) {
	case []string:
		return len(v) == 0
	case string:
		return v == " " || len(v) == 0
	case uint:
		return v == 0
	case uint8:
		return v == 0
	case uint16:
		return v == 0
	case uint32:
		return v == 0
	case uint64:
		return v == 0
	case int:
		return v == 0
	case int8:
		return v == 0
	case int16:
		return v == 0
	case int32:
		return v == 0
	case int64:
		return v == 0
	case float32:
		return v == 0
	case float64:
		return v == 0
	}

	return true
}

func Unpad(phrase string) (out string) {
	if phrase == "" {
		return ""
	}
	out = strings.TrimLeftFunc(phrase, func(r rune) bool {
		return r == ' ' || r == '0'
	})
	if out == "" && phrase[0] == '0' {
		return "0"
	}
	return out
}

func NumericQuality(quality string) (numeric int) {
	numeric, _ = strconv.Atoi(quality[:(len(quality) - 1)])
	return
}

func SurroundWith(with rune, strs ...string) string {
	if len(strs) == 0 {
		return ""
	}

	sb := new(strings.Builder)
	sb.WriteRune(with)
	n := len(strs)

	for index, str := range strs {
		sb.WriteString(str)

		if (n - 1) != index {
			sb.WriteRune(with)
			sb.WriteString(", ")
			sb.WriteRune(with)
		}
	}

	sb.WriteRune(with)

	return sb.String()
}

func Ordinal(x int) string {
	suffix := "th"
	switch x % 10 {
	case 1:
		if (x % 100) != 11 {
			suffix = "st"
		}
	case 2:
		if (x % 100) != 12 {
			suffix = "nd"
		}
	case 3:
		if (x % 100) != 13 {
			suffix = "rd"
		}
	}
	return strconv.Itoa(x) + suffix
}

func ChunkBy[T any](items []T, chunkSize int) (chunks [][]T) {
	for chunkSize < len(items) {
		items, chunks = items[chunkSize:], append(chunks, items[0:chunkSize:chunkSize])
	}
	return append(chunks, items)
}
