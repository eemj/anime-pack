package utils

import (
	"sync"
)

// MergeChan merges all the channels and outputs them into one,
// following the Fan-In Fan-Out pattern.
func MergeChan[N any](cs ...<-chan N) <-chan N {
	wg := new(sync.WaitGroup)
	out := make(chan N)

	output := func(errc <-chan N) {
		for err := range errc {
			out <- err
		}
		wg.Done()
	}

	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}
