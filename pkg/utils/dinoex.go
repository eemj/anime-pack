package utils

import (
	"encoding/binary"
	"net"
	"strconv"
	"strings"
)

// AnyEvilCharacter any character in the switch below will return true on 1st find.
// Source: https://github.com/dinoex/iroffer-dinoex/blob/master/src/dinoex_utilities.c#L555
func AnyEvilCharacter(phrase string) bool {
	indexOf := strings.IndexFunc(phrase, func(r rune) bool {
		switch r {
		case '|', ':', '?', '*', '<', '>', '/', '\\', '"', '\'', '`', '\u007F':
			return true
		default:
			return false
		}
	})
	return indexOf > -1
}

// RemoveEvilCharacters strips special characters by replacing them with underscores.
// Source: https://github.com/dinoex/iroffer-dinoex/blob/master/src/dinoex_utilities.c#L555
func RemoveEvilCharacters(phrase string, replaceSpaces ...bool) string {
	var (
		sb                   strings.Builder
		wantsToReplaceSpaces = len(replaceSpaces) > 0 && replaceSpaces[0]
	)
	// sb will be N length guaranteed, things will just get replaced
	sb.Grow(len(phrase))
	for _, char := range phrase {
		switch char {
		case '|', ':', '?', '*', '<', '>', '/', '\\', '"', '\'', '`', '\u007F':
			sb.WriteRune('_')
		case ' ':
			if wantsToReplaceSpaces {
				sb.WriteRune('_')
			} else {
				sb.WriteRune(char)
			}
		default:
			sb.WriteRune(char)
		}
	}
	return sb.String()
}

// ParseIP is a wrapper on top of the net.ParseIP along with octal parsing.
func ParseIP(s string) (ip net.IP) {
	ip = net.ParseIP(s)
	if ip == nil {
		if octet, err := strconv.ParseUint(s, 10, 32); err == nil {
			ip = make(net.IP, 4)
			binary.BigEndian.PutUint32(ip, uint32(octet))
		}
	}
	return ip
}
