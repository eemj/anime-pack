package utils

import (
	"fmt"
	"hash/crc32"
	"io"
)

// ChecksumCRC32 returns a crc32 hash from the specified reader
func ChecksumCRC32(reader io.Reader) (checksum string, err error) {
	bytes, err := io.ReadAll(reader)
	if err != nil {
		return
	}

	checksum = fmt.Sprintf(
		"%08X",
		crc32.ChecksumIEEE(bytes),
	)

	return
}
