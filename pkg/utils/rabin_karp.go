package utils

import (
	"strings"
	"unicode"
)

// Sourced from: https://github.com/glkz/rabinkarp/blob/master/rabinkarp.go

const base = 16777619

// LowerCaseRemoveSpecial removes special characters from a string, while converting the letters to lowercase.
func LowerCaseRemoveSpecial(phrase string) string {
	var builder strings.Builder
	builder.Grow(len(phrase))
	for _, char := range phrase {
		if unicode.IsLetter(char) {
			if unicode.IsUpper(char) {
				char = unicode.ToLower(char)
			}
			builder.WriteRune(char)
		} else if unicode.IsNumber(char) || unicode.IsSpace(char) {
			builder.WriteRune(char)
		}
	}
	return builder.String()
}

// RabinKarp searches given patterns in phrase and returns the number of matched ones.
// Returns a zero is there are no matches.
func RabinKarp(phrase string, patterns []string) int {
	phrase = LowerCaseRemoveSpecial(phrase)

	// sanitize also the patterns
	for p := range patterns {
		patterns[p] = LowerCaseRemoveSpecial(patterns[p])
	}

	n, m := len(phrase), minLen(patterns)

	if n < m || len(patterns) == 0 {
		return 0
	}

	matches := make(map[int]int)

	var mult uint32 = 1 // mult = base^(m-1)
	for i := 0; i < m-1; i++ {
		mult = (mult * base)
	}

	hp := hashPatterns(patterns, m)
	h := hash(phrase[:m])

	for i := 0; i < n-m+1 && len(hp) > 0; i++ {
		if i > 0 {
			h -= mult * uint32(phrase[i-1])
			h = (h * base) + uint32(phrase[i+m-1])
		}

		if mps, ok := hp[h]; ok {
			for _, pi := range mps {
				pat := patterns[pi]
				e := i + len(pat)

				if _, ok := matches[pi]; !ok && e <= n && pat == phrase[i:e] {
					matches[pi] = i
				}
			}
		}
	}

	return len(matches)
}

func hash(s string) uint32 {
	var h uint32

	for i := 0; i < len(s); i++ {
		h = (h*base + uint32(s[i]))
	}

	return h
}

func hashPatterns(patterns []string, l int) map[uint32][]int {
	m := make(map[uint32][]int)

	for i, t := range patterns {
		h := hash(t[:l])
		m[h] = append(m[h], i)
	}

	return m
}

func minLen(patterns []string) int {
	if len(patterns) == 0 {
		return 0
	}

	m := len(patterns[0])

	for _, pattern := range patterns {
		if m > len(pattern) {
			m = len(pattern)
		}
	}

	return m
}
