package utils

import (
	"errors"
	"net"
	"net/http"
	"strings"
)

const (
	// headerXForwardedFor is a constant for the 'X-Forwarded-For' header.
	headerXForwardedFor string = "X-Forwarded-For"

	// headerXRealIP is a constant for 'X-Real-IP' header.
	headerXRealIP string = "X-Real-IP"

	// headerTrueClientIP is a constant for 'True-Client-IP' header.
	headerTrueClientIP string = "True-Client-IP"

	// headerCFConnectingIP is a constant for Cloudflare's IP ('CF-Connecting-IP') header.
	headerCFConnectingIP string = "CF-Connecting-IP"

	// headerFastlyClientIP is a constant for Fastly's IP ('Fastly-Client-Ip') header.
	headerFastlyClientIP string = "Fastly-Client-Ip"
)

// specialHeaders is a fixed sized slice containing the special headers.
var specialHeaders = [...]string{
	headerXRealIP,
	headerTrueClientIP,
	headerCFConnectingIP,
	headerFastlyClientIP,
}

var (
	ErrNotAValidNetAddress             = errors.New(`address is not valid`)
	ErrEmptyOrInvalidForwardedHeader   = errors.New(`empty or invalid forwarded header`)
	ErrUnableToGetIPFromSpecialHeaders = errors.New(`unable to get ip from special headers`)
)

func retrieveForwardedIP(headerKey string, headers http.Header) (string, error) {
	addresses := strings.Split(headers.Get(headerKey), ",")

	for _, address := range addresses {
		if len(address) == 0 {
			continue
		}

		address = strings.TrimSpace(address)
		ipAddress := net.ParseIP(address)

		if ipAddress == nil {
			return "", ErrNotAValidNetAddress
		}

		return ipAddress.String(), nil
	}

	return "", ErrEmptyOrInvalidForwardedHeader
}

func retrieveSpecialHeaders(headers http.Header) (string, error) {
	for _, header := range specialHeaders {
		if address := headers.Get(header); len(address) > 0 {
			return address, nil
		}
	}

	return "", ErrUnableToGetIPFromSpecialHeaders
}

// RealRemoteAddr first attempts to retrieve the address from the 'X-Forwarded-For'
// if that fails, it attempts to retrieve from the special headers.
func RealRemoteAddr(r *http.Request) (host, port string, err error) {
	host, err = retrieveForwardedIP(headerXForwardedFor, r.Header)

	if err != nil && err != ErrEmptyOrInvalidForwardedHeader {
		return
	}

	_host, _port, _ := net.SplitHostPort(r.RemoteAddr)
	if host != "" {
		return host, _port, nil
	}

	host, err = retrieveSpecialHeaders(r.Header)
	if err == nil && host != "" {
		return host, _port, nil
	}

	return _host, _port, nil
}
