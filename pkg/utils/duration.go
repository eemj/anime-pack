package utils

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func SecondsToDuration(seconds float64) string {
	return fmt.Sprintf(
		"%02d:%02d:%02d.%03d",
		int(seconds/3600), // we'll seek the timeframe from 10 seek
		int(seconds/60.0)%60,
		int(seconds)%60,
		int(seconds)/1000,
	)
}

func DurationToSeconds(duration string) (seconds float64) {
	split := strings.Split(duration, ":")

	if len(split) != 3 {
		return
	}

	hour, _ := strconv.ParseFloat(split[0], 64)
	minute, _ := strconv.ParseFloat(split[1], 64)
	second, _ := strconv.ParseFloat(split[2], 64)

	seconds = (hour * (60 * 60)) + (minute * 60) + second

	return
}

// TillEndOfDay returns the time duration for UTC till we reach end of day.
func TillEndOfDay() time.Duration {
	now := time.Now().UTC()
	y, m, d := now.Date()
	return time.Date(
		y, m, d,
		23, 59, 59,
		int(time.Second-time.Nanosecond),
		now.Location(),
	).Sub(now)
}
