package utils

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
)

func NewIdentifier() string {
	b := make([]byte, 16)

	_, _ = rand.Read(b)

	h := sha1.New()

	fmt.Fprintf(h, "%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return hex.EncodeToString(h.Sum(nil))
}
