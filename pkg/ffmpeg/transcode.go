package ffmpeg

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/analyzer"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

var equalsSpacesRE = regexp.MustCompile(`=\s+`)

// Progress defines a transcode progress.
type Progress struct {
	FramesProcessed string  `json:"framesProcessed"`
	CurrentTime     string  `json:"currentTime"`
	CurrentBitrate  string  `json:"currentBitrate"`
	Percentage      float64 `json:"percantage"`
	Speed           float64 `json:"speed"`
	Duration        float64 `json:"duration"`
	CurrentDuration float64 `json:"currentDuration"`
}

// TranscodeOptions are options. InputOptions are options defined before the source. OutputOptions
// are options defined after the video filters as mappings.
type TranscodeOptions struct {
	InputOptions  []string
	OutputOptions []string
}

// DefaultTranscodeOptions are the default options.
var DefaultTranscodeOptions = TranscodeOptions{
	InputOptions: []string{
		"-hwaccel", "auto",
	},
	OutputOptions: []string{
		"-f", "mp4",
		"-c:v", "libx264",
		"-crf", "18",
		"-preset", "fast",
		"-c:a", "aac",
		"-tune", "animation",
		"-threads", "0",
	},
}

func createFmtError(args []string, innerErr error, stdout, stderr *bytes.Buffer) error {
	sb := new(strings.Builder)
	sb.WriteString("failed to finish ffmpeg `")
	sb.WriteString(sliceutil.SurroundWith('\'', args...))
	sb.WriteString("`")

	if innerErr != nil {
		sb.WriteString(", inner:`")
		sb.WriteString(innerErr.Error())
		sb.WriteString("`")
	}

	if stdout.Len() > 0 {
		sb.WriteString(", stdout:`")
		sb.WriteString(stdout.String())
		sb.WriteString("`")
	}

	if stderr.Len() > 0 {
		sb.WriteString(", stderr:`")
		sb.WriteString(stderr.String())
		sb.WriteString("`")
	}

	return errors.New(sb.String())
}

func CreateArgs(ctx context.Context, source, destination string, tOptions TranscodeOptions) (args []string, duration float64, err error) {
	// get it's the file's options first
	instance, err := analyzer.Analyze(ctx, source)
	if err != nil {
		return
	}

	// subtitleStream holds the subtitle stream that we're dealing with here
	// if we need to hardsub, we'll need to differentiate if we're dealing with
	// text-based subtitles or picture-based as the arguments differ.
	subtitleResult, err := instance.Subtitle(ctx)
	if err != nil {
		return
	}

	audioResult, err := instance.Audio(ctx)
	if err != nil {
		return
	}

	videoResult, err := instance.Video(ctx)
	if err != nil {
		return
	}

	hardsub := subtitleResult.Path != analyzer.SubtitleAnalyzerPath_None

	duration, _ = instance.Duration(ctx)

	args = append([]string{}, tOptions.InputOptions...)

	// append the remaining arguments
	args = append(args, []string{
		"-i", source,
		"-y",          // overwrite
		"-v", "error", // error mode, don't show the streams, but show errors
		"-stats",                  // however, show the progress
		"-hide_banner",            // less string mumbo jumbo
		"-movflags", "+faststart", // faster web playing time
		"-map", ("0:a:" + strconv.Itoa(audioResult.StreamIndex)),
	}...)

	if !(hardsub && subtitleResult.Stream.CodecName == "hdmv_pgs_subtitle") {
		args = append(args, []string{
			"-map",
			("0:v:" + strconv.Itoa(videoResult.StreamIndex)),
		}...)
	}

	if hardsub { // if we need to hardsub..
		// For windows, if it starts with the Drive Letter, we need to escape the ':'
		if runtime.GOOS == "windows" {
			source = filepath.ToSlash(source)

			if len(source) >= 2 &&
				source[1] == ':' &&
				unicode.IsLetter(rune(source[0])) {
				source = string(source[0]) + "\\" + string(source[1:])
			}
		}

		if subtitleResult.Stream.CodecName == datatype.CodecName_HdmvPgsSubtitle { // picture-based subtitles
			filterComplex := ""

			// check if we need to scale the subtitles to match
			// the video dimensions.
			if videoResult.Stream.Width == subtitleResult.Stream.Width &&
				videoResult.Stream.Height == subtitleResult.Stream.Height {
				filterComplex = fmt.Sprintf(
					"[0:v:0][0:s:%d]overlay=0:0:eof_action=pass:eval=init:shortest=0:format=yuv420:repeatlast=0:alpha=straight[v]",
					subtitleResult.StreamIndex,
				)
			} else {
				filterComplex = fmt.Sprintf(
					"[0:s:%d]scale=%d:%d:eval=init:interl=0:flags=area[s];[0:v:0][s]overlay=0:0:eof_action=pass:eval=init:shortest=0:format=yuv420:repeatlast=0:alpha=straight[v]",
					subtitleResult.StreamIndex,
					videoResult.Stream.Width,
					videoResult.Stream.Height,
				)
			}

			args = append(args, []string{
				"-filter_complex",
				// scale the subtitles to the video stream's dimensions to ensure
				// that the subtitles won't go out of bounds.
				filterComplex,
				// overlay
				"-map",
				"[v]",
			}...)

		} else { // assume else are text-based subtitles
			args = append(args, []string{
				// hard-code the pixel format
				"-pix_fmt",
				"yuv420p",

				// no need to overlay, just point the subtitle filter to the input.
				"-vf",
				("subtitles=filename='" + source + "':si=" + strconv.Itoa(subtitleResult.StreamIndex)),
			}...)
		}
	}

	args = append(args, tOptions.OutputOptions...)

	args = append(
		args,
		[]string{
			"-t",
			strconv.FormatFloat(duration, 'f', 6, 64),
		}...,
	)

	args = append(args, destination)

	return
}

// Transcode is specifically created to transcode video files which require some sort
// of burnt subtitles. It'll check for available streams in our source and if it contains
// a subtitle stream, it'll burn the subtitles (hardsub) otherwise it'll leave it and assume
// that the video already contains hardsubs.
// NOTE: assumes you have 'ffmpeg' in your PATH environment variables
func Transcode(ctx context.Context, source, destination string, tOptions TranscodeOptions) (
	progress chan Progress,
	done chan error,
) {
	progress = make(chan Progress)
	done = make(chan error, 1)
	args, duration, err := CreateArgs(ctx, source, destination, tOptions)
	if err != nil {
		done <- err
		close(done)
		return
	}

	// use exec.Command here, instead of exec.CommandContext so we close ffmpeg gracefully.
	proc := exec.Command("ffmpeg", args...)

	stderrPipe, err := proc.StderrPipe()
	if err != nil {
		done <- err
		close(done)
		return
	}

	stderrBuffer := new(bytes.Buffer)

	teeStderr := io.TeeReader(stderrPipe, stderrBuffer)

	stdinPipe, err := proc.StdinPipe()
	if err != nil {
		done <- err
		close(done)
		return
	}

	stdoutBuffer := new(bytes.Buffer)

	proc.Stdout = stdoutBuffer

	err = proc.Start()

	if err != nil {
		done <- createFmtError(args, err, stdoutBuffer, stderrBuffer)
		close(done)
		return
	}

	scanner := customScanner(teeStderr)

	// process done is an internal done,
	// it's main purpose is to let the returned 'done' channel block
	// till the process is closed and had it's error processed.
	processDone := make(chan error, 1)

	go func() { // wait for the process to get closed, and send out an error to the 'processDone' channel
		err := proc.Wait()

		if err != nil {
			processDone <- createFmtError(args, err, stdoutBuffer, stderrBuffer)
		} else {
			processDone <- nil
		}
	}()

	go func() {
		for {
			select {
			case processErr := <-processDone:
				if processErr == nil {
					// replace it with possible error from context
					processErr = ctx.Err()
				}

				done <- processErr
				close(done)

				return
			case <-ctx.Done():
				stdinPipe.Write([]byte(
					"q\n", // send out a 'Q' to stop the process
				))
			default:
				if scanner.Scan() {
					fProgress := ffmpegProgress(scanner.Text(), duration)

					if fProgress.Percentage > 0 {
						progress <- fProgress
					}
				} else if err = scanner.Err(); err != nil {
					done <- err
					return
				}
			}
		}
	}()

	return
}

// ffmpegProgress parses a line from FFMPEG and retusn a progress structure
func ffmpegProgress(line string, duration float64) (prog Progress) {
	if !strings.HasPrefix(line, "frame=") { // starting with 'frame=' is a must otherwise it'll get skipped
		return
	}

	if !(strings.Contains(line, "frame=") && // it must contain the frame
		strings.Contains(line, "time=") && // the time, to calculate the percantage
		strings.Contains(line, "bitrate=")) { // and the bitrate
		return
	}

	st := equalsSpacesRE.ReplaceAllString(line, "=")

	fields := strings.Fields(st)

	var (
		framesProcessed string
		time            string
		bitrate         string
		speed           float64
	)

	for _, field := range fields {
		fieldSplit := strings.Split(field, "=")

		if len(fieldSplit) > 1 {
			name := fieldSplit[0]
			value := fieldSplit[1]

			switch name {
			case "frame":
				framesProcessed = value
			case "time":
				time = value
			case "bitrate":
				bitrate = value
			case "speed":
				speed, _ = strconv.ParseFloat(
					value[:(len(value)-1)],
					64,
				)
			}
		}
	}

	prog.CurrentDuration = utils.DurationToSeconds(time)

	if duration > 0 {
		prog.Percentage = (prog.CurrentDuration * 100) / duration
	}

	prog.CurrentBitrate = bitrate
	prog.FramesProcessed = framesProcessed
	prog.CurrentTime = time
	prog.Speed = speed
	prog.Duration = duration

	return
}

// customScanner creates a new scanner, with a custom split function spefically designed
// to capture carriage retruns and new lines from FFmpeg
func customScanner(reader io.Reader) *bufio.Scanner {
	scanner := bufio.NewScanner(reader)

	scanner.Split(
		func(data []byte, EOF bool) (
			advance int, token []byte, err error,
		) {
			if EOF && len(data) == 0 {
				return
			}

			// check if we have a new line
			newLine := bytes.IndexByte(data, '\n')

			if newLine > -1 {
				advance = newLine + 1
				token = data[0:newLine]
				return
			}

			carriage := bytes.IndexByte(data, '\r')

			if carriage > -1 {
				advance = carriage + 1
				token = data[0:carriage]
				return
			}

			return
		},
	)

	buf := make([]byte, 2)

	scanner.Buffer(buf, bufio.MaxScanTokenSize)

	return scanner
}
