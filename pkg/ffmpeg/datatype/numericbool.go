package datatype

import (
	"fmt"
	"strconv"
)

// NumericBool defines a custom numeric representation of a boolean.
// In this case, if 1 the value is true, otherwise it's false.
type NumericBool int

// UnmarshalJSON transforms 1 / 0 from string form to a numeric value.
func (b *NumericBool) UnmarshalJSON(data []byte) error {
	switch string(data) {
	case "1":
		*b = 1
	case "0":
		*b = 0
	default:
		return fmt.Errorf(
			"unexpected bool representation '%v'",
			data,
		)
	}

	return nil
}

// MarshalJSON prints out the numeric value in quoted string format.
func (b NumericBool) MarshalJSON() ([]byte, error) {
	return []byte(`"` + strconv.Itoa(int(b)) + `"`), nil
}

// Bool returns the bool value representation.
func (b NumericBool) Bool() bool { return b == 1 }
