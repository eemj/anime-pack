package datatype

type Tags struct {
	Language    string      `json:"language"`
	Title       string      `json:"title"`
	DurationEng Sexagesimal `json:"DURATION-eng"`
	Duration    Sexagesimal `json:"DURATION"`
}
