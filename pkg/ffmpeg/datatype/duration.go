package datatype

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

func trimQuotes(s string) string {
	if len(s) >= 2 {
		if s[0] == '"' && s[len(s)-1] == '"' {
			return s[1 : len(s)-1]
		}
	}
	return s
}

type Sexagesimal time.Duration

func (d *Sexagesimal) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(
		`"%s"`,
		utils.SecondsToDuration(time.Duration(*d).Seconds()),
	)), nil
}

func (d *Sexagesimal) UnmarshalJSON(data []byte) error {
	seconds := utils.DurationToSeconds(trimQuotes(string(data)))
	*d = Sexagesimal(time.Second * time.Duration(seconds))
	return nil
}

func (d Sexagesimal) String() string { return d.Unwrap().String() }

func (d Sexagesimal) Unwrap() time.Duration { return time.Duration(d) }

func (d Sexagesimal) ToSeconds() Seconds { return Seconds(d) }

type Seconds time.Duration

func (d *Seconds) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(
		`"%s"`,
		strconv.FormatFloat(d.Unwrap().Seconds(), 'f', 6, 64),
	)), nil
}

func (d *Seconds) UnmarshalJSON(data []byte) error {
	seconds, err := strconv.ParseFloat(trimQuotes(string(data)), 64)
	if err != nil {
		return err
	}

	*d = Seconds(time.Second * time.Duration(seconds))
	return nil
}

func (d Seconds) String() string { return d.Unwrap().String() }

func (d Seconds) Unwrap() time.Duration { return time.Duration(d) }

func (d Seconds) ToSexagesimal() Sexagesimal { return Sexagesimal(d) }
