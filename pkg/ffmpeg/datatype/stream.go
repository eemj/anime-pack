package datatype

// Streams is an array for streams which implements the sort.Interface.
type Streams []*Stream

func (s Streams) Len() int {
	return len(s)
}

func (s Streams) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func (s Streams) Less(i, j int) bool {
	return s[i].Index < s[j].Index
}

// Stream defines the ffprobe's stream object.
type Stream struct {
	Index              int          `json:"index"`
	CodecName          string       `json:"codec_name"`
	CodecLongName      string       `json:"codec_long_name"`
	CodecType          Codec        `json:"codec_type"`
	CodecTimeBase      string       `json:"codec_time_base"`
	PixelFormat        string       `json:"pixfmt"`
	Width              int64        `json:"width"`
	Height             int64        `json:"height"`
	DisplayAspectRatio string       `json:"display_aspect_ratio"`
	SampleAspectRatio  string       `json:"sample_aspect_ratio"`
	Tags               Tags         `json:"tags"`
	StartTime          string       `json:"start_time"`
	StartPts           int          `json:"start_pts"`
	Disposition        *Disposition `json:"disposition,omitempty"`
	NBReadPackets      string       `json:"nb_read_packets"`
}

// Format defines the ffprobe's format object.
type Format struct {
	Duration        Seconds `json:"duration"`
	NumberOfStreams int     `json:"nb_streams"`
	FormatName      string  `json:"format_name"`
	FormatLongName  string  `json:"format_long_name"`
	StartTime       string  `json:"start_time"`
	Size            string  `json:"size"`
	Bitrate         int64   `json:"bitrate"`
}

// Disposition defines the ffprobe stream disposition object.
type Disposition struct {
	Default         NumericBool `json:"default"`
	Dub             NumericBool `json:"dub"`
	Original        NumericBool `json:"original"`
	Comment         NumericBool `json:"comment"`
	Lyrics          NumericBool `json:"lyrics"`
	Karaoke         NumericBool `json:"karaoke"`
	Forced          NumericBool `json:"forced"`
	HearingImpaired NumericBool `json:"hearing_impaired"`
	VisualImpaired  NumericBool `json:"visual_impaired"`
	CleanEffects    NumericBool `json:"clean_effects"`
	AttachedPic     NumericBool `json:"attached_pic"`
	TimedThumbnails NumericBool `json:"timed_thumbnails"`
	Captions        NumericBool `json:"captions"`
	Descriptions    NumericBool `json:"descriptions"`
	Metadata        NumericBool `json:"metadata"`
	Dependent       NumericBool `json:"dependent"`
	StillImage      NumericBool `json:"still_image"`
}

// Options defines the ffprobe root json object.
type Options struct {
	Streams Streams `json:"streams"`
	Format  *Format `json:"format"`
}
