package datatype

type Codec string

const (
	// Codec_Video defines a codec of type video.
	Codec_Video = Codec("video")
	// Codec_Audio defines a codec of type audio.
	Codec_Audio = Codec("audio")
	// Codec_Subtitle defines a codec of type subtitle.
	Codec_Subtitle = Codec("subtitle")
	// Codec_Attachment defines a codec of type attachment.
	Codec_Attachment = Codec("attachment")
)

const (
	// CodecName_HdmvPgsSubtitle represents the name for the `hdmv_pgs_subtitle` codec.
	CodecName_HdmvPgsSubtitle = "hdmv_pgs_subtitle"
)
