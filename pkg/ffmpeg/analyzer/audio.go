package analyzer

import (
	"context"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
)

func (a *analyzer) Audio(ctx context.Context) (result AnalyzeResult[AudioAnalyzerPath], err error) {
	filter := a.getStreamsBy(datatype.Codec_Audio)

	// First.. check if the language or title are set to japanese
	for index, stream := range filter {
		if strings.ToLower(stream.Tags.Language) == "jpn" ||
			strings.ToLower(stream.Tags.Title) == "jap" {
			result = AnalyzeResult[AudioAnalyzerPath]{
				StreamIndex: index,
				Stream:      stream,
				Path:        AudioAnalyzerPath_Japanese,
			}
			return
		}
	}

	// Second.. we'll close an eye if the stream is only with one audio stream
	// & assume that it's in japanese.
	if len(filter) == 1 {
		result = AnalyzeResult[AudioAnalyzerPath]{
			StreamIndex: 0,
			Stream:      filter[0],
			Path:        AudioAnalyzerPath_OnlyOne,
		}
		return
	}

	// Third.. check if we have a default audio stream & if we do.. fallback to it.
	for index, stream := range filter {
		if stream.Disposition != nil && stream.Disposition.Default.Bool() {
			result = AnalyzeResult[AudioAnalyzerPath]{
				StreamIndex: index,
				Stream:      stream,
				Path:        AudioAnalyzerPath_DefaultDisposition,
			}
			return
		}
	}

	err = ErrUnableToAnalyzeAudioStream

	return
}
