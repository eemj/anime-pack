package analyzer

import (
	"context"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
)

func (a *analyzer) Video(ctx context.Context) (result AnalyzeResult[VideoAnaylzerPath], err error) {
	streams := a.getStreamsBy(datatype.Codec_Video)
	return AnalyzeResult[VideoAnaylzerPath]{
		StreamIndex: 0,
		Stream:      streams[0],
		Path:        VideoAnaylzerPath_Default,
	}, nil
}
