package analyzer

import (
	"fmt"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
)

func errUnableToAnalyzeStream(codec datatype.Codec) error {
	return fmt.Errorf("unable to analyze the `%s` stream", codec)
}

var (
	ErrUnableToAnalyzeAudioStream    = errUnableToAnalyzeStream(datatype.Codec_Audio)
	ErrUnableToAnalyzeVideoStream    = errUnableToAnalyzeStream(datatype.Codec_Video)
	ErrUnableToAnalyzeSubtitleStream = errUnableToAnalyzeStream(datatype.Codec_Subtitle)
)
