package analyzer

import (
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
)

type AnalyzerPath interface {
	SubtitleAnalyzerPath | VideoAnaylzerPath | AudioAnalyzerPath
}

type (
	SubtitleAnalyzerPath int
	VideoAnaylzerPath    int
	AudioAnalyzerPath    int
)

const (
	AudioAnalyzerPath_Japanese AudioAnalyzerPath = iota << 1
	AudioAnalyzerPath_OnlyOne
	AudioAnalyzerPath_DefaultDisposition
)

const (
	SubtitleAnalyzerPath_None SubtitleAnalyzerPath = iota << 1
	SubtitleAnalyzerPath_OnlyOne
	SubtitleAnalyzerPath_SingleEnglish
	SubtitleAnalyzerPath_FullSubtitles
	SubtitleAnalyzerPath_StartPts
	SubtitleAnalyzerPath_StreamSize
	SubtitleAnalyzerPath_PacketCount
	SubtitleAnalyzerPath_Unlocalised
	SubtitleAnalyzerPath_FirstEnglish
	SubtitleAnalyzerPath_FirstJapanese
)

const (
	VideoAnaylzerPath_Default VideoAnaylzerPath = iota << 1
)

type AnalyzeResult[P AnalyzerPath] struct {
	StreamIndex int
	Stream      *datatype.Stream
	Path        P
}
