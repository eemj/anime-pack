package analyzer

import (
	"context"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/operation"
)

func (a *analyzer) Subtitle(ctx context.Context) (result AnalyzeResult[SubtitleAnalyzerPath], err error) {
	filter := a.getStreamsBy(datatype.Codec_Subtitle)

	if len(filter) == 0 {
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: -1,
			Stream:      nil,
			Path:        SubtitleAnalyzerPath_None,
		}, nil
	} else if len(filter) == 1 {
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: 0,
			Stream:      filter[0],
			Path:        SubtitleAnalyzerPath_OnlyOne,
		}, nil
	}

	sort.Stable(filter)

	engStreams := make([]streamIndex, 0)
	jpnStreams := make([]streamIndex, 0)

	// Second, filter out streams with languages.
	for index, stream := range filter {
		_streamIndex := streamIndex{Index: index, Stream: stream}

		language := strings.TrimSpace(strings.ToLower(stream.Tags.Language))
		title := strings.TrimSpace(strings.ToLower(stream.Tags.Title))

		if title == "english" || language == "eng" {
			engStreams = append(engStreams, _streamIndex)
		}

		if title == "japanese" || language == "jpn" {
			jpnStreams = append(jpnStreams, _streamIndex)
		}
	}

	if len(engStreams) == 1 {
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: engStreams[0].Index,
			Stream:      engStreams[0].Stream,
			Path:        SubtitleAnalyzerPath_SingleEnglish,
		}, nil
	}

	// Third, filter out streams with titles.
	for _, streamIndex := range engStreams {
		if strings.HasPrefix(
			strings.ToLower(streamIndex.Stream.Tags.Title),
			"full subtitles",
		) {
			return AnalyzeResult[SubtitleAnalyzerPath]{
				StreamIndex: streamIndex.Index,
				Stream:      streamIndex.Stream,
				Path:        SubtitleAnalyzerPath_FullSubtitles,
			}, nil
		}
	}

	// Fourth, if the remaining streams are all type `hdmv_pgs_subtitle`
	// the most sufficient way to tell which is the correct one,
	// is to return the subtitle which starts with the least time.
	pgsCount := 0

	for _, stream := range engStreams {
		if stream.Stream.CodecName == datatype.CodecName_HdmvPgsSubtitle {
			pgsCount++
		}
	}

	// pgsCount could be 0, whilst the filter could be greater than 0
	// which could result in an out of range exception.
	if pgsCount > 0 && filter.Len() == pgsCount {
		previousStream := (*streamIndex)(nil)
		for index, stream := range engStreams {
			if stream.Stream.Width == 0 && stream.Stream.Height == 0 {
				continue
			}

			// If `previousStream` == nil.
			// If `currentStream.StartPts` is the same as the `previousStream.StartPts` and the `currentStream.IsDefault` is true.
			// If `currentStream.StartPts` is less than `previousStream.StartPts`.
			if previousStream == nil ||
				(stream.Stream.StartPts == previousStream.Stream.StartPts && !stream.Stream.Disposition.Default.Bool()) ||
				stream.Stream.StartPts < previousStream.Stream.StartPts {
				previousStream = &engStreams[index]
			}
		}
		if previousStream != nil {
			return AnalyzeResult[SubtitleAnalyzerPath]{
				StreamIndex: previousStream.Index,
				Stream:      previousStream.Stream,
				Path:        SubtitleAnalyzerPath_StartPts,
			}, nil
		}

		// Fifth, we'll sort by the number of packets read and the stream
		// with the most packets surely has the proper subtitles :)
		packets := make([]streamPacketCount, 0)
		for _, stream := range engStreams {
			packetCount, err := strconv.ParseInt(stream.Stream.NBReadPackets, 10, 64)
			if err != nil {
				continue
			}
			packets = append(packets, streamPacketCount{
				PacketCount: packetCount,
				StreamIndex: stream,
			})
		}
		if len(packets) == pgsCount {
			sort.Slice(packets, func(i, j int) bool {
				return packets[i].PacketCount > packets[j].PacketCount
			})

			return AnalyzeResult[SubtitleAnalyzerPath]{
				StreamIndex: packets[0].StreamIndex.Index,
				Stream:      packets[0].StreamIndex.Stream,
				Path:        SubtitleAnalyzerPath_PacketCount,
			}, nil
		}

		// Sixth, retrieve the file size for each and everyone, and return
		// the largest one.
		sizes := make([]streamSize, 0, pgsCount)
		for _, streamIndex := range engStreams {
			size, err := operation.GetStreamSize(ctx, a.source, streamIndex.Index)
			if err != nil {
				return AnalyzeResult[SubtitleAnalyzerPath]{
					StreamIndex: -1,
					Stream:      nil,
					Path:        SubtitleAnalyzerPath_None,
				}, err
			}

			sizes = append(sizes, streamSize{
				StreamIndex: streamIndex,
				Size:        size,
			})
		}
		sort.SliceStable(sizes, func(i, j int) bool {
			return sizes[i].Size > sizes[j].Size // Reverse the less
		})
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: sizes[0].StreamIndex.Index,
			Stream:      sizes[0].StreamIndex.Stream,
			Path:        SubtitleAnalyzerPath_StreamSize,
		}, nil
	}

	// Seventh.. certain subtitle titles are localised/unlocalised.
	// Unlocalised contains subtitle streams which end with "San/Chan/Kun... etc"
	// and those are preferred. (as like 'HorribleSubs' & 'SubsPlease' streams)
	for _, streamIndex := range engStreams {
		if strings.Contains(
			strings.ToLower(streamIndex.Stream.Tags.Title),
			"unlocalized",
		) {
			return AnalyzeResult[SubtitleAnalyzerPath]{
				StreamIndex: streamIndex.Index,
				Stream:      streamIndex.Stream,
				Path:        SubtitleAnalyzerPath_Unlocalised,
			}, nil
		}
	}

	// Eight.. if we still have an english stream.. we'll return the 1st one.
	if len(engStreams) > 0 {
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: engStreams[0].Index,
			Stream:      engStreams[0].Stream,
			Path:        SubtitleAnalyzerPath_FirstEnglish,
		}, nil
	}

	// Ninth.. we'll switch to japanese streams..
	if len(jpnStreams) > 0 {
		// We'd prefer ASS subtitles over anything else tbh..
		sort.SliceStable(jpnStreams, func(i, j int) bool {
			return jpnStreams[i].Stream.CodecName == "ass" // ASS (Advanced SSA) subtitle
		})

		// Return the 1st stream.
		return AnalyzeResult[SubtitleAnalyzerPath]{
			StreamIndex: jpnStreams[0].Index,
			Stream:      jpnStreams[0].Stream,
			Path:        SubtitleAnalyzerPath_FirstJapanese,
		}, nil
	}

	// Tenth.. I give up.
	err = ErrUnableToAnalyzeSubtitleStream

	return
}
