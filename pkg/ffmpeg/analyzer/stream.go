package analyzer

import "gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"

type streamPacketCount struct {
	StreamIndex streamIndex
	PacketCount int64
}

// streamSize is used to keep in-store the index & size of the stream.
type streamSize struct {
	StreamIndex streamIndex
	Size        int64
}

// streamIndex is used to keep in-store the index location of the stream.
type streamIndex struct {
	Index  int
	Stream *datatype.Stream
}
