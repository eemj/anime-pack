package analyzer

import "context"

func (a *analyzer) Duration(ctx context.Context) (duration float64, err error) {
	duration = a.options.Format.Duration.Unwrap().Seconds()
	return
}
