package analyzer

import (
	"context"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/operation"
)

type Analyzer interface {
	// Subtitle will return the subtitle stream, after numerous of checks it'll
	// return the stream index, the stream and an error if we weren't able
	// to analyze what stream to use. If the index is -1 and there are no errors, it means
	// that this video has no subtitle stream and it's either hardsubbed or raw.
	Subtitle(ctx context.Context) (result AnalyzeResult[SubtitleAnalyzerPath], err error)

	Video(ctx context.Context) (result AnalyzeResult[VideoAnaylzerPath], err error)

	Audio(ctx context.Context) (result AnalyzeResult[AudioAnalyzerPath], err error)

	// Duration will attempt to retrieve the correct duration for a media file.
	// There are some cases where the video's duration is set to an hour but the actual
	// episode's duration is set to 24 minutes.
	Duration(ctx context.Context) (duration float64, err error)
}

type analyzer struct {
	source  string
	options *datatype.Options
}

// Analyze will return an analyzer
func Analyze(ctx context.Context, source string) (Analyzer, error) {
	options, err := operation.GetOptions(ctx, source)
	if err != nil {
		return nil, err
	}

	return &analyzer{source, options}, nil
}

func (a *analyzer) getStreamsBy(codec datatype.Codec) datatype.Streams {
	streams := make(datatype.Streams, 0)

	for _, stream := range a.options.Streams {
		if stream.CodecType == codec {
			streams = append(streams, stream)
		}
	}

	return streams
}
