package operation

import (
	"bufio"
	"context"
	"encoding/json"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const (
	Resolution string  = "854x480"
	SeekFrom   float64 = 0.4
)

func GetOptions(ctx context.Context, source string) (options *datatype.Options, err error) {
	options = new(datatype.Options)

	cmd := exec.CommandContext(
		ctx,
		"ffprobe",
		"-i",
		source,
		"-print_format",
		"json",
		"-count_packets",
		"-show_format",
		"-show_streams",
		"-show_error",
	)

	output, err := cmd.Output()
	if err != nil {
		return
	}

	err = json.Unmarshal(output, options)

	return
}

func GetStreams(ctx context.Context, source string) (streams []*datatype.Stream, err error) {
	options, err := GetOptions(ctx, source)
	if err != nil {
		return
	}

	streams = options.Streams

	return
}

func GetStreamSize(ctx context.Context, source string, index int) (size int64, err error) {
	cmd := exec.CommandContext(
		ctx,
		"ffprobe",
		"-v",
		"error",
		"-show_packets",
		"-select_streams",
		strconv.Itoa(index),
		"-show_entries",
		"packet=size",
		"-of",
		"default=noprint_wrappers=1:nokey=1",
		source,
	)

	stdoutReader, err := cmd.StdoutPipe()
	if err != nil {
		return
	}

	defer stdoutReader.Close()

	if err = cmd.Start(); err != nil {
		return
	}

	defer cmd.Wait()

	scanner := bufio.NewScanner(stdoutReader)

	for scanner.Scan() {
		chunk, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			return 0, err
		}

		size += chunk
	}

	if err = scanner.Err(); err != nil {
		return 0, err
	}

	return
}

func GetDuration(ctx context.Context, source string) (duration float64, err error) {
	cmd := exec.CommandContext(
		ctx,
		"ffprobe",
		"-show_entries",
		"format=duration",
		"-v",
		"error",
		"-of",
		"default=noprint_wrappers=1:nokey=1",
		source,
	)

	output, err := cmd.Output()
	if err != nil {
		return
	}

	duration, err = strconv.ParseFloat(strings.TrimSpace(string(output)), 64)

	return
}

func GenerateThumbnail(
	ctx context.Context,
	source, destination string,
	duration float64,
) (err error) {
	cmd := exec.CommandContext(
		ctx,
		"ffmpeg",
		"-y",
		"-ss",
		utils.SecondsToDuration(duration*SeekFrom),
		"-i",
		source,
		"-s",
		Resolution,
		"-vframes",
		"1",
		destination,
	)

	if err = cmd.Run(); err != nil {
		stat, err := os.Stat(destination)
		if os.IsNotExist(err) || stat.Size() == 0 {
			return err
		}
	}

	return nil
}

// GetVideoDimensions executes ffprobe iterates through every video stream and returns the maximum
// number for both the height & width dimensions.
func GetVideoDimensions(ctx context.Context, source string) (width, height int64, err error) {
	options, err := GetOptions(ctx, source)
	if err != nil {
		return -1, -1, err
	}

	// Iterate through each and every stream, filter them by video obviously
	// and return the maximum number of height & width.
	for _, stream := range options.Streams {
		if stream.CodecType == datatype.Codec_Video {
			if stream.Height > height {
				height = stream.Height
			}

			if stream.Width > width {
				width = stream.Width
			}
		}
	}

	return width, height, nil
}
