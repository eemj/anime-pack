package dcc

import (
	"context"
	"io"
	"net"
	"time"
)

// Progress contains the progression of the
// download handled by the DCC client socket.
type Progress struct {
	Speed           float64
	Percentage      float64
	CurrentFileSize float64
	FileSize        float64
}

// Clone creates a clone of a dcc progress.
func (p Progress) Clone() Progress {
	return Progress{
		Speed:           p.Speed,
		Percentage:      p.Percentage,
		CurrentFileSize: p.CurrentFileSize,
		FileSize:        p.FileSize,
	}
}

// DCC creates a new socket client instance where
// it'll download the DCC transaction into the
// specified io.Writer destination.
type DCC interface {
	// Run established the connection with the DCC TCP socket
	// and returns two channels, where one is used for the download progress
	// and the other is used to return exceptions during our transaction.
	// A context is required, where you have the ability to cancel and timeout
	// a download.
	// One should check the second value for the progress/error channels when
	// receiving data as if the channels are closed, it means that the transaction
	// is finished or got interrupted.
	Run(ctx context.Context) (<-chan Progress, <-chan error)
}

type dcc struct {
	// important properties
	address string
	size    int

	// output channels used for the Run and the receiver methods()
	// to avoid parameter passing
	progressc chan Progress
	done      chan error

	// internal DCC socket connection
	conn net.Conn

	// destination writer
	writer io.Writer
}

// NewDCC creates a new DCC instance.
// the host, port are needed for the socket client connection
// the size is required so the download progress is calculated
// the writer is required to store the transaction fragments into
// the specified io.Writer
func NewDCC(
	address string,
	size int,
	writer io.Writer,
) DCC {
	return &dcc{
		address: address,
		size:    size,
		writer:  writer,
	}
}

func (d *dcc) progress(written float64, speed *float64) {
	d.progressc <- Progress{
		Speed:           written - *speed,
		Percentage:      (written / float64(d.size)) * 100,
		CurrentFileSize: written,
		FileSize:        float64(d.size),
	}

	*speed = float64(written)
}

func (d *dcc) receive(ctx context.Context) {
	defer func() { // close channels
		close(d.done)

		// close the connection afterwards..
		d.conn.Close()
	}()

	var (
		written int
		speed   float64
		buf     = make([]byte, 30720)
		reader  = io.LimitReader(d.conn, int64(d.size))
		ticker  = time.NewTicker(time.Second)
	)

	defer ticker.Stop()

D:
	for {
		select {
		case <-ctx.Done():
			d.done <- nil // send empty to notify the watchers that we're done
			return        // terminated..
		case <-ticker.C:
			d.progress(float64(written), &speed)
		default:
			n, err := reader.Read(buf)
			if err != nil {
				if err == io.EOF { // swallow the error
					err = nil
				}

				d.progress(float64(written), &speed)
				d.done <- err

				return
			}

			if n > 0 {
				_, err = d.writer.Write(buf[0:n])

				if err != nil {
					d.done <- err
					return
				} else if written >= d.size { // finished
					break D
				}

				written += n
			}
		}
	}
}

func (d *dcc) Run(ctx context.Context) (<-chan Progress, <-chan error) {
	// assign the output to the struct properties
	d.progressc = make(chan Progress, 1)
	d.done = make(chan error, 1)

	// assign the passed context
	dialer := net.Dialer{}

	conn, err := dialer.DialContext(
		ctx, "tcp", d.address,
	)
	if err != nil {
		d.done <- err
		return d.progressc, d.done
	}

	// setup the connection for the receiver
	d.conn = conn

	go d.receive(ctx)

	return d.progressc, d.done
}
