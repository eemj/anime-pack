package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	bootstrap "gitlab.com/eemj/anime-pack/internal/bootstrap/runner"
	"go.uber.org/zap"
)

func main() {
	b, err := bootstrap.Create()
	if err != nil {
		b.L().Fatal("runner", zap.Error(err))
	}

	b.L().Info("runner", zap.Int("pid", os.Getpid()), zap.Int("parent_pid", os.Getppid()))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if err = b.Run(ctx); err != nil {
		b.L().Fatal("runner", zap.Error(err))
	}

	signalc := make(chan os.Signal, 1)
	signal.Notify(signalc, os.Interrupt, syscall.SIGHUP)

	for signal := range signalc {
		b.L().Info(
			"runner",
			zap.Stringer("signal", signal),
		)

		if signal != syscall.SIGHUP {
			break
		}

		err = b.Reload(ctx)
		if err != nil {
			b.L().Fatal("config", zap.Error(err))
		}
	}

	b.L().Info("closing...")
}
