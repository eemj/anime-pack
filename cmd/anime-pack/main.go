package main

import (
	"context"
	"os/signal"
	"syscall"

	"gitlab.com/eemj/anime-pack/internal/bootstrap/core"

	"go.jamie.mt/logx/log"
)

// Version is passed as an argument during compilation
// specifying the build version.
var Version = ""

func main() {
	app, err := core.Create(Version)
	if err != nil {
		log.Fatal(err)
	}
	defer app.Close()

	ctx, cancel := signal.NotifyContext(
		context.Background(),
		syscall.SIGINT,
		syscall.SIGKILL,
		syscall.SIGTERM,
	)
	defer cancel()

	if err = app.Run(ctx); err != nil {
		log.Fatal(err)
	}

	<-ctx.Done()
}
