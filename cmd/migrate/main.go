package main

import (
	"embed"
	"flag"

	"gitlab.com/eemj/anime-pack/internal/config"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configparser"
	migrateutil "go.jamie.mt/toolbox/migrate_util"
	"go.uber.org/zap"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	_ "github.com/lib/pq"
)

const (
	// ApplicationName defines the application name used for tracing and
	// as a configuration directory.
	ApplicationName = "anime-pack-migrate"

	// EnvironmentPrefix defines the environment prefix used to parse configurations
	EnvironmentPrefix = "ANIME_PACK"
)

var (
	//go:embed migrations/*.sql
	migrationFs embed.FS

	// `down` determines if the schema should be dropped.
	down = false

	// Version provided via build script.
	Version string = "devel"

	// GitSHA provided via the build script.
	GitSHA string = ""
)

func main() {
	flag.BoolVar(&down, "down", false, "drop everything")
	flag.Parse()

	log.Create(zap.InfoLevel, log.ConsoleCore)

	log.Infow(
		"runtime",
		zap.String("version", Version),
		zap.String("git_sha", GitSHA),
	)

	cfg, err := configparser.Parse[*config.ConfigMigrate](configparser.ParserOptions{
		ApplicationName:      ApplicationName,
		ConfigDelimiter:      configparser.DefaultConfigDelimiter,
		EnvironmentDelimiter: configparser.DefaultEnvironmentDelimiter,
		EnvironmentReplacer:  EnvironmentPrefix,
		Filename:             configparser.DefaultFilename,
	})
	if err != nil {
		log.Fatal(err)
	}

	driver, err := iofs.New(migrationFs, "migrations")
	if err != nil {
		log.Fatal(err)
	}

	defer driver.Close()

	log.Named("database").
		Info(
			"connection",
			zap.Stringer("url", cfg.Database),
		)

	migrator, err := migrate.NewWithSourceInstance(
		"iofs",
		driver,
		cfg.Database.UnsafeString(),
	)
	if err != nil {
		log.Fatal(err)
	}

	defer migrator.Close()

	L := log.Named("migrate")

	migrator.Log = migrateutil.NewLogger(L)

	if down {
		err = migrator.Down()
	} else {
		err = migrator.Up()
	}

	if err != nil {
		if err == migrate.ErrNoChange {
			return
		}
		if dirtyErr, ok := err.(migrate.ErrDirty); ok {
			L.Info("forcing version", zap.Int("version", dirtyErr.Version))

			if err := migrator.Force(dirtyErr.Version); err != nil {
				log.Fatal(err)
			}
			return
		}
		log.Fatal(err)
	}
}
