create type anime_format as enum(
    'UNDEFINED',
    'TV',
    'TV_SHORT',
    'OVA',
    'MOVIE',
    'SPECIAL',
    'ONA',
    'MUSIC'
);

create type anime_status as enum(
    'UNDEFINED',
    'FINISHED',
    'RELEASING',
    'NOT_YET_RELEASED',
    'CANCELLED',
    'HIATUS'
);

create type anime_season as enum(
    'UNDEFINED',
    'WINTER',
    'FALL',
    'SUMMER',
    'SPRING'
);