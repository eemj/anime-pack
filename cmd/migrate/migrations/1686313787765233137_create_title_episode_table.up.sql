create sequence if not exists title_episode_id_seq;

create table if not exists title_episode
(
	id bigint default nextval('title_episode_id_seq'::regclass) not null,
	title_id bigint not null,
	episode_id bigint not null,
	created_at timestamp with time zone not null,
	deleted_at timestamp with time zone
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_episode'::regclass
        and conname = 'title_episode_pkey'
    ) then
        alter table title_episode
        add constraint title_episode_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_episode'::regclass
        and conname = 'fk_title_episode_episode'
    ) then
        alter table title_episode
        add constraint fk_title_episode_episode
        foreign key (episode_id) references episode;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_episode'::regclass
        and conname = 'fk_title_episode_title'
    ) then
        alter table title_episode
        add constraint fk_title_episode_title
        foreign key (title_id) references title;
    end if;
end $$;