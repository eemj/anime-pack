alter table title
add column season_number varchar(3);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title'::regclass
        and conname = 'idx_title_name_season_number'
    ) then
        alter table title
        add constraint idx_title_name_season_number
        unique (name, coalesce(season_number, '0'));
    end if;
end $$;
