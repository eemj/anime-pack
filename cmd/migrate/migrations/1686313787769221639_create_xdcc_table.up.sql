create sequence if not exists xdcc_id_seq;

create table if not exists xdcc
(
	id bigint default nextval('xdcc_id_seq'::regclass) not null,
	pack bigint not null,
	size bigint not null,
	filename text not null,
	bot_id bigint not null,
	quality_id bigint,
	release_group_id bigint not null,
	title_episode_id bigint not null,
	deleted_at timestamp with time zone,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'xdcc'::regclass
        and conname = 'xdcc_pkey'
    ) then
        alter table xdcc
        add constraint xdcc_pkey
		primary key (id);
    end if;

    if not exists (
        select 1
        from pg_indexes
        where tablename = 'xdcc'
        and indexname = 'xdcc_title_episode_id'
    ) then
        create index xdcc_title_episode_id on xdcc (title_episode_id);
    end if;

    if not exists (
        select 1
        from pg_indexes
        where tablename = 'xdcc'
        and indexname = 'xdcc_title_episode_id_quality_id_release_group_id'
    ) then
        create index xdcc_title_episode_id_quality_id_release_group_id on xdcc (title_episode_id, quality_id, release_group_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'video'::regclass
        and conname = 'fk_video_xdcc'
    ) then
        alter table video
        add constraint fk_video_xdcc
        foreign key (xdcc_id) references xdcc;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'xdcc'::regclass
        and conname = 'fk_xdcc_bot'
    ) then
        alter table xdcc
        add constraint fk_xdcc_bot
        foreign key (bot_id) references bot;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'xdcc'::regclass
        and conname = 'fk_xdcc_release_group'
    ) then
        alter table xdcc
        add constraint fk_xdcc_release_group
        foreign key (release_group_id) references release_group;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'xdcc'::regclass
        and conname = 'fk_xdcc_title_episode'
    ) then
        alter table xdcc
        add constraint fk_xdcc_title_episode
        foreign key (title_episode_id) references title_episode;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'xdcc'::regclass
        and conname = 'fk_xdcc_quality'
    ) then
        alter table xdcc
        add constraint fk_xdcc_quality
        foreign key (quality_id) references quality;
    end if;
end $$;