create sequence if not exists title_id_seq;

create table if not exists title
(
	id bigint default nextval('title_id_seq'::regclass) not null,
	name text not null,
	reviewed boolean default false,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null,
	deleted_at timestamp with time zone
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title'::regclass
        and conname = 'title_pkey'
    ) then
        alter table title
        add constraint title_pkey
		primary key (id);
    end if;

    if exists (
        select 1
        from pg_constraint
        where conrelid = 'title'::regclass
        and conname = 'idx_title_name'
    ) then
        alter table title
        drop constraint idx_title_name;
    end if;
end $$;
