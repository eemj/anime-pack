create sequence if not exists genre_id_seq;

create table if not exists genre
(
	id bigint default nextval('genre_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'genre'::regclass
        and conname = 'genre_pkey'
    ) then
        alter table genre
        add constraint genre_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime_genre'::regclass
        and conname = 'fk_anime_genre_genre'
    ) then
        alter table anime_genre
        add constraint fk_anime_genre_genre
        foreign key (genre_id) references genre;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'genre'::regclass
        and conname = 'idx_genre_name'
    ) then
        alter table genre
        add constraint idx_genre_name
        unique (name);
    end if;
end $$;