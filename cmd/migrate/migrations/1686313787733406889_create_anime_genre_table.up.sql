create table if not exists anime_genre
(
	anime_id bigint not null,
	genre_id bigint not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime_genre'::regclass
        and conname = 'anime_genre_pkey'
    ) then
        alter table anime_genre
        add constraint anime_genre_pkey primary key (anime_id, genre_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime_genre'::regclass
        and conname = 'fk_anime_genre_anime'
    ) then
        alter table anime_genre
        add constraint fk_anime_genre_anime
        foreign key (anime_id) references anime;
    end if;
end $$;