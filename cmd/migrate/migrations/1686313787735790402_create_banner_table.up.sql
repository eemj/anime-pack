create sequence if not exists banner_id_seq;

create table if not exists banner
(
	id bigint default nextval('banner_id_seq'::regclass) not null,
	name varchar(255) not null,
	hash varchar(32) not null,
	uri varchar(255) not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'banner'::regclass
        and conname = 'banner_id'
    ) then
        alter table banner
        add constraint banner_id
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'anime_banner_id_fkey'
    ) then
        alter table anime
        add constraint anime_banner_id_fkey
        foreign key (banner_id) references banner;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'banner'::regclass
        and conname = 'banner_hash_uri_unique'
    ) then
        alter table banner
        add constraint banner_hash_uri_unique
        unique (uri, hash);
    end if;
end $$;