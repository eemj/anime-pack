create sequence if not exists runner_id_seq;

create table if not exists runner
(
	id bigint default nextval('runner_id_seq'::regclass) not null,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null,
	name text not null,
	"user" text not null,
	ident text not null,
	hostname text not null,
	signature text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'runner'::regclass
        and conname = 'runner_pkey'
    ) then
        alter table runner
        add constraint runner_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'runner'::regclass
        and conname = 'idx_runner_alias'
    ) then
        alter table runner
        add constraint idx_runner_alias
        unique (name);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'runner'::regclass
        and conname = 'idx_runner_ident'
    ) then
        alter table runner
        add constraint idx_runner_ident
        unique (ident);
    end if;
end $$;