create or replace view anime_reviewed as (
    select distinct anime_id
    from title_anime
    inner join title on title_anime.title_id = title.id
    where title.reviewed = true and title.deleted_at is null
);
