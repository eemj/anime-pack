create sequence if not exists thumbnail_id_seq;

create table if not exists thumbnail
(
	id bigint default nextval('thumbnail_id_seq'::regclass) not null,
	name varchar(255) not null,
	hash varchar(32) not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'thumbnail'::regclass
        and conname = 'thumbnail_id'
    ) then
        alter table thumbnail
        add constraint thumbnail_id
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'thumbnail'::regclass
        and conname = 'thumbnail_hash_unique'
    ) then
        alter table thumbnail
        add constraint thumbnail_hash_unique
		unique (hash);
    end if;
end $$;