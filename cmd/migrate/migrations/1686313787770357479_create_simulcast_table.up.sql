create table if not exists simulcast
(
	title_id bigint not null,
	weekday bigint not null,
	hour smallint not null,
	minute smallint not null,
	second smallint not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'simulcast'::regclass
        and conname = 'simulcast_pkey'
    ) then
        alter table simulcast
        add constraint simulcast_pkey
        primary key (title_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'simulcast'::regclass
        and conname = 'fk_simulcast_title'
    ) then
        alter table simulcast
        add constraint fk_simulcast_title
        foreign key (title_id) references title;
    end if;
end $$;