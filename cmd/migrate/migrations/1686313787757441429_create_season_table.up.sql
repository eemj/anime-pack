create sequence if not exists season_id_seq;

create table if not exists season
(
	id bigint default nextval('season_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'season'::regclass
        and conname = 'season_pkey'
    ) then
        alter table season
        add constraint season_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'fk_anime_season'
    ) then
        alter table anime
        add constraint fk_anime_season
		foreign key (season_id) references season;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'season'::regclass
        and conname = 'idx_season_name'
    ) then
        alter table season
        add constraint idx_season_name
		unique (name);
    end if;
end $$;
