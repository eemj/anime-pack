create sequence if not exists episode_id_seq;

create table if not exists episode
(
	id bigint default nextval('episode_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'episode'::regclass
        and conname = 'episode_pkey'
    ) then
        alter table episode
        add constraint episode_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'episode'::regclass
        and conname = 'idx_episode_name'
    ) then
        alter table episode
        add constraint idx_episode_name
        unique (name);
    end if;
end $$;