create table if not exists anime_studio
(
	anime_id bigint not null,
	studio_id bigint not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime_studio'::regclass
        and conname = 'anime_studio_pkey'
    ) then
        alter table anime_studio
        add constraint anime_studio_pkey
        primary key (anime_id, studio_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime_studio'::regclass
        and conname = 'fk_anime_studio_anime'
    ) then
        alter table anime_studio
        add constraint fk_anime_studio_anime
        foreign key (anime_id) references anime;
    end if;
end $$;