-- Title
update title set season_number = '' where season_number is null;
update title set reviewed = false where reviewed is null;

alter table title alter column season_number type varchar(10) using season_number::varchar(10);

alter table title
alter column season_number set not null,
alter column reviewed set not null;
