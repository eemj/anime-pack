create table if not exists preferences
(
	anime_id bigint not null,
	favourite boolean default false,
	automatic_downloads boolean default false,
	perform_checksum boolean default true,
	created_at timestamp with time zone not null,
	updated_at timestamp with time zone not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'preferences'::regclass
        and conname = 'preferences_pkey'
    ) then
        alter table preferences
        add constraint preferences_pkey
        primary key (anime_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'preferences'::regclass
        and conname = 'fk_preferences_anime'
    ) then
        alter table preferences
        add constraint fk_preferences_anime
        foreign key (anime_id) references anime;
    end if;
end $$;