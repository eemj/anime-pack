create sequence if not exists poster_id_seq;

create table if not exists poster
(
	id bigint default nextval('poster_id_seq'::regclass) not null,
	name varchar(255) not null,
	hash varchar(32) not null,
	uri varchar(255) not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'poster'::regclass
        and conname = 'poster_id'
    ) then
        alter table poster
        add constraint poster_id
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'anime_poster_id_fkey'
    ) then
        alter table anime
        add constraint anime_poster_id_fkey
        foreign key (poster_id) references poster;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'poster'::regclass
        and conname = 'poster_hash_uri_unique'
    ) then
        alter table poster
        add constraint poster_hash_uri_unique
        unique (uri, hash);
    end if;
end $$;