-- Add the 3 columns
alter table anime
    add column "status" anime_status not null default 'UNDEFINED'::anime_status,
    add column "format" anime_format not null default 'UNDEFINED'::anime_format,
    add column "season" anime_season;

-- Update all the records based on the ID to the enum value.
update anime set
format = (case
    when format_id = 0 then 'UNDEFINED'::anime_format
    when format_id = 1 then 'TV'::anime_format
    when format_id = 2 then 'TV_SHORT'::anime_format
    when format_id = 3 then 'OVA'::anime_format
    when format_id = 4 then 'MOVIE'::anime_format
    when format_id = 5 then 'SPECIAL'::anime_format
    when format_id = 6 then 'ONA'::anime_format
    when format_id = 7 then 'MUSIC'::anime_format
    else 'UNDEFINED'::anime_format
    end),
status = (case
    when status_id = 0 then 'UNDEFINED'::anime_status
    when status_id = 1 then 'FINISHED'::anime_status
    when status_id = 2 then 'RELEASING'::anime_status
    when status_id = 3 then 'NOT_YET_RELEASED'::anime_status
    when status_id = 4 then 'CANCELLED'::anime_status
    when status_id = 5 then 'HIATUS'::anime_status
    else 'UNDEFINED'::anime_status
    end),
season = (case
    when season_id = 0 then 'UNDEFINED'::anime_season
    when season_id = 1 then 'WINTER'::anime_season
    when season_id = 2 then 'FALL'::anime_season
    when season_id = 3 then 'SUMMER'::anime_season
    when season_id = 4 then 'SPRING'::anime_season
end);

-- Drop the other columns, they're not needed anymore.
alter table anime
    drop column if exists status_id,
    drop column if exists format_id,
    drop column if exists season_id;
