alter table xdcc
add is_deleted boolean not null generated always as (deleted_at is not null) stored;

alter table title_episode
add is_deleted boolean not null generated always as (deleted_at is not null) stored;

alter table title
add is_deleted boolean not null generated always as (deleted_at is not null) stored;
