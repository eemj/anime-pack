-- vim: set ft=sql:

create or replace view vw_xdcc_title_episode as
select title_episode.id,
       title_episode.title_id,
       title_episode.episode_id,
       title_episode.created_at,
       title_episode.is_deleted,
       title_episode.deleted_at
from title_episode
where not title_episode.is_deleted
  and exists ( select 1 from xdcc where xdcc.title_episode_id = title_episode.id and not xdcc.is_deleted );
