create sequence if not exists status_id_seq;

create table if not exists status
(
	id bigint default nextval('status_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'status'::regclass
        and conname = 'status_pkey'
    ) then
        alter table status
        add constraint status_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'fk_anime_status'
    ) then
        alter table anime
        add constraint fk_anime_status
		foreign key (status_id) references status;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'status'::regclass
        and conname = 'idx_status_name'
    ) then
        alter table status
        add constraint idx_status_name
		unique (name);
    end if;
end $$;
