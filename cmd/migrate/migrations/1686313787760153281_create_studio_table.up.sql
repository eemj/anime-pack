create sequence if not exists studio_id_seq;

create table if not exists studio
(
	id bigint default nextval('studio_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'studio'::regclass
        and conname = 'studio_pkey'
    ) then
        alter table studio
        add constraint studio_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'studio'::regclass
        and conname = 'fk_anime_studio'
    ) then
        alter table anime_studio
        add constraint fk_anime_studio
		foreign key (studio_id) references studio;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'studio'::regclass
        and conname = 'idx_studio_name'
    ) then
        alter table studio
        add constraint idx_studio_name
		unique (name);
    end if;
end $$;