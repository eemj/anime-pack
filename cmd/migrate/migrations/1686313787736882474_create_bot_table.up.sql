create sequence if not exists bot_id_seq;

create table if not exists bot
(
	id bigint default nextval('bot_id_seq'::regclass) not null,
	name varchar(100) not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'bot'::regclass
        and conname = 'bot_pkey'
    ) then
        alter table bot
        add constraint bot_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'bot'::regclass
        and conname = 'idx_bot_name'
    ) then
        alter table bot
        add constraint idx_bot_name
        unique (name);
    end if;
end $$;