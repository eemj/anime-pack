create sequence if not exists quality_id_seq;

create table if not exists quality
(
	id bigint default nextval('quality_id_seq'::regclass) not null,
	height bigint not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'quality'::regclass
        and conname = 'quality_pkey'
    ) then
        alter table quality
        add constraint quality_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'quality'::regclass
        and conname = 'idx_quality_height'
    ) then
        alter table quality
        add constraint idx_quality_height
        unique (height);
    end if;
end $$;