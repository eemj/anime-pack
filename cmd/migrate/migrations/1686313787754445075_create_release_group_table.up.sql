create sequence if not exists release_group_id_seq;

create table if not exists release_group
(
	id bigint default nextval('release_group_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if exists ( -- Before the table got renamed
        select 1
        from pg_constraint
        where conrelid = 'release_group'::regclass
        and conname = 'fansub_pkey'
    ) then
        alter index fansub_pkey rename to release_group_pkey;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'release_group'::regclass
        and conname = 'release_group_pkey'
    ) then
        alter table release_group
        add constraint release_group_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'release_group'::regclass
        and conname = 'idx_release_group_name'
    ) then
        alter table release_group
        add constraint idx_release_group_name
        unique (name);
    end if;
end $$;
