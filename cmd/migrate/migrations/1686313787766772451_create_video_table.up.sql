create sequence if not exists video_id_seq;

create table if not exists video
(
	id bigint default nextval('video_id_seq'::regclass) not null,
	path text not null,
	duration numeric not null,
	size bigint not null,
	crc32 varchar(8) not null,
	thumbnail_id bigint not null,
	xdcc_id bigint not null,
	quality_id bigint,
	created_at timestamp with time zone default now() not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'video'::regclass
        and conname = 'video_pkey'
    ) then
        alter table video
        add constraint video_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'video'::regclass
        and conname = 'idx_video_path'
    ) then
        alter table video
        add constraint idx_video_path
        unique (path);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'video'::regclass
        and conname = 'thumbnail_id_xdcc_id_unqiue'
    ) then
        alter table video
        add constraint thumbnail_id_xdcc_id_unqiue
        unique (thumbnail_id, xdcc_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'video'::regclass
        and conname = 'fk_video_thumbnail'
    ) then
        alter table video
        add constraint fk_video_thumbnail
        foreign key (thumbnail_id) references thumbnail;
    end if;
end $$;
