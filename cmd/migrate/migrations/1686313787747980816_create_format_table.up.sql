create sequence if not exists format_id_seq;

create table if not exists format
(
	id bigint default nextval('format_id_seq'::regclass) not null,
	name text not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'format'::regclass
        and conname = 'format_pkey'
    ) then
        alter table format
        add constraint format_pkey
        primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'fk_anime_format'
    ) then
        alter table anime
        add constraint fk_anime_format
        foreign key (format_id) references format;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'format'::regclass
        and conname = 'idx_format_name'
    ) then
        alter table format
        add constraint idx_format_name
        unique (name);
    end if;
end $$;