-- Anime
alter table anime
alter column start_date type timestamp without time zone,
alter column end_date type timestamp without time zone,
alter column next_airing_date type timestamp without time zone,
add column created_at timestamp without time zone default timezone('utc'::text, now()) not null,
add column updated_at timestamp without time zone default timezone('utc'::text, now()) not null;

-- Preferences
alter table preferences
alter column created_at type timestamp without time zone,
alter column updated_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now()),
alter column updated_at set default timezone('utc'::text, now());

-- Runner
alter table runner
alter column created_at type timestamp without time zone,
alter column updated_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now()),
alter column updated_at set default timezone('utc'::text, now());

-- Title
alter table title
alter column created_at type timestamp without time zone,
alter column updated_at type timestamp without time zone,
alter column deleted_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now()),
alter column updated_at set default timezone('utc'::text, now());

-- Title Anime
alter table title_anime
alter column created_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now());

-- Title Episode
alter table title_episode
alter column created_at type timestamp without time zone,
alter column deleted_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now());

-- Video
alter table video
alter column created_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now());

-- XDCC
alter table xdcc
alter column created_at type timestamp without time zone,
alter column updated_at type timestamp without time zone,
alter column deleted_at type timestamp without time zone,
alter column created_at set default timezone('utc'::text, now()),
alter column updated_at set default timezone('utc'::text, now());
