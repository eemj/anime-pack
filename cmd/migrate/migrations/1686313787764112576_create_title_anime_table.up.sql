create table if not exists title_anime
(
	title_id bigint not null,
	anime_id bigint not null,
	created_at timestamp with time zone not null
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_anime'::regclass
        and conname = 'title_anime_pkey'
    ) then
        alter table title_anime
        add constraint title_anime_pkey
        primary key (title_id, anime_id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_anime'::regclass
        and conname = 'fk_title_anime_anime'
    ) then
        alter table title_anime
        add constraint fk_title_anime_anime
        foreign key (anime_id) references anime;
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'title_anime'::regclass
        and conname = 'fk_title_anime_title'
    ) then
        alter table title_anime
        add constraint fk_title_anime_title
        foreign key (title_id) references title;
    end if;
end $$;