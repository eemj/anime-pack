create table if not exists anime
(
	id bigint not null,
	id_mal bigint,
	start_date timestamp with time zone,
	end_date timestamp with time zone,
	score bigint not null,
	description varchar(3096) not null,
	title_romaji text,
	title_english text,
	title_native text,
	title text not null,
	format_id bigint not null,
	poster_id bigint not null,
	banner_id bigint,
	colour text,
	season_id bigint,
	year bigint,
	status_id bigint not null,
	next_airing_date timestamp with time zone
);

do $$
begin
    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'anime_pkey'
    ) then
        alter table anime
        add constraint anime_pkey primary key (id);
    end if;

    if not exists (
        select 1
        from pg_constraint
        where conrelid = 'anime'::regclass
        and conname = 'idx_anime_title_format_id'
    ) then
        alter table anime
        add constraint idx_anime_title_format_id unique (title, format_id);
    end if;
end $$;