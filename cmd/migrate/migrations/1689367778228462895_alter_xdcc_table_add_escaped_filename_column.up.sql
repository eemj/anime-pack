-- add `escaped_filename`

-- index the `escaped_filename` will be used to retrieve in the irc_handlers
-- instead of parsing the filename, we're going to retrieve by escaped instead.
--
-- todo(eemj) we need to remove the default in the future
alter table xdcc
add column if not exists escaped_filename text not null default ''; 

create index if not exists idx_xdcc_escaped_filename on xdcc (escaped_filename desc);

alter table xdcc
drop constraint if exists xdcc_id_bot_pack_unique;

alter index if exists xdcc_title_episode_id
rename to idx_xdcc_title_episode_id;

alter index if exists xdcc_title_episode_id_quality_id_release_group_id
rename to idx_xdcc_title_episode_id_quality_id_release_group_id;

