alter table title
    alter column season_number drop not null;

update title
set season_number = null
where season_number = '';

drop index if exists idx_title_name_season_number;

alter table title
    add column if not exists year int;

alter table title
    add constraint idx_title_name_season_number_year unique nulls not distinct (name, season_number, year);
