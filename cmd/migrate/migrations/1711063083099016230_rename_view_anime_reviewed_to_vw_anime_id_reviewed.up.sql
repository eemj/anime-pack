-- vim: set ft=sql:
drop view if exists anime_reviewed;

create or replace view vw_anime_id_reviewed as
select
    title_anime.anime_id
from title_anime
inner join title on title_anime.title_id = title.id
where not title.is_deleted and title.reviewed