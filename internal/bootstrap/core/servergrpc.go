package core

import (
	"net"
	"os"
	"sync"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/config"
	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	"gitlab.com/eemj/anime-pack/internal/grpc/credential"
	"gitlab.com/eemj/anime-pack/internal/grpc/interceptor"
	"gitlab.com/eemj/anime-pack/internal/grpc/server/runner"
	"gitlab.com/eemj/anime-pack/internal/grpc/wrapper"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/reflection/grpc_reflection_v1alpha"
)

func (b *Bootstrap) setupGRPC(c *config.ConfigAPI) (srv *grpc.Server, err error) {
	serverOptions := make([]grpc.ServerOption, 0)

	if c.Servers.GRPC.CertFile != nil && c.Servers.GRPC.KeyFile != nil {
		// Check if the cert file exists before registration
		if _, err = os.Stat(
			*c.Servers.GRPC.CertFile,
		); err != nil && os.IsNotExist(err) {
			return
		}

		// Check if the cert key file exists before registration
		if _, err = os.Stat(
			*c.Servers.GRPC.KeyFile,
		); err != nil && os.IsNotExist(err) {
			return
		}

		b.L().
			Named("grpc").
			Info(
				"tls",
				zap.Stringp("cert_file", c.Servers.GRPC.CertFile),
				zap.Stringp("key_file", c.Servers.GRPC.KeyFile),
			)

		serverOptions = append(serverOptions, grpc.Creds(
			credential.NewHotReloadTLSFromFile(
				*c.Servers.GRPC.CertFile,
				*c.Servers.GRPC.KeyFile,
			),
		))
	}

	recoveryInterceptor := interceptor.NewRecoveryInterceptor()
	authInteceptor := interceptor.NewAuthInterceptor(
		b.services.Runner(),
		wrapper.NewSkipper(
			grpc_runner_v1.RunnerService_Auth_FullMethodName,
			grpc_health_v1.Health_Check_FullMethodName,
			grpc_health_v1.Health_Watch_FullMethodName,
		),
	)

	kasp := keepalive.ServerParameters{
		Time:    c.Servers.GRPC.Keepalive.PingDuration.Duration(),
		Timeout: c.Servers.GRPC.Keepalive.KeepaliveTimeout.Duration(),
	}
	kaef := keepalive.EnforcementPolicy{
		MinTime:             c.Servers.GRPC.Keepalive.EnforcementMinTime.Duration(),
		PermitWithoutStream: true,
	}

	b.L().Named("grpc").Debug(
		"keep_alive",
		zap.Duration("server_param_time", kasp.Time),
		zap.Duration("server_param_timeout", kasp.Timeout),
		zap.Duration("enforcement_policy_min_time", kaef.MinTime),
	)

	// Options
	serverOptions = append(
		serverOptions,
		grpc.ChainStreamInterceptor(recoveryInterceptor.Stream(), authInteceptor.Stream()),
		grpc.ChainUnaryInterceptor(recoveryInterceptor.Unary(), authInteceptor.Unary()),
		grpc.KeepaliveParams(kasp),
		grpc.KeepaliveEnforcementPolicy(kaef),
		grpc.ConnectionTimeout(kasp.Timeout),
	)

	srv = grpc.NewServer(serverOptions...)

	healthSrv := health.NewServer()
	runnerSrv := runner.NewServer(b.services.Runner())
	reflectionSrv := reflection.NewServer(reflection.ServerOptions{})

	// RPC server registrations
	grpc_health_v1.RegisterHealthServer(srv, healthSrv)
	grpc_runner_v1.RegisterRunnerServiceServer(srv, runnerSrv)
	grpc_reflection_v1alpha.RegisterServerReflectionServer(srv, reflectionSrv)

	return
}

func (b *Bootstrap) serveGRPC(wg *sync.WaitGroup, errc chan<- error) {
	defer wg.Done()

	var err error
	switch b.config.Servers.GRPC.Protocol {
	case configtypes.ProtocolTCP:
		b.grpcListener, err = net.Listen(
			b.config.Servers.GRPC.Protocol.String(),
			b.config.Servers.GRPC.HostPort(),
		)
		if err != nil {
			errc <- err
			return
		}
	case configtypes.ProtocolUNIX:
		b.grpcListener, err = net.ListenUnix(
			"unix",
			&net.UnixAddr{
				Name: b.config.Servers.GRPC.Address,
				Net:  "unix",
			},
		)
		if err != nil {
			errc <- err
			return
		}
		if err := os.Chmod(
			b.config.Servers.GRPC.Address,
			os.FileMode(b.config.Servers.GRPC.UnixPermission),
		); err != nil {
			errc <- err
			return
		}
	}

	go func() {
		if err := b.grpcServer.Serve(b.grpcListener); err != nil {
			errc <- err
		}
	}()

	b.L().
		Named("grpc").
		Info(
			"options",
			zap.Stringer("protocol", b.config.Servers.GRPC.Protocol),
			zap.String("address", b.config.Servers.GRPC.HostPort()),
		)
}

func (b *Bootstrap) shutdownGRPC(wg *sync.WaitGroup, errc chan<- error) {
	defer wg.Done()

	if b.grpcListener != nil {
		if err := b.grpcListener.Close(); err != nil {
			errc <- err
		}
		if b.config.Servers.GRPC.Protocol == configtypes.ProtocolUNIX {
			// Remove the UNIX socket.
			if err := os.Remove(
				b.config.Servers.GRPC.Address,
			); err != nil && !os.IsNotExist(err) {
				errc <- err
				return
			}
		}
	}
}
