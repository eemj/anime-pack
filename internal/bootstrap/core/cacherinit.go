package core

import (
	"context"
	"errors"

	"github.com/redis/go-redis/extra/redisotel/v9"
	goredis "github.com/redis/go-redis/v9"
	"gitlab.com/eemj/anime-pack/internal/config"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/hybrid"
	"go.jamie.mt/mjcache/redis"
	"go.jamie.mt/mjcache/ristretto"
	redisutil "go.jamie.mt/toolbox/redis_util"
	"go.uber.org/zap"
)

func (b *Bootstrap) createCacher(cfg *config.ConfigAPI) (err error) {
	redisOptions := &goredis.UniversalOptions{
		Addrs:      cfg.Redis.Addresses,
		DB:         cfg.Redis.DatabaseNumber,
		MasterName: cfg.Redis.MasterName,
		Password:   cfg.Redis.Password,
	}

	redisLogger := redisutil.NewLogger(func(_ context.Context, format string, v ...interface{}) {
		log.Named("redis").Sugar().Infof(format, v...)
	})

	goredis.SetLogger(redisLogger)

	// Cache stack
	b.redis = goredis.NewUniversalClient(redisOptions)

	b.distributedCacher = redis.NewRedisCacher(b.redis)

	b.memoryCacher, err = ristretto.NewRistretto(ristretto.Config{
		NumCounters: cfg.Memory.MaximumCost,
		MaxCost:     cfg.Memory.MaximumCost,
		BufferItems: cfg.Memory.BufferItems,
	})
	if err != nil {
		return err
	}

	b.hybridCacher, err = hybrid.NewHybridCacher(hybrid.HybridOptions{
		Topic:       ApplicationName,
		RedisCacher: b.distributedCacher,
		LocalCache:  b.memoryCacher,
		EvictCallback: func(keys ...string) {
			log.Named("hybrid").Info("evicting", zap.Strings("keys", keys))
		},
	})
	if err != nil {
		return err
	}

	if cfg.Telemetry.Enable {
		if err = redisotel.InstrumentTracing(b.redis); err != nil {
			return err
		}
	}

	return nil
}

func (b *Bootstrap) closeCacher() (err error) {
	return errors.Join(
		b.hybridCacher.Close(),
		b.distributedCacher.Close(),
		b.memoryCacher.Close(),
	)
}
