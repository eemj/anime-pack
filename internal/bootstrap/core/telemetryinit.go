package core

import (
	"context"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/telemetry"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.uber.org/zap"
)

func (b *Bootstrap) createTelemetry() (err error) {
	b.L().Named("telemetry").
		Info(
			"trace provider",
			zap.String("kind", string(b.config.Telemetry.Kind)),
			zap.String("endpoint", b.config.Telemetry.Endpoint),
		)

	b.otelProvider, err = telemetry.NewTraceProvider(
		b.ctx,
		b.config.Telemetry,
		semconv.ServiceNameKey.String(ApplicationName),
		semconv.ServiceVersionKey.String(Version),
	)
	if err != nil {
		log.Fatal(err)
	}

	if b.config.Telemetry.Enable {
		b.L().Named("telemetry").
			Debug("redis otel instrumentation enabled")

		if err = redisotel.InstrumentTracing(b.redis); err != nil {
			return err
		}
	}

	return nil
}

func (b *Bootstrap) closeTelemetry() (err error) {
	if b.otelProvider != nil {
		err = b.otelProvider.Shutdown(context.Background())
	}
	return err
}
