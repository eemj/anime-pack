package core

import (
	"time"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/ThreeDotsLabs/watermill/message/router/plugin"
	"github.com/ThreeDotsLabs/watermill/pubsub/gochannel"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"go.jamie.mt/logx/log"
)

func (b *Bootstrap) createPubSub() (*gochannel.GoChannel, error) {
	logger := pubsub.NewLogger(log.Named("watermill"))

	router, err := message.NewRouter(message.RouterConfig{}, logger)
	if err != nil {
		return nil, err
	}

	router.AddMiddleware(
		// The handler function is retried if it returns an error.
		// After MaxRetries, the message is Nacked and it's up to the PubSub to resend it.
		middleware.Retry{
			MaxRetries:      3,
			InitialInterval: time.Millisecond * 100,
			Logger:          logger,
		}.Middleware,

		// Recoverer handles panics from handlers.
		// In this case, it passes them as errors to the Retry middleware.
		middleware.Recoverer,
	)

	router.AddPlugin(plugin.SignalsHandler)

	channel := gochannel.NewGoChannel(gochannel.Config{}, logger)

	return channel, nil
}
