package core

import (
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/feed"
	"gitlab.com/eemj/anime-pack/internal/fs"
	jobprocess "gitlab.com/eemj/anime-pack/internal/jobs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
)

func createJobs(
	hub websocket.Hub,
	fs *fs.FileSystem,
	services lifecycle.ServiceLifecycle,
	queue *queue.Queue,
	feed feed.Feed,
	jobsConfig configmodels.Jobs,
) jobprocess.Jobs {
	return jobprocess.New(
		hub,
		services,
		queue,
		fs,
		feed,
		jobsConfig,
	)
}
