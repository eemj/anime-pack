package core

import (
	"context"
	"errors"
	"net"
	"time"

	"github.com/ThreeDotsLabs/watermill/pubsub/gochannel"
	"github.com/jackc/pgx/v5/pgxpool"
	goredis "github.com/redis/go-redis/v9"
	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/config"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/feed"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/irc"
	jobprocess "gitlab.com/eemj/anime-pack/internal/jobs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/metrics"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/internal/router"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	handler "gitlab.com/eemj/anime-pack/internal/websocket/server_handler"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/hybrid"
	"go.jamie.mt/mjcache/redis"
	"go.jamie.mt/mjcache/ristretto"
	"go.jamie.mt/toolbox/configparser"
	"go.opentelemetry.io/otel/sdk/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	ApplicationName string = "anime-pack-api"

	EnvironmentPrefix string = "ANIME_PACK"
)

var Version string = "devel"

type Bootstrap struct {
	ctx context.Context

	grpcListener net.Listener
	httpListener net.Listener

	// Caching
	redis             goredis.UniversalClient
	distributedCacher *redis.RedisCacher
	memoryCacher      *ristretto.RistrettoCacher
	hybridCacher      *hybrid.HybridCacher

	// Telemetry
	otelProvider *trace.TracerProvider

	// Database
	pgxPool *pgxpool.Pool
	querier database.QuerierExtended

	jobs      jobprocess.Jobs
	services  lifecycle.ServiceLifecycle
	router    *router.Router
	ircClient irc.Client
	queue     *queue.Queue
	feed      feed.Feed

	// PubSub
	channel *gochannel.GoChannel

	metrics      *metrics.Metrics
	fileSystem   *fs.FileSystem
	config       *config.ConfigAPI
	grpcServer   *grpc.Server
	version      string
	webSocketHub websocket.Hub
}

func Create(version string) (b *Bootstrap, err error) {
	// Define a default one, in-case a an error is reached.
	log.Create(zap.ErrorLevel, log.ConsoleCore)

	b = &Bootstrap{version: version, ctx: context.TODO()}

	log.Create(zap.ErrorLevel, log.ConsoleCore)

	b.config, err = configparser.Parse[*config.ConfigAPI](configparser.ParserOptions{
		ApplicationName:      ApplicationName,
		EnvironmentReplacer:  EnvironmentPrefix,
		ConfigDelimiter:      configparser.DefaultConfigDelimiter,
		EnvironmentDelimiter: configparser.DefaultEnvironmentDelimiter,
		Filename:             configparser.DefaultFilename,
	})
	if err != nil {
		return nil, err
	}

	b.config.Flags = *config.ParseAPIArgs()

	// This probably isn't the cleanest but..
	// Globally set the `ExternalURL` so it's used by the other apps.
	path.ExternalURL.Store(&b.config.Servers.ExternalURL)

	b.createLogger(b.config.Logging)

	// Re-replace
	zap.ReplaceGlobals(log.With())

	if !utils.IsEmpty(version) {
		b.L().Info("create", zap.String("version", version))
	}

	b.L().Named("config").
		Info("load", zap.String("path", b.config.Path))

	err = b.createCacher(b.config)
	if err != nil {
		return nil, err
	}

	err = b.createTelemetry()
	if err != nil {
		return nil, err
	}

	b.pgxPool, b.querier, err = b.createDatabase(b.config.Database)
	if err != nil {
		return nil, err
	}

	b.fileSystem = fs.NewFileSystem(b.config.Directories)

	if err = b.fileSystem.EnsureDirectories(); err != nil {
		return nil, err
	}

	b.fileSystem.L().Info(
		"permissions",
		zap.Stringer("image", b.config.Directories.MediaPermission.Image),
		zap.Stringer("download", b.config.Directories.MediaPermission.Download),
		zap.Stringer("directory", b.config.Directories.MediaPermission.Directory),
	)

	b.channel, err = b.createPubSub()
	if err != nil {
		return nil, err
	}

	b.webSocketHub = websocket.NewHub()
	webSocket := websocket.NewWebSocket(b.webSocketHub)

	b.queue = queue.NewQueue()

	b.services = b.createServiceLifecycle()

	b.ircClient, err = b.createNewIRC(b.config.IRC, b.queue, b.services, b.fileSystem)
	if err != nil {
		return nil, err
	}

	b.feed = feed.New(b.ircClient, b.distributedCacher)

	b.metrics = metrics.NewMetrics(b.queue)

	webSocketHandler := handler.NewServerHandler(
		b.services,
		b.ircClient,
		b.queue,
		b.fileSystem,
		b.webSocketHub,
	)

	webSocket.SetupHandler(webSocketHandler)

	b.jobs = createJobs(
		b.webSocketHub,
		b.fileSystem,
		b.services,
		b.queue,
		b.feed,
		b.config.Jobs,
	)

	if b.router, err = b.createRouter(
		b.config,
		b.services,
		b.querier,
		webSocket,
		b.jobs,
		b.metrics,
	); err != nil {
		return nil, err
	}

	b.grpcServer, err = b.setupGRPC(b.config)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (b *Bootstrap) Run(ctx context.Context) (err error) {
	b.L().Named("irc").
		Info("status", zap.Bool("disable", b.config.Flags.IrcDisable))

	if !b.config.Flags.IrcDisable {
		// It's a blocking call.. see maybe how we can make it non-blocking.
		// (we'd have to use channels and create a temp handler probably..)
		go func() {
			if err := b.ircClient.Connect(); err != nil {
				log.Fatal(err)
			}
		}()
	}

	b.L().Named("job").
		Info("status", zap.Bool("disable", b.config.Flags.JobDisable))

	if !b.config.Flags.JobDisable {
		err := b.jobs.StartAll(ctx)
		if err != nil {
			b.L().Named("job").Error("failed whilst starting all jobs", zap.Error(err))
		}
	}

	return b.startServers()
}

// Close attempts to gracefully shutdown the application by prioritizing
// on keeping the TCP calls open till they're finished (5 second timeout)
// and closing off it's dependencies afterwards.
func (b *Bootstrap) Close() (err error) {
	if !b.config.Flags.JobDisable {
		b.L().Named("job").Info("closing...")
		b.jobs.Close()
	}

	if !b.config.Flags.IrcDisable {
		b.L().Named("irc").Info("closing...")
		b.ircClient.Close()
	}

	// Servers
	b.L().Named("http").Info("closing...")
	b.L().Named("grpc").Info("closing...")
	stopServerErr := b.stopServers((5 * time.Second))
	if stopServerErr != nil {
		err = errors.Join(err, stopServerErr)
	}

	// Cache
	b.L().Named("cacher").Info("closing...")
	cacheCloseErr := b.closeCacher()
	if cacheCloseErr != nil {
		err = errors.Join(err, cacheCloseErr)
	}

	// Database
	b.L().Named("database").Info("closing...")
	b.pgxPool.Close()

	// Telemetry
	b.L().Named("telemetry").Info("closing...")
	telemetryErr := b.closeTelemetry()
	if telemetryErr != nil {
		err = errors.Join(err, telemetryErr)
	}

	return err
}

func (b *Bootstrap) L() *zap.Logger {
	return log.Named("bootstrap")
}
