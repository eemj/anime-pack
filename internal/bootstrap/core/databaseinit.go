package core

import (
	"context"
	"fmt"

	"github.com/exaring/otelpgx"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/eemj/anime-pack/internal/database"
	"go.jamie.mt/toolbox/configmodels"
	"go.uber.org/zap"
)

var dataTypeNames = [...]string{
	"anime_season", "_anime_season",
	"anime_format", "_anime_format",
	"anime_status", "_anime_status",
}

func registerCustomTypes(ctx context.Context, config configmodels.PostgreSQL) (*pgxpool.Pool, error) {
	cfg, err := pgxpool.ParseConfig(config.UnsafeString())
	if err != nil {
		return nil, err
	}
	cfg.ConnConfig.Tracer = otelpgx.NewTracer(
		otelpgx.WithIncludeQueryParameters(),
		otelpgx.WithTrimSQLInSpanName(),
	)
	pool, err := pgxpool.NewWithConfig(ctx, cfg)
	if err != nil {
		return nil, err
	}

	customDataTypes, err := getCustomDataTypes(ctx, pool)
	if err != nil {
		return nil, err
	}

	// Load all custom types
	cfg.AfterConnect = func(_ context.Context, c *pgx.Conn) error {
		for _, dataType := range customDataTypes {
			c.TypeMap().RegisterType(dataType)
		}
		return nil
	}

	// Close the current pool
	pool.Close()

	// Re-open for the AfterConnect to take action.
	return pgxpool.NewWithConfig(ctx, cfg)
}

func getCustomDataTypes(ctx context.Context, pool *pgxpool.Pool) ([]*pgtype.Type, error) {
	// Get a single connection just to load type information.
	conn, err := pool.Acquire(ctx)
	if err != nil {
		return nil, err
	}
	defer conn.Release()

	typesToRegister := make([]*pgtype.Type, len(dataTypeNames))
	for index, typeName := range dataTypeNames {
		dataType, err := conn.Conn().LoadType(ctx, typeName)
		if err != nil {
			return nil, fmt.Errorf("failed to load type %s: %v", typeName, err)
		}
		conn.Conn().TypeMap().RegisterType(dataType)
		typesToRegister[index] = dataType
	}
	return typesToRegister, nil
}

func (b *Bootstrap) createDatabase(c configmodels.PostgreSQL) (*pgxpool.Pool, database.QuerierExtended, error) {
	pgxpool, err := registerCustomTypes(b.ctx, c)
	if err != nil {
		return nil, nil, err
	}

	b.L().
		Named("database").
		Info("connected", zap.Stringer("connection_string", c))

	return pgxpool, database.NewQuerierExtended(pgxpool), nil
}
