package core

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/config"
	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	controller "gitlab.com/eemj/anime-pack/internal/controllers/api"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/global"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1"
	"gitlab.com/eemj/anime-pack/internal/database"
	jobprocess "gitlab.com/eemj/anime-pack/internal/jobs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/metrics"
	"gitlab.com/eemj/anime-pack/internal/router"
	"gitlab.com/eemj/anime-pack/internal/router/health"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"go.uber.org/zap"
)

func (b *Bootstrap) createRouter(
	cfg *config.ConfigAPI,
	services lifecycle.ServiceLifecycle,
	querier database.QuerierExtended,
	webSocket *websocket.WebSocket,
	jobs jobprocess.Jobs,
	metrics *metrics.Metrics,
) (r *router.Router, err error) {
	r, err = router.New(
		webSocket,
		*cfg.Servers.HTTP.Middleware,
	)
	if err != nil {
		return nil, err
	}

	r.EnableBasicAuth(
		cfg.Servers.HTTP.Authentication.Username,
		cfg.Servers.HTTP.Authentication.Password,
	)

	if cfg.Servers.HTTP.CORS.Enable {
		b.L().
			Named("router").
			Info(
				"cors",
				zap.Bool("allow_credentials", cfg.Servers.HTTP.CORS.AllowCredentials),
				zap.Strings("allow_origins", cfg.Servers.HTTP.CORS.AllowOrigins),
				zap.Strings("allow_methods", cfg.Servers.HTTP.CORS.AllowMethods),
				zap.Duration("max_age", cfg.Servers.HTTP.CORS.MaxAge),
			)

		r.EnableCORS(
			cfg.Servers.HTTP.CORS.AllowCredentials,
			cfg.Servers.HTTP.CORS.AllowOrigins,
			cfg.Servers.HTTP.CORS.AllowMethods,
			cfg.Servers.HTTP.CORS.MaxAge,
		)
	}

	enableSwagger := *cfg.Environment == configtypes.EnvironmentDEVELOPMENT ||
		cfg.Servers.HTTP.EnableSwagger

	b.L().
		Named("router").
		Info(
			"swagger",
			zap.Bool(
				"enabled",
				enableSwagger,
			),
		)

	if enableSwagger {
		r.EnableSwagger()
	}

	if cfg.Servers.HTTP.ServeImages {
		r.ServeImages(cfg.Directories.Images)
	}

	b.L().
		Named("router").
		Info(
			"mode",
			zap.Stringer("environment", cfg.Environment),
		)

	if cfg.Telemetry.Enable {
		r.UseOpenTelemetry()
	}

	if err = r.ServeFrontend(*cfg.Environment); err != nil {
		return nil, err
	}

	if cfg.Servers.HTTP.GZIP.Enable {
		b.L().
			Named("router").
			Info(
				"gzip",
				zap.Bool("enabled", cfg.Servers.HTTP.GZIP.Enable),
				zap.Int("level", cfg.Servers.HTTP.GZIP.Level),
			)

		r.EnableGZIPCompression(cfg.Servers.HTTP.GZIP.Level)
	}

	if err = controller.Register(
		r.Echo,
		global.New(services, metrics),
		api.New(services, jobs),
	); err != nil {
		return nil, err
	}

	var serviceChecks []*health.ServiceCheck
	if b.distributedCacher != nil {
		serviceChecks = append(serviceChecks, health.NewCheck(
			"cache_redis",
			health.AliverFunc(func(ctx context.Context) bool {
				return b.distributedCacher.Instance().Ping(ctx).Err() == nil
			}),
		))
	}
	if b.querier != nil {
		serviceChecks = append(serviceChecks, health.NewCheck("querier", querier))
	}

	if err = health.Register(r.Echo, serviceChecks...); err != nil {
		return nil, err
	}

	return r, nil
}
