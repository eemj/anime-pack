package core

import (
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/fs"
	new_irc "gitlab.com/eemj/anime-pack/internal/irc"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
)

func (b *Bootstrap) createNewIRC(
	c configmodels.IRC,
	queue *queue.Queue,
	services lifecycle.ServiceLifecycle,
	fs *fs.FileSystem,
) (r new_irc.Client, err error) {
	client, err := new_irc.NewClient(
		c,
		services,
		queue,
		fs,
		b.channel,
		b.channel,
	)
	if err != nil {
		return nil, err
	}

	return client, nil
}
