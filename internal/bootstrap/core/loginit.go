package core

import (
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/configmodels"
	"go.uber.org/zap"
)

func (b *Bootstrap) createLogger(cfg configmodels.Logging) {
	coreBuilders := []log.CoreBuilder{log.ConsoleCore}
	if cfg.File.Enable {
		coreBuilders = append(coreBuilders, log.JSONCore(cfg.ZapWriteSync()))
	}

	log.Create(cfg.ZapLevel(), coreBuilders...)

	b.L().Named("logger").
		Info("init", zap.String("filename", "")) // FIXME(eemj)
}
