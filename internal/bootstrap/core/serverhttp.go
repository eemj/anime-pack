package core

import (
	"context"
	"net"
	"os"
	"sync"
	"time"

	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	"go.uber.org/zap"
	"golang.org/x/net/http2"
)

func (b *Bootstrap) serveHTTP(wg *sync.WaitGroup, errc chan<- error) {
	defer wg.Done()

	switch b.config.Servers.HTTP.Protocol {
	case configtypes.ProtocolHTTP2C:
		srv := &http2.Server{
			MaxConcurrentStreams: 250,
			MaxReadFrameSize:     1048576,
			IdleTimeout:          10 * time.Second,
		}

		go func() {
			if err := b.router.Echo.StartH2CServer(
				b.config.Servers.HTTP.HostPort(),
				srv,
			); err != nil {
				errc <- err
				return
			}
		}()
	case configtypes.ProtocolHTTP:
		go func() {
			if err := b.router.Echo.Start(
				b.config.Servers.HTTP.HostPort(),
			); err != nil {
				errc <- err
				return
			}
		}()
	case configtypes.ProtocolUNIX:
		err := (error)(nil)

		b.httpListener, err = net.ListenUnix(
			"unix",
			&net.UnixAddr{
				Name: b.config.Servers.HTTP.Address,
				Net:  "unix",
			},
		)

		if err != nil {
			errc <- err
			return
		}

		if err = os.Chmod(
			b.config.Servers.HTTP.Address,
			os.FileMode(b.config.Servers.GRPC.UnixPermission),
		); err != nil {
			errc <- err
			return
		}

		if err = b.router.Echo.Server.Serve(b.httpListener); err != nil {
			errc <- err
			return
		}
	}

	b.L().
		Named("http").
		Info(
			"options",
			zap.Stringer("protocol", b.config.Servers.HTTP.Protocol),
			zap.String("address", b.config.Servers.HTTP.HostPort()),
		)
}

func (b *Bootstrap) shutdownHTTP(wg *sync.WaitGroup, errc chan<- error, timeout time.Duration) {
	defer wg.Done()

	// Router has a 5 second window to close off
	ctx, cancel := context.WithTimeout(context.Background(), timeout)

	defer cancel()

	if err := b.router.Echo.Shutdown(ctx); err != nil {
		errc <- err
		// Don't return, we need to close out the httpListener if it's a UNIX
		// Shutdown is common amongst all the supported config protocols.
	}

	if b.config.Servers.HTTP.Protocol == configtypes.ProtocolUNIX {
		if b.httpListener != nil {
			if err := b.httpListener.Close(); err != nil {
				errc <- err
				return
			}
		}

		// Remove the UNIX socket.
		if err := os.Remove(
			b.config.Servers.HTTP.Address,
		); err != nil && !os.IsNotExist(err) {
			errc <- err
			return
		}
	}
}
