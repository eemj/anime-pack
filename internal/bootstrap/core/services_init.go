package core

import (
	"gitlab.com/eemj/anime-pack/internal/activity"
	"gitlab.com/eemj/anime-pack/internal/anime"
	animeepisode "gitlab.com/eemj/anime-pack/internal/anime/anime_episode"
	animeepisodelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_episode_latest"
	animeformat "gitlab.com/eemj/anime-pack/internal/anime/anime_format"
	animeindex "gitlab.com/eemj/anime-pack/internal/anime/anime_index"
	animelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_latest"
	animemedia "gitlab.com/eemj/anime-pack/internal/anime/anime_media"
	animereview "gitlab.com/eemj/anime-pack/internal/anime/anime_review"
	animesearch "gitlab.com/eemj/anime-pack/internal/anime/anime_search"
	animeseason "gitlab.com/eemj/anime-pack/internal/anime/anime_season"
	animesimulcast "gitlab.com/eemj/anime-pack/internal/anime/anime_simulcast"
	animestatus "gitlab.com/eemj/anime-pack/internal/anime/anime_status"
	"gitlab.com/eemj/anime-pack/internal/bot"
	"gitlab.com/eemj/anime-pack/internal/episode"
	"gitlab.com/eemj/anime-pack/internal/genre"
	"gitlab.com/eemj/anime-pack/internal/image"
	"gitlab.com/eemj/anime-pack/internal/library"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/packlist"
	"gitlab.com/eemj/anime-pack/internal/preferences"
	"gitlab.com/eemj/anime-pack/internal/quality"
	releasegroup "gitlab.com/eemj/anime-pack/internal/release_group"
	"gitlab.com/eemj/anime-pack/internal/runner"
	"gitlab.com/eemj/anime-pack/internal/simulcast"
	"gitlab.com/eemj/anime-pack/internal/studio"
	"gitlab.com/eemj/anime-pack/internal/title"
	titleanime "gitlab.com/eemj/anime-pack/internal/title/title_anime"
	titleepisode "gitlab.com/eemj/anime-pack/internal/title/title_episode"
	titlereview "gitlab.com/eemj/anime-pack/internal/title/title_review"
	"gitlab.com/eemj/anime-pack/internal/video"
	"gitlab.com/eemj/anime-pack/internal/xdcc"
)

func (b *Bootstrap) createServiceLifecycle() lifecycle.ServiceLifecycle {
	// Genre/Studio/Episode/Preferences/Activity
	genreSvc := genre.NewGenreService(b.querier, b.hybridCacher)
	studioSvc := studio.NewStudioService(b.querier, b.hybridCacher)
	preferencesSvc := preferences.NewPreferencesService(b.querier)
	episodeSvc := episode.NewEpisodeService(b.querier, b.hybridCacher)
	activitySvc := activity.NewActivityService(b.queue)

	// Images
	bannerSvc := image.NewBannerService(b.querier, b.hybridCacher, b.fileSystem)
	posterSvc := image.NewPosterService(b.querier, b.hybridCacher, b.fileSystem)
	thumbnailSvc := image.NewThumbnailService(b.querier, b.hybridCacher, b.fileSystem)
	processorSvc := image.NewProcessorService()
	imageSvc := image.NewImageService(bannerSvc, posterSvc, thumbnailSvc, processorSvc)

	// Anime
	animeSearchSvc := animesearch.NewAnimeSearchService(b.querier, b.hybridCacher)

	animeReviewAssoc := animereview.NewAnimeReviewAssociations(animeSearchSvc)
	animeReviewSvc := animereview.NewAnimeReviewService(b.querier, b.hybridCacher, animeReviewAssoc)

	animeMediaSvc := animemedia.NewAnimeMediaService(b.querier, b.distributedCacher)
	animeIndexSvc := animeindex.NewAnimeIndexService(b.querier, b.hybridCacher)
	animeEpisodeSvc := animeepisode.NewAnimeEpisodeService(b.querier, b.distributedCacher)
	animeEpisodeLatestSvc := animeepisodelatest.NewAnimeEpisodeLatestService(b.querier, b.distributedCacher)
	animeGenreSvc := anime.NewAnimeGenreService(b.querier, b.hybridCacher)
	animeStudioSvc := anime.NewAnimeStudioService(b.querier, b.hybridCacher)
	animeSimulcastSvc := animesimulcast.NewAnimeSimulcastService(b.querier, b.hybridCacher)
	animeLatestSvc := animelatest.NewAnimeLatestService(b.querier, b.distributedCacher)

	// Anime Status/Format/Season
	animeStatusSvc := animestatus.NewAnimeStatusService(b.querier, b.hybridCacher)
	animeFormatSvc := animeformat.NewAnimeFormatService(b.querier, b.hybridCacher)
	animeSeasonSvc := animeseason.NewAnimeSeasonService(b.querier, b.hybridCacher)

	animeAssoc := anime.NewAnimeAssociations(
		genreSvc,
		studioSvc,
		animeMediaSvc,
		animeLatestSvc,
		animeGenreSvc,
		animeStudioSvc,
		animeIndexSvc,
		animeReviewSvc,
		animeEpisodeSvc,
		animeSimulcastSvc,
		animeEpisodeLatestSvc,
		animeStatusSvc,
		animeFormatSvc,
		animeSeasonSvc,
		preferencesSvc,
		imageSvc,
	)

	animeSvc := anime.NewAnimeService(b.querier, b.hybridCacher, animeAssoc)

	// XDCC
	releaseGroupSvc := releasegroup.NewReleaseGroupService(b.querier, b.hybridCacher)
	botSvc := bot.NewBotService(b.querier, b.hybridCacher)
	qualitySvc := quality.NewQualityService(b.querier, b.hybridCacher)

	// Title Episode / Title Anime / Title Review
	titleEpisodeSvc := titleepisode.NewTitleEpisodeService(b.querier, b.distributedCacher)
	titleAnime := titleanime.NewTitleAnimeService(b.querier, b.hybridCacher)
	titleReview := titlereview.NewTitleReviewService(b.querier)

	// XDCC
	xdccAssoc := xdcc.NewXDCCAssociations(titleEpisodeSvc, qualitySvc, releaseGroupSvc, episodeSvc, botSvc)
	xdccSvc := xdcc.NewXDCCService(b.querier, b.distributedCacher, xdccAssoc)

	// Title
	titleAssoc := title.NewTitleAssociations(titleAnime, titleEpisodeSvc, titleReview, xdccSvc, animeSvc)
	titleSvc := title.NewTitleService(b.querier, b.distributedCacher, titleAssoc)

	// Library
	libraryAssoc := library.NewLibraryAssociations(xdccSvc, titleSvc, animeSvc)
	librarySvc := library.NewLibraryService(b.querier, b.distributedCacher, libraryAssoc)

	// Video
	videoAssoc := video.NewVideoAssociations(titleAnime, thumbnailSvc, xdccSvc, qualitySvc, preferencesSvc, librarySvc)
	videoSvc := video.NewVideoService(b.querier, b.distributedCacher, b.fileSystem, videoAssoc)

	// Packlist
	packlistAssoc := packlist.NewPacklistAssociations(xdccSvc)
	packlistSvc := packlist.NewPacklistService(b.querier, packlistAssoc)

	// Runner
	runnerAssoc := runner.NewRunnerAssociations(videoSvc, thumbnailSvc)
	runnerSvc := runner.NewRunnerService(b.querier, b.distributedCacher, b.fileSystem, b.queue, b.webSocketHub, runnerAssoc)

	// Simulcast
	simulcastAssoc := simulcast.NewSimulcastAssociations(librarySvc, titleSvc)
	simulcastSvc := simulcast.NewSimulcastService(b.querier, b.distributedCacher, simulcastAssoc)

	return lifecycle.NewServiceLifecycle(
		animeSvc,
		activitySvc,
		titleSvc,
		genreSvc,
		studioSvc,
		preferencesSvc,
		imageSvc,
		releaseGroupSvc,
		botSvc,
		qualitySvc,
		xdccSvc,
		librarySvc,
		runnerSvc,
		packlistSvc,
		videoSvc,
		episodeSvc,
		simulcastSvc,
	)
}
