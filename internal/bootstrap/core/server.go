package core

import (
	"sync"
	"time"
)

func (b *Bootstrap) startServers() (err error) {
	errc := make(chan error, 1)
	wg := new(sync.WaitGroup)
	wg.Add(2)

	go b.serveHTTP(wg, errc)
	go b.serveGRPC(wg, errc)

	wg.Wait()

	select {
	case err = <-errc:
	default:
	}

	return
}

func (b *Bootstrap) stopServers(timeout time.Duration) (err error) {
	b.router.Close() // Underlyingly close the webSocket hub.

	errc := make(chan error, 1)
	wg := new(sync.WaitGroup)
	wg.Add(2)

	go b.shutdownHTTP(wg, errc, timeout)
	go b.shutdownGRPC(wg, errc)

	wg.Wait()

	select {
	case err = <-errc:
	default:
	}

	return
}
