package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
	"go.uber.org/zap/zapcore"
)

const (
	flagAddress = "address"
	flagWorkDir = "work-dir"

	flagFfmpegInputOptions  = "ffmpeg.input-options"
	flagFfmpegOutputOptions = "ffmpeg.output-options"

	flagLogLevel = "log.level"

	flagGrpcInsecure   = "grpc.insecure"
	flagGrpcSkipVerify = "grpc.skip-verify"
	flagGrpcCertFile   = "grpc.cert-file"
)

type Args struct {
	Address string
	WorkDir string

	FfmpegInputOptions  []string
	FfmpegOutputOptions []string

	LogLevel int

	GrpcInsecure   bool
	GrpcSkipVerify bool
	GrpcCertFile   string
}

func ParseArgs() *Args {
	args := new(Args)

	pflag.StringVar(&args.Address, flagAddress, "", `core backend address (e.g. 'anime.domain.tld')`)

	pflag.StringVar(&args.WorkDir, flagWorkDir, os.TempDir(), `working directory to store raw, encoded & thumbnail for the current transfer`)

	pflag.StringSliceVar(&args.FfmpegInputOptions, flagFfmpegInputOptions, ffmpeg.DefaultTranscodeOptions.InputOptions, `ffmpeg input options, force 'hwaccel' / specify 'hwaccel_vaapi' device`)
	pflag.StringSliceVar(&args.FfmpegOutputOptions, flagFfmpegOutputOptions, ffmpeg.DefaultTranscodeOptions.OutputOptions, `ffmpeg output options, force 'c:v' / 'c:a' along with additional configuration (e.g. preset for 'libx264')`)

	pflag.IntVar(&args.LogLevel, flagLogLevel, int(zapcore.InfoLevel), `values between '-1' to '5', starting with debug and ending with fatal`)

	pflag.BoolVar(&args.GrpcInsecure, flagGrpcInsecure, false, `grpc insecure connection`)
	pflag.BoolVar(&args.GrpcSkipVerify, flagGrpcSkipVerify, false, `grpc skip verify certificate validation`)
	pflag.StringVar(&args.GrpcCertFile, flagGrpcCertFile, "", `grpc cert file location`)

	pflag.Parse()

	return args
}

func (a *Args) Validate() (err error) {
	if strings.TrimSpace(a.Address) == "" {
		return fmt.Errorf("`%s` is required", flagAddress)
	}

	return
}
