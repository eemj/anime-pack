package bootstrap

import "context"

func (b *Bootstrap) Reload(ctx context.Context) (err error) {
	// TODO: on SIGHUP, we need to refresh the ffmpeg's transcode options
	// perhaps also the ssl certificate?
	//
	// b.client.ReplaceTranscodeOptions()
	return
}
