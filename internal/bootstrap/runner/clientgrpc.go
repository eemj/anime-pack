package bootstrap

import (
	"context"
	"strings"
	"time"

	"gitlab.com/eemj/anime-pack/internal/bootstrap/runner/config"
	"gitlab.com/eemj/anime-pack/internal/grpc/client"
	"gitlab.com/eemj/anime-pack/internal/grpc/credential"
	option "gitlab.com/eemj/anime-pack/internal/grpc/option/client"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

func (b *Bootstrap) startGrpc(
	ctx context.Context,
	ident string,
	args *config.Args,
) (
	conn grpc.ClientConnInterface,
	creds *credential.JwtCreds,
	err error,
) {
	creds = credential.NewJwtCreds(ident)

	options := []option.ClientOption{
		option.WithBlock(),
		option.WithRpcCredentials(creds),
		option.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                (3 * time.Second),
			Timeout:             (5 * time.Second),
			PermitWithoutStream: true,
		}),
	}

	if strings.TrimSpace(args.GrpcCertFile) != "" {
		b.L().Info("grpc", zap.String("cert_file", args.GrpcCertFile))

		options = append(options, option.WithCertFile(args.GrpcCertFile))
	}

	if args.GrpcInsecure {
		b.L().Info("grpc", zap.Bool("insecure", args.GrpcInsecure))

		options = append(options, option.WithInsecure())
	}

	if args.GrpcSkipVerify {
		b.L().Info("grpc", zap.Bool("skip_verify", args.GrpcSkipVerify))

		options = append(options, option.WithSkipVerify())
	}

	conn, err = client.DialContext(ctx, args.Address, options...)

	return
}
