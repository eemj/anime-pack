package bootstrap

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/bootstrap/runner/config"
	"gitlab.com/eemj/anime-pack/internal/grpc/client/runner"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Bootstrap struct {
	client   runner.RunnerClient
	identity Identity
	args     *config.Args
}

func Create() (b *Bootstrap, err error) {
	b = &Bootstrap{args: config.ParseArgs()}
	if err = b.args.Validate(); err != nil {
		return
	}

	logLevel := zapcore.Level(b.args.LogLevel)
	log.Create(logLevel, log.ConsoleCore)

	b.L().Info(
		"arguments",
		zap.String("address", b.args.Address),
		zap.Strings("ffmpeg_input_options", b.args.FfmpegInputOptions),
		zap.Strings("ffmpeg_output_options", b.args.FfmpegOutputOptions),
		zap.Stringer("log_level", logLevel),
		zap.Bool("grpc_insecure", b.args.GrpcInsecure),
		zap.Bool("grpc_skip_verify", b.args.GrpcSkipVerify),
		zap.String("grpc_cert_file", b.args.GrpcCertFile),
	)

	b.identity, err = b.getIdentity()
	if err != nil {
		return nil, err
	}

	b.L().Info(
		"identity",
		zap.String("ident", b.identity.Ident),
		zap.String("user", b.identity.User),
		zap.String("hostname", b.identity.Hostname),
		zap.Bool("is_docker", b.identity.Docker),
	)

	return b, nil
}

func (b *Bootstrap) L() *zap.Logger { return log.Named("bootstrap") }

func (b *Bootstrap) Run(ctx context.Context) (err error) {
	conn, creds, err := b.startGrpc(
		ctx,
		b.identity.Ident,
		b.args,
	)
	if err != nil {
		return err
	}

	runnerOpts := []runner.RunnerClientOption{
		runner.WithDocker(b.identity.Docker),
		runner.WithHostname(b.identity.Hostname),
		runner.WithUser(b.identity.User),
		runner.WithIdent(b.identity.Ident),
		runner.WithWorkDir(b.args.WorkDir),
		runner.WithTranscodeOptions(ffmpeg.TranscodeOptions{
			InputOptions:  b.args.FfmpegInputOptions,
			OutputOptions: b.args.FfmpegOutputOptions,
		}),
	}

	runnerClient := grpc_runner_v1.NewRunnerServiceClient(conn)
	b.client, err = runner.NewClient(runnerClient, runnerOpts...)
	if err != nil {
		return err
	}

	b.client.OnAuth(func(token string) {
		creds.SetToken(token)
	})

	return b.client.Run(ctx)
}
