package bootstrap

import (
	"os"
	"os/user"
	"path/filepath"

	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

const (
	identFile = ".ident"
)

type Identity struct {
	Ident    string
	User     string
	Hostname string
	Docker   bool
}

func (b *Bootstrap) getIdent() (ident string, err error) {
	// Check if we have the token file present in the current working directory.
	wd, err := os.Getwd()
	if err != nil {
		return
	}

	path := filepath.Join(wd, identFile)
	created := false

	// If the file doesn't exist, we'll created it.
	if _, err := os.Stat(path); err != nil && os.IsNotExist(err) {
		// A new identifier is created.
		ident = utils.NewIdentifier()

		// Create a new file, based on the path created so
		// we'll save the identity file created.
		fs, err := os.Create(path)
		if err != nil {
			return "", err
		}

		defer fs.Close()

		if _, err = fs.WriteString(ident); err != nil {
			return "", err
		}

		created = true
	}

	if ident == "" {
		content, err := os.ReadFile(path)
		if err != nil {
			return "", err
		}

		ident = string(content)
	}

	b.L().Info("identity",
		zap.String("ident", ident),
		zap.Bool("created", created),
		zap.String("path", path),
	)

	return ident, err
}

func (b *Bootstrap) getIdentity() (identity Identity, err error) {
	identity.Ident, err = b.getIdent()

	if err != nil {
		return
	}

	user, err := user.Current()
	if err != nil {
		return
	}

	identity.User = user.Username

	identity.Hostname, err = os.Hostname()

	if err != nil {
		return
	}

	if _, e := os.Stat("/.dockerenv"); e == nil {
		identity.Docker = true
	}

	return
}
