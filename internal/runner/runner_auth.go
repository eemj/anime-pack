package runner

import (
	"context"
	"fmt"
	"time"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/auth/generator"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	tokenExpiry time.Duration = 3 * (24 * time.Hour)
)

func (s *runnerService) ValidateAuth(ctx context.Context, args params.ValidateAuthRunnerParams) (bool, *claims.RunnerClaims, error) {
	runner, err := s.First(ctx, params.FirstRunnerParams{Ident: &args.Ident})
	if err != nil {
		return false, nil, err
	}
	if runner == nil {
		return false, nil, apperror.ErrRunnerNotFound
	}

	// Validate the provided token with the signature that's stored for the runner.
	secretKey := generator.SecretKey(runner.Signature)
	parsedClaims, err := s.jwtGenerator.Validate(args.Token, &claims.RunnerClaims{}, secretKey)
	if err != nil {
		return false, nil, err
	}

	claims, ok := parsedClaims.(*claims.RunnerClaims)
	if ok {
		err := claims.Valid()
		isValid := err == nil
		return isValid, claims, err
	}
	return false, nil, fmt.Errorf("unexpected runner type '%T'", claims)
}

func (s *runnerService) Auth(ctx context.Context, args *grpc_runner_v1.AuthRequest) (*grpc_runner_v1.AuthResponse, error) {
	runner, err := s.Create(ctx, params.InsertRunnerParams{
		Ident:    args.Ident,
		User:     args.User,
		Hostname: args.Hostname,
	})
	if err != nil {
		return nil, err
	}
	issuedAt := time.Now().UTC()
	expiresAt := issuedAt.Add(tokenExpiry)
	token, err := s.jwtGenerator.Generate(
		&claims.RunnerClaims{
			IssuedAt:  issuedAt,
			ExpiresAt: expiresAt,
			Name:      runner.Name,
			User:      runner.User,
			Hostname:  runner.Hostname,
			Docker:    args.Docker,
			Ident:     runner.Ident,
		},
		// A signature is generated on Runner creation, that signature will hash the runner's JWT
		generator.SecretKey(runner.Signature),
	)
	if err != nil {
		return nil, err
	}
	return &grpc_runner_v1.AuthResponse{
		Token:     token,
		ExpiresAt: timestamppb.New(expiresAt),
	}, nil
}
