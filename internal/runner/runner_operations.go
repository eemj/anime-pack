package runner

import (
	"context"
	"errors"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"gitlab.com/eemj/anime-pack/internal/runner/transfer"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.uber.org/zap"
)

func (s *runnerService) Operation(ctx context.Context, runner client.Client, operation *grpc_runner_v1.OperationRequest) (err error) {
	fields := []zap.Field{
		zap.Stringer("type", operation.Operation),
		zap.Stringer("runner", runner.Info()),
		zap.Any("request", operation.Request),
	}

	if operation.Operation != grpc_runner_v1.Operation_PROGRESS {
		s.L(ctx).Info("operation", fields...)
	}

	switch request := operation.Request.(type) {
	case *grpc_runner_v1.OperationRequest_CloseRequest:
		err = s.operationClose(ctx, runner, request)
	case *grpc_runner_v1.OperationRequest_InterruptRequest:
		err = s.operationInterrupt(ctx, runner, request)
	case *grpc_runner_v1.OperationRequest_OpenRequest:
		err = s.operationOpen(runner, request)
	case *grpc_runner_v1.OperationRequest_MetadataRequest:
		err = s.operationMetadata(runner, request)
	case *grpc_runner_v1.OperationRequest_ProgressRequest:
		err = s.operationProgress(runner, request)
	}
	if err != nil {
		fields = append(fields, zap.Error(err))
		s.L(ctx).Error("operation", fields...)
	}
	return nil
}

func (s *runnerService) operationOpen(_ client.Client, request *grpc_runner_v1.OperationRequest_OpenRequest) (err error) {
	if request.OpenRequest.Error == "" {
		return nil
	}
	return errors.New(request.OpenRequest.Error)
}

// operationInterrupt handles the interrupt operation, this occurs when the user presses the Cancel button on an on-going transfer.
func (s *runnerService) operationInterrupt(ctx context.Context, runner client.Client, _ *grpc_runner_v1.OperationRequest_InterruptRequest) (err error) {
	transfer := runner.Transfer()

	transferIsActive := transfer.Active()

	s.L(ctx).Info(
		"operation interrupt, checking if transfer is active and canceling",
		zap.Bool("is_active", transferIsActive),
		zap.Stringer("runner", runner.Info()),
	)

	if transferIsActive {
		s.queue.Cancel(transfer.Anime())

		s.L(ctx).Debug("operation interrupt, transfer is active, queue canceled - closing down the transfer")

		return transfer.Close()
	}
	return nil
}

func (s *runnerService) operationMetadata(runner client.Client, request *grpc_runner_v1.OperationRequest_MetadataRequest) (err error) {
	if trn := runner.Transfer(); trn.Active() {
		trn.SetMetadata(transfer.Metadata{
			Video: &transfer.MetadataVideo{
				Dimension: &transfer.MetadataVideoDimension{
					Height: request.MetadataRequest.Video.Dimension.Height,
					Width:  request.MetadataRequest.Video.Dimension.Width,
				},
				SHA256:   request.MetadataRequest.Video.Sha256,
				CRC32:    request.MetadataRequest.Video.Crc32,
				Duration: request.MetadataRequest.Video.Duration,
				Size:     request.MetadataRequest.Video.Size,
			},
			Image: &transfer.MetadataImage{
				SHA256: request.MetadataRequest.Image.Sha256,
				Size:   request.MetadataRequest.Image.Size,
			},
		})
	}

	return nil
}

func (s *runnerService) operationProgress(runner client.Client, request *grpc_runner_v1.OperationRequest_ProgressRequest) (err error) {
	trn := runner.Transfer()
	if !trn.Active() {
		// Interrupt the runner, they're working on a transfer
		// that's not even in the queue...
		//
		return runner.Produce(&grpc_runner_v1.OperationResponse{
			Operation: grpc_runner_v1.Operation_INTERRUPT,
			Response: &grpc_runner_v1.OperationResponse_InterruptResponse{
				InterruptResponse: &grpc_runner_v1.InterruptResponse{},
			},
		})
	}

	var (
		anime        = trn.Anime()
		progressType protocol.ProgressType
	)
	// Map the WebSocket protocol progress type to the Protobuf progress type.
	switch request.ProgressRequest.Type {
	case grpc_runner_v1.ProgressRequest_DOWNLOAD:
		progressType = protocol.Progress_Download
	case grpc_runner_v1.ProgressRequest_TRANSCODE:
		progressType = protocol.Progress_Transcode
	case grpc_runner_v1.ProgressRequest_METADATA:
		progressType = protocol.Progress_Metadata
	case grpc_runner_v1.ProgressRequest_UPLOAD:
		progressType = protocol.Progress_Upload
	}

	// We have four progress types. These four types will add 100%
	// together and they're divided as the following:
	//
	// |----------|-----------|------------------|
	// | Position |    Type   |    Percentage    |
	// |----------|-----------|------------------|
	// |    1     | Download  | 33% (33%)  		 |
	// |    2     | Transcode | 66% (33%)  		 |
	// |    3     | Metadata  | 67% (1%)  |
	// |    4     | Upload    | 100% (32%)       |
	// |----------|-----------|------------------|
	percentage := request.ProgressRequest.Percentage

	switch progressType {
	case protocol.Progress_Download:
		percentage *= 0.33
	case protocol.Progress_Transcode:
		percentage = 33.0 + (percentage * 0.33)
	case protocol.Progress_Metadata:
		percentage = 66.0 + (percentage * 0.1)
	case protocol.Progress_Upload:
		percentage = 67 + (percentage * 0.32)
	}

	queueProgress := queue.Progress{
		Type:            string(progressType),
		Percentage:      percentage,
		Speed:           request.ProgressRequest.Speed,
		CurrentFilesize: request.ProgressRequest.CurrentFilesize,
		Filesize:        request.ProgressRequest.Filesize,
		CurrentDuration: request.ProgressRequest.CurrentDuration,
		Duration:        request.ProgressRequest.Duration,
	}

	// Update the queue.
	s.queue.UpdateProgress(
		anime,
		queueProgress,
	)

	return nil
}
