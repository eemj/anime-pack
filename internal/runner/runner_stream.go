package runner

import (
	"context"
	"hash/adler32"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"gitlab.com/eemj/anime-pack/internal/runner/transfer"
)

func (s *runnerService) Stream(ctx context.Context, runner client.Client, stream *grpc_runner_v1.StreamRequest) (err error) {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		if trn := runner.Transfer(); trn.Active() {
			if sum := adler32.Checksum(stream.Content); sum != stream.Adler32 {
				// Mark the request as corrupt, so once everything is sent from the runner-side
				// we'll query to re-send those offsets.
				trn.Corrupt(stream)

				return transfer.ErrRequestAdler32Checksum(stream, sum)
			}

			_, err = trn.WriterAt(stream.Stream).WriteAt(
				stream.Content[:stream.Size],
				stream.Start,
			)
			return err
		}

		return transfer.ErrInactive
	}
}
