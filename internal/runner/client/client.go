package client

import (
	"sync/atomic"

	"github.com/dgrijalva/jwt-go"
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/runner/transfer"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// clientChannelBuffer defines the buffer for the client's operation
const clientChannelBuffer = 32

type Client interface {
	Transfer() transfer.Transfer
	Info() Info
	Claims() jwt.Claims
	Heartbeat() Heartbeat
	Paused() bool
	ToggleState() (state State, err error)

	// Consume & Produce
	Produce(operation *grpc_runner_v1.OperationResponse) (err error)
	Consume() <-chan *grpc_runner_v1.OperationResponse
}

type client struct {
	claims    *claims.RunnerClaims
	transfer  transfer.Transfer
	paused    atomic.Bool
	operation chan *grpc_runner_v1.OperationResponse
	heartbeat Heartbeat
}

// Produce attempts to enqueue a message in the channel buffer.
// An error is returned when the buffer is full.
func (c *client) Produce(operation *grpc_runner_v1.OperationResponse) (err error) {
	select {
	case c.operation <- operation:
		break
	default:
		err = ErrBufferFull
	}
	return
}

// Consume returns a channel with all the produced buffers.
func (c *client) Consume() <-chan *grpc_runner_v1.OperationResponse { return c.operation }

func (c *client) Info() Info {
	info := Info{
		Name:     c.claims.Name,
		User:     c.claims.User,
		Hostname: c.claims.Hostname,
		Docker:   c.claims.Docker,
		Ident:    c.claims.Ident,
		Latency:  c.heartbeat.Latency(),
	}

	if c.paused.Load() {
		info.State = State_Pause
	} else {
		if c.transfer.Active() {
			info.State = State_Run
			info.Transfer = utils.PointerOf(c.transfer.Anime())
		} else {
			info.State = State_Wait
		}
	}

	return info
}

// Paused will return true if the client is paused.
func (c *client) Paused() bool { return c.paused.Load() }

// Toggle will toggle the client state, if it's either waiting or paused.
func (c *client) ToggleState() (state State, err error) {
	isPaused := c.paused.Load()

	if !isPaused && c.transfer.Active() {
		return State_Unknown, ErrCannotPauseDuringActiveTransfer
	}

	c.paused.Swap(!isPaused)
	if isPaused {
		state = State_Wait
	} else {
		state = State_Pause
	}

	return
}

// Heartbeat returns the heart beating operations of the client.
func (c *client) Heartbeat() Heartbeat {
	return c.heartbeat
}

// Claims returns the JWT claims for the client.
func (c *client) Claims() jwt.Claims {
	return c.claims
}

// Transfer returns the transfer manager for the client.
func (c *client) Transfer() transfer.Transfer {
	return c.transfer
}

// String returns a string of the Client's info.
func (c *client) String() string { return c.Info().String() }

func New(claims *claims.RunnerClaims) Client {
	return &client{
		transfer:  transfer.New(),
		claims:    claims,
		heartbeat: newHeartbeat(),
		operation: make(
			chan *grpc_runner_v1.OperationResponse,
			clientChannelBuffer,
		),
	}
}
