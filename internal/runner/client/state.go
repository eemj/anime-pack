package client

import "strings"

type State int32

const (
	State_Unknown State = 0
	State_Wait    State = 1
	State_Pause   State = 2
	State_Run     State = 3
)

func (s State) String() string {
	switch s {
	case State_Unknown:
		return "UNKNOWN"
	case State_Run:
		return "RUN"
	case State_Pause:
		return "PAUSE"
	case State_Wait:
		return "WAIT"
	}

	return "UNKNOWN"
}

func (s State) MarshalJSON() ([]byte, error) {
	return []byte(`"` + s.String() + `"`), nil
}

func (s *State) UnmarshalJSON(b []byte) error {
	*s = GetState(strings.Trim(string(b), `"`))
	return nil
}

func GetState(state string) State {
	switch state {
	case State_Unknown.String():
		return State_Unknown
	case State_Wait.String():
		return State_Wait
	case State_Pause.String():
		return State_Pause
	case State_Run.String():
		return State_Run
	}

	return State_Unknown
}
