package client

import "errors"

var (
	// ErrBufferFull is thrown when the channel buffer is full.
	ErrBufferFull = errors.New("buffer is full")

	// ErrCannotPauseDuringActiveTransfer is thrown when the user attempts to pause a runner
	// during an active transfer.
	ErrCannotPauseDuringActiveTransfer = errors.New("cannot pause during an active transfer")
)
