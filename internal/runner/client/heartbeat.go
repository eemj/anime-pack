package client

import (
	"sync"
	"sync/atomic"
	"time"
)

// Heartbeat defines the heart beating of a client. When a heartbeat
// is verified, the latency get's calculated (the time it took to receive
// and calculate the beat).
type Heartbeat interface {
	Verify(timestamp time.Time) (ok bool)
	Set(timestamp time.Time)
	Get() (timestamp time.Time, ok bool)
	Latency() float64
}

type heartbeat struct {
	rwmutex   sync.RWMutex
	timestamp *time.Time
	latency   int64
}

func newHeartbeat() Heartbeat {
	return &heartbeat{}
}

func (h *heartbeat) Verify(timestamp time.Time) (ok bool) {
	h.rwmutex.Lock()
	defer h.rwmutex.Unlock()

	if ok = h.timestamp != nil && h.timestamp.Equal(timestamp); ok {
		atomic.SwapInt64(
			&h.latency,
			time.Since(*h.timestamp).Nanoseconds(),
		)
		h.timestamp = nil
	}

	return
}

func (h *heartbeat) Set(timestamp time.Time) {
	h.rwmutex.Lock()
	h.timestamp = &timestamp
	h.rwmutex.Unlock()
}

func (h *heartbeat) Get() (timestamp time.Time, ok bool) {
	h.rwmutex.RLock()
	defer h.rwmutex.RUnlock()

	if ok = h.timestamp != nil; ok {
		timestamp = *h.timestamp
	}

	return
}

func (h *heartbeat) Latency() float64 {
	return float64(atomic.LoadInt64(&h.latency)) / float64(time.Millisecond)
}
