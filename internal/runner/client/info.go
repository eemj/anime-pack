package client

import (
	"fmt"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

type Info struct {
	State    State             `json:"state"`
	Transfer *domain.AnimeBase `json:"transfer,omitempty"`
	Name     string            `json:"name"`
	User     string            `json:"user"`
	Hostname string            `json:"hostname"`
	Ident    string            `json:"ident"`
	Docker   bool              `json:"docker"`
	Latency  float64           `json:"ms"`
}

func (i Info) String() string {
	return fmt.Sprintf(
		"%s@%s (%s) #%s",
		i.User,
		i.Hostname,
		i.Name,
		i.Ident,
	)
}
