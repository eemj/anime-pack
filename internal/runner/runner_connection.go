package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"go.uber.org/zap"
)

// ClientInfos will return the client information for all the
// connected clients whether they are waiting, running or paused.
func (s *runnerService) Info() []client.Info {
	return s.manager.Info()
}

// Owns will retrieve the manager and iterate through every client and checks
// if the anime provided is an owned transfer.
func (s *runnerService) Owns(anime domain.AnimeBase) (client client.Client) {
	return s.manager.Owns(anime)
}

// Timeout checks if the transfer is assigned to a client,
// sends an grpc_runner_v1.InterruptResponse{} and closes down the transfer.
func (s *runnerService) Timeout(anime domain.AnimeBase) (err error) {
	// If the transfer is under a client's responsibility, we'll send an
	// interrupt response.
	if client := s.manager.Owns(anime); client != nil {
		client.Produce(&grpc_runner_v1.OperationResponse{
			Operation: grpc_runner_v1.Operation_INTERRUPT,
			Response: &grpc_runner_v1.OperationResponse_InterruptResponse{
				InterruptResponse: &grpc_runner_v1.InterruptResponse{},
			},
		})

		// Transfer must get closed, right after we send that.
		return client.Transfer().Close()
	}

	return nil
}

// Interrupt is similair to Timeout() but it does not close down
// the transfer as it depends on the InterruptRequest{} from the runner to
// close down the transfer. This function also sends a Cancel to the queue item
// or if the item is not active yet, it'll delete the item from the queue.
func (s *runnerService) Interrupt(anime domain.AnimeBase) (err error) {
	client := s.manager.Owns(anime)
	if client == nil {
		s.L(context.TODO()).
			Info(
				"runner transfer is not owned by a runner",
				zap.Stringer("anime", anime),
			)
		return apperror.ErrRunnerTransferIsNotOwned
	}

	// Notify the runner to close down the transfer.
	client.Produce(&grpc_runner_v1.OperationResponse{
		Operation: grpc_runner_v1.Operation_INTERRUPT,
		Response: &grpc_runner_v1.OperationResponse_InterruptResponse{
			InterruptResponse: &grpc_runner_v1.InterruptResponse{},
		},
	})

	// Cancel the queue.
	s.queue.Cancel(anime)

	return nil
}

func (s *runnerService) Assign(
	anime domain.AnimeBase,
	sizeInBytes uint64,
	address string,
) (assignee client.Client, err error) {
	assignee, err = s.manager.AvailableDo(anime, func(runner client.Client) error {
		videoFs, err := s.fs.CreateTempAnime(anime)
		if err != nil {
			return err
		}

		imageFs, err := s.fs.CreateTempImage()
		if err != nil {
			return err
		}

		// Assign the transfer.
		runner.Transfer().Setup(
			anime,
			imageFs,
			videoFs,
		)

		return nil
	})

	if err != nil {
		return
	}

	// Send out to the client that we want to start a pipeline.
	assignee.Produce(&grpc_runner_v1.OperationResponse{
		Operation: grpc_runner_v1.Operation_OPEN,
		Response: &grpc_runner_v1.OperationResponse_OpenResponse{
			OpenResponse: &grpc_runner_v1.OpenResponse{
				Address: address,
				Size:    int64(sizeInBytes),
			},
		},
	})

	return
}

// Client returns a connected client.
func (s *runnerService) Client(runnerClaims *claims.RunnerClaims) (client client.Client) {
	return s.manager.Client(runnerClaims)
}

// Connect adds the runner to the manager, if the client successfully registers 1 is added to the queue limit.
func (s *runnerService) Connect(runnerClaims *claims.RunnerClaims) (client.Client, error) {
	client, err := s.manager.Connect(runnerClaims)
	if err != nil {
		return nil, err
	}
	s.queue.AddLimit(1)
	return client, nil
}

// ToggleState toggles between resumed and paused state for the runner.
func (s *runnerService) ToggleState(runnerClaims *claims.RunnerClaims) (state client.State, err error) {
	state, err = s.manager.ToggleState(runnerClaims)
	if err != nil {
		return client.State_Unknown, err
	}

	if state == client.State_Run {
		s.queue.AddLimit(1)
	} else if state == client.State_Pause {
		s.queue.AddLimit(-1)
	}

	return state, nil
}

// Disconnect
func (s *runnerService) Disconnect(runnerClaims *claims.RunnerClaims) (err error) {
	// Attempt to get the client
	client := s.Client(runnerClaims)
	if client == nil { // Client isn't even connected..
		return apperror.ErrRunnerIdentRequired
	}

	// Pause so it's not assigned during the available check
	s.manager.Pause(runnerClaims)

	// Remove the limit so nothing will fall on this runner.
	s.queue.AddLimit(-1)

	var (
		trn    = client.Transfer()
		anime  = trn.Anime()
		active = trn.Active()
	)

	// If we have the session active, we're going to terminate.
	if active {
		// After the runner disconnects, send the restart.

		err = trn.Close() // Close the transfer.. to prevent progress update.

		s.queue.Restart(anime)
	}
	if err != nil {
		return err
	}

	// Disconnect the runner from the manager.
	s.manager.Disconnect(runnerClaims)

	return nil
}
