package runner

import (
	"gitlab.com/eemj/anime-pack/internal/image"
	"gitlab.com/eemj/anime-pack/internal/video"
)

type RunnerAssociations interface {
	Video() video.VideoService
	Thumbnail() image.ThumbnailService
}

type runnerAssociations struct {
	thumbnail image.ThumbnailService
	video     video.VideoService
}

func (s *runnerAssociations) Thumbnail() image.ThumbnailService { return s.thumbnail }
func (s *runnerAssociations) Video() video.VideoService         { return s.video }

func NewRunnerAssociations(
	video video.VideoService,
	thumbnail image.ThumbnailService,
) RunnerAssociations {
	return &runnerAssociations{
		video:     video,
		thumbnail: thumbnail,
	}
}
