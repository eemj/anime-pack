package runner

import (
	"context"
	"errors"
	"io"
	"os"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"gitlab.com/eemj/anime-pack/internal/runner/transfer"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.uber.org/zap"
)

func (s *runnerService) storeImage(
	ctx context.Context,
	trn transfer.Transfer,
) (<-chan error, <-chan *domain.Thumbnail) {
	var (
		errc   = make(chan error, 1)
		imagec = make(chan *domain.Thumbnail, 1)
	)

	go func() {
		// Store the image data in our filesystem and it's details
		// in the database.
		image, err := s.associations.Thumbnail().CreateFromReader(ctx, params.CreateFromReaderImageParams{
			Reader: trn.Reader(grpc_runner_v1.StreamType_IMAGE),
		})
		if err != nil {
			errc <- err
		} else {
			imagec <- image
		}
	}()

	return errc, imagec
}

func (s *runnerService) storeVideo(
	ctx context.Context,
	anime domain.AnimeBase,
	trn transfer.Transfer,
	detail queue.Detail,
	imagec <-chan *domain.Thumbnail,
) (<-chan error, <-chan *domain.Video) {
	errc := make(chan error, 1)
	videoc := make(chan *domain.Video, 1)

	go func() {
		// Create a path for this anime.
		videoFile, err := s.fs.CreateAnime(anime)
		if err != nil {
			errc <- err
			return
		}
		defer videoFile.Close()

		s.L(ctx).Debug(
			"operation close, anime file created",
			zap.String("name", videoFile.Name()),
		)

		if _, err = io.Copy(
			videoFile,
			trn.Reader(grpc_runner_v1.StreamType_VIDEO),
		); err != nil {
			errc <- err
			return
		}

		videoFilepath := videoFile.Name()
		height := anime.Height
		metadata := trn.Metadata()

		// Since the quality was unknown when the download started,
		// using the determined quality from ffmpeg, we'll have to rename to file
		// to make use of the new quality.
		if anime.Height == 0 {
			height = int(metadata.Video.Dimension.Height)

			newVideoFilepath := s.fs.CreatePathAnime(domain.AnimeBase{
				Title:        anime.Title,
				Episode:      anime.Episode,
				ReleaseGroup: anime.ReleaseGroup,
				Height:       height,
			})

			s.L(ctx).Debug(
				"operation close, renaming video file",
				zap.String("old", videoFilepath),
				zap.String("new", newVideoFilepath),
			)

			if err = os.Rename(videoFilepath, newVideoFilepath); err != nil {
				errc <- err
				return
			}

			// Rename was successful, replace the path.
			videoFilepath = newVideoFilepath
		}

		// Wait for the image to process (it should've processed, by the time we're here..)
		s.L(ctx).Info("operation close, waiting for image to get processed")

		// Block till either an image is provided or the context is canceled
		// the parent context ideally here should have a timeout so this doesn't goroutine leak.
		var image *domain.Thumbnail
		select {
		case <-ctx.Done():
			break
		case image = <-imagec:
		}

		var video *domain.Video
		// A thumbnail is a must when we're creating video files, if the image for some reason
		// ends up being nil, let's remove the created file.
		if image == nil {
			s.L(ctx).Warn("operation close, image came back as nil - cleaning up video")

			// Ensure it's closed so we don't get `file is used by another process` error.
			err = videoFile.Close()
			if err != nil {
				errc <- err
				return
			}

			// Cleanup
			err = os.Remove(videoFilepath)
			if err != nil {
				errc <- err
				return
			}

			errc <- errors.New("unable to create a video, expected an image but got nil") // FIXME(eemj): Custom error for now
		} else {
			s.L(ctx).Debug(
				"operation close, image retrieved",
				zap.String("name", image.Name),
				zap.Int64("id", image.ID),
			)

			qualityHeight := int64(height)
			// Store the video hashes and details in the DB.
			video, err = s.associations.Video().Create(ctx, params.InsertVideoParams{
				Path:        videoFilepath,
				Size:        metadata.Video.Size,
				CRC32:       metadata.Video.CRC32,
				Duration:    metadata.Video.Duration,
				ThumbnailID: image.ID,
				XDCCID:      int64(detail.ID),
				// Send over the height calculated by ffprobe,
				// so it get's saved in the database
				// (especially if we don't have the quality in our XDCC entry).
				QualityHeight: &qualityHeight,
			})
			if err != nil {
				errc <- err
				return
			}

			video.Thumbnail = image
		}

		videoc <- video
	}()

	return errc, videoc
}

// operationClose handles the close operation, this occurs when the runner is done from the transfer. By done this can mean
// that the runner either encountered an error whilst executing the pipeline or it's finished with the whole pipeline and
// it's ready for another pipeline.
func (s *runnerService) operationClose(ctx context.Context, runner client.Client, request *grpc_runner_v1.OperationRequest_CloseRequest) (err error) {
	// Here we're expecting an error message if something went wrong.
	if request.CloseRequest.Error != "" {
		// Dont return this error immediately, we need to close the other resources before
		// we do so.
		err = errors.New(request.CloseRequest.Error)
		s.L(ctx).Error(
			"operation close",
			zap.Stringer("ident", runner.Info()),
			zap.Error(err),
		)
	}

	trn := runner.Transfer()
	if !trn.Active() {
		return transfer.ErrInactive
	}
	defer trn.Close()

	// If there's an anime & an active transfer,
	// we need to mark it as done in our queue
	anime := trn.Anime()

	defer func() {
		// Mark the item as done in our queue
		if value, ok := s.queue.Get(anime); ok && value != nil {
			s.L(ctx).Info("operation close, marking queue item as done", zap.Stringer("anime", anime))
			value.Done(err)
		}
	}()

	// If we have an error from the close request, return.
	if err != nil {
		return err
	}

	var detail queue.Detail
	if value, ok := s.queue.Get(anime); ok && value != nil && value.Active() {
		detail = value.Details[0].Clone()
	}

	// Verify the transfer checksums.
	if err = trn.Verify(); err != nil {
		return err
	}

	// TODO(eemj): Self-recover by integrating the stream chunks and requesting
	// positional bytes from the runner.
	//
	// Check for corrupted parts, if there are any, leave a warning log and proceed.
	if corrupt := trn.CorruptParts(); corrupt > 0 {
		s.L(ctx).Warn(
			"operation close",
			zap.Stringer("runner", runner.Info()),
			zap.Stringer("type", grpc_runner_v1.Operation_CLOSE),
			zap.Stringer("anime", anime),
			zap.Int("corrupt", corrupt),
		)
	}

	imageErrc, imagec := s.storeImage(ctx, trn)
	videoErrc, videoc := s.storeVideo(ctx, anime, trn, detail, imagec)

	var (
		errc  = utils.MergeChan(imageErrc, videoErrc)
		video *domain.Video
	)

	s.L(ctx).Debug("operation close, waiting for video and image store process")

	select {
	case err = <-errc:
		if err != nil {
			return err
		}
	case video = <-videoc:
		break
	}

	if video != nil {
		payload := protocol.Payload{
			Opcode: protocol.Activity_Metadata,
			Data: &protocol.DataActivityMetadata{
				Title:        anime.Title,
				Episode:      anime.Episode,
				Height:       anime.Height,
				ReleaseGroup: anime.ReleaseGroup,
				CRC32:        video.CRC32,
				Duration:     video.Duration,
				Size:         int64(video.Size),
				Thumbnail:    path.PrefixifyImageThumbnail(video.Thumbnail.Name),
				VideoPath:    path.PrefixifyVideo(video.Path),
			},
		}

		s.L(ctx).Debug(
			"operation close, finalized",
			zap.Stringer("anime", anime),
			zap.Int64("size", video.Size),
			zap.String("crc32", video.CRC32),
		)

		// Let the connected users know that this transfer is successful.
		s.wshub.BroadcastPayload(payload)
	}

	return nil
}
