package manager

import (
	"slices"
	"sort"
	"sync"

	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
)

type AvailableDo func(client client.Client) (err error)

type Manager interface {
	Info() []client.Info
	Client(claims *claims.RunnerClaims) (client client.Client)
	Connect(claims *claims.RunnerClaims) (connected client.Client, err error)
	Disconnect(claims *claims.RunnerClaims) (err error)
	ToggleState(claims *claims.RunnerClaims) (state client.State, err error)
	Pause(claims *claims.RunnerClaims)
	Has(claims *claims.RunnerClaims) (ok bool)
	Len() int

	// Owns returns the client that owns the transfer for the requested,
	// owner can be nil if no owner is found.
	Owns(anime domain.AnimeBase) (owner client.Client)

	// AvailableDo will return a client that doesn't have a transfer assigned,
	// for the duration of the available func the manager will have the read lock.
	AvailableDo(anime domain.AnimeBase, fn AvailableDo) (assignee client.Client, err error)
}

type manager struct {
	rwmutex sync.RWMutex
	clients []client.Client
}

func NewManager() Manager { return &manager{} }

func (m *manager) Len() int {
	m.rwmutex.RLock()
	defer m.rwmutex.RUnlock()
	return len(m.clients)
}

func match(jwtClaims *claims.RunnerClaims) func(c client.Client) bool {
	return func(c client.Client) bool {
		clientClaims, ok := c.Claims().(*claims.RunnerClaims)
		return ok && jwtClaims.Ident == clientClaims.Ident
	}
}

func (m *manager) index(jwtClaims *claims.RunnerClaims) int {
	if jwtClaims == nil {
		return -1
	}

	m.rwmutex.RLock()
	defer m.rwmutex.RUnlock()
	return slices.IndexFunc(m.clients, match(jwtClaims))
}

func (m *manager) Has(jwtClaims *claims.RunnerClaims) bool {
	return m.index(jwtClaims) >= 0
}

func (m *manager) Client(jwtClaims *claims.RunnerClaims) client.Client {
	if index := m.index(jwtClaims); index >= 0 {
		m.rwmutex.RLock()
		defer m.rwmutex.RUnlock()
		return m.clients[index]
	}

	return nil
}

func (m *manager) Pause(jwtClaims *claims.RunnerClaims) {
	m.rwmutex.RLock()
	defer m.rwmutex.RUnlock()
	runner := m.Client(jwtClaims)
	if runner != nil && !runner.Paused() && !runner.Transfer().Active() {
		runner.ToggleState()
	}
}

func (m *manager) ToggleState(jwtClaims *claims.RunnerClaims) (state client.State, err error) {
	if runner := m.Client(jwtClaims); runner != nil {
		return runner.ToggleState()
	}

	return client.State_Unknown, ErrClientNotConnected(jwtClaims.Ident)
}

func (m *manager) Disconnect(jwtClaims *claims.RunnerClaims) (err error) {
	if index := m.index(jwtClaims); index >= 0 {
		m.rwmutex.Lock()
		m.clients[index] = nil
		m.clients = slices.Delete(m.clients, index, (index + 1))
		m.rwmutex.Unlock()
		return nil
	}

	return ErrClientNotConnected(jwtClaims.Ident)
}

func (m *manager) Connect(jwtClaims *claims.RunnerClaims) (connected client.Client, err error) {
	if m.Has(jwtClaims) {
		return nil, ErrClientAlreadyConnected(jwtClaims.Ident)
	}

	m.rwmutex.Lock()
	current := client.New(jwtClaims)
	m.clients = append(m.clients, current)
	m.rwmutex.Unlock()

	return current, nil
}

func (m *manager) AvailableDo(anime domain.AnimeBase, fn AvailableDo) (client.Client, error) {
	m.rwmutex.Lock()
	defer m.rwmutex.Unlock()

	if len(m.clients) == 0 {
		return nil, ErrNoAvailableClients
	}

	var (
		owner    client.Client
		inactive []client.Client
	)

	for _, client := range m.clients {
		// We don't want to assign it to a client that's paused.
		if client.Paused() {
			continue
		}

		if trn := client.Transfer(); trn.Active() { // If the transfer is active
			if trn.IsCurrent(anime) { // And the transfer's anime is the requested
				owner = client // Set the owner to this client.
				break
			}
		} else { // If it's not active
			inactive = append(inactive, client)
		}
	}

	// If the owner is not empty, the requested anime is assigned
	// to another client.
	if owner != nil {
		err := ErrTransferIsAlreadyActiveAndOwnedBy(
			anime,
			owner.Info(),
		)
		return nil, err
	}

	if len(inactive) == 0 {
		return nil, ErrNoAvailableClients
	}

	assignee := inactive[0]

	if err := fn(assignee); err != nil {
		return nil, err
	}

	return assignee, nil
}

func (m *manager) Owns(anime domain.AnimeBase) (owner client.Client) {
	m.rwmutex.RLock()
	defer m.rwmutex.RUnlock()

	for _, client := range m.clients {
		if trn := client.Transfer(); trn.Active() && trn.IsCurrent(anime) {
			return client
		}
	}

	return nil
}

func (m *manager) Info() (infos []client.Info) {
	m.rwmutex.RLock()
	defer m.rwmutex.RUnlock()

	infos = make([]client.Info, len(m.clients))
	for index, info := range m.clients {
		infos[index] = info.Info()
	}

	// sort by state where `Run` > `Pause` > `Wait`.
	sort.Slice(infos, func(i, j int) bool {
		return infos[i].State < infos[j].State
	})

	return infos
}
