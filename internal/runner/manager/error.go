package manager

import (
	"fmt"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
)

const (
	// ErrNoAvailableClients is thrown when there are no clients to accept the transfer.
	// This is generally thrown when there are either no clients connected or they're all in use.
	ErrNoAvailableClients = Error("no available clients, either they're all in use or none are connected")
)

// Error is a custom error implementation for the `manager` package
// in-order to have them stored as constants.
type Error string

func (e Error) Error() string {
	return string(e)
}

// ErrClientNotConnected is thrown when the client connection
// is not present in the manager.
func ErrClientNotConnected(ident string) Error {
	return Error(fmt.Sprintf(
		"client '%s' is not connected",
		ident,
	))
}

// ErrClientAlreadyConnected is thrown when the client connection
// is present in the manager.
func ErrClientAlreadyConnected(ident string) Error {
	return Error(fmt.Sprintf(
		"client '%s' is already connected",
		ident,
	))
}

// ErrTransferIsAlreadyActiveAndOwnedBy is thrown when we try to assign a transfer
// that's already owned by another client.
func ErrTransferIsAlreadyActiveAndOwnedBy(anime domain.AnimeBase, info client.Info) Error {
	return Error(fmt.Sprintf(
		"a transfer with '%s' is already owned by '%s'",
		anime.String(),
		info.String(),
	))
}
