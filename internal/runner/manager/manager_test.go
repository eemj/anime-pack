package manager

import (
	"strconv"
	"sync"
	"testing"
	"time"

	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"gitlab.com/eemj/anime-pack/internal/runner/transfer"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

var (
	runnerClaims = generateRunnerClaims(45)
	animes       = generateCommonAnime(45)
)

func generateRunnerClaims(n int) []*claims.RunnerClaims {
	runnerClaims := make([]*claims.RunnerClaims, n)
	for index := range runnerClaims {
		runnerClaims[index] = &claims.RunnerClaims{
			Ident:     utils.NewIdentifier(),
			ExpiresAt: time.Now().Add(15 * time.Minute),
		}
	}
	return runnerClaims
}

func generateCommonAnime(n int) []domain.AnimeBase {
	animes := make([]domain.AnimeBase, n)
	for index := range animes {
		animes[index] = domain.AnimeBase{
			Title:        "anime-pack",
			Episode:      strconv.Itoa(index + 1),
			ReleaseGroup: "anime-pack",
			Height:       1080,
		}
	}
	return animes
}

func TestAvailableDo(t *testing.T) {
	m := NewManager()

	for _, claim := range runnerClaims {
		if _, err := m.Connect(claim); err != nil {
			t.Fatal(err)
		}
	}

	store := sync.Map{}
	wg := sync.WaitGroup{}

	wg.Add(len(animes))

	for _, anime := range animes {
		go func(anime domain.AnimeBase) {
			m.AvailableDo(anime, func(client client.Client) (err error) {
				defer wg.Done()

				ident := client.Info().Ident

				occurences, ok := store.Load(ident)

				if ok {
					store.Store(ident, (occurences.(int) + 1))
				} else {
					store.Store(ident, 1)
				}

				client.Transfer().Setup(
					anime,
					transfer.NewNoopStream(),
					transfer.NewNoopStream(),
				)

				return
			})
		}(anime)
	}

	wg.Wait()

	store.Range(func(key, value any) bool {
		if value.(int) > 1 {
			t.Fatalf("'%s' occurred more than once (%d)", key.(string), value.(int))
		}

		return true
	})
}
