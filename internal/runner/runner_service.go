package runner

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/auth/generator"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"gitlab.com/eemj/anime-pack/internal/runner/manager"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type RunnerService interface {
	// Domain
	First(ctx context.Context, arg params.FirstRunnerParams) (*domain.Runner, error)

	ValidateAuth(ctx context.Context, arg params.ValidateAuthRunnerParams) (bool, *claims.RunnerClaims, error)

	// gRPC
	Auth(ctx context.Context, arg *grpc_runner_v1.AuthRequest) (*grpc_runner_v1.AuthResponse, error)
	Operation(ctx context.Context, runner client.Client, operation *grpc_runner_v1.OperationRequest) error
	Stream(ctx context.Context, runner client.Client, stream *grpc_runner_v1.StreamRequest) error

	// Connection + Leader
	Timeout(anime domain.AnimeBase) error
	Interrupt(anime domain.AnimeBase) error
	Assign(anime domain.AnimeBase, sizeInBytes uint64, address string) (client.Client, error)

	// Connection + State
	Info() []client.Info
	Client(runnerClaims *claims.RunnerClaims) client.Client
	Connect(runnerClaims *claims.RunnerClaims) (client.Client, error)
	ToggleState(runnerClaims *claims.RunnerClaims) (client.State, error)
	Disconnect(runnerClaims *claims.RunnerClaims) error
}

type runnerService struct {
	querier       database.QuerierExtended
	cacher        cacher.Cacher
	fs            *fs.FileSystem
	encodedCacher *encoded.EncodedCacher
	manager       manager.Manager
	queue         *queue.Queue
	jwtGenerator  generator.Generator
	associations  RunnerAssociations
	wshub         websocket.Hub
}

// Count implements RunnerService.
func (s *runnerService) Count(ctx context.Context, arg params.CountRunnerParams) (int64, error) {
	return s.querier.CountRunner(ctx, database.CountRunnerParams(arg))
}

// First implements RunnerService.
func (s *runnerService) First(ctx context.Context, arg params.FirstRunnerParams) (runner *domain.Runner, err error) {
	cacheKey := cachekeys.Runner(arg.Ident)
	if cacheKey != "" {
		runner, err = encoded.Get[*domain.Runner](ctx, s.encodedCacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if runner != nil {
			return runner, nil
		}
	}
	row, err := s.querier.FirstRunner(ctx, database.FirstRunnerParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	runner = &domain.Runner{
		ID:        row.ID,
		Name:      row.Name,
		Ident:     row.Ident,
		User:      row.User,
		Hostname:  row.Hostname,
		Signature: row.Signature,
		CreatedAt: row.CreatedAt,
		UpdatedAt: row.UpdatedAt,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.encodedCacher, cacheKey, runner, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return runner, nil
}

func (s *runnerService) Create(ctx context.Context, arg params.InsertRunnerParams) (*domain.Runner, error) {
	runner, err := s.First(ctx, params.FirstRunnerParams{Ident: &arg.Ident})
	if err != nil {
		return nil, err
	}
	if runner != nil {
		return runner, nil
	}

	// Initialize `signature` and `name` if empty
	if arg.Signature == "" {
		arg.Signature = utils.NewIdentifier()
	}
	if arg.Name == "" {
		arg.Name = fmt.Sprintf(
			"%s@%s~%s",
			arg.User,
			arg.Hostname,
			arg.Ident,
		)
	}

	// We don't have it, let's created it.
	row, err := s.querier.InsertRunner(ctx, database.InsertRunnerParams(arg))
	if err != nil {
		return nil, err
	}
	cacheKey := cachekeys.Runner(&arg.Ident)
	runner = &domain.Runner{
		ID:        row.ID,
		Name:      row.Name,
		Ident:     row.Ident,
		User:      row.User,
		Hostname:  row.Hostname,
		Signature: row.Signature,
		CreatedAt: row.CreatedAt,
		UpdatedAt: row.UpdatedAt,
	}
	// Cache it for next hit if possible.
	if cacheKey != "" {
		err = encoded.Set(ctx, s.encodedCacher, cacheKey, runner, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return runner, nil
}

func (s *runnerService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.RunnerServiceName)
}

func NewRunnerService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	fs *fs.FileSystem,
	queue *queue.Queue,
	wshub websocket.Hub,
	associations RunnerAssociations,
) RunnerService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &runnerService{
		querier:       querier,
		cacher:        cacher,
		fs:            fs,
		wshub:         wshub,
		queue:         queue,
		associations:  associations,
		encodedCacher: encodedCacher,
		manager:       manager.NewManager(),
		jwtGenerator:  generator.NewGenerator(),
	}
}
