package transfer

import (
	"crypto/sha256"
	"encoding/hex"
	"io"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// verify calculates the size and hash of the specified stream,
// and matches then with the specified expected values.
func verify(
	stream Stream,
	expectedSize int64,
	expectedHash string,
) (err error) {
	// prevent a segfault
	if stream == nil {
		return ErrUnallocatedStream
	}

	stat, err := stream.Stat()
	if err != nil {
		return
	}

	size := stat.Size()

	if size != expectedSize {
		return ErrSizeVerification(expectedSize, size)
	}

	if _, err = stream.Seek(0, io.SeekStart); err != nil {
		return
	}

	sha := sha256.New()

	if _, err = io.Copy(sha, stream); err != nil {
		return
	}

	if _, err = stream.Seek(0, io.SeekStart); err != nil {
		return
	}

	if sha := hex.EncodeToString(sha.Sum(nil)); expectedHash != sha {
		return ErrHashVerification(expectedHash, sha)
	}

	return
}

func (t *transfer) verifyVideo() (err error) {
	if err = verify(
		t.videoFs,
		t.metadata.Video.Size,
		t.metadata.Video.SHA256,
	); err != nil {
		return
	}

	videoCRC32, err := utils.ChecksumCRC32(t.videoFs)
	if err != nil {
		return
	}

	_, err = t.videoFs.Seek(0, io.SeekStart)

	if err != nil {
		return
	}

	if t.metadata.Video.CRC32 != videoCRC32 {
		return ErrHashVerification(t.metadata.Video.CRC32, videoCRC32)
	}

	return
}

func (t *transfer) verifyImage() (err error) {
	return verify(
		t.imageFs,
		t.metadata.Image.Size,
		t.metadata.Image.SHA256,
	)
}

func (t *transfer) Verify() (err error) {
	t.rwmutex.RLock()

	// Iterate and execute through the verification methods.
	for _, verification := range []func() error{
		t.verifyImage,
		t.verifyVideo,
	} {
		if err = verification(); err != nil {
			t.rwmutex.RUnlock()
			t.Close()
			return
		}
	}

	t.rwmutex.RUnlock()

	return
}
