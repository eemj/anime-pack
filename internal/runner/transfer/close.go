package transfer

import (
	"os"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

func close(stream Stream) (err error) {
	if stream != nil {
		if err = stream.Close(); err != nil {
			return
		}

		if err = os.Remove(stream.Name()); err != nil && !os.IsNotExist(err) {
			return
		}

		stream = nil
	}

	return
}

// Close resets the stream type, closes all the streams and attempts deletes
// the stream files.
func (t *transfer) Close() (err error) {
	if t.Active() {
		t.rwmutex.Lock()
		defer t.rwmutex.Unlock()

		t.corrupt = t.corrupt[:0]
		t.anime = domain.AnimeBase{}
		t.metadata = Metadata{}

		if err = close(t.videoFs); err != nil {
			return
		}

		err = close(t.imageFs)
	}

	return
}
