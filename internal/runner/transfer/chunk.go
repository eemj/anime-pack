package transfer

import (
	"fmt"
	"sort"
	"sync"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

type StreamChunk struct {
	Start int64
	End   int64
}

type StreamChunks struct {
	cap  int64
	sz   int64
	rmu  sync.RWMutex
	chk  []StreamChunk
	once sync.Once
}

func NewStreamChunks(chunks ...StreamChunk) *StreamChunks {
	return &StreamChunks{chk: chunks}
}

func (c *StreamChunks) Reset() {
	c.rmu.Lock()
	c.cap = 0
	c.sz = 0
	c.once = sync.Once{}
	c.chk = make([]StreamChunk, 0)
	c.rmu.Unlock()
}

func (c *StreamChunks) Append(
	req *grpc_runner_v1.StreamRequest,
) (err error) {
	c.once.Do(func() {
		c.rmu.Lock()
		c.cap = req.Size
		c.rmu.Unlock()
	})

	if req.End > c.cap {
		return fmt.Errorf(
			"end `%d` is larger than capacity `%d`",
			req.End,
			c.cap,
		)
	}

	c.rmu.Lock()
	c.chk = append(c.chk, StreamChunk{Start: req.Start, End: req.End})
	c.rmu.Unlock()

	return
}

func (c *StreamChunks) Len() int {
	c.rmu.RLock()
	defer c.rmu.RUnlock()
	return len(c.chk)
}

func (c *StreamChunks) Swap(i, j int) {
	c.rmu.Lock()
	c.chk[i], c.chk[j] = c.chk[j], c.chk[i]
	c.rmu.Unlock()
}

func (c *StreamChunks) Less(i, j int) bool {
	c.rmu.RLock()
	defer c.rmu.RUnlock()
	return c.chk[i].Start < c.chk[j].End
}

func (c *StreamChunks) Chunks() (streamChunks []StreamChunk) {
	c.rmu.RLock()
	streamChunks = make([]StreamChunk, len(c.chk))
	copy(streamChunks, c.chk)
	c.rmu.RUnlock()
	return
}

func (c *StreamChunks) Copy() (streamChunks *StreamChunks) {
	c.rmu.RLock()
	defer c.rmu.RUnlock()
	chk := make([]StreamChunk, len(c.chk))
	copy(c.chk, chk)
	return &StreamChunks{
		cap: c.cap,
		sz:  c.sz,
		chk: chk,
	}
}

func (c *StreamChunks) Merge() {
	merged := make([]StreamChunk, 0)

	sort.Sort(c)

	c.rmu.Lock()
	// Merge the chunks into the created merged slice.
	for _, chunk := range c.chk {
		last := (len(merged) - 1)
		if last < 0 || chunk.Start > merged[last].End {
			merged = append(merged, chunk)
		} else if chunk.End > merged[last].End {
			merged[last].End = chunk.End
		}
	}

	// Replace our chunks with the merged.
	c.chk = merged[:len(merged):len(merged)]
	c.rmu.Unlock()
}

func (c *StreamChunks) Missing() (streamChunks []StreamChunk) {
	copy := c.Copy()
	copy.Merge()
	num := copy.Len()
	streamChunks = make([]StreamChunk, 0)

	for index, chunk := range copy.chk {
		if (index + 1) >= num {
			nextChunk := copy.chk[(index + 1)]

			streamChunks = append(streamChunks, StreamChunk{
				Start: chunk.End,
				End:   nextChunk.Start,
			})
		}
	}

	return
}
