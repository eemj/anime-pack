package transfer

import (
	"io"
	"io/fs"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// Stream consists of all the methods required for a transfer
// we're basically interfacing out the *os.File so other methods
// could make use of this method (say in the future we decide to store in
// an S3 bucket).
type Stream interface {
	io.Reader
	io.Seeker
	io.WriterAt
	io.Closer
	Stat() (fs.FileInfo, error)
	Name() string
}

type noopStream struct {
	io.Reader
	io.Seeker
	io.WriterAt
	io.Closer
	name string
}

func NewNoopStream() Stream {
	return &noopStream{
		name: utils.NewIdentifier(),
	}
}

func (s *noopStream) Read(p []byte) (int, error) {
	return 0, io.EOF
}

func (s *noopStream) Seek(offset int64, whence int) (int64, error) {
	return (offset + int64(whence)), nil
}

func (s *noopStream) Write(p []byte) (int, error) {
	return len(p), nil
}

func (s *noopStream) Close() error { return nil }

func (s *noopStream) Stat() (fs.FileInfo, error) {
	return nil, nil
}

func (s *noopStream) Name() string { return s.name }
