package transfer

import (
	"fmt"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

// Error is a custom error implementation for the `transfer` package
// in-order to have them stored as constants.
type Error string

func (e Error) Error() string {
	return string(e)
}

const (
	// ErrUnallocatedStream is throw when an action is attempted to be performed on a
	// nil stream.
	ErrUnallocatedStream = Error("unallocated stream")

	// ErrInactive is thrown when a transfer is expected to be active, but it's inactive.
	ErrInactive = Error("inactive transfer")
)

// ErrSizeVerification is thrown during the verification process,
// when the metadata size does not match the stream size
func ErrSizeVerification(expectedSize, outcomeSize int64) Error {
	return Error(fmt.Sprintf(
		"size verification failed (%d != %d)",
		expectedSize,
		outcomeSize,
	))
}

// ErrHashVerification is thrown during the verification process,
// when the metadata hash does not match the stream hash
func ErrHashVerification(expectedHash, outcomeHash string) Error {
	return Error(fmt.Sprintf(
		"hash verification failed ('%s' != '%s')",
		expectedHash,
		outcomeHash,
	))
}

// ErrRequestAdler32Checksum is thrown during the stream upload process,
// when the request fnv32a does not match the []byte request content.
func ErrRequestAdler32Checksum(request *grpc_runner_v1.StreamRequest, outcome uint32) Error {
	return Error(fmt.Sprintf(
		"adler32 checksum failed: (%s) for %d-%d (%d != %d)",
		request.Stream.String(),
		request.Start,
		request.End,
		request.Adler32,
		outcome,
	))
}
