package transfer

type MetadataVideoDimension struct {
	Height int64 `json:"height"`
	Width  int64 `json:"width"`
}

type MetadataVideo struct {
	Dimension *MetadataVideoDimension `json:"dimension"`
	SHA256    string                  `json:"sha256"`
	CRC32     string                  `json:"crc32"`
	Duration  float64                 `json:"duration"`
	Size      int64                   `json:"size"`
}

type MetadataImage struct {
	SHA256 string `json:"sha256"`
	Size   int64  `json:"size"`
}

type Metadata struct {
	Video *MetadataVideo `json:"video"`
	Image *MetadataImage `json:"image"`
}
