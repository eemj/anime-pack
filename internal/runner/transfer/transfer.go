package transfer

import (
	"io"
	"sync"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/domain"
)

type Transfer interface {
	Active() bool

	Setup(anime domain.AnimeBase, imageFs, videoFs Stream)

	WriterAt(typ grpc_runner_v1.StreamType) io.WriterAt
	Reader(typ grpc_runner_v1.StreamType) io.Reader
	Corrupt(request *grpc_runner_v1.StreamRequest)

	CorruptParts() int
	IsCurrent(anime domain.AnimeBase) bool

	SetMetadata(metadata Metadata)

	Metadata() Metadata
	Anime() domain.AnimeBase

	Verify() error
	Close() error
}

type transfer struct {
	Transfer

	rwmutex sync.RWMutex

	anime    domain.AnimeBase
	metadata Metadata

	imageFs Stream
	videoFs Stream

	corrupt []*grpc_runner_v1.StreamRequest
}

func New() Transfer {
	return &transfer{}
}

func (t *transfer) WriterAt(typ grpc_runner_v1.StreamType) io.WriterAt {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	if typ == grpc_runner_v1.StreamType_VIDEO {
		return t.videoFs
	}
	return t.imageFs
}

func (t *transfer) Reader(typ grpc_runner_v1.StreamType) io.Reader {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	if typ == grpc_runner_v1.StreamType_VIDEO {
		return t.videoFs
	}
	return t.imageFs
}

func (t *transfer) CorruptParts() int {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	return len(t.corrupt)
}

func (t *transfer) Corrupt(request *grpc_runner_v1.StreamRequest) {
	t.rwmutex.Lock()
	t.corrupt = append(t.corrupt, request)
	t.rwmutex.Unlock()
}

func (t *transfer) Metadata() Metadata {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	return t.metadata
}

func (t *transfer) Anime() domain.AnimeBase {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	return t.anime
}

func (t *transfer) Active() bool {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()

	if t.anime.IsZero() {
		return false
	}

	return t.imageFs != nil && t.videoFs != nil
}

func (t *transfer) IsCurrent(anime domain.AnimeBase) bool {
	t.rwmutex.RLock()
	defer t.rwmutex.RUnlock()
	return t.anime.Equal(anime)
}

func (t *transfer) Setup(anime domain.AnimeBase, imageFs, videoFs Stream) {
	t.rwmutex.Lock()
	t.anime = anime
	t.imageFs = imageFs
	t.videoFs = videoFs
	t.rwmutex.Unlock()
}

func (t *transfer) SetMetadata(metadata Metadata) {
	t.rwmutex.Lock()
	t.metadata = metadata
	t.rwmutex.Unlock()
}
