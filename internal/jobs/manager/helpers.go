package manager

import (
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

func createPayload(info task.Info) protocol.Payload {
	return protocol.Payload{
		Opcode: protocol.Status_Job,
		Data: &protocol.DataStatusJob{
			ID:           info.ID.String(),
			State:        protocol.JobState(info.State.String()),
			LastActivity: info.LastActivity,
			NextActivity: info.NextActivity,
			StartedAt:    info.StartedAt,
			EndedAt:      info.EndedAt,
			Period:       info.Period,
		},
	}
}
