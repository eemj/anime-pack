package manager

import "time"

type Settings struct {
	Disable bool
	Period  time.Duration
	Manual  bool
}
