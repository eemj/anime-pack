package manager

import (
	"context"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"go.uber.org/zap"
)

type TaskInfo struct {
	rmu sync.RWMutex

	task task.Tasker

	State task.State

	LastActivity time.Time
	StartedAt    *time.Time
	EndedAt      *time.Time

	CancelFunc context.CancelFunc
	Settings   Settings
	Config     any

	Trigger chan struct{}
}

func (s *TaskInfo) Run(ctx context.Context) error     { return s.task.Run(ctx) }
func (s *TaskInfo) Name() string                      { return s.task.Name() }
func (s *TaskInfo) ID() uuid.UUID                     { return s.task.ID() }
func (s *TaskInfo) L(ctx context.Context) *zap.Logger { return s.task.L(ctx) }

func (s *TaskInfo) Info() task.Info {
	s.rmu.RLock()
	defer s.rmu.RUnlock()

	info := task.Info{
		ID:   s.task.ID(),
		Name: s.task.Name(),

		State:        s.State,
		Period:       s.Settings.Period,
		LastActivity: s.LastActivity,
		StartedAt:    s.StartedAt,
		EndedAt:      s.EndedAt,
	}

	if info.State != task.State_Disable {
		nextActivity := s.LastActivity.Add(s.Settings.Period)
		info.NextActivity = &nextActivity
	}

	return info
}

func (s *TaskInfo) CurrentStateIs(states ...task.State) (task.State, bool) {
	s.rmu.RLock()
	defer s.rmu.RUnlock()
	for _, state := range states {
		if s.State == state {
			return s.State, true
		}
	}
	return s.State, false
}

func (s *TaskInfo) StateIs(states ...task.State) bool {
	_, ok := s.CurrentStateIs(states...)
	return ok
}

func (s *TaskInfo) Enable() *TaskInfo {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	s.State = task.State_Enable
	s.LastActivity = time.Now().UTC()

	return s
}

func (s *TaskInfo) Disable() *TaskInfo {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	nowUtc := time.Now().UTC()

	if s.State == task.State_Active {
		if s.CancelFunc != nil {
			s.CancelFunc()
			s.CancelFunc = nil
		}

		s.EndedAt = &nowUtc
	}

	s.State = task.State_Disable
	s.LastActivity = nowUtc

	return s
}

func (s *TaskInfo) Active(cancelFunc context.CancelFunc) *TaskInfo {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	nowUtc := time.Now().UTC()
	s.StartedAt = &nowUtc
	s.LastActivity = nowUtc
	s.State = task.State_Active
	s.CancelFunc = cancelFunc

	return s
}

func (s *TaskInfo) Passive() *TaskInfo {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	nowUtc := time.Now().UTC()

	if s.State == task.State_Active {
		s.EndedAt = &nowUtc
	}

	s.State = task.State_Passive
	s.LastActivity = nowUtc
	s.CancelFunc = nil

	return s
}

func TaskInfoFor(tasker task.Tasker, settings Settings) *TaskInfo {
	taskInfo := &TaskInfo{
		task: tasker,

		Settings:     settings,
		Trigger:      make(chan struct{}, 1),
		LastActivity: time.Now().UTC(),
	}

	if settings.Disable {
		taskInfo.State = task.State_Disable
	} else {
		taskInfo.State = task.State_Enable
	}

	return taskInfo
}
