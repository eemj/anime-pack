package manager

import (
	"github.com/google/uuid"
)

type TaskError struct {
	ID   uuid.UUID
	Name string
	Err  error
}

func (e TaskError) Error() string {
	return e.Err.Error()
}

func NewTaskError(
	id uuid.UUID,
	name string,
	parentError error,
) error {
	return &TaskError{ID: id, Name: name, Err: parentError}
}
