package manager

import (
	"context"
	"errors"
	"reflect"
	"sort"
	"time"

	"github.com/google/uuid"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"go.jamie.mt/logx/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type Manager interface {
	Register(tasker task.Tasker, settings Settings)
	Unregister(tasker task.Tasker)

	Enable(id uuid.UUID) (err error)
	Disable(id uuid.UUID) (err error)

	Run(ctx context.Context, id uuid.UUID) (err error)
	Stop(ctx context.Context, id uuid.UUID) (err error)

	StartAll(ctx context.Context) (err error)

	Get(id uuid.UUID) (taskInfo task.Info, err error)

	Info() []task.Info

	Errors() <-chan error

	Close()
}

type manager struct {
	tracer trace.Tracer
	hub    websocket.Hub
	tasks  IdentMap[*TaskInfo]
	errc   chan error
}

func New(hub websocket.Hub) Manager {
	return &manager{
		tracer: otel.Tracer(reflect.TypeOf(&manager{}).PkgPath()),
		hub:    hub,
		tasks:  newMaperMap[*TaskInfo](),
		errc:   make(chan error, 1),
	}
}

func (m *manager) Close() {
	for _, entry := range m.tasks.Entries() {
		if entry.StateIs(task.State_Active) && entry.CancelFunc != nil {
			entry.CancelFunc()
		}

		m.tasks.Set(entry.Disable())
		m.tasks.Remove(entry)
	}
}

func (m *manager) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named("manager").
		Named("task")
}

func (m *manager) Errors() <-chan error { return m.errc }

func (m *manager) Get(id uuid.UUID) (taskInfo task.Info, err error) {
	if task := m.tasks.FirstByID(id); task == nil {
		err = apperror.ErrJobTaskNotFound
	} else {
		taskInfo = task.Info()
	}

	return
}

func (m *manager) Info() []task.Info {
	infos := make(task.ByName, 0)
	m.tasks.Range(func(n *TaskInfo) bool {
		infos = append(infos, n.Info())
		return true
	})
	sort.Sort(infos)
	return infos
}

func (m *manager) taskSetPassive(ctx context.Context, taskInfo *TaskInfo, err error) {
	if err != nil {
		taskInfo.L(ctx).Error(
			"status_update",
			zap.Stringer("state", taskInfo.State),
			zap.Error(err),
		)

		select {
		case m.errc <- NewTaskError(taskInfo.ID(), taskInfo.Name(), err):
		default:
		}
	}

	m.tasks.Set(taskInfo.Passive())

	taskInfo.L(ctx).Info(
		"status_update",
		zap.Stringer("state", taskInfo.State),
	)

	m.hub.BroadcastPayload(createPayload(taskInfo.Info()))
}

// run will only run if the task's state is PASSIVE. If an error is occurred it will pass
// if it to the manager's error channel.
func (m *manager) run(ctx context.Context, taskInfo *TaskInfo) {
	defer func() {
		if recovery := recover(); recovery != nil {
			var err error
			switch value := recovery.(type) {
			case string:
				err = errors.New(value)
			case error:
				err = value
			default:
				err = errors.New("unknown panic")
			}
			m.taskSetPassive(ctx, taskInfo, err)
		}
	}()

	if !taskInfo.StateIs(task.State_Passive) {
		return
	}

	taskName := taskInfo.Name()
	spanName := (taskName + ".Run")
	spanCtx, span := m.tracer.Start(ctx, spanName, trace.WithAttributes(
		attribute.String("job.name", taskName),
		attribute.String("job.id", taskInfo.ID().String()),
	))
	defer span.End()

	ctx, cancelFunc := context.WithCancel(spanCtx)
	defer cancelFunc()

	m.tasks.Set(taskInfo.Active(cancelFunc))

	taskInfo.L(ctx).Info(
		"status_update",
		zap.Stringer("state", taskInfo.State),
	)

	m.hub.BroadcastPayload(createPayload(taskInfo.Info()))

	err := taskInfo.Run(ctx)

	m.taskSetPassive(ctx, taskInfo, err)
}

func (m manager) Stop(ctx context.Context, uuid uuid.UUID) (err error) {
	taskInfo := m.tasks.FirstByID(uuid)

	if taskInfo == nil {
		return apperror.ErrJobTaskNotFound
	}

	if taskInfo.StateIs(task.State_Passive) {
		return apperror.ErrJobTaskNotRunning
	}

	if taskInfo.CancelFunc != nil {
		taskInfo.CancelFunc()
	}

	m.tasks.Set(taskInfo.Passive())

	m.hub.BroadcastPayload(createPayload(taskInfo.Info()))

	return
}

func (m *manager) Run(ctx context.Context, uuid uuid.UUID) (err error) {
	taskInfo := m.tasks.FirstByID(uuid)
	if taskInfo == nil {
		return apperror.ErrJobTaskNotFound
	}

	currentState, ok := taskInfo.CurrentStateIs(task.State_Disable, task.State_Active)
	if ok {
		switch currentState {
		case task.State_Disable:
			return apperror.ErrJobTaskIsDisabled
		case task.State_Active:
			return apperror.ErrJobTaskAlreadyRunning
		}
	}

	if taskInfo.Settings.Manual {
		if !taskInfo.StateIs(task.State_Passive) {
			m.tasks.Set(taskInfo.Passive())
		} else {
			go m.run(context.TODO(), taskInfo) // De-attach the context
		}
	} else {
		if !taskInfo.StateIs(task.State_Passive) {
			go func() {
				m.tasks.Set(taskInfo.Passive())

				ticker := time.NewTicker(taskInfo.Settings.Period)
				defer ticker.Stop()

				for {
					select {

					// this gets triggered when a Disable/Enable/Force Run event occurs
					case <-taskInfo.Trigger:
						// if the current state is disable, we'll return to close off this goroutine
						if taskInfo.StateIs(task.State_Disable) {
							return
						}

						m.run(ctx, taskInfo)
					case <-ticker.C:
						m.run(ctx, taskInfo)
					case <-ctx.Done():
						return
					}
				}
			}()
		}

		taskInfo.Trigger <- struct{}{} // Start immediately.
	}

	return
}

func (m *manager) Disable(uuid uuid.UUID) (err error) {
	taskInfo := m.tasks.FirstByID(uuid)

	if taskInfo == nil {
		return apperror.ErrJobTaskNotFound
	}

	if taskInfo.StateIs(task.State_Disable) {
		return apperror.ErrJobTaskAlreadyDisabled
	} else if taskInfo.StateIs(task.State_Active, task.State_Passive) {
		m.tasks.Set(taskInfo.Disable())
		taskInfo.Trigger <- struct{}{}
	}

	m.hub.BroadcastPayload(createPayload(taskInfo.Info()))

	return
}

func (m *manager) Enable(uuid uuid.UUID) (err error) {
	taskInfo := m.tasks.FirstByID(uuid)

	if taskInfo == nil {
		return apperror.ErrJobTaskNotFound
	}

	if !taskInfo.StateIs(task.State_Disable) {
		return apperror.ErrJobTaskIsNotDisabled
	}

	m.tasks.Set(taskInfo.Enable())

	m.hub.BroadcastPayload(createPayload(taskInfo.Info()))

	return
}

func (m *manager) StartAll(ctx context.Context) error {
	for _, entry := range m.tasks.Entries() {
		if err := m.Run(ctx, entry.ID()); err != nil &&
			!errors.Is(err, apperror.ErrJobTaskIsDisabled) &&
			!errors.Is(err, apperror.ErrJobTaskNotRunning) &&
			!errors.Is(err, apperror.ErrJobTaskAlreadyRunning) {
			return err
		}
	}

	return nil
}

func (m *manager) Register(tasker task.Tasker, settings Settings) {
	m.tasks.Add(TaskInfoFor(tasker, settings))

	m.L(context.TODO()).Debug(
		"registered job task",
		zap.String("name", tasker.Name()),
		zap.Any("config", tasker.Config()),
	)
}

func (m *manager) Unregister(tasker task.Tasker) {
	m.tasks.Remove(&TaskInfo{task: tasker})

	m.L(context.TODO()).Debug("unregistered job task", zap.String("name", tasker.Name()))
}
