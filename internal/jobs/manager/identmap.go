package manager

import (
	"sync"

	"github.com/google/uuid"
)

type Identifiable interface{ ID() uuid.UUID }

type IdentMap[N Identifiable] interface {
	Add(n N)
	Set(n N)
	Remove(n N)
	Has(n N) (has bool)
	Len() (n int)
	Entries() []N
	Range(func(n N) (next bool))
	First(func(n N) (condition bool)) (n N)
	FirstByID(id uuid.UUID) (n N)
}

type identMap[N Identifiable] struct {
	m   map[uuid.UUID]N
	rmu sync.RWMutex
}

func newMaperMap[N Identifiable]() IdentMap[N] {
	return &identMap[N]{
		m: make(map[uuid.UUID]N),
	}
}

func (m *identMap[N]) Set(n N) {
	m.rmu.Lock()
	m.m[n.ID()] = n
	m.rmu.Unlock()
}

func (m *identMap[N]) Add(n N) {
	m.rmu.Lock()
	if _, exists := m.m[n.ID()]; !exists {
		m.m[n.ID()] = n
	}
	m.rmu.Unlock()
}

func (m *identMap[N]) Remove(n N) {
	m.rmu.Lock()
	delete(m.m, n.ID())
	m.rmu.Unlock()
}

func (m *identMap[N]) Has(n N) (has bool) {
	m.rmu.RLock()
	_, has = m.m[n.ID()]
	m.rmu.RUnlock()
	return
}

func (m *identMap[N]) Len() (n int) {
	m.rmu.RLock()
	n = len(m.m)
	m.rmu.RUnlock()
	return
}

func (m *identMap[N]) Entries() (entries []N) {
	m.rmu.RLock()
	for _, entry := range m.m {
		entries = append(entries, entry)
	}
	m.rmu.RUnlock()
	return
}

func (m *identMap[N]) Range(cb func(n N) (next bool)) {
	m.rmu.RLock()
	defer m.rmu.RUnlock()

	for _, ident := range m.m {
		if !cb(ident) {
			break
		}
	}
}

func (m *identMap[N]) FirstByID(id uuid.UUID) (n N) {
	m.rmu.RLock()
	defer m.rmu.RUnlock()
	return m.m[id]
}

func (m *identMap[N]) First(cb func(n N) (condition bool)) (n N) {
	m.rmu.RLock()
	defer m.rmu.RUnlock()

	for _, ident := range m.m {
		if cb(ident) {
			return ident
		}
	}

	return
}
