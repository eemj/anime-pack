package task

import (
	"github.com/google/uuid"
	"gitlab.com/eemj/anime-pack/internal/feed"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"go.opentelemetry.io/otel"
)

type TaskFactory interface {
	Create(name string, config any) *Task
}

type taskFactory struct {
	svcs  lifecycle.ServiceLifecycle
	queue *queue.Queue
	fs    *fs.FileSystem
	feed  feed.Feed
}

func (f *taskFactory) Create(name string, config any) *Task {
	return &Task{
		svcs:   f.svcs,
		queue:  f.queue,
		fs:     f.fs,
		feed:   f.feed,
		name:   name,
		config: config,
		id:     uuid.New(),
		tracer: otel.Tracer(name),
	}
}

func NewFactory(
	svcs lifecycle.ServiceLifecycle,
	queue *queue.Queue,
	fs *fs.FileSystem,
	feed feed.Feed,
) TaskFactory {
	return &taskFactory{
		svcs:  svcs,
		queue: queue,
		fs:    fs,
		feed:  feed,
	}
}
