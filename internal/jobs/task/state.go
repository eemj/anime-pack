package task

import "strings"

type State int

const (
	State_Undefined State = 1 << iota
	State_Enable
	State_Disable
	State_Passive
	State_Active
)

func (s State) String() string {
	switch s {
	case State_Undefined:
		return "UNDEFINED"
	case State_Enable:
		return "ENABLE"
	case State_Disable:
		return "DISABLE"
	case State_Passive:
		return "PASSIVE"
	case State_Active:
		return "ACTIVE"
	}

	return "UNDEFINED"
}

func (s State) MarshalJSON() ([]byte, error) {
	return []byte(`"` + s.String() + `"`), nil
}

func (s *State) UnmarshalJSON(b []byte) error {
	unquoted := strings.Trim(string(b), `"`)
	*s = GetState(unquoted)
	return nil
}

func GetState(state string) State {
	switch state {
	case State_Undefined.String():
		return State_Undefined
	case State_Enable.String():
		return State_Enable
	case State_Disable.String():
		return State_Disable
	case State_Passive.String():
		return State_Passive
	case State_Active.String():
		return State_Active
	}

	return State_Undefined
}
