package task

import (
	"time"

	"github.com/google/uuid"
)

type ByName []Info

func (n ByName) Less(i, j int) bool { return n[i].Name < n[j].Name }
func (n ByName) Swap(i, j int)      { n[i], n[j] = n[j], n[i] }
func (n ByName) Len() int           { return len(n) }

type ByLastActivity []Info

func (n ByLastActivity) Less(i, j int) bool { return n[i].LastActivity.Before(n[j].LastActivity) }
func (n ByLastActivity) Swap(i, j int)      { n[i], n[j] = n[j], n[i] }
func (n ByLastActivity) Len() int           { return len(n) }

type Info struct {
	ID           uuid.UUID     `json:"id"`
	Name         string        `json:"name"`
	State        State         `json:"state"`
	LastActivity time.Time     `json:"lastActivity,omitempty"`
	StartedAt    *time.Time    `json:"startedAt,omitempty"`
	EndedAt      *time.Time    `json:"endedAt,omitempty"`
	NextActivity *time.Time    `json:"nextActivity,omitempty"`
	Period       time.Duration `json:"period"`
}
