package task

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/feed"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"go.jamie.mt/logx/log"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	"github.com/google/uuid"
)

const (
	taskNamespace = "task"
	fieldID       = "id"
)

type Task struct {
	id uuid.UUID

	svcs   lifecycle.ServiceLifecycle
	tracer trace.Tracer
	queue  *queue.Queue
	fs     *fs.FileSystem
	feed   feed.Feed

	name   string
	ctx    context.Context
	config any
}

func (t *Task) Tracer() trace.Tracer { return t.tracer }

func (t *Task) Svcs() lifecycle.ServiceLifecycle { return t.svcs }
func (t *Task) Queue() *queue.Queue              { return t.queue }
func (t *Task) FS() *fs.FileSystem               { return t.fs }
func (t *Task) Feed() feed.Feed                  { return t.feed }

func (t *Task) ID() uuid.UUID            { return t.id }
func (t *Task) Name() string             { return t.name }
func (t *Task) Context() context.Context { return t.ctx }
func (t *Task) Config() any              { return t.config }
func (t *Task) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(taskNamespace).
		Named(t.name).
		With(zap.Stringer(fieldID, t.id))
}

type Tasker interface {
	Config() any
	Svcs() lifecycle.ServiceLifecycle
	Queue() *queue.Queue
	FS() *fs.FileSystem
	Feed() feed.Feed
	ID() uuid.UUID
	Name() string
	Run(ctx context.Context) error
	L(ctx context.Context) *zap.Logger
}
