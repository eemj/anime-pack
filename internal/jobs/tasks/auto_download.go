package tasks

import (
	"context"
	"sort"

	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

type AutoDownload struct {
	*task.Task
}

func (j *AutoDownload) Run(ctx context.Context) (err error) {
	config, err := utils.TryCast[*configmodels.AutoDownload](j.Config())
	if err != nil {
		return err
	}

	if config.Pages < 1 {
		config.Pages = 1
	}

	var animeLatest []*domain.AnimeLatest
	for page := 1; page <= config.Pages; page++ {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		paginatedAnime, err := j.Svcs().
			Anime().
			Associations().
			AnimeLatest().
			Filter(ctx, params.FilterAnimeLatestParams{
				Page:     1,
				Elements: 100,
			})
		if err != nil {
			return err
		}

		animeLatest = append(animeLatest, paginatedAnime.Data...)
	}

	animePreferences := make(map[*domain.AnimeLatest]*domain.Preferences)
	animeBestQualityPerReleaseGroup := make(map[int64]domain.LibraryBestQualitiesPerReleaseGroup)

	for _, anime := range animeLatest {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		preferences, err := j.Svcs().Preferences().First(ctx, params.FirstPreferenceParams{
			AnimeID:            &anime.ID,
			AutomaticDownloads: utils.PointerOf(true),
		})
		if err != nil {
			return err
		}
		if preferences == nil {
			continue
		}

		if _, exists := animePreferences[anime]; !exists {
			animePreferences[anime] = preferences
		}

		if config.AlwaysBestQuality {
			_, exists := animeBestQualityPerReleaseGroup[anime.ID]
			if exists {
				continue
			}
			bestQualitiesPerReleaseGroup, err := j.Svcs().Library().BestQualityPerReleaseGroup(
				ctx,
				params.BestQualityPerReleaseGroupParams{AnimeID: anime.ID},
			)
			if err != nil {
				return err
			}
			animeBestQualityPerReleaseGroup[anime.ID] = bestQualitiesPerReleaseGroup
		}
	}

	for animeLatest := range animePreferences {
		xdccs, err := j.Svcs().Xdcc().Filter(ctx, params.FilterXDCCParams{
			TitleEpisodeID: &animeLatest.Episode.ID,
		})
		if err != nil {
			return err
		}

		var (
			quality  *domain.Quality
			newXDCCs []*domain.XDCC
		)
		// Determine the best quality by the `height` property.
		for _, xdcc := range xdccs {
			blacklisted := false

			for _, group := range config.BlacklistGroups {
				if xdcc.ReleaseGroup.Name == group {
					blacklisted = true
					break
				}
			}

			if blacklisted {
				continue
			}

			// Auto Download won't work if the XDCC entry does not have a quality entry..
			if xdcc.Quality != nil {
				if quality == nil || quality.Height < xdcc.Quality.Height {
					quality = xdcc.Quality
					newXDCCs = make([]*domain.XDCC, 0)
				}

				if xdcc.Quality.Height == quality.Height {
					newXDCCs = append(newXDCCs, xdcc)
				}
			}
		}

		if len(newXDCCs) == 0 {
			continue
		}

		// Replace the retrieved XDCC's with the just filtered list
		xdccs = newXDCCs

		// After we determine the best quality, next up is to determine the fansub group
		sort.Slice(xdccs, func(a, b int) bool {
			aprio, exists := config.PreferredGroups[xdccs[a].ReleaseGroup.Name]

			if !exists {
				return false
			}

			bprio, exists := config.PreferredGroups[xdccs[b].ReleaseGroup.Name]

			if !exists {
				return false
			}

			return aprio < bprio
		})

		// If we have a video that's associated with one of the filtered XDCC's
		// we'll skip the download.
		exists := false
		for _, xdcc := range xdccs {
			xdccCount, err := j.Svcs().Video().Count(ctx, params.CountVideoParams{
				XDCCID: &xdcc.ID,
			})
			if err != nil {
				return err
			}
			if xdccCount > 0 {
				exists = true
				break
			}
		}
		if exists {
			continue
		}

		anime := domain.AnimeBase{
			ID:           animeLatest.ID,
			Title:        animeLatest.Title,
			Episode:      animeLatest.Episode.Name,
			ReleaseGroup: xdccs[0].ReleaseGroup.Name,
		}

		if quality != nil {
			anime.Height = int(quality.Height)
		}

		if config.AlwaysBestQuality {
			bestQualitiesForReleaseGroup, exists := animeBestQualityPerReleaseGroup[animeLatest.ID]
			if !exists {
				j.L(ctx).Warn(
					"always best quality is on, but there's no entries for this anime",
					zap.Int64("anime_id", animeLatest.ID),
				)
				return nil
			}

			bestQuality := bestQualitiesForReleaseGroup.FindByReleaseGroup(anime.ReleaseGroup)
			if !((bestQuality == nil && quality == nil) || (bestQuality != nil && quality != nil && bestQuality.Height == quality.Height)) {
				if j.L(ctx).Level().Enabled(zap.DebugLevel) {
					var (
						bestQualityHeight *int64
						qualityHeight     *int64
					)
					if bestQuality != nil {
						bestQualityHeight = &bestQuality.Height
					}
					if quality != nil {
						qualityHeight = &quality.Height
					}

					j.L(ctx).Debug(
						"attempted to auto download a quality, which isn't the best",
						zap.Int64p("quality_height", qualityHeight),
						zap.Int64p("best_quality_height", bestQualityHeight),
					)
				}

				continue
			}
		}

		// Check if we have the file already..
		if j.FS().HasAnime(anime) {
			j.L(ctx).Debug("anime is already in our file system", zap.Stringer("anime", anime))
			continue
		}

		queueValue := &queue.State{
			Details:     make([]queue.Detail, len(xdccs)),
			StartedFrom: queue.StartedFrom_AutoDownload,
		}

		for index, xdcc := range xdccs {
			queueValue.Details[index] = queue.Detail{
				ID:   xdcc.ID,
				Bot:  xdcc.Bot.Name,
				Pack: xdcc.Pack,
			}
		}

		j.L(ctx).Info("added", zap.Stringer("anime", anime))

		// AutoDownload will always take top-priority. Therefore,
		// we'll push them in the front when it comes to queueing.
		j.Queue().PushFront(anime, queueValue)
	}

	return
}
