package tasks

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/jobs/task"
)

type SimulcastSync struct{ *task.Task }

func (j *SimulcastSync) Run(ctx context.Context) (err error) {
	err = j.Svcs().Simulcast().Refresh(ctx)
	if err != nil {
		return err
	}
	err = j.Svcs().Anime().Associations().AnimeSimulcast().Refresh(ctx)
	if err != nil {
		return err
	}
	return nil
}
