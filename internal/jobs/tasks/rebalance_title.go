package tasks

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

type RebalanceTitles struct{ *task.Task }

func (r *RebalanceTitles) Run(ctx context.Context) error {
	xdccs, err := r.Svcs().Xdcc().Filter(ctx, params.FilterXDCCParams{
		TitleIsDeleted: utils.PointerOf(false),
	})
	if err != nil {
		return err
	}
	if len(xdccs) == 0 {
		return nil
	}
	err = r.Svcs().Simulcast().Clear(ctx)
	if err != nil {
		return err
	}
	for _, xdcc := range xdccs {
		entry := groups.Parse(xdcc.Filename)
		if entry == nil {
			continue
		}
		var (
			titleBase = domain.TitleBase{
				Name:         entry.Title,
				SeasonNumber: entry.SeasonNumber,
				Year:         entry.Year,
			}

			xdccName         = xdcc.TitleEpisode.Title.Name
			xdccSeasonNumber = xdcc.TitleEpisode.Title.SeasonNumber
			xdccYear         = xdcc.TitleEpisode.Title.Year

			// Ready-param for a possible update to this XDCC entry.
			updateXdccParam = params.UpdateXDCCParams{ID: xdcc.ID}

			// Scoped logger
			L = r.L(ctx).With(zap.String("filename", xdcc.Filename))

			differentName = titleBase.Name != xdccName
			differentYear = ((titleBase.Year != nil && xdccYear == nil) || (titleBase.Year == nil && xdccYear != nil) ||
				(xdccYear != nil && titleBase.Year != nil && *titleBase.Year != *xdccYear))
			differentSeasonNumber = ((titleBase.SeasonNumber != nil && xdccSeasonNumber == nil) || (titleBase.SeasonNumber == nil && xdccSeasonNumber != nil) ||
				(xdccSeasonNumber != nil && titleBase.SeasonNumber != nil && *titleBase.SeasonNumber != *xdccSeasonNumber))
		)

		// Given we changed the parsing algorithm, there's a change in the title..
		// We will try to automate the process as much as we can, be swapping out the titles
		// for the title episode.
		if differentName || differentSeasonNumber || differentYear {
			title, err := r.Svcs().Title().Create(ctx, params.InsertTitleParamsItem{
				Name:         titleBase.Name,
				SeasonNumber: titleBase.SeasonNumber,
				Year:         titleBase.Year,
			})
			if err != nil {
				return err
			}
			if title == nil {
				r.L(ctx).Warn(
					"unable to create title",
					zap.String("name", titleBase.Name),
					zap.Stringp("season_number", titleBase.SeasonNumber),
					zap.Intp("year", titleBase.Year),
				)
				continue
			}

			// Does the previous title have any `anime` associations?
			titleAnimes, err := r.Svcs().Title().Associations().TitleAnime().Filter(ctx, params.FilterTitleAnimeParams{
				TitleID: &xdcc.TitleEpisode.TitleID,
			})
			if err != nil {
				return err
			}
			if len(titleAnimes) > 0 {
				// Replace the anime associations
				createTitleAnimeParams := make([]params.InsertTitleAnimeParamsItem, len(titleAnimes))
				for index, titleAnime := range titleAnimes {
					createTitleAnimeParams[index] = params.InsertTitleAnimeParamsItem{
						AnimeID: titleAnime.AnimeID,
						TitleID: title.ID,
					}
				}
				_, err = r.Svcs().Title().Associations().TitleAnime().CreateMany(ctx, params.InsertTitleAnimeParams{
					Items: createTitleAnimeParams,
				})
				if err != nil {
					return err
				}
			}

			titleEpisode, err := r.Svcs().Title().Associations().TitleEpisode().Create(ctx, params.InsertTitleEpisodeParamsItem{
				TitleID:   title.ID,
				EpisodeID: xdcc.TitleEpisode.EpisodeID,
			})
			if err != nil {
				return err
			}

			updateXdccParam.TitleEpisodeID = &titleEpisode.ID

			L.Info(
				"compare",
				zap.Dict("next",
					zap.String("name", titleBase.Name),
					zap.Stringp("season_number", titleBase.SeasonNumber),
					zap.Intp("year", titleBase.Year),
					zap.Int64("title_episode", titleEpisode.ID),
				),
				zap.Dict("previous",
					zap.String("name", xdccName),
					zap.Stringp("season_number", xdccSeasonNumber),
					zap.Intp("year", xdccYear),
					zap.Int64("title_episode", xdcc.TitleEpisodeID),
				),
			)
		}

		// Compare release group names, previous it could have been we masked the name..
		if xdcc.ReleaseGroup.Name != entry.ReleaseGroup {
			releaseGroup, err := r.Svcs().ReleaseGroup().Create(ctx, params.InsertReleaseGroupParamsItem{
				Name: entry.ReleaseGroup,
			})
			if err != nil {
				return err
			}
			updateXdccParam.ReleaseGroupID = &releaseGroup.ID

			L.Info(
				"compare",
				zap.String("previous_release_group", xdcc.ReleaseGroup.Name),
				zap.String("next_release_group", releaseGroup.Name),
			)
		}

		// We might have missed the quality definition previously..
		if (xdcc.QualityID == nil && entry.VideoResolution.Height > 0) || (xdcc.Quality != nil && xdcc.Quality.Height != int64(entry.VideoResolution.Height)) {
			quality, err := r.Svcs().Quality().Create(ctx, params.InsertQualityParamsItem{
				Height: int64(entry.VideoResolution.Height),
			})
			if err != nil {
				return err
			}
			updateXdccParam.QualityID = &quality.ID

			L.Info(
				"compare",
				zap.Int64p("previous_quality_id", xdcc.QualityID),
				zap.Int64("next_quality_id", quality.ID),
			)
		}

		// 0 updates, we'll skip the update execution.
		if updateXdccParam.IsEmpty() {
			continue
		}

		_, err = r.Svcs().Xdcc().Update(ctx, updateXdccParam)
		if err != nil {
			L.Error("failed to update xdcc", zap.Any("param", updateXdccParam))
			return err
		}
	}

	unusedRowsAffected, err := r.Svcs().Title().DeleteUnused(ctx)
	if err != nil {
		return err
	}
	r.L(ctx).Info(
		"remove unused title, title_episode, title_anime",
		zap.Int64("rows_affected", unusedRowsAffected),
	)

	return nil
}
