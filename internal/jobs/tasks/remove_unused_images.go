package tasks

import (
	"context"
	"path"

	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// RemoveUnusedImages is a job which deos the following:
// - retrieves unused poster/banner/thumbnail entries from the databse
//   - if they're not owned by an anime (poster/banner) or a video (thumbnail) they're
//     removed from the database and the file system.
//
// - retrieves all poster/banner images from the file system
//   - if they're not in the database, they're removed from the file system.
type RemoveUnusedImages struct{ *task.Task }

func (j RemoveUnusedImages) unusedDatabase(ctx context.Context) (err error) {
	removeUnused, err := j.Svcs().Image().DeleteUnused(ctx)
	if err != nil {
		return err
	}
	if removeUnused.AnyUnused() {
		j.L(ctx).Info(
			"removed unused",
			zap.Int64("poster", removeUnused.UnusedPoster),
			zap.Int64("banner", removeUnused.UnusedBanner),
			zap.Int64("thumbnail", removeUnused.UnusedThumbnail),
		)
	}
	return nil
}

func (j RemoveUnusedImages) unusedFileSystemPosters(ctx context.Context) (err error) {
	files, err := j.FS().GetPosterImages()
	if err != nil {
		return
	}

	for _, file := range files {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		filename := path.Base(file.Name())

		poster, err := j.Svcs().Image().Poster().Count(ctx, params.CountPosterParams{
			Name: &filename,
		})
		if err != nil {
			return err
		}
		if poster == 0 {
			j.L(ctx).Info("status", zap.Bool("poster", true), zap.String("filename", filename))

			err = j.FS().DeletePosterImage(file.Name())
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (j RemoveUnusedImages) unusedFileSystemBanners(ctx context.Context) (err error) {
	files, err := j.FS().GetBannerImages()
	if err != nil {
		return
	}

	for _, file := range files {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		filename := path.Base(file.Name())

		banner, err := j.Svcs().Image().Banner().Count(ctx, params.CountBannerParams{
			Name: &filename,
		})
		if err != nil {
			return err
		}
		if banner == 0 {
			j.L(ctx).Info("status", zap.Bool("banner", true), zap.String("filename", filename))

			if err = j.FS().DeleteBannerImage(file.Name()); err != nil {
				return err
			}
		}
	}

	return nil
}

func (j RemoveUnusedImages) Run(ctx context.Context) (err error) {
	group, ctx := errgroup.WithContext(ctx)
	group.Go(func() error { return j.unusedDatabase(ctx) })
	group.Go(func() error { return j.unusedFileSystemPosters(ctx) })
	group.Go(func() error { return j.unusedFileSystemBanners(ctx) })
	return group.Wait()
}
