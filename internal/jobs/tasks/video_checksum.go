package tasks

import (
	"context"
	"os"
	"sync"
	"sync/atomic"

	"go.uber.org/zap"

	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// VideoChecksum grabs all the video entries in our database, goes through every
// video file we have storede in our file system and applie a CRC32 checksum
// with what we have stored in our database.
type VideoChecksum struct {
	*task.Task
}

func (j *VideoChecksum) check(ctx context.Context, entries []*domain.VideoEntry) (err error) {
	for _, entry := range entries {
		if entry == nil || (entry.Preferences.PerformChecksum != nil &&
			!*entry.Preferences.PerformChecksum) {
			continue
		}

		path := j.FS().CreatePathAnime(entry.Anime)

		j.L(ctx).Debug(
			"check",
			zap.String("path", path),
			zap.Stringer("anime", entry.Anime),
		)

		fs, err := os.Open(path)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			}

			return err
		}

		hash, err := utils.ChecksumCRC32(fs)
		if err != nil {
			return err
		}

		if err = fs.Close(); err != nil {
			return err
		}

		// We got a mismatch if we enter this condition, we'll proceed by
		// removing the entry along with the file (the services takes care of the
		// filesystem deletion).
		if entry.Video.CRC32 != hash {
			j.L(ctx).Info(
				"remove",
				zap.String("path", path),
				zap.String("file_hash", hash),
				zap.String("database_hash", entry.Video.CRC32),
				zap.String("reason", "checksum_mismatch"),
			)

			rowsAffected, err := j.Svcs().Video().Delete(ctx, params.DeleteVideoParams{
				ID:          entry.Video.ID,
				ThumbnailID: entry.Video.ThumbnailID,
			})
			if err != nil {
				return err
			}

			if rowsAffected == 0 {
				j.L(ctx).Warn(
					"delete `video` unexpected rows affected",
					zap.String("path", path),
				)
			}
		}
	}

	return
}

// Run will execute the VideoChecksum job.
func (j *VideoChecksum) Run(ctx context.Context) (err error) {
	config, err := utils.TryCast[*configmodels.VideoChecksum](j.Config())
	if err != nil {
		return err
	}

	videoEntries, err := j.Svcs().Video().Entries(ctx)
	if err != nil {
		return err
	}

	c, n := uint32(0), float64(videoEntries.Len())

	semaphore := make(chan struct{}, config.Concurrent)

	entriesc := make(chan []*domain.VideoEntry, int(n))

	wg := new(sync.WaitGroup)
	wg.Add(int(n))

	done := make(chan struct{}, 1)

	for _, entries := range videoEntries.Inner() {
		entriesc <- entries
	}

	go func() {
		wg.Wait()
		done <- struct{}{}
	}()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-done:
			return
		case entries := <-entriesc:
			semaphore <- struct{}{}

			go func(entries []*domain.VideoEntry) {
				defer func() {
					<-semaphore
					wg.Done()
				}()

				if len(entries) == 0 {
					return
				}

				percentage := float64(atomic.AddUint32(&c, 1)) / n * 100

				if err := j.check(ctx, entries); err != nil {
					j.L(ctx).Error(
						"check",
						zap.String("entry", entries[0].Anime.Title),
						zap.Float64("percentage", percentage),
						zap.Error(err),
					)
				} else {
					j.L(ctx).Info(
						"check",
						zap.String("entry", entries[0].Anime.Title),
						zap.Float64("percentage", percentage),
					)
				}
			}(entries)
		}
	}
}
