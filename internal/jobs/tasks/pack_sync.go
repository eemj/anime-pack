package tasks

import (
	"context"
	"errors"
	"sync"
	"sync/atomic"

	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// PackSync synchronizes titles and episodes via with the Packlist Controller.
// Any new titles and episodes added / updated to the packlists will be updated
// thanks to this job.
type PackSync struct{ *task.Task }

func (j *PackSync) SyncTitle(ctx context.Context, packs []*domain.GroupPack) (*domain.PacklistSync, error) {
	var (
		entry      = packs[0].Entry
		attributes = []attribute.KeyValue{attribute.String("pack_sync.title", entry.Title)}
	)
	if entry.SeasonNumber != nil && *entry.SeasonNumber != "" {
		attributes = append(attributes, attribute.String("pack_sync.season_number", *entry.SeasonNumber))
	}
	if entry.Year != nil && *entry.Year > 0 {
		attributes = append(attributes, attribute.Int("pack_sync.year", *entry.Year))
	}
	ctx, span := j.Tracer().Start(ctx, (j.Name() + ".SyncTitle"), trace.WithAttributes(attributes...))
	defer span.End()

	title, err := j.Svcs().Title().First(ctx, params.FirstTitleParams{
		Name:         &entry.Title,
		SeasonNumber: entry.SeasonNumber,
		Year:         entry.Year,
	})
	if err != nil {
		return nil, err
	}
	// Early exit, the title is deleted..
	if title != nil && title.IsDeleted {
		return nil, nil
	}

	if title == nil {
		title, _, err = j.Svcs().Library().Match(ctx, params.MatchLibraryParams{
			TitleName:         entry.Title,
			TitleSeasonNumber: entry.SeasonNumber,
			TitleYear:         entry.Year,
		})
		if err != nil {
			return nil, err
		}
	}

	return j.Svcs().Packlist().CreateForTitle(ctx, params.CreateForTitlePacklist{
		TitleID: title.ID,
		Packs:   packs,
	})
}

func (j *PackSync) Run(ctx context.Context) (err error) {
	config, err := utils.TryCast[*configmodels.PackSync](j.Config())
	if err != nil {
		return err
	}

	if config.Concurrent <= 0 {
		return errors.New("`concurrent` must be greater than 0")
	}

	j.L(ctx).Info("busting packs cache")

	if err := j.Feed().Packlist().BustPacks(ctx); err != nil {
		return err
	}

	j.L(ctx).Info("retrieving packs")

	packItems, err := j.Feed().Packlist().GetPacks(ctx)
	if err != nil {
		return err
	}

	j.L(ctx).Info("checking for dead packs")

	reasons, err := j.Svcs().Packlist().RemoveDeadPacks(ctx, params.RemoveDeadPacksPacklist{
		Items: packItems,
	})
	if err != nil {
		return err
	}

	for _, reason := range reasons {
		j.L(ctx).Info(
			"removed dead pack",
			zap.String("filename", reason.XDCC.Filename),
			zap.String("bot", reason.XDCC.Bot.Name),
			zap.Stringer("reason", reason.Reason),
		)
	}

	packs, packsErr := j.Feed().Packlist().GroupPacks(ctx, packItems)

	var (
		currentPercentage atomic.Uint32
		length            = len(packs)
	)

	j.L(ctx).Info("enqueueing packs", zap.Int("length", length))

	packsc := make(chan []*domain.GroupPack, length)

	wg := new(sync.WaitGroup)
	wg.Add(length)

	done := make(chan struct{}, 1)

	for _, pack := range packs {
		packsc <- pack
	}

	go func() {
		wg.Wait()
		done <- struct{}{}
	}()

	semaphore := make(chan struct{}, config.Concurrent)

	for {
		select {
		case <-ctx.Done():
			return packsErr
		case <-done:
			return packsErr
		case packs := <-packsc:
			semaphore <- struct{}{}

			go func(packs []*domain.GroupPack) {
				defer func() {
					<-semaphore
					wg.Done()
				}()

				var (
					percentage  = float64(currentPercentage.Add(1)) / float64(length) * 100
					synced, err = j.SyncTitle(ctx, packs)
					titleBase   = domain.TitleBase{
						Name:         packs[0].Entry.Title,
						SeasonNumber: packs[0].Entry.SeasonNumber,
						Year:         packs[0].Entry.Year,
					}
					fields = []zap.Field{
						zap.Stringer("title", titleBase),
						zap.Float64("percentage", percentage),
					}
					loggable = (synced != nil && !synced.IsEmpty()) || percentage == 100
				)

				if err != nil {
					fields = append(fields, zap.Error(err))

					j.L(ctx).Error("sync", fields...)
				} else if loggable {
					if synced != nil {
						fields = append(
							fields,
							zap.Int("retrieved", synced.Retrieved),
							zap.Int("created", synced.Created),
							zap.Int("updated", synced.Updated),
							zap.Int("deleted", synced.Deleted),
						)
					}

					j.L(ctx).Info("sync", fields...)
				}
			}(packs)
		}
	}
}
