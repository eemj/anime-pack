package tasks

import (
	"context"
	"os"
	"path/filepath"
	"slices"

	"github.com/dustin/go-humanize"
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

type entryResult struct {
	FS    bool
	DB    bool
	Entry *domain.VideoEntry
}

// VideoSync is responsible for deleting video entries for the following scenarios:
// - Parent path removal.
// - File is present in the file system but not in the database.
// - File is present in the database but not in the file system.
type VideoSync struct {
	*task.Task
}

// Run will execute the VideoSync job.
func (j *VideoSync) Run(ctx context.Context) (err error) {
	config, err := utils.TryCast[*configmodels.VideoSync](j.Config())
	if err != nil {
		return err
	}

	videoEntries, err := j.Svcs().Video().Entries(ctx)
	if err != nil {
		return
	}
	innerVideoEntries := videoEntries.Inner()

	for _, entries := range innerVideoEntries {
		var (
			anime = entries[0].Anime
			title = domain.EscapeTitle(anime.Title)
			path  = filepath.Join(j.FS().GetDownloadsPath(), title)
		)

		// Parent path for all videos
		_, err := os.Stat(path)
		if err != nil {
			if os.IsNotExist(err) {
				j.L(ctx).Info(
					"remove",
					zap.String("path", path),
					zap.String("reason", "not_in_file_system"),
					zap.Int("entries", len(entries)),
				)

				if !config.DryRun {
					for _, entry := range entries {
						_, err := j.Svcs().Video().Delete(ctx, params.DeleteVideoParams{
							ID:          entry.Video.ID,
							ThumbnailID: entry.Video.ThumbnailID,
						})
						if err != nil {
							return err
						}
					}
				}
				continue
			} else {
				j.L(ctx).Error(
					"unable to stat video file",
					zap.String("title", title),
					zap.String("path", path),
					zap.Error(err),
				)
			}
		}

		files, err := os.ReadDir(path)
		if err != nil {
			return err
		}
		if len(files) == 0 {
			continue
		}

		paths := make(map[string]struct{})
		{
			for _, file := range files {
				paths[filepath.Join(path, file.Name())] = struct{}{}
			}

			for _, entry := range entries {
				paths[entry.VideoPath] = struct{}{}
			}
		}

		pathResults := make(map[string]*entryResult)

		// Per-video check
		for path := range paths {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
			}

			stat, err := os.Stat(path)

			result := &entryResult{FS: err == nil}

			if result.FS && j.L(ctx).Level().Enabled(zap.DebugLevel) {
				videoHumanSize := humanize.Bytes(uint64(stat.Size()))
				videoModifiedAt := stat.ModTime()

				j.L(ctx).Debug(
					"video stat information",
					zap.String("title", title),
					zap.String("path", path),
					zap.Int64("size", stat.Size()),
					zap.String("human_size", videoHumanSize),
					zap.Time("modified_at", videoModifiedAt),
				)
			}

			entryIndex := slices.IndexFunc(entries, func(entry *domain.VideoEntry) bool {
				return entry.VideoPath == path
			})
			result.DB = entryIndex > -1
			if result.DB {
				result.Entry = entries[entryIndex]
			}

			pathResults[path] = result
		}

		for path, result := range pathResults {
			select {
			case <-ctx.Done():
				return ctx.Err()
			default:
			}

			if !result.FS && result.Entry != nil {
				j.L(ctx).Info(
					"remove",
					zap.String("path", path),
					zap.String("reason", "not_in_filesystem"),
					zap.Int("entries", 1),
				)

				if !config.DryRun {
					_, err := j.Svcs().Video().Delete(ctx, params.DeleteVideoParams{
						ID:          result.Entry.Video.ID,
						ThumbnailID: result.Entry.Video.ThumbnailID,
					})
					if err != nil {
						return err
					}
				}
			}

			if !result.DB {
				j.L(ctx).Info(
					"remove",
					zap.String("path", path),
					zap.String("reason", "not_in_database"),
					zap.Int("entries", 1),
				)

				if !config.DryRun {
					err := os.Remove(path)
					if err != nil {
						return err
					}
				}
			}
		}
	}

	return nil
}
