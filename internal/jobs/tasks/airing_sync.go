package tasks

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/jobs/task"
)

type AiringSync struct{ *task.Task }

func (j *AiringSync) Run(ctx context.Context) (err error) {
	return j.Svcs().Anime().UpdateReleasing(ctx)
}
