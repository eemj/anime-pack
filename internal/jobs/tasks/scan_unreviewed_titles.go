package tasks

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

type ScanUnreviewedTitles struct{ *task.Task }

func (r *ScanUnreviewedTitles) Run(ctx context.Context) error {
	unreviewedTitles, err := r.Svcs().Title().Filter(ctx, params.FilterTitleParams{
		IsDeleted: utils.PointerOf(false),
		Reviewed:  utils.PointerOf(false),
	})
	if err != nil {
		return err
	}
	for _, unreviewedTitle := range unreviewedTitles {
		anime, reviewed, err := r.Svcs().Anime().CreateFromTitle(ctx, *unreviewedTitle)
		if err != nil {
			r.L(ctx).Error( // this
				"unable to create an anime for title base",
				zap.String("name", unreviewedTitle.Name),
				zap.Stringp("season_number", unreviewedTitle.SeasonNumber),
				zap.Intp("year", unreviewedTitle.Year),
				zap.Error(err),
			)
			continue
		}
		titleBase := domain.TitleBase{
			Name:         unreviewedTitle.Name,
			SeasonNumber: unreviewedTitle.SeasonNumber,
			Year:         unreviewedTitle.Year,
		}
		r.L(ctx).Info(
			"scanned title",
			zap.Stringer("title", titleBase),
			zap.Bool("reviewed", reviewed),
			zap.Int("anime_length", len(anime)),
		)
		unreviewedTitle.Reviewed = reviewed

		_, err = r.Svcs().Title().LinkWithAnime(ctx, unreviewedTitle, anime)
		if err != nil {
			return err
		}

		_, err = r.Svcs().Title().Update(ctx, params.UpdateTitleParams{
			ID:       unreviewedTitle.ID,
			Reviewed: &reviewed,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
