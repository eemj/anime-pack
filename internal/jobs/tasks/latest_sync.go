package tasks

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"go.uber.org/zap"
)

type LatestSync struct {
	*task.Task
}

func (j *LatestSync) Run(ctx context.Context) (err error) {
	animes := make([]*domain.AnimeLatest, 0)

	for page := int64(1); ; page++ {
		j.L(ctx).Debug("fetching", zap.Int64("page", page), zap.Int64("elements", 100))

		paginatedAnime, err := j.Svcs().Anime().Associations().AnimeLatest().Filter(ctx, params.FilterAnimeLatestParams{
			Statuses: []enums.AnimeStatus{enums.AnimeStatusRELEASING},
			Page:     page,
			Elements: 100,
		})
		if err != nil {
			return err
		}
		if len(paginatedAnime.Data) == 0 {
			break
		}
		animes = append(animes, paginatedAnime.Data...)
	}

	titleSet := make(map[domain.TitleBaseKey]*domain.Title)
	// try to match `airing` anime with title, basically we'll query the `title_anime` table
	// and get any title matches with the requested anime.
	for _, anime := range animes {
		if err = ctx.Err(); err != nil {
			return err
		}

		titleAnimes, err := j.Svcs().Title().Associations().TitleAnime().Filter(ctx, params.FilterTitleAnimeParams{
			AnimeID: &anime.ID,
		})
		// We didn't find any matching titles..
		// this should never happen as Anime entries are created according their associated title.
		if err != nil {
			return err
		}
		if len(titleAnimes) == 0 {
			j.L(ctx).Warn("no titles for anime found", zap.Int64("anime_id", anime.ID))
			continue
		}

		for _, titleAnime := range titleAnimes {
			titleBase := domain.NewTitleBaseKey(
				titleAnime.Title.Name,
				titleAnime.Title.SeasonNumber,
				titleAnime.Title.Year,
			)
			titleSet[titleBase] = &titleAnime.Title
		}
	}

	packItems, err := j.Feed().Packlist().GetPacks(ctx)
	if err != nil {
		return err
	}

	// Don't return if an exception is encountered during the packlist retrieval.
	groupPacks, groupPackErr := j.Feed().Packlist().GroupPacks(ctx, packItems)

	for titleBase, title := range titleSet {
		if err = ctx.Err(); err != nil {
			return err
		}

		packs, exists := groupPacks[titleBase]
		if !exists {
			continue
		}

		synced, err := j.Svcs().Packlist().CreateForTitle(ctx, params.CreateForTitlePacklist{
			TitleID: title.ID,
			Packs:   packs,
		})
		if err != nil {
			return err
		}
		if synced == nil || synced.IsEmpty() {
			continue
		}
		j.L(ctx).Info(
			"update",
			zap.String("title", titleBase.Name),
			zap.Int("retrieved", synced.Retrieved),
			zap.Int("created", synced.Created),
			zap.Int("updated", synced.Updated),
			zap.Int("deleted", synced.Deleted),
		)
	}

	return groupPackErr
}
