package jobs

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/feed"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/jobs/manager"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
	"gitlab.com/eemj/anime-pack/internal/jobs/tasks"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type Jobs interface {
	Info() []task.Info

	Get(id uuid.UUID) (taskInfo task.Info, err error)

	Start(ctx context.Context, id uuid.UUID) (err error)
	Stop(ctx context.Context, id uuid.UUID) (err error)

	Enable(id uuid.UUID) (err error)
	Disable(id uuid.UUID) (err error)

	StartAll(ctx context.Context) (err error)

	Close()
}

type jobs struct {
	factory task.TaskFactory
	manager manager.Manager
}

func (j *jobs) Close()                                        { j.manager.Close() }
func (j *jobs) Start(ctx context.Context, id uuid.UUID) error { return j.manager.Run(ctx, id) }
func (j *jobs) Stop(ctx context.Context, id uuid.UUID) error  { return j.manager.Stop(ctx, id) }
func (j *jobs) Enable(id uuid.UUID) error                     { return j.manager.Enable(id) }
func (j *jobs) Disable(id uuid.UUID) error                    { return j.manager.Disable(id) }
func (j *jobs) StartAll(ctx context.Context) error            { return j.manager.StartAll(ctx) }
func (j *jobs) Info() []task.Info                             { return j.manager.Info() }
func (j *jobs) Get(id uuid.UUID) (task.Info, error)           { return j.manager.Get(id) }

func (j *jobs) L() *zap.Logger { return log.Named("jobs") }

func (j *jobs) processErrors() {
	for err := range j.manager.Errors() {
		if taskError, ok := err.(*manager.TaskError); ok {
			j.L().
				Named(taskError.Name).
				With(zap.Stringer("id", taskError.ID)).
				Error("error", zap.Error(taskError.Err))
		} else {
			j.L().Error("error", zap.Error(err))
		}
	}
}

func New(
	hub websocket.Hub,
	svcs lifecycle.ServiceLifecycle,
	queue *queue.Queue,
	fs *fs.FileSystem,
	feed feed.Feed,
	cfg configmodels.Jobs,
) Jobs {
	j := &jobs{
		factory: task.NewFactory(svcs, queue, fs, feed),
		manager: manager.New(hub),
	}

	go j.processErrors()

	j.manager.Register(
		&tasks.AiringSync{Task: j.factory.Create("airing_sync", &cfg.AiringSync)},
		manager.Settings{
			Disable: cfg.AiringSync.Disable,
			Manual:  cfg.AiringSync.Manual,
			Period:  cfg.AiringSync.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.AutoDownload{Task: j.factory.Create("auto_download", &cfg.AutoDownload)},
		manager.Settings{
			Disable: cfg.AutoDownload.Disable,
			Manual:  cfg.AutoDownload.Manual,
			Period:  cfg.AutoDownload.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.LatestSync{Task: j.factory.Create("latest_sync", &cfg.LatestSync)},
		manager.Settings{
			Disable: cfg.LatestSync.Disable,
			Manual:  cfg.LatestSync.Manual,
			Period:  cfg.LatestSync.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.PackSync{Task: j.factory.Create("pack_sync", &cfg.PackSync)},
		manager.Settings{
			Disable: cfg.PackSync.Disable,
			Manual:  cfg.PackSync.Manual,
			Period:  cfg.PackSync.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.RemoveUnusedImages{Task: j.factory.Create("remove_unused_images", &cfg.RemoveUnusedImages)},
		manager.Settings{
			Disable: cfg.RemoveUnusedImages.Disable,
			Manual:  cfg.RemoveUnusedImages.Manual,
			Period:  cfg.RemoveUnusedImages.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.VideoChecksum{Task: j.factory.Create("video_checksum", &cfg.VideoChecksum)},
		manager.Settings{
			Disable: cfg.VideoChecksum.Disable,
			Manual:  cfg.VideoChecksum.Manual,
			Period:  cfg.VideoChecksum.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.VideoSync{Task: j.factory.Create("video_sync", &cfg.VideoSync)},
		manager.Settings{
			Disable: cfg.VideoSync.Disable,
			Manual:  cfg.VideoSync.Manual,
			Period:  cfg.VideoSync.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.SimulcastSync{Task: j.factory.Create("simulcast_sync", &cfg.SimulcastSync)},
		manager.Settings{
			Disable: cfg.SimulcastSync.Disable,
			Manual:  cfg.SimulcastSync.Manual,
			Period:  cfg.SimulcastSync.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.RebalanceTitles{Task: j.factory.Create("rebalance_titles", &cfg.RebalanceTitle)},
		manager.Settings{
			Disable: cfg.RebalanceTitle.Disable,
			Manual:  cfg.RebalanceTitle.Manual,
			Period:  cfg.RebalanceTitle.Period.Duration(),
		},
	)

	j.manager.Register(
		&tasks.ScanUnreviewedTitles{Task: j.factory.Create("scan_unreviewed_titles", &cfg.ScanUnreviewedTitles)},
		manager.Settings{
			Disable: cfg.ScanUnreviewedTitles.Disable,
			Manual:  cfg.ScanUnreviewedTitles.Manual,
			Period:  cfg.ScanUnreviewedTitles.Period.Duration(),
		},
	)

	return j
}
