package video

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/common/maps"
	"gitlab.com/eemj/anime-pack/internal/domain"
)

func (s *videoService) Entries(ctx context.Context) (domain.VideoEntries, error) {
	rows, err := s.querier.ListVideoEntries(ctx)
	if err != nil {
		if err == pgx.ErrNoRows {
			return domain.ZeroVideoEntries, nil
		}
		return nil, err
	}
	flatItems := make(domain.VideoEntryFlat, len(rows))
	for index, row := range rows {
		flatItems[index] = domain.VideoEntryFlatItem(*row)
	}
	result := maps.NewGroupMap[string, *domain.VideoEntry]()
	for _, flatItem := range flatItems {
		newItem := flatItem.Unflatten()
		newItem.VideoPath = s.fs.CreatePathAnime(newItem.Anime)
		result.Add(newItem.Anime.Title, newItem)
	}
	return result, nil
}
