package video

import (
	"gitlab.com/eemj/anime-pack/internal/image"
	"gitlab.com/eemj/anime-pack/internal/library"
	"gitlab.com/eemj/anime-pack/internal/preferences"
	"gitlab.com/eemj/anime-pack/internal/quality"
	titleanime "gitlab.com/eemj/anime-pack/internal/title/title_anime"
	"gitlab.com/eemj/anime-pack/internal/xdcc"
)

type VideoAssociations interface {
	TitleAnime() titleanime.TitleAnimeService
	Thumbnail() image.ThumbnailService
	Quality() quality.QualityService
	XDCC() xdcc.XDCCService
	Preferences() preferences.PreferencesService
	Library() library.LibraryService
}

type videoAssociations struct {
	titleAnime  titleanime.TitleAnimeService
	thumbnail   image.ThumbnailService
	xdcc        xdcc.XDCCService
	quality     quality.QualityService
	preferences preferences.PreferencesService
	library     library.LibraryService
}

func (s *videoAssociations) Preferences() preferences.PreferencesService { return s.preferences }
func (s *videoAssociations) Quality() quality.QualityService             { return s.quality }
func (s *videoAssociations) Thumbnail() image.ThumbnailService           { return s.thumbnail }
func (s *videoAssociations) TitleAnime() titleanime.TitleAnimeService    { return s.titleAnime }
func (s *videoAssociations) XDCC() xdcc.XDCCService                      { return s.xdcc }
func (s *videoAssociations) Library() library.LibraryService             { return s.library }

func NewVideoAssociations(
	titleAnime titleanime.TitleAnimeService,
	thumbnail image.ThumbnailService,
	xdcc xdcc.XDCCService,
	quality quality.QualityService,
	preferences preferences.PreferencesService,
	library library.LibraryService,
) VideoAssociations {
	return &videoAssociations{
		titleAnime:  titleAnime,
		thumbnail:   thumbnail,
		xdcc:        xdcc,
		quality:     quality,
		preferences: preferences,
		library:     library,
	}
}
