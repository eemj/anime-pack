package video

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/google/uuid"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/operation"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.uber.org/zap"
)

type VideoService interface {
	Create(ctx context.Context, arg params.InsertVideoParams) (*domain.Video, error)
	First(ctx context.Context, arg params.FirstVideoParams) (*domain.Video, error)
	Filter(ctx context.Context, arg params.FilterVideoParams) ([]*domain.Video, error)
	Count(ctx context.Context, arg params.CountVideoParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateVideoParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteVideoParams) (int64, error)

	Location(ctx context.Context, arg params.LocateVideoParams) (path string, err error)
	Entries(ctx context.Context) (videoEntries domain.VideoEntries, err error)

	Associations() VideoAssociations
}

type videoService struct {
	querier      database.QuerierExtended
	cacher       cacher.Cacher
	fs           *fs.FileSystem
	associations VideoAssociations
}

// Delete implements VideoService.
func (s *videoService) Delete(ctx context.Context, arg params.DeleteVideoParams) (int64, error) {
	libraryVideos, err := s.associations.Library().Filter(ctx, params.FilterLibraryParams{
		VideoID: &arg.ID,
	})
	if err != nil {
		return 0, err
	}
	var libraryVideo *domain.LibraryVideo
	if len(libraryVideos) > 0 {
		libraryVideo = libraryVideos[0]
	}
	if libraryVideo == nil {
		return 0, fmt.Errorf("no library item found for '%+v'", arg)
	}

	rowsAffected, err := s.querier.DeleteVideo(ctx, database.DeleteVideoParams{
		ID: arg.ID,
	})
	if err != nil {
		return 0, err
	}
	if rowsAffected == 0 {
		s.L(ctx).Warn(
			"delete `video` rows affected",
			zap.Int64("rows_affected", rowsAffected),
			zap.Int64("id", arg.ID),
		)
	}

	if arg.ThumbnailID > 0 {
		thumbnailCount, err := s.querier.CountVideo(ctx, database.CountVideoParams{
			ThumbnailID: &arg.ThumbnailID,
		})
		if err != nil {
			return 0, err
		}
		if thumbnailCount == 0 {
			rowsAffected, err := s.associations.Thumbnail().Delete(ctx, params.DeleteThumbnailParams{
				ID: arg.ThumbnailID,
			})
			if err != nil {
				return 0, err
			}
			if rowsAffected == 0 {
				s.L(ctx).Warn(
					"delete `thumbnail` rows affected",
					zap.Int64("rows_affected", rowsAffected),
					zap.Int64("id", arg.ThumbnailID),
				)
			}
		}
	}

	animeBase := domain.AnimeBase{
		ID:           libraryVideo.AnimeID,
		Title:        libraryVideo.AnimeTitle,
		Height:       int(libraryVideo.QualityHeight),
		Episode:      libraryVideo.EpisodeName,
		ReleaseGroup: libraryVideo.ReleaseGroupName,
	}

	s.L(ctx).Debug("delete video", zap.Stringer("anime", animeBase))

	err = s.fs.DeleteAnime(animeBase)
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}

func (s *videoService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.VideoServiceName)
}

// Count implements VideoService.
func (s *videoService) Count(ctx context.Context, arg params.CountVideoParams) (int64, error) {
	return s.querier.CountVideo(ctx, database.CountVideoParams(arg))
}

// Create implements VideoService.
func (s *videoService) Create(ctx context.Context, arg params.InsertVideoParams) (video *domain.Video, err error) {
	if arg.ThumbnailID == 0 || arg.XDCCID == 0 {
		return nil, errors.New("`ThumbnailID` and `XDCCID` are required")
	}

	duration := arg.Duration
	if duration == 0 {
		duration, err = operation.GetDuration(ctx, arg.Path)
		if err != nil {
			return nil, err
		}
	}

	var (
		qualityID *int64
		quality   *domain.Quality
	)
	if arg.QualityHeight != nil && *arg.QualityHeight > 0 {
		quality, err = s.associations.Quality().Create(ctx, params.InsertQualityParamsItem{
			Height: *arg.QualityHeight,
		})
		if err != nil {
			return nil, err
		}
		if quality == nil {
			s.L(ctx).Warn("unexpected `qualities` row", zap.Int64p("height", arg.QualityHeight))
		} else {
			qualityID = &quality.ID
		}
	}

	var (
		crc32 = arg.CRC32
		size  = arg.Size
	)
	if crc32 == "" || size == 0 {
		fs, err := os.OpenFile(arg.Path, os.O_RDONLY, os.ModePerm)
		if err != nil {
			return nil, err
		}
		defer fs.Close()

		if size == 0 {
			stat, err := fs.Stat()
			if err != nil {
				return nil, err
			}
			size = stat.Size()
		}
		if crc32 == "" {
			crc32, err = utils.ChecksumCRC32(fs)
			if err != nil {
				return nil, err
			}
		}
	}

	// Previously this used to be `utils.NewIdentifier()`, let's stop using it for videos.
	path := uuid.NewString()
	row, err := s.querier.InsertVideo(ctx, database.InsertVideoParams{
		Path:        path,
		ThumbnailID: arg.ThumbnailID,
		XDCCID:      arg.XDCCID,
		Duration:    duration,
		QualityID:   qualityID,
		CRC32:       crc32,
		Size:        size,
	})
	if err != nil {
		return nil, err
	}
	return &domain.Video{
		ID:          row.ID,
		Path:        row.Path,
		ThumbnailID: row.ThumbnailID,
		XDCCID:      row.XDCCID,
		Duration:    row.Duration,
		QualityID:   row.QualityID,
		Quality:     quality,
		CRC32:       row.CRC32,
		Size:        row.Size,
		CreatedAt:   row.CreatedAt,
	}, nil
}

// Filter implements VideoService.
func (*videoService) Filter(ctx context.Context, arg params.FilterVideoParams) ([]*domain.Video, error) {
	panic("unimplemented")
}

// First implements VideoService.
func (*videoService) First(ctx context.Context, arg params.FirstVideoParams) (*domain.Video, error) {
	panic("unimplemented")
}

// Update implements VideoService.
func (*videoService) Update(ctx context.Context, arg params.UpdateVideoParams) (int64, error) {
	panic("unimplemented")
}

func (s *videoService) Location(ctx context.Context, arg params.LocateVideoParams) (string, error) {
	items, err := s.associations.Library().Filter(ctx, params.FilterLibraryParams{
		VideoPath: &arg.Path,
	})
	if err != nil {
		return "", err
	}
	if len(items) == 0 {
		return "", errors.New("video not found")
	}
	item := items[0]
	// TODO(eemj): Stop using domain.AnimeBase and use domain.AnimeBase
	path := s.fs.CreatePathAnime(domain.AnimeBase{
		ID:           item.AnimeID,
		Title:        item.AnimeTitle,
		Episode:      item.EpisodeName,
		Height:       int(item.QualityHeight),
		ReleaseGroup: item.ReleaseGroupName,
	})
	s.L(ctx).Debug("path generated", zap.String("path", path))
	return path, nil
}

func (s *videoService) Associations() VideoAssociations { return s.associations }

func NewVideoService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	fs *fs.FileSystem,
	associations VideoAssociations,
) VideoService {
	return &videoService{
		querier:      querier,
		cacher:       cacher,
		fs:           fs,
		associations: associations,
	}
}
