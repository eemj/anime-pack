package library

import (
	"context"
	"sort"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

func (s *libraryService) BestQualityPerReleaseGroup(
	ctx context.Context,
	arg params.BestQualityPerReleaseGroupParams,
) (domain.LibraryBestQualitiesPerReleaseGroup, error) {
	rows, err := s.querier.BestAnimeQualityPerReleaseGroup(ctx, database.BestAnimeQualityPerReleaseGroupParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.LibraryBestQualityPerReleaseGroup{}, nil
		}
		return nil, err
	}
	items := make([]*domain.LibraryBestQualityPerReleaseGroup, len(rows))
	for index, row := range rows {
		var quality *domain.Quality
		if row.QualityID != nil && row.QualityHeight != nil {
			quality = &domain.Quality{
				ID:     *row.QualityID,
				Height: *row.QualityHeight,
			}
		}
		items[index] = &domain.LibraryBestQualityPerReleaseGroup{
			Quality: quality,
			ReleaseGroup: domain.ReleaseGroup{
				ID:   row.ReleaseGroupID,
				Name: row.ReleaseGroupName,
			},
		}
	}
	sort.Sort(domain.LibraryBestQualitiesPerReleaseGroupSortByReleaseGroup(items))
	return items, nil
}
