package library

import (
	"gitlab.com/eemj/anime-pack/internal/anime"
	"gitlab.com/eemj/anime-pack/internal/title"
	"gitlab.com/eemj/anime-pack/internal/xdcc"
)

type LibraryAssociations interface {
	XDCC() xdcc.XDCCService
	Title() title.TitleService
	Anime() anime.AnimeService
}

type libraryAssociations struct {
	xdcc  xdcc.XDCCService
	title title.TitleService
	anime anime.AnimeService
}

func (a *libraryAssociations) XDCC() xdcc.XDCCService    { return a.xdcc }
func (a *libraryAssociations) Title() title.TitleService { return a.title }
func (a *libraryAssociations) Anime() anime.AnimeService { return a.anime }

func NewLibraryAssociations(
	xdcc xdcc.XDCCService,
	title title.TitleService,
	anime anime.AnimeService,
) LibraryAssociations {
	return &libraryAssociations{
		xdcc:  xdcc,
		title: title,
		anime: anime,
	}
}
