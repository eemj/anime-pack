package library

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type LibraryService interface {
	Filter(ctx context.Context, arg params.FilterLibraryParams) ([]*domain.LibraryVideo, error)
	Import(ctx context.Context, arg params.ImportLibraryParams) error
	Export(ctx context.Context) ([]*domain.LibraryItem, error)
	Match(ctx context.Context, arg params.MatchLibraryParams) (*domain.Title, []*domain.TitleAnime, error)
	BestQualityPerReleaseGroup(ctx context.Context, arg params.BestQualityPerReleaseGroupParams) (domain.LibraryBestQualitiesPerReleaseGroup, error)
}

type libraryService struct {
	querier      database.QuerierExtended
	cacher       *encoded.EncodedCacher
	associations LibraryAssociations
}

func (s *libraryService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.LibraryServiceName)
}

// Export implements LibraryService.
func (s *libraryService) Export(ctx context.Context) ([]*domain.LibraryItem, error) {
	rows, err := s.querier.ListReviewedLibrary(ctx)
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.LibraryItem{}, nil
		}
		return nil, err
	}
	flatItems := make(domain.ListReviewedLibraryItemsFlat, 0)
	for _, row := range rows {
		flatItems = append(flatItems, domain.ListReviewedLibraryItemFlat(*row))
	}
	items := flatItems.Unflatten()
	result := make([]*domain.LibraryItem, 0, len(items))
	for _, item := range items {
		if length := len(item.AnimeID); length > 1 || length == 0 {
			s.L(ctx).Warn(
				"reviewed title has 0 or more than 1 anime",
				zap.Int64("title_id", item.Title.ID),
				zap.Int64s("anime_ids", item.AnimeID),
			)
			continue
		}

		result = append(result, &domain.LibraryItem{
			TitleName:         item.Title.Name,
			TitleSeasonNumber: item.Title.SeasonNumber,
			TitleYear:         item.Title.Year,
			DeletedAt:         item.Title.DeletedAt,
			AnimeID:           &item.AnimeID[0],
		})
	}

	return result, nil
}

// Filter implements LibraryService.
func (s *libraryService) Filter(ctx context.Context, arg params.FilterLibraryParams) ([]*domain.LibraryVideo, error) {
	rows, err := s.querier.FilterLibrary(ctx, database.FilterLibraryParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.LibraryVideo{}, nil
		}
		return nil, err
	}
	result := make([]*domain.LibraryVideo, len(rows))
	for index, row := range rows {
		result[index] = &domain.LibraryVideo{
			AnimeID:          row.AnimeID,
			TitleEpisodeID:   row.TitleEpisodeID,
			XDCCID:           row.XDCCID,
			ReleaseGroupName: row.ReleaseGroupName,
			QualityHeight:    row.QualityHeight,
			EpisodeName:      row.EpisodeName,
			AnimeTitle:       row.AnimeTitle,
		}
	}
	return result, nil
}

// Import implements LibraryService.
func (s *libraryService) Import(ctx context.Context, arg params.ImportLibraryParams) error {
	err := arg.Validate()
	if err != nil {
		return err
	}

	title, err := s.associations.Title().First(ctx, params.FirstTitleParams{
		Name:         &arg.TitleName,
		SeasonNumber: arg.TitleSeasonNumber,
		Year:         arg.TitleYear,
	})
	if err != nil {
		return err
	}
	if title == nil {
		// TODO(eemj): Ideally we return a better error
		return apperror.ErrRecordNotFound
	}

	// Import directed us that we should delete this title.
	if arg.DeletedAt != nil && !arg.DeletedAt.IsZero() {
		rowsAffected, err := s.associations.Title().SoftDelete(ctx, params.SoftDeleteTitleParams{
			IDs: []int64{title.ID},
		})
		if err != nil {
			return err
		}
		if rowsAffected == 0 {
			s.L(ctx).Warn("unexpected `title` delete rows affected", zap.Int("rows_affected", int(rowsAffected)))
		}
		return nil
	}

	// Import directed that this title is reviewed, so let's import it.
	if !title.Reviewed {
		s.L(ctx).Debug(
			"reviewing title",
			zap.String("name", arg.TitleName),
			zap.Stringp("season_number", arg.TitleSeasonNumber),
			zap.Intp("year", arg.TitleYear),
			zap.Int64p("id", arg.AnimeID),
		)

		_, err := s.associations.Title().Review(ctx, params.ReviewTitleParams{
			ID:        title.ID,
			Reviewed:  true,
			AniListID: arg.AnimeID,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func NewLibraryService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations LibraryAssociations,
) LibraryService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &libraryService{
		querier:      querier,
		cacher:       encodedCacher,
		associations: associations,
	}
}
