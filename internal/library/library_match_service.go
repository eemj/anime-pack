package library

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

func (s *libraryService) Match(ctx context.Context, arg params.MatchLibraryParams) (*domain.Title, []*domain.TitleAnime, error) {
	title, err := s.associations.Title().Create(ctx, params.InsertTitleParamsItem{
		Name:         arg.TitleName,
		SeasonNumber: arg.TitleSeasonNumber,
		Year:         arg.TitleYear,
	})
	if err != nil {
		return nil, nil, err
	}
	// This shouldn't really be the case because we're creating
	if title == nil {
		return nil, nil, apperror.ErrTitleNotFoundWith(
			arg.TitleName,
			arg.TitleSeasonNumber,
			arg.TitleYear,
		)
	}

	// The title won't be reviewed if it's just created, however
	// the Title().CreateMany() will return an existing title
	// if it matches the requested parameters.
	var titleAnimes []*domain.TitleAnime
	if !title.Reviewed {
		anime, reviewed, err := s.associations.Anime().CreateFromTitle(ctx, *title)
		if err != nil {
			return nil, nil, err
		}
		title.Reviewed = reviewed

		titleAnimes, err = s.associations.Title().LinkWithAnime(ctx, title, anime)
		if err != nil {
			return nil, nil, err
		}
	} else {
		titleAnimes, err = s.associations.Title().Associations().TitleAnime().Filter(ctx, params.FilterTitleAnimeParams{
			TitleID: &title.ID,
		})
		if err != nil {
			return nil, nil, err
		}
	}

	// We need to return titleAnimes because this methods'
	// callers generally requires the title + anime details.
	return title, titleAnimes, nil
}
