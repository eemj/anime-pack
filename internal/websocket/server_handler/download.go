package handler

import (
	"fmt"
	"sort"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/queue"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

func (h *ServerHandler) download(
	session websocket.Session,
	data *protocol.DataActivityDownload,
) (err error) {
	if data.Len() == 0 {
		return
	}

	// Sort by the priority, true first.
	sort.Sort(*data)

	// Differences
	differences := make(protocol.DataActivityDifferences, 0)

	for _, item := range *data {
		titleEpisode, err := h.services.Title().Associations().TitleEpisode().First(session.Context(), params.FirstTitleEpisodeParams{
			ID: &item.ID,
		})
		if err != nil {
			return nil
		}
		// Double-check that the record exists in our database
		if titleEpisode == nil {
			return errEntryDoesNotExist(
				item.ID,
				item.QualityID,
				item.ReleaseGroupID,
			)
		}

		// Ensure that the title is reviewed
		if !titleEpisode.Title.Reviewed {
			return errTitleIsNotReviewed
		}

		xdccs, err := h.services.Xdcc().Filter(session.Context(), params.FilterXDCCParams{
			TitleEpisodeID: &item.ID,
			ReleaseGroupID: &item.ReleaseGroupID,
			QualityID:      item.QualityID,
			WithQualityID:  true,
		})
		if err != nil {
			return err
		}
		if len(xdccs) == 0 {
			return errXdccEntriesNotFound
		}

		// GetTitle so we'll get the associated Anime from the title to form the common.
		titleAnime, err := h.services.Title().Associations().TitleAnime().First(session.Context(), params.FirstTitleAnimeParams{
			TitleID: &titleEpisode.TitleID,
		})
		if err != nil {
			return err
		}
		// Double-check that there's a title anime record.
		if titleAnime == nil {
			return errEntryDoesNotExist(
				titleEpisode.TitleID,
			)
		}

		queueValue := &queue.State{
			Details:     make([]queue.Detail, len(xdccs)),
			StartedFrom: queue.StartedFrom_Web,
		}

		for index, xdcc := range xdccs {
			queueValue.Details[index] = queue.Detail{
				ID:   xdcc.ID,
				Bot:  xdcc.Bot.Name,
				Pack: xdcc.Pack,
			}
		}

		anime := domain.AnimeBase{
			ID:           titleAnime.Anime.ID,
			Title:        titleAnime.Anime.Title,
			Episode:      titleEpisode.Episode.Name,
			ReleaseGroup: xdccs[0].ReleaseGroup.Name,
		}

		if xdccs[0].Quality != nil {
			anime.Height = int(xdccs[0].Quality.Height)
		}

		if h.fs.HasAnime(anime) {
			session.WriteJSON(protocol.New(
				protocol.Exception,
				&protocol.DataException{
					Message: fmt.Sprintf("`%s` already in file system", anime),
				}),
			)

			continue
		}

		pushed := false

		if item.Prioritize != nil && *item.Prioritize {
			pushed = h.queue.PushFront(anime, queueValue)
		} else {
			pushed = h.queue.PushBack(anime, queueValue)
		}

		if pushed {
			differences = append(differences, &protocol.DataActivityDifference{
				Token: protocol.DifferenceToken_Add,
				Anime: protocol.Anime{
					ID:            anime.ID,
					Title:         anime.Title,
					Episode:       anime.Episode,
					QualityHeight: anime.Height,
					ReleaseGroup:  anime.ReleaseGroup,
				},
			})
		}
	}

	if len(differences) > 0 {
		session.WriteJSON(protocol.New(protocol.Activity_Difference, differences))
	}

	return
}
