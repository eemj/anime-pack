package handler

import (
	"context"
	"errors"
	"io"
	"sync/atomic"

	gwebsocket "github.com/gorilla/websocket"

	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/irc"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"gitlab.com/eemj/anime-pack/pkg/sync"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type ServerHandler struct {
	services  lifecycle.ServiceLifecycle
	ircClient irc.Client
	queue     *queue.Queue
	fs        *fs.FileSystem
	hub       websocket.Hub

	active       atomic.Bool
	done         chan struct{}
	progressPool sync.Pool[[]byte]
}

func NewServerHandler(
	services lifecycle.ServiceLifecycle,
	ircClient irc.Client,
	queue *queue.Queue,
	fs *fs.FileSystem,
	hub websocket.Hub,
) (h *ServerHandler) {
	h = &ServerHandler{
		services:  services,
		ircClient: ircClient,
		queue:     queue,
		fs:        fs,
		hub:       hub,
		done:      make(chan struct{}),
		progressPool: sync.NewPool(func() []byte {
			return protocol.MustBytes(protocol.Activity_Progress, &protocol.DataActivityProgress{})
		}),
	}

	h.ircClient.OnEvent(h.onIRCClientUpdate())

	return
}

func (h *ServerHandler) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.WebsocketServerHandlerName)
}

func (h *ServerHandler) Close() error {
	select {
	case h.done <- struct{}{}:
	default:
	}
	return nil
}

func (h *ServerHandler) OnError(session websocket.Session, err error) {
	if errors.Is(err, gwebsocket.ErrCloseSent) || gwebsocket.IsCloseError(
		err,
		gwebsocket.CloseNoStatusReceived,
		gwebsocket.CloseAbnormalClosure,
		gwebsocket.CloseNoStatusReceived,
	) && err.(*gwebsocket.CloseError).Text == io.ErrUnexpectedEOF.Error() {
		return
	}

	h.L(session.Context()).Error(
		"disconnect",
		zap.Stringer("addr", session.RemoteAddr()),
		zap.Error(err),
	)
}

func (h *ServerHandler) OnMessageText(
	session websocket.Session,
	payload protocol.Payload,
) {
	h.L(session.Context()).Info(
		"message",
		zap.Stringer("addr", session.RemoteAddr()),
		zap.Any("payload", payload),
	)

	var err error
	switch payload.Opcode {
	case protocol.Activity_Download:
		err = h.download(session, payload.Data.(*protocol.DataActivityDownload))
	case protocol.Activity_Cancel:
		err = h.cancel(session, payload.Data.(*protocol.DataActivityCancel))
	case protocol.Activity_GetProgress:
		err = h.sendProgress(session)
	}
	if err != nil {
		err = session.WriteError(err)
		if err != nil {
			h.L(session.Context()).Error(
				"failed to report error to session",
				zap.Stringer("addr", session.RemoteAddr()),
				zap.Error(err),
			)
		}
	}
}
