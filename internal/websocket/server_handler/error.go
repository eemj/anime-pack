package handler

import (
	"errors"
	"fmt"
)

var (
	errTitleIsNotReviewed  = errors.New(`title is not reviewed`)
	errXdccEntriesNotFound = errors.New(`xdcc entries not found`)
)

func errEntryDoesNotExist(params ...any) error {
	return fmt.Errorf(
		"entry does not exist: %v",
		params...,
	)
}
