package handler

import (
	"sort"

	"gitlab.com/eemj/anime-pack/internal/domain"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.uber.org/zap"
)

func (w *ServerHandler) cancel(
	session websocket.Session,
	data *protocol.DataActivityCancel,
) (err error) {
	animeStatus := make(map[*domain.AnimeBase]bool)
	animes := make([]*domain.AnimeBase, 0)

	for _, item := range *data {
		anime, err := w.extractCommonAnime(session.Context(), &protocol.DataDownloadItem{
			ID:             item.ID,
			QualityID:      item.QualityID,
			ReleaseGroupID: item.ReleaseGroupID,
		})
		if err != nil {
			return err
		}

		animeStatus[anime] = w.queue.IsActive(*anime)
		animes = append(animes, anime)
	}

	// In order not to bombard the bots, we'll cancel those that aren't active first.
	// False should be the 1st.
	sort.Slice(animes, func(i, j int) bool {
		aEntry := animeStatus[animes[i]]
		bEntry := animeStatus[animes[j]]

		if (aEntry && bEntry) || (aEntry && !bEntry) {
			return false
		}

		return true
	})

	differences := make(protocol.DataActivityDifferences, len(animes))

	for index, anime := range animes {
		w.L(session.Context()).Debug(
			"cancel",
			zap.Stringer("anime", anime),
			zap.Bool("is_active", animeStatus[anime]),
		)

		if err := w.services.Runner().Interrupt(*anime); err != nil {
			return err
		}

		differences[index] = &protocol.DataActivityDifference{
			Token: protocol.DifferenceToken_Remove,
			Anime: protocol.Anime{
				ID:            anime.ID,
				Title:         anime.Title,
				Episode:       anime.Episode,
				QualityHeight: anime.Height,
				ReleaseGroup:  anime.ReleaseGroup,
			},
		}
	}

	if len(differences) > 0 {
		session.WriteJSON(protocol.New(protocol.Activity_Difference, differences))
	}

	return
}
