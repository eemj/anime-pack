package handler

import (
	"context"
	"time"

	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.uber.org/zap"
)

func (h *ServerHandler) progressBroadcast() {
	var (
		previousItems = make(queue.Items)

		// We'll notify the user every second.
		ticker = time.NewTicker(time.Second)

		L *zap.Logger = h.L(context.TODO()).Named("progress_broadcast")
	)
	defer ticker.Stop()

	for {
		select {
		case <-h.done:
			L.Info("stopping progress broadcast")
			return
		case <-ticker.C:
			nextItems, progress, err := h.services.Activity().Progress()
			if err != nil {
				L.Error("failed to retrieve activity progress", zap.Error(err))
				continue
			}

			progressBytes, err := protocol.NewBytes(protocol.Activity_Progress, &progress)
			if err != nil {
				L.Error("failed to marshal progress bytes", zap.Error(err))
				continue
			}

			h.progressPool.Put(progressBytes)

			differences := h.services.Activity().Difference(previousItems, nextItems)
			differenceBytes, err := protocol.NewBytes(protocol.Activity_Difference, &differences)
			if err != nil {
				L.Error("failed to marshal difference bytes", zap.Error(err))
				continue
			}

			previousItems = nextItems

			if len(differences) > 0 {
				if err = h.hub.BroadcastRaw(differenceBytes); err != nil {
					L.Error("failed to broadcast difference", zap.Error(err))
					continue
				}
			}

			// If we don't have any items left in queue, or the limit is 0, we'll stop broadcasting progress.
			h.active.Swap(h.queue.IsBusy())
		}
	}
}
