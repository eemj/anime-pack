package handler

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

func (w *ServerHandler) extractCommonAnime(ctx context.Context, item *protocol.DataDownloadItem) (anime *domain.AnimeBase, err error) {
	titleEpisode, err := w.services.Title().Associations().TitleEpisode().First(ctx, params.FirstTitleEpisodeParams{
		ID: &item.ID,
	})
	if err != nil {
		return nil, err
	}
	// Ensure that the record is present in our database
	if titleEpisode == nil {
		return nil, errEntryDoesNotExist(
			item.ID,
			item.QualityID,
			item.ReleaseGroupID,
		)
	}

	// Ensure that the title is reviewed
	if !titleEpisode.Title.Reviewed {
		return nil, errTitleIsNotReviewed
	}

	var quality *domain.Quality
	if item.QualityID != nil && !utils.IsEmpty(*item.QualityID) {
		quality, err = w.services.Quality().First(ctx, params.FirstQualityParams{
			ID: item.QualityID,
		})
		if err != nil {
			return nil, err
		}
		// Ensure that the record is present in our database
		if quality == nil {
			return nil, errEntryDoesNotExist(
				item.ID,
				item.QualityID,
				item.ReleaseGroupID,
			)
		}
	}

	titleAnime, err := w.services.Title().Associations().TitleAnime().First(ctx, params.FirstTitleAnimeParams{
		TitleID: &titleEpisode.TitleID,
	})
	if err != nil {
		return nil, err
	}
	if titleAnime == nil {
		w.L(ctx)

		return nil, errEntryDoesNotExist(
			item.ID,
			item.QualityID,
			item.ReleaseGroupID,
		)
	}

	releaseGroup, err := w.services.ReleaseGroup().First(ctx, params.FirstReleaseGroupParams{
		ID: &item.ReleaseGroupID,
	})
	if err != nil {
		return nil, err
	}

	anime = &domain.AnimeBase{
		ID:           titleAnime.Anime.ID,
		Title:        titleAnime.Anime.Title,
		Episode:      titleEpisode.Episode.Name,
		ReleaseGroup: releaseGroup.Name,
	}

	if quality != nil && quality.Height > 0 {
		anime.Height = int(quality.Height)
	}

	return anime, nil
}
