package handler

import (
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

func (h *ServerHandler) OnDisconnect(session websocket.Session) {
	log.Named("websocket").
		Sugar().
		Infow(
			"disconnect",
			zap.Stringer("addr", session.RemoteAddr()),
		)
}
