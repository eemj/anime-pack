package handler

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/irc"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

func (h *ServerHandler) onIRCClientUpdate() irc.OnEvent {
	return func(ctx context.Context, event irc.IRCCustomEvent, msg interface{}) (err error) {
		switch event {
		case irc.Event_CTCP_DCC:
			if h.active.CompareAndSwap(false, true) {
				h.L(ctx).Info("starting progress broadcast")
				go h.progressBroadcast()
			}
		case irc.Event_CONNECTION_STATE_UPDATE:
			connectedMsg, ok := msg.(*irc.ConnectionStateUpdateMessage)
			if ok {
				return h.hub.BroadcastPayload(
					protocol.Payload{
						Opcode: protocol.Status_IRC,
						Data:   &protocol.DataStatusIRC{Connected: connectedMsg.Connected},
					},
				)
			}
		}
		return nil
	}
}
