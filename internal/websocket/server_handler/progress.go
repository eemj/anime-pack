package handler

import (
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"go.uber.org/zap"
)

func (h *ServerHandler) sendProgress(session websocket.Session) error {
	_, err := session.Text().Write(h.progressPool.Get())
	if err != nil {
		h.L(session.Context()).Error("failed to write progress bytes", zap.Error(err))
		return err
	}
	return nil
}
