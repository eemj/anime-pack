package handler

import (
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.uber.org/zap"
)

func (h *ServerHandler) OnConnect(session websocket.Session) {
	h.L(session.Context()).Info(
		"connect",
		zap.Stringer("addr", session.RemoteAddr()),
	)

	session.WriteJSON(protocol.New(
		protocol.Acknowledged,
		&protocol.DataAcknowledged{},
	))

	session.WriteJSON(protocol.New(
		protocol.Status_IRC,
		&protocol.DataStatusIRC{Connected: h.ircClient.IsConnected()},
	))

	h.sendProgress(session)
}
