package websocket

import (
	"context"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

// Handler is an interface which defines a handler,
// methods are called upon when an event occurs.
type Handler interface {
	OnError(Session, error)
	OnConnect(Session)
	OnDisconnect(Session)
	OnMessageText(Session, protocol.Payload)
	Close() error
}

// WebSocket defines a WebSocket server that's designed with the `anime-pack`'s protocol in mind.
type WebSocket struct {
	rwmutex  sync.RWMutex
	upgrader *websocket.Upgrader
	handler  Handler
	config   *Config
	hub      Hub
}

func (w *WebSocket) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.WebsocketServerName)
}

// NewWebSocket returns an initialized WebSocket instance with the default config.
func NewWebSocket(hub Hub) (w *WebSocket) { return NewWithConfig(hub, DefaultConfig) }

// NewWithConfig returns an initialized WebSocket with the specified config.
func NewWithConfig(hub Hub, config *Config) (w *WebSocket) {
	return &WebSocket{
		hub:    hub,
		config: config,
		upgrader: &websocket.Upgrader{
			ReadBufferSize:    config.MaxMessageSize,
			WriteBufferSize:   config.MaxMessageSize,
			Subprotocols:      []string{"anime-pack.v1"},
			EnableCompression: true,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
	}
}

// SetupHandler set's up the provided handler to the websocket in-order to start receiving events.
func (w *WebSocket) SetupHandler(handler Handler) { w.handler = handler }

// Hub returns the WebSocket's hub where it allows the user to
// be able to broadcast all sorts of messages to connected sessions.
func (w *WebSocket) Hub() Hub { return w.hub }

// Close will forcefully close out all the other connections.
func (w *WebSocket) Close() {
	w.rwmutex.Lock() // Hard lock the array, so that no new additions will be added.
	defer w.rwmutex.Unlock()
	w.hub.Close()
	w.handler.Close()
}

// Request provides a `net.http`.Handler to establish a websocket connection
func (w *WebSocket) Request(rw http.ResponseWriter, r *http.Request) {
	conn, err := w.upgrader.Upgrade(rw, r, rw.Header())
	if err != nil {
		w.L(r.Context()).Error(
			"upgrade failed",
			zap.String("remote_addr", r.RemoteAddr),
			zap.Error(err),
		)
		return
	}

	s := newSession(w, conn, r)

	// The type will always be of *session
	sess, _ := s.(*session)

	w.handler.OnConnect(s)

	w.hub.Register(s)

	go sess.writePump()

	sess.readPump()

	w.hub.Unregister(s)

	// Close it out, once the read loop ends.
	sess.close()

	// Let the handler know that this session closed.
	w.handler.OnDisconnect(s)
}
