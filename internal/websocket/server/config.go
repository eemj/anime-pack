package websocket

import "time"

// Config configuration struct
type Config struct {
	WriteWait         time.Duration
	PongWait          time.Duration
	PingPeriod        time.Duration
	MaxMessageSize    int
	MessageBufferSize int
}

var DefaultConfig = &Config{
	WriteWait:         10 * time.Second,
	PongWait:          45 * time.Second,
	PingPeriod:        10 * time.Second,
	MaxMessageSize:    30 * 1024,
	MessageBufferSize: 64,
}
