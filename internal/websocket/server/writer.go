package websocket

import "io"

type wrapper struct {
	s Session
	t int
}

func newWrapper(
	messageType int,
	session Session,
) io.Writer {
	return &wrapper{
		s: session,
		t: messageType,
	}
}

func (w *wrapper) Write(b []byte) (n int, err error) {
	n = len(b)
	w.s.Write(w.t, b)
	return
}
