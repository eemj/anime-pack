package websocket

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	connections = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: `anime_pack`,
		Subsystem: `websocket`,
		Name:      `connections_by_addr`,
		Help:      `Number of active websocket connections by address`,
	}, []string{"addr"})

	bytesSent = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: `anime_pack`,
		Subsystem: `websocket`,
		Name:      `bytes_sent_by_addr`,
		Help:      `Number of bytes sent by address`,
	}, []string{"addr"})
)
