package websocket

import (
	"sync"
	"sync/atomic"

	"github.com/gorilla/websocket"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

// Hub defines the methods for the hub.
// The hub is responsible for broadcasting messages
// amongst registered sessions.
type Hub interface {
	BroadcastPayload(protocol.Payload) error
	BroadcastRaw([]byte) error
	Close()
	Registered(session Session) bool
	Register(session Session)
	Unregister(session Session)
}

type hub struct {
	sessions   map[Session]struct{}
	register   chan Session
	unregister chan Session
	broadcast  chan *envelope
	exit       chan *envelope
	rwmutex    sync.RWMutex
	open       atomic.Bool
}

func NewHub() *hub {
	h := &hub{
		sessions:   make(map[Session]struct{}),
		register:   make(chan Session),
		unregister: make(chan Session),
		broadcast:  make(chan *envelope),
		exit:       make(chan *envelope),
	}

	h.open.Store(true)

	go h.run()

	return h
}

func (h *hub) Registered(s Session) bool {
	h.rwmutex.RLock()
	_, ok := h.sessions[s]
	h.rwmutex.RUnlock()
	return ok
}

func (h *hub) Close() { h.exit <- nil }

func (h *hub) Register(session Session) { h.register <- session }

func (h *hub) Unregister(session Session) {
	if h.open.Load() {
		h.unregister <- session
	}
}

func (h *hub) run() {
LOOP:
	for {
		select {
		case s := <-h.register:
			{
				h.rwmutex.Lock()
				h.sessions[s] = struct{}{}
				h.rwmutex.Unlock()

				if remoteAddr := s.RemoteAddr().Addr(); remoteAddr.IsValid() {
					connections.
						WithLabelValues(remoteAddr.StringExpanded()).
						Inc()
				}
			}
		case s := <-h.unregister:
			if h.Registered(s) {
				h.rwmutex.Lock()
				delete(h.sessions, s)
				{
					if remoteAddr := s.RemoteAddr().Addr(); remoteAddr.IsValid() {
						connections.
							WithLabelValues(remoteAddr.StringExpanded()).
							Dec()
					}
				}
				h.rwmutex.Unlock()
			}
		case m := <-h.broadcast:
			if m != nil {
				h.rwmutex.RLock()
				for s := range h.sessions {
					if m.filter != nil {
						if m.filter(s.RemoteAddr()) {
							s.Write(m.t, m.msg)
						}
					} else {
						s.Write(m.t, m.msg)
					}
				}
				h.rwmutex.RUnlock()
			}
		case m := <-h.exit:
			h.rwmutex.Lock()
			for s := range h.sessions {
				if m != nil {
					s.Write(m.t, m.msg)
				}
				delete(h.sessions, s)
				s.Close(nil)
			}
			h.open.Store(false)
			h.rwmutex.Unlock()

			break LOOP
		}
	}
}

func (h *hub) BroadcastRaw(msg []byte) (err error) {
	h.broadcast <- &envelope{
		t:      websocket.TextMessage,
		msg:    msg,
		filter: truthy,
	}

	return
}

func (h *hub) BroadcastPayload(payload protocol.Payload) (err error) {
	msg, err := payload.MarshalJSON()
	if err != nil {
		return err
	}
	return h.BroadcastRaw(msg)
}
