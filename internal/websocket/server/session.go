package websocket

import (
	"context"
	"encoding/json"
	"io"
	"net"
	"net/http"
	"net/netip"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/eemj/anime-pack/pkg/utils"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"

	"github.com/gorilla/websocket"
)

type Session interface {
	// RemoteAddr is the *http.Request's Real Remote Address (parses X-Forwarded-For or X-Real-IP)
	RemoteAddr() netip.AddrPort
	// Text() returns an io.Writer, writing text messages to the session.
	Text() io.Writer
	Write(messageType int, msg []byte)
	// WriteError writes an error in the custom protocol form. Any error occured during the
	// write is JSON related.
	WriteError(writeError error) (err error)
	WriteJSON(payload any) (err error)
	Close(closeError error)
	Context() context.Context
	Latency() float64
}

type session struct {
	// conn is the Accepted websocket connected established from the Handler().
	conn *websocket.Conn

	// ctx is the *http.Request's context.
	ctx context.Context

	// remoteAddr is the *http.Request's Real Remote Address (headers get parsed).
	remoteAddr netip.AddrPort

	// buffer is a channel buffer which holds messages that need to get sent.
	buffer chan *envelope

	// server holds the parent instance of the websocket server, it's used to retrieve
	// configuration values and access handler callback methods.
	server *WebSocket

	// rwmutex ensures concurrency amongst the session's properties.
	rwmutex sync.RWMutex

	// closed defines if the connection is active
	closed atomic.Bool

	// lastPing holds the nanosecond time when the last ping got sent.
	lastPing atomic.Int64
}

// Context returns the underlying (*http.Request).Context() associated with the session
func (s *session) Context() context.Context { return s.ctx }

func (s *session) RemoteAddr() netip.AddrPort { return s.remoteAddr }

// write is an internal method to a session, it respects the buffer config specified
// in the websocket server.
func (s *session) write(message *envelope) {
	if s.closed.Load() {
		s.server.handler.OnError(
			s,
			newError(
				TriedToWriteToAClosedSession,
				s.RemoteAddr().String(),
				!s.closed.Load(),
			),
		)
		return
	}

	select {
	case s.buffer <- message:
		bytesSent.
			WithLabelValues(s.remoteAddr.Addr().StringExpanded()).
			Add(float64(len(message.msg)))
	default:
		err := newError(
			SessionMessageBufferIsFull,
			s.RemoteAddr().String(),
			!s.closed.Load(),
		)

		// Notify the handler about the exception.
		s.server.handler.OnError(s, err)

		// Close down the connection, we can't keep sending messages
		// when the session is not receiving them.
		s.close()
	}
}

func (s *session) readPump() {
	s.conn.SetReadLimit(int64(s.server.config.MaxMessageSize))
	s.conn.SetReadDeadline(time.Now().Add(s.server.config.PongWait))

	s.conn.SetPongHandler(func(string) error {
		s.lastPing.Swap(time.Now().UnixNano() - s.lastPing.Load())

		return s.conn.SetReadDeadline(
			time.Now().Add(s.server.config.PongWait),
		)
	})

	for {
		typ, content, err := s.conn.ReadMessage()
		if err != nil {
			if !(websocket.IsCloseError(
				err,
				websocket.CloseGoingAway,
			)) {
				s.server.handler.OnError(s, err)
			}
			break
		}

		if typ == websocket.TextMessage {
			if len(content) == 0 {
				continue
			}

			if content[0] != '{' {
				s.WriteError(protocol.ErrExpectedValidJSON)
				continue
			}

			payload := protocol.Payload{}

			if err := payload.UnmarshalJSON(content); err != nil {
				s.WriteError(err)
				continue
			}

			if payload.Data == nil {
				s.WriteError(protocol.ErrDataIsRequired)
				continue
			}

			if err := payload.Data.Validate(); err != nil {
				s.WriteError(err)
				continue
			}

			s.server.handler.OnMessageText(
				s,
				payload,
			)
		} else if typ == websocket.BinaryMessage {
			// We'll discard binary messages, they're no longer supported.
			if _, err = io.Discard.Write(content); err != nil {
				s.WriteError(err)
			}
		}
	}
}

func (s *session) ping() {
	s.write(&envelope{t: websocket.PingMessage, msg: []byte{}})
}

func (s *session) writePump() {
	ticker := time.NewTicker(s.server.config.PingPeriod)

	defer ticker.Stop()

LOOP:
	for {
		select {
		case m, ok := <-s.buffer:
			if !ok {
				break LOOP
			}

			s.conn.SetWriteDeadline(
				time.Now().Add(s.server.config.WriteWait),
			)

			if m.t == websocket.PingMessage {
				s.lastPing.Swap(time.Now().UnixNano())
			}

			err := s.conn.WriteMessage(
				m.t,
				m.msg,
			)
			if err != nil {
				s.server.handler.OnError(s, err)
				break LOOP
			}

			if m.t == websocket.CloseMessage {
				break LOOP
			}
		case <-ticker.C:
			s.ping()
		}
	}
}

func (s *session) Text() io.Writer {
	return newWrapper(
		websocket.TextMessage,
		s,
	)
}

func (s *session) WriteError(err error) error {
	if err == nil {
		return nil
	}

	return s.WriteJSON(protocol.Payload{
		Opcode: protocol.Exception,
		Data: &protocol.DataException{
			Message: err.Error(),
		},
	})
}

func (s *session) Write(messageType int, msg []byte) {
	s.write(&envelope{
		msg: msg,
		t:   messageType,
	})
}

func (s *session) WriteJSON(payload any) (err error) {
	msg, err := json.Marshal(payload)
	if err != nil {
		return
	}

	s.write(&envelope{
		msg: msg,
		t:   websocket.TextMessage,
	})

	return
}

// Latency will return the latency.
func (s *session) Latency() float64 {
	return float64(s.lastPing.Load()) / float64(time.Millisecond)
}

// close will actually close, it'll close the buffer channel, set the `open` boolean to false,
// and close the websocket connection.
func (s *session) close() {
	if s.closed.CompareAndSwap(false, true) {
		s.rwmutex.Lock()
		close(s.buffer)
		s.rwmutex.Unlock()
	}
}

// Close will write out that particular session, it'll send out a formatted websocket
// message depending on the error. if err == nil it'll send out GoingAway, otherwise it'll
// send out AbnormalClosure.
func (s *session) Close(err error) {
	if s.closed.Load() {
		return
	}

	if err != nil {
		s.write(&envelope{
			t: websocket.CloseMessage,
			msg: websocket.FormatCloseMessage(
				websocket.CloseAbnormalClosure,
				err.Error(),
			),
		})
		return
	}

	s.write(&envelope{
		t:   websocket.CloseMessage,
		msg: []byte{},
	})
}

func newSession(
	server *WebSocket,
	conn *websocket.Conn,
	r *http.Request,
) Session {
	host, port, _ := utils.RealRemoteAddr(r)
	remoteAddr, _ := netip.ParseAddrPort(net.JoinHostPort(host, port))

	return &session{
		remoteAddr: remoteAddr,
		conn:       conn,
		ctx:        r.Context(),
		server:     server,
		buffer:     make(chan *envelope, server.config.MessageBufferSize),
	}
}
