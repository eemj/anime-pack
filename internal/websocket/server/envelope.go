package websocket

import "net/netip"

var truthy = filterFunc(func(remoteAddr netip.AddrPort) bool { return true })

type filterFunc func(remoteAddr netip.AddrPort) bool

type envelope struct {
	filter filterFunc
	msg    []byte
	t      int
}
