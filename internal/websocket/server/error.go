package websocket

import "fmt"

const (
	TriedToWriteToAClosedSession = "tried to write to a closed session"
	SessionMessageBufferIsFull   = "session message buffer is full"
)

type Error struct {
	message    string
	remoteAddr string
	connected  bool
}

func (e *Error) Error() string {
	return fmt.Sprintf(
		"%s: (remoteAddr %s, connected: %v)",
		e.message,
		e.remoteAddr,
		e.connected,
	)
}

func (e *Error) Is(err error) bool {
	if sessionError, ok := err.(*Error); ok {
		return sessionError.message == e.message
	}
	return false
}

func newError(message, remoteAddr string, connected bool) error {
	return &Error{
		message:    message,
		remoteAddr: remoteAddr,
		connected:  connected,
	}
}
