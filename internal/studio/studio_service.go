package studio

import (
	"context"
	"sort"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type StudioService interface {
	CreateMany(ctx context.Context, arg params.InsertStudioParams) ([]*domain.Studio, error)
	First(ctx context.Context, arg params.FirstStudioParams) (*domain.Studio, error)
	Filter(ctx context.Context, arg params.FilterStudioParams) ([]*domain.Studio, error)
	Count(ctx context.Context, arg params.CountStudioParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateStudioParams) (int64, error)
	List(ctx context.Context) (domain.Studios, error)
}

type studioService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// List implements StudioService.
func (s *studioService) List(ctx context.Context) (domain.Studios, error) {
	cachedValue, err := encoded.Get[domain.Studios](ctx, s.cacher, cachekeys.AnimeStudios)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	rows, err := s.querier.ListAnimeReviewStudio(ctx)
	if err != nil {
		return nil, err
	}
	result := mapEntitesToDomain(rows...)
	sort.Sort(domain.StudiosSortByName(result))
	err = encoded.Set(
		ctx,
		s.cacher,
		cachekeys.AnimeStudios,
		result,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Count implements Service.
func (s *studioService) Count(ctx context.Context, arg params.CountStudioParams) (int64, error) {
	count, err := s.querier.CountStudio(ctx, database.CountStudioParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// CreateMany implements Service.
func (s *studioService) CreateMany(ctx context.Context, arg params.InsertStudioParams) ([]*domain.Studio, error) {
	args := make([]database.InsertStudioParams, len(arg.Names))
	for index, name := range arg.Names {
		args[index] = database.InsertStudioParams{Name: name}
	}

	bulk := s.querier.InsertStudio(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertStudioRow,
		*domain.Studio,
	](bulk, func(row *database.InsertStudioRow) *domain.Studio {
		return &domain.Studio{
			ID:   row.ID,
			Name: row.Name,
		}
	})
}

func mapEntitesToDomain(entities ...*database.Studio) []*domain.Studio {
	result := make([]*domain.Studio, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Studio{
			ID:   entity.ID,
			Name: entity.Name,
		}
	}
	return result
}

// Filter implements Service.
func (s *studioService) Filter(ctx context.Context, arg params.FilterStudioParams) ([]*domain.Studio, error) {
	entities, err := s.querier.FilterStudio(ctx, database.FilterStudioParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Studio{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *studioService) First(ctx context.Context, arg params.FirstStudioParams) (*domain.Studio, error) {
	entity, err := s.querier.FirstStudio(ctx, database.FirstStudioParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entity)[0], nil
}

// Update implements Service.
func (s *studioService) Update(ctx context.Context, arg params.UpdateStudioParams) (int64, error) {
	return s.querier.UpdateStudio(ctx, database.UpdateStudioParams{
		ID:   arg.ID,
		Name: arg.Name,
	})
}

func NewStudioService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) StudioService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &studioService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
