package bot

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type BotService interface {
	Create(ctx context.Context, arg params.InsertBotParamsItem) (*domain.Bot, error)
	CreateMany(ctx context.Context, arg params.InsertBotParams) ([]*domain.Bot, error)
	First(ctx context.Context, arg params.FirstBotParams) (*domain.Bot, error)
	Filter(ctx context.Context, arg params.FilterBotParams) ([]*domain.Bot, error)
	Count(ctx context.Context, arg params.CountBotParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateBotParams) (int64, error)
}

type botService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements Service.
func (s *botService) Count(ctx context.Context, arg params.CountBotParams) (int64, error) {
	count, err := s.querier.CountBot(ctx, database.CountBotParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// Create implements Service.
func (s *botService) Create(ctx context.Context, arg params.InsertBotParamsItem) (*domain.Bot, error) {
	cacheKey := cachekeys.Bot(&arg.Name)
	cacheValue, err := encoded.Get[*domain.Bot](ctx, s.cacher, cacheKey)
	if err != nil {
		return nil, err
	}
	if cacheValue != nil {
		return cacheValue, nil
	}
	row, err := s.querier.InsertBot(ctx, database.InsertBotParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.Bot{
		ID:   row.ID,
		Name: row.Name,
	}
	// TODO(eemj): Configurable expiry
	err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// CreateMany implements Service.
func (s *botService) CreateMany(ctx context.Context, arg params.InsertBotParams) ([]*domain.Bot, error) {
	args := make([]database.InsertManyBotParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyBotParams{
			Name: item.Name,
		}
	}

	bulk := s.querier.InsertManyBot(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyBotRow,
		*domain.Bot,
	](bulk, func(row *database.InsertManyBotRow) *domain.Bot {
		return &domain.Bot{
			ID:   row.ID,
			Name: row.Name,
		}
	})
}

func mapEntitesToDomain(entities ...*database.Bot) []*domain.Bot {
	result := make([]*domain.Bot, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Bot{
			ID:   entity.ID,
			Name: entity.Name,
		}
	}
	return result
}

// Filter implements Service.
func (s *botService) Filter(ctx context.Context, arg params.FilterBotParams) ([]*domain.Bot, error) {
	entities, err := s.querier.FilterBot(ctx, database.FilterBotParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Bot{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *botService) First(ctx context.Context, arg params.FirstBotParams) (*domain.Bot, error) {
	entity, err := s.querier.FirstBot(ctx, database.FirstBotParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entity)[0], nil
}

// Update implements Service.
func (s *botService) Update(ctx context.Context, arg params.UpdateBotParams) (int64, error) {
	return s.querier.UpdateBot(ctx, database.UpdateBotParams{
		ID:   arg.ID,
		Name: arg.Name,
	})
}

func NewBotService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) BotService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &botService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
