package pgxutilx

import (
	"errors"
	"fmt"

	"gitlab.com/eemj/anime-pack/internal/database"
)

// QueryResulter is an interface for performing query operations and managing the result.
type QueryResulter[T any] interface {
	Close() error
	Query(func(int, []T, error))
}

// QueryResultMap maps the results of a bulk query operation using the provided mapping function.
// It ensures that the batch is closed when an error occurs.
func QueryResultMap[Row, Output any](bulk QueryResulter[Row], mapping func(Row) Output) (output []Output, err error) {
	output = make([]Output, 0)
	bulk.Query(func(_ int, _results []Row, _err error) {
		if _err != nil {
			if err != database.ErrBatchAlreadyClosed {
				err = errors.Join(err, _err)
				bulk.Close() // Close to terminate the loop
			}
			return
		}
		for _, result := range _results {
			output = append(output, mapping(result))
		}
	})
	if err != nil {
		return nil, err
	}
	return output, nil
}

// ExecResulter is an interface for performing execution operations and managing the result.
type ExecResulter interface {
	Close() error
	Exec(func(int, error))
}

// ExecResult executes a batch of operations and handles errors for each operation in the batch.
// It ensures that the batch is closed when an error occurs.
func ExecResult[T any](params []T, cb ExecResulter) (err error) {
	cb.Exec(func(i int, _err error) {
		if err == nil {
			return
		}

		err = errors.Join(
			err,
			fmt.Errorf("%T exec: `%w` value '%+v'", params[i], _err, params[i]),
		)
		if closeErr := cb.Close(); closeErr != nil {
			err = errors.Join(err, closeErr)
		}
	})
	return err
}
