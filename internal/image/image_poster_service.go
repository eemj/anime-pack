package image

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/fs"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
)

type PosterService interface {
	Create(ctx context.Context, arg params.InsertPosterParamsItem) (*domain.Poster, error)
	CreateMany(ctx context.Context, arg params.InsertPosterParams) ([]*domain.Poster, error)
	First(ctx context.Context, arg params.FirstPosterParams) (*domain.Poster, error)
	Filter(ctx context.Context, arg params.FilterPosterParams) ([]*domain.Poster, error)
	Count(ctx context.Context, arg params.CountPosterParams) (int64, error)
	Update(ctx context.Context, arg params.UpdatePosterParams) (int64, error)
	Delete(ctx context.Context, arg params.DeletePosterParams) (int64, error)
	DeleteUnused(ctx context.Context) (int64, error)

	CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Poster, error)
}

type posterService struct {
	querier database.QuerierExtended
	cacher  cacher.Cacher
	fs      *fs.FileSystem
}

// DeleteUnused implements PosterService.
func (s *posterService) DeleteUnused(ctx context.Context) (int64, error) {
	return s.querier.DeleteUnusedPosters(ctx)
}

// CreateFromReader implements PosterService.
func (s *posterService) CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Poster, error) {
	hash, err := s.fs.CreatePosterImage(arg.Reader)
	if err != nil {
		return nil, err
	}
	image, err := s.Create(ctx, params.InsertPosterParamsItem{
		Name: (hash + fs.ExtensionImage),
		Uri:  arg.Uri,
		Hash: hash,
	})
	if err != nil {
		return nil, err
	}
	return image, nil
}

// Count implements PosterService.
func (s *posterService) Count(ctx context.Context, arg params.CountPosterParams) (int64, error) {
	return s.querier.CountPoster(ctx, database.CountPosterParams(arg))
}

// Create implements PosterService.
func (s *posterService) Create(ctx context.Context, arg params.InsertPosterParamsItem) (*domain.Poster, error) {
	row, err := s.querier.InsertPoster(ctx, database.InsertPosterParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &domain.Poster{
		ID:   row.ID,
		Name: row.Name,
		Hash: row.Hash,
		Uri:  row.Uri,
	}, nil
}

// CreateMany implements PosterService.
func (s *posterService) CreateMany(ctx context.Context, arg params.InsertPosterParams) ([]*domain.Poster, error) {
	args := make([]database.InsertManyPosterParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyPosterParams(item)
	}

	bulk := s.querier.InsertManyPoster(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyPosterRow,
		*domain.Poster,
	](bulk, func(row *database.InsertManyPosterRow) *domain.Poster {
		return &domain.Poster{
			ID:   row.ID,
			Name: row.Name,
			Hash: row.Hash,
			Uri:  row.Uri,
		}
	})
}

// Delete implements PosterService.
func (s *posterService) Delete(ctx context.Context, arg params.DeletePosterParams) (int64, error) {
	image, err := s.First(ctx, params.FirstPosterParams{
		ID: &arg.ID,
	})
	if err != nil {
		return 0, err
	}
	err = s.fs.DeletePosterImage(image.Name)
	if err != nil {
		return 0, err
	}

	return s.querier.DeletePoster(ctx, database.DeletePosterParams(arg))
}

func mapPosterDatabaseToDomain(entities ...*database.Poster) []*domain.Poster {
	result := make([]*domain.Poster, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Poster{
			ID:   entity.ID,
			Name: entity.Name,
			Hash: entity.Hash,
			Uri:  entity.Uri,
		}
	}
	return result
}

// Filter implements PosterService.
func (s *posterService) Filter(ctx context.Context, arg params.FilterPosterParams) ([]*domain.Poster, error) {
	rows, err := s.querier.FilterPoster(ctx, database.FilterPosterParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Poster{}, nil
		}
		return nil, err
	}
	return mapPosterDatabaseToDomain(rows...), nil
}

// First implements PosterService.
func (s *posterService) First(ctx context.Context, arg params.FirstPosterParams) (*domain.Poster, error) {
	row, err := s.querier.FirstPoster(ctx, database.FirstPosterParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapPosterDatabaseToDomain(row)[0], nil
}

// Update implements PosterService.
func (s *posterService) Update(ctx context.Context, arg params.UpdatePosterParams) (int64, error) {
	return s.querier.UpdatePoster(ctx, database.UpdatePosterParams(arg))
}

func NewPosterService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	fs *fs.FileSystem,
) PosterService {
	return &posterService{
		querier: querier,
		cacher:  cacher,
		fs:      fs,
	}
}
