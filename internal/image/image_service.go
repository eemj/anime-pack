package image

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

type ImageService interface {
	Banner() BannerService
	Poster() PosterService
	Thumbnail() ThumbnailService
	Processor() ProcessorService

	DeleteUnused(ctx context.Context) (*domain.UnusedImages, error)
}

type imageService struct {
	banner    BannerService
	poster    PosterService
	thumbnail ThumbnailService
	processor ProcessorService
}

// DeleteUnused implements ImageService.
func (s *imageService) DeleteUnused(ctx context.Context) (*domain.UnusedImages, error) {
	posterRowsAffected, err := s.poster.DeleteUnused(ctx)
	if err != nil {
		return nil, err
	}
	bannerRowsAffected, err := s.banner.DeleteUnused(ctx)
	if err != nil {
		return nil, err
	}
	thumbnailRowsAffected, err := s.thumbnail.DeleteUnused(ctx)
	if err != nil {
		return nil, err
	}
	return &domain.UnusedImages{
		UnusedPoster:    posterRowsAffected,
		UnusedBanner:    bannerRowsAffected,
		UnusedThumbnail: thumbnailRowsAffected,
	}, nil
}

func (s *imageService) Banner() BannerService       { return s.banner }
func (s *imageService) Poster() PosterService       { return s.poster }
func (s *imageService) Processor() ProcessorService { return s.processor }
func (s *imageService) Thumbnail() ThumbnailService { return s.thumbnail }

func NewImageService(
	banner BannerService,
	poster PosterService,
	thumbnail ThumbnailService,
	processor ProcessorService,
) ImageService {
	return &imageService{
		banner:    banner,
		poster:    poster,
		thumbnail: thumbnail,
		processor: processor,
	}
}
