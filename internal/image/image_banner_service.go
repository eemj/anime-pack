package image

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/fs"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
)

type BannerService interface {
	Create(ctx context.Context, arg params.InsertBannerParamsItem) (*domain.Banner, error)
	CreateMany(ctx context.Context, arg params.InsertBannerParams) ([]*domain.Banner, error)
	First(ctx context.Context, arg params.FirstBannerParams) (*domain.Banner, error)
	Filter(ctx context.Context, arg params.FilterBannerParams) ([]*domain.Banner, error)
	Count(ctx context.Context, arg params.CountBannerParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateBannerParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteBannerParams) (int64, error)
	DeleteUnused(ctx context.Context) (int64, error)

	CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Banner, error)
}

type bannerService struct {
	querier database.QuerierExtended
	cacher  cacher.Cacher
	fs      *fs.FileSystem
}

// DeleteUnused implements BannerService.
func (s *bannerService) DeleteUnused(ctx context.Context) (int64, error) {
	return s.querier.DeleteUnusedBanners(ctx)
}

// CreateFromReader implements BannerService.
func (s *bannerService) CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Banner, error) {
	hash, err := s.fs.CreateBannerImage(arg.Reader)
	if err != nil {
		return nil, err
	}
	image, err := s.Create(ctx, params.InsertBannerParamsItem{
		Name: (hash + fs.ExtensionImage),
		Uri:  arg.Uri,
		Hash: hash,
	})
	if err != nil {
		return nil, err
	}
	return image, nil
}

// Count implements BannerService.
func (s *bannerService) Count(ctx context.Context, arg params.CountBannerParams) (int64, error) {
	return s.querier.CountBanner(ctx, database.CountBannerParams(arg))
}

// Create implements BannerService.
func (s *bannerService) Create(ctx context.Context, arg params.InsertBannerParamsItem) (*domain.Banner, error) {
	row, err := s.querier.InsertBanner(ctx, database.InsertBannerParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &domain.Banner{
		ID:   row.ID,
		Name: row.Name,
		Hash: row.Hash,
		Uri:  row.Uri,
	}, nil
}

// CreateMany implements BannerService.
func (s *bannerService) CreateMany(ctx context.Context, arg params.InsertBannerParams) ([]*domain.Banner, error) {
	args := make([]database.InsertManyBannerParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyBannerParams(item)
	}

	bulk := s.querier.InsertManyBanner(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyBannerRow,
		*domain.Banner,
	](bulk, func(row *database.InsertManyBannerRow) *domain.Banner {
		return &domain.Banner{
			ID:   row.ID,
			Name: row.Name,
			Hash: row.Hash,
			Uri:  row.Uri,
		}
	})
}

// Delete implements BannerService.
func (s *bannerService) Delete(ctx context.Context, arg params.DeleteBannerParams) (int64, error) {
	image, err := s.First(ctx, params.FirstBannerParams{
		ID: &arg.ID,
	})
	if err != nil {
		return 0, err
	}
	err = s.fs.DeleteBannerImage(image.Name)
	if err != nil {
		return 0, err
	}

	return s.querier.DeleteBanner(ctx, database.DeleteBannerParams(arg))
}

func mapBannerDatabaseToDomain(entities ...*database.Banner) []*domain.Banner {
	result := make([]*domain.Banner, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Banner{
			ID:   entity.ID,
			Name: entity.Name,
			Hash: entity.Hash,
			Uri:  entity.Uri,
		}
	}
	return result
}

// Filter implements BannerService.
func (s *bannerService) Filter(ctx context.Context, arg params.FilterBannerParams) ([]*domain.Banner, error) {
	rows, err := s.querier.FilterBanner(ctx, database.FilterBannerParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Banner{}, nil
		}
		return nil, err
	}
	return mapBannerDatabaseToDomain(rows...), nil
}

// First implements BannerService.
func (s *bannerService) First(ctx context.Context, arg params.FirstBannerParams) (*domain.Banner, error) {
	row, err := s.querier.FirstBanner(ctx, database.FirstBannerParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapBannerDatabaseToDomain(row)[0], nil
}

// Update implements BannerService.
func (s *bannerService) Update(ctx context.Context, arg params.UpdateBannerParams) (int64, error) {
	return s.querier.UpdateBanner(ctx, database.UpdateBannerParams(arg))
}

func NewBannerService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	fs *fs.FileSystem,
) BannerService {
	return &bannerService{
		querier: querier,
		cacher:  cacher,
		fs:      fs,
	}
}
