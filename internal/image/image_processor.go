package image

import (
	"context"
	"errors"
	"fmt"
	"image"
	"io"
	"net/http"

	"github.com/EdlinOrg/prominentcolor"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/pkg/utils"

	// Image decode capabilities
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"

	// Extension decode capabilities
	_ "golang.org/x/image/tiff"
	_ "golang.org/x/image/webp"
)

// ErrUninitializedBuffer is returned when *bytes.Buffer is nil
var ErrUninitializedBuffer = errors.New("uninitialized buffer")

type ProcessorService interface {
	DominantColour(ctx context.Context, reader io.Reader) *string
	ReadImage(ctx context.Context, uri string) (imageBytes []byte, err error)
}

type processorService struct{}

// DominantColour implements ProcessorService
func (p *processorService) DominantColour(ctx context.Context, reader io.Reader) (colour *string) {
	img, _, err := image.Decode(reader)
	if err != nil {
		return nil
	}

	colours, err := prominentcolor.Kmeans(img)
	if err != nil {
		return
	}
	if len(colours) > 0 {
		colour = new(string)
		*colour = fmt.Sprintf(
			"#%.2X%.2X%.2X",
			colours[0].Color.R,
			colours[0].Color.G,
			colours[0].Color.B,
		)
	}

	return colour
}

// ReadImage implements ProcessorService
func (p *processorService) ReadImage(ctx context.Context, uri string) (imageBytes []byte, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", utils.GetUserAgent())
	req.Header.Set("Accept", "image/jpeg, image/png, image/gif, image/webp, image/*")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, apperror.ErrExpectedStatusCodeOkButGot(res.StatusCode)
	}

	defer res.Body.Close()

	return io.ReadAll(res.Body)
}

func NewProcessorService() ProcessorService {
	return &processorService{}
}
