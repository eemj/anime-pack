package image

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/fs"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
)

type ThumbnailService interface {
	Create(ctx context.Context, arg params.InsertThumbnailParamsItem) (*domain.Thumbnail, error)
	CreateMany(ctx context.Context, arg params.InsertThumbnailParams) ([]*domain.Thumbnail, error)
	First(ctx context.Context, arg params.FirstThumbnailParams) (*domain.Thumbnail, error)
	Filter(ctx context.Context, arg params.FilterThumbnailParams) ([]*domain.Thumbnail, error)
	Count(ctx context.Context, arg params.CountThumbnailParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateThumbnailParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteThumbnailParams) (int64, error)
	DeleteUnused(ctx context.Context) (int64, error)

	CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Thumbnail, error)
}

type thumbnailService struct {
	querier database.QuerierExtended
	cacher  cacher.Cacher
	fs      *fs.FileSystem
}

// DeleteUnused implements ThumbnailService.
func (s *thumbnailService) DeleteUnused(ctx context.Context) (int64, error) {
	return s.querier.DeleteUnusedThumbnails(ctx)
}

// CreateFromReader implements PosterService.
func (s *thumbnailService) CreateFromReader(ctx context.Context, arg params.CreateFromReaderImageParams) (*domain.Thumbnail, error) {
	hash, err := s.fs.CreateThumbnailImage(arg.Reader)
	if err != nil {
		return nil, err
	}
	images, err := s.CreateMany(ctx, params.InsertThumbnailParams{
		Items: []params.InsertThumbnailParamsItem{{
			Name: (hash + fs.ExtensionImage),
			Hash: hash,
		}},
	})
	if err != nil {
		return nil, err
	}
	return images[0], nil
}

// Count implements ThumbnailService.
func (s *thumbnailService) Count(ctx context.Context, arg params.CountThumbnailParams) (int64, error) {
	return s.querier.CountThumbnail(ctx, database.CountThumbnailParams(arg))
}

// Create implements ThumbnailService.
func (s *thumbnailService) Create(ctx context.Context, arg params.InsertThumbnailParamsItem) (*domain.Thumbnail, error) {
	row, err := s.querier.InsertThumbnail(ctx, database.InsertThumbnailParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &domain.Thumbnail{
		ID:   row.ID,
		Name: row.Name,
		Hash: row.Hash,
	}, nil
}

// CreateMany implements ThumbnailService.
func (s *thumbnailService) CreateMany(ctx context.Context, arg params.InsertThumbnailParams) ([]*domain.Thumbnail, error) {
	args := make([]database.InsertManyThumbnailParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyThumbnailParams(item)
	}

	bulk := s.querier.InsertManyThumbnail(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyThumbnailRow,
		*domain.Thumbnail,
	](bulk, func(row *database.InsertManyThumbnailRow) *domain.Thumbnail {
		return &domain.Thumbnail{
			ID:   row.ID,
			Name: row.Name,
			Hash: row.Hash,
		}
	})
}

// Delete implements ThumbnailService.
func (s *thumbnailService) Delete(ctx context.Context, arg params.DeleteThumbnailParams) (int64, error) {
	return s.querier.DeleteThumbnail(ctx, database.DeleteThumbnailParams(arg))
}

func mapThumbnailDatabaseToDomain(entities ...*database.Thumbnail) []*domain.Thumbnail {
	result := make([]*domain.Thumbnail, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Thumbnail{
			ID:   entity.ID,
			Name: entity.Name,
			Hash: entity.Hash,
		}
	}
	return result
}

// Filter implements ThumbnailService.
func (s *thumbnailService) Filter(ctx context.Context, arg params.FilterThumbnailParams) ([]*domain.Thumbnail, error) {
	rows, err := s.querier.FilterThumbnail(ctx, database.FilterThumbnailParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Thumbnail{}, nil
		}
		return nil, err
	}
	return mapThumbnailDatabaseToDomain(rows...), nil
}

// First implements ThumbnailService.
func (s *thumbnailService) First(ctx context.Context, arg params.FirstThumbnailParams) (*domain.Thumbnail, error) {
	row, err := s.querier.FirstThumbnail(ctx, database.FirstThumbnailParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapThumbnailDatabaseToDomain(row)[0], nil
}

// Update implements ThumbnailService.
func (s *thumbnailService) Update(ctx context.Context, arg params.UpdateThumbnailParams) (int64, error) {
	return s.querier.UpdateThumbnail(ctx, database.UpdateThumbnailParams(arg))
}

func NewThumbnailService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	fs *fs.FileSystem,
) ThumbnailService {
	return &thumbnailService{
		querier: querier,
		cacher:  cacher,
		fs:      fs,
	}
}
