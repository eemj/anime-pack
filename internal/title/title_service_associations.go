package title

import (
	"gitlab.com/eemj/anime-pack/internal/anime"
	titleanime "gitlab.com/eemj/anime-pack/internal/title/title_anime"
	titleepisode "gitlab.com/eemj/anime-pack/internal/title/title_episode"
	titlereview "gitlab.com/eemj/anime-pack/internal/title/title_review"
	"gitlab.com/eemj/anime-pack/internal/xdcc"
)

type TitleAssociations interface {
	TitleAnime() titleanime.TitleAnimeService
	TitleEpisode() titleepisode.TitleEpisodeService
	TitleReview() titlereview.TitleReviewService
	XDCC() xdcc.XDCCService
	Anime() anime.AnimeService
}

type titleAssociations struct {
	titleAnime   titleanime.TitleAnimeService
	titleEpisode titleepisode.TitleEpisodeService
	titleReview  titlereview.TitleReviewService
	xdcc         xdcc.XDCCService
	anime        anime.AnimeService
}

func (a *titleAssociations) TitleAnime() titleanime.TitleAnimeService       { return a.titleAnime }
func (a *titleAssociations) TitleEpisode() titleepisode.TitleEpisodeService { return a.titleEpisode }
func (a *titleAssociations) TitleReview() titlereview.TitleReviewService    { return a.titleReview }
func (a *titleAssociations) XDCC() xdcc.XDCCService                         { return a.xdcc }
func (a *titleAssociations) Anime() anime.AnimeService                      { return a.anime }

func NewTitleAssociations(
	titleAnime titleanime.TitleAnimeService,
	titleEpisode titleepisode.TitleEpisodeService,
	titleReview titlereview.TitleReviewService,
	xdcc xdcc.XDCCService,
	anime anime.AnimeService,
) TitleAssociations {
	return &titleAssociations{
		titleAnime:   titleAnime,
		titleEpisode: titleEpisode,
		titleReview:  titleReview,
		xdcc:         xdcc,
		anime:        anime,
	}
}
