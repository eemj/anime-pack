package title

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/apis/anilist"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.uber.org/zap"
)

func (s *titleService) Review(ctx context.Context, arg params.ReviewTitleParams) (*domain.Title, error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}

	var current *domain.Title
	titleContext, ok := ctx.Value(common.TitleContextKey).(*common.TitleContext)
	if ok {
		current = titleContext.Title
	} else {
		current, err = s.First(ctx, params.FirstTitleParams{
			ID: &arg.ID,
		})
		if err != nil {
			return nil, err
		}
	}
	current.Reviewed = arg.Reviewed

	titleAnimes, err := s.Associations().TitleAnime().Filter(ctx, params.FilterTitleAnimeParams{
		TitleID: &current.ID,
	})
	if err != nil {
		return nil, err
	}

	var anime *domain.Anime
	for _, titleAnime := range titleAnimes {
		if (arg.AniListID != nil && titleAnime.Anime.ID == *arg.AniListID) ||
			(arg.MALID != nil && titleAnime.Anime.IDMal == arg.MALID) {
			anime = &titleAnime.Anime
			break
		}
	}
	if anime == nil { // Attempt to see if this anime is in our database
		animes, err := s.associations.Anime().Filter(ctx, params.FilterAnimeParams{
			ID:       arg.AniListID,
			IDMal:    arg.MALID,
			Page:     1,
			Elements: 1,
		})
		if err != nil {
			return nil, err
		}
		if len(animes) > 0 {
			anime = animes[0]
		}
	}
	// If it's empty again, we'll have to retrieve it from third parties.
	if anime == nil {
		var media *anilist.Media
		if arg.AniListID != nil && *arg.AniListID > 0 {
			media, err = s.apis.AniList.GetMediaByID(ctx, uint(*arg.AniListID))
		} else if arg.MALID != nil && *arg.MALID > 0 {
			media, err = s.apis.AniList.GetMediaByIDMAL(ctx, uint(*arg.AniListID))
		}
		if err != nil {
			s.L(ctx).Error(
				"failed to retrieve from third party",
				zap.Int64p("anilist_id", arg.AniListID),
				zap.Int64p("my_anime_list_id", arg.MALID),
			)
			return nil, err
		}
		anime, err = s.associations.Anime().CreateFromThirdParty(ctx, *media.Model())
		if err != nil {
			return nil, err
		}
	}

	// Early exit.
	if !arg.Reviewed {
		rowsAffected, err := s.querier.UpdateTitle(ctx, database.UpdateTitleParams{
			ID:       current.ID,
			Reviewed: &arg.Reviewed,
		})
		if err != nil {
			return nil, err
		}
		if rowsAffected == 0 {
			s.L(ctx).Warn("unexpected `title` rows affected")
		}
		return current, nil
	}

	// Since it's reviewed, delete any residual anime that's
	// not part of our review and link the title anime, whilst creating preferences.
	isAssociated := false
	for _, titleAnime := range titleAnimes {
		if titleAnime.AnimeID == anime.ID && titleAnime.TitleID == current.ID {
			isAssociated = true // Skip this entry, we don't want to remove this associations
			continue
		}

		rowsAffected, err := s.associations.TitleAnime().Delete(ctx, params.DeleteTitleAnimeParams{
			TitleID: &titleAnime.TitleID,
			AnimeID: &titleAnime.AnimeID,
		},
		)
		if err != nil {
			return nil, err
		}
		if rowsAffected == 0 {
			s.L(ctx).Warn(
				"unexpected `title_anime` rows affected",
				zap.Int64("title_id", titleAnime.TitleID),
				zap.Int64("anime_id", titleAnime.AnimeID),
			)
		}

		// Before we delete the anime, check if it's associated to another title other
		// than this one..
		otherLinkedCount, err := s.associations.TitleAnime().Count(ctx, params.CountTitleAnimeParams{
			AnimeID: &titleAnime.AnimeID,
		})
		if err != nil {
			return nil, err
		}
		if otherLinkedCount == 0 {
			rowsAffected, err := s.associations.Anime().Delete(ctx, params.DeleteAnimeParams{ID: titleAnime.AnimeID})
			if err != nil {
				return nil, err
			}
			if rowsAffected == 0 {
				s.L(ctx).Warn(
					"unexpected `anime` rows affected",
					zap.Int64("anime_id", titleAnime.AnimeID),
				)
			}
		}
	}

	if !isAssociated {
		titleAnimes, err = s.associations.TitleAnime().CreateMany(ctx, params.InsertTitleAnimeParams{
			Items: []params.InsertTitleAnimeParamsItem{{
				TitleID: current.ID,
				AnimeID: anime.ID,
			}},
		})
		if err != nil {
			return nil, err
		}
		s.L(ctx).Debug("review created `title_anime` entries", zap.Int("length", len(titleAnimes)))
	}

	// Since this anime is re
	_, err = s.associations.Anime().Associations().Preferences().Create(ctx, params.InsertPreferencesParams{
		AnimeID: anime.ID,
	})
	if err != nil {
		return nil, err
	}

	_, err = s.querier.UpdateTitle(ctx, database.UpdateTitleParams{
		ID:       current.ID,
		Reviewed: &arg.Reviewed,
	})
	if err != nil {
		return nil, err
	}

	// Bust the cache for this title.
	err = s.cacher.DeleteMany(
		ctx,
		cachekeys.Title(&current.ID, nil, nil, nil),
		cachekeys.Title(nil, &current.Name, current.SeasonNumber, nil),
		cachekeys.Title(nil, &current.Name, current.SeasonNumber, current.Year),
	)
	if err != nil {
		return nil, err
	}

	return current, nil
}

func (s *titleService) LinkWithAnime(
	ctx context.Context,
	title *domain.Title,
	animes []*domain.Anime,
) (titleAnimes []*domain.TitleAnime, err error) {
	titleAnimeItems := make([]params.InsertTitleAnimeParamsItem, len(animes))
	for index, anime := range animes {
		titleAnimeItems[index] = params.InsertTitleAnimeParamsItem{
			TitleID: title.ID,
			AnimeID: anime.ID,
		}
	}
	if len(titleAnimeItems) > 0 {
		titleAnimes, err = s.associations.TitleAnime().CreateMany(ctx, params.InsertTitleAnimeParams{
			Items: titleAnimeItems,
		})
		if err != nil {
			return nil, err
		}
	}
	rowsAffected, err := s.Update(ctx, params.UpdateTitleParams{
		ID:       title.ID,
		Reviewed: &title.Reviewed,
	})
	if err != nil {
		return nil, err
	}
	if rowsAffected == 0 {
		s.L(ctx).Warn(
			"unexpected `rows_affected` for title anime link",
			zap.Int64("rows_affected", rowsAffected),
			zap.Any("title", title),
		)
	}
	return titleAnimes, nil
}
