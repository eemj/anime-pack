package titleepisode

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type TitleEpisodeService interface {
	Create(ctx context.Context, arg params.InsertTitleEpisodeParamsItem) (*domain.TitleEpisode, error)
	CreateMany(ctx context.Context, arg params.InsertTitleEpisodeParams) ([]*domain.TitleEpisode, error)
	First(ctx context.Context, arg params.FirstTitleEpisodeParams) (*domain.TitleEpisode, error)
	Filter(ctx context.Context, arg params.FilterTitleEpisodeParams) ([]*domain.TitleEpisode, error)
	Count(ctx context.Context, arg params.CountTitleEpisodeParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteTitleEpisodeParams) (int64, error)
	SoftDelete(ctx context.Context, arg params.SoftDeleteTitleEpisodeParams) (int64, error)
}

type titleEpisodeService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements TitleEpisodeService.
func (s *titleEpisodeService) Count(ctx context.Context, arg params.CountTitleEpisodeParams) (int64, error) {
	count, err := s.querier.CountTitleEpisode(ctx, database.CountTitleEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return count, nil
}

// Create implements TitleEpisodeService.
func (s *titleEpisodeService) Create(ctx context.Context, arg params.InsertTitleEpisodeParamsItem) (*domain.TitleEpisode, error) {
	cacheKey := cachekeys.TitleEpisode(nil, &arg.TitleID, &arg.EpisodeID)
	if cacheKey != "" {
		cacheValue, err := encoded.Get[*domain.TitleEpisode](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cacheValue != nil {
			return cacheValue, nil
		}
	}
	row, err := s.querier.InsertTitleEpisode(ctx, database.InsertTitleEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.TitleEpisode{
		ID:        row.ID,
		EpisodeID: row.EpisodeID,
		TitleID:   row.TitleID,
		CreatedAt: row.CreatedAt,
		DeletedAt: row.DeletedAt,
		IsDeleted: row.IsDeleted,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// CreateMany implements TitleEpisodeService.
func (s *titleEpisodeService) CreateMany(ctx context.Context, arg params.InsertTitleEpisodeParams) ([]*domain.TitleEpisode, error) {
	args := make([]database.InsertManyTitleEpisodeParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyTitleEpisodeParams(item)
	}

	results := make([]*domain.TitleEpisode, 0)
	bulk := s.querier.InsertManyTitleEpisode(ctx, args)
	defer bulk.Close()
	bulk.Query(func(_ int, iter []*database.InsertManyTitleEpisodeRow, err error) {
		if err != nil {
			return
		}

		for _, row := range iter {
			results = append(results, &domain.TitleEpisode{
				ID:        row.ID,
				EpisodeID: row.EpisodeID,
				TitleID:   row.TitleID,
				CreatedAt: row.CreatedAt,
				DeletedAt: row.DeletedAt,
				IsDeleted: row.IsDeleted,
			})
		}
	})

	return results, nil
}

// Delete implements TitleEpisodeService.
func (s *titleEpisodeService) Delete(ctx context.Context, arg params.DeleteTitleEpisodeParams) (int64, error) {
	return s.querier.DeleteTitleEpisode(ctx, database.DeleteTitleEpisodeParams(arg))
}

// SoftDelete implements TitleEpisodeService.
func (s *titleEpisodeService) SoftDelete(ctx context.Context, arg params.SoftDeleteTitleEpisodeParams) (int64, error) {
	return s.querier.SoftDeleteTitleEpisode(ctx, database.SoftDeleteTitleEpisodeParams(arg))
}

// Filter implements TitleEpisodeService.
func (s *titleEpisodeService) Filter(ctx context.Context, arg params.FilterTitleEpisodeParams) ([]*domain.TitleEpisode, error) {
	rows, err := s.querier.FilterTitleEpisode(ctx, database.FilterTitleEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.TitleEpisode{}, nil
		}
		return nil, err
	}
	results := make([]*domain.TitleEpisode, len(rows))
	for index, row := range rows {
		results[index] = domain.TitleEpisodeFlat(*row).Unflatten()
	}
	return results, nil
}

// First implements TitleEpisodeService.
func (s *titleEpisodeService) First(ctx context.Context, arg params.FirstTitleEpisodeParams) (*domain.TitleEpisode, error) {
	row, err := s.querier.FirstTitleEpisode(ctx, database.FirstTitleEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return domain.TitleEpisodeFlat(*row).Unflatten(), nil
}

func NewTitleEpisodeService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) TitleEpisodeService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &titleEpisodeService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
