package title

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.uber.org/zap"
)

func (s *titleService) deleteAssociations(ctx context.Context, titleID int64) (hardDelete bool, title *domain.Title, err error) {
	titleAnimes, err := s.associations.TitleAnime().Filter(ctx, params.FilterTitleAnimeParams{
		TitleID: &titleID,
	})
	if err != nil {
		return false, nil, err
	}
	if len(titleAnimes) >= 1 {
		title = &titleAnimes[0].Title
		rowsAffected, err := s.associations.TitleAnime().Delete(ctx, params.DeleteTitleAnimeParams{
			TitleID: &titleID,
		})
		if err != nil {
			return false, nil, err
		}
		if rowsAffected == 0 {
			s.L(ctx).Warn(
				"rows affected 0 during `title_anime` delete",
				zap.Int64("title_id", titleID),
			)
		}
	}

	// We first need to remove XDCC entries, because `xdcc` has a foreign key
	// with the `title_episode` records.
	xdccs, err := s.associations.XDCC().Filter(ctx, params.FilterXDCCParams{
		TitleID: &titleID,
	})
	if err != nil {
		return false, nil, err
	}
	if len(xdccs) > 0 {
		xdccIDs := make([]int64, len(xdccs))
		for index, xdcc := range xdccs {
			xdccIDs[index] = xdcc.ID
		}

		rowsAffected, err := s.associations.XDCC().Delete(ctx, params.DeleteXDCCParams{
			IDs: xdccIDs,
		})
		if err != nil {
			return false, nil, err
		}
		if rowsAffected == 0 {
			s.L(ctx).Warn(
				"rows affected 0 during `xdcc` delete",
				zap.Int64("title_id", titleID),
			)
		}
		hardDelete = rowsAffected > 0
	}

	// Instead of retrieving the `title_episodes`, we'll live on the edge and delete all by `title_id`.
	_, err = s.associations.TitleEpisode().Delete(ctx, params.DeleteTitleEpisodeParams{
		TitleID: &titleID,
	})
	if err != nil {
		return false, nil, err
	}

	return hardDelete, title, nil
}

func (s *titleService) Delete(ctx context.Context, arg params.DeleteTitleParams) (int64, error) {
	hardDelete, title, err := s.deleteAssociations(ctx, arg.ID)
	if err != nil {
		return 0, err
	}
	if title == nil {
		// Retrieve, we need this for the cache cleanup.
		title, err = s.First(ctx, params.FirstTitleParams{ID: &arg.ID})
		if err != nil {
			return 0, nil
		}
	}
	var rowsAffected int64
	if hardDelete {
		rowsAffected, err = s.querier.DeleteTitle(ctx, database.DeleteTitleParams{ID: arg.ID})
	} else {
		rowsAffected, err = s.querier.SoftDeleteTitle(ctx, database.SoftDeleteTitleParams{
			IDs: []int64{arg.ID},
		})
	}
	if err != nil {
		return 0, err
	}
	if rowsAffected == 0 {
		s.L(ctx).Warn(
			"rows affected 0 during `title` delete",
			zap.Bool("hard_delete", hardDelete),
			zap.Int64("id", arg.ID),
		)
	}
	// Remove the possible keys where this title might be stored on.
	err = s.cacher.DeleteMany(
		ctx,
		cachekeys.Title(&title.ID, nil, nil, nil),
		cachekeys.Title(nil, &title.Name, title.SeasonNumber, nil),
		cachekeys.Title(nil, &title.Name, title.SeasonNumber, title.Year),
	)
	if err != nil {
		return 0, err
	}
	return rowsAffected, nil
}
