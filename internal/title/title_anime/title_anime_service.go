package titleanime

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type TitleAnimeService interface {
	CreateMany(ctx context.Context, arg params.InsertTitleAnimeParams) ([]*domain.TitleAnime, error)
	First(ctx context.Context, arg params.FirstTitleAnimeParams) (*domain.TitleAnime, error)
	Filter(ctx context.Context, arg params.FilterTitleAnimeParams) ([]*domain.TitleAnime, error)
	Count(ctx context.Context, arg params.CountTitleAnimeParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteTitleAnimeParams) (int64, error)
}

type titleAnimeService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements TitleAnimeService.
func (s *titleAnimeService) Count(ctx context.Context, arg params.CountTitleAnimeParams) (int64, error) {
	count, err := s.querier.CountTitleAnime(ctx, database.CountTitleAnimeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return count, nil
}

// CreateMany implements TitleAnimeService.
func (s *titleAnimeService) CreateMany(ctx context.Context, arg params.InsertTitleAnimeParams) ([]*domain.TitleAnime, error) {
	args := make([]database.InsertTitleAnimeParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertTitleAnimeParams{
			TitleID: item.TitleID,
			AnimeID: item.AnimeID,
		}
	}

	results := make([]*domain.TitleAnime, 0)
	bulk := s.querier.InsertTitleAnime(ctx, args)
	defer bulk.Close()
	bulk.Query(func(_ int, itar []*database.InsertTitleAnimeRow, err error) {
		if err != nil {
			return
		}

		for _, row := range itar {
			results = append(results, &domain.TitleAnime{
				TitleID:   row.TitleID,
				AnimeID:   row.AnimeID,
				CreatedAt: row.CreatedAt,
			})
		}
	})

	return results, nil
}

// Delete implements TitleAnimeService.
func (s *titleAnimeService) Delete(ctx context.Context, arg params.DeleteTitleAnimeParams) (int64, error) {
	return s.querier.DeleteTitleAnime(ctx, database.DeleteTitleAnimeParams(arg))
}

// Filter implements TitleAnimeService.
func (s *titleAnimeService) Filter(ctx context.Context, arg params.FilterTitleAnimeParams) ([]*domain.TitleAnime, error) {
	rows, err := s.querier.FilterTitleAnime(ctx, database.FilterTitleAnimeParams{
		TitleID: arg.TitleID,
		AnimeID: arg.AnimeID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.TitleAnime{}, nil
		}
		return nil, err
	}
	results := make([]*domain.TitleAnime, len(rows))
	for index, row := range rows {
		results[index] = domain.TitleAnimeFlat(*row).Unflatten()
	}
	return results, nil
}

// First implements TitleAnimeService.
func (s *titleAnimeService) First(ctx context.Context, arg params.FirstTitleAnimeParams) (*domain.TitleAnime, error) {
	row, err := s.querier.FirstTitleAnime(ctx, database.FirstTitleAnimeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return domain.TitleAnimeFlat(*row).Unflatten(), nil
}

func NewTitleAnimeService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) TitleAnimeService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &titleAnimeService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
