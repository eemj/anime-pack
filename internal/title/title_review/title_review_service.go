package titlereview

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pager"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type TitleReviewService interface {
	Filter(ctx context.Context, arg params.FilterTitleReviewParams) (*pager.PageSet[domain.TitleReviewItems], error)
}

type titleReviewService struct {
	querier database.QuerierExtended
}

func (s *titleReviewService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.TitleReviewServiceName)
}

// Filter implements TitleReviewService.
func (s *titleReviewService) Filter(ctx context.Context, arg params.FilterTitleReviewParams) (*pager.PageSet[domain.TitleReviewItems], error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}

	count, err := s.querier.CountTitleReview(ctx, database.CountTitleReviewParams{
		Reviewed:  arg.Reviewed,
		IsDeleted: arg.IsDeleted,
	})
	if err != nil && err != pgx.ErrNoRows {
		return nil, err
	}

	if count == 0 {
		return &pager.PageSet[domain.TitleReviewItems]{
			Page:     arg.Page,
			Elements: arg.Elements,
			Count:    0,
			Data:     domain.TitleReviewItems{},
		}, nil
	}
	offset, limit := pager.CalculateLimitOffset(arg.Page, arg.Elements)
	rows, err := s.querier.FilterTitleReview(ctx, database.FilterTitleReviewParams{
		Reviewed:    arg.Reviewed,
		IsDeleted:   arg.IsDeleted,
		OrderBy:     arg.OrderBy,
		OrderByDesc: enums.OrderByDirectionIsDescending(arg.OrderByDesc),
		Offset:      offset,
		Limit:       limit,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return &pager.PageSet[domain.TitleReviewItems]{
				Page:     arg.Page,
				Elements: arg.Elements,
				Count:    0,
				Data:     domain.TitleReviewItems{},
			}, nil
		}
		return nil, err
	}
	flatItems := make(domain.TitleReviewFlatItems, len(rows))
	for index, row := range rows {
		flatItems[index] = domain.TitleReviewFlat(*row)
	}
	result := flatItems.Unflatten()
	return &pager.PageSet[domain.TitleReviewItems]{
		Page:     arg.Page,
		Elements: arg.Elements,
		Count:    count,
		Data:     result,
	}, nil
}

func NewTitleReviewService(
	querier database.QuerierExtended,
) TitleReviewService {
	return &titleReviewService{
		querier: querier,
	}
}
