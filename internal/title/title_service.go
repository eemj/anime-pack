package title

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/apis"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type TitleService interface {
	Create(ctx context.Context, arg params.InsertTitleParamsItem) (*domain.Title, error)
	CreateMany(ctx context.Context, arg params.InsertTitleParams) ([]*domain.Title, error)
	First(ctx context.Context, arg params.FirstTitleParams) (*domain.Title, error)
	Filter(ctx context.Context, arg params.FilterTitleParams) ([]*domain.Title, error)
	Count(ctx context.Context, arg params.CountTitleParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateTitleParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteTitleParams) (int64, error)
	SoftDelete(ctx context.Context, arg params.SoftDeleteTitleParams) (int64, error)

	Review(ctx context.Context, arg params.ReviewTitleParams) (*domain.Title, error)
	LinkWithAnime(ctx context.Context, title *domain.Title, animes []*domain.Anime) ([]*domain.TitleAnime, error)
	DeleteUnused(ctx context.Context) (int64, error)

	Associations() TitleAssociations
}

type titleService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
	apis    *apis.APIS

	associations TitleAssociations
}

// DeleteUnused implements TitleService.
func (s *titleService) DeleteUnused(ctx context.Context) (int64, error) {
	return s.querier.DeleteUnusedTitles(ctx)
}

func (s *titleService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.TitleAnimeServiceName)
}

// Count implements TitleService.
func (s *titleService) Count(ctx context.Context, arg params.CountTitleParams) (int64, error) {
	var (
		count int64
		err   error
	)
	if arg.ID != nil {
		count, err = s.querier.CountTitleById(ctx, database.CountTitleByIdParams{
			ID:        *arg.ID,
			Reviewed:  arg.Reviewed,
			IsDeleted: arg.IsDeleted,
		})
	} else if arg.Name != nil {
		count, err = s.querier.CountTitleByNameSeasonNumberYear(ctx, database.CountTitleByNameSeasonNumberYearParams{
			Name:         *arg.Name,
			SeasonNumber: arg.SeasonNumber,
			Year:         arg.Year,
			Reviewed:     arg.Reviewed,
			IsDeleted:    arg.IsDeleted,
		})
	} else {
		count, err = s.querier.CountTitleByOthers(ctx, database.CountTitleByOthersParams{Reviewed: arg.Reviewed, IsDeleted: arg.IsDeleted})
	}
	if err != nil {
		if err == pgx.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return count, nil
}

// Create implements TitleService.
func (s *titleService) Create(ctx context.Context, arg params.InsertTitleParamsItem) (*domain.Title, error) {
	cacheKey := cachekeys.Title(nil, &arg.Name, arg.SeasonNumber, arg.Year)
	if cacheKey != "" {
		cachedValue, err := encoded.Get[*domain.Title](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cachedValue != nil {
			return cachedValue, nil
		}
	}
	row, err := s.querier.InsertTitle(ctx, database.InsertTitleParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.Title{
		ID:           row.ID,
		Name:         row.Name,
		SeasonNumber: row.SeasonNumber,
		Year:         row.Year,
		Reviewed:     row.Reviewed,
		CreatedAt:    row.CreatedAt,
		UpdatedAt:    row.UpdatedAt,
		DeletedAt:    row.DeletedAt,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// CreateMany implements TitleService.
func (s *titleService) CreateMany(ctx context.Context, arg params.InsertTitleParams) ([]*domain.Title, error) {
	args := make([]database.InsertManyTitleParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyTitleParams{
			Name:         item.Name,
			SeasonNumber: item.SeasonNumber,
			Year:         item.Year,
			Reviewed:     item.Reviewed,
		}
	}

	bulk := s.querier.InsertManyTitle(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap(bulk, func(row *database.InsertManyTitleRow) *domain.Title {
		return &domain.Title{
			ID:           row.ID,
			Name:         row.Name,
			SeasonNumber: row.SeasonNumber,
			Year:         row.Year,
			Reviewed:     row.Reviewed,
			CreatedAt:    row.CreatedAt,
			UpdatedAt:    row.UpdatedAt,
			DeletedAt:    row.DeletedAt,
		}
	})
}

// Filter implements TitleService.
func (s *titleService) Filter(ctx context.Context, arg params.FilterTitleParams) ([]*domain.Title, error) {
	var (
		rows []*database.Title
		err  error
	)
	if arg.ID != nil {
		rows, err = s.querier.FilterTitleById(ctx, database.FilterTitleByIdParams{
			ID:        *arg.ID,
			Reviewed:  arg.Reviewed,
			IsDeleted: arg.IsDeleted,
		})
	} else if arg.Name != nil {
		rows, err = s.querier.FilterTitleByNameSeasonNumberYear(ctx, database.FilterTitleByNameSeasonNumberYearParams{
			Name:         *arg.Name,
			SeasonNumber: arg.SeasonNumber,
			Year:         arg.Year,
			Reviewed:     arg.Reviewed,
			IsDeleted:    arg.IsDeleted,
		})
	} else {
		rows, err = s.querier.FilterTitleByOthers(ctx, database.FilterTitleByOthersParams{Reviewed: arg.Reviewed, IsDeleted: arg.IsDeleted})
	}
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapTitleDatabaseToDomain(rows...), nil
}

func mapTitleDatabaseToDomain(entities ...*database.Title) []*domain.Title {
	result := make([]*domain.Title, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Title{
			ID:           entity.ID,
			Name:         entity.Name,
			SeasonNumber: entity.SeasonNumber,
			Year:         entity.Year,
			Reviewed:     entity.Reviewed,
			CreatedAt:    entity.CreatedAt,
			UpdatedAt:    entity.UpdatedAt,
			DeletedAt:    entity.DeletedAt,
			IsDeleted:    entity.IsDeleted,
		}
	}
	return result
}

// First implements TitleService.
func (s *titleService) First(ctx context.Context, arg params.FirstTitleParams) (*domain.Title, error) {
	var (
		row *database.Title
		err error
	)
	if arg.ID != nil {
		row, err = s.querier.FirstTitleById(ctx, database.FirstTitleByIdParams{
			ID:        *arg.ID,
			Reviewed:  arg.Reviewed,
			IsDeleted: arg.IsDeleted,
		})
	} else if arg.Name != nil {
		row, err = s.querier.FirstTitleByNameSeasonNumberYear(ctx, database.FirstTitleByNameSeasonNumberYearParams{
			Name:         *arg.Name,
			SeasonNumber: arg.SeasonNumber,
			Year:         arg.Year,
			Reviewed:     arg.Reviewed,
			IsDeleted:    arg.IsDeleted,
		})
	} else {
		row, err = s.querier.FirstTitleByOthers(ctx, database.FirstTitleByOthersParams{Reviewed: arg.Reviewed, IsDeleted: arg.IsDeleted})
	}
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := mapTitleDatabaseToDomain(row)[0]
	return result, nil
}

// SoftDelete implements TitleService.
func (s *titleService) SoftDelete(ctx context.Context, arg params.SoftDeleteTitleParams) (int64, error) {
	return s.querier.SoftDeleteTitle(ctx, database.SoftDeleteTitleParams(arg))
}

// Associations implements TitleService.
func (s *titleService) Associations() TitleAssociations { return s.associations }

func NewTitleService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations TitleAssociations,
) TitleService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &titleService{
		querier: querier,
		cacher:  encodedCacher,
		apis:    apis.New(),

		associations: associations,
	}
}
