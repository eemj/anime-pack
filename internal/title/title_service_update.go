package title

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

// Update implements TitleService.
func (s *titleService) Update(ctx context.Context, arg params.UpdateTitleParams) (int64, error) {
	return s.querier.UpdateTitle(ctx, database.UpdateTitleParams{
		ID:       arg.ID,
		Reviewed: arg.Reviewed,
	})
}
