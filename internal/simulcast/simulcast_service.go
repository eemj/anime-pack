package simulcast

import (
	"context"
	"strings"

	"gitlab.com/eemj/anime-pack/internal/apis/subsplease"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type SimulcastService interface {
	Refresh(ctx context.Context) error
	Clear(ctx context.Context) error
}

type simulcastService struct {
	querier      database.QuerierExtended
	cacher       *encoded.EncodedCacher
	associations SimulcastAssociations
}

func (s *simulcastService) L(ctx context.Context) *zap.Logger {
	return log.WithTrace(ctx).Named(common.SimulcastServiceName)
}

// Clear implements SimulcastService.
func (s *simulcastService) Clear(ctx context.Context) error {
	rowsAffected, err := s.querier.TruncateSimulcast(ctx)
	if err != nil {
		return err
	}
	var logLevel zapcore.Level
	if rowsAffected == 0 {
		logLevel = zapcore.WarnLevel
	} else {
		logLevel = zapcore.InfoLevel
	}
	s.L(ctx).Log(
		logLevel,
		"truncate simulcast",
		zap.Int64("rows_affected", rowsAffected),
	)
	return nil
}

// Refresh implements SimulcastService.
func (s *simulcastService) Refresh(ctx context.Context) error {
	schedules, err := subsplease.GetSchedule(ctx)
	if err != nil {
		return err
	}

	entries := make([]database.Simulcast, 0)

	for _, schedule := range schedules {
		for _, release := range schedule.Releases {
			// FIXME(eemj): This is a huge hack, but for SubsPlease they are
			// consistent with their filenames so we're going to hardcode a title
			// format because our parser separates the title and the season number..
			predictedFilename := "[SubsPlease] " + strings.TrimSpace(release.Title) + " - 01 [1080p].mp4"

			entry := groups.Parse(predictedFilename)
			if entry == nil {
				s.L(ctx).Warn("group parse failed", zap.String("filename", predictedFilename))
				continue
			}

			title, err := s.associations.Title().First(ctx, params.FirstTitleParams{
				Name:         &entry.Title,
				SeasonNumber: entry.SeasonNumber,
				Year:         entry.Year,
			})
			if err != nil {
				return err
			}
			if title == nil {
				var titleAnimes []*domain.TitleAnime
				title, titleAnimes, err = s.associations.Library().Match(ctx, params.MatchLibraryParams{
					TitleName:         entry.Title,
					TitleSeasonNumber: entry.SeasonNumber,
					TitleYear:         entry.Year,
				})
				if err != nil {
					return err
				}

				s.L(ctx).Info(
					"created title",
					zap.String("name", title.Name),
					zap.Int64("id", title.ID),
					zap.Bool("reviewed", title.Reviewed),
					zap.Int("anime_count", len(titleAnimes)),
				)
			}

			hour, minute, second := release.Time.Clock()

			entries = append(entries, database.Simulcast{
				TitleID: title.ID,
				Weekday: int64(schedule.Weekday),
				Hour:    int16(hour),
				Minute:  int16(minute),
				Second:  int16(second),
			})
		}
	}

	if len(entries) == 0 {
		return nil
	}

	err = s.querier.ReplaceAllSimulcast(ctx, entries)
	if err != nil {
		return err
	}
	return nil
}

func NewSimulcastService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations SimulcastAssociations,
) SimulcastService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &simulcastService{
		querier:      querier,
		cacher:       encodedCacher,
		associations: associations,
	}
}
