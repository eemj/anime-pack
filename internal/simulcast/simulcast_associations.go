package simulcast

import (
	"gitlab.com/eemj/anime-pack/internal/library"
	"gitlab.com/eemj/anime-pack/internal/title"
)

type SimulcastAssociations interface {
	Library() library.LibraryService
	Title() title.TitleService
}

type simulcastAssociations struct {
	library library.LibraryService
	title   title.TitleService
}

func (s *simulcastAssociations) Library() library.LibraryService { return s.library }
func (s *simulcastAssociations) Title() title.TitleService       { return s.title }

func NewSimulcastAssociations(
	library library.LibraryService,
	title title.TitleService,
) SimulcastAssociations {
	return &simulcastAssociations{
		library: library,
		title:   title,
	}
}
