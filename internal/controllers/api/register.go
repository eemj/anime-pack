package controller

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/base"
)

// @title						Anime Pack API
// @version					1.0.0
// @description				An anime streaming/hoarding platform.
//
// @contact.name				eemj
// @contact.url				https://gitlab.com/eemj/anime-pack/-/issues
//
// @license.name				MIT
// @license.url				https://opensource.org/licenses/MIT
//
// @securityDefinitions.basic	BasicAuth
func Register(
	echo *echo.Echo,
	global base.Global,
	apis ...base.API,
) (err error) {
	for _, api := range apis {
		group := echo.Group(fmt.Sprintf(
			"/api/v%d",
			api.Version(),
		))

		if err = api.Register(group); err != nil {
			return
		}
	}

	return global.Register(echo)
}
