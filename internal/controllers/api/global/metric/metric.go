package metric

import (
	"github.com/labstack/echo/v4"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/eemj/anime-pack/internal/metrics"
)

type MetricController interface {
	// Group represents the `/metrics` endpoint
	Group(next echo.HandlerFunc) echo.HandlerFunc
}

type metricController struct {
	metrics *metrics.Metrics
}

func NewMetric(metrics *metrics.Metrics) MetricController {
	return &metricController{metrics: metrics}
}

// Get implements MetricController
func (m *metricController) Group(next echo.HandlerFunc) echo.HandlerFunc {
	return echo.WrapHandler(promhttp.HandlerFor(
		m.metrics.Gatherer(),
		promhttp.HandlerOpts{},
	))
}
