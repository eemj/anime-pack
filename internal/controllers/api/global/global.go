package global

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/base"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/global/metric"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/global/video"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/metrics"
)

type global struct {
	base.Global

	services lifecycle.ServiceLifecycle

	video  video.VideoController
	metric metric.MetricController
}

func New(
	services lifecycle.ServiceLifecycle,
	metrics *metrics.Metrics,
) base.Global {
	return &global{
		services: services,

		video:  video.NewVideo(services.Video()),
		metric: metric.NewMetric(metrics),
	}
}

func (g *global) Register(e *echo.Echo) (err error) {
	e.Group((path.PrefixVideo + ":path"), g.video.Group)
	e.Group("/metrics", g.metric.Group)

	return
}
