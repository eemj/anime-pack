package video

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	"gitlab.com/eemj/anime-pack/internal/video"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

type VideoController interface {
	Group(next echo.HandlerFunc) echo.HandlerFunc
}

type videoController struct {
	video video.VideoService
}

func NewVideo(video video.VideoService) VideoController {
	return &videoController{video}
}

func (v *videoController) Group(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		switch c.Request().Method {
		case http.MethodGet, http.MethodPost:
			path := c.Param("path")
			if utils.IsEmpty(path) {
				return response.NewError(c, apperror.ErrVideoPathRequired)
			}

			path, err := v.video.Location(
				c.Request().Context(),
				params.LocateVideoParams{Path: path},
			)
			if err != nil {
				return response.NewError(c, err)
			}
			return c.File(path)
		default:
			return response.NewErrorCode(c, echo.ErrMethodNotAllowed, http.StatusMethodNotAllowed)
		}
	}
}
