package season

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animeseason "gitlab.com/eemj/anime-pack/internal/anime/anime_season"
	_ "gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type SeasonController interface {
	GetAll(c echo.Context) (err error)
	GetAllYears(c echo.Context) (err error)
}

type seasonController struct {
	animeSeasonSvc animeseason.AnimeSeasonService
}

func NewSeasonController(
	animeSeasonSvc animeseason.AnimeSeasonService,
) SeasonController {
	return &seasonController{animeSeasonSvc: animeSeasonSvc}
}

// GetAllSeasons godoc
//
//	@Summary		Get all available seasons.
//	@Description	Get all available seasons.
//	@Tags			season
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]enums.AnimeSeason]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/season [get]
func (f *seasonController) GetAll(c echo.Context) (err error) {
	seasons, err := f.animeSeasonSvc.List(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, seasons)
}

// GetAllSeasonYears godoc
//
//	@Summary		Get all available season years.
//	@Description	Get all available season years.
//	@Tags			season
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]int64]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/season-year [get]
func (f *seasonController) GetAllYears(c echo.Context) (err error) {
	seasonYears, err := f.animeSeasonSvc.ListYears(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, seasonYears)
}
