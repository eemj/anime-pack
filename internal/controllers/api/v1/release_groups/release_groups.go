package releasegroups

import (
	"net/http"

	"github.com/labstack/echo/v4"
	releasegroup "gitlab.com/eemj/anime-pack/internal/release_group"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type ReleaseGroupController interface {
	GetAll(c echo.Context) (err error)
}

type releaseGroupController struct {
	releaseGroupSvc releasegroup.ReleaseGroupService
}

// GetAllReleaseGroups godoc
//
//	@Summary		Get all available release groups.
//	@Description	Get all available release groups.
//	@Tags			release-group
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]domain.ReleaseGroup]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/release-group [get]
func (r *releaseGroupController) GetAll(c echo.Context) error {
	releaseGroups, err := r.releaseGroupSvc.Available(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, releaseGroups)
}

func NewReleaseGroupController(
	releaseGroupSvc releasegroup.ReleaseGroupService,
) ReleaseGroupController {
	return &releaseGroupController{releaseGroupSvc: releaseGroupSvc}
}
