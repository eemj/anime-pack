package title

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	"gitlab.com/eemj/anime-pack/internal/title"
)

type TitleController interface {
	Middleware() echo.MiddlewareFunc

	Get(c echo.Context) (err error)
	GetAll(c echo.Context) (err error)
	Update(c echo.Context) (err error)
	Delete(c echo.Context) (err error)
}

type titleController struct {
	titleSvc title.TitleService
}

func NewTitleController(
	titleSvc title.TitleService,
) TitleController {
	return &titleController{
		titleSvc: titleSvc,
	}
}

func (t *titleController) Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			param := c.Param("id")
			if param == "" {
				return next(c)
			}

			id, err := strconv.ParseInt(param, 10, 64)
			if err != nil {
				return response.NewError(c, apperror.ErrIdentifierMustBeNumeric)
			}

			title, err := t.titleSvc.First(c.Request().Context(), params.FirstTitleParams{
				ID: &id,
			})
			if err != nil {
				return response.NewError(c, err)
			}
			if title == nil {
				return response.NewError(c, apperror.ErrTitleNotFound)
			}

			newCtx := &TitleContext{Context: c, Title: *title}
			return next(newCtx)
		}
	}
}

// GetTitle godoc
//
//	@Summary		Get a specific title.
//	@Description	Get a specific title.
//	@Tags			title
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Title ID"
//	@Success		200	{object}	response.DataResponse[domain.Title]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/title/{id} [get]
func (t *titleController) Get(c echo.Context) (err error) {
	if ctx, ok := c.(*TitleContext); ok {
		return response.NewData(c, http.StatusOK, ctx.Title)
	}
	return response.NewError(c, apperror.ErrWrongContextDataType)
}

// GetAllTitle godoc
//
//	@Summary		Get all titles.
//	@Description	Get all titles.
//	@Tags			title
//	@Accept			json
//	@Produce		json
//	@Param			reviewed	query		bool	false	"Reviewed only"
//	@Param			isDeleted	query		int		false	"Soft deleted entries will be returned"
//	@Param			orderBy		query		string	false	"Ordering should be done in a descending order"	Enums(id, createdAt)
//	@Param			direction	query		string	false	"Ordering should be done in a descending order"	Enums(asc, desc)
//	@Param			page		query		int		false	"Response page"									default(1)
//	@Param			elements	query		int		false	"Response number of elements"					default(20)
//	@Success		200			{object}	response.DataResponse[[]domain.Title]
//	@Failure		404			{object}	response.ErrorResponse
//	@Failure		500			{object}	response.ErrorResponse
//	@Router			/api/v1/title [get]
func (t *titleController) GetAll(c echo.Context) error {
	var (
		filter params.FilterTitleReviewParams
		err    = c.Bind(&filter)
	)
	if err != nil {
		return response.NewError(c, err)
	}

	paginatedTitles, err := t.titleSvc.Associations().
		TitleReview().
		Filter(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewPageData(c, http.StatusOK, paginatedTitles)
}

// UpdateTitle godoc
//
//	@Summary		Update a specific title.
//	@Description	Update a specific title.
//	@Tags			title
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Title ID"
//	@Success		200	{object}	response.DataResponse[domain.Title]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/title/{id} [patch]
func (t *titleController) Update(c echo.Context) error {
	ctx, ok := c.(*TitleContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	var (
		filter params.ReviewTitleParams
		err    = c.Bind(&filter)
	)
	if err != nil {
		return response.NewError(c, err)
	}
	filter.ID = ctx.Title.ID

	title, err := t.titleSvc.Review(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewData(
		c,
		http.StatusAccepted,
		title,
	)
}

// DeleteTitle godoc
//
//	@Summary		Delete a specific title.
//	@Description	Delete a specific title.
//	@Tags			title
//	@Accept			json
//	@Produce		json
//	@Param			id	path	string	true	"Title ID"
//	@Success		202	""
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/title/{id} [delete]
func (t *titleController) Delete(c echo.Context) (err error) {
	ctx, ok := c.(*TitleContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	_, err = t.titleSvc.Delete(
		c.Request().Context(),
		params.DeleteTitleParams{ID: ctx.Title.ID},
	)
	if err != nil {
		return response.NewError(c, err)
	}
	return c.NoContent(http.StatusAccepted)
}
