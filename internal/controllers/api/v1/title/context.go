package title

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/domain"
)

type TitleContext struct {
	echo.Context
	Title domain.Title
}
