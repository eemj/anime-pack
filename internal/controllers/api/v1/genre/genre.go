package genre

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/genre"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type GenreController interface {
	Get(c echo.Context) (err error)
	GetAll(c echo.Context) (err error)
}

type genreController struct {
	genreSvc genre.GenreService
}

func NewGenreController(genreSvc genre.GenreService) GenreController {
	return &genreController{genreSvc: genreSvc}
}

// GetGenres godoc
//
//	@Summary		Get a specific genre.
//	@Description	Get a specific genre.
//	@Tags			genre
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Genre ID"
//	@Success		200	{object}	response.DataResponse[domain.Genre]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/genre/{id} [get]
func (f *genreController) Get(c echo.Context) (err error) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return response.NewError(
			c,
			apperror.ErrIdentifierMustBeNumeric,
		)
	}

	genre, err := f.genreSvc.First(c.Request().Context(), params.FirstGenreParams{
		ID: &id,
	})
	switch {
	case err != nil:
		return response.NewError(c, err)
	case genre == nil:
		return response.NewError(c, apperror.ErrRecordNotFound)
	}

	return response.NewData(c, http.StatusOK, genre)
}

// GetAllGenres godoc
//
//	@Summary		Get all genres.
//	@Description	Get all genres.
//	@Tags			genre
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]domain.Genre]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/genre [get]
func (f *genreController) GetAll(c echo.Context) (err error) {
	genres, err := f.genreSvc.List(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, genres)
}
