package anime

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_latest"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimeLatestController interface {
	GetAll(c echo.Context) (err error)
}

type animeLatestController struct {
	animeLatestSvc animelatest.AnimeLatestService
}

func NewAnimeLatestController(
	animeLatestSvc animelatest.AnimeLatestService,
) AnimeLatestController {
	return &animeLatestController{
		animeLatestSvc: animeLatestSvc,
	}
}

// GetAnimeLatest godoc
//
//	@Summary		Get latest anime releases
//	@Description	Get latest anime releases with filtering capabilities using the provided query strings.
//	@Tags			anime
//	@Accept			json
//	@Produce		json
//	@param			format		query		uint	false	"Anime format"
//	@param			status		query		uint	false	"Anime status"
//	@param			favourite	query		bool	false	"Anime favourite"
//	@Param			sortBy		query		string	false	"Sorting"						Enums(score, createdAt)
//	@Param			direction	query		string	false	"Sorting Direction"				Enums(desc, asc)
//	@Param			page		query		int		false	"Response page"					default(1)
//	@Param			elements	query		int		false	"Response number of elements"	default(20)
//	@Success		200			{object}	response.DataResponse[[]domain.AnimeLatest]
//	@Failure		404			{object}	response.ErrorResponse
//	@Failure		500			{object}	response.ErrorResponse
//	@Router			/api/v1/anime/latest [get]
func (a *animeLatestController) GetAll(c echo.Context) (err error) {
	var filter params.FilterAnimeLatestParams
	err = c.Bind(&filter)
	if err != nil {
		return response.NewErrorCode(c, err, http.StatusBadRequest)
	}

	paginatedAnimeLatest, err := a.animeLatestSvc.Filter(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewPageData(c, http.StatusOK, paginatedAnimeLatest)
}
