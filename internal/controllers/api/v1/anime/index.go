package anime

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/anime"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimeIndexController interface {
	GetAll(c echo.Context) (err error)
	Get(c echo.Context) (err error)
}

type animeIndexController struct {
	animeSvc anime.AnimeService
}

func NewAnimeIndexController(anime anime.AnimeService) AnimeIndexController {
	return &animeIndexController{anime}
}

// GetAnimeIndexes godoc
//
//	@Summary		Get anime indexes.
//	@Description	Get anime indexes.
//	@Tags			index
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]string]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/index [get]
func (a *animeIndexController) GetAll(c echo.Context) (err error) {
	indexes, err := a.animeSvc.Associations().AnimeIndex().ListIndexes(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, indexes)
}

// GetAnimeIndex godoc
//
//	@Summary		Get anime under an index.
//	@Description	Get anime under an index.
//	@Tags			index
//	@Accept			json
//	@Produce		json
//	@Param			index	path		string	true	"Anime Index"
//	@Success		200		{object}	response.DataResponse[[]domain.Anime]
//	@Failure		400		{object}	response.ErrorResponse
//	@Failure		404		{object}	response.ErrorResponse
//	@Failure		500		{object}	response.ErrorResponse
//	@Router			/api/v1/anime/index/{index} [get]
func (a *animeIndexController) Get(c echo.Context) (err error) {
	index := strings.TrimSpace(c.Param("index"))

	if len(index) != 1 {
		return response.NewError(c, apperror.ErrExpectedCharButGotString)
	}

	anime, err := a.animeSvc.Associations().AnimeIndex().ListAnimeByIndex(
		c.Request().Context(), params.ListAnimeByIndexParams{Index: index})
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewData(c, http.StatusOK, anime)
}
