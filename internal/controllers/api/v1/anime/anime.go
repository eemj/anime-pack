package anime

import (
	"context"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/anime"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	_ "gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type AnimeController interface {
	Middleware() echo.MiddlewareFunc

	Get(c echo.Context) (err error)
	Filter(c echo.Context) (err error)
}

// animeController contains all HTTP request methods in the '/anime' router group.
type animeController struct {
	animeSvc anime.AnimeService
}

func (c *animeController) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.AnimeControllerName)
}

// NewAnimeController returns a new AnimeController instance.
func NewAnimeController(anime anime.AnimeService) AnimeController {
	return &animeController{anime}
}

func (a *animeController) Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			param := c.Param("id")
			if param == "" {
				return next(c)
			}

			id, err := strconv.ParseInt(param, 10, 64)
			if err != nil {
				return response.NewError(c, apperror.ErrIdentifierMustBeNumeric)
			}

			a.L(c.Request().Context()).Info("get anime", zap.Int64("id", id))

			anime, err := a.animeSvc.Associations().
				AnimeReview().
				First(
					c.Request().Context(),
					params.FirstAnimeReviewParams{ID: id},
				)
			if err != nil {
				a.L(c.Request().Context()).
					Error("anime review filter", zap.Error(err))
				return response.NewError(c, apperror.ErrSomethingWentWrong)
			}
			if anime == nil {
				return response.NewError(c, apperror.ErrAnimeDoesNotExist)
			}

			newCtx := &AnimeContext{
				Context: c,
				Anime:   *anime,
			}
			return next(newCtx)
		}
	}
}

// GetAnime godoc
//
//	@Version		1.0
//	@Summary		Get a specific anime.
//	@Description	Get a specific anime by it's identifier.
//	@Tags			anime
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Anime ID"
//	@Success		200	{object}	response.DataResponse[domain.Anime]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/{id} [get]
func (a *animeController) Get(c echo.Context) (err error) {
	if ctx, ok := c.(*AnimeContext); ok {
		return response.NewData(c, http.StatusOK, ctx.Anime)
	}

	return response.NewError(c, apperror.ErrWrongContextDataType)
}

// GetAnimeFilter godoc
//
//	@Summary		Get all anime.
//	@Description	Get all anime with filtering capabilities using the provided query strings.
//	@Tags			anime
//	@Accept			json
//	@Produce		json
//	@Param			id			query		[]int64		false	"IDs"
//	@Param			title		query		string		false	"Title"
//	@Param			genreIDs	query		[]int64		false	"Genres IDs"
//	@Param			studioIDs	query		[]int64		false	"Studios IDs"
//	@Param			format		query		[]string	false	"Formats"	Enums(TV, TV_SHORT, OVA, MOVIE, SPECIAL, ONA, MUSIC)
//	@Param			season		query		[]string	false	"Seasons"	Enums(WINTER, FALL, SUMMER, SPRING)
//	@Param			status		query		[]string	false	"Statuses"	Enums(FINISHED, RELEASING, NOT_YET_RELEASED, CANCELLED, HIATUS)
//	@Param			seasonYear	query		int64		false	"Season Year"
//	@Param			favourite	query		bool		false	"Favourite"
//	@Param			orderBy		query		string		false	"Sorting"						Enums(id, title, score, nextAiringDate, startDate)
//	@Param			direction	query		string		false	"Sorting Direction"				Enums(desc, asc)
//	@Param			page		query		int64		false	"Response page"					default(1)
//	@Param			elements	query		int64		false	"Response number of elements"	default(20)
//	@Success		200			{object}	response.DataResponse[[]domain.Anime]
//	@Failure		400			{object}	response.ErrorResponse
//	@Failure		404			{object}	response.ErrorResponse
//	@Failure		500			{object}	response.ErrorResponse
//	@Router			/api/v1/anime [get]
func (a *animeController) Filter(c echo.Context) (err error) {
	var filter params.FilterAnimeReviewParams
	err = c.Bind(&filter)
	if err != nil {
		return response.NewError(c, err)
	}

	paginatedAnime, err := a.animeSvc.Associations().
		AnimeReview().
		Filter(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewPageData(c, http.StatusOK, paginatedAnime)
}
