package anime

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animeepisode "gitlab.com/eemj/anime-pack/internal/anime/anime_episode"
	animeepisodelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_episode_latest"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimeEpisodeController interface {
	Get(c echo.Context) (err error)
	GetAll(c echo.Context) (err error)
	Filter(c echo.Context) (err error)
}

type animeEpisodeController struct {
	animeEpisodeSvc       animeepisode.AnimeEpisodeService
	animeEpisodeLatestSvc animeepisodelatest.AnimeEpisodeLatestService
}

func NewAnimeEpisodeController(
	animeEpisodeSvc animeepisode.AnimeEpisodeService,
	animeEpisodeLatestSvc animeepisodelatest.AnimeEpisodeLatestService,
) AnimeEpisodeController {
	return &animeEpisodeController{
		animeEpisodeSvc:       animeEpisodeSvc,
		animeEpisodeLatestSvc: animeEpisodeLatestSvc,
	}
}

// GetAnimeEpisodes godoc
//
//	@Summary		Get all episodes for an anime.
//	@Description	Get all episodes for an anime.
//	@Tags			episode
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Anime ID"
//	@Success		200	{object}	response.DataResponse[[]domain.AnimeEpisode]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/{id}/episode [get]
func (a *animeEpisodeController) GetAll(c echo.Context) (err error) {
	ctx, ok := c.(*AnimeContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	episodes, err := a.animeEpisodeSvc.Filter(c.Request().Context(), params.FilterAnimeEpisodeParams{
		AnimeID: ctx.Anime.ID,
	})
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, episodes)
}

// GetAnimeEpisode godoc
//
//	@Summary		Get specific episode for an anime.
//	@Description	Get specific episode for an anime.
//	@Tags			episode
//	@Accept			json
//	@Produce		json
//	@Param			id		path		int		true	"Anime ID"
//	@Param			episode	path		string	true	"Episode name"
//	@Success		200		{object}	response.DataResponse[domain.AnimeEpisode]
//	@Failure		404		{object}	response.ErrorResponse
//	@Failure		500		{object}	response.ErrorResponse
//	@Router			/api/v1/anime/{id}/episode/{episode} [get]
func (a *animeEpisodeController) Get(c echo.Context) (err error) {
	ctx, ok := c.(*AnimeContext)

	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	episodeName := c.Param("episode")
	if episodeName == "" {
		return response.NewError(c, apperror.ErrEpisodeNameRequired)
	}

	episodes, err := a.animeEpisodeSvc.Filter(c.Request().Context(), params.FilterAnimeEpisodeParams{
		AnimeID:     ctx.Anime.ID,
		EpisodeName: &episodeName,
	})
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, episodes)
}

// GetAnimeEpisodeLatestFilter godoc
//
//	@Summary		Get filtered anime latest episodes
//	@Description	Get filtered anime latest episodes with filtering capabilities using the provided query strings.
//	@Tags			episode
//	@Accept			json
//	@Produce		json
//	@Param			favourite	query		bool	false	"Favourite"
//	@Param			status		query		string	false	"Status"						Enums(FINISHED, RELEASING, NOT_YET_RELEASED, CANCELLED, HIATUS)
//	@Param			orderBy		query		string	false	"Sorting"						Enums(createdAt, size, score, duration)
//	@Param			direction	query		string	false	"Sorting Direction"				Enums(desc, asc)
//	@Param			page		query		int		false	"Response page"					default(1)
//	@Param			elements	query		int		false	"Response number of elements"	default(20)
//	@Success		200			{object}	response.DataResponse[[]domain.AnimeEpisodeLatest]
//	@Failure		400			{object}	response.ErrorResponse
//	@Failure		404			{object}	response.ErrorResponse
//	@Failure		500			{object}	response.ErrorResponse
//	@Router			/api/v1/anime/episode/latest [get]
func (a *animeEpisodeController) Filter(c echo.Context) (err error) {
	var filter params.FilterAnimeEpisodeLatestParams
	err = c.Bind(&filter)
	if err != nil {
		return response.NewErrorCode(c, err, http.StatusBadRequest)
	}

	paginatedEpisodeLatest, err := a.animeEpisodeLatestSvc.Filter(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewPageData(c, http.StatusOK, paginatedEpisodeLatest)
}
