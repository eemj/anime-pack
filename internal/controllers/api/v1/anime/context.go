package anime

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/domain"
)

// AnimeContext holds the current Anime based on the identifier parameter.
type AnimeContext struct {
	echo.Context
	Anime domain.Anime
}
