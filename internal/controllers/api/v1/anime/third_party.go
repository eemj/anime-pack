package anime

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/anime"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimeThirdPartyController interface {
	Get(c echo.Context) (err error)
}

type animeThirdPartyController struct {
	anime anime.AnimeService
}

func NewThirdPartyController(anime anime.AnimeService) AnimeThirdPartyController {
	return &animeThirdPartyController{anime}
}

// GetAnimeThirdParty godoc
//
//	@Summary		Get third party anime.
//	@Description	Get third party anime.
//	@Tags			third-party
//	@Accept			json
//	@Produce		json
//	@Param			id		query		uint	false	"AniList ID"
//	@Param			idMAL	query		uint	false	"MyAnimeList ID"
//	@Success		200		{object}	response.DataResponse[domain.Anime]
//	@Failure		400		{object}	response.ErrorResponse
//	@Failure		404		{object}	response.ErrorResponse
//	@Failure		500		{object}	response.ErrorResponse
//	@Router			/api/v1/anime/third-party [get]
func (s *animeThirdPartyController) Get(c echo.Context) error {
	var (
		filter params.FilterAnimeMediaParams
		err    = c.Bind(&filter)
	)
	if err != nil {
		return response.NewError(c, err)
	}

	anime, err := s.anime.Associations().
		AnimeMedia().
		Filter(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, anime)
}
