package anime

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animesimulcast "gitlab.com/eemj/anime-pack/internal/anime/anime_simulcast"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimeSimulcastController interface {
	GetAll(c echo.Context) (err error)
}

type animeSimulcastController struct {
	animeSimulcastSvc animesimulcast.AnimeSimulcastService
}

func NewAnimeSimulcastController(animeSimulcastSvc animesimulcast.AnimeSimulcastService) AnimeSimulcastController {
	return &animeSimulcastController{animeSimulcastSvc}
}

// GetAllAnimeSimulcasts godoc
//
//	@Summary		Get anime simulcasts
//	@Description	Get anime simulcasts
//	@Tags			anime
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]domain.AnimeSimulcast]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/simulcast [get]
func (a *animeSimulcastController) GetAll(c echo.Context) (err error) {
	animeSimulcasts, err := a.animeSimulcastSvc.List(c.Request().Context())
	if err != nil {
		return response.NewErrorCode(
			c,
			err,
			http.StatusInternalServerError,
		)
	}

	return response.NewData(c, http.StatusOK, animeSimulcasts)
}
