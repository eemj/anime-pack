package anime

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/preferences"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type AnimePreferencesController interface {
	Get(c echo.Context) (err error)
	Update(c echo.Context) (err error)
}

type animePreferencesController struct {
	preferencesSvc preferences.PreferencesService
}

func NewAnimePreferencesController(
	preferencesSvc preferences.PreferencesService,
) AnimePreferencesController {
	return &animePreferencesController{preferencesSvc}
}

// GetAnimePreferences godoc
//
//	@Summary		Get preferences for an anime.
//	@Description	Get preferences for an anime.
//	@Tags			preferences
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Anime ID"
//	@Success		200	{object}	response.DataResponse[domain.Preferences]
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/{id}/preferences [get]
func (p *animePreferencesController) Get(c echo.Context) (err error) {
	ctx, ok := c.(*AnimeContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	preferences, err := p.preferencesSvc.First(c.Request().Context(), params.FirstPreferenceParams{
		AnimeID: &ctx.Anime.ID,
	})
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewData(c, http.StatusOK, preferences)
}

// PatchAnimePreferences godoc
//
//	@Summary		Update a particular preference for an anime.
//	@Description	Update a particular preference for an anime.
//	@Tags			preferences
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Anime ID"
//	@Success		200	{object}	response.DataResponse[domain.Preferences]
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/anime/{id}/preferences [patch]
func (p *animePreferencesController) Update(c echo.Context) error {
	ctx, ok := c.(*AnimeContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	var (
		filter params.UpdatePreferencesParams
		err    = c.Bind(&filter)
	)
	if err != nil {
		return response.NewError(c, err)
	}

	filter.AnimeID = ctx.Anime.ID

	preferences, err := p.preferencesSvc.Update(c.Request().Context(), filter)
	if err != nil {
		return response.NewError(c, err)
	}

	return response.NewData(c, http.StatusAccepted, preferences)
}
