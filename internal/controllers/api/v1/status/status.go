package status

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animestatus "gitlab.com/eemj/anime-pack/internal/anime/anime_status"
	_ "gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type StatusController interface {
	GetAll(c echo.Context) (err error)
}

type statusController struct {
	animeStatusSvc animestatus.AnimeStatusService
}

func NewStatusController(
	animeStatusSvc animestatus.AnimeStatusService,
) StatusController {
	return &statusController{animeStatusSvc: animeStatusSvc}
}

// GetAllStatuses godoc
//
//	@Summary		Get all available statuses.
//	@Description	Get all available statuses.
//	@Tags			status
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]enums.AnimeStatus]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/status [get]
func (f *statusController) GetAll(c echo.Context) (err error) {
	statuss, err := f.animeStatusSvc.List(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, statuss)
}
