package format

import (
	"net/http"

	"github.com/labstack/echo/v4"
	animeformat "gitlab.com/eemj/anime-pack/internal/anime/anime_format"
	_ "gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type FormatController interface {
	GetAll(c echo.Context) (err error)
}

type formatController struct {
	animeFormatSvc animeformat.AnimeFormatService
}

func NewFormatController(
	animeFormatSvc animeformat.AnimeFormatService,
) FormatController {
	return &formatController{
		animeFormatSvc: animeFormatSvc,
	}
}

// GetAllFormats godoc
//
//	@Summary		Get all available formats.
//	@Description	Get all available formats.
//	@Tags			format
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]enums.AnimeFormat]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/format [get]
func (f *formatController) GetAll(c echo.Context) (err error) {
	formats, err := f.animeFormatSvc.List(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, formats)
}
