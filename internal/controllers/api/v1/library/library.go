package library

import (
	"net/http"

	"github.com/labstack/echo/v4"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/library"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

type LibraryController interface {
	GetExport(c echo.Context) (err error)
	PostImport(c echo.Context) (err error)
}

type libraryController struct {
	librarySvc library.LibraryService
}

func NewLibraryController(
	librarySvc library.LibraryService,
) LibraryController {
	return &libraryController{librarySvc: librarySvc}
}

// GetExport godoc
//
//	@Summary		Get exported title reviews.
//	@Description	Get exported title reviews whether they're deleted or associated with an anime.
//	@Tags			library
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]domain.LibraryItem]
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/library/export [get]
func (co *libraryController) GetExport(c echo.Context) (err error) {
	entries, err := co.librarySvc.Export(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, entries)
}

// PostExport godoc
//
//	@Summary		Import title review.
//	@Description	Import title review by specifying if it's deleted or associated with an anime.
//	@Tags			library
//	@Accept			json
//	@Produce		json
//	@Success		202	""
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/library/import [post]
func (co *libraryController) PostImport(c echo.Context) error {
	var (
		param params.ImportLibraryParams
		err   = c.Bind(&param)
	)
	if err != nil {
		return response.NewError(c, err)
	}

	err = co.librarySvc.Import(c.Request().Context(), param)
	if err != nil {
		return response.NewError(c, err)
	}

	return c.NoContent(http.StatusAccepted)
}
