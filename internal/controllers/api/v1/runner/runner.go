package runner

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	"gitlab.com/eemj/anime-pack/internal/runner"
	"gitlab.com/eemj/anime-pack/internal/runner/manager"

	_ "gitlab.com/eemj/anime-pack/internal/runner/client" // For swag to resolve the `client` package.
)

type RunnerController interface {
	Middleware() echo.MiddlewareFunc

	Get(c echo.Context) (err error)
	GetAll(c echo.Context) (err error)
	ToggleState(c echo.Context) (err error)
}

type runnerController struct {
	runner runner.RunnerService
}

func NewRunnerController(runner runner.RunnerService) RunnerController {
	return &runnerController{runner}
}

func (r *runnerController) Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			ident := c.Param("ident")
			if ident == "" {
				return response.NewError(c, apperror.ErrRunnerIdentRequired)
			}

			client := r.runner.Client(&claims.RunnerClaims{Ident: ident})
			if client == nil {
				return response.NewErrorCode(
					c,
					manager.ErrClientNotConnected(ident),
					http.StatusNotFound,
				)
			}

			nextCtx := &RunnerContext{Context: c, Client: client}
			return next(nextCtx)
		}
	}
}

// GetAllRunners godoc
//
//	@Summary		Get all runners.
//	@Description	Get all runners.
//	@Tags			runner
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]client.Info]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/runner [get]
func (r *runnerController) GetAll(c echo.Context) (err error) {
	return response.NewData(c, http.StatusOK, r.runner.Info())
}

// GetRunner godoc
//
//	@Summary		Get a specific runner.
//	@Description	Get a specific runner by it's `ident`.
//	@Tags			runner
//	@Accept			json
//	@Produce		json
//	@Param			ident	path		string	true	"Runner Ident"
//	@Success		200		{object}	response.DataResponse[client.Info]
//	@Failure		404		{object}	response.ErrorResponse
//	@Failure		500		{object}	response.ErrorResponse
//	@Router			/api/v1/runner/{ident} [get]
func (r *runnerController) Get(c echo.Context) (err error) {
	ctx, ok := c.(*RunnerContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	return response.NewData(
		c,
		http.StatusOK,
		ctx.Client.Info(),
	)
}

// ToggleStateRunner godoc
//
//	@Summary		Toggle state for a specific runner.
//	@Description	Toggle state for a specific runner by it's `ident`.
//	@Tags			runner
//	@Accept			json
//	@Produce		json
//	@Param			ident	path	string	true	"Runner Ident"
//	@Success		202		""
//	@Failure		400		{object}	response.ErrorResponse
//	@Failure		404		{object}	response.ErrorResponse
//	@Failure		500		{object}	response.ErrorResponse
//	@Router			/api/v1/runner/{ident} [put]
func (r *runnerController) ToggleState(c echo.Context) (err error) {
	ctx, ok := c.(*RunnerContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	if runnerClaims, ok := ctx.Client.Claims().(*claims.RunnerClaims); ok {
		state, err := r.runner.ToggleState(runnerClaims)
		if err != nil {
			return response.NewError(c, err)
		}

		return response.NewData(c, http.StatusAccepted, state)
	}

	return response.NewError(c, apperror.ErrRunnerNotFound)
}
