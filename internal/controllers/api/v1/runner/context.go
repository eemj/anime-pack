package runner

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
)

type RunnerContext struct {
	echo.Context
	Client client.Client
}
