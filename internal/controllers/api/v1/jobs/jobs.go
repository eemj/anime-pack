package jobs

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	jobprocess "gitlab.com/eemj/anime-pack/internal/jobs"
	"gitlab.com/eemj/anime-pack/internal/router/response"

	_ "gitlab.com/eemj/anime-pack/internal/jobs/task" // For swag to resolve the `task` package.
)

type JobsController interface {
	Middleware() echo.MiddlewareFunc

	GetAll(c echo.Context) (err error)
	Get(c echo.Context) (err error)
	PutStart(c echo.Context) (err error)
	DeleteStop(c echo.Context) (err error)
	DeleteDisable(c echo.Context) (err error)
}

type jobsController struct {
	jobs jobprocess.Jobs
}

func NewJobsController(
	jobs jobprocess.Jobs,
) JobsController {
	return &jobsController{jobs}
}

func (j *jobsController) Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			id, err := uuid.Parse(c.Param("id"))
			if err != nil {
				return response.NewError(c, err)
			}

			taskInfo, err := j.jobs.Get(id)
			if err != nil {
				return response.NewError(c, err)
			}

			ctx := &JobContext{Context: c, Info: taskInfo}

			return next(ctx)
		}
	}
}

// GetJobs godoc
//
//	@Summary		Get all registered jobs.
//	@Description	Get all registered jobs.
//	@Tags			job
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]task.Info]
//	@Router			/api/v1/jobs [get]
func (j *jobsController) GetAll(c echo.Context) (err error) {
	return response.NewData(
		c,
		http.StatusOK,
		j.jobs.Info(),
	)
}

// GetJob godoc
//
//	@Summary		Get a specific job.
//	@Description	Get a specific job.
//	@Tags			job
//	@Accept			json
//	@Produce		json
//	@Param			id	path		string	true	"Job ID"
//	@Success		200	{object}	response.DataResponse[task.Info]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/jobs/{id} [get]
func (j *jobsController) Get(c echo.Context) (err error) {
	if ctx, ok := c.(*JobContext); ok {
		return response.NewData(c, http.StatusOK, ctx.Info)
	}

	return response.NewError(c, apperror.ErrWrongContextDataType)
}

// PutStart godoc
//
//	@Summary		Start a specific job.
//	@Description	Start a specific job.
//	@Tags			job
//	@Accept			json
//	@Produce		json
//	@Param			id	path	string	true	"Job ID"
//	@Success		202	""
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/jobs/{id}/start [put]
func (j *jobsController) PutStart(c echo.Context) (err error) {
	ctx, ok := c.(*JobContext)

	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	err = j.jobs.Start(c.Request().Context(), ctx.Info.ID)
	if err != nil {
		return response.NewError(c, err)
	}

	return c.NoContent(http.StatusAccepted)
}

// DeleteStop godoc
//
//	@Summary		Stop a specific job.
//	@Description	Stop a specific job.
//	@Tags			job
//	@Accept			json
//	@Produce		json
//	@Param			id	path	string	true	"Job ID"
//	@Success		202	""
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/jobs/{id}/stop [delete]
func (j *jobsController) DeleteStop(c echo.Context) (err error) {
	ctx, ok := c.(*JobContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	err = j.jobs.Stop(c.Request().Context(), ctx.Info.ID)
	if err != nil {
		return response.NewError(c, err)
	}

	return c.NoContent(http.StatusAccepted)
}

// DeleteDisable godoc
//
//	@Summary		Disable a specific job.
//	@Description	Disable a specific job.
//	@Tags			job
//	@Accept			json
//	@Produce		json
//	@Param			id	path	string	true	"Job ID"
//	@Success		202	""
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		400	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/jobs/{id}/disable [delete]
func (j *jobsController) DeleteDisable(c echo.Context) (err error) {
	ctx, ok := c.(*JobContext)
	if !ok {
		return response.NewError(c, apperror.ErrWrongContextDataType)
	}

	err = j.jobs.Disable(ctx.Info.ID)
	if err != nil {
		return response.NewError(c, err)
	}

	return c.NoContent(http.StatusAccepted)
}
