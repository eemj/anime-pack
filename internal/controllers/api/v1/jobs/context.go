package jobs

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/jobs/task"
)

// JobContext holds the current Job based on the identifier parameter.
type JobContext struct {
	echo.Context
	Info task.Info
}
