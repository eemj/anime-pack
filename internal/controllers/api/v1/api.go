package api

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/base"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/anime"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/format"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/genre"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/jobs"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/library"
	releasegroups "gitlab.com/eemj/anime-pack/internal/controllers/api/v1/release_groups"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/runner"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/season"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/status"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/studio"
	"gitlab.com/eemj/anime-pack/internal/controllers/api/v1/title"
	jobprocess "gitlab.com/eemj/anime-pack/internal/jobs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
)

type API struct {
	base.API

	status       status.StatusController
	format       format.FormatController
	genre        genre.GenreController
	anime        anime.AnimeController
	season       season.SeasonController
	studio       studio.StudioController
	runner       runner.RunnerController
	title        title.TitleController
	releaseGroup releasegroups.ReleaseGroupController

	preferences     anime.AnimePreferencesController
	animeThirdParty anime.AnimeThirdPartyController
	animeIndex      anime.AnimeIndexController
	animeLatest     anime.AnimeLatestController
	animeEpisode    anime.AnimeEpisodeController
	animeSimulcast  anime.AnimeSimulcastController

	jobs jobs.JobsController

	library library.LibraryController
}

func (a API) Register(group *echo.Group) (err error) {
	// Status/Format/Season/Season Year
	group.GET("/status", a.status.GetAll)
	group.GET("/format", a.format.GetAll)
	group.GET("/season", a.season.GetAll)
	group.GET("/season-year", a.season.GetAllYears)

	// Genre
	group.GET("/genre", a.genre.GetAll)
	group.GET("/genre/:id", a.genre.Get)

	// Studio
	group.GET("/studio", a.studio.GetAll)
	group.GET("/studio/:id", a.studio.Get)

	// Runner
	group.GET("/runner", a.runner.GetAll)
	group.GET("/runner/:ident", a.runner.Get, a.runner.Middleware())
	group.PUT("/runner/:ident", a.runner.ToggleState, a.runner.Middleware())

	// Title
	group.GET("/title", a.title.GetAll)
	group.GET("/title/:id", a.title.Get, a.title.Middleware())
	group.PATCH("/title/:id", a.title.Update, a.title.Middleware())
	group.DELETE("/title/:id", a.title.Delete, a.title.Middleware())

	// Release Group
	group.GET("/release-group", a.releaseGroup.GetAll)

	// Anime
	group.GET("/anime", a.anime.Filter)
	group.GET("/anime/index", a.animeIndex.GetAll)
	group.GET("/anime/index/:index", a.animeIndex.Get)
	group.GET("/anime/third-party", a.animeThirdParty.Get)
	group.GET("/anime/latest", a.animeLatest.GetAll)
	group.GET("/anime/simulcast", a.animeSimulcast.GetAll)
	group.GET("/anime/episode/latest", a.animeEpisode.Filter)

	group.GET("/anime/:id", a.anime.Get, a.anime.Middleware())
	group.GET("/anime/:id/episode", a.animeEpisode.GetAll, a.anime.Middleware())
	group.GET("/anime/:id/episode/:episode", a.animeEpisode.Get, a.anime.Middleware())
	group.GET("/anime/:id/preferences", a.preferences.Get, a.anime.Middleware())
	group.PATCH("/anime/:id/preferences", a.preferences.Update, a.anime.Middleware())

	// Library
	group.GET("/library/export", a.library.GetExport)
	group.POST("/library/import", a.library.PostImport)

	// Jobs
	group.GET("/jobs", a.jobs.GetAll)
	group.GET("/jobs/:id", a.jobs.Get, a.jobs.Middleware())
	group.PUT("/jobs/:id/start", a.jobs.PutStart, a.jobs.Middleware())
	group.DELETE("/jobs/:id/stop", a.jobs.DeleteStop, a.jobs.Middleware())
	group.DELETE("/jobs/:id/disable", a.jobs.DeleteDisable, a.jobs.Middleware())

	return
}

func (a API) Version() int { return 1 }

func New(services lifecycle.ServiceLifecycle, jobprocess jobprocess.Jobs) API {
	statusCtrl := status.NewStatusController(services.Anime().Associations().AnimeStatus())
	seasonCtrl := season.NewSeasonController(services.Anime().Associations().AnimeSeason())
	formatCtrl := format.NewFormatController(services.Anime().Associations().AnimeFormat())

	genreCtrl := genre.NewGenreController(services.Genre())
	animeCtrl := anime.NewAnimeController(services.Anime())
	studioCtrl := studio.NewStudioController(services.Studio())
	runnerCtrl := runner.NewRunnerController(services.Runner())
	titleCtrl := title.NewTitleController(services.Title())

	releaseGroupCtrl := releasegroups.NewReleaseGroupController(services.ReleaseGroup())

	preferencesCtrl := anime.NewAnimePreferencesController(services.Preferences())
	animeThirdPartyCtrl := anime.NewThirdPartyController(services.Anime())
	animeIndexCtrl := anime.NewAnimeIndexController(services.Anime())

	animeLatestCtrl := anime.NewAnimeLatestController(
		services.Anime().Associations().AnimeLatest(),
	)
	animeEpisodeCtrl := anime.NewAnimeEpisodeController(
		services.Anime().Associations().AnimeEpisode(),
		services.Anime().Associations().AnimeEpisodeLatest(),
	)
	animeSimulcastCtrl := anime.NewAnimeSimulcastController(
		services.Anime().Associations().AnimeSimulcast(),
	)

	jobsCtrl := jobs.NewJobsController(jobprocess)

	libraryCtrl := library.NewLibraryController(services.Library())

	return API{
		status:          statusCtrl,
		season:          seasonCtrl,
		format:          formatCtrl,
		genre:           genreCtrl,
		anime:           animeCtrl,
		studio:          studioCtrl,
		runner:          runnerCtrl,
		title:           titleCtrl,
		releaseGroup:    releaseGroupCtrl,
		preferences:     preferencesCtrl,
		animeThirdParty: animeThirdPartyCtrl,
		animeIndex:      animeIndexCtrl,
		animeLatest:     animeLatestCtrl,
		animeEpisode:    animeEpisodeCtrl,
		animeSimulcast:  animeSimulcastCtrl,
		jobs:            jobsCtrl,
		library:         libraryCtrl,
	}
}
