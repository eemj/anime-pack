package studio

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	_ "gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	"gitlab.com/eemj/anime-pack/internal/studio"
)

type StudioController interface {
	Get(c echo.Context) (err error)
	GetAll(c echo.Context) (err error)
}

type studioController struct {
	studioSvc studio.StudioService
}

func NewStudioController(studioSvc studio.StudioService) StudioController {
	return &studioController{studioSvc: studioSvc}
}

// GetStudio godoc
//
//	@Summary		Get a specific studio.
//	@Description	Get a specific studio.
//	@Tags			studio
//	@Accept			json
//	@Produce		json
//	@Param			id	path		int	true	"Studio ID"
//	@Success		200	{object}	response.DataResponse[domain.Studio]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/studio/{id} [get]
func (f *studioController) Get(c echo.Context) (err error) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		return response.NewError(
			c,
			apperror.ErrIdentifierMustBeNumeric,
		)
	}

	studio, err := f.studioSvc.First(c.Request().Context(), params.FirstStudioParams{
		ID: &id,
	})
	switch {
	case err != nil:
		return response.NewError(c, err)
	case studio == nil:
		return response.NewError(c, apperror.ErrRecordNotFound)
	}

	return response.NewData(c, http.StatusOK, studio)
}

// GetAllStudios godoc
//
//	@Summary		Get all studios.
//	@Description	Get all studios.
//	@Tags			studio
//	@Accept			json
//	@Produce		json
//	@Success		200	{object}	response.DataResponse[[]domain.Studio]
//	@Failure		404	{object}	response.ErrorResponse
//	@Failure		500	{object}	response.ErrorResponse
//	@Router			/api/v1/studio [get]
func (f *studioController) GetAll(c echo.Context) (err error) {
	studios, err := f.studioSvc.List(c.Request().Context())
	if err != nil {
		return response.NewError(c, err)
	}
	return response.NewData(c, http.StatusOK, studios)
}
