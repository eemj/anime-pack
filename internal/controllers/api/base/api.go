package base

import "github.com/labstack/echo/v4"

type API interface {
	Register(group *echo.Group) (err error)
	Version() int
}
