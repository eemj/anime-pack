package base

import (
	"github.com/labstack/echo/v4"
)

type Global interface {
	Register(echo *echo.Echo) (err error)
}
