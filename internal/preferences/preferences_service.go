package preferences

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

type PreferencesService interface {
	Create(ctx context.Context, arg params.InsertPreferencesParams) (*domain.Preferences, error)
	First(ctx context.Context, arg params.FirstPreferenceParams) (*domain.Preferences, error)
	Filter(ctx context.Context, arg params.FilterPreferencesParams) ([]*domain.Preferences, error)
	Count(ctx context.Context, arg params.CountPreferencesParams) (int64, error)
	Update(ctx context.Context, arg params.UpdatePreferencesParams) (int64, error)
	Delete(ctx context.Context, arg params.DeletePreferencesParams) (int64, error)
}

type preferencesService struct {
	querier database.QuerierExtended
}

// Count implements PreferencesService.
func (s *preferencesService) Count(ctx context.Context, arg params.CountPreferencesParams) (int64, error) {
	return s.querier.CountPreferences(ctx, database.CountPreferencesParams(arg))
}

// CreateMany implements PreferencesService.
func (s *preferencesService) Create(ctx context.Context, arg params.InsertPreferencesParams) (*domain.Preferences, error) {
	row, err := s.querier.InsertPreferences(ctx, database.InsertPreferencesParams(arg))
	if err != nil {
		return nil, err
	}
	return &domain.Preferences{
		AnimeID:            row.AnimeID,
		Favourite:          row.Favourite,
		AutomaticDownloads: row.AutomaticDownloads,
		PerformChecksum:    row.PerformChecksum,
		CreatedAt:          row.CreatedAt,
		UpdatedAt:          row.UpdatedAt,
	}, nil
}

// Delete implements PreferencesService.
func (s *preferencesService) Delete(ctx context.Context, arg params.DeletePreferencesParams) (int64, error) {
	return s.querier.DeletePreferences(ctx, database.DeletePreferencesParams(arg))
}

func mapDatabaseToDomain(entities ...*database.Preference) []*domain.Preferences {
	results := make([]*domain.Preferences, len(entities))
	for index, entity := range entities {
		results[index] = &domain.Preferences{
			AnimeID:            entity.AnimeID,
			Favourite:          entity.Favourite,
			AutomaticDownloads: entity.AutomaticDownloads,
			PerformChecksum:    entity.PerformChecksum,
			CreatedAt:          entity.CreatedAt,
			UpdatedAt:          entity.UpdatedAt,
		}
	}
	return results
}

// Filter implements PreferencesService.
func (s *preferencesService) Filter(ctx context.Context, arg params.FilterPreferencesParams) ([]*domain.Preferences, error) {
	rows, err := s.querier.FilterPreferences(ctx, database.FilterPreferencesParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Preferences{}, nil
		}
		return nil, err
	}
	return mapDatabaseToDomain(rows...), nil
}

// First implements PreferencesService.
func (s *preferencesService) First(ctx context.Context, arg params.FirstPreferenceParams) (*domain.Preferences, error) {
	row, err := s.querier.FirstPreference(ctx, database.FirstPreferenceParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapDatabaseToDomain(row)[0], nil
}

// Update implements PreferencesService.
func (s *preferencesService) Update(ctx context.Context, arg params.UpdatePreferencesParams) (int64, error) {
	err := arg.Validate()
	if err != nil {
		return 0, err
	}
	return s.querier.UpdatePreferences(ctx, database.UpdatePreferencesParams(arg))
}

func NewPreferencesService(
	querier database.QuerierExtended,
) PreferencesService {
	return &preferencesService{
		querier: querier,
	}
}
