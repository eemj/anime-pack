package feed

import (
	"gitlab.com/eemj/anime-pack/internal/feed/packlist"
	"gitlab.com/eemj/anime-pack/internal/irc"
	"go.jamie.mt/mjcache/redis"
)

type Feed interface {
	Packlist() packlist.PacklistFeed
}

type feed struct {
	packlist packlist.PacklistFeed
}

func (f *feed) Packlist() packlist.PacklistFeed { return f.packlist }

func New(
	ircClient irc.Client,
	redisCacher *redis.RedisCacher,
) Feed {
	return &feed{
		packlist: packlist.NewFeed(ircClient, redisCacher),
	}
}
