package packlist

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

func (s *packlistFeed) BustPacks(ctx context.Context) (err error) {
	return s.encodedCacher.Delete(ctx, cachekeys.Packlist)
}

func (s *packlistFeed) GetPacks(ctx context.Context) (packs domain.PackItems, err error) {
	cachedValue, err := encoded.Get[domain.PackItems](ctx, s.encodedCacher, cachekeys.Packlist)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}

	err = s.redisMutex.LockContext(ctx)
	if err != nil {
		return nil, err
	}

	s.L(ctx).Debug("empty packlist, fetching..")

	var packsErr error
	for _, packList := range s.integrations {
		newPacks, newPacksErr := packList.Packs(ctx)
		if newPacksErr != nil {
			packsErr = errors.Join(packsErr, newPacksErr)
		} else {
			packs = append(packs, newPacks...)
		}
	}
	if packsErr != nil {
		s.L(ctx).Warn("errors occurred whilst fetching packlist", zap.Error(packsErr))
	}

	err = encoded.Set(
		ctx,
		s.encodedCacher,
		cachekeys.Packlist,
		packs,
		(30 * time.Minute), // Expiry
	)
	if err != nil {
		return nil, err
	}

	_, err = s.redisMutex.Unlock()
	if err != nil {
		return nil, fmt.Errorf("failed to unlock mutex `%s` due to %w", cachekeys.PacklistMutex, err)
	}

	return packs, nil
}

func (s *packlistFeed) GroupPacks(ctx context.Context, packItems domain.PackItems) (domain.GroupPacks, error) {
	groupPacks := make(domain.GroupPacks)
	for _, pack := range packItems {
		entry := groups.Parse(pack.Filename)
		if entry == nil { // Was not able to parse.
			continue
		}
		titleBase := domain.NewTitleBaseKey(entry.Title, entry.SeasonNumber, entry.Year)
		_, exists := groupPacks[titleBase]
		if !exists {
			groupPacks[titleBase] = make([]*domain.GroupPack, 0)
		}
		groupPacks[titleBase] = append(groupPacks[titleBase], &domain.GroupPack{
			Entry: *entry,
			Pack:  *pack,
		})
	}
	return groupPacks, nil
}
