package packlist

import (
	"context"
	"reflect"
	"time"

	"github.com/go-redsync/redsync/v4"
	"github.com/go-redsync/redsync/v4/redis/goredis/v9"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/arutha"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/blargh"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/kawaiiyuri"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/nibl"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/oyatsu"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/sadaharu"
	// "gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/sakuraserv"
	"gitlab.com/eemj/anime-pack/internal/irc"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.jamie.mt/mjcache/redis"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type PacklistFeed interface {
	GetPacks(ctx context.Context) (domain.PackItems, error)
	GroupPacks(ctx context.Context, packItems domain.PackItems) (domain.GroupPacks, error)
	BustPacks(ctx context.Context) (err error)
}

type packlistFeed struct {
	trace trace.Tracer

	ircClient irc.Client

	redisCacher   *redis.RedisCacher
	redisMutex    *redsync.Mutex
	encodedCacher *encoded.EncodedCacher

	integrations []base.PacklistIntegration
}

func (s *packlistFeed) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.PacklistFetcherName)
}

func NewFeed(
	ircClient irc.Client,
	redisCacher *redis.RedisCacher,
) *packlistFeed {
	encodedCacher := encoded.NewEncodedCacher(redisCacher, &codec.MsgPack{})

	redisClient := redisCacher.Instance()
	redisPool := goredis.NewPool(redisClient)
	redisSync := redsync.New(redisPool)
	redisMutex := redisSync.NewMutex(
		cachekeys.PacklistMutex,
		redsync.WithExpiry((1 * time.Minute)),
		redsync.WithFailFast(true),
	)

	return &packlistFeed{
		trace:         otel.Tracer(reflect.TypeFor[packlistFeed]().PkgPath()),
		ircClient:     ircClient,
		redisCacher:   redisCacher,
		redisMutex:    redisMutex,
		encodedCacher: encodedCacher,

		// TODO(eemj): We really need to make this configurable..
		integrations: []base.PacklistIntegration{
			sadaharu.New(),
			arutha.New(),
			nibl.New(),
			// chinesecartoons.New(), // Seems like they went away..
			oyatsu.New(),
			kawaiiyuri.New(),
			blargh.New(),
			// sakuraserv.New(ircClient), // Disabled
		},
	}
}
