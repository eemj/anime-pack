package sakuraserv

import (
	"bytes"
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
	"gitlab.com/eemj/anime-pack/internal/irc"
	"gitlab.com/eemj/anime-pack/pkg/dcc"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

const Name = "SakuraServ"

type sakuraServ struct {
	ircClient irc.Client

	indexes []base.Index
}

// Bots implements base.PacklistIntegration.
func (s *sakuraServ) Bots() (bots []string) {
	for _, index := range s.indexes {
		bots = append(bots, index.Bot)
	}
	return bots
}

// Name implements base.PacklistIntegration.
func (s *sakuraServ) Name() string {
	return Name
}

func (s *sakuraServ) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(Name)
}

// Packs implements base.PacklistIntegration.
func (s *sakuraServ) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	for _, index := range s.indexes {
		dccMessage, err := s.ircClient.OneshotRequestPack(ctx, index.Bot)
		if err != nil {
			return nil, err
		}

		listBufferBytes := make([]byte, dccMessage.SizeInBytes)
		listBuffer := bytes.NewBuffer(listBufferBytes)

		dccInstance := dcc.NewDCC(
			dccMessage.AddrPort.String(),
			int(dccMessage.SizeInBytes),
			listBuffer,
		)

		progressC, errC := dccInstance.Run(ctx)
	DCC_SELECT:
		for {
			select {
			case err, ok := <-errC:
				if !ok {
					break DCC_SELECT
				}
				if err != nil {
					return nil, err
				}
			case progress := <-progressC:
				s.L(ctx).Info(
					"progress for indexer",
					zap.String("filename", dccMessage.Filename),
					zap.String("index", index.Bot),
					zap.Any("progress", progress),
				)
			}
		}

		parsedPacks, err := helpers.ParseTXTFromReader(index.Bot, listBuffer)
		if err != nil {
			return nil, err
		}

		// GC
		listBuffer = nil
		listBufferBytes = nil

		packs = append(packs, parsedPacks...)
	}
	return packs, nil
}

func New(ircClient irc.Client) base.PacklistIntegration {
	return &sakuraServ{
		ircClient: ircClient,
		indexes: []base.Index{
			{
				URI: "", // Unused
				Bot: "SakuraServ",
			},
		},
	}
}
