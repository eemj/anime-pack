package kawaiiyuri

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "Kawaiiyuri"

type kawaiiyuri struct {
	base.PacklistIntegration

	indexes []base.Index
}

func New() base.PacklistIntegration {
	return &kawaiiyuri{
		indexes: []base.Index{
			{
				URI: "https://shinobu.kawaiiyuri.com/xdcc.txt",
				Bot: "[CMS]Shinobu",
			},
		},
	}
}

func (s *kawaiiyuri) Bots() (bots []string) {
	for _, index := range s.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (s *kawaiiyuri) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.ParseTXT,
		s.indexes,
	)
}

func (s *kawaiiyuri) Name() string { return Name }
