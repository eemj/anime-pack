package base

import (
	"context"
	"fmt"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

type Index struct {
	URI string `json:"uri,omitempty"`
	Bot string `json:"bot,omitempty"`
}

func (i Index) String() string {
	uriEmpty := i.URI != ""
	botEmpty := i.Bot != ""

	switch {
	case uriEmpty && botEmpty:
		return "n/a"
	case uriEmpty:
		return i.Bot
	case botEmpty:
		return i.URI
	}

	return fmt.Sprintf("%s: %s", i.Bot, i.URI)
}

type PacklistIntegration interface {
	Name() string
	Bots() []string
	Packs(ctx context.Context) (packs []*domain.Pack, err error)
}
