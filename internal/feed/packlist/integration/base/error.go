package base

import (
	"fmt"
	"strings"
)

type PackError struct {
	Index Index
	Err   error
}

func (e PackError) Error() string {
	return fmt.Sprintf(
		"%s: %v",
		e.Index,
		e.Err,
	)
}

type Errors struct {
	Errors []error
}

func (e Errors) Error() string {
	sb := new(strings.Builder)
	n := (len(e.Errors) - 1)

	for index, err := range e.Errors {
		sb.WriteString(err.Error())

		if index < n {
			sb.WriteString(", ")
		}
	}

	return sb.String()
}
