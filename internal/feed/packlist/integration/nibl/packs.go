package nibl

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const getPacksUri = "https://api.nibl.co.uk/nibl/packs/%d"

type pack struct {
	Number int64  `json:"number"`
	Name   string `json:"name"`
	Size   int64  `json:"sizekbits"`
}

type getPacksResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Content []pack `json:"content"`
}

func getPacks(botName string, botID int) (packs []*domain.Pack, err error) {
	req, err := http.NewRequest(
		http.MethodGet,
		fmt.Sprintf(getPacksUri, botID),
		nil,
	)
	if err != nil {
		return nil, err
	}

	utils.SetUserAgent(req)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	packsResponse := new(getPacksResponse)

	if err := json.NewDecoder(res.Body).Decode(packsResponse); err != nil {
		return nil, err
	}

	if packsResponse.Status == "OK" {
		for _, pack := range packsResponse.Content {
			packs = append(packs, &domain.Pack{
				Bot:      botName,
				Size:     pack.Size,
				Pack:     pack.Number,
				Filename: pack.Name,
			})
		}
	} else {
		err = errors.New(packsResponse.Message)
	}

	return
}
