package nibl

import (
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const getBotsUri = "https://api.nibl.co.uk/nibl/bots"

type bot struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type getBotsResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Content []bot  `json:"content"`
}

func getBots() (bots []bot, err error) {
	req, err := http.NewRequest(
		http.MethodGet,
		getBotsUri,
		nil,
	)
	if err != nil {
		return nil, err
	}

	utils.SetUserAgent(req)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	botsResponse := new(getBotsResponse)

	if err = json.NewDecoder(res.Body).Decode(botsResponse); err != nil {
		return nil, err
	}

	if botsResponse.Status == "OK" {
		bots = botsResponse.Content
	} else {
		err = errors.New(botsResponse.Message)
	}

	return
}
