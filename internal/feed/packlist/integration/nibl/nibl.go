package nibl

import (
	"context"
	"sync"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
)

const Name = "NIBL"

type nibl struct {
	base.PacklistIntegration

	rwmutex sync.RWMutex
	bots    []bot
}

func New() base.PacklistIntegration {
	return &nibl{
		bots: []bot{
			{Name: "O-L|Releases"},

			{Name: "SaberLily"},
			{Name: "Kerbster"},
			{Name: "Fincayra"},

			{Name: "E-D|Raphtalia"},

			{Name: "KRP|Lucinal"},

			{Name: "[Migoto]Kobato"},

			{Name: "pcela-anime|Archiwum"},
			{Name: "pcela-anime|BiriBiri"},

			{Name: "[BR]-Yuna"},

			{Name: "aurika"},

			{Name: "L-E|Ayukawa"},
			{Name: "L-E|Yawara"},
		},
	}
}

func (n *nibl) Bots() (bots []string) {
	n.rwmutex.RLock()
	defer n.rwmutex.RUnlock()
	for _, bot := range n.bots {
		bots = append(bots, bot.Name)
	}
	return
}

// unassigned returns a list of bots whose ID is set to 0.
func (n *nibl) unassigned() (bots []bot, err error) {
	n.rwmutex.RLock()
	defer n.rwmutex.RUnlock()
	bots = make([]bot, 0)
	for _, bot := range n.bots {
		if bot.ID == 0 {
			bots = append(bots, bot)
		}
	}
	return
}

func (n *nibl) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	// Retrieve the bots first..
	// (make sure that all the bots have an assigned ID to their names)
	unassignedBots, err := n.unassigned()

	if len(unassignedBots) > 0 {
		bots, err := getBots()
		if err != nil {
			return nil, err
		}

		for index, unassignedBot := range unassignedBots {
			for _, bot := range bots {
				if unassignedBot.Name == bot.Name {
					unassignedBots[index].ID = bot.ID
					break
				}
			}
		}

		// Save the ID's to the assigned bots.
		n.rwmutex.Lock()
		for _, unassignedBot := range unassignedBots {
			for index, bot := range n.bots {
				if unassignedBot.Name == bot.Name {
					n.bots[index].ID = unassignedBot.ID
					break
				}
			}
		}
		n.rwmutex.Unlock()
	}

	// After we checked if the bots are assigned, we'll
	// retrieve the packs from the respective API.
	for _, bot := range n.bots {
		if bot.ID > 0 {
			indexPacks, err := getPacks(bot.Name, bot.ID)
			if err != nil {
				return nil, err
			}

			packs = append(packs, indexPacks...)
		}
	}

	return
}

func (n *nibl) Name() string { return Name }
