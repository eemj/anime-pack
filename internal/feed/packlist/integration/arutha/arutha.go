package arutha

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "Arutha"

type arutha struct {
	indexes []base.Index
}

func New() *arutha {
	return &arutha{
		indexes: []base.Index{
			{
				URI: "https://arutha.info/xdcc/CR-ARUTHA.NEW.xdcc.txt",
				Bot: "CR-ARUTHA|NEW",
			},
			{
				URI: "https://arutha.info/xdcc/CR-ARUTHA.IPv6.NEW.xdcc.txt",
				Bot: "CR-ARUTHA-IPv6|NEW",
			},
			{
				URI: "https://arutha.info/xdcc/CR-NL.NEW.xdcc.txt",
				Bot: "CR-HOLLAND|NEW",
			},
			{
				URI: "https://arutha.info/xdcc/CR-NL.IPv6.NEW.xdcc.txt",
				Bot: "CR-HOLLAND-IPv6|NEW",
			},
			{
				URI: "https://arutha.info/xdcc/Arutha.Naruto.xdcc.txt",
				Bot: "Arutha|Naruto",
			},
			{
				URI: "https://arutha.info/xdcc/Arutha.One-Piece.xdcc.txt",
				Bot: "Arutha|One-Piece",
			},
			{
				URI: "https://arutha.info/xdcc/Arutha.DragonBall.xdcc.txt",
				Bot: "Arutha|DragonBall",
			},
			{
				URI: "https://arutha.info/xdcc/Arutha.Bleach.xdcc.txt",
				Bot: "Arutha|Bleach",
			},
			{
				URI: "https://arutha.info/xdcc/Arutha.xdcc.txt",
				Bot: "Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/ARUTHA-BATCH.1080p.xdcc.txt",
				Bot: "ARUTHA-BATCH|1080p",
			},
			{
				URI: "https://arutha.info/xdcc/ARUTHA-BATCH.720p.xdcc.txt",
				Bot: "ARUTHA-BATCH|720p",
			},
			{
				URI: "https://arutha.info/xdcc/ARUTHA-BATCH.SD.xdcc.txt",
				Bot: "ARUTHA-BATCH|SD",
			},
			{
				URI: "https://arutha.info/xdcc/[FFF]Arutha.xdcc.txt",
				Bot: "[FFF]Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/[DCTP]Arutha.xdcc.txt",
				Bot: "[DCTP]Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/Ghouls.Arutha.xdcc.txt",
				Bot: "Ghouls|Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/Saizen.Arutha.xdcc.txt",
				Bot: "Saizen|Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/THORA.Arutha.xdcc.txt",
				Bot: "THORA|Arutha",
			},
			{
				URI: "https://arutha.info/xdcc/AEOG.Arutha.xdcc.txt",
				Bot: "AEOG|Arutha",
			},
		},
	}
}

func (a *arutha) Bots() (bots []string) {
	for _, index := range a.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (a *arutha) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.InsecureParseTXT,
		a.indexes,
	)
}

func (a *arutha) Name() string { return Name }
