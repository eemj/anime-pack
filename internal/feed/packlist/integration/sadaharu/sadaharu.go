package sadaharu

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "Sadaharu"

type sadaharu struct {
	base.PacklistIntegration

	indexes []base.Index
}

func New() base.PacklistIntegration {
	return &sadaharu{
		indexes: []base.Index{
			{
				URI: "https://gin.sadaharu.eu/Gin.txt",
				Bot: "Ginpachi-Sensei",
			},
		},
	}
}

func (s *sadaharu) Bots() (bots []string) {
	for _, index := range s.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (s *sadaharu) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.ParseTXT,
		s.indexes,
	)
}

func (s *sadaharu) Name() string { return Name }
