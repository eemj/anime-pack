package moodkiller

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "MoodKiller"

type moodkiller struct {
	base.PacklistIntegration

	indexes []base.Index
}

func New() base.PacklistIntegration {
	return &moodkiller{
		indexes: []base.Index{
			{
				URI: "https://rory.animk.info/RoryXDCC.txt",
				Bot: "Rory|XDCC",
			},
		},
	}
}

func (m *moodkiller) Bots() (bots []string) {
	for _, index := range m.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (m *moodkiller) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.ParseTXT,
		m.indexes,
	)
}

func (m *moodkiller) Name() string { return Name }
