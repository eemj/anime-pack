package chinesecartoons

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "Chinese-Cartoons"

type chineseCartoons struct {
	base.PacklistIntegration

	indexes []base.Index
}

func New() base.PacklistIntegration {
	return &chineseCartoons{
		indexes: []base.Index{
			{
				URI: "http://chinesecartoons.club/packlist/txt",
				Bot: "Chinese-Cartoons",
			},
		},
	}
}

func (c *chineseCartoons) Bots() (bots []string) {
	for _, index := range c.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (c *chineseCartoons) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.ParseTXT,
		c.indexes,
	)
}

func (c *chineseCartoons) Name() string { return Name }
