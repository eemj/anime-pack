package helpers

import (
	"bufio"
	"context"
	"crypto/tls"
	"errors"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

var packlistLineRegexp = regexp.MustCompile(`^#(\d+)\s+(\d+)x\s\[\s?([0-9.]+)(\w)]\s(.+)$`)

func ParseTXTFromReader(bot string, reader io.Reader) (packs []*domain.Pack, err error) {
	bufioReader := bufio.NewReader(reader)
	for {
		msg, err := bufioReader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}

			return packs, err
		}

		msg = strings.TrimSpace(msg)

		if !packlistLineRegexp.MatchString(msg) {
			continue
		}

		matches := packlistLineRegexp.FindStringSubmatch(msg)

		pack, _ := strconv.Atoi(matches[1])
		filename := matches[5]
		sizeBytes, err := humanize.ParseBytes(matches[3] + matches[4])
		if err != nil {
			return nil, err
		}

		packs = append(packs, &domain.Pack{
			Bot:      bot,
			Pack:     int64(pack),
			Size:     int64(sizeBytes),
			Filename: filename,
		})
	}
	return packs, nil
}

func parseTXT(ctx context.Context, httpClient *http.Client, bot, uri string) (packs []*domain.Pack, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}

	utils.SetUserAgent(req)

	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return ParseTXTFromReader(bot, res.Body)
}

type TXTParser func(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error)

func InsecureParseTXT(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error) {
	transport := http.DefaultTransport.(*http.Transport)
	transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	httpClient := &http.Client{Timeout: (30 * time.Second), Transport: transport}

	return parseTXT(ctx, httpClient, bot, uri)
}

func ParseTXT(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error) {
	httpClient := &http.Client{Timeout: (30 * time.Second)}

	return parseTXT(ctx, httpClient, bot, uri)
}

func GetTXTPacks(ctx context.Context, parser TXTParser, indexes []base.Index) (packs []*domain.Pack, err error) {
	for _, index := range indexes {
		indexPacks, indexErr := parser(ctx, index.Bot, index.URI)

		if indexErr != nil {
			err = errors.Join(err, base.PackError{
				Index: index,
				Err:   indexErr,
			})
			continue
		}

		packs = append(packs, indexPacks...)
	}
	return packs, err
}
