package helpers

import (
	"context"
	"crypto/tls"
	"encoding/xml"
	"io"
	"net/http"
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

type xmlResponse struct {
	XMLName  xml.Name `xml:"iroffer"`
	Text     string   `xml:",chardata"`
	Packlist struct {
		Text string `xml:",chardata"`
		Pack []struct {
			Text      string `xml:",chardata"`
			Packname  string `xml:"packname"`
			Packnr    int64  `xml:"packnr"`
			Packsize  string `xml:"packsize"`
			Packbytes int64  `xml:"packbytes"`
			Packgets  int    `xml:"packgets"`
			Packcolor int    `xml:"packcolor"`
			Adddate   string `xml:"adddate"`
			Md5sum    string `xml:"md5sum"`
		} `xml:"pack"`
	} `xml:"packlist"`
}

func ParseXMLFromReader(bot string, reader io.Reader) (packs []*domain.Pack, err error) {
	var xmlRes xmlResponse
	decoder := xml.NewDecoder(reader)
	err = decoder.Decode(&xmlRes)
	if err != nil {
		return nil, err
	}
	for _, pack := range xmlRes.Packlist.Pack {
		packs = append(packs, &domain.Pack{
			Bot:      bot,
			Pack:     pack.Packnr,
			Filename: pack.Packname,
			Size:     pack.Packbytes,
		})
	}
	return packs, nil
}

func parseXML(ctx context.Context, httpClient *http.Client, bot, uri string) (packs []*domain.Pack, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	utils.SetUserAgent(req)

	res, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return ParseXMLFromReader(bot, res.Body)
}

type XMLParser func(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error)

func InsecureParseXML(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error) {
	transport := http.DefaultTransport.(*http.Transport)
	transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	httpClient := &http.Client{Timeout: (30 * time.Second), Transport: transport}

	return parseXML(ctx, httpClient, bot, uri)
}

func ParseXML(ctx context.Context, bot, uri string) (packs []*domain.Pack, err error) {
	httpClient := &http.Client{Timeout: (30 * time.Second)}

	return parseXML(ctx, httpClient, bot, uri)
}
