package oyatsu

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/base"
	"gitlab.com/eemj/anime-pack/internal/feed/packlist/integration/helpers"
)

const Name = "Oyatsu"

type oyatsu struct {
	base.PacklistIntegration

	indexes []base.Index
}

func New() base.PacklistIntegration {
	return &oyatsu{
		indexes: []base.Index{
			{
				URI: "http://oyatsu-jikan.org:32000/",
				Bot: "[Oyatsu]Sena",
			},
		},
	}
}

func (s *oyatsu) Bots() (bots []string) {
	for _, index := range s.indexes {
		bots = append(bots, index.Bot)
	}
	return
}

func (s *oyatsu) Packs(ctx context.Context) (packs []*domain.Pack, err error) {
	return helpers.GetTXTPacks(
		ctx,
		helpers.ParseTXT,
		s.indexes,
	)
}

func (s *oyatsu) Name() string { return Name }
