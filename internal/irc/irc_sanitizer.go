package irc

import (
	"regexp"
	"strings"
)

var (
	textFormatting = regexp.MustCompile(`\x03(((\d{1,2},\d{2})|\d{2})?)|\x04((([a-fA-F0-9]{6},[a-fA-F0-9]{6})|[a-fA-F0-9]{6})?)`)
)

// sanitize will return text without any IRC formatting
func sanitize(in string) (out string) {
	if strings.ContainsRune(in, '\x03') || strings.ContainsRune(in, '\x04') {
		in = textFormatting.ReplaceAllString(in, "")
	}

	for _, char := range in {
		if int(rune(char)) >= 0x20 && char != '\x7F' {
			out += string(char)
		}
	}

	return
}

func getFieldsByWhitespaceExcludingQuotes(raw string) []string {
	var (
		fields        []string
		inQuotes      bool
		stringBuilder strings.Builder
	)

	for _, char := range raw {
		switch char {
		case '"':
			inQuotes = !inQuotes
		case ' ':
			if !inQuotes {
				fields = append(fields, stringBuilder.String())
				stringBuilder.Reset()
				continue
			}
			fallthrough
		default:
			stringBuilder.WriteRune(char)
		}
	}

	// Append the last field if not already appended
	if stringBuilder.Len() > 0 {
		fields = append(fields, stringBuilder.String())
	}

	return fields
}
