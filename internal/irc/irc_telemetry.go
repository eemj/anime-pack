package irc

import (
	"context"
	"strings"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/common"
	semconv "go.opentelemetry.io/otel/semconv/v1.21.0"
	"go.opentelemetry.io/otel/trace"
)

const (
	TagTraceID string = "trace-id"
	TagSpanID  string = "span-id"
)

func (c *client) createSpanFromEvent(event *girc.Event) (context.Context, trace.Span) {
	// Check the event's tags - if we have the following tags set:
	// - `trace-id`
	// - `span-id`
	//
	// We'll create a remote child span context with the above values,
	// otherwise, we'll just create a new parent span and set the tag values.
	// This is useful, because we want to propogate the telemetry information - in-case
	// the RecoverFunc is triggered.

	rawTraceID, okTraceID := event.Tags.Get(TagTraceID)
	rawSpanID, okSpanID := event.Tags.Get(TagSpanID)

	// We managed to get telemetry information from the tags.
	if okTraceID && okSpanID {
		spanContext := trace.NewSpanContext(trace.SpanContextConfig{
			TraceID:    trace.TraceID([]byte(rawTraceID)),
			SpanID:     trace.SpanID([]byte(rawSpanID)),
			TraceFlags: trace.FlagsSampled,
			Remote:     true,
		})
		ctx := trace.ContextWithRemoteSpanContext(context.Background(), spanContext)
		return ctx, trace.SpanFromContext(ctx)
	}

	// Create a new span - we didn't manage to get information.
	var (
		sb                strings.Builder
		isCTCP, ctcpEvent = event.IsCTCP()
	)
	sb.WriteString("(")
	sb.WriteString(event.Command)
	sb.WriteRune(')')
	if event.Source != nil && event.Source.Name != "" {
		sb.WriteString(" ")
		sb.WriteString(event.Source.Name)
	}
	if isCTCP {
		sb.WriteString(" ")
		sb.WriteString(ctcpEvent.Command)
	}

	ctx, span := c.tracer.Start(
		context.Background(),
		sb.String(),
		trace.WithAttributes(semconv.ServiceName(common.IrcModuleName)),
	)

	// Since opentelemetry logging for Go is still alpha -
	// let's just add the stringified event as an event.
	span.AddEvent(event.String())

	// Mark the tags that telemetry was added to this event.
	event.Tags.Set(TagTraceID, span.SpanContext().TraceID().String())
	event.Tags.Set(TagSpanID, span.SpanContext().SpanID().String())

	return ctx, span
}
