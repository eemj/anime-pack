package irc

import (
	"encoding/binary"
	"fmt"
	"net/netip"
	"path"
	"strconv"
	"strings"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
)

const CTCP_DCC = "DCC"

type PackType int

const (
	PackType_PACKLIST PackType = iota
	PackType_OTHER
)

type DCCSendMessage struct {
	Source      *girc.Source   `json:"source,omitempty"`
	AddrPort    netip.AddrPort `json:"addr_port,omitempty"`
	Filename    string         `json:"filename,omitempty"`
	SizeInBytes uint64         `json:"size_in_bytes,omitempty"`
	PackType    PackType       `json:"pack_type,omitempty"`
	Entry       *entry.Entry   `json:"entry,omitempty"`
}

func IsCTCPDCCSEND(event girc.CTCPEvent) bool {
	return event.Command == CTCP_DCC &&
		strings.HasPrefix(event.Text, "SEND ")
}

func ParseCTCPDCCSEND(event girc.CTCPEvent) (*DCCSendMessage, error) {
	if event.Command != CTCP_DCC {
		return nil, fmt.Errorf("command is not '%s' but it's '%s'", CTCP_DCC, event.Command)
	}

	var (
		fields = getFieldsByWhitespaceExcludingQuotes(event.Text)
		msg    = &DCCSendMessage{Source: event.Source, Filename: fields[1]}
	)
	port, err := strconv.ParseUint(fields[3], 10, 16)
	if err != nil {
		return nil, err
	}

	// First try to get the address with the dotted decimal, or maybe it's an IPv6
	addr, err := netip.ParseAddr(fields[2])
	if err != nil {
		// Then it's a number, big-endian it in a byte slice and create an address from it.
		octet, err := strconv.ParseUint(fields[2], 10, 32)
		if err == nil {
			bytes := make([]byte, 4)
			binary.BigEndian.PutUint32(bytes, uint32(octet))
			newAddr, ok := netip.AddrFromSlice(bytes)
			if ok {
				addr = newAddr
			}
		}
	}

	msg.SizeInBytes, err = strconv.ParseUint(fields[4], 10, 64)
	if err != nil {
		return nil, err
	}

	msg.AddrPort = netip.AddrPortFrom(addr, uint16(port))

	if msg.Filename != "" {
		switch strings.ToUpper(path.Ext(msg.Filename)) {
		case ".TXT":
			{
				msg.PackType = PackType_PACKLIST
			}
		default:
			{
				msg.PackType = PackType_OTHER
				msg.Entry = groups.Parse(msg.Filename)
			}
		}
	}

	return msg, nil
}
