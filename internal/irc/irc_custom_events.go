package irc

import "context"

type IRCCustomEvent string

const (
	Event_NOTICE_PENDING_TRANSFER   IRCCustomEvent = "event_notice_pending_transfer"
	Event_PRIVMSG_ANNOUNCE_PACK     IRCCustomEvent = "event_privmsg_announce_pack"
	Event_PRIVMSG_TRANSFER_IN_QUEUE IRCCustomEvent = "event_privmsg_transfer_in_queue"
	Event_CONNECTION_STATE_UPDATE   IRCCustomEvent = "event_connection_state_update"
	Event_BOT_STATE_UPDATE          IRCCustomEvent = "event_bot_state_update"
	Event_CTCP_DCC                  IRCCustomEvent = "event_ctcp_dcc"
)

type OnEvent func(
	ctx context.Context,
	event IRCCustomEvent,
	msg interface{},
) (err error)

func (cl *client) OnEvent(fn OnEvent) {
	cl.onEventFn = fn
}
