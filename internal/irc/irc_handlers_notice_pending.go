package irc

import (
	"context"
	"strings"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"go.uber.org/zap"
)

func (cl *client) handleNoticePendingTransfer(ctx context.Context, msg *PendingTransferMessage) (err error) {
	if cl.onEventFn != nil {
		cl.onEventFn(ctx, Event_NOTICE_PENDING_TRANSFER, msg)

		err := pubsub.PublishMessage(ctx, cl.publisher, pubsub.Topic_IRC_NOTICE_PENDING_TRANSFER, msg)
		if err != nil {
			return err
		}
	}

	// TODO(eemj): An improvement - We're given a duration here, if the occurences is greater than 1 -
	// we should add extra time to the other items
	cl.cancelUnused(ctx, msg.Source)

	return nil
}

func (cl *client) cancelUnused(ctx context.Context, source *girc.Source) {
	occurrences := 0

	// Filter through the Active items so that we'll retrieve details
	// and match the items with the current bot.
	for _, state := range cl.queue.ActiveItems() {
		detail, ok := state.Current()
		if ok && strings.EqualFold(detail.Bot, source.ID()) {
			occurrences++
		}
	}

	L(ctx).
		Info(
			"queue item occurrences for bot is",
			zap.Stringer("source", source),
			zap.Int("occurrences", occurrences),
		)

	// If we only have this occurence with the bot cancel,
	// If occurences are greater than 2, we might interrupt another running transfer, since
	// some bots can have more than 1 transfer.
	if occurrences <= 1 {
		cl.girc.Cmd.Message(
			source.Name,
			"XDCC CANCEL",
		)
	}
}
