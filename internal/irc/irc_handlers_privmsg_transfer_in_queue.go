package irc

import (
	"context"
	"strings"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"go.uber.org/zap"
)

func (cl *client) handlePrivmsgTransferInQueue(ctx context.Context, msg *TransferInQueueMessage) (err error) {
	updateUnmanagedFn := func(key queue.Key, value *queue.State) bool {
		detail, ok := value.Current()

		foundInQueue := ok &&
			strings.EqualFold(detail.Bot, msg.Source.ID()) &&
			detail.Pack == msg.Pack

		if foundInQueue {
			L(ctx).
				Info(
					"found in queue, toggling item as un-managed",
					zap.Stringer("key", key.(domain.AnimeBase)),
				)
		}

		return foundInQueue
	}

	cl.queue.UpdateUnmanaged(updateUnmanagedFn)

	if cl.onEventFn != nil {
		cl.onEventFn(ctx, Event_PRIVMSG_TRANSFER_IN_QUEUE, msg)

		err := pubsub.PublishMessage(ctx, cl.publisher, pubsub.Topic_IRC_PRIVATE_MESSAGE_TRANSFER_IN_QUEUE, msg)
		if err != nil {
			return err
		}
	}

	return nil
}
