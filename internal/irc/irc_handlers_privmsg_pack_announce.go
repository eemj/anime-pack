package irc

import (
	"context"

	"github.com/dustin/go-humanize"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

func (cl *client) handlePrivmsgPackAnnounce(ctx context.Context, msg *NewPackAnnounceMessage) (err error) {
	if msg.Entry == nil {
		L(ctx).Warn(
			"unable to entry->groups parse the filename",
			zap.String("filename", msg.Filename),
		)
		return nil
	}

	title, _, err := cl.services.Library().Match(ctx, params.MatchLibraryParams{
		TitleName:         msg.Entry.Title,
		TitleSeasonNumber: msg.Entry.SeasonNumber,
		TitleYear:         msg.Entry.Year,
	})
	if err != nil {
		return err
	}

	episode, err := cl.services.Episode().Create(ctx, params.InsertEpisodeParamsItem{
		Name: msg.Entry.Episode,
	})
	if err != nil {
		return err
	}

	titleEpisode, err := cl.services.Title().Associations().TitleEpisode().Create(ctx, params.InsertTitleEpisodeParamsItem{
		TitleID:   title.ID,
		EpisodeID: episode.ID,
	})
	if err != nil {
		return err
	}

	bot, err := cl.services.Bot().Create(ctx, params.InsertBotParamsItem{
		Name: msg.Source.Name,
	})
	if err != nil {
		return err
	}

	var qualityID *int64
	if msg.Entry.VideoResolution.Height > 0 {
		quality, err := cl.services.Quality().Create(ctx, params.InsertQualityParamsItem{
			Height: int64(msg.Entry.VideoResolution.Height),
		})
		if err != nil {
			return err
		}
		if quality != nil {
			qualityID = &quality.ID
		}
	}

	releaseGroup, err := cl.services.ReleaseGroup().Create(ctx, params.InsertReleaseGroupParamsItem{
		Name: msg.Entry.ReleaseGroup,
	})
	if err != nil {
		return err
	}

	escapedFilename := utils.RemoveEvilCharacters(msg.Filename)

	L(ctx).Debug(
		"adding a new xdcc entry",
		zap.Any("bot", bot),
		zap.Any("title", title),
		zap.Any("episode", episode),
		zap.Any("title_episode", titleEpisode),
		zap.Int64p("quality_id", qualityID),
		zap.Uint64("size", msg.SizeInBytes),
		zap.String("escaped_filename", escapedFilename),
	)

	if titleEpisode != nil && bot != nil && releaseGroup != nil {
		err = cl.services.Xdcc().CreateMany(ctx, params.InsertXDCCParams{
			Items: []params.InsertXDCCParamsItem{{
				BotID:           bot.ID,
				TitleEpisodeID:  titleEpisode.ID,
				ReleaseGroupID:  releaseGroup.ID,
				Pack:            msg.Pack,
				Size:            int64(msg.SizeInBytes),
				Filename:        msg.Filename,
				EscapedFilename: escapedFilename,
				QualityID:       qualityID,
			}},
		})
		if err != nil {
			return err
		}

		L(ctx).Info(
			"added a new xdcc entry",
			zap.String("filename", msg.Filename),
			zap.String("bot_name", bot.Name),
			zap.Int64("pack", msg.Pack),
			zap.String("size", humanize.Bytes(msg.SizeInBytes)),
		)
	}

	if cl.onEventFn != nil {
		cl.onEventFn(ctx, Event_PRIVMSG_ANNOUNCE_PACK, msg)

		err := pubsub.PublishMessage(ctx, cl.publisher, pubsub.Topic_IRC_PRIVATE_ANNOUNCE_PACK, msg)
		if err != nil {
			return err
		}
	}

	return nil
}
