package irc

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/eemj/anime-pack/internal/pubsub"
)

func (c *client) OneshotRequestPack(ctx context.Context, nick string) (dcc *DCCSendMessage, err error) {
	// Ensure that the user is connected to one of the channels we are in.
	user := c.girc.LookupUser(nick)
	if user == nil {
		return nil, fmt.Errorf("user(%s) is not connected", nick)
	}

	subscribeCtx, cancel := context.WithTimeout(ctx, (45 * time.Second))
	defer cancel()
	messages, err := c.subscriber.Subscribe(subscribeCtx, pubsub.Topic_IRC_CTCP_DCC)
	if err != nil {
		return nil, err
	}

	c.girc.Cmd.Message(nick, "XDCC SEND -1")

MESSAGE_POLL:
	for {
		select {
		case <-ctx.Done():
			break MESSAGE_POLL
		case message := <-messages:
			if message != nil {
				var currentDccMsg DCCSendMessage
				err = json.Unmarshal(message.Payload, &currentDccMsg)
				if err != nil {
					return nil, err
				}

				message.Ack()

				if currentDccMsg.PackType == PackType_PACKLIST &&
					currentDccMsg.Source.Name == nick {
					return &currentDccMsg, nil
				}
			}
		}
	}

	return nil, fmt.Errorf("did not get the pack in-time from user('%s')", nick)
}
