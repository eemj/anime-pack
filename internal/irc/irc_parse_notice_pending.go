package irc

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/lrstanley/girc"
)

var (
	pendingTransferExp = regexp.MustCompile(`You have a DCC pending, Set your client to receive the transfer\. Type "\/MSG \S+ XDCC CANCEL\" to abort the transfer\. \((?P<numeric>\d+) (?P<unit>\w+) remaining until timeout\)`)
)

type PendingTransferMessage struct {
	Source   *girc.Source  `json:"source,omitempty"`
	Duration time.Duration `json:"duration,omitempty"`
}

func IsPendingTransfer(event girc.Event) bool {
	if event.Command != girc.NOTICE {
		return false
	}
	text := sanitize(event.String())
	return pendingTransferExp.MatchString(text)
}

func ParsePendingTransfer(event girc.Event) (*PendingTransferMessage, error) {
	if event.Command != girc.NOTICE {
		return nil, fmt.Errorf("command is not '%s' but it's '%s'", girc.NOTICE, event.Command)
	}

	text := sanitize(event.String())
	if !pendingTransferExp.MatchString(text) {
		return nil, fmt.Errorf("unable to match '%s' for pending transfer", text)
	}

	var (
		msg     = &PendingTransferMessage{Source: event.Source, Duration: 1}
		matches = pendingTransferExp.FindStringSubmatch(text)
	)
	for index, value := range matches {
		switch strings.ToLower(pendingTransferExp.SubexpNames()[index]) {
		case `numeric`:
			{
				numericValue, err := strconv.Atoi(value)
				if err != nil {
					return nil, err
				}
				msg.Duration += time.Duration(numericValue)
			}
		case `unit`:
			{
				switch strings.ToLower(value) {
				case `days`:
					msg.Duration *= (24 * time.Hour)
				case `hours`:
					msg.Duration *= (1 * time.Hour)
				case `minutes`:
					msg.Duration *= (1 * time.Minute)
				case `seconds`:
					msg.Duration *= (1 * time.Second)
				}
			}
		}
	}

	return msg, nil
}
