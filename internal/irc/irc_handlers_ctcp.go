package irc

import (
	"context"
	"fmt"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"go.jamie.mt/toolbox/telemetry"
	"go.uber.org/zap"
)

func (cl *client) ctcpDcc() girc.CTCPHandler {
	return func(c *girc.Client, ctcp girc.CTCPEvent) {
		switch {
		case IsCTCPDCCSEND(ctcp):
			{
				ctx, span := cl.createSpanFromEvent(ctcp.Origin)
				defer span.End()

				msg, err := ParseCTCPDCCSEND(ctcp)
				if err != nil {
					L(ctx).Error(
						"failed to parse `dcc_send` event",
						zap.String("raw", ctcp.Origin.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
				L(ctx).Info(
					"dcc message",
					zap.Stringer("address_port", msg.AddrPort),
					zap.String("filename", msg.Filename),
					zap.Uint64("size", msg.SizeInBytes),
					zap.Stringer("source", msg.Source),
				)
				if err := cl.handleCTCPDCCSend(ctx, msg); err != nil {
					L(ctx).Error(
						"failed to handle `dcc_send` event",
						zap.String("raw", ctcp.Origin.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
			}
		}
	}
}

func (cl *client) handleCTCPDCCSend(ctx context.Context, msg *DCCSendMessage) (err error) {
	// TODO(eemj): Notify the socket that a DCC operation started.
	if cl.onEventFn != nil {
		cl.onEventFn(ctx, Event_CTCP_DCC, msg)

		err := pubsub.PublishMessage(ctx, cl.publisher, pubsub.Topic_IRC_CTCP_DCC, msg)
		if err != nil {
			return err
		}
	}

	switch {
	// Packlists aren't handled here.
	case msg.PackType == PackType_PACKLIST:
		return nil
	// Having the entry populated is a hard requirement.
	case msg.Entry == nil:
		L(ctx).Warn(
			"unable to entry->groups parse the filename",
			zap.String("filename", msg.Filename),
		)
		return nil
	}

	xdcc, err := cl.services.Xdcc().First(ctx, params.FirstXDCCParams{
		WithoutVideo:    true,
		EscapedFilename: &msg.Filename,
	})
	if err != nil {
		return err
	}
	if xdcc == nil {
		return fmt.Errorf(
			"xdcc with the escaped filename('%s') was not found",
			msg.Filename,
		)
	}

	titleAnime, err := cl.services.Title().Associations().TitleAnime().First(ctx, params.FirstTitleAnimeParams{
		TitleID: &xdcc.TitleEpisode.TitleID,
	})
	if err != nil {
		return err
	}
	if titleAnime == nil {
		return fmt.Errorf(
			"title id('%d') does not have any associated animes",
			xdcc.TitleEpisode.TitleID,
		)
	}

	var (
		anime = domain.AnimeBase{
			ID:           titleAnime.AnimeID,
			Title:        titleAnime.Anime.Title,
			ReleaseGroup: xdcc.ReleaseGroup.Name,
			Height:       msg.Entry.VideoResolution.Height,
			Episode:      msg.Entry.Episode,
		}
		queueContains = cl.queue.Contains(anime)
	)
	L(ctx).Info(
		"anime base created and checked if queue exists",
		zap.Stringer("anime", anime),
		zap.Bool("queue_contains", queueContains),
		zap.Stringer("addr_port", msg.AddrPort),
		zap.String("bot_name", xdcc.Bot.Name),
		zap.Uint64("size_in_bytes", msg.SizeInBytes),
	)

	if !queueContains {
		L(ctx).Info("queue does not contain, attempting to cancel this unused anime")
		cl.cancelUnused(ctx, msg.Source)
		return nil
	}

	runner, err := cl.services.Runner().Assign(
		anime,
		msg.SizeInBytes,
		msg.AddrPort.String(),
	)
	if err != nil {
		return err
	}

	L(ctx).Info(
		"anime assigned to a runner",
		zap.Stringer("runner", runner.Info()),
		zap.Stringer("anime", anime),
		zap.Stringer("addr_port", msg.AddrPort),
		zap.Uint64("size_in_bytes", msg.SizeInBytes),
	)

	return nil
}
