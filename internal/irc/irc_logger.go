package irc

import (
	"context"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/common"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type clientLogger struct {
	config girc.Config
	logger *zap.Logger
}

func (c *clientLogger) Write(bytes []byte) (n int, err error) {
	c.logger.Log(
		zap.DebugLevel,
		"event",
		zap.String("server", c.config.Server),
		zap.ByteString("event", bytes),
	)
	return len(bytes), nil
}

func L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.IrcModuleName)
}
