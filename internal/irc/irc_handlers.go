package irc

import (
	"context"

	"github.com/lrstanley/girc"
	"go.uber.org/zap"
)

func (cl *client) registerHandlers() {
	// CONNECT / CLOSED / DISCONNECT
	cl.girc.Handlers.AddHandler(girc.CLOSED, cl.connectedDisconnected())
	cl.girc.Handlers.AddHandler(girc.CONNECTED, cl.connectedDisconnected())
	cl.girc.Handlers.AddHandler(girc.DISCONNECTED, cl.connectedDisconnected())

	// DCC SEND
	cl.girc.CTCP.SetBg(CTCP_DCC, cl.ctcpDcc())

	// PRIVMSG
	cl.girc.Handlers.AddBg(girc.PRIVMSG, cl.privmsg())

	// NOTICE
	cl.girc.Handlers.AddBg(girc.NOTICE, cl.notice())

	// DEBUG
	if L(context.TODO()).Core().Enabled(zap.DebugLevel) {
		// CTCP DEBUG
		cl.girc.CTCP.SetBg(girc.ALL_EVENTS, func(c *girc.Client, ctcp girc.CTCPEvent) {
			L(context.TODO()).Debug(
				"ctcp",
				zap.String("command", ctcp.Command),
				zap.Stringer("source", ctcp.Source),
				zap.String("text", ctcp.Text),
			)
		})

		// OTHER DEBUG
		cl.girc.Handlers.Add(girc.ALL_EVENTS, func(client *girc.Client, event girc.Event) {
			switch event.Command {
			case girc.UPDATE_STATE, girc.UPDATE_GENERAL:
				return
			}

			L(context.TODO()).Debug(
				"event",
				zap.String("command", event.Command),
				zap.Stringer("source", event.Source),
				zap.String("server", client.Config.Server),
				zap.Strings("parameters", event.Params),
			)
		})
	}
}
