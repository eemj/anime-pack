package irc

import (
	"github.com/lrstanley/girc"
	"go.jamie.mt/toolbox/telemetry"
	"go.uber.org/zap"
)

func (cl *client) privmsg() girc.HandlerFunc {
	return func(c *girc.Client, event girc.Event) {
		switch {
		case IsTransferInQueue(event):
			{
				ctx, span := cl.createSpanFromEvent(&event)
				defer span.End()

				msg, err := ParseTransferInQueue(event)
				if err != nil {
					L(ctx).Error(
						"failed to parse `transfer_in_queue` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
				L(ctx).Info(
					"transfer in queue message",
					zap.String("filename", msg.Filename),
					zap.Int("transfer_limit", msg.TransferLimit),
					zap.Int("position", msg.Position),
					zap.Int64("pack", msg.Pack),
					zap.Stringer("source", msg.Source),
				)
				if err := cl.handlePrivmsgTransferInQueue(ctx, msg); err != nil {
					L(ctx).Error(
						"failed to handle `transfer_in_queue` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
			}
		case IsNewPackAnnounce(event):
			{
				ctx, span := cl.createSpanFromEvent(&event)
				defer span.End()

				msg, err := ParseNewPackAnnounce(event)
				if err != nil {
					L(ctx).Error(
						"failed to parse `pending_transfer` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
				L(ctx).Info(
					"new pack announce message",
					zap.Stringer("source", msg.Source),
					zap.Any("entry", msg.Entry),
					zap.Int64("pack", msg.Pack),
					zap.String("filename", msg.Filename),
					zap.Uint64("size", msg.SizeInBytes),
				)
				if err := cl.handlePrivmsgPackAnnounce(ctx, msg); err != nil {
					L(ctx).Error(
						"failed to handle `pending_transfer` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
			}
		}
	}
}
