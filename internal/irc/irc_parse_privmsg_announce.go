package irc

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/dustin/go-humanize"
	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"gitlab.com/eemj/anime-pack/pkg/groups/entry"
)

// Examples:
// - 4[Fresh From Teh Internets] - [1.3G] - High Card - S02E11 - Sweet Home HDTV-1080p.mkv - /MSG SakuraServ XDCC SEND 34519
// - 4[Fresh From Teh Internets] - [960M] - Synduality Noir - S01E23 - Over the Limit WEBDL-1080p.mkv - /MSG SakuraServ XDCC SEND 67320
// - [https://gin.sadaharu.eu] - [366M] - [SubsPlease] High Card - 23 (480p) [0F194399].mkv - /MSG Ginpachi-Sensei xdcc send 9177
// - From #SubsPlease, come join the one stop shop for new anime * [366M] * [SubsPlease] High Card - 23 (480p) [0F194399].mkv * /MSG CR-ARUTHA|NEW XDCC SEND 18772
// - From #SubsPlease, come join the one stop shop for new anime * [366M] * [SubsPlease] High Card - 23 (480p) [0F194399].mkv * /MSG CR-ARUTHA-IPv6|NEW XDCC SEND 18772
// - From #SubsPlease, come join the one stop shop for new anime * [366M] * [SubsPlease] High Card - 23 (480p) [0F194399].mkv * /MSG CR-HOLLAND|NEW XDCC SEND 18772
var (
	newPackAnnounceExp = regexp.MustCompile(`(?i).* . \[(?P<size>[\d\S]+)\] . "?(?P<filename>.*)"? . \/MSG \S+ xdcc send (?P<pack_number>\d+)`)
)

type NewPackAnnounceMessage struct {
	Source      *girc.Source `json:"source,omitempty"`
	SizeInBytes uint64       `json:"size_in_bytes,omitempty"`
	Filename    string       `json:"filename,omitempty"`
	Entry       *entry.Entry `json:"entry,omitempty"`
	Pack        int64        `json:"pack,omitempty"`
}

func ParseNewPackAnnounce(event girc.Event) (*NewPackAnnounceMessage, error) {
	if event.Command != girc.PRIVMSG {
		return nil, fmt.Errorf("command is not '%s' but it's '%s'", girc.PRIVMSG, event.Command)
	}

	text := sanitize(event.String())
	if !newPackAnnounceExp.MatchString(text) {
		return nil, fmt.Errorf("unable to match '%s' for new pack announce", text)
	}

	var (
		msg     = &NewPackAnnounceMessage{Source: event.Source}
		err     error
		matches = newPackAnnounceExp.FindStringSubmatch(text)
	)
	for index, value := range matches {
		switch strings.ToLower(newPackAnnounceExp.SubexpNames()[index]) {
		case `size`:
			{
				msg.SizeInBytes, err = humanize.ParseBytes(value)
				if err != nil {
					return nil, fmt.Errorf("failed to parse bytes for value '%s' due to '%w'", value, err)
				}
			}
		case `filename`:
			{
				msg.Filename = value
			}
		case `pack_number`:
			{
				msg.Pack, err = strconv.ParseInt(value, 10, 64)
				if err != nil {
					return nil, fmt.Errorf("failed to parse pack for value '%s' due to '%w'", value, err)
				}
			}
		}
	}

	if msg.Filename != "" {
		msg.Entry = groups.Parse(msg.Filename)
	}

	return msg, nil
}

func IsNewPackAnnounce(event girc.Event) bool {
	if event.Command != girc.PRIVMSG {
		return false
	}
	text := sanitize(event.String())
	return newPackAnnounceExp.MatchString(text)
}
