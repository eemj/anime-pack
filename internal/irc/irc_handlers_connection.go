package irc

import (
	"strings"
	"time"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/pubsub"
	"go.jamie.mt/toolbox/telemetry"
	"go.uber.org/zap"
)

type ConnectionStateUpdateMessage struct {
	Connected bool `json:"connected"`
}

func (cl *client) connectedDisconnected() girc.HandlerFunc {
	return func(c *girc.Client, event girc.Event) {
		ctx, span := cl.createSpanFromEvent(&event)
		defer span.End()

		msg := &ConnectionStateUpdateMessage{}

		switch event.Command {
		case girc.CONNECTED:
			{
				msg.Connected = true

				cl.queue.Resume() // Resume the queue - it's paused by default

				cl.backoff.Reset() // Reset the backoff timer.

				// Re-join the channels again - since we potentially disconnected
				// we want to make sure that the channel connection is still in-tact.
				for _, channel := range cl.cfg.Channels {
					currentChannel := channel
					if !strings.HasPrefix(channel, "#") {
						currentChannel = ("#" + currentChannel)
					}

					L(ctx).Info(
						"joining channel",
						zap.String("channel", currentChannel),
					)

					c.Cmd.Join(currentChannel)
				}
			}
		case girc.CLOSED:
			if cl.closed.CompareAndSwap(false, true) {
				cl.queue.Pause() // We disconnected.. prevent anymore notifications from the queue.
			}
			return
		case girc.DISCONNECTED:
			if !cl.closed.Load() {
				msg.Connected = false

				// FIXME(eemj): This can block the application shutdown, ideally we have a context something
				// or some sort of signal which interrupts this.
				for !cl.closed.Load() {
					nextBackOff := cl.backoff.NextBackOff()

					L(ctx).Info(
						"disconnected, backoff in action",
						zap.Duration("retrying_after", nextBackOff),
						zap.Time("retrying_at", time.Now().Add(nextBackOff)),
					)

					// Wait for X seconds/minutes before re-connecting
					// to not burst the connection and potentially get banned.
					<-time.After(nextBackOff)

					if err := c.Connect(); err != nil {
						L(ctx).Error(
							"failed to reconnect to irc server",
							zap.String("server", c.Config.Server),
							zap.Int("port", c.Config.Port),
						)
						telemetry.WithError(span, err)
					}
				}
			}
		}

		if cl.onEventFn != nil {
			cl.onEventFn(ctx, Event_CONNECTION_STATE_UPDATE, msg)
		}

		err := pubsub.PublishMessage(ctx, cl.publisher, pubsub.Topic_IRC_CONNECTION_STATE_UPDATE, msg)
		if err != nil {
			L(ctx).Error("failed to publish message", zap.Error(err))
			telemetry.WithError(span, err)
		}
	}
}
