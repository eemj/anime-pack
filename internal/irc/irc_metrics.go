package irc

import (
	"github.com/lrstanley/girc"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

func (cl *client) registerMetrics() {
	// `anime_pack_irc_is_connected`
	_ = promauto.NewGaugeFunc(prometheus.GaugeOpts{
		Namespace: `anime_pack`,
		Subsystem: `irc`,
		Name:      `is_connected`,
		Help:      `0/1 if the relay is connected`,
	}, func() float64 {
		if cl.girc.IsConnected() {
			return 1
		}
		return 0
	})

	// `anime_pack_irc_user_count{channel, server}`
	userCount := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: `anime_pack`,
		Subsystem: `irc`,
		Name:      `user_count`,
		Help:      `number of users in a channel which are not hidden`,
	}, []string{
		"server",
		"channel",
	})
	userCountHandler := girc.HandlerFunc(func(c *girc.Client, event girc.Event) {
		var eventChannelName *string

		if event.Command == girc.JOIN || event.Command == girc.PART {
			eventChannelName = utils.PointerOf(event.Last())
		}

		for _, channelName := range c.ChannelList() {
			if eventChannelName != nil && channelName != *eventChannelName {
				continue
			}

			// On QUIT - every channel will be re-synced because we don't receive
			// the channel name from where the user disconnected from.
			channel := c.LookupChannel(channelName)
			if channel != nil {
				userCount.
					WithLabelValues(c.Server(), channelName).
					Set(float64(len(channel.UserList)))
			}
		}
	})

	// JOIN / PART / QUIT
	cl.girc.Handlers.AddBg(girc.JOIN, userCountHandler)
	cl.girc.Handlers.AddBg(girc.PART, userCountHandler)
	cl.girc.Handlers.AddBg(girc.QUIT, userCountHandler)
}
