package irc

import (
	"context"
	"reflect"
	"sync/atomic"

	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/cenkalti/backoff/v4"
	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/lifecycle"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/telemetry"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type Client interface {
	OneshotRequestPack(ctx context.Context, nick string) (*DCCSendMessage, error)
	OnEvent(fn OnEvent)
	IsConnected() bool
	Connect() (err error)
	Close()
}

type client struct {
	cfg        configmodels.IRC
	backoff    backoff.BackOff
	tracer     trace.Tracer
	services   lifecycle.ServiceLifecycle
	queue      *queue.Queue
	fs         *fs.FileSystem
	publisher  message.Publisher
	subscriber message.Subscriber

	closed atomic.Bool

	girc *girc.Client

	onEventFn OnEvent
}

func NewClient(
	cfg configmodels.IRC,
	services lifecycle.ServiceLifecycle,
	queue *queue.Queue,
	fs *fs.FileSystem,
	publisher message.Publisher,
	subscriber message.Subscriber,
) (Client, error) {
	client := &client{
		cfg:        cfg,
		services:   services,
		queue:      queue,
		publisher:  publisher,
		subscriber: subscriber,
		fs:         fs,
		tracer:     otel.Tracer(reflect.TypeOf(&client{}).PkgPath()),
		backoff:    backoff.NewExponentialBackOff(),
	}

	gircConfig := girc.Config{
		Server:       "irc.rizon.net",
		Port:         6697,
		SSL:          true,
		GlobalFormat: false,
		User:         cfg.Nickname,
		Nick:         cfg.Nickname,
		Name:         cfg.Nickname,
		RecoverFunc:  client.recoverFunc,
	}

	gircConfig.Out = &clientLogger{config: gircConfig, logger: log.Named(common.IrcModuleName)}

	client.girc = girc.New(gircConfig)

	client.registerHandlers()
	client.registerMetrics()

	go client.listenForQueueEvents()

	return client, nil
}

func (cl *client) Close() { cl.girc.Close() }

func (cl *client) IsConnected() bool { return cl.girc.IsConnected() }

func (cl *client) Connect() (err error) {
	if cl.girc.IsConnected() {
		return nil
	}
	return cl.girc.Connect()
}

func (cl *client) recoverFunc(c *girc.Client, e *girc.HandlerError) {
	ctx, span := cl.createSpanFromEvent(&e.Event)
	defer span.End()

	L(ctx).Error(
		"recover handler",
		zap.String("server", c.Config.Server),
		zap.ByteString("stack", e.Stack),
		zap.String("function", e.Func),
		zap.String("file", e.File),
		zap.Stringer("event", &e.Event),
		zap.Int("line", e.Line),
		zap.Error(e),
	)

	telemetry.WithError(span, e)
}
