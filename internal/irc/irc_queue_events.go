package irc

import (
	"context"
	"errors"
	"fmt"

	"github.com/lrstanley/girc"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"go.uber.org/zap"
)

func (cl *client) listenForQueueEvents() {
	for {
		select {
		case err := <-cl.queue.ErrC():
			{
				if errors.Is(err, queue.ErrQueueTimeout) || errors.Is(err, queue.ErrQueueNoAvailableItem) {
					if keyError, ok := err.(*queue.KeyError); ok {
						if anime, ok := keyError.Key.(domain.AnimeBase); ok {
							err = cl.services.Runner().Timeout(anime)
							if err != nil {
								L(context.TODO()).Error(
									"failed to mark anime as timed out in our runners",
									zap.Stringer("anime", anime),
								)
							}
						}
					}
				}
			}
		case <-cl.queue.Notifier():
			go cl.handleQueueNotifier()
		}
	}
}

const QUEUE_NOTIFIER = "QUEUE_NOTIFIER"

func (cl *client) handleQueueNotifier() {
	// Hacky workaround to create the span..
	ctx, span := cl.createSpanFromEvent(&girc.Event{Command: QUEUE_NOTIFIER})
	defer span.End()

	animeBase, state, ok := cl.queue.Pop()
	if !ok {
		L(ctx).Warn(
			"unexpected queue pop behaviour",
			zap.String("error", "`.Pop()` was called but there was nothing in queue"),
		)
		return
	}

	currentLimit := cl.queue.GetLimit()
	isConnected := cl.IsConnected()
	if currentLimit == 0 || !cl.IsConnected() {
		L(ctx).Info(
			"pushing the anime back to the front",
			zap.Int("limit", currentLimit),
			zap.Bool("is_connected", isConnected),
			zap.Stringer("anime", animeBase),
		)
		cl.queue.PushFront(animeBase, state)
		return
	}

	span.AddEvent(
		fmt.Sprintf("popped anime `%s`", animeBase),
	)

	// Filter the details - make sure that our users/bots are online.
	detailsForOnlineUsers := make([]queue.Detail, 0, len(state.Details))
	for _, detail := range state.Details {
		user := cl.girc.LookupUser(detail.Bot)
		if user == nil {
			L(ctx).Info("user is not available", zap.String("bot_name", detail.Bot))
			continue
		}
		detailsForOnlineUsers = append(detailsForOnlineUsers, detail)
	}

	// Replace the details with the filtered one
	state.Details = detailsForOnlineUsers
	cl.queue.Update(animeBase, state)

	// Check if we can proceed
	if len(state.Details) == 0 {
		L(ctx).Info(
			"there are no available details for this anime left",
			zap.Stringer("anime", animeBase),
		)

		state.Done(queue.ErrQueueNoAvailableItems(animeBase))
	} else {
		currentDetail, ok := state.Current()
		if ok {
			L(ctx).Info(
				"sending and handling the following detail",
				zap.Stringer("started_from", state.StartedFrom),
				zap.Any("detail", currentDetail),
			)

			cl.girc.Cmd.Messagef(
				currentDetail.Bot,
				"XDCC SEND #%d", currentDetail.Pack,
			)
		}
	}
}
