package irc

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/lrstanley/girc"
)

// Examples:
// - ** You can only have 1 transfer at a time, Added you to the main queue for pack 212323 (\"ONE PIECE (2023) - S01E04 - MULTi 1080p WEB H.264 -NanDesuKa (NF).mkv\") in position 3. To Remove yourself at a later time type \"/MSG SakuraServ XDCC REMOVE 212323\".
var (
	transferInQueueExp = regexp.MustCompile(`You can only have (?P<transfer_limit>\d+) transfer at a time, Added you to the \S+ queue for pack (?P<pack_number>\d+) \("?(?P<filename>.+)"?\) in position (?P<position>\d+)\. To Remove yourself at a later time type "\/MSG \S+ XDCC REMOVE \d+"\.`)
)

type TransferInQueueMessage struct {
	Source        *girc.Source `json:"source,omitempty"`
	Filename      string       `json:"filename,omitempty"`
	Pack          int64        `json:"pack,omitempty"`
	Position      int          `json:"position,omitempty"`
	TransferLimit int          `json:"transfer_limit,omitempty"`
}

func IsTransferInQueue(event girc.Event) bool {
	if event.Command != girc.PRIVMSG {
		return false
	}
	text := sanitize(event.String())
	return transferInQueueExp.MatchString(text)
}

func ParseTransferInQueue(event girc.Event) (*TransferInQueueMessage, error) {
	if event.Command != girc.PRIVMSG {
		return nil, fmt.Errorf("command is not '%s' but it's '%s'", girc.PRIVMSG, event.Command)
	}

	text := sanitize(event.String())
	if !transferInQueueExp.MatchString(text) {
		return nil, fmt.Errorf("unable to match '%s' for transfer in queue", text)
	}

	var (
		res     = &TransferInQueueMessage{Source: event.Source}
		err     error
		matches = transferInQueueExp.FindStringSubmatch(text)
	)
	for index, value := range matches {
		switch strings.ToLower(transferInQueueExp.SubexpNames()[index]) {
		case `pack_number`:
			{
				res.Pack, err = strconv.ParseInt(value, 10, 64)
				if err != nil {
					return nil, err
				}
			}
		case `filename`:
			{
				res.Filename = value
			}
		case `position`:
			{
				res.Position, err = strconv.Atoi(value)
				if err != nil {
					return nil, err
				}
			}
		case `transfer_limit`:
			{
				res.TransferLimit, err = strconv.Atoi(value)
				if err != nil {
					return nil, err
				}
			}
		}
	}

	return res, nil
}
