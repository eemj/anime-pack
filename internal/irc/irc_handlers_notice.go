package irc

import (
	"github.com/lrstanley/girc"
	"go.jamie.mt/toolbox/telemetry"
	"go.uber.org/zap"
)

func (cl *client) notice() girc.HandlerFunc {
	return func(c *girc.Client, event girc.Event) {
		switch {
		case IsPendingTransfer(event):
			{
				ctx, span := cl.createSpanFromEvent(&event)
				defer span.End()

				msg, err := ParsePendingTransfer(event)
				if err != nil {
					L(ctx).Error(
						"failed to parse `pending_transfer` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
				L(ctx).Info(
					"pending transfer message",
					zap.Duration("duration", msg.Duration),
					zap.Stringer("source", msg.Source),
				)
				if err := cl.handleNoticePendingTransfer(ctx, msg); err != nil {
					L(ctx).Error(
						"failed to handle `pending_transfer` event",
						zap.String("raw", event.String()),
						zap.Error(err),
					)
					telemetry.WithError(span, err)
					return
				}
			}
		}
	}
}
