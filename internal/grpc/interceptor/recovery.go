package interceptor

import (
	"context"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type RecoveryInterceptor struct{}

func NewRecoveryInterceptor() *RecoveryInterceptor {
	return &RecoveryInterceptor{}
}

func (i *RecoveryInterceptor) recoverHandler(ctx context.Context, p any) (err error) {
	return status.Errorf(codes.Internal, "%v", p)
}

func (i *RecoveryInterceptor) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req any,
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp any, err error) {
		panicked := true

		defer func() {
			if r := recover(); r != nil || panicked {
				err = i.recoverHandler(ctx, r)
			}
		}()

		rsp, err := handler(ctx, req)
		panicked = false

		return rsp, err
	}
}

func (i *RecoveryInterceptor) Stream() grpc.StreamServerInterceptor {
	return func(
		srv any,
		ss grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler,
	) (err error) {
		panicked := true

		defer func() {
			if r := recover(); r != nil || panicked {
				err = i.recoverHandler(ss.Context(), r)
			}
		}()

		err = handler(srv, ss)
		panicked = false

		return err
	}
}
