package interceptor

import (
	"context"
	"encoding/base64"
	"strings"

	"gitlab.com/eemj/anime-pack/internal/domain/params"
	cmetadata "gitlab.com/eemj/anime-pack/internal/grpc/metadata"
	"gitlab.com/eemj/anime-pack/internal/grpc/wrapper"
	"gitlab.com/eemj/anime-pack/internal/runner"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type AuthInterceptor struct {
	runnerSvc runner.RunnerService
	skipper   wrapper.Skipper
}

func NewAuthInterceptor(
	runnerSvc runner.RunnerService,
	skipper wrapper.Skipper,
) *AuthInterceptor {
	return &AuthInterceptor{
		runnerSvc: runnerSvc,
		skipper:   skipper,
	}
}

func (a *AuthInterceptor) auth(ctx context.Context) (context.Context, error) {
	md, ok := metadata.FromIncomingContext(ctx)

	if !ok {
		return nil, status.Error(
			codes.NotFound,
			"no metadata headers",
		)
	}

	ident, ok := cmetadata.ByKey(md, cmetadata.MetadataIdent)

	if !ok {
		return nil, status.Errorf(
			codes.NotFound,
			"missing '%s' metadata",
			cmetadata.MetadataIdent,
		)
	}

	authorization, ok := cmetadata.ByKey(md, cmetadata.MetadataAuthorization)

	if !ok {
		return nil, status.Errorf(
			codes.NotFound,
			"missing '%s' metadata",
			cmetadata.MetadataAuthorization,
		)
	}

	if !strings.HasPrefix(authorization, "Bearer ") {
		return nil, status.Error(
			codes.Unauthenticated,
			"unsupported authorization type",
		)
	}

	// Remove the 'Bearer ' prefix from the Authorization header.
	authorization = authorization[7:]

	if decoded, err := base64.RawStdEncoding.DecodeString(authorization); err == nil {
		authorization = string(decoded)
	}

	valid, claims, err := a.runnerSvc.ValidateAuth(ctx, params.ValidateAuthRunnerParams{
		Ident: ident,
		Token: authorization,
	})

	if !valid {
		if err != nil {
			return nil, status.Error(
				codes.Unauthenticated,
				err.Error(),
			)
		}

		return nil, status.Error(
			codes.Internal,
			err.Error(),
		)
	}

	return wrapper.AppendClaims(ctx, claims), nil
}

func (a *AuthInterceptor) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req any,
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp any, err error) {
		if a.skipper != nil && a.skipper.Skip(info.FullMethod) {
			return handler(ctx, req)
		}

		newCtx, err := a.auth(ctx)
		if err != nil {
			return nil, err
		}

		return handler(
			newCtx,
			req,
		)
	}
}

func (a *AuthInterceptor) Stream() grpc.StreamServerInterceptor {
	return func(
		srv any,
		ss grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler,
	) (err error) {
		if a.skipper != nil && a.skipper.Skip(info.FullMethod) {
			return handler(srv, ss)
		}

		ctx := ss.Context()
		newCtx, err := a.auth(ctx)
		if err != nil {
			return err
		}

		wrapped := wrapper.NewClaimsServerStream(ss)
		wrapped.Ctx = newCtx

		return handler(srv, wrapped)
	}
}
