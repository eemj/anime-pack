package wrapper

// ContextKey defines a key type for a context value.
type ContextKey string

const (
	keyClaims = ContextKey("claims")
)
