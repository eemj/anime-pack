package wrapper

import (
	"context"

	"github.com/dgrijalva/jwt-go"
	"google.golang.org/grpc"
)

type ClaimsServerStream struct {
	grpc.ServerStream
	Ctx context.Context
}

func (c *ClaimsServerStream) Context() context.Context {
	return c.Ctx
}

func (c *ClaimsServerStream) Claims() (jwt.Claims, bool) {
	return GetClaims(c.Ctx)
}

func NewClaimsServerStream(stream grpc.ServerStream) *ClaimsServerStream {
	if claimsServerStream, ok := stream.(*ClaimsServerStream); ok {
		return claimsServerStream
	}

	return &ClaimsServerStream{
		ServerStream: stream,
		Ctx:          stream.Context(),
	}
}

func AppendClaims(ctx context.Context, claims jwt.Claims) context.Context {
	return context.WithValue(
		ctx,
		keyClaims,
		claims,
	)
}

func GetClaims(ctx context.Context) (jwt.Claims, bool) {
	claims, ok := ctx.Value(keyClaims).(jwt.Claims)
	return claims, ok
}
