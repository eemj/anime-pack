package wrapper

import "sync"

// Skipper defines an interface which will be used to skip
// interceptor features for certain methods.
type Skipper interface {
	// Skip determines if the method will get skipped. generally `method` = `info.FullMethod`
	Skip(method string) (skip bool)
}

type skipper struct{ m sync.Map }

func (s *skipper) Skip(method string) (skip bool) {
	_, skip = s.m.Load(method)
	return
}

func NewSkipper(
	methods ...string,
) Skipper {
	s := &skipper{m: sync.Map{}}

	for _, method := range methods {
		s.m.Store(method, struct{}{})
	}

	return s
}
