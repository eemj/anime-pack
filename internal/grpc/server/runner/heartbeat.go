package runner

import "time"

// HeartbeatConfig defines the heartbeat configuration for every
// client that connects to the gRPC server.
type HeartbeatConfig struct {
	// Timeout defines the X amount of time the client will classify
	// a heartbeat as a timed-out entry.
	Timeout time.Duration
}
