package runner

import (
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/grpc/wrapper"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// operationReceivePump is defines because grpc.Recv() method blocks and we can't have
// two blocking methods in the main stream call.
func (s *runnerServer) operationReceivePump(
	srv grpc_runner_v1.RunnerService_OperationServer,
) <-chan *grpc_runner_v1.OperationRequest {
	receiver := make(chan *grpc_runner_v1.OperationRequest, 1)

	go func() {
		ctx := srv.Context()

		for {
			select {
			case <-ctx.Done():
				return
			default:
			}

			request, err := srv.Recv()
			if err != nil {
				close(receiver)
				return
			}

			receiver <- request
		}
	}()

	return receiver
}

func (s *runnerServer) Operation(
	srv grpc_runner_v1.RunnerService_OperationServer,
) (err error) {
	ctx := srv.Context()
	wrappedCtx := wrapper.NewClaimsServerStream(srv).Context()

	runnerClaims, err := s.claims(wrappedCtx)
	if err != nil {
		return
	}

	client, err := s.runnerSvc.Connect(runnerClaims)
	if err != nil {
		return status.Error(codes.PermissionDenied, err.Error())
	}
	if client == nil {
		return status.Error(codes.Internal, ClientDisappeared)
	}

	L := func() *zap.Logger {
		return log.WithTrace(ctx).
			Named("runner.operation").
			With(zap.Stringer("client", client.Info()))
	}

	L().Debug("establish")

	defer s.runnerSvc.Disconnect(runnerClaims)

	requests := s.operationReceivePump(srv)
	clientConsumer := client.Consume()

	for {
		select {
		case <-ctx.Done():
			err = ctx.Err()
			L().Debug("interrupted, potentially cancelled", zap.Error(err))
			return err
		case response := <-clientConsumer:
			if err = srv.Send(response); err != nil {
				return err
			}
		case request := <-requests:
			if request == nil {
				L().Warn("received an empty request")
				break
			}

			if err = s.connected(client); err != nil {
				return err
			}

			err = s.runnerSvc.Operation(ctx, client, request)
			if err != nil {
				L().Error(
					"error",
					zap.Stringer("operation", request.Operation),
					zap.Error(err),
				)
			}
		}
	}
}
