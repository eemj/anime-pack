package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

// Auth defines the Unary RPC call within the `runner.proto` file.
//
// This is just a proxy call to the service, the service handles all the logic.
func (s *runnerServer) Auth(
	ctx context.Context,
	req *grpc_runner_v1.AuthRequest,
) (res *grpc_runner_v1.AuthResponse, err error) {
	return s.runnerSvc.Auth(ctx, req)
}
