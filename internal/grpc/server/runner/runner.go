package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/runner"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type runnerServer struct {
	grpc_runner_v1.UnimplementedRunnerServiceServer

	runnerSvc runner.RunnerService
}

func (s *runnerServer) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.RunnerServerName)
}

func NewServer(runnerSvc runner.RunnerService) grpc_runner_v1.RunnerServiceServer {
	return &runnerServer{runnerSvc: runnerSvc}
}
