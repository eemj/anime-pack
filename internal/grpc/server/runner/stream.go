package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

// Stream defines the Stream RPC call within the `runner.proto` file.
func (s *runnerServer) Stream(ctx context.Context, req *grpc_runner_v1.StreamRequest) (res *grpc_runner_v1.StreamResponse, err error) {
	claims, err := s.claims(ctx)
	if err != nil {
		return nil, err
	}

	client := s.runnerSvc.Client(claims)
	err = s.connected(client)
	if err != nil {
		return nil, err
	}

	res = &grpc_runner_v1.StreamResponse{
		Stream: req.Stream,
		Start:  req.Start,
		End:    req.End,
	}
	err = s.runnerSvc.Stream(ctx, client, req)
	if err != nil {
		res.Error = err.Error()
	}
	return res, nil
}
