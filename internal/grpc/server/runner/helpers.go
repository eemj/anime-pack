package runner

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/auth/claims"
	"gitlab.com/eemj/anime-pack/internal/grpc/wrapper"
	"gitlab.com/eemj/anime-pack/internal/runner/client"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// ClientNoLongerConnected is thrown when
	ClientNoLongerConnected = "client is no longer connected"
	UnexpectedClaimsType    = "unexpected claims type"
	NoClaims                = "no claims"
	HeartbeatExpired        = "heartbeat expired"
	ClientDisappeared       = "client disappeared"
)

// claims attempts to parse the retrieved `jwt.Claims` from the `AuthInterceptor`, and transforms them
// into `*claims.RunnerClaims`.
func (s *runnerServer) claims(ctx context.Context) (runnerClaims *claims.RunnerClaims, err error) {
	jwtClaims, ok := wrapper.GetClaims(ctx)
	if !ok {
		return nil, status.Error(codes.Unauthenticated, NoClaims)
	}

	runnerClaims, ok = jwtClaims.(*claims.RunnerClaims)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, UnexpectedClaimsType)
	}

	return runnerClaims, nil
}

// connected will check if the client is nil, if it's nil it'll send
// a `ClientNoLongerConnected` exception.
func (s *runnerServer) connected(runner client.Client) (err error) {
	if runner != nil {
		return nil
	}

	return status.Error(codes.Unavailable, ClientNoLongerConnected)
}
