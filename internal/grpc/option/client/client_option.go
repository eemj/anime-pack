package option

import (
	"crypto/tls"
	"time"

	"gitlab.com/eemj/anime-pack/pkg/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/keepalive"
)

type ClientOptions struct {
	block                bool
	insecure             bool
	skipVerify           bool
	certFile             string
	rpcCredentials       credentials.PerRPCCredentials
	keepaliveParams      *keepalive.ClientParameters
	transportCredentials credentials.TransportCredentials
}

func (c *ClientOptions) Options() (options []grpc.DialOption, err error) {
	if !c.insecure && c.skipVerify {
		options = append(options, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})))
	} else if !c.skipVerify && c.insecure {
		options = append(options, grpc.WithTransportCredentials(insecure.NewCredentials()))
	} else if !utils.IsEmpty(c.certFile) && c.transportCredentials == nil {
		creds, err := credentials.NewClientTLSFromFile(c.certFile, "")
		if err != nil {
			return nil, err
		}

		options = append(options, grpc.WithTransportCredentials(creds))
	}

	options = append(options, grpc.WithConnectParams(grpc.ConnectParams{
		Backoff:           backoff.DefaultConfig,
		MinConnectTimeout: 3 * time.Second,
	}))

	if c.block {
		options = append(options, grpc.WithBlock(), grpc.WithReturnConnectionError())
	}

	if c.rpcCredentials != nil {
		options = append(
			options,
			grpc.WithPerRPCCredentials(c.rpcCredentials),
		)
	}

	if c.keepaliveParams != nil {
		options = append(
			options,
			grpc.WithKeepaliveParams(*c.keepaliveParams),
		)
	}

	if c.transportCredentials != nil {
		options = append(
			options,
			grpc.WithTransportCredentials(c.transportCredentials),
		)
	}

	return
}

type ClientOption func(*ClientOptions)

func WithBlock() ClientOption {
	return func(co *ClientOptions) {
		co.block = true
	}
}

func WithInsecure() ClientOption {
	return func(co *ClientOptions) {
		co.insecure = true
	}
}

func WithSkipVerify() ClientOption {
	return func(co *ClientOptions) {
		co.skipVerify = true
	}
}

func WithRpcCredentials(rpcCredentials credentials.PerRPCCredentials) ClientOption {
	return func(co *ClientOptions) {
		co.rpcCredentials = rpcCredentials
	}
}

func WithKeepaliveParams(keepaliveParams keepalive.ClientParameters) ClientOption {
	return func(co *ClientOptions) {
		co.keepaliveParams = &keepaliveParams
	}
}

func WithTransportCredentials(transportCredentials credentials.TransportCredentials) ClientOption {
	return func(co *ClientOptions) {
		co.transportCredentials = transportCredentials
	}
}

func WithCertFile(certFile string) ClientOption {
	return func(co *ClientOptions) {
		co.certFile = certFile
	}
}
