package metadata

const (
	// MetadataIdent holds the services' identity as it will be used for authentication purposes.
	MetadataIdent = "ident"

	// MetadataAuthorization holds the authentication token for that server it will be used for authentication purposes.
	MetadataAuthorization = "authorization"
)
