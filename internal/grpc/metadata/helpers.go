package metadata

import "google.golang.org/grpc/metadata"

func ByKey(md metadata.MD, key string) (value string, ok bool) {
	values := md.Get(key)
	if len(values) == 0 {
		return "", false
	}
	return values[0], true
}
