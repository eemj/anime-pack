package runner

import (
	"context"
	"io"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/pipeline"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

const (
	operationInBuffer  = 32
	operationOutBuffer = 32
)

type Operation interface {
	Pipeline() pipeline.Pipeline
	Sync(ctx context.Context) (err error)
	Close() (err error)
}

type operation struct {
	client           grpc_runner_v1.RunnerServiceClient
	pipeline         pipeline.Pipeline
	transcodeOptions *ffmpeg.TranscodeOptions

	operationClient grpc_runner_v1.RunnerService_OperationClient

	in  chan *grpc_runner_v1.OperationResponse
	out chan *grpc_runner_v1.OperationRequest

	workDir string
}

func newOperation(
	ctx context.Context,
	client grpc_runner_v1.RunnerServiceClient,
	transcodeOptions *ffmpeg.TranscodeOptions,
	workDir string,
) (Operation, error) {
	operation := &operation{
		client:           client,
		pipeline:         pipeline.New(),
		transcodeOptions: transcodeOptions,

		in:  make(chan *grpc_runner_v1.OperationResponse, operationInBuffer),
		out: make(chan *grpc_runner_v1.OperationRequest, operationOutBuffer),

		workDir: workDir,
	}

	if err := operation.Sync(ctx); err != nil {
		return operation, err
	}

	return operation, nil
}

func (o *operation) L() *zap.Logger { return log.Named("operation") }

func (o *operation) operate(srv grpc_runner_v1.RunnerService_OperationClient) <-chan error {
	errc := make(chan error, 1)

	go func() {
		ctx := srv.Context()

		for {
			select {
			case <-ctx.Done():
				return
			case operation := <-o.pipeline.Operation():
				o.out <- operation
			}
		}
	}()

	go func() {
		ctx := srv.Context()
		for msg := range o.in {
			switch response := msg.Response.(type) {
			case *grpc_runner_v1.OperationResponse_OpenResponse:
				go o.Open(ctx, response, errc)
			case *grpc_runner_v1.OperationResponse_InterruptResponse:
				o.Interrupt(response)
			}
		}
	}()

	return errc
}

func (o *operation) read(srv grpc_runner_v1.RunnerService_OperationClient) <-chan error {
	var (
		errc = make(chan error, 1)
		ctx  = srv.Context()
	)

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				req, err := srv.Recv()
				if err != nil {
					if err == io.EOF {
						return
					}
					errc <- err
					return
				}

				o.L().Debug("receive", zap.Any("message", req))

				o.in <- req
			}
		}
	}(ctx)

	return errc
}

func (o *operation) write(srv grpc_runner_v1.RunnerService_OperationClient) <-chan error {
	var (
		errc = make(chan error, 1)
		ctx  = srv.Context()
	)

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			case res := <-o.out:
				o.L().Debug("send", zap.Any("message", res))

				if err := srv.Send(res); err != nil {
					if err != io.EOF {
						errc <- err
					}
					return
				}
			}
		}
	}(ctx)

	return errc
}

func (o *operation) Pipeline() pipeline.Pipeline { return o.pipeline }

func (o *operation) Sync(ctx context.Context) (err error) {
	o.operationClient, err = o.client.Operation(
		ctx,
		grpc.WaitForReady(true),
	)
	if err != nil {
		return err
	}

	errc := utils.MergeChan(
		o.read(o.operationClient),
		o.write(o.operationClient),
		o.operate(o.operationClient),
		o.streamer(ctx, o.client, o.pipeline),
	)
	for err = range errc {
		if err != nil {
			return err
		}
	}
	return nil
}

func (o *operation) Close() error {
	if o.operationClient != nil {
		return o.operationClient.CloseSend()
	}
	return nil
}
