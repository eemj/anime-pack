package runner

import (
	"time"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
)

type RunnerClientOptions struct {
	Docker                bool
	User, Hostname, Ident string
	Timeout               time.Duration
	TranscodeOptions      ffmpeg.TranscodeOptions
	WorkDir               string
}

func (o *RunnerClientOptions) Clone() *RunnerClientOptions {
	return &RunnerClientOptions{
		Docker:           o.Docker,
		User:             o.User,
		Hostname:         o.Hostname,
		Ident:            o.Ident,
		Timeout:          o.Timeout,
		TranscodeOptions: o.TranscodeOptions,
	}
}

type RunnerClientOption func(*RunnerClientOptions)

func WithWorkDir(workDir string) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.WorkDir = workDir
	}
}

func WithDocker(docker bool) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.Docker = true
	}
}

func WithHostname(hostname string) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.Hostname = hostname
	}
}

func WithUser(user string) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.User = user
	}
}

func WithIdent(ident string) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.Ident = ident
	}
}

func WithTimeout(timeout time.Duration) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.Timeout = timeout
	}
}

func WithTranscodeOptions(transcodeOptions ffmpeg.TranscodeOptions) RunnerClientOption {
	return func(rco *RunnerClientOptions) {
		rco.TranscodeOptions = transcodeOptions
	}
}
