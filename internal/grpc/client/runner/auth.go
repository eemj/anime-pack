package runner

import (
	"context"
	"time"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

const (
	renewBuffer = 5 * time.Minute
)

// expiryTicker watches the expiration for the token
func (c *runnerClient) expiryTicker(ctx context.Context) (chan<- time.Time, <-chan struct{}) {
	var (
		in  = make(chan time.Time, 1)
		out = make(chan struct{}, 1)
	)

	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				close(in)
				close(out)
				c.L().Debug("authentication expiry check closing")
				return
			case t := <-in:
				duration := t.Sub(time.Now().UTC())
				renewDuration := duration - renewBuffer
				c.L().Info("authentication",
					zap.Stringer("expires_in", duration),
					zap.Stringer("renewing_in", renewDuration),
					zap.Time("expires_at", t),
					zap.Time("renews_at", t.Add(-renewBuffer)),
				)

				go time.AfterFunc(renewDuration, func() {
					c.L().Info("authentication expiring, attempting to renew")
					out <- struct{}{}
				})
			}
		}
	}(ctx)

	return in, out
}

func (c *runnerClient) OnAuth(onAuth AuthCallback) {
	c.onAuth = onAuth
}

func (c *runnerClient) BackgroundAuth(ctx context.Context) <-chan error {
	var (
		errc     = make(chan error, 1)
		in, out  = c.expiryTicker(ctx)
		res, err = c.RefreshAuth(ctx)
	)
	if err == nil {
		in <- res.ExpiresAt.AsTime()
	} else {
		errc <- err
	}

	reAuth := utils.MergeChan(out, c.reAuth)
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				errc <- ctx.Err()
				return
			case <-reAuth:
				res, err := c.RefreshAuth(ctx)
				if err != nil {
					errc <- err
				} else {
					in <- res.ExpiresAt.AsTime()
				}
			}
		}
	}(ctx)

	return errc
}

func (c *runnerClient) RefreshAuth(ctx context.Context) (res *grpc_runner_v1.AuthResponse, err error) {
	ctx, cancel := context.WithTimeout(ctx, c.options.Timeout)
	defer cancel()

	res, err = c.client.Auth(ctx, &grpc_runner_v1.AuthRequest{
		User:     c.options.User,
		Hostname: c.options.Hostname,
		Ident:    c.options.Ident,
		Docker:   c.options.Docker,
	})
	if err != nil {
		return nil, err
	}
	// Fire a callback with the token so it can be handled by the listener.
	if c.onAuth != nil {
		c.onAuth(res.Token)
	}
	return res, nil
}
