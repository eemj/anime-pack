package runner

import (
	"context"
	"time"

	"github.com/jpillora/backoff"
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/pipeline"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type RunnerClient interface {
	// Run starts the background routine where the runner
	// authenticates and listens to the operation stream call.
	Run(ctx context.Context) (err error)

	// RefreshAuth forcefully authenticates the runner.
	RefreshAuth(ctx context.Context) (res *grpc_runner_v1.AuthResponse, err error)

	// OnAuth's actions within must not cuase deadlocks/block otherwise
	// the runner client will loose it's authentication status and will
	// cause you issues.
	OnAuth(f AuthCallback)

	// Returns the specified options provided after creation.
	Options() RunnerClientOptions

	// ReplaceTranscodeOptions will replace the ffmpeg's transcode options.
	ReplaceTranscodeOptions(transcodeOptions ffmpeg.TranscodeOptions) (err error)
}

type AuthCallback func(token string)

type runnerClient struct {
	options *RunnerClientOptions
	client  grpc_runner_v1.RunnerServiceClient
	backoff *backoff.Backoff

	reAuth chan struct{}
	onAuth AuthCallback

	operation Operation
}

func NewClient(
	client grpc_runner_v1.RunnerServiceClient,
	optionFunc ...RunnerClientOption,
) (RunnerClient, error) {
	rclient := &runnerClient{
		client:  client,
		options: new(RunnerClientOptions),
		reAuth:  make(chan struct{}),
		backoff: &backoff.Backoff{
			Min:    100 * time.Millisecond,
			Max:    30 * time.Second,
			Factor: 1.5,
			Jitter: true,
		},
	}

	for _, opt := range optionFunc {
		opt(rclient.options)
	}

	if rclient.options.Timeout == 0 {
		rclient.options.Timeout = (5 * time.Second)
	}

	return rclient, nil
}

func (c *runnerClient) L() *zap.Logger { return log.Named("client") }

// Run keeps the runner under persistence mode, meaning that once
// a network error/closure occurs, the runner will try to re-connect
// respecting the backoff periods.
func (c *runnerClient) Run(ctx context.Context) (err error) {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			c.L().Info("connecting")

			if err := c.run(ctx); err != nil {
				// Let the listener know to re-authenticate if the code is
				// `codes.Unauthenticated`.
				if statusError, ok := status.FromError(err); ok && statusError.Code() == codes.Unauthenticated {
					c.reAuth <- struct{}{}
				}

				var (
					isActive = false
					duration = c.backoff.Duration()
				)

				if c.operation != nil {
					// Close down the pipeline if it's active.
					if isActive = c.operation.Pipeline().Active(); isActive {
						c.L().Debug("closing pipeline")

						c.operation.Pipeline().Close()
					}
				}

				c.L().Error(
					"unexpected disconnect",
					zap.Bool("pipeline_active", isActive),
					zap.Duration("backoff_duration", duration),
					zap.Error(err),
				)

				<-time.After(duration)
			}
		}
	}
}

func (c *runnerClient) run(ctx context.Context) (err error) {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case err := <-c.BackgroundAuth(ctx):
		return err
	default:
	}

	c.operation, err = newOperation(
		ctx,
		c.client,
		&c.options.TranscodeOptions,
		c.options.WorkDir,
	)

	return
}

func (c *runnerClient) Options() RunnerClientOptions {
	if c.options == nil {
		return RunnerClientOptions{}
	}
	return *c.options.Clone()
}

func (c *runnerClient) ReplaceTranscodeOptions(transcodeOptions ffmpeg.TranscodeOptions) (err error) {
	if active := c.operation.Pipeline().Active(); active {
		return pipeline.ErrPipelineIsAlreadyActive
	}
	WithTranscodeOptions(transcodeOptions)(c.options)
	return
}
