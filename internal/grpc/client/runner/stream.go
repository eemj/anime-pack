package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/pipeline"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

func (o *operation) streamer(
	ctx context.Context,
	client grpc_runner_v1.RunnerServiceClient,
	pipeline pipeline.Pipeline,
) <-chan error {
	errc := make(chan error, 1)

	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-pipeline.Done():
				o.L().Info("pipeline", zap.String("status", "done"))
			case request := <-pipeline.Stream():
				res, err := client.Stream(ctx, request)

				if err != nil {
					errc <- err
				} else if res != nil && !utils.IsEmpty(res.Error) {
					if err := pipeline.Retry(ctx, res); err != nil {
						errc <- err
					}
				}
			}
		}
	}()

	return errc
}
