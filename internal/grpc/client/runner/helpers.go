package runner

import (
	"os"
)

const (
	// Patterns for the os.CreateTemp()..
	videoRawPattern       = "video-raw*"
	videoEncodedPattern   = "video-encoded*"
	imageThumbnailPattern = "image-thumbnail*"
)

// createTemp is utility helper to create temporary files for the pipeline.
func createTemp(dir, pattern string) (string, error) {
	file, err := os.CreateTemp(dir, pattern)
	if err != nil {
		return "", err
	}

	name := file.Name()

	if err = file.Close(); err != nil {
		return "", err
	}

	return name, nil
}
