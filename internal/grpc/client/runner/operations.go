package runner

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/internal/fs"
	"gitlab.com/eemj/anime-pack/internal/pipeline"
	"go.uber.org/zap"
)

func (o *operation) pipelineSetup(response *grpc_runner_v1.OperationResponse_OpenResponse) (options *pipeline.Options, err error) {
	videoSource, err := createTemp(o.workDir, videoRawPattern)
	if err != nil {
		return nil, err
	}

	videoDestination, err := createTemp(o.workDir, videoEncodedPattern)
	if err != nil {
		return nil, err
	}

	imageDestination, err := createTemp(o.workDir, (imageThumbnailPattern + fs.ExtensionImage))
	if err != nil {
		return nil, err
	}

	return pipeline.NewOptions(
		int(response.OpenResponse.GetSize()),
		response.OpenResponse.GetAddress(),
		videoSource,
		videoDestination,
		imageDestination,
		*o.transcodeOptions,
	), nil
}

func (o *operation) Open(ctx context.Context, response *grpc_runner_v1.OperationResponse_OpenResponse, errc chan<- error) {
	options, err := o.pipelineSetup(response)
	if err != nil {
		o.out <- &grpc_runner_v1.OperationRequest{
			Operation: grpc_runner_v1.Operation_CLOSE,
			Request: &grpc_runner_v1.OperationRequest_CloseRequest{
				CloseRequest: &grpc_runner_v1.CloseRequest{
					Error: err.Error(),
				},
			},
		}
		errc <- err
		return
	}

	if err = o.pipeline.Setup(options); err != nil {
		errc <- err
		return
	}

	o.L().Info(
		"pipeline",
		zap.String("status", "starting"),
		zap.String("address", options.GetAddress()),
		zap.Int("size", options.GetSize()),
		zap.Strings("temp_paths", []string{
			options.GetVideoSource(),
			options.GetVideoDestination(),
			options.GetImageDestination(),
		}),
	)

	err = o.pipeline.Run(ctx)
	errStr := ""

	if err != nil && err != context.Canceled {
		// If an error is encountered during the pipeline, we'll send
		// a CLOSE operation to the server with the exception as part of the
		// request.
		//
		// If the exception is that it's cancelled,
		// don't sent the CloseRequest otherwise it'll confuse the service.
		errStr = err.Error()

		o.L().Error(
			"pipeline",
			zap.String("status", "failed"),
			zap.Error(err),
		)
	}

	o.out <- &grpc_runner_v1.OperationRequest{
		Operation: grpc_runner_v1.Operation_CLOSE,
		Request: &grpc_runner_v1.OperationRequest_CloseRequest{
			CloseRequest: &grpc_runner_v1.CloseRequest{
				Error: errStr,
			},
		},
	}

	switch err {
	case context.Canceled:
		o.L().Info(
			"pipeline",
			zap.String("status", "canceled"), // Otherwise, output that it's canceled
		)
	case nil:
		o.L().Info(
			"pipeline",
			zap.String("status", "finished"), // Log finished if the error is empty.
		)
	}
}

func (o *operation) Interrupt(response *grpc_runner_v1.OperationResponse_InterruptResponse) {
	request := &grpc_runner_v1.OperationRequest{
		Operation: grpc_runner_v1.Operation_INTERRUPT,
		Request: &grpc_runner_v1.OperationRequest_InterruptRequest{
			InterruptRequest: &grpc_runner_v1.InterruptRequest{},
		},
	}

	if err := o.pipeline.Close(); err != nil {
		request.Request.(*grpc_runner_v1.OperationRequest_InterruptRequest).InterruptRequest.Error = err.Error()
	}

	o.out <- request
}
