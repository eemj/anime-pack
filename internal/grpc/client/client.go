package client

import (
	"context"

	option "gitlab.com/eemj/anime-pack/internal/grpc/option/client"
	"google.golang.org/grpc"
)

func Dial(address string, optionFunc ...option.ClientOption) (*grpc.ClientConn, error) {
	return DialContext(context.Background(), address, optionFunc...)
}

func DialContext(ctx context.Context, address string, optionFunc ...option.ClientOption) (*grpc.ClientConn, error) {
	clientOptions := new(option.ClientOptions)

	for _, opt := range optionFunc {
		opt(clientOptions)
	}

	grpcOptions, err := clientOptions.Options()
	if err != nil {
		return nil, err
	}

	return grpc.DialContext(
		ctx,
		address,
		grpcOptions...,
	)
}
