package resolver

import (
	"google.golang.org/grpc/resolver"
	"google.golang.org/grpc/resolver/manual"
	"google.golang.org/grpc/serviceconfig"
)

const (
	Schema = "anime-pack-endpoints"
)

type AnimePackResolver struct {
	resolver.Builder
	*manual.Resolver
	endpoints     []string
	serviceConfig *serviceconfig.ParseResult
}

func New(endpoints ...string) *AnimePackResolver {
	return &AnimePackResolver{
		Resolver:      manual.NewBuilderWithScheme(Schema),
		endpoints:     endpoints,
		serviceConfig: nil,
	}
}

func (r *AnimePackResolver) SetEndpoints(endpoints []string) {
	r.endpoints = endpoints
}

func (r AnimePackResolver) updateState() {
	if r.CC != nil {
		addresses := make([]resolver.Address, len(r.endpoints))
		for index, addr := range r.endpoints {
			addresses[index] = resolver.Address{Addr: addr}
		}
		state := resolver.State{Addresses: addresses}
		r.UpdateState(state)
	}
}

func (r *AnimePackResolver) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {
	r.serviceConfig = cc.ParseServiceConfig(`{"loadBalancingPolicy": "round_robin"}`)

	if r.serviceConfig.Err != nil {
		return nil, r.serviceConfig.Err
	}

	res, err := r.Resolver.Build(target, cc, opts)
	if err != nil {
		return nil, err
	}

	r.updateState()

	return res, nil
}
