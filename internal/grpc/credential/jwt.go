package credential

import (
	"context"
	"strings"
	"sync"

	"gitlab.com/eemj/anime-pack/internal/grpc/metadata"
)

type JwtCreds struct {
	rwmutex sync.RWMutex
	ident   string
	token   *string
}

func NewJwtCreds(ident string) *JwtCreds {
	return &JwtCreds{ident: ident}
}

// GetRequestMetadata returns the metadata required to interact with the gRPC server.
// The token isn't included if it's not set.
func (j *JwtCreds) GetRequestMetadata(ctx context.Context, in ...string) (md map[string]string, err error) {
	j.rwmutex.RLock()
	defer j.rwmutex.RUnlock()

	md = map[string]string{metadata.MetadataIdent: j.ident}

	if j.token != nil {
		md[metadata.MetadataAuthorization] = *j.token
	}

	return md, nil
}

// SetToken provides the ability to set the token at a later stage
// e.g. after retrieving it from the Auth() call in the `/runner.RunnerService/Auth`.
func (j *JwtCreds) SetToken(token string) {
	j.rwmutex.Lock()
	defer j.rwmutex.Unlock()

	if !strings.HasPrefix(token, "Bearer ") {
		token = "Bearer " + token // Prefix the Bearer auth type in-front of the token.
	}

	j.token = &token
}

// RequireTransportSecurity will always return false,
// for now we'll assume that the service is behind a reverse-proxy with a certificate.
func (j *JwtCreds) RequireTransportSecurity() bool {
	return false
}
