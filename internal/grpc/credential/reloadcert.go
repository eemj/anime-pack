package credential

import (
	"crypto/tls"
	"os"
	"sync"
	"time"

	"google.golang.org/grpc/credentials"
)

type certItem struct {
	ModTime     time.Time
	Certificate tls.Certificate
}

func NewHotReloadTLSFromFile(certFile, keyFile string) credentials.TransportCredentials {
	var certs sync.Map

	return credentials.NewTLS(&tls.Config{
		GetCertificate: func(chi *tls.ClientHelloInfo) (*tls.Certificate, error) {
			stat, err := os.Stat(keyFile)
			if err != nil {
				return nil, err
			}

			value, loaded := certs.Load(chi.ServerName)

			if loaded {
				if item, ok := value.(certItem); ok && stat.ModTime().Equal(item.ModTime) {
					return &item.Certificate, nil
				}
			}

			cert, err := tls.LoadX509KeyPair(certFile, keyFile)
			if err != nil {
				return nil, err
			}

			certs.Store(chi.ServerName, certItem{
				Certificate: cert,
				ModTime:     stat.ModTime(),
			})

			return &cert, nil
		},
	})
}
