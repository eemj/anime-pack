package paths

import (
	"os"
	"path/filepath"
	"runtime"
)

func Get(applicationName string) (paths []string, err error) {
	wd, err := os.Getwd()
	if err != nil {
		return
	}

	paths = append(paths, wd)

	// configuration paths
	switch runtime.GOOS {
	case "windows":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("$PROGRAMDATA", applicationName)),
			os.ExpandEnv(filepath.Join("$APPDATA", applicationName)),
			os.ExpandEnv(filepath.Join("$USERPROFILE", applicationName)),
		)
	case "darwin":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("/Library", "Application Support", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME", "/Library", "Application Support", applicationName)),
		)
	case "linux", "freebsd":
		paths = append(
			paths,
			os.ExpandEnv(filepath.Join("$XDC_CONFIG_HOME/", applicationName)),
			os.ExpandEnv(filepath.Join("$XDC_CONFIG_DIRS/", applicationName)),
			os.ExpandEnv(filepath.Join("/etc/", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME/", applicationName)),
			os.ExpandEnv(filepath.Join("$HOME/.config", applicationName)),
		)
	}

	return
}
