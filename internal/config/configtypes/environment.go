package configtypes

import (
	"errors"
	"fmt"
	"reflect"

	"github.com/mitchellh/mapstructure"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type Environment string

const (
	EnvironmentDEVELOPMENT Environment = "development"
	EnvironmentPRODUCTION  Environment = "production"
)

func (e Environment) Enum() *Environment { return &e }

func (e Environment) String() string { return string(e) }

func (e *Environment) Validate() error {
	if e == nil {
		return errors.New("environment is required")
	}

	switch *e {
	case EnvironmentDEVELOPMENT,
		EnvironmentPRODUCTION:
		return nil
	}

	return ErrInvalidEnvironment(*e)
}

func parseEnvironment(environment string) Environment {
	switch environment {
	case EnvironmentDEVELOPMENT.String():
		return EnvironmentDEVELOPMENT
	case EnvironmentPRODUCTION.String():
		return EnvironmentPRODUCTION
	}
	return EnvironmentPRODUCTION
}

func EnvironmentHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data any,
	) (any, error) {
		if f.Kind() != reflect.String ||
			t != reflect.TypeOf(Environment("")) {
			return data, nil
		}

		return parseEnvironment(data.(string)), nil
	}
}

// ErrInvalidEnvironment returns 'invalid environment: %s' with
// the specified environment.
func ErrInvalidEnvironment(env Environment) error {
	return fmt.Errorf("environment `%s` invalid, expected (%s)",
		env,
		sliceutil.SurroundWith(
			'`',
			EnvironmentDEVELOPMENT.String(),
			EnvironmentPRODUCTION.String(),
		),
	)
}
