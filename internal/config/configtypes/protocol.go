package configtypes

import (
	"reflect"

	"github.com/mitchellh/mapstructure"
)

type Protocol string

const (
	ProtocolUNKNOWN Protocol = "unknown"
	ProtocolTCP     Protocol = "tcp"
	ProtocolUNIX    Protocol = "unix"
	ProtocolHTTP    Protocol = "http"
	ProtocolHTTP2C  Protocol = "http2c"
	ProtocolHTTPS   Protocol = "https"
)

func (p Protocol) String() string { return string(p) }

func parseProtocol(scheme string) Protocol {
	switch scheme {
	case ProtocolTCP.String():
		return ProtocolTCP
	case ProtocolUNIX.String():
		return ProtocolUNIX
	case ProtocolHTTP.String():
		return ProtocolHTTP
	case ProtocolHTTP2C.String():
		return ProtocolHTTP2C
	case ProtocolHTTPS.String():
		return ProtocolHTTPS
	}
	return ProtocolUNKNOWN
}

func ProtocolHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data any,
	) (any, error) {
		if f.Kind() != reflect.String ||
			t != reflect.TypeOf(Protocol("")) {
			return data, nil
		}

		return parseProtocol(data.(string)), nil
	}
}
