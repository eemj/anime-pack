package configtypes

import (
	"reflect"
	"time"

	"github.com/mitchellh/mapstructure"
)

type HumanizeDuration string

func (d HumanizeDuration) Duration() time.Duration {
	duration, _ := time.ParseDuration(string(d))
	return duration
}

func HumanizeDurationHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data any,
	) (any, error) {
		if f.Kind() != reflect.String ||
			t != reflect.TypeOf(HumanizeDuration("")) {
			return data, nil
		}

		if _, err := time.ParseDuration(data.(string)); err != nil {
			return nil, err
		}

		return HumanizeDuration(data.(string)), nil
	}
}
