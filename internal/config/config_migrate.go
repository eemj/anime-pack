package config

import (
	"errors"

	"go.jamie.mt/toolbox/configmodels"
)

type ConfigMigrate struct {
	Database configmodels.PostgreSQL `json:"database" mapstructure:"database"`

	Path string `json:"-"`
}

func (c *ConfigMigrate) SetPath(path string) { c.Path = path }

func (c ConfigMigrate) Validate() error {
	return errors.Join(
		c.Database.Validate(),
	)
}
