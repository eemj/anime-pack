package config

import (
	"errors"

	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	customconfig "go.jamie.mt/toolbox/configmodels"
)

type ConfigAPI struct {
	Environment *configtypes.Environment `json:"environment" mapstructure:"environment"`

	Database  customconfig.PostgreSQL `json:"database" mapstructure:"database"`
	Logging   customconfig.Logging    `json:"logging" mapstructure:"logging"`
	Redis     customconfig.Redis      `json:"redis" mapstructure:"redis"`
	Telemetry customconfig.Telemetry  `json:"telemetry" mapstructure:"telemetry"`

	Memory      configmodels.Memory      `json:"memory" mapstructure:"memory"`
	Servers     configmodels.Servers     `json:"servers" mapstructure:"servers"`
	Directories configmodels.Directories `json:"directories" mapstructure:"directories"`
	Security    configmodels.Security    `json:"security" mapstructure:"security"`
	IRC         configmodels.IRC         `json:"irc" mapstructure:"irc"`
	Jobs        configmodels.Jobs        `json:"jobs" mapstructure:"jobs"`

	Flags APIFlags `mapstructure:"-"`
	Path  string   `mapstructure:"-"`
}

func (c *ConfigAPI) SetPath(path string) { c.Path = path }

func (c *ConfigAPI) Validate() error {
	return errors.Join(
		c.Environment.Validate(),

		c.Database.Validate(),
		c.Logging.Validate(),
		c.Redis.Validate(),
		c.Telemetry.Validate(),

		c.Memory.Validate(),
		c.Servers.Validate(),
		c.Directories.Validate(),
		c.Security.Validate(),
		c.IRC.Validate(),

		c.Jobs.Validate(),
	)
}
