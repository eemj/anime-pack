package configmodels

import (
	"errors"
	"strings"
)

// IRC defines the `irc` configuration.
type IRC struct {
	// Nickname will be the nickname the IRC client will be using.
	Nickname string `mapstructure:"nickname"`
	// Channels will be the channel the IRC client will be used
	// (this is mostly used for debugging purposes and defaults to #nibl by default).
	Channels []string `mapstructure:"channels"`
}

func (r *IRC) Validate() (err error) {
	if r.Nickname == "" {
		err = errors.Join(err, errors.New("`irc.nickname` is required"))
	}

	if r.Channels == nil || len(r.Channels) == 0 {
		err = errors.Join(err, errors.New("`irc.channels` is required"))
	} else {
		for _, channel := range r.Channels {
			if !strings.HasPrefix(channel, "#") {
				err = errors.Join(err, errors.New("`irc.channels` values must start with a '#'"))
			}
		}
	}

	return err
}
