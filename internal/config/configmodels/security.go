package configmodels

import "errors"

// Security will get auto generated. Security key is used to
// create hashed JWT tokens for the runners, for authentication purposes.
type Security struct {
	SecretKey string `mapstructure:"secret_key"`
}

func (s *Security) Validate() error {
	if s.SecretKey == "" {
		return errors.New("`secret_key` is required")
	}
	return nil
}
