package configmodels

import "gitlab.com/eemj/anime-pack/internal/config/configtypes"

const (
	// CacheMemory set the cache to use the a hashmap memory for caching purposes.
	CacheMemory string = "memory"
	// CacheRedis sets the cache to use a redis store for caching purposes.
	CacheRedis string = "redis"
)

// RedisOptions defines the `cache.redis_options` options.
type RedisOptions struct {
	Addresses    []string                     `mapstructure:"addresses"`
	MasterName   string                       `mapstructure:"master_name,omitempty"`
	Password     string                       `mapstructure:"password,omitempty"`
	WriteTimeout configtypes.HumanizeDuration `mapstructure:"write_timeout,omitempty"`
	ReadTimeout  configtypes.HumanizeDuration `mapstructure:"read_timeout,omitempty"`
}

// Cache defines the `cache` configuration.
type Cache struct {
	Type         string       `mapstructure:"type"`
	RedisOptions RedisOptions `mapstructure:"redis_options,omitempty"`
}
