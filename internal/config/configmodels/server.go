package configmodels

import (
	"compress/gzip"
	"errors"
	"fmt"
	"net"
	"strconv"
	"time"

	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	"gitlab.com/eemj/anime-pack/internal/router/middleware"
	sliceutil "go.jamie.mt/toolbox/slice_util"
	stringutil "go.jamie.mt/toolbox/string_util"
)

// CORS defines the 'cors' configuration.
type CORS struct {
	Enable           bool          `json:"enable" mapstructure:"enable"`
	AllowOrigins     []string      `json:"allowOrigins" mapstructure:"allow_origins,omitempty"`
	AllowMethods     []string      `json:"allowMethods" mapstructure:"allow_methods,omitempty"`
	MaxAge           time.Duration `json:"maxAge" mapstructure:"max_age,omitempty"`
	AllowCredentials bool          `json:"allowCredentials" mapstructure:"allow_credentials"`
}

func (c CORS) Validate() (err error) {
	if c.Enable {
		if len(c.AllowOrigins) == 0 {
			err = errors.Join(err, errors.New("`cors.allow_origins` is required"))
		}
		if len(c.AllowMethods) == 0 {
			err = errors.Join(err, errors.New("`cors.allow_methods` is required"))
		}
		if c.MaxAge < 0 {
			err = errors.Join(err, errors.New("`cors.max_age` must be greater than 0"))
		}
	}
	return err
}

// Authentication defines the HTTP Basic model and the 'authentication' configuration.
type Authentication struct {
	Username string `json:"username" mapstructure:"username"`
	Password string `json:"password" mapstructure:"password"`
}

// Servers defines the 'servers' configuration.
type Servers struct {
	ExternalURL string `json:"externalURL" mapstructure:"external_url"`
	GRPC        *GRPC  `json:"grpc" mapstructure:"grpc"`
	HTTP        *HTTP  `json:"http" mapstructure:"http"`
}

func (s Servers) Validate() (err error) {
	if s.GRPC == nil {
		err = errors.Join(err, errors.New("`servers.grpc` is required"))
	} else {
		err = errors.Join(err, s.GRPC.Validate())
	}

	if s.HTTP == nil {
		err = errors.Join(err, errors.New("`servers.http` is required"))
	} else {
		err = errors.Join(err, s.HTTP.Validate())
	}

	if stringutil.IsTrimmedEmpty(s.ExternalURL) {
		err = errors.Join(err, errors.New("`servers.external_url` is required"))
	}

	return err
}

var DefaultGRPCKeepalive = &GRPCKeepalive{
	PingDuration:       configtypes.HumanizeDuration((3 * time.Second).String()),
	KeepaliveTimeout:   configtypes.HumanizeDuration(((7 * time.Second) + (500 * time.Millisecond)).String()),
	EnforcementMinTime: configtypes.HumanizeDuration((3 * time.Second).String()),
}

// GRPCKeepalive defines the `servers.grpc.keep_alive` configuration.
type GRPCKeepalive struct {
	PingDuration       configtypes.HumanizeDuration `json:"pingDuration" mapstructure:"ping_duration"`
	KeepaliveTimeout   configtypes.HumanizeDuration `json:"keepaliveTimeout" mapstructure:"keepalive_timeout"`
	EnforcementMinTime configtypes.HumanizeDuration `json:"enforcementMinTime" mapstructure:"enforcement_min_time"`
}

func (g *GRPCKeepalive) Validate() (err error) {
	if g.PingDuration.Duration() == 0 {
		g.PingDuration = DefaultGRPCKeepalive.PingDuration
	} else if g.PingDuration.Duration() < 0 {
		err = errors.Join(err, errors.New("`grpc.keep_alive.ping_duration` must be greater than 0"))
	}

	if g.KeepaliveTimeout.Duration() == 0 {
		g.KeepaliveTimeout = DefaultGRPCKeepalive.KeepaliveTimeout
	} else if g.KeepaliveTimeout.Duration() < 0 {
		err = errors.Join(err, errors.New("`grpc.keep_alive.keepalive_timeout` must be greater than 0"))
	}

	if g.EnforcementMinTime.Duration() == 0 {
		g.EnforcementMinTime = DefaultGRPCKeepalive.EnforcementMinTime
	} else if g.EnforcementMinTime.Duration() < 0 {
		err = errors.Join(err, errors.New("`grpc.keep_alive.enforcement_min_time` must be greater than 0"))
	}

	return err
}

// GRPC defines the 'servers.grpc' configuration.
type GRPC struct {
	Protocol       configtypes.Protocol `json:"protocol" mapstructure:"protocol"`
	Address        string               `json:"address" mapstructure:"address"`
	Port           int                  `json:"port,omitempty" mapstructure:"port,omitempty"`
	UnixPermission int                  `json:"unixPermission" mapstructure:"unix_permission"`
	CertFile       *string              `json:"certFile" mapstructure:"cert_file"`
	KeyFile        *string              `json:"keyFile" mapstructure:"key_file"`
	Keepalive      *GRPCKeepalive       `json:"keepalive" mapstructure:"keepalive"`
}

func (g GRPC) HostPort() string {
	return net.JoinHostPort(
		g.Address,
		strconv.FormatUint(uint64(g.Port), 10),
	)
}

func (g *GRPC) Validate() (err error) {
	if g.Address == "" {
		err = errors.Join(err, errors.New("`grpc.address` is required"))
	}

	if g.Port < 0 || g.Port > 65535 {
		err = errors.Join(err, errors.New("`grpc.port` must be between 0 and 65535"))
	}

	if g.Protocol == configtypes.ProtocolUNIX && g.UnixPermission == 0 {
		err = errors.Join(err, errors.New("`grpc.unix_permission` must be greater than 0"))
	}

	if g.Protocol != configtypes.ProtocolTCP && g.Protocol != configtypes.ProtocolUNIX {
		err = errors.Join(fmt.Errorf(
			"`grpc.protocol` is unsupported, expected either (%s)",
			sliceutil.SurroundWith(
				'`',
				configtypes.ProtocolTCP.String(),
				configtypes.ProtocolUNIX.String(),
			),
		))
	}

	if g.Keepalive == nil {
		g.Keepalive = DefaultGRPCKeepalive
	} else {
		err = errors.Join(err, g.Keepalive.Validate())
	}

	return err
}

// GZIP defines the `servers.http.gzip` configuration.
type GZIP struct {
	Enable bool `json:"enable" mapstructure:"enable,omitempty"`
	Level  int  `json:"level" mapstructure:"level,omitempty"`
}

func (g GZIP) Validate() (err error) {
	if g.Enable && g.Level > gzip.BestSpeed && g.Level <= gzip.BestCompression {
		return fmt.Errorf(
			"unexpected gzip level, expected value between `%d` and `%d`",
			gzip.BestSpeed,
			gzip.BestCompression,
		)
	}

	return
}

// HTTP defines the 'servers.http' configuration.
type HTTP struct {
	Protocol configtypes.Protocol `json:"protocol" mapstructure:"protocol"`
	Address  string               `json:"address" mapstructure:"address"`
	Port     int                  `json:"port,omitempty" mapstructure:"port,omitempty"`

	ServeImages    bool `json:"serveImages,omitempty" mapstructure:"serve_images,omitempty"`
	UnixPermission int  `json:"unixPermission,omitempty" mapstructure:"unix_permission,omitempty"`

	GZIP GZIP `json:"gzip" mapstructure:"gzip"`

	EnablePProf   bool `json:"enablePprof,omitempty" mapstructure:"enable_pprof,omitempty"`
	EnableSwagger bool `json:"enableSwagger,omitempty" mapstructure:"enable_swagger,omitempty"`

	CORS           CORS            `json:"cors,omitempty" mapstructure:"cors,omitempty"`
	Authentication Authentication  `json:"authentication,omitempty" mapstructure:"authentication,omitempty"`
	Middleware     *HTTPMiddleware `json:"middleware,omitempty" mapstructure:"middleware,omitempty"`
}

type LoggerHTTPMiddleware struct {
	FilterExclude string `json:"filterExclude,omitempty" mapstructure:"filter_exclude,omitempty"`
}

func (h *LoggerHTTPMiddleware) Validate() error {
	if stringutil.IsTrimmedEmpty(h.FilterExclude) {
		h.FilterExclude = middleware.DefaultLoggerMiddlewareFilterExclude
	}

	return nil
}

var DefaultHTTPMiddleware = HTTPMiddleware{
	Logger: &LoggerHTTPMiddleware{
		FilterExclude: middleware.DefaultLoggerMiddlewareFilterExclude,
	},
}

type HTTPMiddleware struct {
	Logger *LoggerHTTPMiddleware `json:"logger,omitempty" mapstructure:"logger,omitempty"`
}

func (h *HTTPMiddleware) Validate() error {
	if h.Logger == nil {
		h.Logger = &LoggerHTTPMiddleware{FilterExclude: middleware.DefaultLoggerMiddlewareFilterExclude}
		return nil
	}

	return h.Logger.Validate()
}

// HostPort returns the host and port combined into an address.
func (h HTTP) HostPort() string {
	return net.JoinHostPort(
		h.Address,
		strconv.FormatUint(uint64(h.Port), 10),
	)
}

func (h *HTTP) Validate() (err error) {
	if h.Address == "" {
		err = errors.Join(err, errors.New("`http.address` is required"))
	}

	if h.Port < 0 || h.Port > 65535 {
		err = errors.Join(err, errors.New("`http.port` must be between 0 and 65535"))
	}

	if h.Protocol == configtypes.ProtocolUNIX && h.UnixPermission == 0 {
		err = errors.Join(err, errors.New("`http.unix_permission` must be greater than 0"))
	}

	if h.Protocol != configtypes.ProtocolHTTP2C &&
		h.Protocol != configtypes.ProtocolHTTP &&
		h.Protocol != configtypes.ProtocolUNIX {
		err = errors.Join(err, fmt.Errorf(
			"unsupported protocol, expected either (%s)",
			sliceutil.SurroundWith(
				'`',
				configtypes.ProtocolHTTP.String(),
				configtypes.ProtocolHTTP2C.String(),
				configtypes.ProtocolUNIX.String(),
			),
		))
	}

	if h.Middleware == nil {
		h.Middleware = &DefaultHTTPMiddleware
	}

	err = errors.Join(
		err,
		h.GZIP.Validate(),
		h.CORS.Validate(),
		h.Middleware.Validate(),
	)

	return
}
