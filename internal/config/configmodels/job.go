package configmodels

import (
	"errors"

	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
)

// SimulcastSync defines the `jobs.simulcast_sync` configuration.
type SimulcastSync struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// LatestSync defines the `jobs.latest_sync` configuration.
type LatestSync struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// AiringSync defines the `jobs.airing_sync` configuration.
type AiringSync struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// RemoveUnusedImages defines the `jobs.remove_unused_images` configuration.
type RemoveUnusedImages struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// AutoDownload defines the `jobs.auto_download` configuration.
type AutoDownload struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`

	Pages             int            `json:"pages" mapstructure:"pages"`
	PreferredGroups   map[string]int `json:"preferredGroups" mapstructure:"preferred_groups"`
	BlacklistGroups   []string       `json:"blacklistGroups" mapstructure:"blacklist_groups"`
	AlwaysBestQuality bool           `json:"alwaysBestQuality" mapstructure:"always_best_quality"`
}

// VideoSync defines the `jobs.video_sync` configuration.
type VideoSync struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`

	DryRun bool `json:"dry_run" mapstructure:"dry_run"`
}

// VideoChecksum defines the `jobs.video_checksum` configuration.
type VideoChecksum struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`

	Concurrent int `json:"concurrent" mapstructure:"concurrent"`
}

// PackSync defines the `jobs.pack_sync` configuration.
type PackSync struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`

	Concurrent int `json:"concurrent" mapstructure:"concurrent"`
}

// RebalanceTitle defines the `jobs.rebalance_title` configuration.
type RebalanceTitle struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// RebalanceTitle defines the `jobs.scan_unreviewed_titles` configuration.
type ScanUnreviewedTitles struct {
	Disable bool                         `json:"disable" mapstructure:"disable"`
	Manual  bool                         `json:"manual" mapstructure:"manual"`
	Period  configtypes.HumanizeDuration `json:"period" mapstructure:"period"`
}

// Jobs defines the `jobs` configuration.
type Jobs struct {
	LatestSync           LatestSync           `json:"latestSync" mapstructure:"latest_sync"`
	AiringSync           AiringSync           `json:"airingSync" mapstructure:"airing_sync"`
	PackSync             PackSync             `json:"packSync" mapstructure:"pack_sync"`
	VideoSync            VideoSync            `json:"videoSync" mapstructure:"video_sync"`
	SimulcastSync        SimulcastSync        `json:"simulcastSync" mapstructure:"simulcast_sync"`
	RemoveUnusedImages   RemoveUnusedImages   `json:"removeUnusedImages" mapstructure:"remove_unused_images"`
	AutoDownload         AutoDownload         `json:"autoDownload" mapstructure:"auto_download"`
	VideoChecksum        VideoChecksum        `json:"videoChecksum" mapstructure:"video_checksum"`
	RebalanceTitle       RebalanceTitle       `json:"rebalanceTitle" mapstructure:"rebalance_title"`
	ScanUnreviewedTitles ScanUnreviewedTitles `json:"scanUnreviewedTitles" mapstructure:"scan_unreviewed_titles"`
}

// Validate ensure that the job configuration values are given as valid values.
func (j *Jobs) Validate() (err error) {
	if !j.SimulcastSync.Disable && !j.SimulcastSync.Manual && j.SimulcastSync.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`simulcast_sync.period` expected to be greater than 0"))
	}

	if !j.LatestSync.Disable && !j.LatestSync.Manual && j.LatestSync.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`latest_sync.period` expected to be greater than 0"))
	}

	if !j.AiringSync.Disable && !j.AiringSync.Manual && j.AiringSync.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`airing_sync.period` expected to be greater than 0"))
	}

	if !j.VideoSync.Disable && !j.VideoSync.Manual && j.VideoSync.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`video_sync.period` expected to be greater than 0"))
	}

	if j.PackSync.Concurrent <= 0 {
		err = errors.Join(err, errors.New("`pack_sync.concurrent` expected to be greater than 0"))
	}
	if !j.PackSync.Disable && !j.PackSync.Manual && j.PackSync.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`pack_sync.period` expected to be greater than 0"))
	}

	if j.AutoDownload.Pages <= 0 {
		err = errors.Join(err, errors.New("`auto_download.pages` expected to be greater than 0"))
	}

	if !j.AutoDownload.Disable && !j.AutoDownload.Manual && j.AutoDownload.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`auto_download.period` expected to be greater than 0"))
	}

	if j.VideoChecksum.Concurrent <= 0 {
		err = errors.Join(err, errors.New("`video_checksum.concurrent` expected to be greater than 0"))
	}
	if !j.VideoChecksum.Disable && !j.VideoChecksum.Manual && j.VideoChecksum.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`video_checksum.period` expected to be greater than 0"))
	}

	if !j.ScanUnreviewedTitles.Disable && !j.ScanUnreviewedTitles.Manual && j.ScanUnreviewedTitles.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`scan_unreviewed_titles.period` expected to be greater than 0"))
	}

	if !j.RemoveUnusedImages.Disable && !j.RemoveUnusedImages.Manual && j.RemoveUnusedImages.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`remove_unused_images.period` expected to be greater than 0"))
	}

	if !j.RebalanceTitle.Disable && !j.RebalanceTitle.Manual && j.RebalanceTitle.Period.Duration() == 0 {
		err = errors.Join(err, errors.New("`rebalance_title` expected to be greater than 0"))
	}

	if err != nil {
		return err
	}
	return nil
}
