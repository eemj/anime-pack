package configmodels

const (
	DatabaseDriverPostgres string = "postgres"
	DatabaseDriverMSSQL    string = "mssql"
	DatabaseDriverMySQL    string = "mysql"
	DatabaseDriverSqlite3  string = "sqlite3"
)

type (
	// Database defines the `database` configuration
	Database struct {
		Address  string `mapstructure:"address,omitempty"`
		Port     int    `mapstructure:"port,omitempty"`
		User     string `mapstructure:"username,omitempty"`
		Password string `mapstructure:"password,omitempty"`
		Name     string `mapstructure:"name"`
		Driver   string `mapstructure:"driver"`
	}
)
