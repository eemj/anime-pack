package configmodels

import "errors"

type Memory struct {
	NumberOfCounters int64 `json:"numberOfCounters,omitempty" mapstructure:"number_of_counters"`
	MaximumCost      int64 `json:"maximumCost,omitempty" mapstructure:"maximum_cost"`
	BufferItems      int64 `json:"bufferItems,omitempty" mapstructure:"buffer_items"`
}

func (m *Memory) Validate() (err error) {
	if m.NumberOfCounters < 0 {
		err = errors.Join(errors.New("`memory.number_of_counters` must be greater than 0"))
	} else if m.NumberOfCounters == 0 {
		m.NumberOfCounters = 1e7
	}

	if m.BufferItems < 0 {
		err = errors.Join(errors.New("`memory.buffer_items` must be greater than 0"))
	} else if m.BufferItems == 0 {
		m.BufferItems = 64
	}

	if m.MaximumCost < 0 {
		err = errors.Join(errors.New("`memory.maximum_cost` must be greater than 0"))
	} else if m.MaximumCost == 0 {
		m.MaximumCost = 1 << 30 // 1GB
	}

	return err
}
