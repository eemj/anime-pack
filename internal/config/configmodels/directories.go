package configmodels

import (
	"errors"
	"io/fs"
	"os"
	"os/user"
	"path/filepath"
)

const (
	applicationName = "anime-pack"

	// defaultPattern determines the defualt directories pattern.
	defaultPattern = "{{Title}} Episode {{Episode}} [{{Height}}p]"
)

// defaultMediaPermissions defines the default permissions
// if they're not set in the configuration.
var defaultMediaPermissions = &MediaPermission{
	Directory: os.ModeDir | os.ModePerm,
	Download:  os.ModePerm,
	Image:     os.ModePerm,
}

// Directories defines the `directories` configuration.
// These directories represent where certain items are going to stored
// on the file system.
type Directories struct {
	Images             string `mapstructure:"images"`
	Downloads          string `mapstructure:"downloads"`
	TemporaryDownloads string `mapstructure:"temporary_downloads"`
	Pattern            string `mapstructure:"pattern"`

	MediaPermission MediaPermission `mapstructure:"media_permission"`
}

func (d *Directories) setDefault() (err error) {
	usr, err := user.Current()

	if d.Downloads == "" {
		if usr != nil {
			d.Downloads = filepath.Join(usr.HomeDir, "Downloads", applicationName)
		} else {
			d.Downloads = "downloads"
		}
	}
	if d.Images == "" {
		if usr != nil {
			d.Images = filepath.Join(usr.HomeDir, "Pictures", applicationName)
		} else {
			d.Images = "images"
		}
	}
	if d.TemporaryDownloads == "" {
		d.TemporaryDownloads = filepath.Join(d.Downloads, ".tmp")
	}
	if d.Pattern == "" {
		d.Pattern = defaultPattern
	}
	return
}

func (d *Directories) Validate() (err error) {
	return errors.Join(
		d.setDefault(),
		d.MediaPermission.Validate(),
	)
}

// MediaPermission defines the permission structure for the files
// to be-created with.
type MediaPermission struct {
	Directory fs.FileMode `mapstructure:"directory"`
	Download  fs.FileMode `mapstructure:"download"`
	Image     fs.FileMode `mapstructure:"image"`
}

func (m *MediaPermission) Validate() (err error) {
	if m.Directory == 0o000 {
		m.Directory = defaultMediaPermissions.Directory
	}
	if m.Image == 0o000 {
		m.Image = defaultMediaPermissions.Image
	}
	if m.Download == 0o000 {
		m.Download = defaultMediaPermissions.Download
	}

	return nil
}
