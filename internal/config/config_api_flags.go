package config

import (
	"github.com/spf13/pflag"
	"go.uber.org/zap/zapcore"
)

const (
	flagLogLevel = "log.level"

	flagDatabaseShowQueries = "database.show-queries"

	flagIrcDisable = "irc.disable"

	flagJobDisable = "job.disable"

	flagConfigPath = "config.path"
)

type APIFlags struct {
	DatabaseShowQueries bool

	IrcDisable bool
	JobDisable bool

	LogLevel int

	ConfigPath string
}

func ParseAPIArgs() *APIFlags {
	args := &APIFlags{}

	pflag.BoolVar(&args.DatabaseShowQueries, flagDatabaseShowQueries, false, `show database queries (debug must be enabled)`)
	pflag.BoolVar(&args.IrcDisable, flagIrcDisable, false, `skip IRC connection, it will just run the application without IRC`)
	pflag.BoolVar(&args.JobDisable, flagJobDisable, false, `skip Job initialization, it will just run the application without any jobs`)
	pflag.IntVar(&args.LogLevel, flagLogLevel, int(zapcore.InfoLevel), `values between '-1' to '5', starting with debug and ending with fatal`)
	pflag.StringVar(&args.ConfigPath, flagConfigPath, ``, `read config path`)
	pflag.Parse()

	return args
}
