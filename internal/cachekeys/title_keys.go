package cachekeys

import (
	"strconv"
)

func Title(id *int64, name, seasonNumber *string, year *int) string {
	if (id == nil || *id == 0) &&
		(name == nil || *name == "") &&
		(seasonNumber == nil || *seasonNumber == "") &&
		(year == nil || *year == 0) {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("title:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if name != nil && *name != "" {
		kb.WriteString("{name=")
		kb.WriteString(*name)
		kb.WriteRune('}')
	}
	if seasonNumber != nil && *seasonNumber != "" {
		kb.WriteString("{seasonNumber=")
		kb.WriteString(*seasonNumber)
		kb.WriteRune('}')
	}
	if year != nil && *year > 0 {
		kb.WriteString("{year=")
		kb.WriteString(strconv.Itoa(*year))
		kb.WriteString("}")
	}
	return kb.String()
}

func TitleEpisode(id, titleID, episodeID *int64) string {
	if (id == nil || *id == 0) &&
		(titleID == nil || *titleID == 0) &&
		(episodeID == nil || *episodeID == 0) {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("title-episode:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if titleID != nil && *titleID > 0 {
		kb.WriteString("{titleID=")
		kb.WriteString(strconv.FormatInt(*titleID, 10))
		kb.WriteRune('}')
	}
	if episodeID != nil && *episodeID > 0 {
		kb.WriteString("{episodeID=")
		kb.WriteString(strconv.FormatInt(*episodeID, 10))
		kb.WriteRune('}')
	}
	return kb.String()
}
