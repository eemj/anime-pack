package cachekeys

import "testing"

func TestRunner(t *testing.T) {
	// Test case 1: Nil ident
	var nilIdent *string
	expected := ""
	result := Runner(nilIdent)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Empty ident
	emptyIdent := ""
	expected = ""
	result = Runner(&emptyIdent)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-empty ident
	ident := "example"
	expected = (KeyPrefix + "runner:{ident=example}")
	result = Runner(&ident)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}
