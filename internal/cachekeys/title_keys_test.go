package cachekeys

import (
	"testing"
)

// Test function for Title
func TestTitle(t *testing.T) {
	// Test case 1: All nil parameters
	var id *int64
	var name, seasonNumber *string
	var year *int
	expected := ""
	result := Title(id, name, seasonNumber, year)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}

	// Test case 2: Non-zero id, empty name, seasonNumber, and year
	id = new(int64)
	*id = 123
	expected = "anime-pack:title:{id=123}"
	result = Title(id, name, seasonNumber, year)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}

	// Test case 3: Non-zero id, non-empty name, and non-empty seasonNumber
	name = new(string)
	*name = "Test Title"
	seasonNumber = new(string)
	*seasonNumber = "Season 1"
	expected = "anime-pack:title:{id=123}{name=Test Title}{seasonNumber=Season 1}"
	result = Title(id, name, seasonNumber, year)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}

	// Test case 4: Non-zero id, non-empty name, non-empty seasonNumber, and non-zero year
	year = new(int)
	*year = 2024
	expected = "anime-pack:title:{id=123}{name=Test Title}{seasonNumber=Season 1}{year=2024}"
	result = Title(id, name, seasonNumber, year)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}
}

// Test function for TitleEpisode
func TestTitleEpisode(t *testing.T) {
	// Test case 1: All nil parameters
	var id, titleID, episodeID *int64
	expected := ""
	result := TitleEpisode(id, titleID, episodeID)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}

	// Test case 2: Non-zero id, titleID, and episodeID
	id = new(int64)
	*id = 123
	titleID = new(int64)
	*titleID = 456
	episodeID = new(int64)
	*episodeID = 789
	expected = "anime-pack:title-episode:{id=123}{titleID=456}{episodeID=789}"
	result = TitleEpisode(id, titleID, episodeID)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}

	// Test case 3: Non-zero id, nil titleID, and non-zero episodeID
	titleID = nil
	expected = "anime-pack:title-episode:{id=123}{episodeID=789}"
	result = TitleEpisode(id, titleID, episodeID)
	if result != expected {
		t.Errorf("Expected '%s', but got '%s'", expected, result)
	}
}
