package cachekeys

import "testing"

func TestAnime(t *testing.T) {
	// Test case 1: Valid non-zero id
	id := int64(123)
	expected := (KeyPrefix + "anime:{id=123}")
	result := Anime(&id)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Valid zero id
	zeroID := int64(0)
	expected = ""
	result = Anime(&zeroID)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: nil id
	var nilID *int64
	expected = ""
	result = Anime(nilID)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}
