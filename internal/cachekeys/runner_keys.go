package cachekeys

func Runner(ident *string) string {
	if ident == nil || *ident == "" {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("runner:")
	kb.WriteString("{ident=")
	kb.WriteString(*ident)
	kb.WriteRune('}')
	return kb.String()
}
