package cachekeys

import "strconv"

func Bot(name *string) string {
	if name == nil || *name == "" {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("bot:")
	kb.WriteString("{name=")
	kb.WriteString(*name)
	kb.WriteRune('}')
	return kb.String()
}

func ReleaseGroup(name *string) string {
	if name == nil || *name == "" {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("releaseGroup:")
	kb.WriteString("{name=")
	kb.WriteString(*name)
	kb.WriteRune('}')
	return kb.String()
}

func Episode(id *int64, name *string) string {
	if (id == nil || *id == 0) &&
		(name == nil || *name == "") {
		return ""
	}
	kb := newKeyBuilder()
	kb.WriteString("episode:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if name != nil && *name != "" {
		kb.WriteString("{name=")
		kb.WriteString(*name)
		kb.WriteRune('}')
	}
	return kb.String()
}

func Quality(id, height *int64) string {
	if (id == nil || *id == 0) &&
		(height == nil || *height == 0) {
		return ""
	}
	kb := newKeyBuilder()
	kb.WriteString("quality:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if height != nil && *height > 0 {
		kb.WriteString("{height=")
		kb.WriteString(strconv.FormatInt(*height, 10))
		kb.WriteRune('}')
	}
	return kb.String()
}

func Genre(id *int64, name *string) string {
	if (id == nil || *id == 0) &&
		(name == nil || *name == "") {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("genre:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if name != nil && *name != "" {
		kb.WriteString("{name=")
		kb.WriteString(*name)
		kb.WriteRune('}')
	}
	return kb.String()
}

func Studio(id *int64, name *string) string {
	if (id == nil || *id == 0) &&
		(name == nil || *name == "") {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("studio:")
	if id != nil && *id > 0 {
		kb.WriteString("{id=")
		kb.WriteString(strconv.FormatInt(*id, 10))
		kb.WriteRune('}')
	}
	if name != nil && *name != "" {
		kb.WriteString("{name=")
		kb.WriteString(*name)
		kb.WriteRune('}')
	}
	return kb.String()
}

const (
	AnimeSeasons       = (KeyPrefix + "anime-seasons")
	AnimeSeasonYears   = (KeyPrefix + "anime-season-years")
	AnimeFormats       = (KeyPrefix + "anime-formats")
	AnimeStatuses      = (KeyPrefix + "anime-statuses")
	AnimeGenres        = (KeyPrefix + "anime-genres")
	AnimeStudios       = (KeyPrefix + "anime-studios")
	AnimeSearchSet     = (KeyPrefix + "anime-search-set")
	AnimeReleaseGroups = (KeyPrefix + "anime-release-groups")

	Simulcasts = (KeyPrefix + "simulcasts")

	Packlist = (KeyPrefix + "packlist")
)
