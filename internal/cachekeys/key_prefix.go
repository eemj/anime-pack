package cachekeys

import "strings"

const (
	// BaseKeyPrefix resembles the cache prefix to be used
	// for distributed systems.
	BaseKeyPrefix = "anime-pack"

	// KeyPrefix resembles the cache prefix to be used
	// for distributed systems.
	KeyPrefix = (BaseKeyPrefix + ":")
)

func newKeyBuilder() *strings.Builder {
	sb := new(strings.Builder)
	sb.WriteString(KeyPrefix)
	return sb
}
