package cachekeys

import "testing"

func TestBot(t *testing.T) {
	// Test case 1: Nil name
	var nilName *string
	expected := ""
	result := Bot(nilName)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Empty name
	emptyName := ""
	expected = ""
	result = Bot(&emptyName)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-empty name
	name := "example"
	expected = (KeyPrefix + "bot:{name=example}")
	result = Bot(&name)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func TestReleaseGroup(t *testing.T) {
	// Test case 1: Nil name
	var nilName *string
	expected := ""
	result := ReleaseGroup(nilName)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Empty name
	emptyName := ""
	expected = ""
	result = ReleaseGroup(&emptyName)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-empty name
	name := "example"
	expected = (KeyPrefix + "releaseGroup:{name=example}")
	result = ReleaseGroup(&name)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func TestEpisode(t *testing.T) {
	// Test case 1: All nil parameters
	var id *int64
	var name *string
	expected := ""
	result := Episode(id, name)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Non-zero id, empty name
	idVal := int64(123)
	nameVal := ""
	expected = (KeyPrefix + "episode:{id=123}")
	result = Episode(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-zero id, non-empty name
	nameVal = "Test Episode"
	expected = (KeyPrefix + "episode:{id=123}{name=Test Episode}")
	result = Episode(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func TestQuality(t *testing.T) {
	// Test case 1: All nil parameters
	var id, height *int64
	expected := ""
	result := Quality(id, height)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Non-zero id, zero height
	idVal := int64(123)
	heightVal := int64(0)
	expected = (KeyPrefix + "quality:{id=123}")
	result = Quality(&idVal, &heightVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-zero id, non-zero height
	heightVal = int64(1080)
	expected = (KeyPrefix + "quality:{id=123}{height=1080}")
	result = Quality(&idVal, &heightVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func TestGenre(t *testing.T) {
	// Test case 1: All nil parameters
	var id *int64
	var name *string
	expected := ""
	result := Genre(id, name)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Non-zero id, empty name
	idVal := int64(123)
	nameVal := ""
	expected = (KeyPrefix + "genre:{id=123}")
	result = Genre(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-zero id, non-empty name
	nameVal = "Test Genre"
	expected = (KeyPrefix + "genre:{id=123}{name=Test Genre}")
	result = Genre(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}

func TestStudio(t *testing.T) {
	// Test case 1: All nil parameters
	var id *int64
	var name *string
	expected := ""
	result := Studio(id, name)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 2: Non-zero id, empty name
	idVal := int64(123)
	nameVal := ""
	expected = (KeyPrefix + "studio:{id=123}")
	result = Studio(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}

	// Test case 3: Non-zero id, non-empty name
	nameVal = "Test Studio"
	expected = (KeyPrefix + "studio:{id=123}{name=Test Studio}")
	result = Studio(&idVal, &nameVal)
	if result != expected {
		t.Errorf("Expected %s, but got %s", expected, result)
	}
}
