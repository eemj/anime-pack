package cachekeys

import (
	"strconv"

	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

func LibraryVideo(arg params.FilterLibraryParams) string {
	if (arg.VideoPath != nil && *arg.VideoPath != "") &&
		(arg.XDCCID != nil && *arg.XDCCID > 0) {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("library-video:")
	if arg.VideoPath != nil && *arg.VideoPath != "" {
		kb.WriteString("{videoPath=")
		kb.WriteString(*arg.VideoPath)
		kb.WriteRune('}')
	}
	if arg.XDCCID != nil && *arg.XDCCID > 0 {
		kb.WriteString("{xdccID=")
		kb.WriteString(strconv.FormatInt(*arg.XDCCID, 10))
		kb.WriteRune('}')
	}

	return kb.String()
}

func AnimeEpisode(arg params.FilterAnimeEpisodeParams) string {
	if arg.AnimeID == 0 {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("anime-episode:")
	kb.WriteString("{animeID=")
	kb.WriteString(strconv.FormatInt(arg.AnimeID, 10))
	kb.WriteRune('}')
	if arg.EpisodeID != nil && *arg.EpisodeID > 0 {
		kb.WriteString("{episodeID=")
		kb.WriteString(strconv.FormatInt(*arg.EpisodeID, 10))
		kb.WriteRune('}')
	}
	if arg.EpisodeName != nil && *arg.EpisodeName != "" {
		kb.WriteString("{episodeName=")
		kb.WriteString(*arg.EpisodeName)
		kb.WriteRune('}')
	}
	return kb.String()
}

func AnimeMedia(arg params.FilterAnimeMediaParams) string {
	kb := newKeyBuilder()
	kb.WriteString("anime-media:")
	if arg.AnilistID != nil && *arg.AnilistID > 0 {
		kb.WriteString("{anilistID=")
		kb.WriteString(strconv.FormatInt(*arg.AnilistID, 10))
		kb.WriteRune('}')
	}
	if arg.MalID != nil && *arg.MalID > 0 {
		kb.WriteString("{malID=")
		kb.WriteString(strconv.FormatInt(*arg.MalID, 10))
		kb.WriteRune('}')
	}
	return kb.String()
}
