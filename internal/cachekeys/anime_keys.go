package cachekeys

import "strconv"

func Anime(id *int64) string {
	if id == nil || *id == 0 {
		return ""
	}

	kb := newKeyBuilder()
	kb.WriteString("anime:")
	kb.WriteString("{id=")
	kb.WriteString(strconv.FormatInt(*id, 10))
	kb.WriteRune('}')
	return kb.String()
}
