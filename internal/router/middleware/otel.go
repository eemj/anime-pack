package middleware

import (
	"fmt"
	"reflect"

	"github.com/labstack/echo/v4"

	"go.opentelemetry.io/otel"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/propagation"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/semconv/v1.17.0/httpconv"
	oteltrace "go.opentelemetry.io/otel/trace"
)

const (
	serviceName = "anime-pack-api"
	tracerKey   = "otel"
	tracerName  = "gitlab.com/eemj/anime-pack/internal/router/middleware/otel.go"
)

// OtelMiddleware returns echo middleware which will trace incoming requests.
func OtelMiddleware() echo.MiddlewareFunc {
	tracer := otel.GetTracerProvider().Tracer(reflect.TypeOf(OtelMiddleware).PkgPath())

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			path := c.Path()
			if path == "/*/*" || path == "/images/*" || path == "/ws" || path == "/metrics" || path == "/healthz" {
				return next(c)
			}

			c.Set(tracerKey, tracer)
			request := c.Request()
			savedCtx := request.Context()
			defer func() {
				request = request.WithContext(savedCtx)
				c.SetRequest(request)
			}()
			ctx := otel.GetTextMapPropagator().Extract(savedCtx, propagation.HeaderCarrier(request.Header))
			opts := []oteltrace.SpanStartOption{
				oteltrace.WithAttributes(httpconv.ServerRequest(serviceName, request)...),
				oteltrace.WithSpanKind(oteltrace.SpanKindServer),
			}
			if path := c.Path(); path != "" {
				rAttr := semconv.HTTPRoute(path)
				opts = append(opts, oteltrace.WithAttributes(rAttr))
			}
			rawPath := c.Request().URL.Path
			spanName := request.Method + " " + rawPath
			if spanName == "" {
				spanName = fmt.Sprintf("HTTP %s route not found", request.Method)
			}

			ctx, span := tracer.Start(ctx, spanName, opts...)
			defer span.End()

			// pass the span through the request context
			c.SetRequest(request.WithContext(ctx))

			if span.SpanContext().HasTraceID() {
				c.Response().Header().Add("AP-Correlation-ID", span.SpanContext().TraceID().String())
			}

			// serve the request to the next middleware
			err := next(c)
			if err != nil {
				span.SetAttributes(attribute.String("echo.error", err.Error()))
				// invokes the registered HTTP error handler
				c.Error(err)
			}

			status := c.Response().Status
			span.SetStatus(httpconv.ServerStatus(status))
			if status > 0 {
				span.SetAttributes(semconv.HTTPStatusCode(status))
			}

			return err
		}
	}
}
