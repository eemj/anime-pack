package middleware

import (
	"context"
	"strconv"
	"time"

	"github.com/google/cel-go/cel"
	"github.com/google/cel-go/common/types"
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/toolbox/http_util/headers"
	"go.uber.org/zap"
)

const (
	DefaultLoggerMiddlewareFilterExclude = `((path.startsWith("/metrics") || path.startsWith("/healthz")) || status_code == 404)`
)

const (
	filterExcludeMethod        = "method"
	filterExcludePath          = "path"
	filterExcludeContentLength = "content_length"
	filterExcludeStatusCode    = "status_code"
)

type LoggerMiddlewareConfig struct {
	FilterExclude string
}

type LoggerMiddleware struct {
	astProgram cel.Program
}

func (h *LoggerMiddleware) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named("http.logger")
}

func (h *LoggerMiddleware) shouldExclude(
	ctx context.Context,
	method, path string,
	contentLength, statusCode int,
) bool {
	out, _, err := h.astProgram.ContextEval(ctx, map[string]any{
		filterExcludeMethod:        method,
		filterExcludePath:          path,
		filterExcludeContentLength: contentLength,
		filterExcludeStatusCode:    statusCode,
	})
	if err != nil {
		return false
	}
	if types.IsBool(out) {
		return out.Value().(bool)
	}
	return false
}

func (h *LoggerMiddleware) Middleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var (
				start      = time.Now()
				statusCode int
				err        = next(c)
			)

			if err != nil {
				if httpError, ok := err.(*echo.HTTPError); ok {
					statusCode = httpError.Code
				}
			}

			var (
				req   = c.Request()
				res   = c.Response()
				since = time.Since(start)
				path  = req.URL.Path
				query = req.URL.Query().Encode()
			)
			if statusCode == 0 {
				statusCode = res.Status
			}

			requestsByStatusCode.
				WithLabelValues(path, strconv.Itoa(statusCode)).
				Inc()

			if len(query) > 0 {
				path += "/?" + query
			}

			var (
				username, _, _   = req.BasicAuth()
				rawContentLength = req.Header.Get(headers.HeaderContentLength)
				contentLength, _ = strconv.Atoi(rawContentLength) // Defaults to 0
				userAgent        = req.Header.Get(headers.HeaderUserAgent)
				method           = req.Method
			)

			host, _, hostErr := utils.RealRemoteAddr(req)
			if hostErr != nil {
				host = c.RealIP()
			}

			requestsByAddr.WithLabelValues(host).Inc()

			if h.shouldExclude(
				req.Context(),
				method,
				path,
				contentLength,
				statusCode,
			) {
				return nil
			}

			h.L(req.Context()).Info(
				"access",
				zap.String("method", method),
				zap.Int("status_code", statusCode),
				zap.Int("content_length", contentLength),
				zap.String("user_agent", userAgent),
				zap.String("path", path),
				zap.String("host", host),
				zap.String("auth", username),
				zap.Int64("bytes_out", res.Size),
				zap.Duration("duration", since),
			)

			return nil
		}
	}
}

func NewLoggerMiddleware(cfg LoggerMiddlewareConfig) (*LoggerMiddleware, error) {
	env, err := cel.NewEnv(
		cel.Variable(filterExcludeMethod, cel.StringType),
		cel.Variable(filterExcludePath, cel.StringType),
		cel.Variable(filterExcludeContentLength, cel.IntType),
		cel.Variable(filterExcludeStatusCode, cel.IntType),
	)
	if err != nil {
		return nil, err
	}
	ast, issues := env.Compile(cfg.FilterExclude)
	if issues != nil {
		err := issues.Err()
		if err != nil {
			return nil, err
		}
	}
	program, err := env.Program(ast)
	if err != nil {
		return nil, err
	}
	return &LoggerMiddleware{
		astProgram: program,
	}, nil
}
