package middleware

import (
	"net/url"

	"github.com/labstack/echo/v4"
	mw "github.com/labstack/echo/v4/middleware"
)

type StaticBalancer struct {
	host *url.URL
}

// AddTarget doesn't do anything but return true
func (b *StaticBalancer) AddTarget(*mw.ProxyTarget) bool { return true }

// RemoveTarget doesn't do anything but return true
func (b *StaticBalancer) RemoveTarget(string) bool { return true }

// StaticBalancer always returns the host specified by the user
func (b *StaticBalancer) Next(c echo.Context) *mw.ProxyTarget {
	return &mw.ProxyTarget{
		URL:  b.host,
		Name: b.host.String(),
		Meta: nil,
	}
}

func NewStaticBalancer(host *url.URL) mw.ProxyBalancer {
	return &StaticBalancer{host: host}
}
