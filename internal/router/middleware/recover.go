package middleware

import (
	"fmt"
	"runtime"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

// Recover prevents panics and outputs the panic message through our logger.
func Recover(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer func() {
			if r := recover(); r != nil {
				err, ok := r.(error)
				if !ok {
					err = fmt.Errorf("%v", r)
				}

				stack := make([]byte, 0x1000)
				length := runtime.Stack(stack, false)
				scopeLog().Errorw(
					"recover",
					zap.Error(err),
					zap.String("stack_trace", string(stack[:length])),
				)
				c.Error(err)
			}
		}()
		return next(c)
	}
}
