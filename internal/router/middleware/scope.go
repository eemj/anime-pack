package middleware

import (
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

const namespace = "middleware"

func scopeLog() *zap.SugaredLogger {
	return log.Named(namespace).Sugar()
}
