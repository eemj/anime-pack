package middleware

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	requestsByStatusCode = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: `anime_pack`,
		Subsystem: `http`,
		Name:      `status_code_by_path`,
		Help:      `HTTP status codes per path`,
	}, []string{"path", "status_code"})

	requestsByAddr = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: `anime_pack`,
		Subsystem: `http`,
		Name:      `requests_by_addr`,
		Help:      `HTTP requests per address`,
	}, []string{"addr"})
)
