package router

import (
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const (
	manifestPath = "/manifest.json"
)

func (r *Router) EnableBasicAuth(username, password string) {
	if utils.IsEmpty(username) || utils.IsEmpty(password) {
		return
	}

	r.Use(middleware.BasicAuthWithConfig(middleware.BasicAuthConfig{
		Skipper: func(c echo.Context) bool {
			path := c.Request().URL.Path

			return strings.HasPrefix(path, manifestPath) ||
				strings.HasPrefix(path, imagesPath)
		},
		Validator: func(_username, _password string, _ echo.Context) (bool, error) {
			return _username == username && _password == password, nil
		},
	}))
}
