package response

import (
	"github.com/labstack/echo/v4"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/pager"
)

type Pagination struct {
	Current     int64 `json:"current"`
	Total       int64 `json:"total"`
	Elements    int64 `json:"elements"`
	Remaining   int64 `json:"remaining"`
	HasNextPage bool  `json:"hasNextPage"`
}

func NewPagination[T any](pageSet *pager.PageSet[T]) Pagination {
	total := pager.CalculateTotal(pageSet.Elements, pageSet.Count)
	remaining := pager.CalculateRemaining(total, pageSet.Count, pageSet.Elements)

	return Pagination{
		Current:     pageSet.Page,
		Elements:    pageSet.Elements,
		Total:       total,
		Remaining:   remaining,
		HasNextPage: remaining > 0,
	}
}

type ErrorResponse struct {
	Error any `json:"error,omitempty"`
}

type DataResponse[T any] struct {
	Data       T           `json:"data"`
	Pagination *Pagination `json:"page,omitempty"`
}

func NewErrorCode(c echo.Context, err error, code int) error {
	return c.JSON(code, ErrorResponse{Error: err.Error()})
}

func NewError(c echo.Context, err error) error {
	switch customErr := err.(type) {
	case *apperror.Error:
		return NewErrorCode(
			c,
			customErr,
			customErr.Code(),
		)
	default:
		log.
			WithTrace(c.Request().Context()).
			Named("router").
			Error("something went wrong", zap.Error(err))

		// Mask the error
		return NewError(c, apperror.ErrSomethingWentWrong)
	}
}

func NewData[T any](c echo.Context, code int, data T) error {
	return c.JSON(code, DataResponse[T]{Data: data})
}

func NewPageData[T any](
	c echo.Context, code int, pageSet *pager.PageSet[T],
) error {
	pagination := NewPagination[T](pageSet)
	return c.JSON(code, DataResponse[T]{
		Data:       pageSet.Data,
		Pagination: &pagination,
	})
}
