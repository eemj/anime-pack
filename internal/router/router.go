package router

import (
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	customMiddleware "gitlab.com/eemj/anime-pack/internal/router/middleware"
	"gitlab.com/eemj/anime-pack/internal/router/response"
	websocket "gitlab.com/eemj/anime-pack/internal/websocket/server"

	_ "gitlab.com/eemj/anime-pack/docs"
)

// Router is our HTTP router which contains echo.Echo, database.DB
// and WebSocket as injected dependencies.
type Router struct {
	*echo.Echo

	websocket *websocket.WebSocket
}

// New creates a new router instance intialized with pre-defined
// routes for web use.
func New(
	websocket *websocket.WebSocket,
	middlewareConfig configmodels.HTTPMiddleware,
) (*Router, error) {
	router := &Router{Echo: echo.New(), websocket: websocket}
	router.HideBanner = true
	router.HidePort = true

	loggerMiddleware, err := customMiddleware.NewLoggerMiddleware(customMiddleware.LoggerMiddlewareConfig{
		FilterExclude: middlewareConfig.Logger.FilterExclude,
	})
	if err != nil {
		return nil, err
	}

	router.Use(
		loggerMiddleware.Middleware(),
		customMiddleware.Recover,
		middleware.RemoveTrailingSlash(),
	)

	// WebSocket
	router.GET("/ws", echo.WrapHandler(http.HandlerFunc(
		router.websocket.Request,
	)))

	return router, nil
}

func (r *Router) HTTPErrorHandler(err error, c echo.Context) {
	code := http.StatusInternalServerError
	message := "Internal Server Error"

	if httpError, ok := err.(*echo.HTTPError); ok {
		code = httpError.Code
		message = httpError.Message.(string)
	}

	if code == http.StatusInternalServerError {
		response.NewError(c, apperror.ErrSomethingWentWrong)
	} else {
		response.NewErrorCode(
			c,
			errors.New(message),
			code,
		)
	}
}

func (r *Router) Close() { r.websocket.Close() }
