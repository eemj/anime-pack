package router

import (
	"time"

	"github.com/labstack/echo/v4/middleware"
)

func (r *Router) EnableCORS(
	allowCredentials bool,
	allowOrigins []string,
	allowMethods []string,
	maxAge time.Duration,
) {
	r.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowCredentials: allowCredentials,
		AllowMethods:     allowMethods,
		AllowOrigins:     allowOrigins,
		MaxAge:           int(maxAge.Seconds()),
	}))
}
