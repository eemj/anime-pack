package router

import (
	"net/http"
	"net/http/pprof"

	"github.com/labstack/echo/v4"
)

func (r *Router) GetPProf(c echo.Context) error {
	pprof.Index(c.Response().Writer, c.Request())
	c.Response().WriteHeader(http.StatusOK)
	return nil
}
