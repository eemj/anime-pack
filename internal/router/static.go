package router

import (
	"fmt"
	"io/fs"
	"net/http"
	"net/url"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/config/configtypes"
	customMiddleware "gitlab.com/eemj/anime-pack/internal/router/middleware"
	"gitlab.com/eemj/anime-pack/web"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

const (
	webRootPath     = "dist"
	imagesPath      = "/images"
	reverseProxyURL = "http://127.0.0.1:3000"
)

func (r *Router) ServeImages(dir string) {
	r.Static(path.PrefixImageBase, dir)
}

func (r *Router) UseOpenTelemetry() {
	r.Use(customMiddleware.OtelMiddleware())
}

func (r *Router) ServeFrontend(env configtypes.Environment) error {
	switch env {
	case configtypes.EnvironmentPRODUCTION:
		distRoot, err := fs.Sub(web.Dist, webRootPath)
		if err != nil {
			return err
		}

		files := make([]string, 0)
		walkDirFunc := fs.WalkDirFunc(func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			if !d.IsDir() {
				files = append(files, path)
			}
			return nil
		})

		err = fs.WalkDir(distRoot, ".", walkDirFunc)
		if err != nil {
			return err
		}

		log.Named("router").
			Sugar().
			Infow("static", zap.Strings("files", files))

		r.GET("/*", echo.WrapHandler(http.FileServer(http.FS(distRoot))))
	case configtypes.EnvironmentDEVELOPMENT:
		reverseProxy, _ := url.Parse(reverseProxyURL)

		log.Named("router").
			Sugar().
			Infow(
				"proxy",
				zap.Stringer("url", reverseProxy),
			)

		r.Group("/*", middleware.Proxy(
			customMiddleware.NewStaticBalancer(reverseProxy),
		))
	default:
		return fmt.Errorf("unknown environment value `%s`", env)
	}

	return nil
}
