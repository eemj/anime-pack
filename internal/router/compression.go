package router

import (
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func (r *Router) EnableGZIPCompression(level int) {
	r.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: level,
		Skipper: func(c echo.Context) bool {
			return strings.HasPrefix(c.Request().URL.Path, "/swagger")
		},
	}))
}
