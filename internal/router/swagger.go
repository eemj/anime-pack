package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
)

func (r *Router) EnableSwagger() {
	r.GET("/swagger", func(c echo.Context) error {
		return c.Redirect(
			http.StatusPermanentRedirect,
			"/swagger/index.html",
		)
	})

	r.GET("/swagger/*", echoSwagger.WrapHandler)
}
