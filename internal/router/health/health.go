package health

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/eemj/anime-pack/internal/router/response"
)

func Register(e *echo.Echo, serviceChecks ...*ServiceCheck) (err error) {
	e.GET("/healthz", func(c echo.Context) error {
		code, checks := check(
			c.Request().Context(),
			serviceChecks,
		)

		return response.NewData(
			c,
			code,
			checks,
		)
	})

	return
}
