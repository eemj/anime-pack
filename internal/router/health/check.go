package health

import (
	"context"
	"net/http"
)

type aliver struct {
	fn func(ctx context.Context) bool
}

func (a *aliver) Alive(ctx context.Context) bool {
	return a.fn(ctx)
}

func AliverFunc(fn func(ctx context.Context) bool) Aliver {
	return &aliver{fn: fn}
}

// Aliver defines an interface which abides to our health checks requirement.
type Aliver interface {
	Alive(ctx context.Context) (alive bool)
}

type Check struct {
	Name    string `json:"name"`
	Healthy bool   `json:"healthy"`
}

type ServiceCheck struct {
	Name   string
	Aliver Aliver
}

func NewCheck(name string, aliver Aliver) *ServiceCheck {
	return &ServiceCheck{
		Name:   name,
		Aliver: aliver,
	}
}

func check(
	ctx context.Context,
	serviceChecks []*ServiceCheck,
) (statusCode int, checks []*Check) {
	checks = make([]*Check, 0)
	statusCode = http.StatusOK

	for _, serviceCheck := range serviceChecks {
		check := &Check{
			Name:    serviceCheck.Name,
			Healthy: serviceCheck.Aliver.Alive(ctx),
		}

		if !check.Healthy {
			statusCode = http.StatusServiceUnavailable
		}

		checks = append(checks, check)
	}

	return
}
