package anidb

import (
	"compress/gzip"
	"context"
	"encoding/xml"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.com/eemj/anime-pack/internal/apis/customhttp"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/ratelimit"
)

type AniDB interface {
	GetTitles(ctx context.Context) (anime Animes, err error)
}

type anidb struct {
	client customhttp.RateLimitClient
}

func New() AniDB {
	return &anidb{
		client: customhttp.NewRateLimitClient(
			http.DefaultTransport,
			1,
			ratelimit.Per((2 * time.Second)),
		),
	}
}

func (a *anidb) GetTitles(ctx context.Context) (animes Animes, err error) {
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		`http://anidb.net/api/anime-titles.xml.gz`,
		nil,
	)
	if err != nil {
		return
	}

	utils.SetUserAgent(req)

	res, err := a.client.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		err = apperror.ErrExpectedStatusCodeOkButGot(res.StatusCode)
		return
	}

	greader, err := gzip.NewReader(res.Body)
	if err != nil {
		return
	}

	defer greader.Close()

	animeTitles := new(AnimeTitles)

	if err = xml.NewDecoder(greader).Decode(animeTitles); err != nil {
		return
	}

	animes = make(Animes, 0)

	for _, entry := range animeTitles.Anime {
		id, err := strconv.ParseUint(entry.Aid, 10, 64)
		if err != nil {
			return nil, err
		}

		anime := &Anime{ID: uint(id)}

		for _, title := range entry.Title {
			anime.Titles = append(anime.Titles, &Title{
				Name:     strings.TrimSpace(title.Text),
				Language: title.Lang,
				Type:     title.Type,
			})
		}

		sort.Sort(anime.Titles)

		animes = append(animes, anime)
	}

	sort.Sort(animes)

	return
}
