package anidb

import (
	"encoding/xml"
)

const (
	type_Main = "main"
)

type AnimeTitles struct {
	XMLName xml.Name `xml:"animetitles"`
	Text    string   `xml:",chardata"`
	Anime   []struct {
		Text  string `xml:",chardata"`
		Aid   string `xml:"aid,attr"`
		Title []struct {
			Text string `xml:",chardata"`
			Lang string `xml:"lang,attr"`
			Type string `xml:"type,attr"`
		} `xml:"title"`
	} `xml:"anime"`
}

type Title struct {
	Language string `json:"language"`
	Name     string `json:"name"`
	Type     string `json:"type"`
}

type Titles []*Title

func (t Titles) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t Titles) Len() int {
	return len(t)
}

func (t Titles) Less(i, j int) bool {
	return t[i].Type == type_Main
}

type Animes []*Anime

func (a Animes) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a Animes) Less(i, j int) bool {
	return a[i].Titles[0].Name < a[j].Titles[0].Name
}

func (a Animes) Len() int {
	return len(a)
}

type Anime struct {
	ID     uint   `json:"id"`
	Titles Titles `json:"titles"`
}
