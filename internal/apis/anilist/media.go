package anilist

import (
	"context"
	"encoding/json"
	"net/http"
)

func (a *aniList) getMedia(ctx context.Context, gql GraphQLRequest) (media *Media, err error) {
	reader, err := gql.Reader()
	if err != nil {
		return
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		uri,
		reader,
	)
	if err != nil {
		return
	}

	res, err := a.rclient.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	mediaResponse := new(MediaResponse)

	if err = json.NewDecoder(res.Body).Decode(mediaResponse); err != nil {
		return
	}

	if len(mediaResponse.Errors) > 0 {
		err = mediaResponse.Errors[0]
		return
	}

	if mediaResponse.Data != nil {
		media = &mediaResponse.Data.Media
	}

	return
}

func (a *aniList) GetMediaByID(ctx context.Context, id uint) (media *Media, err error) {
	return a.getMedia(ctx, QueryMediaByID([]uint{id}))
}

func (a *aniList) GetMediaByIDMAL(ctx context.Context, idMAL uint) (media *Media, err error) {
	return a.getMedia(ctx, QueryMediaByMALID([]uint{idMAL}))
}
