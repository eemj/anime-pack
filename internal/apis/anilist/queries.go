package anilist

import (
	"bytes"
	"encoding/json"
	"io"
	"strconv"
	"strings"
)

type ErrorResponse struct {
	Message   string `json:"message"`
	Status    int    `json:"status"`
	Locations []struct {
		Line   int
		Column int
	} `json:"locations"`
}

func (e ErrorResponse) Error() string {
	return e.Message
}

type MediaPageListResponse struct {
	Data *struct {
		Page struct {
			PageInfo struct {
				HasNextPage bool `json:"hasNextPage"`
			} `json:"pageInfo"`
			Media []*Media `json:"Media"`
		} `json:"Page"`
	} `json:"data"`
	Errors []ErrorResponse `json:"errors"`
}

type DynamicMediaListResponse struct {
	Data *map[string]struct {
		Media    []*Media `json:"Media"`
		PageInfo struct {
			HasNextPage bool `json:"hasNextPage"`
		} `json:"pageInfo"`
	} `json:"data"`
	Errors []ErrorResponse `json:"errors"`
}

type MediaListResponse struct {
	Data *struct {
		Page struct {
			Media    []*Media `json:"Media"`
			PageInfo struct {
				HasNextPage bool `json:"hasNextPage"`
			} `json:"pageInfo"`
		} `json:"Page"`
	} `json:"data"`
	Errors []ErrorResponse `json:"errors"`
}

type MediaResponse struct {
	Data *struct {
		Media Media `json:"Media"`
	} `json:"data"`
	Errors []ErrorResponse `json:"errors"`
}

type GraphQLRequest struct {
	Query     string         `json:"query"`
	Variables map[string]any `json:"variables"`
}

const (
	fragment_mediaInfo string = `fragment MediaInfo on Media {
	countryOfOrigin
	format
	title {
		romaji
		english
		native
		userPreferred
	}
	id
	idMal
	description(asHtml: false)
	genres
	averageScore
	studios {
		nodes {
			id
			name
		}
	}
	status
	episodes
	startDate {
		year
		month
		day
	}
	endDate {
		year
		month
		day
	}
	type
	coverImage {
		extraLarge
		large
		medium
		color
	}
	bannerImage
	seasonYear
	season
	nextAiringEpisode {
		airingAt
	}
	tags {
		name
		rank
		description
		isGeneralSpoiler
		isMediaSpoiler
	}
	synonyms
	relations {
		edges {
			relationType(version: 2)
			node {
				id
				format
			}
		}
	}
	recommendations(sort: RATING_DESC) {
		nodes {
			rating
			mediaRecommendation {
			id
			}
		}
	}
}`
)

func concatQueryFragment(query string) string {
	return fragment_mediaInfo + "\n" + query
}

func (r GraphQLRequest) Reader() (reader io.Reader, err error) {
	payload, err := json.Marshal(&r)
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(payload), nil
}

func QueryMediaByMALID(ids []uint) GraphQLRequest {
	return GraphQLRequest{
		Query: concatQueryFragment(`query ($ids: [Int]) {
	Media(idMal_in: $ids, type: ANIME) {
		...MediaInfo
	}
}`), Variables: map[string]any{"ids": ids},
	}
}

func QueryMediaByID(ids []uint) GraphQLRequest {
	return GraphQLRequest{
		Query: concatQueryFragment(`query ($ids: [Int]) {
	Media(id_in: $ids, type: ANIME) {
		...MediaInfo
	}
}`), Variables: map[string]any{"ids": ids},
	}
}

func QueryMediaSearch(page, perPage int, titles []string) GraphQLRequest {
	query := new(strings.Builder)

	query.WriteString(`query ($page: Int, $perPage: Int) {`)

	for index, title := range titles {
		query.WriteString(`
	n`)
		query.WriteString(strconv.Itoa(index))
		query.WriteString(`: Page(page: $page, perPage: $perPage) {
			media(search: "`)
		query.WriteString(title)
		query.WriteString(`", type: ANIME, sort: SEARCH_MATCH) {
			...MediaInfo
		}
		pageInfo {
			hasNextPage
		}
	}`)
	}

	query.WriteString(`
}`)

	return GraphQLRequest{
		Query: concatQueryFragment(query.String()),
		Variables: map[string]any{
			"page":    page,
			"perPage": perPage,
		},
	}
}

func QueryMediaAiring(page, perPage int) GraphQLRequest {
	return GraphQLRequest{
		Query: concatQueryFragment(`query($page: Int, $perPage: Int) {
	Page(page: $page, perPage: $perPage) {
		pageInfo {
			hasNextPage
		}
		media(type: ANIME, status: RELEASING, sort: [POPULARITY_DESC, SCORE_DESC]) {
			...MediaInfo
		}
	}
}`), Variables: map[string]any{
			"page":    page,
			"perPage": perPage,
		},
	}
}
