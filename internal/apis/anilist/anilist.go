package anilist

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/eemj/anime-pack/internal/apis/customhttp"
	"go.uber.org/ratelimit"
)

const uri = "https://graphql.anilist.co/"

type AniList interface {
	GetMediaByID(ctx context.Context, id uint) (media *Media, err error)
	GetMediaByIDMAL(ctx context.Context, id uint) (media *Media, err error)
	GetAiring(ctx context.Context) (media []*Media, err error)
	Search(ctx context.Context, titles []string) (media []*Media, err error)
}

type aniList struct {
	rclient customhttp.RateLimitClient
}

func New() AniList {
	return &aniList{
		rclient: customhttp.NewRateLimitClient(
			http.DefaultTransport,
			90,
			ratelimit.Per((1 * time.Minute)),
		),
	}
}
