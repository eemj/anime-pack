package anilist

import (
	"fmt"
	"time"

	"gitlab.com/eemj/anime-pack/internal/apis/models"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const (
	timeFormat = "2006-1-2"
)

type YearMonthDay struct {
	Year  *int `json:"year"`
	Month *int `json:"month"`
	Day   *int `json:"day"`
}

func (ymd YearMonthDay) String() string {
	if ymd.Year != nil && ymd.Month != nil && ymd.Day != nil {
		return fmt.Sprintf(
			"%d-%d-%d",
			*ymd.Year,
			*ymd.Month,
			*ymd.Day,
		)
	}

	return ""
}

type Media struct {
	ID              int64  `json:"id"`
	IDMAL           *int64 `json:"idMal,omitempty"`
	CountryOfOrigin string `json:"countryOfOrigin"`
	Format          string `json:"format"`
	Title           struct {
		Romaji        *string `json:"romaji,omitempty"`
		English       *string `json:"english,omitempty"`
		Native        *string `json:"native,omitempty"`
		UserPreferred string  `json:"userPreferred"`
	} `json:"title"`
	Description string   `json:"description"`
	Genres      []string `json:"genres"`
	Studios     struct {
		Nodes []struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"nodes"`
	} `json:"studios"`
	Status     string       `json:"status"`
	Episodes   *int         `json:"episodes"`
	StartDate  YearMonthDay `json:"startDate"`
	EndDate    YearMonthDay `json:"endDate"`
	CoverImage struct {
		ExtraLarge string `json:"extraLarge"`
		Large      string `json:"large"`
		Medium     string `json:"medium"`
		Color      string `json:"color"`
	} `json:"coverImage"`
	BannerImage       string `json:"bannerImage"`
	AverageScore      uint   `json:"averageScore"`
	NextAiringEpisode *struct {
		AiringAt int64 `json:"airingAt"`
	} `json:"nextAiringEpisode"`
	SeasonYear int    `json:"seasonYear"`
	Season     string `json:"season"`
	Tags       []struct {
		Name             string `json:"name"`
		Description      string `json:"description"`
		Rank             int    `json:"rank"`
		IsGeneralSpoiler bool   `json:"isGeneralSpoiler"`
		IsMediaSpoiler   bool   `json:"isMediaSpoiler"`
	} `json:"tags"`
	Synonyms  []string `json:"synonyms"`
	Relations struct {
		Edges []struct {
			RelationType string `json:"relationType"`
			Node         struct {
				ID     uint   `json:"id"`
				Format string `json:"format"`
			} `json:"node"`
		} `json:"edges"`
	} `json:"relations"`
	Recommendations struct {
		Nodes []struct {
			Rating              int `json:"rating"`
			MediaRecommendation struct {
				ID int `json:"id"`
			} `json:"mediaRecommendation"`
		} `json:"nodes"`
	} `json:"recommendations"`
}

func (m *Media) Model() *models.Media {
	media := &models.Media{
		ID:              m.ID,
		IDMAL:           m.IDMAL,
		CountryOfOrigin: m.CountryOfOrigin,
		Format:          m.Format,
		Title:           m.Title.UserPreferred,
		TitleEnglish:    m.Title.English,
		TitleJapanese:   m.Title.Native,
		TitleRomaji:     m.Title.Romaji,
		Description:     m.Description,
		Genres:          m.Genres,
		Score:           m.AverageScore,
		Status:          m.Status,
		Synonyms:        m.Synonyms,
		Relations:       make([]models.Relation, 0),
		Tags:            make([]string, 0),
	}

	if !utils.IsEmpty(m.BannerImage) {
		media.Banner = &m.BannerImage
	}

	if !utils.IsEmpty(m.Season) {
		media.Season = &m.Season
	}

	if !utils.IsEmpty(m.SeasonYear) {
		media.SeasonYear = &m.SeasonYear
	}

	if m.NextAiringEpisode != nil {
		if nextAiringDate := time.Unix(
			m.NextAiringEpisode.AiringAt, 0,
		); !nextAiringDate.IsZero() {
			media.NextAiringDate = new(time.Time)
			*media.NextAiringDate = nextAiringDate.UTC()
		}
	}

	for _, tag := range m.Tags {
		media.Tags = append(media.Tags, tag.Name)
	}

	for _, relation := range m.Relations.Edges {
		media.Relations = append(media.Relations, models.Relation{
			Type: relation.RelationType,
			ID:   relation.Node.ID,
		})
	}

	switch { // Cover image preference: 'Extra Large' > 'Large' > 'Medium'
	case !utils.IsEmpty(m.CoverImage.ExtraLarge):
		media.Poster = m.CoverImage.ExtraLarge
	case !utils.IsEmpty(m.CoverImage.Large):
		media.Poster = m.CoverImage.Large
	case !utils.IsEmpty(m.CoverImage.Medium):
		media.Poster = m.CoverImage.Medium
	}

	if startDate := m.StartDate.String(); startDate != "" {
		if _startDate, err := time.ParseInLocation(
			timeFormat,
			startDate,
			time.UTC,
		); err == nil {
			media.StartDate = &_startDate
		}
	}

	if endDate := m.EndDate.String(); endDate != "" {
		if _endDate, err := time.ParseInLocation(
			timeFormat,
			endDate,
			time.UTC,
		); err == nil {
			media.EndDate = &_endDate
		}
	}

	for _, studio := range m.Studios.Nodes {
		media.Studios = append(media.Studios, studio.Name)
	}

	if media.StartDate != nil && !media.StartDate.IsZero() {
		if media.Season == nil {
			media.Season = new(string)
			*media.Season = string(models.DetermineSeason(*media.StartDate))
		}

		if media.SeasonYear == nil {
			startDateYear := media.StartDate.Year()
			media.SeasonYear = &startDateYear
		}
	}

	return media
}
