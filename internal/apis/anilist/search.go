package anilist

import (
	"context"
	"encoding/json"
	"net/http"
)

func (a *aniList) Search(ctx context.Context, titles []string) (media []*Media, err error) {
	reader, err := QueryMediaSearch(1, 50, titles).Reader()
	if err != nil {
		return
	}

	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodPost,
		uri,
		reader,
	)
	if err != nil {
		return
	}

	res, err := a.rclient.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	dynamicMediaListResponse := new(DynamicMediaListResponse)

	if err = json.NewDecoder(res.Body).Decode(dynamicMediaListResponse); err != nil {
		return
	}

	if len(dynamicMediaListResponse.Errors) > 0 {
		err = dynamicMediaListResponse.Errors[0]
		return
	}

	if dynamicMediaListResponse.Data != nil {
		distinct := make(map[int64]*Media)
		for _, items := range *dynamicMediaListResponse.Data {
			for _, item := range items.Media {
				if _, exists := distinct[item.ID]; !exists {
					distinct[item.ID] = item
					media = append(media, item)
				}
			}
		}
	}

	return
}
