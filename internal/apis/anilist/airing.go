package anilist

import (
	"context"
	"encoding/json"
	"net/http"
)

func (a *aniList) GetAiring(ctx context.Context) (media []*Media, err error) {
	hasNextPage := true

	for page := 1; hasNextPage; page++ {
		reader, err := QueryMediaAiring(page, 50).Reader()
		if err != nil {
			return media, err
		}

		req, err := http.NewRequestWithContext(
			ctx,
			http.MethodPost,
			uri,
			reader,
		)
		if err != nil {
			return nil, err
		}

		res, err := a.rclient.Do(req)
		if err != nil {
			return nil, err
		}

		defer res.Body.Close()

		mediaPageListResponse := new(MediaPageListResponse)

		if err = json.NewDecoder(res.Body).Decode(mediaPageListResponse); err != nil {
			return nil, err
		}

		if len(mediaPageListResponse.Errors) > 0 {
			return nil, mediaPageListResponse.Errors[0]
		}

		if mediaPageListResponse.Data != nil {
			hasNextPage = mediaPageListResponse.Data.Page.PageInfo.HasNextPage

			media = append(
				media,
				mediaPageListResponse.Data.Page.Media...,
			)
		}
	}

	return
}
