package models

import "time"

type Relation struct {
	Type string `json:"type"`
	ID   uint   `json:"id"`
}

type Media struct {
	ID              int64      `json:"id"`
	IDMAL           *int64     `json:"idMal"`
	CountryOfOrigin string     `json:"countryOfOrigin"`
	Description     string     `json:"description"`
	StartDate       *time.Time `json:"startDate,omitempty"`
	EndDate         *time.Time `json:"endDate,omitempty"`
	NextAiringDate  *time.Time `json:"nextAiringDate,omitempty"`
	Genres          []string   `json:"genres,omitempty"`
	Studios         []string   `json:"studios,omitempty"`
	Title           string     `json:"title"`
	TitleEnglish    *string    `json:"titleEnglish,omitempty"`
	TitleJapanese   *string    `json:"titleJapanese,omitempty"`
	TitleRomaji     *string    `json:"titleRomaji,omitempty"`
	Duration        uint       `json:"duration"`
	Score           uint       `json:"score"`
	Poster          string     `json:"poster"`
	Banner          *string    `json:"banner,omitempty"`
	Format          string     `json:"format"`
	Status          string     `json:"status"`
	Colour          *string    `json:"colour"`
	Season          *string    `json:"season,omitempty"`
	SeasonYear      *int       `json:"seasonYear,omitempty"`
	Tags            []string   `json:"tags"`
	Synonyms        []string   `json:"synonyms"`
	Relations       []Relation `json:"relations"`
}
