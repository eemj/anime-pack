package models

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

func DetermineSeason(date time.Time) enums.AnimeSeason {
	month := date.Month()

	switch {
	case (month == time.December || (month >= time.January && month <= time.February)):
		return enums.AnimeSeasonWINTER
	case month >= time.March && month <= time.May:
		return enums.AnimeSeasonSPRING
	case month >= time.June && month <= time.August:
		return enums.AnimeSeasonSUMMER
	case month >= time.September && month <= time.November:
		return enums.AnimeSeasonFALL
	}

	return enums.AnimeSeasonUNDEFINED
}
