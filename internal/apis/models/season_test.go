package models

import (
	"strconv"
	"testing"
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type TestDetermineSeasonScenario struct {
	Date   time.Time
	Season enums.AnimeSeason
}

func createDateForMonth(month time.Month) time.Time {
	date, _ := time.Parse("2006-1-1", ("2000-1-" + strconv.Itoa(int(month))))
	return date
}

func TestDetermineSeason(t *testing.T) {
	scenarios := []TestDetermineSeasonScenario{
		{Date: createDateForMonth(time.January), Season: enums.AnimeSeasonWINTER},
		{Date: createDateForMonth(time.February), Season: enums.AnimeSeasonWINTER},
		{Date: createDateForMonth(time.March), Season: enums.AnimeSeasonSPRING},
		{Date: createDateForMonth(time.April), Season: enums.AnimeSeasonSPRING},
		{Date: createDateForMonth(time.May), Season: enums.AnimeSeasonSPRING},
		{Date: createDateForMonth(time.June), Season: enums.AnimeSeasonSUMMER},
		{Date: createDateForMonth(time.July), Season: enums.AnimeSeasonSUMMER},
		{Date: createDateForMonth(time.August), Season: enums.AnimeSeasonSUMMER},
		{Date: createDateForMonth(time.September), Season: enums.AnimeSeasonFALL},
		{Date: createDateForMonth(time.October), Season: enums.AnimeSeasonFALL},
		{Date: createDateForMonth(time.November), Season: enums.AnimeSeasonFALL},
		{Date: createDateForMonth(time.December), Season: enums.AnimeSeasonWINTER},
	}

	for _, scenario := range scenarios {
		if outcome := DetermineSeason(scenario.Date); outcome != scenario.Season {
			t.Fatalf(
				`expected '%v' to be in season '%s' but got '%s'`,
				scenario.Date,
				scenario.Season,
				outcome,
			)
		}
	}
}
