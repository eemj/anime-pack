package subsplease

import "time"

type scheduleResponse struct {
	Timezone string                     `json:"tz"`
	Schedule map[string][]scheduleEntry `json:"schedule"`
}

type scheduleEntry struct {
	Title    string `json:"title"`
	Page     string `json:"page"`
	ImageUrl string `json:"image_url"`
	Time     string `json:"time"`
}

type Releases []*Release

func (r Releases) Len() int           { return len(r) }
func (r Releases) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }
func (r Releases) Less(i, j int) bool { return r[i].Time.Before(r[j].Time) }

type Release struct {
	Title string    `json:"title"`
	Time  time.Time `json:"time"`
}

func (r *Release) String() string { return (r.Title + " @ " + r.Time.Format(time.ANSIC)) }

type Schedule struct {
	Weekday  time.Weekday `json:"weekday"`
	Releases Releases     `json:"releases"`
}

type Schedules []*Schedule

func (s Schedules) Len() int      { return len(s) }
func (s Schedules) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s Schedules) Less(i, j int) bool {
	var (
		iTime time.Time
		jTime time.Time
	)

	if len(s[i].Releases) > 0 {
		iTime = s[i].Releases[0].Time
	}

	if len(s[j].Releases) > 0 {
		jTime = s[j].Releases[0].Time
	}

	return iTime.Before(jTime)
}
