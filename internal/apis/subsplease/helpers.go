package subsplease

import (
	"strings"
	"time"
)

var _lowerWeekdays = map[string]time.Weekday{
	strings.ToLower(time.Monday.String()):    time.Monday,
	strings.ToLower(time.Tuesday.String()):   time.Tuesday,
	strings.ToLower(time.Wednesday.String()): time.Wednesday,
	strings.ToLower(time.Thursday.String()):  time.Thursday,
	strings.ToLower(time.Friday.String()):    time.Friday,
	strings.ToLower(time.Saturday.String()):  time.Saturday,
	strings.ToLower(time.Sunday.String()):    time.Sunday,
}

func ParseWeekday(name string) (time.Weekday, bool) {
	weekday, ok := _lowerWeekdays[strings.ToLower(name)]
	return weekday, ok
}
