package subsplease

import (
	"context"
	"encoding/json"
	"net/http"
	"sort"
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

const (
	bareTimeLayout = "15:04"
)

func GetSchedule(ctx context.Context) (schedules Schedules, err error) {
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		"https://subsplease.org/api/?f=schedule&tz=UTC",
		nil,
	)
	if err != nil {
		return
	}

	utils.SetUserAgent(req)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		err = apperror.ErrExpectedStatusCodeOkButGot(res.StatusCode)
		return
	}

	response := new(scheduleResponse)

	if err = json.NewDecoder(res.Body).Decode(&response); err != nil {
		return
	}

	for weekdayName, entries := range response.Schedule {
		weekday, ok := ParseWeekday(weekdayName)

		if !ok {
			continue
		}

		schedule := &Schedule{Weekday: weekday, Releases: make(Releases, 0)}

		for _, entry := range entries {
			scheduleEntry, err := time.ParseInLocation(
				bareTimeLayout, entry.Time, time.UTC)
			if err != nil {
				return nil, err
			}

			schedule.Releases = append(schedule.Releases, &Release{
				Title: entry.Title,
				Time:  scheduleEntry,
			})
		}

		sort.Stable(schedule.Releases)

		schedules = append(schedules, schedule)
	}

	sort.Stable(schedules)

	return
}
