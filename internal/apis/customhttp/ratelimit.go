package customhttp

import (
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"sync"
	"time"

	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/ratelimit"
)

type caller struct {
	proxy  *url.URL
	client *http.Client
}

type RateLimitClient interface {
	Do(req *http.Request) (res *http.Response, err error)
}

type rateLimitClient struct {
	callers      []caller
	roundTripper http.RoundTripper
}

type rateLimitInterceptor struct {
	roundTripper http.RoundTripper
	limiter      ratelimit.Limiter
	once         sync.Once
	rate         int
}

func (r *rateLimitInterceptor) RoundTrip(req *http.Request) (res *http.Response, err error) {
	utils.SetUserAgent(req)
	req.Header.Set("Content-Type", "application/json")

	r.limiter.Take()

	res, err = r.roundTripper.RoundTrip(req)

	if err == nil {
		r.once.Do(func() {
			remaining, err := strconv.Atoi(
				res.Header.Get("x-ratelimit-remaining"),
			)

			if err == nil {
				// Decrement the amount used up already
				for offset := 0; offset < (r.rate - remaining); offset++ {
					r.limiter.Take()
				}
			}
		})
	}

	return res, err
}

func (a *rateLimitClient) createClient(
	proxy *url.URL,
	rate int,
	options ...ratelimit.Option,
) *http.Client {
	transport := a.roundTripper.(*http.Transport).Clone()

	if proxy != nil {
		transport.Proxy = http.ProxyURL(proxy)
	}

	return &http.Client{
		Timeout: 10 * time.Second,
		Transport: &rateLimitInterceptor{
			roundTripper: transport,
			rate:         rate,
			limiter:      ratelimit.New(rate, options...),
		},
	}
}

// TODO: Implement round robin proxy selection
func (a *rateLimitClient) Do(req *http.Request) (res *http.Response, err error) {
	if len(a.callers) == 0 {
		return nil, errors.New("no callers")
	}

	return a.callers[0].client.Do(req)
}

func NewRateLimitClient(
	roundTripper http.RoundTripper,
	rate int,
	options ...ratelimit.Option,
) RateLimitClient {
	rclient := &rateLimitClient{
		callers:      make([]caller, 0),
		roundTripper: roundTripper,
	}

	rclient.callers = append(rclient.callers, caller{
		proxy:  nil,
		client: rclient.createClient(nil, rate, options...),
	})

	return rclient
}
