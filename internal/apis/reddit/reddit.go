package reddit

import (
	"context"
	"crypto/tls"
	"net/http"
	"time"

	"gitlab.com/eemj/anime-pack/internal/apis/customhttp"
	"go.uber.org/ratelimit"
)

type Reddit interface {
	GetPosts(ctx context.Context) (posts []*Post, err error)
}

type reddit struct {
	client customhttp.RateLimitClient
}

func New() Reddit {
	transport := http.DefaultTransport.(*http.Transport)
	transport.TLSNextProto = map[string]func(authority string, c *tls.Conn) http.RoundTripper{}

	return &reddit{
		client: customhttp.NewRateLimitClient(
			transport,
			30,
			ratelimit.Per((1 * time.Minute)),
		),
	}
}
