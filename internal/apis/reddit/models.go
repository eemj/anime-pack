package reddit

import "time"

type Listing struct {
	Kind string `json:"kind"`
	Data struct {
		After    string `json:"after"`
		Children []struct {
			Kind string `json:"t3"`
			Data struct {
				Selftext         string  `json:"selftext"`
				LinkFlairText    string  `json:"link_flair_text"`
				Score            int     `json:"score"`
				CreatedUtc       float64 `json:"created_utc"`
				Title            string  `json:"title"`
				Hidden           bool    `json:"hidden"`
				Ups              int     `json:"ups"`
				Downs            int     `json:"downs"`
				Name             string  `json:"name"`
				Over18           bool    `json:"over_18"`
				ID               string  `json:"id"`
				Author           string  `json:"author"`
				NumberOfComments int     `json:"comments"`
				Permalink        string  `json:"permalink"`
			} `json:"data"`
		} `json:"children"`
		Before *string `json:"before"`
	} `json:"data"`
}

type Post struct {
	ID        string    `json:"id"`
	Ups       int       `json:"ups"`
	Downs     int       `json:"downs"`
	Title     string    `json:"title"`
	Comments  int       `json:"comments"`
	CreatedAt time.Time `json:"createdAt"`
	Score     int       `json:"score"`
}
