package reddit

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

func (r *reddit) GetPosts(ctx context.Context) (posts []*Post, err error) {
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		"https://www.reddit.com/r/anime.json",
		nil,
	)
	if err != nil {
		return
	}

	utils.SetUserAgent(req)
	req.Header.Set("Host", "reddit.com")

	res, err := r.client.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()

	listings := new(Listing)

	if err = json.NewDecoder(res.Body).Decode(listings); err != nil {
		return
	}

	for _, listing := range listings.Data.Children {
		if listing.Data.LinkFlairText == "Episode" &&
			listing.Data.Author == "AutoLovepon" {
			posts = append(posts, &Post{
				ID:        listing.Data.ID,
				Ups:       listing.Data.Ups,
				Downs:     listing.Data.Downs,
				Score:     listing.Data.Score,
				Title:     listing.Data.Title,
				Comments:  listing.Data.NumberOfComments,
				CreatedAt: time.Unix(int64(listing.Data.CreatedUtc), 0),
			})
		}
	}

	return
}
