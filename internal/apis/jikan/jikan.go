package jikan

type Details struct {
	MalID  uint   `json:"mal_id"`
	Url    string `json:"url"`
	Images struct {
		Jpg struct {
			ImageUrl      string `json:"image_url"`
			SmallImageUrl string `json:"small_image_url"`
			LargeImageUrl string `json:"large_image_url"`
		} `json:"jpg"`
		Webp struct {
			ImageUrl      string `json:"image_url"`
			SmallImageUrl string `json:"small_image_url"`
			LargeImageUrl string `json:"large_image_url"`
		} `json:"webp"`
	}
	Trailer struct {
		YoutubeID string `json:"youtube_id"`
		Url       string `json:"url"`
		EmbedUrl  string `json:"embed_url"`
	}
	Title         string `json:"title"`
	TitleEnglish  string `json:"title_english"`
	TitleJapanese string `json:"title_japanese"`
	TitleSynonyms string
	Type          string
	Source        string
	Episodes      int
	Status        string
	Airing        bool
	Aired         struct {
		From string
		To   string
		Prop struct {
			From struct {
				Day   int
				Month int
				Year  int
			}
			To struct {
				Day   int
				Month int
				Year  int
			}
		}
	}
	Duration   string
	Rating     string
	Score      int
	ScoredBy   int
	Rank       int
	Popularity int
	Members    int
	Favourite  int
	Synopsis   string
	Background string
	Season     string
	Year       int
}
