package apis

import (
	"gitlab.com/eemj/anime-pack/internal/apis/anilist"
)

type APIS struct{ AniList anilist.AniList }

func New() *APIS {
	return &APIS{AniList: anilist.New()}
}
