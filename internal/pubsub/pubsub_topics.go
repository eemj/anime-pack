package pubsub

const (
	// IRC
	Topic_IRC_CONNECTION_STATE_UPDATE           = "ANIME_PACK/IRC.CONNECTION_STATE_UPDATE"
	Topic_IRC_NOTICE_PENDING_TRANSFER           = "ANIME_PACK/IRC.NOTICE_PENDING_TRANSFER"
	Topic_IRC_PRIVATE_ANNOUNCE_PACK             = "ANIME_PACK/IRC.PRIVMSG_ANNOUNCE_PACK"
	Topic_IRC_PRIVATE_MESSAGE_TRANSFER_IN_QUEUE = "ANIME_PACK/IRC.PRIVMSG_TRANSFER_IN_QUEUE"
	Topic_IRC_BOT_STATE_UPDATE                  = "ANIME_PACK/IRC.BOT_STATE_UPDATE"
	Topic_IRC_CTCP_DCC                          = "ANIME_PACK/IRC.CTCP_DCC"

	// Job
	Topic_JOB_STATE_UPDATE = "ANIME_PACK/JOB.STATE_UPDATE"
)
