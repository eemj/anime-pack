package pubsub

import (
	"context"
	"encoding/json"

	"github.com/ThreeDotsLabs/watermill/message"
	"go.opentelemetry.io/otel/trace"
)

const (
	PubSubTraceID = "trace-id"
	PubSubSpanID  = "span-id"
)

func PublishMessage[P any](
	ctx context.Context,
	publisher message.Publisher,
	topic string,
	payload P,
) error {
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	payloadMsg := message.NewMessage("", payloadBytes)
	payloadMsg.SetContext(ctx)

	spanContext := trace.SpanContextFromContext(ctx)
	if spanContext.IsValid() {
		payloadMsg.Metadata.Set(PubSubTraceID, spanContext.TraceID().String())
		payloadMsg.Metadata.Set(PubSubSpanID, spanContext.SpanID().String())
	}

	return publisher.Publish(topic, payloadMsg)
}
