package lifecycle

import (
	"gitlab.com/eemj/anime-pack/internal/activity"
	"gitlab.com/eemj/anime-pack/internal/anime"
	"gitlab.com/eemj/anime-pack/internal/bot"
	"gitlab.com/eemj/anime-pack/internal/episode"
	"gitlab.com/eemj/anime-pack/internal/genre"
	"gitlab.com/eemj/anime-pack/internal/image"
	"gitlab.com/eemj/anime-pack/internal/library"
	"gitlab.com/eemj/anime-pack/internal/packlist"
	"gitlab.com/eemj/anime-pack/internal/preferences"
	"gitlab.com/eemj/anime-pack/internal/quality"
	releasegroup "gitlab.com/eemj/anime-pack/internal/release_group"
	"gitlab.com/eemj/anime-pack/internal/runner"
	"gitlab.com/eemj/anime-pack/internal/simulcast"
	"gitlab.com/eemj/anime-pack/internal/studio"
	"gitlab.com/eemj/anime-pack/internal/title"
	"gitlab.com/eemj/anime-pack/internal/video"
	"gitlab.com/eemj/anime-pack/internal/xdcc"
)

type ServiceLifecycle interface {
	Anime() anime.AnimeService
	Activity() activity.ActivityService
	Title() title.TitleService
	Genre() genre.GenreService
	Studio() studio.StudioService
	Preferences() preferences.PreferencesService
	Image() image.ImageService
	ReleaseGroup() releasegroup.ReleaseGroupService
	Bot() bot.BotService
	Quality() quality.QualityService
	Xdcc() xdcc.XDCCService
	Library() library.LibraryService
	Runner() runner.RunnerService
	Packlist() packlist.PacklistService
	Video() video.VideoService
	Episode() episode.EpisodeService
	Simulcast() simulcast.SimulcastService
}

type serviceLifecycle struct {
	anime        anime.AnimeService
	activity     activity.ActivityService
	title        title.TitleService
	genre        genre.GenreService
	studio       studio.StudioService
	preferences  preferences.PreferencesService
	image        image.ImageService
	releaseGroup releasegroup.ReleaseGroupService
	bot          bot.BotService
	quality      quality.QualityService
	xdcc         xdcc.XDCCService
	library      library.LibraryService
	runner       runner.RunnerService
	packlist     packlist.PacklistService
	video        video.VideoService
	episode      episode.EpisodeService
	simulcast    simulcast.SimulcastService
}

func (s *serviceLifecycle) Anime() anime.AnimeService                      { return s.anime }
func (s *serviceLifecycle) Activity() activity.ActivityService             { return s.activity }
func (s *serviceLifecycle) Bot() bot.BotService                            { return s.bot }
func (s *serviceLifecycle) Genre() genre.GenreService                      { return s.genre }
func (s *serviceLifecycle) Image() image.ImageService                      { return s.image }
func (s *serviceLifecycle) Library() library.LibraryService                { return s.library }
func (s *serviceLifecycle) Packlist() packlist.PacklistService             { return s.packlist }
func (s *serviceLifecycle) Preferences() preferences.PreferencesService    { return s.preferences }
func (s *serviceLifecycle) Quality() quality.QualityService                { return s.quality }
func (s *serviceLifecycle) ReleaseGroup() releasegroup.ReleaseGroupService { return s.releaseGroup }
func (s *serviceLifecycle) Runner() runner.RunnerService                   { return s.runner }
func (s *serviceLifecycle) Studio() studio.StudioService                   { return s.studio }
func (s *serviceLifecycle) Title() title.TitleService                      { return s.title }
func (s *serviceLifecycle) Video() video.VideoService                      { return s.video }
func (s *serviceLifecycle) Xdcc() xdcc.XDCCService                         { return s.xdcc }
func (s *serviceLifecycle) Episode() episode.EpisodeService                { return s.episode }
func (s *serviceLifecycle) Simulcast() simulcast.SimulcastService          { return s.simulcast }

func NewServiceLifecycle(
	anime anime.AnimeService,
	activity activity.ActivityService,
	title title.TitleService,
	genre genre.GenreService,
	studio studio.StudioService,
	preferences preferences.PreferencesService,
	image image.ImageService,
	releaseGroup releasegroup.ReleaseGroupService,
	bot bot.BotService,
	quality quality.QualityService,
	xdcc xdcc.XDCCService,
	library library.LibraryService,
	runner runner.RunnerService,
	packlist packlist.PacklistService,
	video video.VideoService,
	episode episode.EpisodeService,
	simulcast simulcast.SimulcastService,
) ServiceLifecycle {
	return &serviceLifecycle{
		anime:        anime,
		activity:     activity,
		title:        title,
		genre:        genre,
		studio:       studio,
		preferences:  preferences,
		image:        image,
		releaseGroup: releaseGroup,
		bot:          bot,
		quality:      quality,
		xdcc:         xdcc,
		library:      library,
		runner:       runner,
		packlist:     packlist,
		video:        video,
		episode:      episode,
		simulcast:    simulcast,
	}
}
