package pager

import "testing"

type TestCalculateLimitOffsetScenario struct {
	Page           int64
	Elements       int64
	ExpectedOffset int64
	ExpectedLimit  int64
}

func TestCalculateLimitOffset(t *testing.T) {
	scenarios := []TestCalculateLimitOffsetScenario{
		{
			Page:           1,
			Elements:       10,
			ExpectedOffset: 0,
			ExpectedLimit:  10,
		},
		{
			Page:           0,
			Elements:       10,
			ExpectedOffset: 0,
			ExpectedLimit:  10,
		},
		{
			Page:           2, // 0 will be 0, 1 will be 0, 2 will be 10
			Elements:       10,
			ExpectedOffset: 10,
			ExpectedLimit:  10,
		},
	}

	for index, scenario := range scenarios {
		offset, limit := CalculateLimitOffset(scenario.Page, scenario.Elements)

		if offset != scenario.ExpectedOffset ||
			limit != scenario.ExpectedLimit {
			t.Fatalf(
				"(%d) offset->'%d' != '%d', limit->'%d' != '%d'",
				(index + 1),
				offset, scenario.ExpectedOffset,
				limit, scenario.ExpectedLimit,
			)
		}
	}
}

type TestCalculateTotalScenario struct {
	Limit         int64
	Offset        int64
	Count         int64
	ExpectedTotal int64
}

func TestCalculateTotal(t *testing.T) {
	scenarios := []TestCalculateTotalScenario{
		{
			Limit:         10,
			Offset:        10,
			Count:         77,
			ExpectedTotal: 8,
		},
		{
			Limit:         10,
			Offset:        0,
			Count:         9,
			ExpectedTotal: 9,
		},
	}

	for index, scenario := range scenarios {
		total := CalculateTotal(scenario.Limit, scenario.Count)

		if total != scenario.ExpectedTotal {
			t.Fatalf("(%d) total->'%d' != '%d'", (index + 1), total, scenario.ExpectedTotal)
		}
	}
}

type TestCalculateRemainingScenario struct {
	Page              int64
	Total             int64
	Elements          int64
	ExpectedRemaining int64
}

func TestCalculateRemaining(t *testing.T) {
	scenarios := []TestCalculateRemainingScenario{
		{
			Page:              1,
			Total:             (10 * (1 * 20)), // Pretend that we have 10 pages worth of elements
			Elements:          20,
			ExpectedRemaining: 9,
		},
		{
			Page:              10,
			Total:             10, // We have a total of 10 elements, page 1 should have been the last page
			Elements:          20,
			ExpectedRemaining: 0,
		},
		{
			Page:              10,
			Total:             9,
			Elements:          20,
			ExpectedRemaining: 0,
		},
	}

	for index, scenario := range scenarios {
		remaining := CalculateRemaining(scenario.Total, scenario.Page, scenario.Elements)

		if remaining != scenario.ExpectedRemaining {
			t.Fatalf(
				"(%d) remaining->'%d' != '%d'",
				(index + 1),
				remaining,
				scenario.ExpectedRemaining,
			)
		}
	}
}
