package pager

import "gitlab.com/eemj/anime-pack/internal/common/apperror"

func ValidatePageElements(page, elements *int64) error {
	if *page == 0 {
		*page = 1
	}
	if *elements == 0 {
		*elements = 20
	}

	if *elements > 100 {
		return apperror.ErrPageElementsCannotBeGreaterThan100
	}

	return nil
}
