package pager

import (
	"math"
)

func CalculateCurrentPage(page int64) int64 {
	if page <= 0 {
		return 1
	}
	return page
}

func CalculateLimitOffset(page, elements int64) (offset, limit int64) {
	offset = int64(0)
	if page > 1 {
		offset = (page - 1) * elements
	}

	limit = elements
	if limit == 0 {
		limit = 20
	}

	return offset, limit
}

func CalculateTotal(limit, count int64) int64 {
	if count < limit {
		return count
	}
	return int64(math.Ceil(float64(count) / float64(limit)))
}

func CalculateRemaining(total, page, elements int64) int64 {
	remaining := total - (page * elements)
	if elements > 0 { // Ensure no divide by 0 panics occur
		remaining /= elements
	}
	if remaining < 0 {
		return 0
	}
	return remaining
}

func HasAnyElements(page, elements, count int64) bool {
	return page*elements > count
}
