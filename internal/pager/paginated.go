package pager

type PageSet[T any] struct {
	Page     int64 `json:"page"`
	Elements int64 `json:"elements"`
	Count    int64 `json:"count"`
	Data     T
}
