package episode

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type EpisodeService interface {
	Create(ctx context.Context, arg params.InsertEpisodeParamsItem) (*domain.Episode, error)
	CreateMany(ctx context.Context, arg params.InsertEpisodeParams) ([]*domain.Episode, error)
	First(ctx context.Context, arg params.FirstEpisodeParams) (*domain.Episode, error)
	Filter(ctx context.Context, arg params.FilterEpisodeParams) ([]*domain.Episode, error)
	Count(ctx context.Context, arg params.CountEpisodeParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateEpisodeParams) (int64, error)
}

type episodeService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements Service.
func (s *episodeService) Count(ctx context.Context, arg params.CountEpisodeParams) (int64, error) {
	count, err := s.querier.CountEpisode(ctx, database.CountEpisodeParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// Create implements Service.
func (s *episodeService) Create(ctx context.Context, arg params.InsertEpisodeParamsItem) (*domain.Episode, error) {
	cacheKey := cachekeys.Episode(nil, &arg.Name)
	if cacheKey != "" {
		cachedValue, err := encoded.Get[*domain.Episode](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cachedValue != nil {
			return cachedValue, nil
		}
	}
	row, err := s.querier.InsertEpisode(ctx, database.InsertEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.Episode{
		ID:   row.ID,
		Name: row.Name,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// CreateMany implements Service.
func (s *episodeService) CreateMany(ctx context.Context, arg params.InsertEpisodeParams) ([]*domain.Episode, error) {
	args := make([]database.InsertManyEpisodeParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyEpisodeParams{
			Name: item.Name,
		}
	}

	bulk := s.querier.InsertManyEpisode(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyEpisodeRow,
		*domain.Episode,
	](bulk, func(row *database.InsertManyEpisodeRow) *domain.Episode {
		return &domain.Episode{
			ID:   row.ID,
			Name: row.Name,
		}
	})
}

func mapEntitesToDomain(entities ...*database.Episode) []*domain.Episode {
	result := make([]*domain.Episode, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Episode{
			ID:   entity.ID,
			Name: entity.Name,
		}
	}
	return result
}

// Filter implements Service.
func (s *episodeService) Filter(ctx context.Context, arg params.FilterEpisodeParams) ([]*domain.Episode, error) {
	entities, err := s.querier.FilterEpisode(ctx, database.FilterEpisodeParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Episode{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *episodeService) First(ctx context.Context, arg params.FirstEpisodeParams) (*domain.Episode, error) {
	entity, err := s.querier.FirstEpisode(ctx, database.FirstEpisodeParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entity)[0], nil
}

// Update implements Service.
func (s *episodeService) Update(ctx context.Context, arg params.UpdateEpisodeParams) (int64, error) {
	return s.querier.UpdateEpisode(ctx, database.UpdateEpisodeParams{
		ID:   arg.ID,
		Name: arg.Name,
	})
}

func NewEpisodeService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) EpisodeService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &episodeService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
