package common

const (
	IrcModuleName = "irc_module"
)

const (
	// anime
	AnimeServiceName              = "anime_service"
	AnimeReviewServiceName        = "anime_review_service"
	AnimeEpisodeLatestServiceName = "anime_episode_latest_service"
	AnimeSearchServiceName        = "anime_search_service"
	AnimeControllerName           = "anime_controller"

	// Title
	TitleServiceName        = "title_service"
	TitleReviewServiceName  = "title_review_service"
	TitleEpisodeServiceName = "title_episode_service"
	TitleAnimeServiceName   = "title_anime_service"

	// Bot
	BotServiceName = "bot_service"

	// Release Group
	ReleaseGroupServiceName = "release_group_service"

	// Quality
	QualityServiceName = "quality_service"

	// Episode
	EpisodeServiceName = "episode_service"

	// XDCC
	XDCCServiceName = "xdcc_service"

	// Video
	VideoServiceName = "video_service"

	// Packlist
	PacklistServiceName = "packlist_service"
	PacklistFetcherName = "packlist_fetcher"

	// Runner
	RunnerServiceName = "runner_service"
	RunnerServerName  = "runner_server"

	// Library
	LibraryServiceName = "library_service"

	// Simuclast
	SimulcastServiceName = "simulcast_service"

	// Genre
	GenreServiceName = "genre_service"

	// Studio
	StudioServiceName = "studio_service"

	// WebScoket
	WebsocketServerName        = "websocket_server"
	WebsocketServerHandlerName = "websocket_server_handler"
)
