package maps

import "sync"

type GroupMap[K, V comparable] interface {
	Add(t K, v V)
	Remove(t K, v V)
	RemoveKey(t K)
	Has(t K) bool
	Len() int
	Inner() map[K][]V
	Range(n func(K, []V) bool)
}

type groupMap[K, V comparable] struct {
	rmu sync.RWMutex
	s   map[K][]V
}

func NewGroupMap[K, V comparable]() GroupMap[K, V] {
	return &groupMap[K, V]{s: make(map[K][]V)}
}

func (s *groupMap[K, V]) Add(k K, v V) {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	if _, exists := s.s[k]; !exists {
		s.s[k] = []V{v}
	} else {
		found := false

		for _, _v := range s.s[k] {
			if found = v == _v; found {
				break
			}
		}

		if !found {
			s.s[k] = append(s.s[k], v)
		}
	}
}

func (s *groupMap[K, V]) Remove(k K, v V) {
	s.rmu.Lock()
	defer s.rmu.Unlock()

	if _, exists := s.s[k]; exists {
		index := -1

		for _index, value := range s.s[k] {
			if value == v {
				index = _index
				break
			}
		}

		if index >= 0 {
			s.s[k] = append(s.s[k][:index], s.s[k][(index+1):]...)
		}
	}
}

func (s *groupMap[K, V]) RemoveKey(k K) {
	s.rmu.Lock()
	delete(s.s, k)
	s.rmu.Unlock()
}

func (s *groupMap[K, V]) Has(k K) (exists bool) {
	s.rmu.RLock()
	_, exists = s.s[k]
	s.rmu.RUnlock()
	return
}

func (s *groupMap[K, V]) Len() (n int) {
	s.rmu.RLock()
	n = len(s.s)
	s.rmu.RUnlock()
	return
}

func (s *groupMap[K, V]) Inner() map[K][]V {
	entries := make(map[K][]V)

	s.rmu.RLock()
	defer s.rmu.RUnlock()

	for key, value := range s.s {
		entries[key] = value
	}

	return entries
}

func (s *groupMap[K, V]) Range(cb func(K, []V) bool) {
	s.rmu.RLock()
	defer s.rmu.RUnlock()

	for key, value := range s.s {
		if !cb(key, value) {
			break
		}
	}
}
