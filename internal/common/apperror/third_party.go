package apperror

import "net/http"

var (
	ErrThirdPartyIntegrationError    = NewError(http.StatusFailedDependency, `third_party_integration`)
	ErrThirdPartyAnimeDoesNotExist   = NewError(http.StatusNotFound, `third_party_anime_does_not_exist`)
	ErrThirdPartyExpectedOneOfTheIds = NewError(http.StatusBadRequest, `third_party_expected_either_one_of_the_ids`)
)
