package apperror

import "net/http"

var (
	ErrPreferencesAnimeIDIsRequired = NewError(http.StatusBadRequest, `preferences_anime_id_is_required`)

	ErrPreferencesUnexpectedPayload = NewError(http.StatusBadRequest, `preferences_unexpected_payload`)
)
