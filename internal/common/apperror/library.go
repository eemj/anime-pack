package apperror

import "net/http"

var (
	ErrLibraryTitleMissing               = NewError(http.StatusBadRequest, `library_title_missing`)
	ErrLibraryAnimeIDGreaterThan0        = NewError(http.StatusBadRequest, `library_anime_id_must_be_greater_than_0`)
	ErrLibraryExpectedDeletedAtOrAnimeID = NewError(http.StatusBadRequest, `library_expected_deleted_at_or_anime_id`)
)
