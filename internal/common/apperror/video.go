package apperror

import "net/http"

var (
	ErrVideoPathRequired = NewError(http.StatusBadRequest, `video_path_is_required`)
	ErrVideoDoesNotExist = NewError(http.StatusNotFound, `video_does_not_exist`)
)
