package apperror

import "net/http"

var (
	ErrIdentifierMustBeNumeric  = NewError(http.StatusBadRequest, `id_must_be_numeric`)
	ErrWrongContextDataType     = NewError(http.StatusInternalServerError, `wrong_context_data_type`)
	ErrExpectedCharButGotString = NewError(http.StatusBadRequest, `expected_char_but_got_string`)
	ErrRecordNotFound           = NewError(http.StatusNotFound, `record_not_found`)
	ErrEpisodeNameRequired      = NewError(http.StatusBadRequest, `episode_name_is_required`)
	ErrUnchangedValue           = NewError(http.StatusBadRequest, `unchanged_value`)
)
