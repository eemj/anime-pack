package apperror

import (
	"net/http"
	"strconv"
)

// Error is defined globally amongst other internal packages.
// It's made up of a code, key and an inner exception.
// Use .Unwrap() to retrieve the inner exception.
type Error struct {
	code  int
	inner error
	key   string
}

func (e *Error) Code() int {
	return e.code
}

func (e *Error) Uwnrap() error {
	return e.inner
}

func (e *Error) Error() string {
	return e.key
}

func (e *Error) Is(err error) bool {
	er, ok := err.(*Error)
	return ok && er.key == e.key
}

func NewError(code int, key string, errors ...error) error {
	err := &Error{code: code, key: key}
	if len(errors) > 0 {
		err.inner = errors[0]
	}
	return err
}

var (
	// ErrSomethingWentWrong is when an unexpected error occurred.
	ErrSomethingWentWrong = NewError(http.StatusInternalServerError, `something_went_wrong`)

	// ErrPageElementsCannotBeGreaterThan100 must not be less than 100.
	ErrPageElementsCannotBeGreaterThan100 = NewError(http.StatusBadRequest, `elements_must_be_less_than_100`)

	// ErrOrderByDirectionUnexpectedValue is when an unexpected direction is specified.
	ErrOrderByDirectionUnexpectedValue = NewError(http.StatusBadRequest, `order_by_direction_unexpected_value`)
)

// ErrExpectedStatusCodeOkButGot is a helper method for when the status code is not 2xx.
func ErrExpectedStatusCodeOkButGot(statusCode int) error {
	return NewError(statusCode, `unexpected_status_code_expected_2xx`)
}

// ErrMustBeGreaterThan is a helper method for when the `prefix` is now greater than `than`.
func ErrMustBeGreaterThan(prefix string, than int) error {
	return NewError(http.StatusBadRequest, (prefix + "_must_be_greater_than_" + strconv.Itoa(than)))
}
