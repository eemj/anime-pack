package apperror

import "net/http"

var (
	ErrRunnerIdentRequired      = NewError(http.StatusBadRequest, `runner_ident_is_required`)
	ErrRunnerNotFound           = NewError(http.StatusNotFound, `runner_not_found`)
	ErrRunnerIsNotConnected     = NewError(http.StatusConflict, `runner_is_not_connected`)
	ErrRunnerTransferIsNotOwned = NewError(http.StatusFailedDependency, `runner_transfer_is_not_owned`)
)
