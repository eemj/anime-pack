package apperror

import "net/http"

var (
	ErrFilterIDsMustBeGreaterThan0               = ErrMustBeGreaterThan(`filter_ids`, 0)
	ErrFilterGenreIDsMustBeGreaterThan0          = ErrMustBeGreaterThan(`filter_genre_ids`, 0)
	ErrFilterStudioIDsMustBeGreaterThan0         = ErrMustBeGreaterThan(`filter_studio_ids`, 0)
	ErrFilterFormatMustBeGreaterThan0            = ErrMustBeGreaterThan(`filter_format`, 0)
	ErrFilterSeasonMustBeGreaterThan0            = ErrMustBeGreaterThan(`filter_season`, 0)
	ErrFilterStatusMustBeGreaterThan0            = ErrMustBeGreaterThan(`filter_status`, 0)
	ErrFilterSeasonYearMustBeGreaterThan0        = ErrMustBeGreaterThan(`filter_season`, 0)
	ErrFilterNextAiringDateGteMustBeGreaterThan0 = ErrMustBeGreaterThan(`filter_next_airing_date_gte`, 0)
	ErrFilterNextAiringDateLteMustBeGreaterThan0 = ErrMustBeGreaterThan(`filter_next_airing_date_lte`, 0)

	ErrFilterEitherIDorTitle        = NewError(http.StatusBadRequest, `filter_either_id_or_title`)
	ErrFilterTitleSpecifiedButEmpty = NewError(http.StatusBadRequest, `filter_title_specified_but_empty`)
	ErrFilterOrderByUnexpectedValue = NewError(http.StatusBadRequest, `filter_orderby_unexpected_value`)
	ErrFilterUnexpectedParse        = NewError(http.StatusBadRequest, `filter_unexpected_body`)
)
