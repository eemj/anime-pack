package apperror

import "net/http"

var (
	// ErrPubSubAlreadySubbedToTopic is when a client is already subscribed to a topic.
	ErrPubSubAlreadySubbedToTopic = NewError(http.StatusBadRequest, `pubsub_already_subbed_to_topic`)
	// ErrPubSubNotSubbedToTopic is when a client is not subscribed to a topic.
	ErrPubSubNotSubbedToTopic = NewError(http.StatusBadRequest, `pubsub_not_subbed_to_topic`)
	// ErrPubSubTopicNotFound is when we attempt subscribe to a non-existant topic.
	ErrPubSubTopicNotFound = NewError(http.StatusNotFound, `pubsub_topic_not_found`)
)
