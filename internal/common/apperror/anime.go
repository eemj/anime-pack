package apperror

import "net/http"

var ErrAnimeDoesNotExist = NewError(http.StatusNotFound, `anime_does_not_exist`)
