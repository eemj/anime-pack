package apperror

import "net/http"

var (
	ErrJobTaskNotFound        = NewError(http.StatusNotFound, `job_task_not_found`)
	ErrJobTaskAlreadyRunning  = NewError(http.StatusBadRequest, `job_task_already_running`)
	ErrJobTaskAlreadyDisabled = NewError(http.StatusBadRequest, `job_task_already_disabled`)
	ErrJobTaskNotRunning      = NewError(http.StatusBadRequest, `job_task_not_running`)
	ErrJobTaskIsNotDisabled   = NewError(http.StatusBadRequest, `job_task_is_not_disabled`)
	ErrJobTaskIsDisabled      = NewError(http.StatusBadRequest, `job_task_is_disabled`)
)
