package apperror

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/eemj/anime-pack/pkg/utils"
)

var (
	ErrTitleIDMissing                       = NewError(http.StatusBadRequest, `title_id_missing`)
	ErrTitleMustNotBeReviewedBeforeDeletion = NewError(http.StatusBadRequest, `title_must_be_reviewed_before_delete`)
	ErrTitleExpectedOneOfTheIds             = NewError(http.StatusBadRequest, `title_expected_either_one_of_the_ids`)
	ErrTitleNotFound                        = NewError(http.StatusNotFound, `title_not_found`)
)

func ErrTitleNotFoundWith(
	name string,
	seasonNumber *string,
	year *int,
) error {
	seasonNumberValue := "<nil>"
	yearValue := "<nil>"
	if seasonNumber != nil {
		seasonNumberValue = *seasonNumber
	}
	if year != nil {
		yearValue = strconv.Itoa(utils.ValueOf(year))
	}
	return fmt.Errorf(
		"title with name:`%s`,season_number:`%s`,year:`%s` not found",
		name,
		seasonNumberValue,
		yearValue,
	)
}
