package common

import (
	"gitlab.com/eemj/anime-pack/internal/domain"
)

type contextKeys string

var (
	TitleContextKey = contextKeys("title")
	AnimeContextKey = contextKeys("anime")
)

type TitleContext struct {
	Title *domain.Title
}

type AnimeContext struct {
	Anime *domain.Anime
}
