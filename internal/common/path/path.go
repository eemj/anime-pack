package path

import (
	"net/url"
	"strings"
	"sync/atomic"
)

var ExternalURL atomic.Pointer[string]

const (
	// prefixImageBase defines the base prefix for the Image name.
	prefixImageBase = "images"
	prefixVideoBase = "videos"

	prefixImagePoster    = "poster"
	prefixImageBanner    = "banner"
	prefixImageThumbnail = "thumbnail"
)

const (
	PrefixImageBase string = "/images/"

	// Deprecated: use the exported Prefixify methods
	// PrefixImagePoster will get appended to the Poster images name for the router.
	PrefixImagePoster string = "/images/poster/"

	// Deprecated: use the exported Prefixify methods
	// PrefixImageBanner will get appended to the Banner images name for the router.
	PrefixImageBanner string = "/images/banner/"

	// Deprecated: use the exported Prefixify methods
	// PrefixImageThumbnail will get appended to the Thumbnail images name for the router.
	PrefixImageThumbnail string = "/images/thumbnail/"

	// Deprecated: use the exported Prefixify methods
	// PrefixVideo will get appended to the Video path for the router.
	PrefixVideo string = "/videos/"
)

func joinPath(paths ...string) string {
	if externalUrl := ExternalURL.Load(); externalUrl != nil && *externalUrl != "" {
		newURL, err := url.JoinPath(*externalUrl, paths...)
		if err == nil {
			return newURL
		}
	}
	newURL, err := url.JoinPath("/", paths...)
	if err == nil {
		return newURL
	}
	return ""
}

func PrefixifyVideo(path string) string { return joinPath(prefixVideoBase, path) }

func PrefixifyImagePoster(path string) string {
	return joinPath(prefixImageBase, prefixImagePoster, path)
}

func PrefixifyImageBanner(path string) string {
	return joinPath(prefixImageBase, prefixImageBanner, path)
}

func PrefixifyImageThumbnail(path string) string {
	return joinPath(prefixImageBase, prefixImageThumbnail, path)
}

func Unprefixify(path string) string {
	lastIndex := strings.LastIndexByte(path, '/')
	if lastIndex == -1 {
		return path
	}
	return path[(lastIndex + 1):]
}
