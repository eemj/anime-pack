package fs

import (
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/eemj/anime-pack/internal/config/configmodels"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/fs/pattern"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

const (
	// ExtensionImage is the format images are going to be saved as
	ExtensionImage string = ".jpg"
	// ExtensionVideo is the format videos are going to be saved as
	ExtensionVideo string = ".mp4"

	// ThumbnailDir defines the sub-dir used to store thumbnails.
	ThumbnailDir string = "thumbnail"
	// PosterDir defines the sub-dir used to store posters.
	PosterDir string = "poster"
	// BannerDir defines the sub-dir used to store banners.
	BannerDir string = "banner"
)

// FileSystem is made up of methods will help us have better Input/Output
// control over out anime/images.
type FileSystem struct {
	patternizer *pattern.Patternizer

	// Media/Downloads/Directory permissions.
	permissions configmodels.MediaPermission

	// Absolute paths.
	downloads          string
	temporaryDownloads string
	images             string
	thumbnails         string
	posters            string
	banners            string
}

// NewFileSystem creates a new instance of the filesystem with the provided configuration,
// while initializing the patternizer.
func NewFileSystem(c configmodels.Directories) (fs *FileSystem) {
	fs = &FileSystem{
		patternizer: &pattern.Patternizer{
			Pattern: c.Pattern,
			Fields:  make(pattern.Fields),
		},

		permissions:        c.MediaPermission,
		downloads:          c.Downloads,
		temporaryDownloads: c.TemporaryDownloads,
		images:             c.Images,
		thumbnails:         filepath.Join(c.Images, ThumbnailDir),
		banners:            filepath.Join(c.Images, BannerDir),
		posters:            filepath.Join(c.Images, PosterDir),
	}

	fs.patternizer.LoadFields(domain.AnimeBase{})

	return fs
}

func (fs FileSystem) exists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

// EnsureDirectories ensures that all directories within the config.Paths are created.
func (fs FileSystem) EnsureDirectories() (err error) {
	for _, path := range []string{
		fs.downloads,
		fs.temporaryDownloads,
		fs.images,
		fs.thumbnails,
		fs.posters,
		fs.banners,
	} {
		stat, err := os.Stat(path)

		if os.IsNotExist(err) {
			if err = os.MkdirAll(path, os.ModePerm); err != nil {
				return err
			}
			continue
		}

		if stat != nil && !stat.IsDir() {
			if err = os.Remove(path); err != nil {
				return err
			}

			if err = os.MkdirAll(path, os.ModePerm); err != nil {
				return err
			}
		}
	}

	return
}

// GetDownloadsPath returns the downloads path
func (fs FileSystem) GetDownloadsPath() string { return fs.downloads }

// GetTemporaryDownloadsPath returns the temporary downloads path
func (fs FileSystem) GetTemporaryDownloadsPath() string { return fs.temporaryDownloads }

// GetPosterPath returns the posters path
func (fs FileSystem) GetPosterPath() string { return fs.posters }

// GetImagesPath returns the images path.
func (fs FileSystem) GetImagesPath() string { return fs.images }

// GetThumbnailPath returns the thumbnails path
func (fs FileSystem) GetThumbnailPath() string { return fs.thumbnails }

// GetBannerPath returns the banners path
func (fs FileSystem) GetBannerPath() string { return fs.banners }

// Close removes the `temporary_downloads` path along with it's contents.
func (fs FileSystem) Close() error {
	return os.RemoveAll(fs.temporaryDownloads)
}

// createFile attempts to create an anime path using the mode, if upon the first creation
// the mode is umasked we'll chmod the file.
func (f FileSystem) createFile(path string, mode fs.FileMode) (*os.File, error) {
	baseDir := filepath.Dir(path)

	if _, err := os.Stat(baseDir); err != nil && os.IsNotExist(err) {
		if err = os.MkdirAll(baseDir, mode); err != nil {
			return nil, err
		}
	}

	fs, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, mode)
	if err != nil {
		return nil, err
	}

	stat, err := fs.Stat()
	if err != nil {
		return nil, err
	}

	// umask might have masked our permission, chmod it to ensure
	// that the permissions are what's expected.
	if stat.Mode() != mode {
		err = os.Chmod(path, mode)
		if err != nil {
			return nil, err
		}
	}

	return fs, nil
}

// L returns a named logger for this module
func (f FileSystem) L() *zap.Logger { return log.Named("fs") }
