package fs

import (
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

func (fs FileSystem) CreateTempAnime(anime domain.AnimeBase) (*os.File, error) {
	path := (*anime.Hash() + "*" + ExtensionVideo)

	return os.CreateTemp(fs.temporaryDownloads, path)
}

func (fs FileSystem) CreatePathAnime(anime domain.AnimeBase) string {
	escaped := anime.Escape()
	path := fs.patternizer.Encode(escaped)

	if !strings.HasSuffix(path, ExtensionVideo) {
		path += ExtensionVideo
	}

	return filepath.Join(fs.downloads, escaped.Title, path)
}

func (fs FileSystem) GetAnime(anime domain.AnimeBase) (io.ReadCloser, error) {
	return os.Open(fs.CreatePathAnime(anime))
}

func (fs FileSystem) HasAnime(anime domain.AnimeBase) bool {
	return fs.exists(fs.CreatePathAnime(anime))
}

// DeleteAnime will delete an anime.
// It won't throw an execption if it doesn't exist.
func (fs FileSystem) DeleteAnime(anime domain.AnimeBase) (err error) {
	path := fs.CreatePathAnime(anime)

	if fs.exists(path) {
		return os.Remove(path)
	}

	return
}

// CreateAnime creates a file based on the created anime and it's base/parent
// dir is ensured that it's created.
func (f FileSystem) CreateAnime(anime domain.AnimeBase) (*os.File, error) {
	path := f.CreatePathAnime(anime)
	return f.createFile(path, f.permissions.Download)
}
