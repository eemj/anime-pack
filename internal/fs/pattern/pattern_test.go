package pattern

import (
	"os"
	"testing"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

type PatternScenario struct {
	Name, With, Pattern string
}

var anime = domain.AnimeBase{
	ID:           21,
	Title:        "One Piece",
	Episode:      "5",
	Height:       720,
	ReleaseGroup: "HorribeSubs",
}

func TestPattern(t *testing.T) {
	scenarios := []PatternScenario{
		{
			Name:    "default pattern without spaces",
			Pattern: "{{Title}} {{Episode}} [{{Height}}p]",
			With:    "One Piece 5 [720p]",
		},
		{
			Name:    "default pattern with spaces",
			Pattern: "{{ Title }} {{ Episode }} [{{ Height }}p]",
			With:    "One Piece 5 [720p]",
		},
		{
			Name:    "random pattern with an invalid property and spaces",
			Pattern: "{{ Titles }} {{ Episode }} {{ Title }} {{ Title }}",
			With:    "{{ Titles }} 5 One Piece One Piece",
		},
		{
			Name:    "default pattern with release groups",
			Pattern: "[{{ReleaseGroup}}] {{Title}} Episode {{Episode}} [{{Height}}p]",
			With:    "[HorribeSubs] One Piece Episode 5 [720p]",
		},
		{
			Name:    "default pattern with an ignored struct tag",
			Pattern: "[{{ReleaseGroup}}] {{Title}} Episode {{Episode}} [{{Height}}p] [anilist:{{ID}}]",
			With:    "[HorribeSubs] One Piece Episode 5 [720p] [anilist:{{ID}}]",
		},
	}

	patternizer := new(Patternizer)
	patternizer.LoadFields(domain.AnimeBase{})

	for _, scenario := range scenarios {
		(*patternizer).Pattern = scenario.Pattern

		encoded := patternizer.Encode(anime)

		if encoded != scenario.With {
			t.Logf("failed scenario: '%s'", scenario.Name)
			t.Logf("unexpected '%s', expected '%s'", encoded, scenario.With)
			t.Fail()
		}
	}
}

func TestMain(m *testing.M) {
	code := m.Run()
	os.Exit(code)
}
