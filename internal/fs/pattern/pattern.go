package pattern

import (
	"reflect"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

const (
	structTag = "patternizer"
)

type Fields map[string]string

type Patternizer struct {
	Pattern string
	Fields  Fields
}

func (p *Patternizer) LoadFields(iface any) {
	p.Fields = make(Fields)

	value := reflect.ValueOf(iface)

	for i := 0; i < value.NumField(); i++ {
		field := value.Type().Field(i)
		if tag := strings.TrimSpace(field.Tag.Get(structTag)); tag != "-" {
			p.Fields[tag] = field.Name
		}
	}
}

func (p *Patternizer) Encode(anime domain.AnimeBase) (decoded string) {
	enclosed := false
	key := ""
	decoded = ""
	start := 0

	for i := 0; i < len(p.Pattern); i++ {
		current := rune(p.Pattern[i])

		if i == 0 && unicode.IsSpace(current) {
			continue
		} else if (i+1) == len(p.Pattern) && !unicode.IsSpace(current) {
			decoded += string(current)
			continue
		}

		next := rune(p.Pattern[i+1])

		if (current == '{' && next == '{') || (current == '}' && next == '}') {
			if enclosed = (current == '{' && next == '{'); enclosed {
				start = i
			} else {
				if value, exists := p.Fields[key]; exists {
					valueType := reflect.ValueOf(anime).FieldByName(value)
					kind := valueType.Kind()

					if kind == reflect.String {
						decoded += valueType.String()
					} else if kind == reflect.Uint {
						decoded += strconv.FormatUint(valueType.Uint(), 10)
					} else if kind == reflect.Int {
						decoded += strconv.FormatInt(valueType.Int(), 10)
					}
				} else {
					decoded += p.Pattern[start:(i + 2)]
				}
				key = ""
			}

			i++

			continue
		}

		switch {
		case enclosed && !unicode.IsSpace(current):
			key += string(current)
		case !enclosed:
			decoded += string(current)
		}
	}

	return
}
