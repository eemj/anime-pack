package fs

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"

	"golang.org/x/image/webp"
)

func (fs FileSystem) HasPosterImage(hash string) bool {
	return fs.exists(filepath.Join(fs.posters, (hash + ExtensionImage)))
}

func (fs FileSystem) HasBannerImage(hash string) bool {
	return fs.exists(filepath.Join(fs.banners, (hash + ExtensionImage)))
}

func (fs FileSystem) HasThumbnailImage(hash string) bool {
	return fs.exists(filepath.Join(fs.thumbnails, (hash + ExtensionImage)))
}

const (
	contentType_ImagePng  = "image/png"
	contentType_ImageWebp = "image/webp"
	contentType_ImageGif  = "image/gif"
)

func (fs *FileSystem) writeImage(dir string, reader io.Reader) (hash string, err error) {
	readerData, err := io.ReadAll(reader)
	if err != nil {
		return "", err
	}
	if len(readerData) == 0 {
		return "", errors.New("provided image does not have any data")
	}

	var (
		img           image.Image
		needsEncoding bool
		contentType   = http.DetectContentType(readerData)
	)

	switch contentType {
	case contentType_ImagePng, contentType_ImageGif, contentType_ImageWebp:
		needsEncoding = true

		switch contentType {
		case contentType_ImagePng:
			img, err = png.Decode(bytes.NewReader(readerData))
		case contentType_ImageGif:
			img, err = gif.Decode(bytes.NewReader(readerData))
		case contentType_ImageWebp:
			img, err = webp.Decode(bytes.NewReader(readerData))
		}
	}
	if err != nil {
		return "", err
	}

	var (
		buffer         = bytes.NewBuffer(nil)
		cipher         = md5.New()
		cipheredBuffer = io.MultiWriter(buffer, cipher)
	)

	if needsEncoding {
		err := jpeg.Encode(cipheredBuffer, img, &jpeg.Options{Quality: 100})
		if err != nil {
			return "", err
		}
	} else {
		_, err := io.Copy(cipheredBuffer, bytes.NewReader(readerData))
		if err != nil {
			return "", err
		}
	}

	hash = hex.EncodeToString(cipher.Sum(nil))
	path := filepath.Join(dir, (hash + ExtensionImage))

	if fs.exists(path) {
		return hash, nil
	}

	f, err := fs.createFile(path, fs.permissions.Image)
	if err != nil {
		return "", err
	}
	defer f.Close()

	_, err = buffer.WriteTo(f)
	if err != nil {
		return "", err
	}

	return hash, nil
}

func (fs FileSystem) deleteImage(path string) error {
	if fs.exists(path) {
		return os.Remove(path)
	}
	return nil
}

func (fs FileSystem) CreatePosterImage(reader io.Reader) (string, error) {
	return fs.writeImage(fs.posters, reader)
}

func (fs FileSystem) CreateBannerImage(reader io.Reader) (string, error) {
	return fs.writeImage(fs.banners, reader)
}

func (fs FileSystem) CreateThumbnailImage(reader io.Reader) (string, error) {
	return fs.writeImage(fs.thumbnails, reader)
}

func (fs FileSystem) DeletePosterImage(filename string) error {
	return fs.deleteImage(filepath.Join(fs.posters, filename))
}

func (fs FileSystem) DeleteBannerImage(filename string) error {
	return fs.deleteImage(filepath.Join(fs.banners, filename))
}

func (fs FileSystem) DeleteThumbnailImage(filename string) error {
	return fs.deleteImage(filepath.Join(fs.thumbnails, filename))
}

func (fs FileSystem) CreateTempImage() (*os.File, error) {
	return os.CreateTemp(
		fs.temporaryDownloads,
		("image*" + ExtensionImage),
	)
}

func (fs FileSystem) GetBannerImages() ([]fs.DirEntry, error) {
	return os.ReadDir(fs.banners)
}

func (fs FileSystem) GetPosterImages() ([]fs.DirEntry, error) {
	return os.ReadDir(fs.posters)
}

func (fs FileSystem) GetThumbnailImages() ([]fs.DirEntry, error) {
	return os.ReadDir(fs.thumbnails)
}
