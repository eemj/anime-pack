package releasegroup

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type ReleaseGroupService interface {
	Available(ctx context.Context) ([]*domain.ReleaseGroup, error)
	Create(ctx context.Context, arg params.InsertReleaseGroupParamsItem) (*domain.ReleaseGroup, error)
	CreateMany(ctx context.Context, arg params.InsertReleaseGroupParams) ([]*domain.ReleaseGroup, error)
	First(ctx context.Context, arg params.FirstReleaseGroupParams) (*domain.ReleaseGroup, error)
	Filter(ctx context.Context, arg params.FilterReleaseGroupParams) ([]*domain.ReleaseGroup, error)
	Count(ctx context.Context, arg params.CountReleaseGroupParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateReleaseGroupParams) (int64, error)
}

type releaseGroupService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements Service.
func (s *releaseGroupService) Count(ctx context.Context, arg params.CountReleaseGroupParams) (int64, error) {
	count, err := s.querier.CountReleaseGroup(ctx, database.CountReleaseGroupParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// Create implements Service.
func (s *releaseGroupService) Create(ctx context.Context, arg params.InsertReleaseGroupParamsItem) (*domain.ReleaseGroup, error) {
	cacheKey := cachekeys.ReleaseGroup(&arg.Name)
	if cacheKey != "" {
		cacheValue, err := encoded.Get[*domain.ReleaseGroup](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cacheValue != nil {
			return cacheValue, nil
		}
	}
	row, err := s.querier.InsertReleaseGroup(ctx, database.InsertReleaseGroupParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.ReleaseGroup{
		ID:   row.ID,
		Name: row.Name,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// CreateMany implements Service.
func (s *releaseGroupService) CreateMany(ctx context.Context, arg params.InsertReleaseGroupParams) ([]*domain.ReleaseGroup, error) {
	args := make([]database.InsertManyReleaseGroupParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyReleaseGroupParams{
			Name: item.Name,
		}
	}

	bulk := s.querier.InsertManyReleaseGroup(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyReleaseGroupRow,
		*domain.ReleaseGroup,
	](bulk, func(row *database.InsertManyReleaseGroupRow) *domain.ReleaseGroup {
		return &domain.ReleaseGroup{
			ID:   row.ID,
			Name: row.Name,
		}
	})
}

func mapEntitesToDomain(entities ...*database.ReleaseGroup) []*domain.ReleaseGroup {
	result := make([]*domain.ReleaseGroup, len(entities))
	for index, entity := range entities {
		result[index] = &domain.ReleaseGroup{
			ID:   entity.ID,
			Name: entity.Name,
		}
	}
	return result
}

// Filter implements Service.
func (s *releaseGroupService) Filter(ctx context.Context, arg params.FilterReleaseGroupParams) ([]*domain.ReleaseGroup, error) {
	entities, err := s.querier.FilterReleaseGroup(ctx, database.FilterReleaseGroupParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.ReleaseGroup{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *releaseGroupService) First(ctx context.Context, arg params.FirstReleaseGroupParams) (*domain.ReleaseGroup, error) {
	entity, err := s.querier.FirstReleaseGroup(ctx, database.FirstReleaseGroupParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entity)[0], nil
}

// Update implements Service.
func (s *releaseGroupService) Update(ctx context.Context, arg params.UpdateReleaseGroupParams) (int64, error) {
	return s.querier.UpdateReleaseGroup(ctx, database.UpdateReleaseGroupParams{
		ID:   arg.ID,
		Name: arg.Name,
	})
}

// Available implements Service.
func (s *releaseGroupService) Available(ctx context.Context) ([]*domain.ReleaseGroup, error) {
	cachedValues, err := encoded.Get[[]*domain.ReleaseGroup](ctx, s.cacher, cachekeys.AnimeReleaseGroups)
	if err != nil {
		return nil, err
	}
	if len(cachedValues) > 0 {
		return cachedValues, nil
	}
	releaseGroups, err := s.querier.AvailableReleaseGroups(ctx)
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.ReleaseGroup{}, nil
		}
		return nil, err
	}
	items := mapEntitesToDomain(releaseGroups...)
	err = encoded.Set(ctx, s.cacher, cachekeys.AnimeReleaseGroups, items, cacher.DefaultTTL)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func NewReleaseGroupService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) ReleaseGroupService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &releaseGroupService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
