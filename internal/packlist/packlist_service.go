package packlist

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/logx/log"
	"go.uber.org/zap"
)

type PacklistService interface {
	CreateForTitle(ctx context.Context, arg params.CreateForTitlePacklist) (*domain.PacklistSync, error)
	RemoveDeadPacks(ctx context.Context, arg params.RemoveDeadPacksPacklist) ([]*domain.DeadXDCCReason, error)
}

type packlistService struct {
	querier      database.QuerierExtended
	associations PacklistAssociations
}

func (s *packlistService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.PacklistServiceName)
}

func NewPacklistService(
	querier database.QuerierExtended,
	associations PacklistAssociations,
) PacklistService {
	return &packlistService{
		querier:      querier,
		associations: associations,
	}
}
