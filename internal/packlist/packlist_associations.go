package packlist

import "gitlab.com/eemj/anime-pack/internal/xdcc"

type PacklistAssociations interface {
	XDCC() xdcc.XDCCService
}

type packlistAssociations struct {
	xdcc xdcc.XDCCService
}

func (s *packlistAssociations) XDCC() xdcc.XDCCService { return s.xdcc }

func NewPacklistAssociations(
	xdcc xdcc.XDCCService,
) PacklistAssociations {
	return &packlistAssociations{
		xdcc: xdcc,
	}
}
