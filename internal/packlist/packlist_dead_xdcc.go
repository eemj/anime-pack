package packlist

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/groups"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

func (s *packlistService) RemoveDeadPacks(ctx context.Context, arg params.RemoveDeadPacksPacklist) ([]*domain.DeadXDCCReason, error) {
	xdccs, err := s.associations.XDCC().Filter(ctx, params.FilterXDCCParams{
		IsDeleted:    utils.PointerOf(false),
		WithoutVideo: true,
	})
	if err != nil {
		return nil, err
	}
	var (
		reasons []*domain.DeadXDCCReason
		xdccIDs []int64
	)
	for _, xdcc := range xdccs {
		// See CreateForTitle() implementation, same reason why
		// we're checking if this XDCC is eligible for deletion.
		if xdcc.IsFairlyNew() {
			continue
		}

		var pack *domain.Pack
		for _, _pack := range arg.Items {
			if _pack.Filename == xdcc.Filename && _pack.Bot == xdcc.Bot.Name {
				pack = _pack
				break
			}
		}
		if pack == nil {
			reasons = append(reasons, &domain.DeadXDCCReason{
				Reason: enums.XDCCRemoveReasonPACKNOTFOUND,
				XDCC:   *xdcc,
			})
			xdccIDs = append(xdccIDs, xdcc.ID)
			continue
		}

		entry := groups.Parse(pack.Filename)
		if entry == nil {
			reasons = append(reasons, &domain.DeadXDCCReason{
				Reason: enums.XDCCRemoveReasonGROUPPARSEFAILED,
				XDCC:   *xdcc,
			})
			xdccIDs = append(xdccIDs, xdcc.ID)
		}
	}

	if len(xdccIDs) > 0 {
		rowsAffected, err := s.associations.XDCC().Delete(ctx, params.DeleteXDCCParams{
			IDs: xdccIDs,
		})
		if err != nil {
			return nil, err
		}
		s.L(ctx).Info(
			"removed dead xdccs",
			zap.Int64("rows_affected", rowsAffected),
			zap.Int("rows_inputted", len(xdccIDs)),
		)
	}

	return reasons, nil
}
