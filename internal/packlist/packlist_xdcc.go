package packlist

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

func (s *packlistService) CreateForTitle(ctx context.Context, arg params.CreateForTitlePacklist) (*domain.PacklistSync, error) {
	xdccs, err := s.associations.XDCC().Filter(ctx, params.FilterXDCCParams{
		WithoutVideo: false,
		TitleID:      &arg.TitleID,
	})
	if err != nil {
		return nil, err
	}
	var (
		count  = len(xdccs)
		create []params.InsertXDCCParamsItem
		status = &domain.PacklistSync{
			Retrieved: count,
		}
	)
	for _, pack := range arg.Packs {
		if pack.Entry.Episode == "" {
			continue
		}

		// Check if we have this XDCC entry in our database, if we do
		// compare the `pack` and the `size`, if they're different
		// do an update, otherwise continue with the flow to create.
		if count > 0 {
			var xdcc *domain.XDCC
			for _, nextXdcc := range xdccs {
				if nextXdcc.Filename == pack.Pack.Filename &&
					nextXdcc.Bot.Name == pack.Pack.Bot &&
					nextXdcc.ReleaseGroup.Name == pack.Entry.ReleaseGroup {
					xdcc = nextXdcc
					break
				}
			}

			if xdcc != nil {
				packSizeUpdated := xdcc.Pack != pack.Pack.Pack || xdcc.Size != pack.Pack.Size
				filenameUpdated := xdcc.EscapedFilename == ""

				if packSizeUpdated || filenameUpdated {
					updateArg := params.UpdateXDCCParams{ID: xdcc.ID}

					if filenameUpdated {
						updateArg.EscapedFilename = utils.PointerOf(utils.RemoveEvilCharacters(xdcc.Filename))
					}
					if packSizeUpdated {
						updateArg.Pack = &pack.Pack.Pack
						updateArg.Size = &pack.Pack.Size
					}

					rowsAffected, err := s.associations.XDCC().Update(ctx, updateArg)
					if err != nil {
						return nil, err
					}
					if rowsAffected > 0 {
						status.Updated++
					}
				}

				// We just updated, skip the create
				continue
			}
		}

		// TODO(eemj): A BIG TODO here, we have the potential to improve the speeds here by
		// finally making use of batching compared to the previous implementation
		// were we were limited to create them 1 by 1.
		//
		// For now it'll stay 1 by 1 till we get everything working and properly tested
		// afterwards, well start looking into the below:

		// Create Bot
		bot, err := s.associations.XDCC().Associations().Bot().Create(ctx, params.InsertBotParamsItem{
			Name: pack.Pack.Bot,
		})
		if err != nil {
			return nil, err
		}
		if bot == nil {
			s.L(ctx).Warn(
				"unexpected `bot` row",
				zap.String("name", pack.Pack.Bot),
			)
		}

		// Create Quality if present
		var qualityID *int64
		if pack.Entry.VideoResolution.Height > 0 {
			quality, err := s.associations.XDCC().Associations().Quality().Create(ctx, params.InsertQualityParamsItem{
				Height: int64(pack.Entry.VideoResolution.Height),
			})
			if err != nil {
				return nil, err
			}
			if quality != nil {
				qualityID = &quality.ID
			} else {
				s.L(ctx).Warn(
					"unexpected `qualities` row",
					zap.Int("height", pack.Entry.VideoResolution.Height),
				)
			}
		}

		// Create ReleaseGroup
		releaseGroup, err := s.associations.XDCC().Associations().ReleaseGroup().Create(ctx, params.InsertReleaseGroupParamsItem{
			Name: pack.Entry.ReleaseGroup,
		})
		if err != nil {
			return nil, err
		}

		// Create Episode
		episode, err := s.associations.XDCC().Associations().Episode().Create(ctx, params.InsertEpisodeParamsItem{
			Name: pack.Entry.Episode,
		})
		if err != nil {
			return nil, err
		}

		// Create Title Episode
		titleEpisode, err := s.associations.XDCC().Associations().TitleEpisode().Create(ctx, params.InsertTitleEpisodeParamsItem{
			TitleID:   arg.TitleID,
			EpisodeID: episode.ID,
		})
		if err != nil {
			return nil, err
		}

		createXdcc := params.InsertXDCCParamsItem{
			Pack:            pack.Pack.Pack,
			Size:            pack.Pack.Size,
			Filename:        pack.Pack.Filename,
			EscapedFilename: utils.RemoveEvilCharacters(pack.Pack.Filename),
			BotID:           bot.ID,
			ReleaseGroupID:  releaseGroup.ID,
			TitleEpisodeID:  titleEpisode.ID,
			QualityID:       qualityID,
		}

		// Append XDCC creation
		create = append(create, createXdcc)
	}

	if created := len(create); created > 0 {
		err := s.associations.XDCC().CreateMany(ctx, params.InsertXDCCParams{Items: create})
		if err != nil {
			return nil, err
		}
		status.Created = created
	}

	// This logic needs to be implemented better.. as packs from bots can be removed/moved to another bot
	// if they're by the same owner. E.g. once thing I've noticed, is that Ginpachi-sensei removes
	// packs once they're in the ARUTHA batches.. in this case, we should remove ours as well..
	//
	// Given the query above, we're not retrieving XDCC's which belongs to a video, therefore deletion here
	// can he hard (completely removeXDCCIDs the entry instead of setting the `deleted_at` property).
	var removeXDCCIDs []int64
	for _, xdcc := range xdccs {
		// If we have a video, the video entry has a dependency on us so we'll have to skip
		// the deletion.
		if len(xdcc.Videos) > 0 {
			continue
		}

		// A bit of a hack, but the XDCC entry must be an old one to consider it as deleted
		// the issue here is that we can have an active download for this exact XDCC
		// but the cache still remains.
		//
		// Ideally we enhance the cache so when we receive an IRC notice
		// we'll update the cache upon XDCC create.
		if xdcc.IsFairlyNew() {
			continue
		}

		found := false
		for _, pack := range arg.Packs {
			if pack.Pack.Filename == xdcc.Filename &&
				pack.Pack.Bot == xdcc.Bot.Name &&
				pack.Pack.Pack == xdcc.Pack &&
				pack.Pack.Size == xdcc.Size &&
				pack.Entry.ReleaseGroup == xdcc.ReleaseGroup.Name &&
				((xdcc.Quality != nil && int64(pack.Entry.VideoResolution.Height) == xdcc.Quality.Height) || xdcc.Quality == nil) {
				found = true
				break
			}
		}

		if !found {
			removeXDCCIDs = append(removeXDCCIDs, xdcc.ID)
		}
	}

	if len(removeXDCCIDs) > 0 {
		rowsAffected, err := s.associations.XDCC().Delete(ctx, params.DeleteXDCCParams{
			IDs: removeXDCCIDs,
		})
		if err != nil {
			return nil, err
		}
		status.Deleted = int(rowsAffected)
	}

	return status, nil
}
