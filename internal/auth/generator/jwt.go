package generator

import (
	"encoding/base64"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

type generator struct{}

// Generates creates a base64 encoded JWT token based on the claims specified.
func (j *generator) Generate(claims jwt.Claims, secret SecretKey) (token string, err error) {
	signature := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)

	jwtToken, err := signature.SignedString([]byte(secret))
	if err != nil {
		return
	}

	return base64.RawStdEncoding.EncodeToString([]byte(jwtToken)), nil
}

func (j *generator) Validate(token string, claims jwt.Claims, secret SecretKey) (parsed jwt.Claims, err error) {
	if decoded, err := base64.RawStdEncoding.DecodeString(token); err == nil {
		token = string(decoded)
	}

	jwtToken, err := jwt.ParseWithClaims(
		token,
		claims,
		func(t *jwt.Token) (any, error) {
			if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf(
					"unexpected signing method '%v'",
					t.Header["alg"],
				)
			}

			return []byte(secret), nil
		},
	)
	if err != nil {
		return
	}

	if jwtToken != nil && jwtToken.Valid {
		return jwtToken.Claims, nil
	}

	return
}

func NewGenerator() Generator { return &generator{} }
