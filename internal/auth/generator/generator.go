package generator

import "github.com/dgrijalva/jwt-go"

type SecretKey string

type Generator interface {
	Generate(claims jwt.Claims, secret SecretKey) (token string, err error)
	Validate(token string, claims jwt.Claims, secret SecretKey) (parsed jwt.Claims, err error)
}
