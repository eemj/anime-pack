package claims

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

// RunnerClaims implements the jwt.Claims interface.
type RunnerClaims struct {
	IssuedAt  time.Time `json:"iat,omitempty"`
	ExpiresAt time.Time `json:"exp,omitempty"`

	Name     string `json:"n"`
	User     string `json:"us"`
	Docker   bool   `json:"dkr"`
	Hostname string `json:"hst"`
	Ident    string `json:"idt"`
}

// Valid validates the Runner claims. Validations occuring are ExpiredAt & IssuedAt.
func (r *RunnerClaims) Valid() (err error) {
	now := time.Now()

	if now.After(r.ExpiresAt) {
		return jwt.NewValidationError(
			fmt.Sprintf("token is expired by '%.4f'", now.Sub(r.ExpiresAt).Seconds()),
			jwt.ValidationErrorExpired,
		)
	}

	if now.Before(r.IssuedAt) {
		return jwt.NewValidationError(
			"token used before issued",
			jwt.ValidationErrorIssuedAt,
		)
	}

	if utils.IsEmpty(r.Name) {
		return jwt.NewValidationError(
			"`n` is required",
			jwt.ValidationErrorClaimsInvalid,
		)
	}

	if utils.IsEmpty(r.User) {
		return jwt.NewValidationError(
			"`us` is required",
			jwt.ValidationErrorClaimsInvalid,
		)
	}

	if utils.IsEmpty(r.Hostname) {
		return jwt.NewValidationError(
			"`hst` is required",
			jwt.ValidationErrorClaimsInvalid,
		)
	}

	if utils.IsEmpty(r.Ident) {
		return jwt.NewValidationError(
			"`idt` is required",
			jwt.ValidationErrorClaimsInvalid,
		)
	}

	return
}
