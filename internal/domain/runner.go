package domain

import "time"

type Runner struct {
	ID        int64     `json:"id,omitempty"`
	CreatedAt time.Time `json:"createdAt,omitempty"`
	UpdatedAt time.Time `json:"updatedAt,omitempty"`
	Name      string    `json:"name,omitempty"`
	User      string    `json:"user,omitempty"`
	Ident     string    `json:"ident,omitempty"`
	Hostname  string    `json:"hostname,omitempty"`
	Signature string    `json:"signature,omitempty"`
}
