package domain

import "time"

type Preferences struct {
	AnimeID   int64     `json:"-"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`

	Favourite          *bool `json:"favourite,omitempty"`
	AutomaticDownloads *bool `json:"automaticDownloads,omitempty"`
	PerformChecksum    *bool `json:"performChecksum,omitempty"`
}
