package domain

import (
	"time"
)

type LibraryItem struct {
	TitleName         string     `json:"titleName,omitempty"`
	TitleSeasonNumber *string    `json:"seasonNumber,omitempty"`
	TitleYear         *int       `json:"year,omitempty"`
	DeletedAt         *time.Time `json:"deletedAt,omitempty"`
	AnimeID           *int64     `json:"animeID,omitempty"`
}

type LibraryVideo struct {
	AnimeID          int64  `json:"animeID,omitempty"`
	TitleEpisodeID   int64  `json:"titleEpisodeID,omitempty"`
	XDCCID           int64  `json:"xdccid,omitempty"`
	ReleaseGroupName string `json:"releaseGroupName,omitempty"`
	QualityHeight    int64  `json:"qualityHeight,omitempty"`
	AnimeTitle       string `json:"animeTitle,omitempty"`
	EpisodeName      string `json:"episodeName,omitempty"`
}

type ListReviewedLibraryItem struct {
	Title   Title
	AnimeID []int64
}

type ListReviewedLibraryItemsFlat []ListReviewedLibraryItemFlat

func (a ListReviewedLibraryItemsFlat) Unflatten() []*ListReviewedLibraryItem {
	group := make(map[int64]*ListReviewedLibraryItem)
	result := make([]*ListReviewedLibraryItem, 0)
	for _, item := range a {
		libraryItem, exists := group[item.TitleID]
		if !exists {
			libraryItem = &ListReviewedLibraryItem{
				Title: Title{
					ID:           item.TitleID,
					Name:         item.TitleName,
					Reviewed:     item.TitleReviewed,
					CreatedAt:    item.TitleCreatedAt,
					UpdatedAt:    item.TitleUpdatedAt,
					DeletedAt:    item.TitleDeletedAt,
					SeasonNumber: item.TitleSeasonNumber,
					Year:         item.TitleYear,
				},
			}
			result = append(result, libraryItem)
		}
		libraryItem.AnimeID = append(libraryItem.AnimeID, item.AnimeID)
		group[item.TitleID] = libraryItem
	}
	return result
}

type ListReviewedLibraryItemFlat struct {
	TitleID           int64
	TitleName         string
	TitleReviewed     bool
	TitleCreatedAt    time.Time
	TitleUpdatedAt    time.Time
	TitleDeletedAt    *time.Time
	TitleSeasonNumber *string
	TitleYear         *int
	AnimeID           int64
}

type LibraryBestQualityPerReleaseGroup struct {
	Quality      *Quality     `json:"quality,omitempty"`
	ReleaseGroup ReleaseGroup `json:"releaseGroup,omitempty"`
}

type LibraryBestQualitiesPerReleaseGroup []*LibraryBestQualityPerReleaseGroup

func (items LibraryBestQualitiesPerReleaseGroup) FindByReleaseGroup(releaseGroupName string) *Quality {
	for _, item := range items {
		if item.ReleaseGroup.Name == releaseGroupName {
			return item.Quality
		}
	}
	return nil
}

type LibraryBestQualitiesPerReleaseGroupSortByReleaseGroup LibraryBestQualitiesPerReleaseGroup

func (a LibraryBestQualitiesPerReleaseGroupSortByReleaseGroup) Len() int { return len(a) }
func (a LibraryBestQualitiesPerReleaseGroupSortByReleaseGroup) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a LibraryBestQualitiesPerReleaseGroupSortByReleaseGroup) Less(i, j int) bool {
	return a[i].ReleaseGroup.Name < a[j].ReleaseGroup.Name
}
