package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type XDCC struct {
	ID              int64      `json:"id,omitempty"`
	Pack            int64      `json:"pack,omitempty"`
	Size            int64      `json:"size,omitempty"`
	Filename        string     `json:"filename,omitempty"`
	EscapedFilename string     `json:"escapedFilename,omitempty"`
	BotID           int64      `json:"botID,omitempty"`
	QualityID       *int64     `json:"qualityID,omitempty"`
	ReleaseGroupID  int64      `json:"releaseGroupID,omitempty"`
	TitleEpisodeID  int64      `json:"titleEpisodeID,omitempty"`
	DeletedAt       *time.Time `json:"deletedAt,omitempty"`
	CreatedAt       time.Time  `json:"createdAt,omitempty"`
	UpdatedAt       time.Time  `json:"updatedAt,omitempty"`
	IsDeleted       bool       `json:"isDeleted,omitempty"`

	Bot          Bot          `json:"bot,omitempty"`
	Quality      *Quality     `json:"quality,omitempty"`
	ReleaseGroup ReleaseGroup `json:"releaseGroup,omitempty"`
	TitleEpisode TitleEpisode `json:"titleEpisode,omitempty"`
	Videos       []*Video     `json:"videos.omitempty"`
}

func (x XDCC) IsFairlyNew() bool {
	return !x.IsDeleted && time.Since(x.CreatedAt) >= 6*time.Hour
}

type XDCCFlat struct {
	ID                    int64
	Pack                  int64
	Size                  int64
	Filename              string
	EscapedFilename       string
	BotID                 int64
	QualityID             *int64
	ReleaseGroupID        int64
	TitleEpisodeID        int64
	CreatedAt             time.Time
	UpdatedAt             time.Time
	DeletedAt             *time.Time
	IsDeleted             bool
	VideoID               *int64
	BotName               string
	ReleaseGroupName      string
	QualityHeight         *int64
	TitleEpisodeTitleID   int64
	TitleEpisodeEpisodeID int64
	EpisodeName           string
	TitleName             string
	TitleSeasonNumber     *string
	TitleYear             *int
}

type XDCCFlatItems []XDCCFlat

func (items XDCCFlatItems) Unflatten() []*XDCC {
	result := make([]*XDCC, 0)
	for _, flatItem := range items {
		var found *XDCC
		for _, item := range result {
			if item.ID == flatItem.ID {
				found = item
				break
			}
		}
		if found == nil {
			found = flatItem.Unflatten()
			result = append(result, found)
		} else if flatItem.VideoID != nil {
			var foundVideo *Video
			for _, video := range found.Videos {
				if video.ID == *flatItem.VideoID {
					foundVideo = video
					break
				}
			}
			if foundVideo != nil {
				continue
			}
			found.Videos = append(found.Videos, &Video{
				ID: *flatItem.VideoID,
			})
		}
	}
	return result
}

func (x XDCCFlat) Unflatten() *XDCC {
	result := &XDCC{
		ID:              x.ID,
		Pack:            x.Pack,
		Size:            x.Size,
		Filename:        x.Filename,
		EscapedFilename: x.EscapedFilename,
		BotID:           x.BotID,
		Bot: Bot{
			ID:   x.BotID,
			Name: x.BotName,
		},
		QualityID:      x.QualityID,
		ReleaseGroupID: x.ReleaseGroupID,
		ReleaseGroup: ReleaseGroup{
			ID:   x.ReleaseGroupID,
			Name: x.ReleaseGroupName,
		},
		TitleEpisodeID: x.TitleEpisodeID,
		TitleEpisode: TitleEpisode{
			ID:        x.TitleEpisodeID,
			TitleID:   x.TitleEpisodeTitleID,
			EpisodeID: x.TitleEpisodeEpisodeID,
			Title: Title{
				ID:           x.TitleEpisodeTitleID,
				Name:         x.TitleName,
				SeasonNumber: x.TitleSeasonNumber,
				Year:         x.TitleYear,
			},
			Episode: Episode{
				ID:   x.TitleEpisodeEpisodeID,
				Name: x.EpisodeName,
			},
		},
		CreatedAt: x.CreatedAt,
		UpdatedAt: x.UpdatedAt,
		DeletedAt: x.DeletedAt,
		IsDeleted: x.IsDeleted,
	}
	if x.QualityID != nil && *x.QualityID > 0 {
		result.Quality = &Quality{
			ID:     *x.QualityID,
			Height: *x.QualityHeight,
		}
	}
	if x.VideoID != nil && *x.VideoID > 0 {
		result.Videos = append(result.Videos, &Video{
			ID: *x.VideoID,
		})
	}
	return result
}

type DeadXDCCReason struct {
	Reason enums.XDCCRemoveReason
	XDCC   XDCC
}
