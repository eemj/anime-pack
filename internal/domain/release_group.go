package domain

type ReleaseGroup struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}
