package domain

import "gitlab.com/eemj/anime-pack/internal/common/path"

type ThumbnailReview struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
}

type Thumbnail struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
}

func (p *Thumbnail) Review() *ThumbnailReview {
	return &ThumbnailReview{
		ID:   p.ID,
		Hash: p.Hash,
		Name: path.PrefixifyImageThumbnail(p.Name),
	}
}
