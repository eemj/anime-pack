package enums

import (
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type AnimeLatestOrderBy string

const (
	AnimeLatestOrderByID        AnimeLatestOrderBy = "id"
	AnimeLatestOrderByCREATEDAT AnimeLatestOrderBy = "createdAt"
	AnimeLatestOrderBySCORE     AnimeLatestOrderBy = "score"
)

const AnimeLatestOrderByDEFAULT = AnimeLatestOrderByCREATEDAT

var (
	PossibleAnimeLatestOrderBy = [...]string{
		AnimeLatestOrderByID.String(),
		AnimeLatestOrderByCREATEDAT.String(),
		AnimeLatestOrderBySCORE.String(),
	}

	ErrUnexpectedAnimeLatestOrderByValue = apperror.NewError(
		http.StatusBadRequest,
		`unexpected_anime_latest_order_by_value`,
		fmt.Errorf(
			"expected one of the following values: [`%s`]",
			sliceutil.SurroundWith(',', PossibleAnimeLatestOrderBy),
		),
	)
)

func (a AnimeLatestOrderBy) String() string {
	switch a {
	case AnimeLatestOrderByID,
		AnimeLatestOrderByCREATEDAT,
		AnimeLatestOrderBySCORE:
		return string(a)
	}
	return string(AnimeLatestOrderByDEFAULT)
}
func (a AnimeLatestOrderBy) Enum() *AnimeLatestOrderBy { return &a }

func ValidateAnimeLatestOrderBy(orderBy *string) error {
	if *orderBy == "" {
		*orderBy = AnimeLatestOrderByDEFAULT.String()
		return nil
	}

	switch *orderBy {
	case AnimeLatestOrderByID.String(),
		AnimeLatestOrderBySCORE.String(),
		AnimeLatestOrderByCREATEDAT.String():
		return nil
	}
	return ErrUnexpectedAnimeLatestOrderByValue
}
