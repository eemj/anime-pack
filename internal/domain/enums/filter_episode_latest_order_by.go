package enums

import (
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type EpisodeLatestOrderBy string

const (
	EpisodeLatestOrderByCREATEDAT EpisodeLatestOrderBy = "createdAt"
	EpisodeLatestOrderBySIZE      EpisodeLatestOrderBy = "size"
	EpisodeLatestOrderBySCORE     EpisodeLatestOrderBy = "score"
	EpisodeLatestOrderByDURATION  EpisodeLatestOrderBy = "duration"
)

const EpisodeLatestOrderByDEFAULT = EpisodeLatestOrderByCREATEDAT

var (
	PossibleEpisodeLatestOrderBy = [...]string{
		EpisodeLatestOrderByCREATEDAT.String(),
		EpisodeLatestOrderBySIZE.String(),
		EpisodeLatestOrderBySCORE.String(),
		EpisodeLatestOrderByDURATION.String(),
	}

	ErrUnexpectedEpisodeLatestOrderByValue = apperror.NewError(
		http.StatusBadRequest,
		`unexpected_episode_latest_order_by_value`,
		fmt.Errorf(
			"expected one of the following values: [`%s`]",
			sliceutil.SurroundWith(',', PossibleEpisodeLatestOrderBy),
		),
	)
)

func (a EpisodeLatestOrderBy) Enum() *EpisodeLatestOrderBy { return &a }

func (a EpisodeLatestOrderBy) String() string {
	switch a {
	case EpisodeLatestOrderByCREATEDAT,
		EpisodeLatestOrderBySIZE,
		EpisodeLatestOrderBySCORE,
		EpisodeLatestOrderByDURATION:
		return string(a)
	}
	return string(EpisodeLatestOrderByDEFAULT)
}

func ValidateEpisodeLatestOrderBy(orderBy *string) error {
	if *orderBy == "" {
		*orderBy = EpisodeLatestOrderByDEFAULT.String()
		return nil
	}

	switch *orderBy {
	case EpisodeLatestOrderByCREATEDAT.String(),
		EpisodeLatestOrderBySIZE.String(),
		EpisodeLatestOrderBySCORE.String(),
		EpisodeLatestOrderByDURATION.String():
		return nil
	}
	return ErrUnexpectedEpisodeLatestOrderByValue
}
