package enums

import (
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type AnimeOrderBy string

const (
	AnimeOrderByID             AnimeOrderBy = "id"
	AnimeOrderByTITLE          AnimeOrderBy = "title"
	AnimeOrderBySCORE          AnimeOrderBy = "score"
	AnimeOrderByNEXTAIRINGDATE AnimeOrderBy = "nextAiringDate"
	AnimeOrderBySTARTDATE      AnimeOrderBy = "startDate"
)

const AnimeOrderByDEFAULT = AnimeOrderByID

var (
	PossibleAnimeOrderBy = [...]string{
		AnimeOrderByID.String(),
		AnimeOrderByTITLE.String(),
		AnimeOrderBySCORE.String(),
		AnimeOrderByNEXTAIRINGDATE.String(),
		AnimeOrderBySTARTDATE.String(),
	}

	ErrUnexpectedAnimeOrderByValue = apperror.NewError(
		http.StatusBadRequest,
		`unexpected_anime_order_by_value`,
		fmt.Errorf(
			"expected one of the following values: [`%s`]",
			sliceutil.SurroundWith(',', PossibleAnimeOrderBy),
		),
	)
)

func (a AnimeOrderBy) Enum() *AnimeOrderBy { return &a }

func (a AnimeOrderBy) String() string {
	switch a {
	case AnimeOrderByID,
		AnimeOrderByTITLE,
		AnimeOrderBySCORE,
		AnimeOrderByNEXTAIRINGDATE,
		AnimeOrderBySTARTDATE:
		return string(a)
	}
	return string(AnimeOrderByDEFAULT)
}

func ValidateAnimeOrderBy(orderBy *string) error {
	if *orderBy == "" {
		*orderBy = AnimeOrderByDEFAULT.String()
		return nil
	}

	switch *orderBy {
	case AnimeOrderByID.String(),
		AnimeOrderByTITLE.String(),
		AnimeOrderBySCORE.String(),
		AnimeOrderByNEXTAIRINGDATE.String(),
		AnimeOrderBySTARTDATE.String():
		return nil
	}
	return ErrUnexpectedAnimeOrderByValue
}
