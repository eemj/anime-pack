package enums

import (
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type OrderByDirection string

const (
	OrderByDirectionASCENDING  OrderByDirection = "asc"
	OrderByDirectionDESCENDING OrderByDirection = "desc"
)

const OrderByDirectionDEFAULT = OrderByDirectionDESCENDING

var (
	PossibleOrderByDirection = [...]string{
		OrderByDirectionASCENDING.String(),
		OrderByDirectionDESCENDING.String(),
	}

	ErrUnexpectedOrderByDirectionValue = apperror.NewError(
		http.StatusBadRequest,
		`unexpected_order_by_direction_value`,
		fmt.Errorf(
			"expected one of the following values: [`%s`]",
			sliceutil.SurroundWith(',', PossibleOrderByDirection),
		),
	)
)

func (o OrderByDirection) Enum() *OrderByDirection { return &o }

func (o OrderByDirection) String() string {
	switch o {
	case OrderByDirectionASCENDING, OrderByDirectionDESCENDING:
		return string(o)
	}
	return string(OrderByDirectionDEFAULT)
}

func (o OrderByDirection) IsDescending() bool {
	return o == OrderByDirectionDESCENDING
}

func ValidateOrderByDirection(direction *string) error {
	if *direction == "" {
		*direction = OrderByDirectionDESCENDING.String()
		return nil
	}

	switch *direction {
	case OrderByDirectionASCENDING.String(),
		OrderByDirectionDESCENDING.String():
		return nil
	}
	return ErrUnexpectedOrderByDirectionValue
}

func OrderByDirectionIsDescending(direction string) bool {
	return OrderByDirection(direction).IsDescending()
}
