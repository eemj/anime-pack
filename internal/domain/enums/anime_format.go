package enums

import "fmt"

type AnimeFormat string

const (
	AnimeFormatUNDEFINED AnimeFormat = "UNDEFINED"
	AnimeFormatTV        AnimeFormat = "TV"
	AnimeFormatTVSHORT   AnimeFormat = "TV_SHORT"
	AnimeFormatOVA       AnimeFormat = "OVA"
	AnimeFormatMOVIE     AnimeFormat = "MOVIE"
	AnimeFormatSPECIAL   AnimeFormat = "SPECIAL"
	AnimeFormatONA       AnimeFormat = "ONA"
	AnimeFormatMUSIC     AnimeFormat = "MUSIC"
)

func (e AnimeFormat) Enum() *AnimeFormat { return &e }

func (e *AnimeFormat) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AnimeFormat(s)
	case string:
		*e = AnimeFormat(s)
	default:
		return fmt.Errorf("unsupported scan type for AnimeFormat: %T", src)
	}
	return nil
}

type (
	AnimeFormats            []*AnimeFormat
	AnimeFormatsSortByValue AnimeFormats
)

func (a AnimeFormatsSortByValue) Len() int           { return len(a) }
func (a AnimeFormatsSortByValue) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnimeFormatsSortByValue) Less(i, j int) bool { return *a[i] < *a[j] }
