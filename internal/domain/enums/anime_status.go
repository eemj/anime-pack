package enums

import "fmt"

type AnimeStatus string

const (
	AnimeStatusUNDEFINED      AnimeStatus = "UNDEFINED"
	AnimeStatusFINISHED       AnimeStatus = "FINISHED"
	AnimeStatusRELEASING      AnimeStatus = "RELEASING"
	AnimeStatusNOTYETRELEASED AnimeStatus = "NOT_YET_RELEASED"
	AnimeStatusCANCELLED      AnimeStatus = "CANCELLED"
	AnimeStatusHIATUS         AnimeStatus = "HIATUS"
)

func (e AnimeStatus) Enum() *AnimeStatus { return &e }

func (e *AnimeStatus) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AnimeStatus(s)
	case string:
		*e = AnimeStatus(s)
	default:
		return fmt.Errorf("unsupported scan type for AnimeStatus: %T", src)
	}
	return nil
}

type (
	AnimeStatuses            []*AnimeStatus
	AnimeStatusesSortByValue AnimeStatuses
)

func (a AnimeStatusesSortByValue) Len() int           { return len(a) }
func (a AnimeStatusesSortByValue) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnimeStatusesSortByValue) Less(i, j int) bool { return *a[i] < *a[j] }
