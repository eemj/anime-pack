package enums

import "fmt"

type AnimeSeason string

const (
	AnimeSeasonUNDEFINED AnimeSeason = "UNDEFINED"
	AnimeSeasonWINTER    AnimeSeason = "WINTER"
	AnimeSeasonFALL      AnimeSeason = "FALL"
	AnimeSeasonSUMMER    AnimeSeason = "SUMMER"
	AnimeSeasonSPRING    AnimeSeason = "SPRING"
)

func (season AnimeSeason) Index() int {
	switch season {
	case AnimeSeasonSPRING:
		return 1
	case AnimeSeasonSUMMER:
		return 2
	case AnimeSeasonFALL:
		return 3
	case AnimeSeasonWINTER:
		return 4
	}
	return -1
}

func (e AnimeSeason) Enum() *AnimeSeason { return &e }

func (e *AnimeSeason) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = AnimeSeason(s)
	case string:
		*e = AnimeSeason(s)
	default:
		return fmt.Errorf("unsupported scan type for AnimeSeason: %T", src)
	}
	return nil
}

type (
	AnimeSeasons             []*AnimeSeason
	AnimeSeasonsSortBySeason AnimeSeasons
)

func (a AnimeSeasonsSortBySeason) Len() int           { return len(a) }
func (a AnimeSeasonsSortBySeason) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnimeSeasonsSortBySeason) Less(i, j int) bool { return a[i].Index() < a[j].Index() }
