package enums

import (
	"fmt"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type TitleOrderBy string

const (
	TitleOrderByID        TitleOrderBy = "id"
	TitleOrderByCREATEDAT TitleOrderBy = "createdAt"
)

const TitleOrderByDEFAULT = TitleOrderByCREATEDAT

var (
	PossibleTitleOrderBy = [...]string{
		TitleOrderByID.String(),
		TitleOrderByCREATEDAT.String(),
	}

	ErrUnexpectedTitleOrderByValue = apperror.NewError(
		http.StatusBadRequest,
		`unexpected_title_order_by_value`,
		fmt.Errorf(
			"expected one of the following values: [`%s`]",
			sliceutil.SurroundWith(',', PossibleTitleOrderBy),
		),
	)
)

func (a TitleOrderBy) Enum() *TitleOrderBy { return &a }

func (a TitleOrderBy) String() string {
	switch a {
	case TitleOrderByID, TitleOrderByCREATEDAT:
		return string(a)
	}
	return TitleOrderByDEFAULT.String()
}

func ValidateTitleOrderBy(orderBy *string) error {
	if *orderBy == "" {
		*orderBy = TitleOrderByDEFAULT.String()
		return nil
	}

	switch *orderBy {
	case TitleOrderByID.String(), TitleOrderByCREATEDAT.String():
		return nil
	}
	return ErrUnexpectedTitleOrderByValue
}
