package enums

type XDCCRemoveReason string

const (
	XDCCRemoveReasonNONE             XDCCRemoveReason = "NONE"
	XDCCRemoveReasonGROUPPARSEFAILED XDCCRemoveReason = "GROUP_PARSE_FAILED"
	XDCCRemoveReasonPACKNOTFOUND     XDCCRemoveReason = "PACK_NOT_FOUND"
)

func (s XDCCRemoveReason) String() string          { return string(s) }
func (s XDCCRemoveReason) Enum() *XDCCRemoveReason { return &s }
