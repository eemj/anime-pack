package domain

import (
	"crypto/sha1"
	"encoding/hex"
	"strconv"
	"strings"
)

// zeroAnimeBase is a zero value for AnimeBase
var ZeroAnimeBase AnimeBase

type AnimeBase struct {
	ID int64 `json:"id" patternizer:"-"`

	Title        string `json:"title" patternizer:"Title"`
	Episode      string `json:"episode" patternizer:"Episode"`
	Height       int    `json:"height" patternizer:"Height"`
	ReleaseGroup string `json:"releaseGroup" patternizer:"ReleaseGroup"`
}

func (a AnimeBase) IsZero() bool { return a == ZeroAnimeBase }

func (a AnimeBase) Hash() *string {
	if a.IsZero() {
		return nil
	}

	hasher := sha1.New()
	hasher.Write([]byte(a.String()))
	encoded := hex.EncodeToString(hasher.Sum(nil))

	return &encoded
}

func (a AnimeBase) String() string {
	if a.IsZero() {
		return ""
	}

	var sb strings.Builder
	sb.WriteRune('`')
	if a.ReleaseGroup != "" {
		sb.WriteString("[")
		sb.WriteString(a.ReleaseGroup)
		sb.WriteString("] ")
	}
	sb.WriteString(a.Title)
	if a.Episode != "" {
		sb.WriteString(" - ")
		sb.WriteString(a.Episode)
	}
	if a.Height > 0 {
		sb.WriteString(" [")
		sb.WriteString(strconv.Itoa(a.Height))
		sb.WriteString("p]")
	}
	sb.WriteRune('`')
	if a.ID > 0 {
		sb.WriteString(" {id:")
		sb.WriteString(strconv.FormatInt(a.ID, 10))
		sb.WriteRune('}')
	}
	return sb.String()
}

// EscapeTitle ensures that the filename abides the NTFS file naming scheme
// where special characters aren't allowed.
// More information can be found here: https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
func EscapeTitle(title string) string {
	var sb strings.Builder
	for _, char := range title {
		switch char {
		// don't do anything, IGNORE
		case '<', '>', ':', '"', '/', '\\', '|', '?', '*':
		case '\'': // special case
			sb.WriteRune('_')
		default:
			sb.WriteRune(char)
		}
	}
	return sb.String()
}

// Escape ensures that the filename abides the NTFS file naming scheme
// where special characters aren't allowed.
// More information can be found here: https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
func (a AnimeBase) Escape() (out AnimeBase) {
	return AnimeBase{
		Episode:      a.Episode,
		Height:       a.Height,
		ReleaseGroup: a.ReleaseGroup,
		Title:        EscapeTitle(a.Title),
	}
}

// Equal determines if the provided element matches.
func (a AnimeBase) Equal(anime AnimeBase) bool {
	return a.Title == anime.Title &&
		a.ReleaseGroup == anime.ReleaseGroup &&
		a.Height == anime.Height &&
		a.Episode == anime.Episode
}
