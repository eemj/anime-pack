package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type AnimeIndexFlat struct {
	AnimeID             int64
	AnimeIDMal          *int64
	AnimeStartDate      *time.Time
	AnimeEndDate        *time.Time
	AnimeScore          int64
	AnimeDescription    string
	AnimeTitleRomaji    *string
	AnimeTitleEnglish   *string
	AnimeTitleNative    *string
	AnimeTitle          string
	AnimePosterID       int64
	AnimeBannerID       *int64
	AnimeColour         *string
	AnimeYear           *int64
	AnimeNextAiringDate *time.Time
	AnimeCreatedAt      time.Time
	AnimeUpdatedAt      time.Time
	AnimeStatus         enums.AnimeStatus
	AnimeFormat         enums.AnimeFormat
	AnimeSeason         *enums.AnimeSeason
	PosterName          string
	PosterHash          string
	PosterUri           string
	BannerName          *string
	BannerHash          *string
	BannerUri           *string
}

func (a AnimeIndexFlat) Unflatten() *Anime {
	element := &Anime{
		ID:             a.AnimeID,
		IDMal:          a.AnimeIDMal,
		StartDate:      a.AnimeStartDate,
		EndDate:        a.AnimeEndDate,
		Score:          a.AnimeScore,
		Description:    a.AnimeDescription,
		TitleRomaji:    a.AnimeTitleRomaji,
		TitleEnglish:   a.AnimeTitleEnglish,
		TitleNative:    a.AnimeTitleNative,
		Title:          a.AnimeTitle,
		PosterID:       a.AnimePosterID,
		BannerID:       a.AnimeBannerID,
		Colour:         a.AnimeColour,
		Year:           a.AnimeYear,
		NextAiringDate: a.AnimeNextAiringDate,
		CreatedAt:      a.AnimeCreatedAt,
		UpdatedAt:      a.AnimeUpdatedAt,
		Status:         a.AnimeStatus,
		Format:         a.AnimeFormat,
		Season:         a.AnimeSeason,
		Poster: Poster{
			ID:   a.AnimePosterID,
			Hash: a.PosterHash,
			Uri:  a.PosterUri,
			Name: path.PrefixifyImagePoster(a.PosterName),
		},
		Genres:  make([]*Genre, 0),
		Studios: make([]*Studio, 0),
	}
	if a.AnimeBannerID != nil {
		element.Banner = &Banner{
			ID:   *a.AnimeBannerID,
			Hash: *a.BannerHash,
			Uri:  *a.BannerUri,
			Name: path.PrefixifyImageBanner(*a.BannerName),
		}
	}
	return element
}
