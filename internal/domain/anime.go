package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type Anime struct {
	ID             int64              `json:"id,omitempty"`
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          int64              `json:"score,omitempty"`
	Description    string             `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          string             `json:"title,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Year           *int64             `json:"year,omitempty"`
	NextAiringDate *time.Time         `json:"nextAiringDate,omitempty"`
	Status         enums.AnimeStatus  `json:"status,omitempty"`
	Format         enums.AnimeFormat  `json:"format,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`

	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`

	PosterID    int64        `json:"-"`
	BannerID    *int64       `json:"-"`
	Poster      Poster       `json:"poster,omitempty"`
	Banner      *Banner      `json:"banner,omitempty"`
	Genres      []*Genre     `json:"genres,omitempty"`
	Studios     []*Studio    `json:"studios,omitempty"`
	Preferences *Preferences `json:"preferences,omitempty"`
}

type AnimeItems []*Anime

func (items AnimeItems) Unprefixify() {
	for _, item := range items {
		item.Poster.Name = path.Unprefixify(item.Poster.Name)
		if item.Banner != nil {
			item.Banner.Name = path.Unprefixify(item.Banner.Name)
		}
	}
}

func (items AnimeItems) Prefixify() {
	for _, item := range items {
		item.Poster.Name = path.PrefixifyImagePoster(item.Poster.Name)
		if item.Banner != nil {
			item.Banner.Name = path.PrefixifyImageBanner(item.Banner.Name)
		}
	}
}

type AnimeFlatItems []AnimeFlat

func (items AnimeFlatItems) Unflatten() AnimeItems {
	animes := make(AnimeItems, 0)
	for _, item := range items {
		var foundAnime *Anime
		for _, anime := range animes {
			if anime.ID == item.AnimeID {
				foundAnime = anime
				break
			}
		}

		if foundAnime == nil {
			foundAnime = item.Unflatten()
			animes = append(animes, foundAnime)
		} else {
			if item.GenreID != nil {
				var foundGenre bool
				for _, genre := range foundAnime.Genres {
					if genre.ID == *item.GenreID {
						foundGenre = true
						break
					}
				}
				if !foundGenre {
					foundAnime.Genres = append(foundAnime.Genres, &Genre{
						ID:   *item.GenreID,
						Name: *item.GenreName,
					})
				}
			}

			if item.StudioID != nil {
				var foundStudio bool
				for _, studio := range foundAnime.Studios {
					if studio.ID == *item.StudioID {
						foundStudio = true
						break
					}
				}
				if !foundStudio {
					foundAnime.Studios = append(foundAnime.Studios, &Studio{
						ID:   *item.StudioID,
						Name: *item.StudioName,
					})
				}
			}
		}
	}
	return animes
}

type AnimeFlat struct {
	AnimeID                       int64
	AnimeIDMal                    *int64
	AnimeStartDate                *time.Time
	AnimeEndDate                  *time.Time
	AnimeScore                    int64
	AnimeDescription              string
	AnimeTitleRomaji              *string
	AnimeTitleEnglish             *string
	AnimeTitleNative              *string
	AnimeTitle                    string
	AnimePosterID                 int64
	AnimeBannerID                 *int64
	AnimeColour                   *string
	AnimeYear                     *int64
	AnimeNextAiringDate           *time.Time
	AnimeCreatedAt                time.Time
	AnimeUpdatedAt                time.Time
	AnimeStatus                   enums.AnimeStatus
	AnimeFormat                   enums.AnimeFormat
	AnimeSeason                   *enums.AnimeSeason
	PosterName                    string
	PosterHash                    string
	PosterUri                     string
	BannerName                    *string
	BannerHash                    *string
	BannerUri                     *string
	PreferencesAnimeID            *int64
	PreferencesFavourite          *bool
	PreferencesAutomaticDownloads *bool
	PreferencesPerformChecksum    *bool
	GenreID                       *int64
	GenreName                     *string
	StudioID                      *int64
	StudioName                    *string
}

func (a AnimeFlat) Unflatten() *Anime {
	anime := &Anime{
		ID:             a.AnimeID,
		IDMal:          a.AnimeIDMal,
		StartDate:      a.AnimeStartDate,
		EndDate:        a.AnimeEndDate,
		Score:          a.AnimeScore,
		Description:    a.AnimeDescription,
		TitleRomaji:    a.AnimeTitleRomaji,
		TitleEnglish:   a.AnimeTitleEnglish,
		TitleNative:    a.AnimeTitleNative,
		Title:          a.AnimeTitle,
		PosterID:       a.AnimePosterID,
		BannerID:       a.AnimeBannerID,
		Colour:         a.AnimeColour,
		Year:           a.AnimeYear,
		NextAiringDate: a.AnimeNextAiringDate,
		CreatedAt:      a.AnimeCreatedAt,
		UpdatedAt:      a.AnimeUpdatedAt,
		Status:         a.AnimeStatus,
		Format:         a.AnimeFormat,
		Season:         a.AnimeSeason,
		Poster: Poster{
			ID:   a.AnimePosterID,
			Hash: a.PosterHash,
			Name: a.PosterName,
			Uri:  a.PosterUri,
		},
		Genres:  make([]*Genre, 0),
		Studios: make([]*Studio, 0),
	}
	if a.AnimeBannerID != nil {
		anime.Banner = &Banner{
			ID:   *a.AnimeBannerID,
			Name: *a.BannerName,
			Hash: *a.BannerHash,
			Uri:  *a.BannerUri,
		}
	}
	if a.PreferencesAnimeID != nil {
		anime.Preferences = &Preferences{
			AnimeID:            a.AnimeID,
			Favourite:          a.PreferencesFavourite,
			AutomaticDownloads: a.PreferencesAutomaticDownloads,
			PerformChecksum:    a.PreferencesPerformChecksum,
		}
	}
	if a.GenreID != nil {
		anime.Genres = append(anime.Genres, &Genre{
			ID:   *a.GenreID,
			Name: *a.GenreName,
		})
	}
	if a.StudioID != nil {
		anime.Studios = append(anime.Studios, &Studio{
			ID:   *a.StudioID,
			Name: *a.StudioName,
		})
	}
	return anime
}
