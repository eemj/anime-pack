package domain

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

type TitleBaseKey struct {
	Name         string `json:"name"`
	SeasonNumber string `json:"seasonNumber"`
	Year         int    `json:"year"`
}

func NewTitleBaseKey(name string, seasonNumber *string, year *int) TitleBaseKey {
	titleBase := TitleBaseKey{Name: name}
	if seasonNumber != nil && *seasonNumber != "" {
		titleBase.SeasonNumber = *seasonNumber
	}
	if year != nil && *year > 0 {
		titleBase.Year = *year
	}
	return titleBase
}

type TitleBase struct {
	Name         string  `json:"name,omitempty"`
	SeasonNumber *string `json:"seasonNumber,omitempty"`
	Year         *int    `json:"year,omitempty"`
}

func (p TitleBase) String() string {
	var sb strings.Builder
	sb.WriteString(p.Name)
	if p.Year != nil && *p.Year > 0 {
		sb.WriteString(" (")
		sb.WriteString(strconv.Itoa(*p.Year))
		sb.WriteRune(')')
	}
	if p.SeasonNumber != nil && *p.SeasonNumber != "" {
		sb.WriteString(" S")
		sb.WriteString(*p.SeasonNumber)
	}
	return sb.String()
}

type Title struct {
	ID           int64   `json:"id,omitempty"`
	Name         string  `json:"name,omitempty"`
	Reviewed     bool    `json:"reviewed,omitempty"`
	SeasonNumber *string `json:"seasonNumber,omitempty"`
	Year         *int    `json:"year,omitempty"`

	IsDeleted bool       `json:"-"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
}

func (t Title) Names() []string {
	names := []string{t.Name}

	if t.Year != nil && *t.Year > 0 {
		names = append(names, fmt.Sprintf("%s %d", t.Name, *t.Year))
	}

	if t.SeasonNumber == nil || (t.SeasonNumber != nil && *t.SeasonNumber == "") {
		return names
	}

	seasonNumber, err := strconv.Atoi(*t.SeasonNumber)
	if err != nil {
		return names
	}

	names = []string{
		fmt.Sprintf("%s %d", t.Name, seasonNumber),
		fmt.Sprintf("%s %s Season", t.Name, humanize.Ordinal(seasonNumber)),
		fmt.Sprintf("%s S%d", t.Name, seasonNumber),
		fmt.Sprintf("%s Season %d", t.Name, seasonNumber),
	}

	return names
}
