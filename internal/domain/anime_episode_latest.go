package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
)

type AnimeEpisodeLatest struct {
	ID            int64                             `json:"id,omitempty"`
	Title         string                            `json:"title,omitempty"`
	TitleEnglish  *string                           `json:"titleEnglish,omitempty"`
	TitleNative   *string                           `json:"titleNative,omitempty"`
	TitleRomaji   *string                           `json:"titleRomaji,omitempty"`
	Score         int64                             `json:"score,omitempty"`
	Poster        Poster                            `json:"poster,omitempty"`
	Episode       Episode                           `json:"episode,omitempty"`
	ReleaseGroups []*AnimeEpisodeLatestReleaseGroup `json:"releaseGroups,omitempty"`
}

type AnimeEpisodeLatestItems []*AnimeEpisodeLatest

type AnimeEpisodeLatestReleaseGroup struct {
	EpisodeID int64                                    `json:"episodeID,omitempty"`
	ID        int64                                    `json:"id,omitempty"`
	Name      string                                   `json:"name,omitempty"`
	Qualities []*AnimeEpisodeLatestReleaseGroupQuality `json:"qualities,omitempty"`
}

type AnimeEpisodeLatestReleaseGroupQuality struct {
	ID        int64      `json:"id,omitempty"`
	Height    int64      `json:"height,omitempty"`
	Video     *Video     `json:"video,omitempty"`
	Thumbnail *Thumbnail `json:"thumbnail,omitempty"`
}

type AnimeEpisodeLatestFlat struct {
	EpisodeID          int64
	EpisodeName        string
	TitleEpisodeID     int64
	AnimeID            int64
	AnimeTitle         string
	AnimeTitleEnglish  *string
	AnimeTitleNative   *string
	AnimeTitleRomaji   *string
	AnimeScore         int64
	PosterID           int64
	PosterHash         string
	PosterName         string
	VideoPath          string
	VideoCrc32         string
	VideoSize          int64
	VideoDuration      float64
	VideoCreatedAt     time.Time
	ThumbnailID        int64
	ThumbnailHash      string
	ThumbnailName      string
	ReleaseGroupID     int64
	ReleaseGroupName   string
	XdccQualityID      *int64
	XdccQualityHeight  *int64
	VideoQualityID     *int64
	VideoQualityHeight *int64
}

type AnimeEpisodeLatestFlatItems []AnimeEpisodeLatestFlat

func (items AnimeEpisodeLatestFlatItems) Unflatten() []*AnimeEpisodeLatest {
	result := make([]*AnimeEpisodeLatest, len(items))

	for index, item := range items {
		var (
			qualityID     int64
			qualityHeight int64
		)
		if item.VideoQualityID != nil && item.VideoQualityHeight != nil {
			qualityID, qualityHeight = *item.VideoQualityID, *item.VideoQualityHeight
		} else if item.XdccQualityID != nil && item.XdccQualityHeight != nil {
			qualityID, qualityHeight = *item.XdccQualityID, *item.XdccQualityHeight
		}

		episode := &AnimeEpisodeLatest{
			ID:           item.AnimeID,
			Title:        item.AnimeTitle,
			TitleEnglish: item.AnimeTitleEnglish,
			TitleNative:  item.AnimeTitleNative,
			TitleRomaji:  item.AnimeTitleRomaji,
			Score:        item.AnimeScore,
			Poster: Poster{
				ID:   item.PosterID,
				Hash: item.PosterHash,
				Name: path.PrefixifyImagePoster(item.PosterName),
			},
			Episode: Episode{
				ID:   item.EpisodeID,
				Name: item.EpisodeName,
			},
			// FIXME(eemj): This is wrong, an episode latest can have multiple
			// release groups but we're only setting 1..
			ReleaseGroups: []*AnimeEpisodeLatestReleaseGroup{{
				EpisodeID: item.TitleEpisodeID,
				ID:        item.ReleaseGroupID,
				Name:      item.ReleaseGroupName,
				Qualities: []*AnimeEpisodeLatestReleaseGroupQuality{{
					ID:     qualityID,
					Height: qualityHeight,
					Video: &Video{
						Size:      item.VideoSize,
						Duration:  item.VideoDuration,
						CreatedAt: item.VideoCreatedAt,
						CRC32:     item.VideoCrc32,
						Path:      path.PrefixifyVideo(item.VideoPath),
					},
					Thumbnail: &Thumbnail{
						ID:   item.ThumbnailID,
						Hash: item.ThumbnailHash,
						Name: path.PrefixifyImageThumbnail(item.ThumbnailName),
					},
				}},
			}},
		}

		result[index] = episode
	}

	return result
}
