package domain

type UnusedImages struct {
	UnusedPoster    int64
	UnusedBanner    int64
	UnusedThumbnail int64
}

func (u UnusedImages) AnyUnused() bool {
	return u.UnusedPoster == 0 &&
		u.UnusedBanner == 0 &&
		u.UnusedThumbnail == 0
}
