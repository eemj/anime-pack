package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/maps"
)

type Video struct {
	CreatedAt time.Time `json:"createdAt,omitempty"`
	Path      string    `json:"path,omitempty"`
	CRC32     string    `json:"crc32,omitempty"`
	ID        int64     `json:"id,omitempty"`
	Duration  float64   `json:"duration,omitempty"`
	Size      int64     `json:"size,omitempty"`

	QualityID   *int64 `json:"-"`
	ThumbnailID int64  `json:"-"`
	XDCCID      int64  `json:"-"`

	Thumbnail *Thumbnail `json:"thumbnail,omitempty"`
	XDCC      *XDCC      `json:"xdcc,omitempty"`
	Quality   *Quality   `json:"quality,omitempty"`
}

type VideoEntry struct {
	Anime       AnimeBase   `json:"anime,omitempty"`
	Video       Video       `json:"video,omitempty"`
	Preferences Preferences `json:"preferences,omitempty"`
	VideoPath   string      `json:"videoPath,omitempty"`
}

type VideoEntryFlatItem struct {
	VideoID                       int64
	VideoPath                     string
	VideoDuration                 float64
	VideoSize                     int64
	VideoCrc32                    string
	VideoThumbnailID              int64
	VideoXdccID                   int64
	VideoQualityID                *int64
	VideoCreatedAt                time.Time
	AnimeID                       int64
	AnimeTitle                    string
	PreferencesFavourite          *bool
	PreferencesAutomaticDownloads *bool
	PreferencesPerformChecksum    *bool
	EpisodeName                   string
	QualityHeight                 int64
	ReleaseGroupName              string
}

func (h VideoEntryFlatItem) Unflatten() *VideoEntry {
	return &VideoEntry{
		Anime: AnimeBase{
			ID:           h.AnimeID,
			Title:        h.AnimeTitle,
			Episode:      h.EpisodeName,
			Height:       int(h.QualityHeight),
			ReleaseGroup: h.ReleaseGroupName,
		},
		Preferences: Preferences{
			AnimeID:            h.AnimeID,
			Favourite:          h.PreferencesFavourite,
			AutomaticDownloads: h.PreferencesAutomaticDownloads,
			PerformChecksum:    h.PreferencesPerformChecksum,
		},
		Video: Video{
			ID:          h.VideoID,
			Path:        h.VideoPath,
			Duration:    h.VideoDuration,
			Size:        h.VideoSize,
			CRC32:       h.VideoCrc32,
			ThumbnailID: h.VideoThumbnailID,
			XDCCID:      h.VideoXdccID,
			QualityID:   h.VideoQualityID,
		},
	}
}

var ZeroVideoEntries = VideoEntries(maps.NewGroupMap[string, *VideoEntry]())

type VideoEntries maps.GroupMap[string, *VideoEntry]

type VideoEntryFlat []VideoEntryFlatItem

func (items VideoEntryFlat) Unflatten() VideoEntries {
	result := maps.NewGroupMap[string, *VideoEntry]()
	for _, item := range items {
		newItem := item.Unflatten()
		result.Add(newItem.Anime.Title, newItem)
	}
	return result
}
