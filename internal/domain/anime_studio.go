package domain

type AnimeStudio struct {
	AnimeID  int64
	StudioID int64

	Anime  Anime
	Studio Studio
}

type AnimeStudioFlat struct {
	AnimeID    int64
	StudioID   int64
	StudioName string
}

func (a AnimeStudioFlat) Unflatten() *AnimeStudio {
	return &AnimeStudio{
		AnimeID:  a.AnimeID,
		StudioID: a.StudioID,
		Studio: Studio{
			ID:   a.StudioID,
			Name: a.StudioName,
		},
	}
}
