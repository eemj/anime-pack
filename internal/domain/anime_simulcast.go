package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type AnimeSimulcastFlat struct {
	AnimeID           int64
	AnimeIDMal        *int64
	AnimeStartDate    *time.Time
	AnimeEndDate      *time.Time
	AnimeScore        int64
	AnimeDescription  string
	AnimeTitleRomaji  *string
	AnimeTitleEnglish *string
	AnimeTitleNative  *string
	AnimeTitle        string
	AnimeStatus       enums.AnimeStatus
	AnimeFormat       enums.AnimeFormat
	AnimePosterID     int64
	AnimeBannerID     *int64
	AnimeSeason       *enums.AnimeSeason
	AnimeYear         *int64
	AnimeColour       *string
	PosterName        string
	PosterHash        string
	BannerName        *string
	BannerHash        *string
	SimulcastHour     int16
	SimulcastMinute   int16
	SimulcastSecond   int16
	SimulcastWeekday  int64
	GenreID           *int64
	GenreName         *string
	StudioID          *int64
	StudioName        *string
}

func (a AnimeSimulcastFlat) calculateNextAiringDate(currentTime time.Time) time.Time {
	// Calculate the difference between the current weekday and the desired weekday
	weekdayDiff := int(a.SimulcastWeekday) - int(currentTime.Weekday())
	if weekdayDiff < 0 {
		weekdayDiff += 7 // Add 7 days if the desired weekday has already passed in the current week
	}

	// Calculate the duration until the desired time on the next occurrence of the desired weekday
	nextDate := currentTime.AddDate(0, 0, weekdayDiff)
	nextTime := time.Date(
		nextDate.Year(), nextDate.Month(), nextDate.Day(),
		int(a.SimulcastHour), int(a.SimulcastMinute), int(a.SimulcastSecond), 0,
		nextDate.Location(),
	)

	return nextTime
}

func (a AnimeSimulcastFlat) Unflatten(currentTime time.Time) *AnimeSimulcast {
	anime := &AnimeSimulcast{
		ID:           a.AnimeID,
		IDMal:        a.AnimeIDMal,
		StartDate:    a.AnimeStartDate,
		EndDate:      a.AnimeEndDate,
		Score:        a.AnimeScore,
		Description:  a.AnimeDescription,
		TitleRomaji:  a.AnimeTitleRomaji,
		TitleEnglish: a.AnimeTitleEnglish,
		TitleNative:  a.AnimeTitleNative,
		Title:        a.AnimeTitle,
		Colour:       a.AnimeColour,
		SeasonYear:   a.AnimeYear,
		Status:       a.AnimeStatus,
		Format:       a.AnimeFormat,
		Season:       a.AnimeSeason,
		Poster: Poster{
			ID:   a.AnimePosterID,
			Hash: a.PosterHash,
			Name: a.PosterName,
		},
		Genres:         make([]*Genre, 0),
		Studios:        make([]*Studio, 0),
		NextAiringDate: a.calculateNextAiringDate(currentTime),
	}
	if a.AnimeBannerID != nil {
		anime.Banner = &Banner{
			ID:   *a.AnimeBannerID,
			Name: *a.BannerName,
			Hash: *a.BannerHash,
		}
	}
	if a.GenreID != nil {
		anime.Genres = append(anime.Genres, &Genre{
			ID:   *a.GenreID,
			Name: *a.GenreName,
		})
	}
	if a.StudioID != nil {
		anime.Studios = append(anime.Studios, &Studio{
			ID:   *a.StudioID,
			Name: *a.StudioName,
		})
	}
	return anime
}

type AnimeSimulcastFlatItems []AnimeSimulcastFlat

func (items AnimeSimulcastFlatItems) Unflatten() AnimeSimulcasts {
	currentTime := time.Now().UTC()

	animes := make(AnimeSimulcasts, 0)
	for _, item := range items {
		var foundAnime *AnimeSimulcast
		for _, anime := range animes {
			if anime.ID == item.AnimeID {
				foundAnime = anime
				break
			}
		}

		if foundAnime == nil {
			foundAnime = item.Unflatten(currentTime)
			animes = append(animes, foundAnime)
		} else {
			if item.GenreID != nil {
				var foundGenre bool
				for _, genre := range foundAnime.Genres {
					if genre.ID == *item.GenreID {
						foundGenre = true
						break
					}
				}
				if !foundGenre {
					foundAnime.Genres = append(foundAnime.Genres, &Genre{
						ID:   *item.GenreID,
						Name: *item.GenreName,
					})
				}
			}

			if item.StudioID != nil {
				var foundStudio bool
				for _, studio := range foundAnime.Studios {
					if studio.ID == *item.StudioID {
						foundStudio = true
						break
					}
				}
				if !foundStudio {
					foundAnime.Studios = append(foundAnime.Studios, &Studio{
						ID:   *item.StudioID,
						Name: *item.StudioName,
					})
				}
			}
		}
	}
	return animes
}

type AnimeSimulcast struct {
	ID             int64              `json:"id,omitempty"`
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          int64              `json:"score,omitempty"`
	Description    string             `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          string             `json:"title,omitempty"`
	Status         enums.AnimeStatus  `json:"status,omitempty"`
	Format         enums.AnimeFormat  `json:"format,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`
	SeasonYear     *int64             `json:"seasonYear,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Poster         Poster             `json:"poster,omitempty"`
	Banner         *Banner            `json:"banner,omitempty"`
	Genres         []*Genre           `json:"genres,omitempty"`
	Studios        []*Studio          `json:"studios,omitempty"`
	Weekday        time.Weekday       `json:"weekday,omitempty"`
	NextAiringDate time.Time          `json:"nextAiringDate,omitempty"`
}

type AnimeSimulcasts []*AnimeSimulcast

func (items AnimeSimulcasts) Unprefixify() {
	for _, item := range items {
		item.Poster.Name = path.Unprefixify(item.Poster.Name)
		if item.Banner != nil {
			item.Banner.Name = path.Unprefixify(item.Banner.Name)
		}
	}
}

func (items AnimeSimulcasts) Prefixify() {
	for _, item := range items {
		item.Poster.Name = path.PrefixifyImagePoster(item.Poster.Name)
		if item.Banner != nil {
			item.Banner.Name = path.PrefixifyImageBanner(item.Banner.Name)
		}
	}
}

type AnimeSimulcastsSortByNextAiringDate AnimeSimulcasts

func (a AnimeSimulcastsSortByNextAiringDate) Len() int      { return len(a) }
func (a AnimeSimulcastsSortByNextAiringDate) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a AnimeSimulcastsSortByNextAiringDate) Less(i, j int) bool {
	return a[i].NextAiringDate.Before(a[j].NextAiringDate)
}
