package domain

import "gitlab.com/eemj/anime-pack/internal/common/path"

type BannerReview struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
}

type Banner struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
	Uri  string `json:"uri,omitempty"`
}

func (p *Banner) Review() *BannerReview {
	return &BannerReview{
		ID:   p.ID,
		Hash: p.Hash,
		Name: path.PrefixifyImageBanner(p.Name),
	}
}
