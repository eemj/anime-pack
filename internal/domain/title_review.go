package domain

import (
	"sort"
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type TitleReview struct {
	ID           int64   `json:"id,omitempty"`
	Name         string  `json:"name,omitempty"`
	Reviewed     bool    `json:"reviewed,omitempty"`
	SeasonNumber *string `json:"seasonNumber,omitempty"`
	Year         *int    `json:"year,omitempty"`

	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
	IsDeleted bool       `json:"-"`

	Filenames []string   `json:"filenames,omitempty"`
	Anime     AnimeItems `json:"anime,omitempty"`
}

type TitleReviewItems []*TitleReview

type TitleReviewFlat struct {
	TitleID             int64
	TitleName           string
	TitleReviewed       bool
	TitleCreatedAt      time.Time
	TitleUpdatedAt      time.Time
	TitleDeletedAt      *time.Time
	TitleIsDeleted      bool
	TitleSeasonNumber   *string
	TitleYear           *int
	AnimeID             *int64
	AnimeIDMal          *int64
	AnimeStartDate      *time.Time
	AnimeEndDate        *time.Time
	AnimeScore          *int64
	AnimeDescription    *string
	AnimeTitleRomaji    *string
	AnimeTitleEnglish   *string
	AnimeTitleNative    *string
	AnimeTitle          *string
	AnimePosterID       *int64
	AnimeBannerID       *int64
	AnimeColour         *string
	AnimeYear           *int64
	AnimeNextAiringDate *time.Time
	AnimeCreatedAt      *time.Time
	AnimeUpdatedAt      *time.Time
	AnimeStatus         *enums.AnimeStatus
	AnimeFormat         *enums.AnimeFormat
	AnimeSeason         *enums.AnimeSeason
	PosterName          *string
	PosterHash          *string
	PosterUri           *string
	BannerName          *string
	BannerHash          *string
	BannerUri           *string
	XdccFilename        *string
}

type TitleReviewFlatItems []TitleReviewFlat

func (a TitleReviewFlatItems) Unflatten() []*TitleReview {
	items := make([]*TitleReview, 0)
	group := make(map[int64]*TitleReview)

	for _, item := range a {
		reviewItem, exists := group[item.TitleID]
		if !exists {
			reviewItem = &TitleReview{
				ID:           item.TitleID,
				Name:         item.TitleName,
				Reviewed:     item.TitleReviewed,
				CreatedAt:    item.TitleCreatedAt,
				UpdatedAt:    item.TitleCreatedAt,
				DeletedAt:    item.TitleDeletedAt,
				IsDeleted:    item.TitleIsDeleted,
				SeasonNumber: item.TitleSeasonNumber,
				Year:         item.TitleYear,
			}
			items = append(items, reviewItem)
		}

		if item.XdccFilename != nil && *item.XdccFilename != "" {
			foundFilename := false
			for _, filename := range reviewItem.Filenames {
				if filename == *item.XdccFilename {
					foundFilename = true
					break
				}
			}
			if !foundFilename {
				reviewItem.Filenames = append(reviewItem.Filenames, *item.XdccFilename)
			}
		}

		if item.AnimeID != nil && *item.AnimeID > 0 {
			foundAnime := false
			for _, anime := range reviewItem.Anime {
				if anime.ID == *item.AnimeID {
					foundAnime = true
					break
				}
			}
			if !foundAnime {
				anime := &Anime{
					ID:           *item.AnimeID,
					IDMal:        item.AnimeIDMal,
					StartDate:    item.AnimeStartDate,
					EndDate:      item.AnimeEndDate,
					Score:        *item.AnimeScore,
					Description:  *item.AnimeDescription,
					TitleRomaji:  item.AnimeTitleRomaji,
					TitleEnglish: item.AnimeTitleEnglish,
					TitleNative:  item.AnimeTitleNative,
					Title:        *item.AnimeTitle,
					PosterID:     *item.AnimePosterID,
					Poster: Poster{
						ID:   *item.AnimePosterID,
						Uri:  *item.PosterUri,
						Hash: *item.PosterHash,
						Name: path.PrefixifyImagePoster(*item.PosterName),
					},
					BannerID:       item.AnimeBannerID,
					Colour:         item.AnimeColour,
					Year:           item.AnimeYear,
					NextAiringDate: item.AnimeNextAiringDate,
					CreatedAt:      *item.AnimeCreatedAt,
					UpdatedAt:      *item.AnimeUpdatedAt,
					Status:         *item.AnimeStatus,
					Format:         *item.AnimeFormat,
					Season:         item.AnimeSeason,
				}
				if item.AnimeBannerID != nil && *item.AnimeBannerID > 0 {
					anime.Banner = &Banner{
						ID:   *item.AnimeBannerID,
						Uri:  *item.BannerUri,
						Hash: *item.BannerUri,
						Name: path.PrefixifyImageBanner(*item.BannerName),
					}
				}
				reviewItem.Anime = append(reviewItem.Anime, anime)
			}
		}

		group[item.TitleID] = reviewItem
	}

	for _, item := range items {
		sort.Strings(item.Filenames)
	}

	return items
}
