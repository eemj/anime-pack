package domain

type Quality struct {
	ID     int64 `json:"id,omitempty"`
	Height int64 `json:"height,omitempty"`
}
