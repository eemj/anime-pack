package domain

import "time"

type Simulcast struct {
	TitleID int64
	Weekday time.Weekday
	Hour    int8
	Minute  int8
	Second  int8
}
