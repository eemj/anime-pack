package domain

import (
	"sort"
	"strconv"
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type AnimeEpisode struct {
	Name          string                    `json:"name,omitempty"`
	CreatedAt     *time.Time                `json:"createdAt,omitempty"`
	ReleaseGroups AnimeEpisodeReleaseGroups `json:"releaseGroups"`
}

type (
	AnimeEpisodes           []*AnimeEpisode
	AnimeEpisodesSortByName AnimeEpisodes
)

func (a AnimeEpisodesSortByName) Len() int      { return len(a) }
func (a AnimeEpisodesSortByName) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a AnimeEpisodesSortByName) Less(i, j int) bool {
	firstName, _ := strconv.ParseFloat(a[i].Name, 64)
	secondName, _ := strconv.ParseFloat(a[j].Name, 64)
	return firstName < secondName
}

type AnimeEpisodeReleaseGroup struct {
	EpisodeID int64                             `json:"episodeID,omitempty"`
	ID        int64                             `json:"id,omitempty"`
	Name      string                            `json:"name,omitempty"`
	Qualities AnimeEpisodeReleaseGroupQualities `json:"qualities"`
}

type AnimeEpisodeReleaseGroups []*AnimeEpisodeReleaseGroup

func (group AnimeEpisodeReleaseGroup) FindByQuality(quality Quality) (*AnimeEpisodeReleaseGroupQuality, bool) {
	for _, q := range group.Qualities {
		if (quality.ID == 0 && quality.Height == 0 && q.ID == nil && q.Height == nil) ||
			(q != nil && q.ID != nil && q.Height != nil && *q.ID == quality.ID && *q.Height == quality.Height) {
			return q, true
		}
	}
	return nil, false
}

type AnimeEpisodeReleaseGroupsSortByName AnimeEpisodeReleaseGroups

func (a AnimeEpisodeReleaseGroupsSortByName) Len() int           { return len(a) }
func (a AnimeEpisodeReleaseGroupsSortByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnimeEpisodeReleaseGroupsSortByName) Less(i, j int) bool { return a[i].Name < a[j].Name }

type AnimeEpisodeReleaseGroupQuality struct {
	ID        *int64     `json:"id,omitempty"`
	Height    *int64     `json:"height,omitempty"`
	Video     *Video     `json:"video,omitempty"`
	Thumbnail *Thumbnail `json:"thumbnail,omitempty"`
}

type (
	AnimeEpisodeReleaseGroupQualities             []*AnimeEpisodeReleaseGroupQuality
	AnimeEpisodeReleaseGroupQualitiesSortByHeight AnimeEpisodeReleaseGroupQualities
)

func (a AnimeEpisodeReleaseGroupQualitiesSortByHeight) Len() int      { return len(a) }
func (a AnimeEpisodeReleaseGroupQualitiesSortByHeight) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a AnimeEpisodeReleaseGroupQualitiesSortByHeight) Less(i, j int) bool {
	var firstHeight, secondHeight int64 = 0, 0
	if a[i].Height != nil {
		firstHeight = *a[i].Height
	} else if a[i].Video != nil && a[i].Video.Quality != nil && a[i].Video.Quality.Height > 0 {
		firstHeight = a[i].Video.Quality.Height
	}
	if a[j].Height != nil {
		secondHeight = *a[j].Height
	} else if a[j].Video != nil && a[j].Video.Quality != nil && a[j].Video.Quality.Height > 0 {
		secondHeight = a[j].Video.Quality.Height
	}
	return firstHeight < secondHeight
}

type AnimeEpisodeFlat struct {
	TitleEpisodeID        int64
	TitleEpisodeCreatedAt time.Time
	EpisodeName           string
	ReleaseGroupID        *int64
	ReleaseGroupName      *string
	XdccQualityID         *int64
	XdccQualityHeight     *int64
	VideoQualityID        *int64
	VideoQualityHeight    *int64
	ThumbnailID           *int64
	ThumbnailName         *string
	ThumbnailHash         *string
	VideoID               *int64
	VideoPath             *string
	VideoCrc32            *string
	VideoDuration         *float64
	VideoSize             *int64
	VideoCreatedAt        *time.Time
	AnimeStatus           enums.AnimeStatus
}

type AnimeEpisodeFlatItems []AnimeEpisodeFlat

func (items AnimeEpisodeFlatItems) Unflatten() AnimeEpisodes {
	result := make(AnimeEpisodes, 0)
	episodes := make(map[string]*AnimeEpisode)
	creation := make(map[int64]*time.Time)

	for _, item := range items {
		episode, exists := episodes[item.EpisodeName]
		if !exists {
			episode = &AnimeEpisode{
				Name:          item.EpisodeName,
				ReleaseGroups: make(AnimeEpisodeReleaseGroups, 0),
			}
			episodes[item.EpisodeName] = episode
		}

		if episode.CreatedAt == nil || item.TitleEpisodeCreatedAt.Before(*episode.CreatedAt) {
			episode.CreatedAt = &item.TitleEpisodeCreatedAt
		}

		if !item.TitleEpisodeCreatedAt.IsZero() {
			creation[item.TitleEpisodeID] = &item.TitleEpisodeCreatedAt
		}

		if item.ReleaseGroupID == nil && item.ReleaseGroupName == nil {
			continue
		}

		var foundReleaseGroup *AnimeEpisodeReleaseGroup
		for _, releaseGroup := range episode.ReleaseGroups {
			if releaseGroup.ID == *item.ReleaseGroupID && releaseGroup.Name == *item.ReleaseGroupName {
				foundReleaseGroup = releaseGroup
				break
			}
		}

		if foundReleaseGroup == nil {
			foundReleaseGroup = &AnimeEpisodeReleaseGroup{
				ID:        *item.ReleaseGroupID,
				Name:      *item.ReleaseGroupName,
				EpisodeID: item.TitleEpisodeID,
				Qualities: make(AnimeEpisodeReleaseGroupQualities, 0),
			}

			episode.ReleaseGroups = append(episode.ReleaseGroups, foundReleaseGroup)
		}

		var quality Quality
		if item.XdccQualityID != nil && item.XdccQualityHeight != nil {
			quality.ID, quality.Height = *item.XdccQualityID, *item.XdccQualityHeight
		}

		groupQuality, exists := foundReleaseGroup.FindByQuality(quality)

		if !exists {
			groupQuality = &AnimeEpisodeReleaseGroupQuality{}
			foundReleaseGroup.Qualities = append(foundReleaseGroup.Qualities, groupQuality)
		}

		if quality.ID > 0 && quality.Height > 0 {
			groupQuality.ID, groupQuality.Height = &quality.ID, &quality.Height
		}

		if groupQuality.Video == nil && item.VideoID != nil {
			groupQuality.Video = &Video{
				ID:        *item.VideoID,
				CRC32:     *item.VideoCrc32,
				Size:      *item.VideoSize,
				Duration:  *item.VideoDuration,
				CreatedAt: *item.VideoCreatedAt,
				QualityID: item.VideoQualityID,
				Quality: &Quality{
					ID:     *item.VideoQualityID,
					Height: *item.VideoQualityHeight,
				},
				Path: path.PrefixifyVideo(*item.VideoPath),
			}
		}

		if groupQuality.Thumbnail == nil && item.ThumbnailID != nil {
			groupQuality.Thumbnail = &Thumbnail{
				ID:   *item.ThumbnailID,
				Hash: *item.ThumbnailHash,
				Name: path.PrefixifyImageThumbnail(*item.ThumbnailName),
			}
		}
	}

	for _, episode := range episodes {
		for _, releaseGroup := range episode.ReleaseGroups {
			sort.Stable(sort.Reverse(AnimeEpisodeReleaseGroupQualitiesSortByHeight(releaseGroup.Qualities)))
		}

		sort.Stable(sort.Reverse(AnimeEpisodeReleaseGroupsSortByName(episode.ReleaseGroups)))

		result = append(result, episode)
	}

	sort.Stable(AnimeEpisodesSortByName(result))

	return result
}
