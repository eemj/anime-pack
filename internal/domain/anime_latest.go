package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/path"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type AnimeLatestFlat struct {
	AnimeID               int64
	AnimeIDMal            *int64
	AnimeTitle            string
	AnimeTitleEnglish     *string
	AnimeTitleNative      *string
	AnimeTitleRomaji      *string
	AnimeColour           *string
	AnimeFormat           enums.AnimeFormat
	AnimeStatus           enums.AnimeStatus
	PosterID              int64
	PosterName            string
	PosterHash            string
	EpisodeName           string
	TitleEpisodeID        int64
	TitleEpisodeCreatedAt time.Time
	PreferencesFavourite  *bool
}

type AnimeLatestFlatItems []AnimeLatestFlat

func (c AnimeLatestFlatItems) Unflatten() []*AnimeLatest {
	result := make([]*AnimeLatest, len(c))
	for index, item := range c {
		result[index] = &AnimeLatest{
			ID:           item.AnimeID,
			IDMal:        item.AnimeIDMal,
			TitleRomaji:  item.AnimeTitleRomaji,
			TitleEnglish: item.AnimeTitleEnglish,
			TitleNative:  item.AnimeTitleNative,
			Title:        item.AnimeTitle,
			Poster: Poster{
				ID:   item.PosterID,
				Hash: item.PosterHash,
				Name: path.PrefixifyImagePoster(item.PosterName),
			},
			Episode: Episode{
				ID:   item.TitleEpisodeID,
				Name: item.EpisodeName,
			},
			Format:    item.AnimeFormat,
			Status:    item.AnimeStatus,
			Colour:    item.AnimeColour,
			CreatedAt: item.TitleEpisodeCreatedAt,
		}
	}
	return result
}

type AnimeLatest struct {
	ID           int64             `json:"id,omitempty"`
	IDMal        *int64            `json:"idMal,omitempty"`
	TitleRomaji  *string           `json:"titleRomaji,omitempty"`
	TitleEnglish *string           `json:"titleEnglish,omitempty"`
	TitleNative  *string           `json:"titleNative,omitempty"`
	Title        string            `json:"title,omitempty"`
	Poster       Poster            `json:"poster,omitempty"`
	Episode      Episode           `json:"episode,omitempty"`
	Format       enums.AnimeFormat `json:"format,omitempty"`
	Status       enums.AnimeStatus `json:"status,omitempty"`
	Colour       *string           `json:"colour,omitempty"`
	CreatedAt    time.Time         `json:"createdAt,omitempty"`
}

type AnimeLatestItems []*AnimeLatest
