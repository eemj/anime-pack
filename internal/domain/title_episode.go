package domain

import "time"

type TitleEpisode struct {
	ID        int64 `json:"id,omitempty"`
	TitleID   int64 `json:"titleID,omitempty"`
	EpisodeID int64 `json:"episodeID,omitempty"`
	IsDeleted bool  `json:"isDeleted,omitempty"`

	CreatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`

	Title   Title   `json:"-"`
	Episode Episode `json:"-"`
}

type TitleEpisodeFlat struct {
	ID                int64
	TitleID           int64
	EpisodeID         int64
	CreatedAt         time.Time
	DeletedAt         *time.Time
	IsDeleted         bool
	TitleName         string
	TitleSeasonNumber *string
	TitleYear         *int
	TitleReviewed     bool
	TitleIsDeleted    bool
	TitleDeletedAt    *time.Time
	EpisodeName       string
}

func (e TitleEpisodeFlat) Unflatten() *TitleEpisode {
	return &TitleEpisode{
		ID:        e.ID,
		TitleID:   e.TitleID,
		EpisodeID: e.EpisodeID,
		CreatedAt: e.CreatedAt,
		DeletedAt: e.DeletedAt,
		IsDeleted: e.IsDeleted,
		Title: Title{
			ID:           e.TitleID,
			Name:         e.TitleName,
			SeasonNumber: e.TitleSeasonNumber,
			Year:         e.TitleYear,
			Reviewed:     e.TitleReviewed,
			DeletedAt:    e.TitleDeletedAt,
			IsDeleted:    e.TitleIsDeleted,
		},
		Episode: Episode{
			ID:   e.EpisodeID,
			Name: e.EpisodeName,
		},
	}
}
