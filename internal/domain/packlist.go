package domain

import "gitlab.com/eemj/anime-pack/pkg/groups/entry"

type Pack struct {
	Pack     int64
	Size     int64
	Bot      string
	Filename string
}

type PackItems []*Pack

type GroupPacks map[TitleBaseKey][]*GroupPack

type GroupPack struct {
	Entry entry.Entry
	Pack  Pack
}

type PacklistSync struct {
	Retrieved int
	Created   int
	Updated   int
	Deleted   int
}

func (s PacklistSync) IsEmpty() bool {
	return s.Created == 0 &&
		s.Updated == 0 &&
		s.Deleted == 0
}
