package domain

type Poster struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Hash string `json:"hash,omitempty"`
	Uri  string `json:"-"`
}
