package domain

type AnimeGenre struct {
	AnimeID int64 `json:"animeID,omitempty"`
	GenreID int64 `json:"genreID,omitempty"`

	Anime Anime `json:"anime,omitempty"`
	Genre Genre `json:"genre,omitempty"`
}

type AnimeGenreFlat struct {
	AnimeID   int64
	GenreID   int64
	GenreName string
}

func (a AnimeGenreFlat) Unflatten() *AnimeGenre {
	return &AnimeGenre{
		AnimeID: a.AnimeID,
		GenreID: a.GenreID,
		Genre: Genre{
			ID:   a.GenreID,
			Name: a.GenreName,
		},
	}
}
