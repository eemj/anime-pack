package params

type CountTitleAnimeParams struct {
	TitleID *int64
	AnimeID *int64
}

type DeleteTitleAnimeParams struct {
	TitleID *int64
	AnimeID *int64
}

type FilterTitleAnimeParams struct {
	TitleID *int64
	AnimeID *int64
}

type FirstTitleAnimeParams struct {
	TitleID *int64
	AnimeID *int64
}

type InsertTitleAnimeParamsItem struct {
	TitleID int64
	AnimeID int64
}

type InsertTitleAnimeParams struct {
	Items []InsertTitleAnimeParamsItem
}
