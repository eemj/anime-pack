package params

type CountXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
}

type DeleteXDCCParams struct {
	IDs             []int64
	TitleEpisodeIDs []int64
}

type FilterXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
	TitleID         *int64
	TitleReviewed   *bool
	TitleIsDeleted  *bool
	WithoutVideo    bool
	WithQualityID   bool
	QualityID       *int64
}

type FirstXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
	TitleID         *int64
	WithoutVideo    bool
}

type InsertXDCCParamsItem struct {
	Pack            int64
	Size            int64
	Filename        string
	EscapedFilename string
	BotID           int64
	QualityID       *int64
	ReleaseGroupID  int64
	TitleEpisodeID  int64
}

type InsertXDCCParams struct {
	Items []InsertXDCCParamsItem
}

type SoftDeleteXDCCParams struct {
	IDs             []int64
	TitleEpisodeIDs []int64
}

type UpdateXDCCParams struct {
	Pack            *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	QualityID       *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	ID              int64
}

func (a UpdateXDCCParams) IsEmpty() bool {
	return (a.Pack == nil || *a.Pack == 0) &&
		(a.Size == nil || *a.Size == 0) &&
		(a.Filename == nil || *a.Filename == "") &&
		(a.EscapedFilename == nil || *a.EscapedFilename == "") &&
		(a.BotID == nil || *a.BotID == 0) &&
		(a.QualityID == nil || *a.QualityID == 0) &&
		(a.ReleaseGroupID == nil || *a.ReleaseGroupID == 0) &&
		(a.TitleEpisodeID == nil || *a.TitleEpisodeID == 0)
}
