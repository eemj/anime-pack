package params

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type CountAnimeParams struct {
	ID             *int64             `json:"id,omitempty"`
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          *int64             `json:"score,omitempty"`
	Description    *string            `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          *string            `json:"title,omitempty"`
	Format         *enums.AnimeFormat `json:"format,omitempty"`
	Poster         *int64             `json:"poster,omitempty"`
	Banner         *int64             `json:"banner,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Status         *enums.AnimeStatus `json:"status,omitempty"`
	Year           *int64             `json:"year,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`
	NextAiringDate *time.Time         `json:"nextAiringDate,omitempty"`
}

type DeleteAnimeParams struct {
	ID int64 `json:"id,omitempty"`
}

type FilterAnimeParams struct {
	ID             *int64             `json:"id,omitempty"`
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          *int64             `json:"score,omitempty"`
	Description    *string            `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          *string            `json:"title,omitempty"`
	Format         *enums.AnimeFormat `json:"format,omitempty"`
	Poster         *int64             `json:"poster,omitempty"`
	Banner         *int64             `json:"banner,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Status         *enums.AnimeStatus `json:"status,omitempty"`
	Year           *int64             `json:"year,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`
	NextAiringDate *time.Time         `json:"nextAiringDate,omitempty"`
	OrderBy        enums.AnimeOrderBy `json:"orderBy,omitempty"`
	OrderByDesc    bool               `json:"orderByDesc,omitempty"`
	Page           int64              `json:"page,omitempty"`
	Elements       int64              `json:"elements,omitempty"`
}

type InsertAnimeParams struct {
	ID             int64              `json:"id,omitempty"`
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          int64              `json:"score,omitempty"`
	Description    string             `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          string             `json:"title,omitempty"`
	Format         enums.AnimeFormat  `json:"format,omitempty"`
	PosterID       int64              `json:"posterID,omitempty"`
	BannerID       *int64             `json:"bannerID,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`
	Year           *int64             `json:"year,omitempty"`
	Status         enums.AnimeStatus  `json:"status,omitempty"`
	NextAiringDate *time.Time         `json:"nextAiringDate,omitempty"`

	GenreNames  []string `json:"genreNames,omitempty"`
	StudioNames []string `json:"studioNames,omitempty"`
	PosterUri   string   `json:"posterUri,omitempty"`
	BannerUri   *string  `json:"bannerUri,omitempty"`
}

type UpdateAnimeParams struct {
	IDMal          *int64             `json:"idMal,omitempty"`
	StartDate      *time.Time         `json:"startDate,omitempty"`
	EndDate        *time.Time         `json:"endDate,omitempty"`
	Score          *int64             `json:"score,omitempty"`
	Description    *string            `json:"description,omitempty"`
	TitleRomaji    *string            `json:"titleRomaji,omitempty"`
	TitleEnglish   *string            `json:"titleEnglish,omitempty"`
	TitleNative    *string            `json:"titleNative,omitempty"`
	Title          *string            `json:"title,omitempty"`
	Format         *enums.AnimeFormat `json:"format,omitempty"`
	PosterID       *int64             `json:"posterID,omitempty"`
	BannerID       *int64             `json:"bannerID,omitempty"`
	Colour         *string            `json:"colour,omitempty"`
	Season         *enums.AnimeSeason `json:"season,omitempty"`
	Year           *int64             `json:"year,omitempty"`
	Status         *enums.AnimeStatus `json:"status,omitempty"`
	NextAiringDate *time.Time         `json:"nextAiringDate,omitempty"`
	ID             int64              `json:"id,omitempty"`
}

func (a UpdateAnimeParams) IsEmpty() bool {
	return (a.IDMal == nil || *a.IDMal == 0) &&
		(a.StartDate == nil || a.StartDate.IsZero()) &&
		(a.EndDate == nil || a.EndDate.IsZero()) &&
		(a.Score == nil || *a.Score == 0) &&
		(a.Description == nil || *a.Description == "") &&
		(a.TitleRomaji == nil || *a.TitleRomaji == "") &&
		(a.TitleEnglish == nil || *a.TitleEnglish == "") &&
		(a.TitleNative == nil || *a.TitleNative == "") &&
		(a.Title == nil || *a.Title == "") &&
		a.Format == nil &&
		(a.PosterID == nil || *a.PosterID == 0) &&
		(a.BannerID == nil || *a.BannerID == 0) &&
		(a.Colour == nil || *a.Colour == "") &&
		a.Season == nil &&
		(a.Year == nil || *a.Year == 0) &&
		a.Status == nil &&
		(a.NextAiringDate == nil || a.NextAiringDate.IsZero())
}
