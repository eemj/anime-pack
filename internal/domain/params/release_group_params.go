package params

type CountReleaseGroupParams struct {
	Name *string
	ID   *int64
}

type DeleteReleaseGroupParams struct {
	IDs []int64
}

type FilterReleaseGroupParams struct {
	Name *string
	ID   *int64
}

type FirstReleaseGroupParams struct {
	Name *string
	ID   *int64
}

type UpdateReleaseGroupParams struct {
	Name *string
	ID   int64
}

type InsertReleaseGroupParamsItem struct {
	Name string
}

type InsertReleaseGroupParams struct {
	Items []InsertReleaseGroupParamsItem
}
