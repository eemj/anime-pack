package params

type CountStudioParams struct {
	Name *string
	ID   *int64
}

type DeleteStudioParams struct {
	IDs []int64
}

type FilterStudioParams struct {
	Name *string
	ID   *int64
}

type FirstStudioParams struct {
	Name *string
	ID   *int64
}

type UpdateStudioParams struct {
	Name *string
	ID   int64
}

type InsertStudioParams struct {
	Names []string
}
