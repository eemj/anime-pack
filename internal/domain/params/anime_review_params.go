package params

import (
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/pager"
	stringutil "go.jamie.mt/toolbox/string_util"
)

type FilterAnimeReviewParams struct {
	OrderBy     string `json:"orderBy,omitempty" query:"orderBy"`
	OrderByDesc string `json:"direction,omitempty" query:"direction"`

	IDs        []int64             `json:"ids,omitempty" query:"ids"`
	Statuses   []enums.AnimeStatus `json:"statuses,omitempty" query:"statuses"`
	Formats    []enums.AnimeFormat `json:"formats,omitempty" query:"formats"`
	Seasons    []enums.AnimeSeason `json:"seasons,omitempty" query:"seasons"`
	SeasonYear *int64              `json:"seasonYear,omitempty" query:"seasonYear"`
	GenreIDs   []int64             `json:"genreIDs,omitempty" query:"genreIDs"`
	StudioIDs  []int64             `json:"studioIDs,omitempty" query:"studioIDs"`
	Favourite  *bool               `json:"favourite,omitempty" query:"favourite"`
	Title      *string             `json:"title,omitempty" query:"title"`

	Page     int64 `json:"page,omitempty" query:"page"`
	Elements int64 `json:"elements,omitempty" query:"elements"`
}

func (f *FilterAnimeReviewParams) Validate() error {
	err := pager.ValidatePageElements(&f.Page, &f.Elements)
	if err != nil {
		return err
	}
	err = enums.ValidateOrderByDirection(&f.OrderByDesc)
	if err != nil {
		return err
	}
	err = enums.ValidateAnimeOrderBy(&f.OrderBy)
	if err != nil {
		return err
	}
	if f.IDs != nil && f.Title != nil {
		return apperror.ErrFilterEitherIDorTitle
	}
	if f.Title != nil && stringutil.IsTrimmedEmpty(*f.Title) {
		return apperror.ErrFilterTitleSpecifiedButEmpty
	}
	if integerIsEmpty(f.GenreIDs) {
		return apperror.ErrFilterGenreIDsMustBeGreaterThan0
	}
	if integerIsEmpty(f.StudioIDs) {
		return apperror.ErrFilterStudioIDsMustBeGreaterThan0
	}
	if integerIsEmpty(f.IDs) {
		return apperror.ErrFilterIDsMustBeGreaterThan0
	}
	if f.SeasonYear != nil && *f.SeasonYear <= 0 {
		return apperror.ErrFilterSeasonYearMustBeGreaterThan0
	}
	return nil
}

type FirstAnimeReviewParams struct {
	ID int64 `json:"id,omitempty"`
}
