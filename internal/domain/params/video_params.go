package params

type CountVideoParams struct {
	ID          *int64
	Duration    *float64
	Size        *int64
	CRC32       *string
	ThumbnailID *int64
	XDCCID      *int64
	QualityID   *int64
}

type DeleteVideoParams struct {
	ID          int64
	ThumbnailID int64
}

type FilterVideoParams struct {
	ID          *int64
	Duration    *float64
	Size        *int64
	CRC32       *string
	ThumbnailID *int64
	XDCCID      *int64
	QualityID   *int64
}

type FirstVideoParams struct {
	ID          *int64
	Duration    *float64
	Size        *int64
	CRC32       *string
	ThumbnailID *int64
	XDCCID      *int64
	QualityID   *int64
}

type InsertVideoParams struct {
	Path          string
	Duration      float64
	Size          int64
	CRC32         string
	ThumbnailID   int64
	XDCCID        int64
	QualityHeight *int64
}

type UpdateVideoParams struct {
	Duration    *float64
	Size        *int64
	CRC32       *string
	ThumbnailID *int64
	XDCCID      *int64
	QualityID   *int64
	ID          int64
}

type LocateVideoParams struct {
	Path string
}
