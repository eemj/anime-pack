package params

import "gitlab.com/eemj/anime-pack/internal/domain"

type CreateForTitlePacklist struct {
	TitleID int64
	Packs   []*domain.GroupPack
}

type RemoveDeadPacksPacklist struct {
	Items domain.PackItems
}
