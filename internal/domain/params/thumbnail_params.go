package params

type CountThumbnailParams struct {
	ID   *int64
	Name *string
	Hash *string
}

type DeleteThumbnailParams struct {
	ID int64
}

type FilterThumbnailParams struct {
	ID   *int64
	Name *string
	Hash *string
}

type FirstThumbnailParams struct {
	ID   *int64
	Name *string
	Hash *string
}

type UpdateThumbnailParams struct {
	Name *string
	Hash *string
	ID   int64
}

type InsertThumbnailParamsItem struct {
	Name string
	Hash string
}

type InsertThumbnailParams struct {
	Items []InsertThumbnailParamsItem
}
