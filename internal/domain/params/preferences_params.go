package params

import "gitlab.com/eemj/anime-pack/internal/common/apperror"

type CountPreferencesParams struct {
	AnimeID            *int64 `json:"animeID,omitempty"`
	Favourite          *bool  `json:"favourite,omitempty"`
	AutomaticDownloads *bool  `json:"automaticDownloads,omitempty"`
	PerformChecksum    *bool  `json:"performChecksum,omitempty"`
}

type DeletePreferencesParams struct {
	AnimeIDs []int64 `json:"animeIDs,omitempty"`
}

type FilterPreferencesParams struct {
	AnimeID            *int64 `json:"animeID,omitempty"`
	Favourite          *bool  `json:"favourite,omitempty"`
	AutomaticDownloads *bool  `json:"automaticDownloads,omitempty"`
	PerformChecksum    *bool  `json:"performChecksum,omitempty"`
}

type FirstPreferenceParams struct {
	AnimeID            *int64 `json:"animeID,omitempty"`
	Favourite          *bool  `json:"favourite,omitempty"`
	AutomaticDownloads *bool  `json:"automaticDownloads,omitempty"`
	PerformChecksum    *bool  `json:"performChecksum,omitempty"`
}

type InsertPreferencesParams struct {
	AnimeID int64 `json:"animeID,omitempty"`
}

type UpdatePreferencesParams struct {
	Favourite          *bool `json:"favourite,omitempty"`
	AutomaticDownloads *bool `json:"automaticDownloads,omitempty"`
	PerformChecksum    *bool `json:"performChecksum,omitempty"`
	AnimeID            int64 `json:"animeID,omitempty"`
}

func (a UpdatePreferencesParams) Validate() (err error) {
	if a.AnimeID == 0 {
		return apperror.ErrPreferencesAnimeIDIsRequired
	}

	if a.Favourite == nil && a.AutomaticDownloads == nil && a.PerformChecksum == nil {
		return apperror.ErrPreferencesUnexpectedPayload
	}

	return nil
}
