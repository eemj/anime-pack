package params

type CountGenreParams struct {
	Name *string
	ID   *int64
}

type DeleteGenreParams struct {
	IDs []int64
}

type FilterGenreParams struct {
	Name *string
	ID   *int64
}

type FirstGenreParams struct {
	Name *string
	ID   *int64
}

type UpdateGenreParams struct {
	Name *string
	ID   int64
}

type InsertGenreParams struct {
	Names []string
}
