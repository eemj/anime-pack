package params

import (
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
)

type FilterAnimeMediaParams struct {
	AnilistID *int64 `json:"idAniList" query:"idAniList"`
	MalID     *int64 `json:"idMAL" query:"idMAL"`
}

func (a *FilterAnimeMediaParams) Validate() error {
	isAnilistEmpty := a.AnilistID == nil || (a.AnilistID != nil && *a.AnilistID == 0)
	isMalEmpty := a.MalID == nil || (a.MalID != nil && *a.MalID == 0)

	if (isAnilistEmpty && isMalEmpty) || (!isAnilistEmpty && !isMalEmpty) {
		return apperror.ErrThirdPartyExpectedOneOfTheIds
	}

	return nil
}

type SearchAnimeMediaParams struct {
	Titles []string `json:"titles"`
}
