package params

type CountAnimeStudioParams struct {
	AnimeID  *int64
	StudioID *int64
}

type DeleteAnimeStudioParams struct {
	AnimeID  int64
	StudioID *int64
}

type FilterAnimeStudioParams struct {
	AnimeID  *int64
	StudioID *int64
}

type FirstAnimeStudioParams struct {
	AnimeID  *int64
	StudioID *int64
}

type InsertAnimeStudioParamsItem struct {
	AnimeID  int64
	StudioID int64
}

type InsertAnimeStudioParams struct {
	Items []InsertAnimeStudioParamsItem
}
