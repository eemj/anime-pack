package params

import (
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/pager"
)

type FilterAnimeEpisodeParams struct {
	AnimeID     int64   `json:"animeID,omitempty"`
	EpisodeID   *int64  `json:"episodeID,omitempty"`
	EpisodeName *string `json:"episodeName,omitempty"`
}

type FilterAnimeEpisodeLatestParams struct {
	Status    *enums.AnimeStatus `json:"status,omitempty" query:"status"`
	Favourite *bool              `json:"favourite,omitempty" query:"favourite"`

	OrderBy          string `json:"orderBy,omitempty" query:"orderBy"`
	OrderByDirection string `json:"direction,omitempty" query:"direction"`

	Page     int64 `json:"page,omitempty" query:"page"`
	Elements int64 `json:"elements,omitempty" query:"elements"`
}

func (e *FilterAnimeEpisodeLatestParams) Validate() error {
	err := pager.ValidatePageElements(&e.Page, &e.Elements)
	if err != nil {
		return err
	}

	err = enums.ValidateOrderByDirection(&e.OrderByDirection)
	if err != nil {
		return err
	}

	err = enums.ValidateEpisodeLatestOrderBy(&e.OrderBy)
	if err != nil {
		return err
	}

	return nil
}
