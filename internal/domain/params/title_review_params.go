package params

import (
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/pager"
)

type FilterTitleReviewParams struct {
	Reviewed  *bool `json:"reviewed,omitempty" query:"reviewed"`
	IsDeleted *bool `json:"isDeleted,omitempty" query:"isDeleted"`

	OrderBy     string `json:"orderBy,omitempty" query:"orderBy"`
	OrderByDesc string `json:"direction,omitempty" query:"direction"`

	Page     int64 `json:"page,omitempty" query:"page"`
	Elements int64 `json:"elements,omitempty" query:"elements"`
}

func (f *FilterTitleReviewParams) Validate() error {
	err := pager.ValidatePageElements(&f.Page, &f.Elements)
	if err != nil {
		return err
	}
	err = enums.ValidateOrderByDirection(&f.OrderByDesc)
	if err != nil {
		return err
	}
	err = enums.ValidateTitleOrderBy(&f.OrderBy)
	if err != nil {
		return err
	}
	return nil
}
