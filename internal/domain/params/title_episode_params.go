package params

type CountTitleEpisodeParams struct {
	ID        *int64
	TitleID   *int64
	EpisodeID *int64
	IsDeleted *bool
}

type FilterTitleEpisodeParams struct {
	ID        *int64
	TitleID   *int64
	EpisodeID *int64
	IsDeleted *bool
}

type FirstTitleEpisodeParams struct {
	ID        *int64
	TitleID   *int64
	EpisodeID *int64
	IsDeleted *bool
}

type DeleteTitleEpisodeParams struct {
	IDs       []int64
	TitleID   *int64
	EpisodeID *int64
}

type SoftDeleteTitleEpisodeParams struct {
	IDs       []int64
	TitleID   *int64
	EpisodeID *int64
}

type InsertTitleEpisodeParamsItem struct {
	TitleID   int64
	EpisodeID int64
}

type InsertTitleEpisodeParams struct {
	Items []InsertTitleEpisodeParamsItem
}
