package params

type CountBotParams struct {
	Name *string
	ID   *int64
}

type DeleteBotParams struct {
	IDs []int64
}

type FilterBotParams struct {
	Name *string
	ID   *int64
}

type FirstBotParams struct {
	Name *string
	ID   *int64
}

type UpdateBotParams struct {
	Name *string
	ID   int64
}

type InsertBotParamsItem struct {
	Name string
}

type InsertBotParams struct {
	Items []InsertBotParamsItem
}
