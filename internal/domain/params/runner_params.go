package params

type CountRunnerParams struct {
	ID        *int64
	Name      *string
	User      *string
	Ident     *string
	Hostname  *string
	Signature *string
}

type FilterRunnerParams struct {
	ID        *int64
	Name      *string
	User      *string
	Ident     *string
	Hostname  *string
	Signature *string
}

type FirstRunnerParams struct {
	ID        *int64
	Name      *string
	User      *string
	Ident     *string
	Hostname  *string
	Signature *string
}

type InsertRunnerParams struct {
	Name      string
	User      string
	Ident     string
	Hostname  string
	Signature string
}

type UpdateRunnerParams struct {
	Name     *string
	User     *string
	Hostname *string
	ID       int64
}

type DeleteRunnerParams struct {
	ID int64
}

type AuthRunnerParams struct {
	Ident    string
	User     string
	Hostname string
	Docker   bool
}

type ValidateAuthRunnerParams struct {
	Ident string
	Token string
}
