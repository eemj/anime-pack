package params

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	stringutil "go.jamie.mt/toolbox/string_util"
)

type ImportLibraryParams struct {
	TitleName         string     `json:"titleName,omitempty" query:"titleName"`
	TitleSeasonNumber *string    `json:"titleSeasonNumber,omitempty" query:"titleSeasonNumber"`
	TitleYear         *int       `json:"titleYear,omitempty" query:"titleYear"`
	DeletedAt         *time.Time `json:"deletedAt,omitempty" query:"deletedAt"`
	AnimeID           *int64     `json:"animeID,omitempty" query:"animeID"`
}

func (e ImportLibraryParams) Validate() (err error) {
	if stringutil.IsTrimmedEmpty(e.TitleName) {
		return apperror.ErrLibraryTitleMissing
	}
	if e.DeletedAt == nil {
		if e.AnimeID == nil {
			return apperror.ErrLibraryExpectedDeletedAtOrAnimeID
		} else if *e.AnimeID == 0 {
			return apperror.ErrLibraryAnimeIDGreaterThan0
		}
	}
	return nil
}

type FilterLibraryParams struct {
	VideoPath *string
	VideoID   *int64
	XDCCID    *int64
}

type MatchLibraryParams struct {
	TitleName         string  `json:"name,omitempty"`
	TitleSeasonNumber *string `json:"seasonNumber,omitempty"`
	TitleYear         *int    `json:"year,omitempty"`
}

type BestQualityPerReleaseGroupParams struct {
	AnimeID int64 `json:"animeID,omitempty"`
}
