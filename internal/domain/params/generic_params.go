package params

import (
	"golang.org/x/exp/constraints"
)

func integerIsEmpty[T constraints.Integer](items []T) bool {
	for _, item := range items {
		if item <= 0 {
			return true
		}
	}
	return false
}
