package params

type InsertEpisodeParamsItem struct {
	Name string
}

type InsertEpisodeParams struct {
	Items []InsertEpisodeParamsItem
}

type CountEpisodeParams struct {
	Name *string
	ID   *int64
}

type DeleteEpisodeParams struct {
	IDs []int64
}

type FilterEpisodeParams struct {
	Name *string
	ID   *int64
}

type FirstEpisodeParams struct {
	Name *string
	ID   *int64
}

type UpdateEpisodeParams struct {
	Name *string
	ID   int64
}
