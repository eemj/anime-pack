package params

type CountAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

type DeleteAnimeGenreParams struct {
	AnimeID int64
	GenreID *int64
}

type FilterAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

type FirstAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

type InsertAnimeGenreParamsItem struct {
	AnimeID int64
	GenreID int64
}

type InsertAnimeGenreParams struct {
	Items []InsertAnimeGenreParamsItem
}
