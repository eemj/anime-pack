package params

import "io"

type CreateFromReaderImageParams struct {
	Reader io.Reader
	Uri    string
}
