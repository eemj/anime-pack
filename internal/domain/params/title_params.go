package params

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/common/apperror"
)

type CountTitleParams struct {
	ID           *int64
	Name         *string
	Reviewed     *bool
	SeasonNumber *string
	Year         *int
	DeletedAt    *time.Time
	IsDeleted    *bool
}

type DeleteTitleParams struct {
	ID int64
}

type FilterTitleParams struct {
	ID           *int64
	Name         *string
	Reviewed     *bool
	IsDeleted    *bool
	SeasonNumber *string
	Year         *int
}

type FirstTitleParams struct {
	ID           *int64
	Name         *string
	Reviewed     *bool
	IsDeleted    *bool
	SeasonNumber *string
	Year         *int
}

type SoftDeleteTitleParams struct {
	IDs []int64
}

type UpdateTitleParams struct {
	Reviewed *bool
	ID       int64
}

type InsertTitleParamsItem struct {
	Name         string
	SeasonNumber *string
	Year         *int
	Reviewed     bool
}

type InsertTitleParams struct {
	Items []InsertTitleParamsItem
}

type ReviewTitleParams struct {
	ID int64 `json:"-"`

	Reviewed  bool   `json:"reviewed" query:"reviewed" form:"reviewed"`
	AniListID *int64 `json:"idAniList" query:"idAniList" form:"idAniList"`
	MALID     *int64 `json:"idMAL" query:"idMAL" form:"idMAL"`
}

func (a *ReviewTitleParams) Validate() error {
	if (a.MALID != nil && a.AniListID != nil) || (a.MALID == nil && a.AniListID == nil) {
		return apperror.ErrThirdPartyExpectedOneOfTheIds
	}
	return nil
}

type CreateTitleAnimeAssociationParams struct {
	TitleID  int64
	AnimeIDs []int64
}
