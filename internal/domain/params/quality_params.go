package params

type CountQualityParams struct {
	Height *int64
	ID     *int64
}

type DeleteQualityParams struct {
	IDs []int64
}

type FilterQualityParams struct {
	Height *int64
	ID     *int64
}

type FirstQualityParams struct {
	Height *int64
	ID     *int64
}

type UpdateQualityParams struct {
	Height *int64
	ID     int64
}

type InsertQualityParamsItem struct {
	Height int64
}

type InsertQualityParams struct {
	Items []InsertQualityParamsItem
}
