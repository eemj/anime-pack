package params

type CountPosterParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type DeletePosterParams struct {
	ID int64
}

type FilterPosterParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type FirstPosterParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type InsertPosterParamsItem struct {
	Name string
	Hash string
	Uri  string
}

type InsertPosterParams struct {
	Items []InsertPosterParamsItem
}

type UpdatePosterParams struct {
	Name *string
	Hash *string
	Uri  *string
	ID   int64
}
