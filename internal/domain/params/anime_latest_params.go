package params

import (
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/pager"
)

type FilterAnimeLatestParams struct {
	Statuses  []enums.AnimeStatus `json:"statuses,omitempty" query:"statuses"`
	Formats   []enums.AnimeFormat `json:"formats,omitempty" query:"formats"`
	Favourite *bool               `json:"favourite,omitempty" query:"favourite"`

	OrderBy     string `json:"orderBy,omitempty" query:"orderBy"`
	OrderByDesc string `json:"direction,omitempty" query:"direction"`

	Page     int64 `json:"page,omitempty" query:"page"`
	Elements int64 `json:"elements,omitempty" query:"elements"`
}

func (f *FilterAnimeLatestParams) Validate() error {
	err := pager.ValidatePageElements(&f.Page, &f.Elements)
	if err != nil {
		return err
	}
	err = enums.ValidateOrderByDirection(&f.OrderByDesc)
	if err != nil {
		return err
	}
	err = enums.ValidateAnimeLatestOrderBy(&f.OrderBy)
	if err != nil {
		return err
	}
	return nil
}
