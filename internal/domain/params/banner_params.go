package params

type CountBannerParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type DeleteBannerParams struct {
	ID int64
}

type FilterBannerParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type FirstBannerParams struct {
	ID   *int64
	Name *string
	Hash *string
	Uri  *string
}

type InsertBannerParamsItem struct {
	Name string
	Hash string
	Uri  string
}

type InsertBannerParams struct {
	Items []InsertBannerParamsItem
}

type UpdateBannerParams struct {
	Name *string
	Hash *string
	Uri  *string
	ID   int64
}
