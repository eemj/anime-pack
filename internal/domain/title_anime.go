package domain

import (
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

type TitleAnime struct {
	TitleID   int64     `json:"titleID,omitempty"`
	AnimeID   int64     `json:"animeID,omitempty"`
	CreatedAt time.Time `json:"createdAt,omitempty"`

	Title Title `json:"title,omitempty"`
	Anime Anime `json:"anime,omitempty"`
}

type TitleAnimeFlat struct {
	TitleID             int64
	AnimeID             int64
	CreatedAt           time.Time
	TitleName           string
	TitleSeasonNumber   *string
	TitleYear           *int
	TitleReviewed       bool
	TitleIsDeleted      bool
	AnimeIDMal          *int64
	AnimeStartDate      *time.Time
	AnimeEndDate        *time.Time
	AnimeScore          int64
	AnimeDescription    string
	AnimeTitleRomaji    *string
	AnimeTitleEnglish   *string
	AnimeTitleNative    *string
	AnimeTitle          string
	AnimePosterID       int64
	AnimeBannerID       *int64
	AnimeColour         *string
	AnimeYear           *int64
	AnimeNextAiringDate *time.Time
	AnimeCreatedAt      time.Time
	AnimeUpdatedAt      time.Time
	AnimeStatus         enums.AnimeStatus
	AnimeFormat         enums.AnimeFormat
	AnimeSeason         *enums.AnimeSeason
	PosterName          string
	PosterHash          string
	PosterUri           string
	BannerName          *string
	BannerHash          *string
	BannerUri           *string
}

func (a TitleAnimeFlat) Unflatten() *TitleAnime {
	titleAnime := &TitleAnime{
		AnimeID:   a.AnimeID,
		TitleID:   a.TitleID,
		CreatedAt: a.CreatedAt,
		Title: Title{
			ID:           a.TitleID,
			Name:         a.TitleName,
			SeasonNumber: a.TitleSeasonNumber,
			Year:         a.TitleYear,
			IsDeleted:    a.TitleIsDeleted,
			Reviewed:     a.TitleReviewed,
		},
		Anime: Anime{
			ID:           a.AnimeID,
			IDMal:        a.AnimeIDMal,
			StartDate:    a.AnimeStartDate,
			EndDate:      a.AnimeEndDate,
			Score:        a.AnimeScore,
			Description:  a.AnimeDescription,
			TitleRomaji:  a.AnimeTitleRomaji,
			TitleEnglish: a.AnimeTitleEnglish,
			TitleNative:  a.AnimeTitleNative,
			Title:        a.AnimeTitle,
			PosterID:     a.AnimePosterID,
			Poster: Poster{
				ID:   a.AnimePosterID,
				Name: a.PosterName,
				Uri:  a.PosterUri,
				Hash: a.PosterHash,
			},
			BannerID:       a.AnimeBannerID,
			Colour:         a.AnimeColour,
			Year:           a.AnimeYear,
			NextAiringDate: a.AnimeNextAiringDate,
			CreatedAt:      a.AnimeCreatedAt,
			UpdatedAt:      a.AnimeUpdatedAt,
			Status:         a.AnimeStatus,
			Format:         a.AnimeFormat,
			Season:         a.AnimeSeason,
		},
	}
	if a.AnimeBannerID != nil {
		titleAnime.Anime.Banner = &Banner{
			ID:   *a.AnimeBannerID,
			Name: *a.BannerName,
			Hash: *a.BannerHash,
			Uri:  *a.BannerUri,
		}
	}
	return titleAnime
}
