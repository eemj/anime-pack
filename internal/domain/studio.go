package domain

type Studio struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type (
	Studios           []*Studio
	StudiosSortByName Studios
)

func (a StudiosSortByName) Len() int           { return len(a) }
func (a StudiosSortByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a StudiosSortByName) Less(i, j int) bool { return a[i].Name < a[j].Name }
