package domain

import (
	"sort"

	sliceutil "go.jamie.mt/toolbox/slice_util"
)

type AnimeSearch struct {
	AnimeID int64
	Title   string
}

type (
	AnimeSearchItems            []*AnimeSearch
	AnimeSearchItemsSortByTitle AnimeSearchItems
)

func (a AnimeSearchItemsSortByTitle) Len() int           { return len(a) }
func (a AnimeSearchItemsSortByTitle) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a AnimeSearchItemsSortByTitle) Less(i, j int) bool { return a[i].Title < a[j].Title }

type AnimeSearchScore struct {
	AnimeSearch AnimeSearch
	Score       int
}

type AnimeSearchContext struct {
	IDs    []int64
	Scores []*AnimeSearchScore
}

func (a *AnimeSearchContext) Append(score *AnimeSearchScore) {
	a.IDs = append(a.IDs, score.AnimeSearch.AnimeID)
	a.Scores = append(a.Scores, score)
}

func (a *AnimeSearchContext) Results() []int64 {
	sort.Sort(sort.Reverse(a))
	return sliceutil.Distinct(a.IDs)
}

func (a AnimeSearchContext) Len() int {
	return len(a.Scores)
}

func (a AnimeSearchContext) Swap(i, j int) {
	a.IDs[i], a.IDs[j] = a.IDs[j], a.IDs[i]
	a.Scores[i], a.Scores[j] = a.Scores[j], a.Scores[i]
}

func (a AnimeSearchContext) Less(i, j int) bool {
	return a.Scores[i].Score < a.Scores[j].Score
}
