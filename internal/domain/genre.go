package domain

type Genre struct {
	ID   int64  `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type (
	Genres           []*Genre
	GenresSortByName Genres
)

func (a GenresSortByName) Len() int           { return len(a) }
func (a GenresSortByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a GenresSortByName) Less(i, j int) bool { return a[i].Name < a[j].Name }
