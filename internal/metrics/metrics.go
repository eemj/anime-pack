package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/eemj/anime-pack/internal/queue"
)

type Metrics struct {
	gatherer prometheus.Gatherer
}

func NewMetrics(
	queue *queue.Queue,
) *Metrics {
	var registry prometheus.Registerer = prometheus.DefaultRegisterer

	registry = prometheus.WrapRegistererWithPrefix(`anime_pack_`, registry)
	registry.MustRegister(queue.Collector())

	return &Metrics{
		gatherer: prometheus.DefaultGatherer,
	}
}

func (m *Metrics) Gatherer() prometheus.Gatherer { return m.gatherer }
