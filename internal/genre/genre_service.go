package genre

import (
	"context"
	"sort"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type GenreService interface {
	CreateMany(ctx context.Context, arg params.InsertGenreParams) ([]*domain.Genre, error)
	First(ctx context.Context, arg params.FirstGenreParams) (*domain.Genre, error)
	Filter(ctx context.Context, arg params.FilterGenreParams) ([]*domain.Genre, error)
	Count(ctx context.Context, arg params.CountGenreParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateGenreParams) (int64, error)
	List(ctx context.Context) (domain.Genres, error)
}

type genreService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

func (s *genreService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.GenreServiceName)
}

// List implements GenreService.
func (s *genreService) List(ctx context.Context) (domain.Genres, error) {
	cachedValue, err := encoded.Get[domain.Genres](ctx, s.cacher, cachekeys.AnimeGenres)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	rows, err := s.querier.ListAnimeReviewGenre(ctx)
	if err != nil {
		return nil, err
	}
	result := mapEntitesToDomain(rows...)

	sort.Sort(domain.GenresSortByName(result))
	err = encoded.Set(
		ctx,
		s.cacher,
		cachekeys.AnimeGenres,
		result,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Count implements Service.
func (s *genreService) Count(ctx context.Context, arg params.CountGenreParams) (int64, error) {
	count, err := s.querier.CountGenre(ctx, database.CountGenreParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// CreateMany implements Service.
func (s *genreService) CreateMany(ctx context.Context, arg params.InsertGenreParams) ([]*domain.Genre, error) {
	args := make([]database.InsertGenreParams, len(arg.Names))
	for index, name := range arg.Names {
		args[index] = database.InsertGenreParams{Name: name}
	}

	bulk := s.querier.InsertGenre(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertGenreRow,
		*domain.Genre,
	](bulk, func(row *database.InsertGenreRow) *domain.Genre {
		return &domain.Genre{
			ID:   row.ID,
			Name: row.Name,
		}
	})
}

func mapEntitesToDomain(entities ...*database.Genre) []*domain.Genre {
	result := make([]*domain.Genre, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Genre{
			ID:   entity.ID,
			Name: entity.Name,
		}
	}
	return result
}

// Filter implements Service.
func (s *genreService) Filter(ctx context.Context, arg params.FilterGenreParams) ([]*domain.Genre, error) {
	entities, err := s.querier.FilterGenre(ctx, database.FilterGenreParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Genre{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *genreService) First(ctx context.Context, arg params.FirstGenreParams) (*domain.Genre, error) {
	entity, err := s.querier.FirstGenre(ctx, database.FirstGenreParams{
		Name: arg.Name,
		ID:   arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entity)[0], nil
}

// Update implements Service.
func (s *genreService) Update(ctx context.Context, arg params.UpdateGenreParams) (int64, error) {
	return s.querier.UpdateGenre(ctx, database.UpdateGenreParams{
		ID:   arg.ID,
		Name: arg.Name,
	})
}

func NewGenreService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) GenreService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &genreService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
