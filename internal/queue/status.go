package queue

// Mapper defines an interface to be able to map a queue items
// into any other values
type Mapper[V any] interface {
	Map(active, inactive Items) (V, error)
}
