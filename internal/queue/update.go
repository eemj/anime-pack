package queue

import "gitlab.com/eemj/anime-pack/internal/domain"

func (q *Queue) UpdateProgress(anime domain.AnimeBase, progress Progress) {
	q.active.UpdateFunc(anime, func(v *State) {
		v.UpdateProgress(progress)
	})
}

func (q *Queue) UpdateUnmanaged(cb func(Key, *State) bool) {
	for key, value := range q.active.Map() {
		if cb(key, value.Clone()) {
			q.active.UpdateFunc(key, func(v *State) { v.ToggleUnmanaged() })
			break
		}
	}
}

func (q *Queue) Update(anime domain.AnimeBase, state *State) {
	q.active.Update(anime, state)
}
