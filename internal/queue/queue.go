package queue

import (
	"context"
	"sync"
	"sync/atomic"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/queue/active"
	"gitlab.com/eemj/anime-pack/internal/queue/inactive"
	psync "gitlab.com/eemj/anime-pack/pkg/sync"
	"go.jamie.mt/logx/log"
	"go.uber.org/multierr"
	"go.uber.org/zap"
)

const (
	// DefaultTick defines the Pop goroutine ticker's duration.
	DefaultTick = 30 * time.Second
)

type Queue struct {
	active   active.Active[domain.AnimeBase, *State]
	inactive inactive.Inactive[domain.AnimeBase, *State]

	// rwmutex ...
	rwmutex sync.RWMutex

	// notifyc is linked with the NextC method, which sends an empty struct{}
	// once an element is ready to be downloaded
	notifyc chan struct{}

	// errc is linked with the ErrC method, which sends an error when a problem
	// has occurred
	errc chan error

	// semaphore is used to ensure that we have the right amount of concurrent
	// elements running in the queue
	semaphore *psync.ResizableSemaphore

	// paused state, 0 -> running, 1 -> paused
	paused atomic.Bool

	tick time.Duration

	cond *sync.Cond
}

func (q *Queue) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named("queue")
}

func NewCustomQueue(tick time.Duration) *Queue {
	queue := &Queue{
		active:   active.NewActive[domain.AnimeBase, *State](),
		inactive: inactive.NewInactive[domain.AnimeBase, *State](),

		semaphore: psync.NewResizableSemaphore(0),
		notifyc:   make(chan struct{}, 1),
		errc:      make(chan error, 1),

		tick: tick,

		cond: sync.NewCond(&sync.Mutex{}),
	}

	queue.Pause()

	return queue
}

func NewQueue() *Queue {
	return NewCustomQueue(DefaultTick)
}

// Collector creates a `prometheus.Collector` with metrics such metrics such as
// `active`, `inactive` and `limit` guage statistics.
func (q *Queue) Collector() prometheus.Collector { return newCollector(q) }

// Len returns the number of active and inactive items
func (q *Queue) Len() int64 {
	return q.inactive.Len() + q.active.Len()
}

// Delete removes the queue entry (it does not send an empty to `.Cancel` though)
func (q *Queue) Delete(anime domain.AnimeBase) {
	if q.active.Contains(anime) {
		q.active.Delete(anime)
	} else if q.inactive.Contains(anime) {
		q.inactive.Delete(anime)
	}
}

// ErrC returns any errors that occurred during the Pop background routine.
func (q *Queue) ErrC() <-chan error {
	return q.errc
}

// Restart attempts to restart the queue item.
func (q *Queue) Restart(anime domain.AnimeBase) {
	if value, ok := q.active.Get(anime); ok && value != nil {
		value.Restart()
	}
}

// Cancel attempts to cancel out the current active item. If the item is not canceled
// it'll call the inactive Delete().
func (q *Queue) Cancel(anime domain.AnimeBase) {
	if value, ok := q.active.Get(anime); ok && value != nil {
		value.Cancel()
	} else {
		q.inactive.Delete(anime)
	}
}

// Get will return the value in pointer form, if it returns nil, the item probably does not exist.
func (q *Queue) Get(anime domain.AnimeBase) (value *State, ok bool) {
	if value, ok = q.active.Get(anime); ok && value != nil {
		return
	}

	return q.inactive.Get(anime)
}

// Contains will return true if the key is stored in the active or inactive queue.
func (q *Queue) Contains(anime domain.AnimeBase) (found bool) {
	return q.active.Contains(anime) || q.inactive.Contains(anime)
}

// IsActive will return true if the key is stored in the active sub-queue.
func (q *Queue) IsActive(anime domain.AnimeBase) (isActive bool) {
	return q.active.Contains(anime)
}

// Close will clear out all the active and inactive items.
// On-going active items will get canceled here.
func (q *Queue) Close() (err error) {
	return multierr.Combine(
		q.inactive.Close(),
		q.active.Close(),
	)
}

// IsBusy returns true if there's items in the queue and
// there's a semaphore allocation (someone's ready to take the work).
func (q *Queue) IsBusy() bool {
	return q.Len() > 0 && q.GetLimit() > 0
}
