package queue

import (
	"sync"
	"sync/atomic"
	"time"
)

var (
	// AcquireTTL is how long we'll wait to Acquire() a sempahore release.
	AcquireTTL = 1 * time.Second

	// ManagedTTL is how long till a timeout occurs for un-updated entry that's managed.
	ManagedTTL = 30 * time.Second

	// UnmanagedTTL is how long till a timeout occurs for un-updated entry that's unmanaged.
	UnmanagedTTL = 10 * time.Minute
)

// Progress defines the progress for a value whilst it's in queue.
type Progress struct {
	Type string //	@refer	to 'pkg/websocket/protocol' ProgressType

	Percentage float64
	Speed      float64

	CurrentFilesize *float64
	Filesize        *float64

	CurrentDuration *float64
	Duration        *float64
}

func (p Progress) Equal(other Progress) bool {
	// Compare the Type field
	if p.Type != other.Type {
		return false
	}

	// Compare the Percentage field
	if p.Percentage != other.Percentage {
		return false
	}

	// Compare the Speed field
	if p.Speed != other.Speed {
		return false
	}

	// Compare CurrentFilesize and Filesize fields
	if (p.CurrentFilesize == nil && other.CurrentFilesize != nil) ||
		(p.CurrentFilesize != nil && other.CurrentFilesize == nil) ||
		(p.Filesize == nil && other.Filesize != nil) ||
		(p.Filesize != nil && other.Filesize == nil) {
		return false
	}

	if p.CurrentFilesize != nil && other.CurrentFilesize != nil && *p.CurrentFilesize != *other.CurrentFilesize {
		return false
	}

	if p.Filesize != nil && other.Filesize != nil && *p.Filesize != *other.Filesize {
		return false
	}

	// Compare CurrentDuration and Duration fields
	if (p.CurrentDuration == nil && other.CurrentDuration != nil) ||
		(p.CurrentDuration != nil && other.CurrentDuration == nil) ||
		(p.Duration == nil && other.Duration != nil) ||
		(p.Duration != nil && other.Duration == nil) {
		return false
	}

	if p.CurrentDuration != nil && other.CurrentDuration != nil && *p.CurrentDuration != *other.CurrentDuration {
		return false
	}

	if p.Duration != nil && other.Duration != nil && *p.Duration != *other.Duration {
		return false
	}

	return true
}

type Detail struct {
	ID   int64
	Pack int64
	Bot  string
}

// Clone will create a clone of the detail.
func (d Detail) Clone() Detail {
	return Detail{
		ID:   d.ID,
		Pack: d.Pack,
		Bot:  d.Bot,
	}
}

type StartedFrom int

const (
	StartedFrom_AutoDownload StartedFrom = iota << 1
	StartedFrom_Web
)

// StartedFrom implements fmt.Stringer
func (s StartedFrom) String() string {
	switch s {
	case StartedFrom_AutoDownload:
		return "AUTO_DOWNLOAD"
	case StartedFrom_Web:
		return "WEB"
	}
	return ""
}

// State is the queue struct, where we hold the available items along with the progress.
// Details contain all the possible iterations
type State struct {
	rwmutex   sync.RWMutex
	updatedAt time.Time
	done      chan error
	unmanaged atomic.Bool
	closed    atomic.Bool

	Progress    Progress
	Details     []Detail
	StartedFrom StartedFrom
}

func (v *State) OnUpdate() {
	v.rwmutex.Lock()
	v.updatedAt = time.Now()
	v.rwmutex.Unlock()
}

func (v *State) UpdatedAt() time.Time {
	v.rwmutex.RLock()
	defer v.rwmutex.RUnlock()
	return v.updatedAt
}

func (v *State) Init() {
	v.rwmutex.Lock()
	v.done = make(chan error, 1)
	v.rwmutex.Unlock()
}

func (v *State) Active() bool {
	v.rwmutex.RLock()
	defer v.rwmutex.RUnlock()
	return v.done != nil && !v.closed.Load() && len(v.Details) > 0
}

func (v *State) UpdateProgress(progress Progress) {
	v.rwmutex.Lock()
	v.updatedAt = time.Now()
	v.Progress = progress
	v.rwmutex.Unlock()
}

func (v *State) ToggleUnmanaged() {
	v.rwmutex.Lock()
	v.updatedAt = time.Now()
	v.unmanaged.Swap(!v.unmanaged.Load())
	v.rwmutex.Unlock()
}

// Clone will copy the queue value, but only the details are copied.
func (v *State) Clone() (value *State) {
	v.rwmutex.RLock()
	value = &State{
		Details:     make([]Detail, len(v.Details)),
		Progress:    v.Progress,
		StartedFrom: v.StartedFrom,
	}
	copy(value.Details, v.Details)
	v.rwmutex.RUnlock()
	return value
}

// IsUnmanaged returns if the queue value is managed by us, or we're awaiting something.
func (v *State) IsUnmanaged() bool { return v.unmanaged.Load() }

func (v *State) Close() (err error) {
	v.rwmutex.Lock()
	if v.done != nil {
		v.closed.Swap(true)
		close(v.done)
		v.done = nil
	}
	v.rwmutex.Unlock()
	return
}

// Done will attempt to close off any progress to the value given the error passed.
func (v *State) Done(err error) {
	if v.closed.CompareAndSwap(false, true) && v.done != nil {
		v.done <- err
	}
}

// Restart will attempt to restart the queue.
func (v *State) Restart() { v.Done(ErrQueueRestart) }

// Cancel will attempt to cancel the active value.
func (v *State) Cancel() { v.Done(ErrQueueCanceled) }

// Current returns the current detail.
func (v *State) Current() (*Detail, bool) {
	v.rwmutex.RLock()
	defer v.rwmutex.RUnlock()
	ok := len(v.Details) > 0
	if ok {
		clonedDetail := v.Details[0].Clone()
		return &clonedDetail, ok
	}
	return nil, ok
}

// TimedOut checks if the value is native, if it's not native it'll check if the `updatedAt` entry
// is less than the time provided minus 30 seconds.
func (v *State) TimedOut(from time.Time) bool {
	v.rwmutex.RLock()
	defer v.rwmutex.RUnlock()
	if v.unmanaged.Load() {
		return from.Sub(v.updatedAt) >= UnmanagedTTL
	}
	return from.Sub(v.updatedAt) >= ManagedTTL
}
