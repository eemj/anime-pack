package queue

import (
	"strconv"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

var (
	queueValue = &State{
		Details: generateDetail(4),
	}

	animes = generateAnime(10)
)

func generateDetail(length int) []Detail {
	detail := make([]Detail, length)
	for index := range detail {
		detail[index].Bot = "anime-pack"
		detail[index].Pack = (int64(index) + 1)
	}
	return detail
}

func generateAnime(length int) []domain.AnimeBase {
	animes := make([]domain.AnimeBase, length)
	for index := range animes {
		animes[index].ID = int64(index + 1)
		animes[index].Title = "Anime " + strconv.Itoa(index)
		animes[index].Episode = strconv.Itoa(index + 1)
		animes[index].Height = 1080
		animes[index].ReleaseGroup = "anime-pack"
	}
	return animes
}

func dumpQueueChannels(queue *Queue) {
	go func() {
		for range queue.Notifier() {
		}
	}()
}
