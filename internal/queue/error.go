package queue

import (
	"fmt"
)

// Error can be made into a custom error.
type Error string

func (e Error) Error() string {
	return string(e)
}

// KeyError targets a 'queue.Key' with an error message.
type KeyError struct {
	Key Key
	err error
}

func (e *KeyError) Error() string {
	return fmt.Sprintf("%v: %s", e.err, e.Key)
}

func (e *KeyError) Unwrap() (err error) { return e.err }

func (e *KeyError) Is(target error) bool {
	keyError, ok := target.(*KeyError)
	return ok && keyError.Key == e.Key
}

const (
	// ErrQueueTimeout is thrown when an item hasn't been updated by the timeout variable defined.
	ErrQueueTimeout = Error("queue entry timed-out")

	// ErrQueueNoAvailableItem is thrown when an item does not have any items left to push.
	ErrQueueNoAvailableItem = Error("no available items")

	// ErrQueueRestart is used when one wants to restart the current active item.
	ErrQueueRestart = Error("queue entry restart")

	// ErrQueueCanceled is used when a user sends an empty struct{}{} to `value.Cancel`.
	ErrQueueCanceled = Error("queue item canceled")

	// ErrValueDisappeared is used when the corresponding value for a key suddenly
	// turned into a <nil>. This could only happen, if someone decided to set the
	// pointer to <nil>, however on general terms it's used to prevent deadlocks within
	// the popAwait() routine.
	ErrValueDisappeared = Error("value disappeared")
)

// ErrQueueNoAvailableItems throws ErrQueueNoAvailableItem for the specified Key.
func ErrQueueNoAvailableItems(key Key) error {
	return &KeyError{err: ErrQueueNoAvailableItem, Key: key}
}

// ErrQueueEntryTimedOut throws ErrQueueTimeout for the specified Key.
func ErrQueueEntryTimedOut(key Key) error {
	return &KeyError{err: ErrQueueTimeout, Key: key}
}

// ErrQueueValueDisappeared throws ErrValueDisappeared for the specified Key.
func ErrQueueValueDisappeared(key Key) error {
	return &KeyError{err: ErrValueDisappeared, Key: key}
}
