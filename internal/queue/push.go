package queue

import "gitlab.com/eemj/anime-pack/internal/domain"

func (q *Queue) push(anime domain.AnimeBase, state *State, pushFront bool) (pushed bool) {
	if q.Contains(anime) || len(state.Details) == 0 {
		return false
	}

	if pushFront { // we'll push front if pushFront is true
		q.inactive.PushFront(anime, state)
	} else {
		q.inactive.PushBack(anime, state)
	}

	q.notify()

	return true
}

func (q *Queue) PushFront(anime domain.AnimeBase, state *State) (pushed bool) {
	return q.push(anime, state, true)
}

func (q *Queue) PushBack(anime domain.AnimeBase, state *State) (pushed bool) {
	return q.push(anime, state, false)
}
