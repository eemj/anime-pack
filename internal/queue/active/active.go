package active

import (
	"sync"
	"sync/atomic"

	"gitlab.com/eemj/anime-pack/internal/queue/state"
	"go.uber.org/multierr"
)

type Active[K comparable, V state.Stateful[V]] interface {
	Add(k K, v V)
	Delete(k K)
	Contains(k K) (ok bool)
	Get(k K) (v V, ok bool)
	Map() map[K]V
	Update(k K, v V)
	UpdateFunc(k K, cb func(v V))
	Len() int64
	Close() error
}

type active[K comparable, V state.Stateful[V]] struct {
	rmu   sync.RWMutex
	wg    sync.WaitGroup
	store map[K]V
	count atomic.Int64
}

func NewActive[K comparable, V state.Stateful[V]]() Active[K, V] {
	return &active[K, V]{store: make(map[K]V)}
}

func (a *active[K, V]) Add(k K, v V) {
	if !a.Contains(k) {
		a.wg.Add(1)
		a.rmu.Lock()
		a.store[k] = v
		a.rmu.Unlock()
		a.count.Add(1)
	}
}

func (a *active[K, V]) Delete(k K) {
	if a.Contains(k) {
		a.rmu.Lock()
		delete(a.store, k)
		a.rmu.Unlock()
		a.count.Add(-1)
		a.wg.Done()
	}
}

func (a *active[K, V]) Contains(k K) bool {
	a.rmu.RLock()
	_, exists := a.store[k]
	a.rmu.RUnlock()
	return exists
}

func (a *active[K, V]) Get(k K) (v V, ok bool) {
	a.rmu.RLock()
	v, ok = a.store[k]
	a.rmu.RUnlock()
	return
}

func (a *active[K, V]) Map() (store map[K]V) {
	a.rmu.RLock()
	store = make(map[K]V, len(a.store))
	for key, value := range a.store {
		store[key] = value.Clone()
	}
	a.rmu.RUnlock()

	return
}

func (a *active[K, V]) Update(k K, v V) {
	if a.Contains(k) {
		a.rmu.Lock()
		a.store[k] = v
		v.OnUpdate()
		a.rmu.Unlock()
	}
}

func (a *active[K, V]) UpdateFunc(k K, cb func(v V)) {
	if a.Contains(k) {
		a.rmu.Lock()
		cb(a.store[k])
		a.store[k].OnUpdate()
		a.rmu.Unlock()
	}
}

func (a *active[K, V]) Len() int64 { return a.count.Load() }

func (a *active[K, V]) Close() (err error) {
	errors := make([]error, 0)
	a.rmu.RLock()
	for _, v := range a.store {
		if err := v.Close(); err != nil {
			errors = append(errors, err)
		}
	}
	a.rmu.RUnlock()
	a.wg.Wait()

	if len(errors) > 0 {
		return multierr.Combine(errors...)
	}

	return nil
}
