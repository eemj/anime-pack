package state

import "time"

type Stateful[T any] interface {
	Clone() T
	OnUpdate()
	UpdatedAt() (updatedAt time.Time)
	Close() (err error)
}
