package queue

import (
	"context"

	"go.uber.org/zap"
)

// Notifier will trigger when an item is added to the queue
func (q *Queue) Notifier() <-chan struct{} {
	return q.notifyc
}

func (q *Queue) notifyUnsafe() {
	if q.paused.Load() {
		return
	}

	limit := q.semaphore.GetLimit()

	activeLen := q.active.Len()
	inactiveLen := q.inactive.Len()

	q.L(context.TODO()).
		Debug(
			"notification state",
			zap.Int64("inactive", inactiveLen),
			zap.Int64("active", activeLen),
			zap.Int("limit", limit),
			zap.Int("slots", (limit-int(activeLen))),
		)

	if limit > 0 && q.inactive.Len() > 0 {
		slots := limit - int(q.active.Len()) // How many notification slots are open

		// Slots is less than 0, we probably got a limit decrease.
		// Skip the queue notify to avoid a deadlock.
		if slots < 1 {
			return
		}

		// If we don't have enough inactive items to notify, we'll set the slots to the number
		// of inactive items.
		//
		// (e.g. If we have 3 inactive items, but we have 5 slots, there isn't enough items to
		// cater for the slots, therefore 2 of the slots will get ignored)
		if inactive := int(q.inactive.Len()); slots > inactive {
			slots = inactive
		}

		// We'll send notifier signals to the watchers that the limit increased
		// and we can accept more concurrent queue items.
		for slot := 0; slot < slots; slot++ {
			q.notifyc <- struct{}{}
		}
	}
}

func (q *Queue) notify() {
	if q.Paused() {
		return
	}

	q.rwmutex.Lock()
	defer q.rwmutex.Unlock()

	q.notifyUnsafe()
}
