package queue

import "github.com/prometheus/client_golang/prometheus"

type queueCollector struct {
	queue *Queue

	active   *prometheus.Desc
	inactive *prometheus.Desc
	limit    *prometheus.Desc
}

func newCollector(queue *Queue) prometheus.Collector {
	return &queueCollector{
		queue: queue,

		active: prometheus.NewDesc(
			"queue_active",
			"Number of items in the active queue",
			[]string{},
			prometheus.Labels{},
		),
		inactive: prometheus.NewDesc(
			"queue_inactive",
			"Number of items in the in-active queue",
			[]string{},
			prometheus.Labels{},
		),
		limit: prometheus.NewDesc(
			"queue_limit",
			"Number of possible active queue items",
			[]string{},
			prometheus.Labels{},
		),
	}
}

var _ prometheus.Collector = &queueCollector{}

// Collect implements prometheus.Collector
func (q *queueCollector) Collect(ch chan<- prometheus.Metric) {
	active := float64(q.queue.active.Len())
	inactive := float64(q.queue.inactive.Len())
	limit := float64(q.queue.GetLimit())

	ch <- prometheus.MustNewConstMetric(
		q.active,
		prometheus.GaugeValue,
		active,
	)
	ch <- prometheus.MustNewConstMetric(
		q.inactive,
		prometheus.GaugeValue,
		inactive,
	)
	ch <- prometheus.MustNewConstMetric(
		q.limit,
		prometheus.GaugeValue,
		limit,
	)
}

// Describe implements prometheus.Collector
func (q *queueCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- q.active
	ch <- q.inactive
	ch <- q.limit
}
