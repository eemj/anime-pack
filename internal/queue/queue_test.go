package queue

import (
	"context"
	"errors"
	"os"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

// TestQueueConcurrentLimitUpdates ensures that when adding and subtracting limits the number
// is as expected at the end.
func TestQueueConcurrentLimitUpdates(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	wg := new(sync.WaitGroup)
	expected := 5

	for i := 0; i < expected; i++ {
		wg.Add(1)

		go func(wg *sync.WaitGroup) {
			defer wg.Done()

			queue.AddLimit(1)
		}(wg)
	}

	wg.Wait()

	if current := queue.GetLimit(); current != expected {
		t.Fatalf("expected queue limit to be '%d' but got '%d'", expected, current)
	}
}

// TestQueueContainsInactive_PushBack ensures that after we call the PushBack() method
// and call the Contains() right afterwards with the value passed previously, we'll get true.
func TestQueueContainsInactive_PushBack(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.Resume()
	queue.AddLimit(1)

	queue.PushBack(animes[0], queueValue)

	if !queue.Contains(animes[0]) {
		t.Fatalf("queue should contain '%v' in inactive", animes[0])
	}
}

// TestQueueContainsInactive_PushFront ensures that after we call the PushFront() method
// and call the Contains() right afterwards with the value passed previously, we'll get true.
func TestQueueContainsInactive_PushFront(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.Resume()
	queue.AddLimit(1)

	queue.PushFront(animes[0], queueValue)

	if !queue.Contains(animes[0]) {
		t.Fatalf("queue should contain '%v' in inactive", animes[0])
	}
}

// TestQueueContainsActive ensure that after we call the Pop() method
// whilst having an item in the inactive store, it'll get moved to the active store.
func TestQueueContainsActive(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.Resume()
	queue.AddLimit(1)

	queue.PushBack(animes[0], queueValue)

	queue.Pop()

	if !queue.active.Contains(animes[0]) {
		t.Fatalf("queue should contain '%v' in active", animes[0])
	}
}

// TestQueueSameValuePushBack ensures that we won't have duplicate keys between
// the active and inactive stores
func TestQueueSameValuePushBack(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.Resume()
	queue.AddLimit(1)

	queue.PushBack(animes[0], queueValue)

	queue.Pop() // move it to active

	queue.PushBack(animes[0], queueValue)

	if queue.inactive.Len() > 1 {
		t.Fatalf("queue shouldn't contain the same '%v' entry in queue", animes[0])
	}
}

// TestQueueSemaphore ensures that eventhough the queue concurrency is set to 1,
// we're going to have 1 active and 9 inactive.
func TestQueueSemaphore(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.Resume()
	queue.AddLimit(1)

	for _, anime := range animes[:10] {
		queue.PushBack(anime, queueValue)
	}

	queue.Pop()

	if queue.inactive.Len() != 9 {
		t.Fatalf("queue should contain 9 inactive")
	}
}

// TestQueueTimeout tests the queue timeout.
func TestQueueTimeout(t *testing.T) {
	t.Parallel()

	queue := NewCustomQueue(ManagedTTL / 2)
	defer queue.Close()
	dumpQueueChannels(queue)

	queue.PushBack(animes[0], queueValue)

	queue.Resume()
	queue.AddLimit(1)

	queue.Pop()

	select {
	case err := <-queue.ErrC():
		if err == nil {
			t.Fatalf("expected a queue timeout error, but got '<nil>'")
		}

		if !errors.Is(err, ErrQueueEntryTimedOut(animes[0])) {
			t.Fatalf("expected a queue timeout error, but got '%v'", err)
		}

		return
	case <-time.After(ManagedTTL + queue.tick):
		t.Fatalf("expected a Notifier() trigger but we didn't get it")
	}
}

// TestQueueConcurrentUpdate tests the queue flow to ensure that the queue get's updated
// depending on the specified limit and the on-going routines.
func TestQueueConcurrentUpdate(t *testing.T) {
	t.Parallel()

	animes := generateAnime(10)
	n := int32(len(animes))
	queue := NewQueue()

	defer queue.Close()

	wg := sync.WaitGroup{}
	errc := make(chan error, 1)
	ctx, cancel := context.WithCancel(context.Background())
	finished := int32(0)

	wg.Add(int(n))

	// call .Pop() on <-Notifier()
	go func(t *testing.T) {
		for {
			select {
			case err := <-queue.ErrC():
				errc <- err
			case <-queue.Notifier():
				anime, _, ok := queue.Pop()

				if ok {
					go func(anime domain.AnimeBase, wg *sync.WaitGroup) {
						t.Logf("popped '%s'", anime)

						for pct := 1; pct <= 100; pct++ {
							queue.UpdateProgress(anime, Progress{
								Type:       string(protocol.Progress_Download),
								Percentage: float64(pct),
								Speed:      1,
							})
						}

						if state, ok := queue.Get(anime); ok {
							state.Done(nil)
						}

						wg.Done()
						atomic.AddInt32(&finished, 1)
					}(anime, &wg)
				}
			}
		}
	}(t)

	go func() {
		wg.Wait()
		cancel()
	}()

	// populate the animes
	for _, anime := range animes[:n] {
		queue.PushBack(anime, &State{Details: []Detail{
			queueValue.Details[0],
		}})
	}

	queue.Resume()
	queue.AddLimit(int(n))

	// after 10 seconds, if the progress hasn't completed we'll terminate
	for {
		select {
		case <-time.After(ManagedTTL):
			t.Fatalf("timeout, exceeded %v", ManagedTTL)
		case err := <-errc:
			t.Fatalf("unexpected error during queue update '%v'", err)
		case <-ctx.Done():
			if atomic.LoadInt32(&finished) != n {
				t.Fatalf("apparently we didn't progress with our enqueued items")
			}

			return
		}
	}
}

// TestQueueMultipleTimeout tests the queue flow to ensure timeouts are thrown
// when a value is not updated.
func TestQueueMultipleTimeout(t *testing.T) {
	t.Parallel()

	animes := generateAnime(10)
	n := int32(len(animes))
	m := int32(2)
	queue := NewCustomQueue(time.Second)

	defer queue.Close()

	wg := sync.WaitGroup{}
	errc := make(chan error, 1)
	ctx, cancel := context.WithCancel(context.Background())
	finished := int32(0)

	wg.Add(int(n))

	// call .Pop() on <-Notifier()
	go func(t *testing.T) {
		for {
			select {
			case err := <-queue.ErrC():
				errc <- err
			case <-queue.Notifier():
				queue.Pop()
			}
		}
	}(t)

	go func() {
		wg.Wait()
		cancel()
	}()

	// populate the animes
	for _, anime := range animes[:n] {
		queue.PushBack(anime, &State{
			updatedAt: time.Now(),
			Details:   generateDetail(int(m)),
		})
	}

	queue.Resume()
	queue.AddLimit(int(n))

	// after 10 seconds, if the progress hasn't completed we'll terminate
	for {
		select {
		case <-errc:
			finished++
		case <-ctx.Done():
			if atomic.LoadInt32(&finished) != n {
				t.Fatalf("apparently we didn't progress with our enqueued items")
			}

			return
		default:
			if finished == (n * m) {
				return
			}
		}
	}
}

func TestQueueUnmanaged(t *testing.T) {
	t.Parallel()

	queue := NewQueue()
	animes := generateAnime(2)

	defer queue.Close()

	dumpQueueChannels(queue)

	queue.PushFront(animes[0], queueValue.Clone())

	queue.AddLimit(1)
	queue.Resume()

	queue.Pop()

	queue.UpdateUnmanaged(func(k Key, v *State) bool { return true })

	value, ok := queue.Get(animes[0])

	if !ok {
		t.Fatal("expected to find a value, but we didn't")
	}

	if value == nil || !value.unmanaged.Load() {
		t.Fatal("value was not expected to be '<nil>'/unmanaged")
	}
}

func TestQueueValueClose(t *testing.T) {
	t.Parallel()

	queue := NewCustomQueue(ManagedTTL / 2)
	animes := generateAnime(2)

	defer queue.Close()

	dumpQueueChannels(queue)

	queue.PushFront(
		animes[0],
		&State{Details: []Detail{{Bot: "anime-pack", Pack: 1}}},
	)

	queue.AddLimit(1)
	queue.Resume()

	queue.Pop()

	queue.UpdateUnmanaged(func(k Key, v *State) bool { return true })

	value, ok := queue.Get(animes[0])

	if !ok {
		t.Fatal("expected to find a value, but we didn't")
	}

	// Wait for the value to timeout
	<-time.After(ManagedTTL + queue.tick)

	if !value.closed.Load() || value.done != nil {
		t.Fatalf(
			"expected `closing` (%+v) || `.done` == `nil` (%+v(%v))",
			value.closed.Load(),
			value.done,
			value.done == nil,
		)
	}

	if value, ok = queue.Get(animes[0]); ok {
		t.Fatalf("expected to not find a value, but we found '%+v'", value)
	}
}

func TestMain(m *testing.M) {
	ManagedTTL = 2 * time.Second
	UnmanagedTTL = 2 * time.Second

	os.Exit(m.Run())
}
