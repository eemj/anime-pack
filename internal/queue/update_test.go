package queue

import (
	"sync"
	"testing"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

// ConcurrentUpdateFlow mocks the concurrent flow for the queue.
// `concurrent` defines the number of items getting updated concurrently.
//
// The flow here will be as follows:
//   - `concurrent` routines are created where items are going to get created
//     and updated from 1 to 100.
//   - wait group is created to ensure that all the routines are finished.
func ConcurrentUpdateFlow(concurrent int) {
	queue := NewQueue()

	defer queue.Close()

	// The limit will be the concurrent specified.
	queue.AddLimit(concurrent)

	// Generate X number of anime, in this case the queue animes.
	animes := generateAnime(concurrent)

	// Push the keys to the queue
	for _, key := range animes {
		queue.PushBack(key, queueValue)
	}

	wg := new(sync.WaitGroup)
	wg.Add(concurrent)

	for offset := 0; offset < concurrent; offset++ {
		go func(anime domain.AnimeBase) {
			defer wg.Done()

			filesize := float64(100)

			for i := float64(0); i < 100; i++ {
				queue.UpdateProgress(
					anime,
					Progress{
						Percentage:      i,
						Speed:           1,
						CurrentFilesize: &i,
						Filesize:        &filesize,
					},
				)
			}
		}(animes[offset])
	}

	wg.Wait()
}

func BenchmarkUpdate_20(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(20)
	}
}

func BenchmarkUpdate_50(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(50)
	}
}

func BenchmarkUpdate_100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(100)
	}
}

func BenchmarkUpdate_250(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(250)
	}
}

func BenchmarkUpdate_512(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(512)
	}
}

func BenchmarkUpdate_1024(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(1024)
	}
}

func BenchmarkUpdate_4096(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ConcurrentUpdateFlow(4096)
	}
}
