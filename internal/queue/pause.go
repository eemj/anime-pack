package queue

func (q *Queue) Pause() { q.paused.Swap(true) }

func (q *Queue) Resume() {
	if q.paused.CompareAndSwap(true, false) {
		q.notify()
	}
}

func (q *Queue) Paused() bool { return q.paused.Load() }
