package queue

import (
	"context"
	"errors"
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain"
	"go.uber.org/zap"
)

func (q *Queue) block(anime domain.AnimeBase) (errc chan error) {
	errc = make(chan error, 1)

	go func() {
		ticker := time.NewTicker(q.tick)
		defer ticker.Stop()

		if value, ok := q.Get(anime); ok && value != nil && value.Active() {
			for {
				select {
				case err := <-value.done:
					errc <- err
					return
				case tick := <-ticker.C:
					value, ok = q.Get(anime)

					if ok && value != nil && value.TimedOut(tick) {
						q.L(context.TODO()).
							Info("block value timeout",
								zap.Stringer("anime", anime),
								zap.Time("updated_at", value.UpdatedAt()),
								zap.Time("tick_at", tick),
							)
						errc <- ErrQueueEntryTimedOut(anime)
						return
					} else if value == nil {
						// Theoritically, this should never happen.., however it was added here
						// to prevent future deadlocks.
						q.L(context.TODO()).
							Info("block value disappearance",
								zap.Stringer("anime", anime),
								zap.Time("tick_at", tick),
							)
						errc <- ErrQueueValueDisappeared(anime)
						return
					}
				}
			}
		} else {
			// Applies the same as the above comment, this is a deadlock prevention.
			q.L(context.TODO()).Info("block value disappearance", zap.Stringer("anime", anime))
			errc <- ErrQueueValueDisappeared(anime)
		}
	}()

	return
}

func (q *Queue) popAwait(anime domain.AnimeBase) {
	blockErr := <-q.block(anime)
	value, ok := q.Get(anime)
	if !ok {
		q.L(context.TODO()).Error(
			"unable to get value for anime during pop",
			zap.Stringer("anime", anime),
		)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), AcquireTTL)
	defer cancel()

	acquireErr := q.semaphore.Acquire(ctx, 1)
	if acquireErr != nil {
		q.L(ctx).Error(
			"failed whilst acquiring semaphore during pop",
			zap.Stringer("anime", anime),
			zap.Error(blockErr),
		)
	}
	details := value.Clone().Details
	q.active.Delete(anime)
	value.Close()

	if blockErr != nil && !errors.Is(blockErr, ErrQueueCanceled) {
		if errors.Is(blockErr, ErrQueueRestart) {
			blockErr = nil // Reset the error back to nil, don't report it.
			q.inactive.PushFront(anime, &State{
				Details:     details,
				StartedFrom: value.StartedFrom,
			})
		} else if len(details) > 1 { // If there's more than one remaining.
			// Silently re-enqueue our item, but don't send out a
			// next channel, (JUST IN CASE) the NextC should happen at the end of this
			// method. PushFront, so that the next Pop() would re-start this item

			q.inactive.PushFront(anime, &State{
				Details:     details[1:], // slice from the first one
				StartedFrom: value.StartedFrom,
			})
		} else {
			blockErr = ErrQueueNoAvailableItems(anime)
		}

		if blockErr != nil {
			q.errc <- blockErr
		}
	}

	q.notify()
}

func (q *Queue) Pop() (anime domain.AnimeBase, state *State, ok bool) {
	anime, state, ok = q.inactive.Pop()

	if ok {
		state.Init()

		q.semaphore.Release(1)
		q.active.Add(anime, state)

		// Spawn a background routine to wait for the download
		// to finalize.
		go q.popAwait(anime)
	}

	return
}
