package queue

import "gitlab.com/eemj/anime-pack/internal/domain"

type Items map[domain.AnimeBase]*State

func (q *Queue) ActiveItems() Items { return q.active.Map() }

func (q *Queue) InactiveItems() Items { return q.inactive.Map() }
