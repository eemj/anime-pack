package queue

func (q *Queue) AddLimit(limit int) {
	q.rwmutex.Lock()
	defer q.rwmutex.Unlock()
	q.semaphore.SetLimit(q.semaphore.GetLimit() + limit)
	q.notifyUnsafe()
}

func (q *Queue) GetLimit() int {
	q.rwmutex.RLock()
	defer q.rwmutex.RUnlock()
	return q.semaphore.GetLimit()
}
