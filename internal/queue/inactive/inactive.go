package inactive

import (
	"container/list"
	"sync"

	"gitlab.com/eemj/anime-pack/internal/queue/state"
)

// Inactive
type Inactive[K comparable, V state.Stateful[V]] interface {
	Len() int64
	Get(k K) (v V, ok bool)
	Contains(k K) bool
	PushFront(k K, v V)
	PushBack(k K, v V)
	Delete(k K)
	Pop() (k K, v V, ok bool)
	Map() map[K]V
	Close() error
}

// keyValue is an array representation of our inactive map.
type keyValue[K any, V state.Stateful[V]] struct {
	K K
	V V
}

// Inactive represents the inactive part of the Queue.
type inactive[K comparable, V state.Stateful[V]] struct {
	list    *list.List
	rwmutex sync.RWMutex
}

func NewInactive[K comparable, V state.Stateful[V]]() Inactive[K, V] {
	return &inactive[K, V]{list: list.New()}
}

// Len returns the number of items stored
func (i *inactive[K, V]) Len() int64 {
	i.rwmutex.RLock()
	defer i.rwmutex.RUnlock()
	return int64(i.list.Len())
}

func (i *inactive[K, V]) elementFor(k K) *list.Element {
	i.rwmutex.RLock()
	defer i.rwmutex.RUnlock()

	for element := i.list.Front(); element != nil; element = element.Next() {
		if kv, ok := element.Value.(keyValue[K, V]); ok && kv.K == k {
			return element
		}
	}

	return nil
}

// Get returns a pointer with the given key's value
func (i *inactive[K, V]) Get(k K) (v V, ok bool) {
	element := i.elementFor(k)

	if ok = element != nil; ok {
		v = element.Value.(keyValue[K, V]).V
	}

	return
}

// Contains returns true/false if the value exists within the inactive list
func (i *inactive[K, V]) Contains(k K) bool {
	_, ok := i.Get(k)
	return ok
}

// PushFront pushes the value at the beginning of the array, value = 0th index
func (i *inactive[K, V]) PushFront(k K, v V) {
	if !i.Contains(k) {
		i.rwmutex.Lock()
		i.list.PushFront(keyValue[K, V]{K: k, V: v})
		i.rwmutex.Unlock()
	}
}

// PushBack pushes the value at the end of the array, value = nth index, n being the length of the array
func (i *inactive[K, V]) PushBack(k K, v V) {
	if !i.Contains(k) {
		i.rwmutex.Lock()
		i.list.PushBack(keyValue[K, V]{K: k, V: v})
		i.rwmutex.Unlock()
	}
}

// Delete deletes an entry from the inactivity list and re-indexes
func (i *inactive[K, V]) Delete(k K) {
	if element := i.elementFor(k); element != nil {
		i.rwmutex.Lock()
		i.list.Remove(element)
		i.rwmutex.Unlock()
	}
}

// Pop returns the first value stored in the items array
func (i *inactive[K, V]) Pop() (k K, v V, ok bool) {
	i.rwmutex.Lock()
	defer i.rwmutex.Unlock()

	element := i.list.Front()
	ok = element != nil

	if ok {
		kv := element.Value.(keyValue[K, V])
		k, v = kv.K, kv.V
		i.list.Remove(element)
	}

	return
}

// GetItems creates a clone of an the items array
func (i *inactive[K, V]) Map() (store map[K]V) {
	i.rwmutex.RLock()
	defer i.rwmutex.RUnlock()

	store = make(map[K]V)

	for element := i.list.Front(); element != nil; element = element.Next() {
		if kv, ok := element.Value.(keyValue[K, V]); ok {
			store[kv.K] = kv.V
		}
	}

	return store
}

func (i *inactive[K, V]) Close() error {
	i.rwmutex.Lock()
	defer i.rwmutex.Unlock()
	i.list.Init()
	return nil
}
