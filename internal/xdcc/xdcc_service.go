package xdcc

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type XDCCService interface {
	CreateMany(ctx context.Context, arg params.InsertXDCCParams) error
	First(ctx context.Context, arg params.FirstXDCCParams) (*domain.XDCC, error)
	Filter(ctx context.Context, arg params.FilterXDCCParams) ([]*domain.XDCC, error)
	Count(ctx context.Context, arg params.CountXDCCParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteXDCCParams) (int64, error)
	SoftDelete(ctx context.Context, arg params.SoftDeleteXDCCParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateXDCCParams) (int64, error)

	Associations() XDCCAssociations
}

type xdccService struct {
	querier      database.QuerierExtended
	cacher       *encoded.EncodedCacher
	associations XDCCAssociations
}

// Associations implements XDCCService.
func (s *xdccService) Associations() XDCCAssociations {
	return s.associations
}

// Count implements XDCCService.
func (s *xdccService) Count(ctx context.Context, arg params.CountXDCCParams) (int64, error) {
	count, err := s.querier.CountXDCC(ctx, database.CountXDCCParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return count, nil
}

// CreateMany implements XDCCService.
func (s *xdccService) CreateMany(ctx context.Context, arg params.InsertXDCCParams) error {
	args := make([]database.InsertXDCCParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertXDCCParams(item)
	}

	bulk := s.querier.InsertXDCC(ctx, args)
	defer bulk.Close()
	return pgxbulk.ExecResult(args, bulk)
}

// Delete implements XDCCService.
func (s *xdccService) Delete(ctx context.Context, arg params.DeleteXDCCParams) (int64, error) {
	return s.querier.DeleteXDCC(ctx, database.DeleteXDCCParams(arg))
}

// Filter implements XDCCService.
func (s *xdccService) Filter(ctx context.Context, arg params.FilterXDCCParams) ([]*domain.XDCC, error) {
	rows, err := s.querier.FilterXDCC(ctx, database.FilterXDCCParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.XDCC{}, nil
		}
		return nil, err
	}
	results := make(domain.XDCCFlatItems, len(rows))
	for index, row := range rows {
		results[index] = domain.XDCCFlat(*row)
	}
	return results.Unflatten(), nil
}

// First implements XDCCService.
func (s *xdccService) First(ctx context.Context, arg params.FirstXDCCParams) (*domain.XDCC, error) {
	row, err := s.querier.FirstXDCC(ctx, database.FirstXDCCParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return domain.XDCCFlat(*row).Unflatten(), nil
}

// SoftDelete implements XDCCService.
func (s *xdccService) SoftDelete(ctx context.Context, arg params.SoftDeleteXDCCParams) (int64, error) {
	return s.querier.SoftDeleteXDCC(ctx, database.SoftDeleteXDCCParams(arg))
}

// Update implements XDCCService.
func (s *xdccService) Update(ctx context.Context, arg params.UpdateXDCCParams) (int64, error) {
	return s.querier.UpdateXDCC(ctx, database.UpdateXDCCParams(arg))
}

func NewXDCCService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations XDCCAssociations,
) XDCCService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &xdccService{
		querier:      querier,
		cacher:       encodedCacher,
		associations: associations,
	}
}
