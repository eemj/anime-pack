package xdcc

import (
	"gitlab.com/eemj/anime-pack/internal/bot"
	"gitlab.com/eemj/anime-pack/internal/episode"
	"gitlab.com/eemj/anime-pack/internal/quality"
	releasegroup "gitlab.com/eemj/anime-pack/internal/release_group"
	titleepisode "gitlab.com/eemj/anime-pack/internal/title/title_episode"
)

type XDCCAssociations interface {
	TitleEpisode() titleepisode.TitleEpisodeService
	Quality() quality.QualityService
	ReleaseGroup() releasegroup.ReleaseGroupService
	Episode() episode.EpisodeService
	Bot() bot.BotService
}

type xdccAssociations struct {
	titleEpisode titleepisode.TitleEpisodeService
	quality      quality.QualityService
	releaseGroup releasegroup.ReleaseGroupService
	episode      episode.EpisodeService
	bot          bot.BotService
}

func (s *xdccAssociations) Bot() bot.BotService                            { return s.bot }
func (s *xdccAssociations) Episode() episode.EpisodeService                { return s.episode }
func (s *xdccAssociations) Quality() quality.QualityService                { return s.quality }
func (s *xdccAssociations) ReleaseGroup() releasegroup.ReleaseGroupService { return s.releaseGroup }
func (s *xdccAssociations) TitleEpisode() titleepisode.TitleEpisodeService { return s.titleEpisode }

func NewXDCCAssociations(
	titleEpisode titleepisode.TitleEpisodeService,
	quality quality.QualityService,
	releaseGroup releasegroup.ReleaseGroupService,
	episode episode.EpisodeService,
	bot bot.BotService,
) XDCCAssociations {
	return &xdccAssociations{
		titleEpisode: titleEpisode,
		quality:      quality,
		releaseGroup: releaseGroup,
		episode:      episode,
		bot:          bot,
	}
}
