package animereview

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pager"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type AnimeReviewService interface {
	Filter(ctx context.Context, arg params.FilterAnimeReviewParams) (*pager.PageSet[domain.AnimeItems], error)
	First(ctx context.Context, arg params.FirstAnimeReviewParams) (*domain.Anime, error)
}

type animeReviewService struct {
	querier      database.QuerierExtended
	cacher       *encoded.EncodedCacher
	associations AnimeReviewAssociations
}

func (s *animeReviewService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.AnimeReviewServiceName)
}

// Filter implements AnimeReviewService.
func (s *animeReviewService) Filter(ctx context.Context, arg params.FilterAnimeReviewParams) (*pager.PageSet[domain.AnimeItems], error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}
	if arg.Title != nil && *arg.Title != "" {
		ids, err := s.associations.AnimeSearch().Search(ctx, *arg.Title)
		if err != nil {
			return nil, err
		}
		arg.IDs = ids
	}

	s.L(ctx).Debug(
		"revised filter",
		zap.Any("filter", arg),
	)

	count, err := s.querier.CountAnimeReview(ctx, database.CountAnimeReviewParams{
		IDs:        arg.IDs,
		Statuses:   arg.Statuses,
		Formats:    arg.Formats,
		Seasons:    arg.Seasons,
		GenreIDs:   arg.GenreIDs,
		StudioIDs:  arg.StudioIDs,
		SeasonYear: arg.SeasonYear,
		Favourite:  arg.Favourite,
	})
	if err != nil && err != pgx.ErrNoRows {
		return nil, err
	}
	if count == 0 {
		return &pager.PageSet[domain.AnimeItems]{
			Page:     arg.Page,
			Elements: arg.Elements,
			Count:    0,
			Data:     domain.AnimeItems{},
		}, nil
	}
	offset, limit := pager.CalculateLimitOffset(arg.Page, arg.Elements)
	rows, err := s.querier.FilterAnimeReview(ctx, database.FilterAnimeReviewParams{
		OrderBy:     arg.OrderBy,
		OrderByDesc: enums.OrderByDirectionIsDescending(arg.OrderByDesc),
		IDs:         arg.IDs,
		Statuses:    arg.Statuses,
		Formats:     arg.Formats,
		Seasons:     arg.Seasons,
		GenreIDs:    arg.GenreIDs,
		StudioIDs:   arg.StudioIDs,
		SeasonYear:  arg.SeasonYear,
		Favourite:   arg.Favourite,
		Offset:      offset,
		Limit:       limit,
	})
	if err != nil {
		return nil, err
	}
	flatItems := make(domain.AnimeFlatItems, len(rows))
	for index, row := range rows {
		flatItems[index] = domain.AnimeFlat(*row)
	}
	result := flatItems.Unflatten()
	result.Prefixify()
	return &pager.PageSet[domain.AnimeItems]{
		Page:     arg.Page,
		Elements: arg.Elements,
		Count:    count,
		Data:     result,
	}, nil
}

// First implements AnimeReviewService.
func (s *animeReviewService) First(ctx context.Context, arg params.FirstAnimeReviewParams) (*domain.Anime, error) {
	rows, err := s.querier.FirstAnimeReview(ctx, database.FirstAnimeReviewParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	flatItems := make(domain.AnimeFlatItems, len(rows))
	for index, row := range rows {
		flatItems[index] = domain.AnimeFlat(*row)
	}

	result := flatItems.Unflatten()
	result.Prefixify()

	if length := len(result); length == 1 {
		return result[0], nil
	}

	return nil, nil
}

func NewAnimeReviewService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations AnimeReviewAssociations,
) AnimeReviewService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &animeReviewService{
		querier:      querier,
		cacher:       encodedCacher,
		associations: associations,
	}
}
