package animereview

import animesearch "gitlab.com/eemj/anime-pack/internal/anime/anime_search"

type AnimeReviewAssociations interface {
	AnimeSearch() animesearch.AnimeSearchService
}

type animeReviewAssociations struct {
	animeSearch animesearch.AnimeSearchService
}

func (s *animeReviewAssociations) AnimeSearch() animesearch.AnimeSearchService { return s.animeSearch }

func NewAnimeReviewAssociations(
	animeSearch animesearch.AnimeSearchService,
) AnimeReviewAssociations {
	return &animeReviewAssociations{
		animeSearch: animeSearch,
	}
}
