package anime

import (
	animeepisode "gitlab.com/eemj/anime-pack/internal/anime/anime_episode"
	animeepisodelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_episode_latest"
	animeformat "gitlab.com/eemj/anime-pack/internal/anime/anime_format"
	animeindex "gitlab.com/eemj/anime-pack/internal/anime/anime_index"
	animelatest "gitlab.com/eemj/anime-pack/internal/anime/anime_latest"
	animemedia "gitlab.com/eemj/anime-pack/internal/anime/anime_media"
	animereview "gitlab.com/eemj/anime-pack/internal/anime/anime_review"
	animeseason "gitlab.com/eemj/anime-pack/internal/anime/anime_season"
	animesimulcast "gitlab.com/eemj/anime-pack/internal/anime/anime_simulcast"
	animestatus "gitlab.com/eemj/anime-pack/internal/anime/anime_status"
	"gitlab.com/eemj/anime-pack/internal/genre"
	"gitlab.com/eemj/anime-pack/internal/image"
	"gitlab.com/eemj/anime-pack/internal/preferences"
	"gitlab.com/eemj/anime-pack/internal/studio"
)

type AnimeAssociations interface {
	Genre() genre.GenreService
	AnimeGenre() AnimeGenreService
	Studio() studio.StudioService
	AnimeMedia() animemedia.AnimeMediaService
	AnimeLatest() animelatest.AnimeLatestService
	AnimeStudio() AnimeStudioService
	AnimeReview() animereview.AnimeReviewService
	AnimeIndex() animeindex.AnimeIndexService
	AnimeEpisode() animeepisode.AnimeEpisodeService
	AnimeSimulcast() animesimulcast.AnimeSimulcastService
	AnimeEpisodeLatest() animeepisodelatest.AnimeEpisodeLatestService
	AnimeStatus() animestatus.AnimeStatusService
	AnimeFormat() animeformat.AnimeFormatService
	AnimeSeason() animeseason.AnimeSeasonService
	Preferences() preferences.PreferencesService
	Image() image.ImageService
}

type animeAssociations struct {
	genre              genre.GenreService
	studio             studio.StudioService
	animeMedia         animemedia.AnimeMediaService
	animeLatest        animelatest.AnimeLatestService
	animeGenre         AnimeGenreService
	animeStudio        AnimeStudioService
	animeIndex         animeindex.AnimeIndexService
	animeReview        animereview.AnimeReviewService
	animeEpisode       animeepisode.AnimeEpisodeService
	animeSimulcast     animesimulcast.AnimeSimulcastService
	animeEpisodeLatest animeepisodelatest.AnimeEpisodeLatestService
	animeStatus        animestatus.AnimeStatusService
	animeFormat        animeformat.AnimeFormatService
	animeSeason        animeseason.AnimeSeasonService
	preferences        preferences.PreferencesService
	image              image.ImageService
}

func (a *animeAssociations) Genre() genre.GenreService                      { return a.genre }
func (a *animeAssociations) Studio() studio.StudioService                   { return a.studio }
func (a *animeAssociations) AnimeMedia() animemedia.AnimeMediaService       { return a.animeMedia }
func (a *animeAssociations) AnimeLatest() animelatest.AnimeLatestService    { return a.animeLatest }
func (a *animeAssociations) AnimeGenre() AnimeGenreService                  { return a.animeGenre }
func (a *animeAssociations) AnimeStudio() AnimeStudioService                { return a.animeStudio }
func (a *animeAssociations) AnimeIndex() animeindex.AnimeIndexService       { return a.animeIndex }
func (a *animeAssociations) AnimeReview() animereview.AnimeReviewService    { return a.animeReview }
func (a *animeAssociations) AnimeEpisode() animeepisode.AnimeEpisodeService { return a.animeEpisode }
func (a *animeAssociations) AnimeSimulcast() animesimulcast.AnimeSimulcastService {
	return a.animeSimulcast
}

func (a *animeAssociations) AnimeEpisodeLatest() animeepisodelatest.AnimeEpisodeLatestService {
	return a.animeEpisodeLatest
}
func (a *animeAssociations) AnimeStatus() animestatus.AnimeStatusService { return a.animeStatus }
func (a *animeAssociations) AnimeFormat() animeformat.AnimeFormatService { return a.animeFormat }
func (a *animeAssociations) AnimeSeason() animeseason.AnimeSeasonService { return a.animeSeason }
func (a *animeAssociations) Preferences() preferences.PreferencesService { return a.preferences }
func (a *animeAssociations) Image() image.ImageService                   { return a.image }

func NewAnimeAssociations(
	genre genre.GenreService,
	studio studio.StudioService,
	animeMedia animemedia.AnimeMediaService,
	animeLatest animelatest.AnimeLatestService,
	animeGenre AnimeGenreService,
	animeStudio AnimeStudioService,
	animeIndex animeindex.AnimeIndexService,
	animeReview animereview.AnimeReviewService,
	animeEpisode animeepisode.AnimeEpisodeService,
	animeSimulcast animesimulcast.AnimeSimulcastService,
	animeEpisodeLatest animeepisodelatest.AnimeEpisodeLatestService,
	animeStatus animestatus.AnimeStatusService,
	animeFormat animeformat.AnimeFormatService,
	animeSeason animeseason.AnimeSeasonService,
	preferences preferences.PreferencesService,
	image image.ImageService,
) AnimeAssociations {
	return &animeAssociations{
		genre:              genre,
		studio:             studio,
		animeMedia:         animeMedia,
		animeLatest:        animeLatest,
		animeGenre:         animeGenre,
		animeStudio:        animeStudio,
		animeIndex:         animeIndex,
		animeReview:        animeReview,
		animeEpisode:       animeEpisode,
		animeSimulcast:     animeSimulcast,
		animeEpisodeLatest: animeEpisodeLatest,
		animeStatus:        animeStatus,
		animeFormat:        animeFormat,
		animeSeason:        animeSeason,
		preferences:        preferences,
		image:              image,
	}
}
