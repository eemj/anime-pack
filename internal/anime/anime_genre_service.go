package anime

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeGenreService interface {
	CreateMany(ctx context.Context, arg params.InsertAnimeGenreParams) ([]*domain.AnimeGenre, error)
	First(ctx context.Context, arg params.FirstAnimeGenreParams) (*domain.AnimeGenre, error)
	Filter(ctx context.Context, arg params.FilterAnimeGenreParams) ([]*domain.AnimeGenre, error)
	Count(ctx context.Context, arg params.CountAnimeGenreParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteAnimeGenreParams) (int64, error)
}

type animeGenreService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements AnimeGenreService.
func (s *animeGenreService) Count(ctx context.Context, arg params.CountAnimeGenreParams) (int64, error) {
	return s.querier.CountAnimeGenre(ctx, database.CountAnimeGenreParams(arg))
}

// CreateMany implements AnimeGenreService.
func (s *animeGenreService) CreateMany(ctx context.Context, arg params.InsertAnimeGenreParams) ([]*domain.AnimeGenre, error) {
	args := make([]database.InsertAnimeGenreParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertAnimeGenreParams(item)
	}

	bulk := s.querier.InsertAnimeGenre(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertAnimeGenreRow,
		*domain.AnimeGenre,
	](bulk, func(row *database.InsertAnimeGenreRow) *domain.AnimeGenre {
		return &domain.AnimeGenre{
			AnimeID: row.AnimeID,
			GenreID: row.GenreID,
		}
	})
}

// Delete implements AnimeGenreService.
func (s *animeGenreService) Delete(ctx context.Context, arg params.DeleteAnimeGenreParams) (int64, error) {
	return s.querier.DeleteAnimeGenre(ctx, database.DeleteAnimeGenreParams(arg))
}

// Filter implements AnimeGenreService.
func (s *animeGenreService) Filter(ctx context.Context, arg params.FilterAnimeGenreParams) ([]*domain.AnimeGenre, error) {
	rows, err := s.querier.FilterAnimeGenre(ctx, database.FilterAnimeGenreParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.AnimeGenre{}, nil
		}
		return nil, err
	}
	results := make([]*domain.AnimeGenre, len(rows))
	for index, row := range rows {
		results[index] = domain.AnimeGenreFlat(*row).Unflatten()
	}
	return results, nil
}

// First implements AnimeGenreService.
func (s *animeGenreService) First(ctx context.Context, arg params.FirstAnimeGenreParams) (*domain.AnimeGenre, error) {
	row, err := s.querier.FirstAnimeGenre(ctx, database.FirstAnimeGenreParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return domain.AnimeGenreFlat(*row).Unflatten(), nil
}

func NewAnimeGenreService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeGenreService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeGenreService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
