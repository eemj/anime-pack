package animeseason

import (
	"context"
	"sort"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeSeasonService interface {
	List(ctx context.Context) (enums.AnimeSeasons, error)
	ListYears(ctx context.Context) ([]*int64, error)
}

type animeSeasonService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// ListYears implements AnimeSeasonService.
func (s *animeSeasonService) ListYears(ctx context.Context) ([]*int64, error) {
	cachedValue, err := encoded.Get[[]*int64](ctx, s.cacher, cachekeys.AnimeSeasonYears)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	seasonYears, err := s.querier.ListAnimeReviewSeasonYear(ctx)
	if err != nil {
		return nil, err
	}
	sort.Slice(seasonYears, func(i, j int) bool {
		firstYear := *seasonYears[i]
		secondYear := *seasonYears[j]
		return firstYear < secondYear
	})
	err = encoded.Set(ctx, s.cacher, cachekeys.AnimeSeasonYears, seasonYears, cacher.DefaultTTL)
	if err != nil {
		return nil, err
	}
	return seasonYears, nil
}

// List implements AnimeSeasonService.
func (s *animeSeasonService) List(ctx context.Context) (enums.AnimeSeasons, error) {
	cachedValue, err := encoded.Get[enums.AnimeSeasons](ctx, s.cacher, cachekeys.AnimeSeasons)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	seasons, err := s.querier.ListAnimeReviewSeason(ctx)
	if err != nil {
		return nil, err
	}
	sort.Sort(sort.Reverse(enums.AnimeSeasonsSortBySeason(seasons)))
	err = encoded.Set(
		ctx,
		s.cacher,
		cachekeys.AnimeSeasons,
		seasons,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return seasons, nil
}

func NewAnimeSeasonService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeSeasonService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeSeasonService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
