package anime

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeStudioService interface {
	CreateMany(ctx context.Context, arg params.InsertAnimeStudioParams) ([]*domain.AnimeStudio, error)
	First(ctx context.Context, arg params.FirstAnimeStudioParams) (*domain.AnimeStudio, error)
	Filter(ctx context.Context, arg params.FilterAnimeStudioParams) ([]*domain.AnimeStudio, error)
	Count(ctx context.Context, arg params.CountAnimeStudioParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteAnimeStudioParams) (int64, error)
}

type animeStudioService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements AnimeStudioService.
func (s *animeStudioService) Count(ctx context.Context, arg params.CountAnimeStudioParams) (int64, error) {
	return s.querier.CountAnimeStudio(ctx, database.CountAnimeStudioParams(arg))
}

// CreateMany implements AnimeStudioService.
func (s *animeStudioService) CreateMany(ctx context.Context, arg params.InsertAnimeStudioParams) ([]*domain.AnimeStudio, error) {
	args := make([]database.InsertAnimeStudioParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertAnimeStudioParams(item)
	}
	bulk := s.querier.InsertAnimeStudio(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertAnimeStudioRow,
		*domain.AnimeStudio,
	](bulk, func(row *database.InsertAnimeStudioRow) *domain.AnimeStudio {
		return &domain.AnimeStudio{
			AnimeID:  row.AnimeID,
			StudioID: row.StudioID,
		}
	})
}

// Delete implements AnimeStudioService.
func (s *animeStudioService) Delete(ctx context.Context, arg params.DeleteAnimeStudioParams) (int64, error) {
	return s.querier.DeleteAnimeStudio(ctx, database.DeleteAnimeStudioParams(arg))
}

// Filter implements AnimeStudioService.
func (s *animeStudioService) Filter(ctx context.Context, arg params.FilterAnimeStudioParams) ([]*domain.AnimeStudio, error) {
	rows, err := s.querier.FilterAnimeStudio(ctx, database.FilterAnimeStudioParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.AnimeStudio{}, nil
		}
		return nil, err
	}
	results := make([]*domain.AnimeStudio, len(rows))
	for index, row := range rows {
		results[index] = domain.AnimeStudioFlat(*row).Unflatten()
	}
	return results, nil
}

// First implements AnimeStudioService.
func (s *animeStudioService) First(ctx context.Context, arg params.FirstAnimeStudioParams) (*domain.AnimeStudio, error) {
	row, err := s.querier.FirstAnimeStudio(ctx, database.FirstAnimeStudioParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return domain.AnimeStudioFlat(*row).Unflatten(), nil
}

func NewAnimeStudioService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeStudioService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeStudioService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
