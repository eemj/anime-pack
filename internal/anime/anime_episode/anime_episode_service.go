package animeepisode

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeEpisodeService interface {
	Filter(ctx context.Context, arg params.FilterAnimeEpisodeParams) (domain.AnimeEpisodes, error)
}

type animeEpisodeService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Filter implements AnimeEpisodeService.
func (s *animeEpisodeService) Filter(ctx context.Context, arg params.FilterAnimeEpisodeParams) (domain.AnimeEpisodes, error) {
	// TODO(eemj): We have a caching issue, if something is FINISHED
	// we could still have new releases coming out for it
	// we need to bust the episodes cache when a new release is out.
	// Ideally we port create release logic into it's own service and handle the cache bust there.
	//
	// cacheKey := cachekeys.AnimeEpisode(arg)

	// values, err := encoded.Get[domain.AnimeEpisodes](ctx, s.cacher, cacheKey)
	// if err != nil {
	// 	return nil, err
	// }
	// if values != nil {
	// 	return values, nil
	// }

	rows, err := s.querier.FilterAnimeEpisode(ctx, database.FilterAnimeEpisodeParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.AnimeEpisode{}, nil
		}
		return nil, err
	}
	// if len(rows) == 0 || (len(rows) > 0 && rows[0].AnimeStatus != enums.AnimeStatusFINISHED) {
	// 	cacheKey = ""
	// }

	items := make(domain.AnimeEpisodeFlatItems, len(rows))
	for index, row := range rows {
		items[index] = domain.AnimeEpisodeFlat(*row)
	}
	values := items.Unflatten()
	// if cacheKey != "" {
	// 	err = encoded.Set(ctx, s.cacher, cacheKey, values, cacher.DefaultTTL)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// }
	return values, nil
}

func NewAnimeEpisodeService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeEpisodeService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeEpisodeService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
