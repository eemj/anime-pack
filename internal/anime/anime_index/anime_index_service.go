package animeindex

import (
	"context"
	"sort"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeIndexService interface {
	ListIndexes(ctx context.Context) ([]string, error)
	ListAnimeByIndex(ctx context.Context, args params.ListAnimeByIndexParams) ([]*domain.Anime, error)
}

type animeIndexService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// ListAnimeByIndex implements AnimeIndexService.
func (s *animeIndexService) ListAnimeByIndex(ctx context.Context, args params.ListAnimeByIndexParams) ([]*domain.Anime, error) {
	rows, err := s.querier.ListAnimeByIndex(ctx, database.ListAnimeByIndexParams(args))
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Anime{}, nil
		}
		return nil, err
	}
	result := make([]*domain.Anime, len(rows))
	for index, row := range rows {
		result[index] = domain.AnimeIndexFlat(*row).Unflatten()
	}
	return result, nil
}

// ListIndexes implements AnimeIndexService.
func (s *animeIndexService) ListIndexes(ctx context.Context) ([]string, error) {
	runes, err := s.querier.ListAnimeIndexes(ctx)
	if err != nil {
		if err == pgx.ErrNoRows {
			return []string{}, nil
		}
		return nil, err
	}
	var (
		result   []string
		nonAlpha = false
	)
	for _, ascii := range runes {
		prefix := rune(ascii)
		if prefix >= 'A' && prefix <= 'Z' {
			result = append(result, string(prefix))
		} else if !nonAlpha {
			nonAlpha = true
		}
	}
	if nonAlpha {
		result = append(result, "#")
	}
	sort.Slice(result, func(i, j int) bool {
		return result[i] < result[j]
	})
	return result, nil
}

func NewAnimeIndexService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeIndexService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeIndexService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
