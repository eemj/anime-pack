package animeepisodelatest

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pager"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type AnimeEpisodeLatestService interface {
	Filter(ctx context.Context, arg params.FilterAnimeEpisodeLatestParams) (*pager.PageSet[domain.AnimeEpisodeLatestItems], error)
}

type animeEpisodeLatestService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

func (a *animeEpisodeLatestService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.AnimeEpisodeLatestServiceName)
}

// Filter implements AnimeEpisodeLatestService.
func (s *animeEpisodeLatestService) Filter(ctx context.Context, arg params.FilterAnimeEpisodeLatestParams) (*pager.PageSet[domain.AnimeEpisodeLatestItems], error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}

	count, err := s.querier.CountAnimeEpisodeLatest(ctx, database.CountAnimeEpisodeLatestParams{
		Status:    arg.Status,
		Favourite: arg.Favourite,
	})
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return &pager.PageSet[domain.AnimeEpisodeLatestItems]{
			Page:     arg.Page,
			Elements: arg.Elements,
			Count:    0,
			Data:     domain.AnimeEpisodeLatestItems{},
		}, nil
	}

	offset, limit := pager.CalculateLimitOffset(arg.Page, arg.Elements)
	rows, err := s.querier.FilterAnimeEpisodeLatest(ctx, database.FilterAnimeEpisodeLatestParams{
		Status:      arg.Status,
		Favourite:   arg.Favourite,
		OrderBy:     arg.OrderBy,
		OrderByDesc: enums.OrderByDirectionIsDescending(arg.OrderByDirection),
		Offset:      offset,
		Limit:       limit,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return &pager.PageSet[domain.AnimeEpisodeLatestItems]{
				Page:     arg.Page,
				Elements: arg.Elements,
				Count:    0,
				Data:     domain.AnimeEpisodeLatestItems{},
			}, nil
		}
		return nil, err
	}

	flatItems := make(domain.AnimeEpisodeLatestFlatItems, len(rows))
	for index, row := range rows {
		flatItems[index] = domain.AnimeEpisodeLatestFlat(*row)
	}
	result := flatItems.Unflatten()
	return &pager.PageSet[domain.AnimeEpisodeLatestItems]{
		Page:     arg.Page,
		Elements: arg.Elements,
		Count:    count,
		Data:     result,
	}, nil
}

func NewAnimeEpisodeLatestService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeEpisodeLatestService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeEpisodeLatestService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
