package anime

import (
	"bytes"
	"context"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/encoded"
)

// Create implements AnimeService.
func (s *animeService) Create(ctx context.Context, arg params.InsertAnimeParams) (*domain.Anime, error) {
	cacheKey := cachekeys.Anime(&arg.ID)
	if cacheKey != "" {
		result, err := encoded.Get[*domain.Anime](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if result != nil {
			return result, nil
		}
	}

	animeArg := database.InsertAnimeParams{
		ID:             arg.ID,
		IDMal:          arg.IDMal,
		StartDate:      arg.StartDate,
		EndDate:        arg.EndDate,
		Score:          arg.Score,
		Description:    arg.Description,
		TitleRomaji:    arg.TitleRomaji,
		TitleEnglish:   arg.TitleEnglish,
		TitleNative:    arg.TitleNative,
		Title:          arg.Title,
		Format:         arg.Format,
		Season:         arg.Season,
		Status:         arg.Status,
		Year:           arg.Year,
		NextAiringDate: arg.NextAiringDate,
	}
	poster, colour, err := s.createAnimePoster(ctx, arg)
	if err != nil {
		return nil, err
	}
	banner, err := s.createAnimeBanner(ctx, arg)
	if err != nil {
		return nil, err
	}
	animeArg.PosterID = poster.ID
	animeArg.Colour = colour
	if banner != nil {
		animeArg.BannerID = &banner.ID
	}

	row, err := s.querier.InsertAnime(ctx, animeArg)
	if err != nil {
		return nil, err
	}
	anime := &domain.Anime{
		ID:             row.ID,
		IDMal:          row.IDMal,
		StartDate:      row.StartDate,
		EndDate:        row.EndDate,
		Score:          row.Score,
		Description:    row.Description,
		TitleRomaji:    row.TitleRomaji,
		TitleEnglish:   row.TitleEnglish,
		TitleNative:    row.TitleNative,
		Title:          row.Title,
		PosterID:       row.PosterID,
		BannerID:       row.BannerID,
		Colour:         row.Colour,
		Year:           row.Year,
		NextAiringDate: row.NextAiringDate,
		CreatedAt:      row.CreatedAt,
		UpdatedAt:      row.UpdatedAt,
		Status:         row.Status,
		Format:         row.Format,
		Season:         row.Season,
		Poster:         *poster,
		Banner:         banner,
	}
	anime.Genres, err = s.createAnimeGenres(ctx, arg)
	if err != nil {
		return nil, err
	}
	anime.Studios, err = s.createAnimeStudios(ctx, arg)
	if err != nil {
		return nil, err
	}

	if cacheKey != "" {
		// TODO(eemj): Configurable cacher.DefaultTTL
		err = encoded.Set(ctx, s.cacher, cacheKey, anime, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}

	return anime, nil
}

func (s *animeService) createAnimeGenres(ctx context.Context, arg params.InsertAnimeParams) ([]*domain.Genre, error) {
	if len(arg.GenreNames) == 0 {
		return []*domain.Genre{}, nil
	}

	genres, err := s.associations.Genre().CreateMany(ctx, params.InsertGenreParams{
		Names: arg.GenreNames,
	})
	if err != nil {
		return nil, err
	}
	animeGenreArg := make([]params.InsertAnimeGenreParamsItem, len(genres))
	for index, genre := range genres {
		animeGenreArg[index] = params.InsertAnimeGenreParamsItem{
			AnimeID: arg.ID,
			GenreID: genre.ID,
		}
	}
	_, err = s.associations.AnimeGenre().CreateMany(ctx, params.InsertAnimeGenreParams{
		Items: animeGenreArg,
	})
	if err != nil {
		return nil, err
	}
	return genres, nil
}

func (s *animeService) createAnimeStudios(ctx context.Context, arg params.InsertAnimeParams) ([]*domain.Studio, error) {
	if len(arg.StudioNames) == 0 {
		return []*domain.Studio{}, nil
	}

	studios, err := s.associations.Studio().CreateMany(ctx, params.InsertStudioParams{
		Names: arg.StudioNames,
	})
	if err != nil {
		return nil, err
	}
	animeStudioArg := make([]params.InsertAnimeStudioParamsItem, len(studios))
	for index, studio := range studios {
		animeStudioArg[index] = params.InsertAnimeStudioParamsItem{
			AnimeID:  arg.ID,
			StudioID: studio.ID,
		}
	}
	_, err = s.associations.AnimeStudio().CreateMany(ctx, params.InsertAnimeStudioParams{
		Items: animeStudioArg,
	})
	if err != nil {
		return nil, err
	}
	return studios, nil
}

func (s *animeService) createAnimeBanner(ctx context.Context, arg params.InsertAnimeParams) (banner *domain.Banner, err error) {
	if arg.BannerUri == nil {
		return nil, nil
	}

	bannerBytes, err := s.associations.Image().Processor().ReadImage(ctx, *arg.BannerUri)
	if err != nil {
		return
	}
	banner, err = s.associations.Image().Banner().CreateFromReader(ctx, params.CreateFromReaderImageParams{
		Reader: bytes.NewReader(bannerBytes),
		Uri:    *arg.BannerUri,
	})
	if err != nil {
		return nil, err
	}
	return banner, nil
}

func (s *animeService) createAnimePoster(ctx context.Context, arg params.InsertAnimeParams) (poster *domain.Poster, colour *string, err error) {
	posterBytes, err := s.associations.Image().Processor().ReadImage(ctx, arg.PosterUri)
	if err != nil {
		return nil, nil, err
	}

	if arg.Colour == nil || *arg.Colour == "" {
		colour = s.associations.Image().Processor().DominantColour(ctx, bytes.NewReader(posterBytes))
	} else {
		colour = arg.Colour
	}

	poster, err = s.associations.Image().Poster().CreateFromReader(ctx, params.CreateFromReaderImageParams{
		Reader: bytes.NewReader(posterBytes),
		Uri:    arg.PosterUri,
	})
	if err != nil {
		return nil, nil, err
	}
	return poster, colour, nil
}
