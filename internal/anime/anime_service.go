package anime

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/apis"
	"gitlab.com/eemj/anime-pack/internal/apis/models"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pager"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

type AnimeService interface {
	Create(ctx context.Context, arg params.InsertAnimeParams) (*domain.Anime, error)
	Filter(ctx context.Context, arg params.FilterAnimeParams) ([]*domain.Anime, error)
	Count(ctx context.Context, arg params.CountAnimeParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateAnimeParams) (int64, error)
	Delete(ctx context.Context, arg params.DeleteAnimeParams) (int64, error)

	UpdateReleasing(ctx context.Context) error
	CreateFromThirdParty(ctx context.Context, media models.Media) (*domain.Anime, error)
	CreateFromTitle(ctx context.Context, title domain.Title) ([]*domain.Anime, bool, error)

	Associations() AnimeAssociations
}

type animeService struct {
	querier      database.QuerierExtended
	cacher       *encoded.EncodedCacher
	associations AnimeAssociations
	apis         *apis.APIS // FIXME(eemj): Abstract this
}

// Associations implements AnimeService.
func (s *animeService) Associations() AnimeAssociations { return s.associations }

// Count implements AnimeService.
func (s *animeService) Count(ctx context.Context, arg params.CountAnimeParams) (int64, error) {
	return s.querier.CountAnime(ctx, database.CountAnimeParams(arg))
}

// Filter implements AnimeService.
func (s *animeService) Filter(ctx context.Context, arg params.FilterAnimeParams) ([]*domain.Anime, error) {
	offset, limit := pager.CalculateLimitOffset(arg.Page, arg.Elements)
	rows, err := s.querier.FilterAnime(ctx, database.FilterAnimeParams{
		ID:             arg.ID,
		IDMal:          arg.IDMal,
		StartDate:      arg.StartDate,
		EndDate:        arg.EndDate,
		Score:          arg.Score,
		Description:    arg.Description,
		TitleRomaji:    arg.TitleRomaji,
		TitleEnglish:   arg.TitleEnglish,
		TitleNative:    arg.TitleNative,
		Title:          arg.Title,
		Format:         arg.Format,
		Poster:         arg.Poster,
		Banner:         arg.Banner,
		Colour:         arg.Colour,
		Status:         arg.Status,
		Year:           arg.Year,
		Season:         arg.Season,
		NextAiringDate: arg.NextAiringDate,
		OrderBy:        arg.OrderBy.String(),
		OrderByDesc:    arg.OrderByDesc,
		Offset:         offset,
		Limit:          limit,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Anime{}, nil
		}
		return nil, err
	}
	results := make(domain.AnimeFlatItems, len(rows))
	for index, row := range rows {
		results[index] = domain.AnimeFlat(*row)
	}
	return results.Unflatten(), nil
}

// Update implements AnimeService.
func (s *animeService) Update(ctx context.Context, arg params.UpdateAnimeParams) (int64, error) {
	return s.querier.UpdateAnime(ctx, database.UpdateAnimeParams(arg))
}

func (s *animeService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.AnimeServiceName)
}

func NewAnimeService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
	associations AnimeAssociations,
) AnimeService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &animeService{
		querier: querier,
		cacher:  encodedCacher,
		apis:    apis.New(),

		associations: associations,
	}
}
