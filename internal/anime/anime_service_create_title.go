package anime

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/apis/anilist"
	"gitlab.com/eemj/anime-pack/internal/apis/models"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	stringutil "go.jamie.mt/toolbox/string_util"
)

func tryMatchTitle(
	seasonYear *int,
	titleNames []string,
	results []*anilist.Media,
) *anilist.Media {
	for _, title := range titleNames {
		title = utils.LowerCaseRemoveSpecial(title)
		for _, result := range results {
			if seasonYear != nil && result.SeasonYear > 0 && result.SeasonYear != *seasonYear {
				continue
			}

			userPreferred := utils.LowerCaseRemoveSpecial(result.Title.UserPreferred)
			if userPreferred == title {
				return result
			}

			if result.Title.English != nil && utils.LowerCaseRemoveSpecial(*result.Title.English) == title {
				return result
			}
			if result.Title.Romaji != nil && utils.LowerCaseRemoveSpecial(*result.Title.Romaji) == title {
				return result
			}
			if result.Title.Native != nil && utils.LowerCaseRemoveSpecial(*result.Title.Native) == title {
				return result
			}
		}
	}
	return nil
}

func (s *animeService) CreateFromTitle(ctx context.Context, title domain.Title) ([]*domain.Anime, bool, error) {
	names := title.Names()
	results, err := s.apis.AniList.Search(ctx, names)
	if err != nil {
		return nil, false, err
	}
	if len(results) == 0 {
		return []*domain.Anime{}, false, nil
	}
	match := tryMatchTitle(title.Year, names, results)
	reviewed := match != nil

	// Since we found the match, set the results to only our match
	// to prevent additional iterations.
	if reviewed {
		results = []*anilist.Media{match}
	}

	animes := make([]*domain.Anime, 0)
	for _, result := range results {
		anime, err := s.CreateFromThirdParty(ctx, *result.Model())
		if err != nil {
			return nil, false, err
		}
		animes = append(animes, anime)
	}

	if reviewed {
		anime := animes[0]
		_, err = s.associations.Preferences().Create(ctx, params.InsertPreferencesParams{
			AnimeID: anime.ID,
		})
		if err != nil {
			return animes, reviewed, err
		}
	}

	return animes, reviewed, nil
}

func apiModelToInsertParam(media models.Media) params.InsertAnimeParams {
	arg := params.InsertAnimeParams{
		ID:             media.ID,
		IDMal:          media.IDMAL,
		Score:          int64(media.Score),
		Description:    media.Description,
		GenreNames:     media.Genres,
		StudioNames:    media.Studios,
		Title:          media.Title,
		StartDate:      media.StartDate,
		EndDate:        media.EndDate,
		Format:         enums.AnimeFormat(media.Format),
		Status:         enums.AnimeStatus(media.Status),
		Colour:         media.Colour,
		NextAiringDate: media.NextAiringDate,
		PosterUri:      media.Poster,
		BannerUri:      media.Banner,
	}
	if media.TitleEnglish != nil && !stringutil.IsTrimmedEmpty(*media.TitleEnglish) {
		arg.TitleEnglish = media.TitleEnglish
	}
	if media.TitleRomaji != nil && !stringutil.IsTrimmedEmpty(*media.TitleRomaji) {
		arg.TitleRomaji = media.TitleRomaji
	}
	if media.TitleJapanese != nil && !stringutil.IsTrimmedEmpty(*media.TitleJapanese) {
		arg.TitleNative = media.TitleJapanese
	}
	if media.Season != nil && *media.Season != "" {
		arg.Season = new(enums.AnimeSeason)
		*arg.Season = enums.AnimeSeason(*media.Season)
	}
	if media.SeasonYear != nil && *media.SeasonYear > 0 {
		arg.Year = new(int64)
		*arg.Year = int64(*media.SeasonYear)
	}
	return arg
}
