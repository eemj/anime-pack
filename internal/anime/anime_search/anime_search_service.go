package animesearch

import (
	"context"
	"regexp"
	"sort"
	"strings"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/logx/log"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
	"go.uber.org/zap"
)

var blacklist = regexp.MustCompile(`[\p{Han}\p{Katakana}\p{Hiragana}\p{Hangul}\p{Arabic}]+`)

type AnimeSearchService interface {
	Search(ctx context.Context, phrase string) ([]int64, error)
}

type animeSearchService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

func (s *animeSearchService) L(ctx context.Context) *zap.Logger {
	return log.
		WithTrace(ctx).
		Named(common.AnimeSearchServiceName)
}

func (s *animeSearchService) List(ctx context.Context) (domain.AnimeSearchItems, error) {
	result, err := encoded.Get[domain.AnimeSearchItems](ctx, s.cacher, cachekeys.AnimeSearchSet)
	if err != nil {
		return nil, err
	}
	if result != nil {
		return result, nil
	}

	rows, err := s.querier.ListAnimeReviewSearch(ctx)
	if err != nil {
		if err == pgx.ErrNoRows {
			return domain.AnimeSearchItems{}, nil
		}
		return nil, err
	}

	result = make(domain.AnimeSearchItems, 0, len(rows))
	for _, row := range rows {
		if blacklist.MatchString(row.Title) {
			continue
		}
		result = append(result, (*domain.AnimeSearch)(row))
	}

	sort.Sort(domain.AnimeSearchItemsSortByTitle(result))

	err = encoded.Set(
		ctx,
		s.cacher,
		cachekeys.AnimeSearchSet,
		result,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (s *animeSearchService) Search(ctx context.Context, phrase string) ([]int64, error) {
	// FIXME(eemj): Ideally we use something like bleve or elasticsearch here to do this
	// but 1st we need to get this thing to work.
	items, err := s.List(ctx)
	if err != nil {
		return nil, err
	}
	var (
		patterns       = strings.Fields(phrase)
		patternsLength = len(patterns)
		search         = &domain.AnimeSearchContext{}
	)
	for _, item := range items {
		if score := utils.RabinKarp(item.Title, patterns); score == patternsLength {
			search.Append(&domain.AnimeSearchScore{
				AnimeSearch: *item,
				Score:       score,
			})
		}
	}
	result := search.Results()
	return result, nil
}

func NewAnimeSearchService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeSearchService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeSearchService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
