package animestatus

import (
	"context"
	"sort"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeStatusService interface {
	List(ctx context.Context) (enums.AnimeStatuses, error)
}

type animeStatusService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// List implements AnimeStatusService.
func (f *animeStatusService) List(ctx context.Context) (enums.AnimeStatuses, error) {
	cachedValue, err := encoded.Get[enums.AnimeStatuses](ctx, f.cacher, cachekeys.AnimeStatuses)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	rows, err := f.querier.ListAnimeReviewStatus(ctx)
	if err != nil {
		return nil, err
	}
	result := make(enums.AnimeStatuses, len(rows))
	for index := range rows {
		result[index] = &rows[index]
	}
	sort.Sort(sort.Reverse(enums.AnimeStatusesSortByValue(result)))
	err = encoded.Set(
		ctx,
		f.cacher,
		cachekeys.AnimeStatuses,
		result,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func NewAnimeStatusService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeStatusService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeStatusService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
