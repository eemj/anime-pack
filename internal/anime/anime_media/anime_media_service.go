package animemedia

import (
	"context"
	"net/http"

	"gitlab.com/eemj/anime-pack/internal/apis"
	"gitlab.com/eemj/anime-pack/internal/apis/anilist"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/common/apperror"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeMediaService interface {
	Filter(ctx context.Context, arg params.FilterAnimeMediaParams) (*domain.Anime, error)
	Airing(ctx context.Context) ([]*domain.Anime, error)
	Search(ctx context.Context, arg params.SearchAnimeMediaParams) ([]*domain.Anime, error)
}

type animeMediaService struct {
	apis    *apis.APIS
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Filter implements AnimeMediaService.
func (s *animeMediaService) Filter(ctx context.Context, arg params.FilterAnimeMediaParams) (*domain.Anime, error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}

	var (
		cacheKey = cachekeys.AnimeMedia(arg)
		result   *domain.Anime
	)

	if cacheKey != "" {
		result, err = encoded.Get[*domain.Anime](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if result != nil {
			return result, nil
		}
	}

	var media *anilist.Media
	if arg.AnilistID != nil {
		media, err = s.apis.AniList.GetMediaByID(ctx, uint(*arg.AnilistID))
	} else {
		media, err = s.apis.AniList.GetMediaByIDMAL(ctx, uint(*arg.MalID))
	}

	if err != nil {
		anilistErr, ok := err.(anilist.ErrorResponse)
		if ok && anilistErr.Status == http.StatusNotFound {
			return nil, apperror.ErrThirdPartyAnimeDoesNotExist
		}
		return nil, err
	}

	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}

	result = anilistMediaToDomainAnime(*media)
	return result, nil
}

func (s animeMediaService) Search(ctx context.Context, arg params.SearchAnimeMediaParams) ([]*domain.Anime, error) {
	medias, err := s.apis.AniList.Search(ctx, arg.Titles)
	if err != nil {
		return nil, err
	}
	anime := make([]*domain.Anime, len(medias))
	for index, media := range medias {
		anime[index] = anilistMediaToDomainAnime(*media)
	}
	return anime, nil
}

func anilistMediaToDomainAnime(media anilist.Media) *domain.Anime {
	var (
		anilistModel = media.Model()
		genres       = make([]*domain.Genre, len(anilistModel.Genres))
		studios      = make([]*domain.Studio, len(anilistModel.Studios))
		format       enums.AnimeFormat
		status       enums.AnimeStatus
		season       *enums.AnimeSeason
		seasonYear   *int64
		banner       *domain.Banner
	)

	for index, genre := range anilistModel.Genres {
		genres[index] = &domain.Genre{
			Name: genre,
		}
	}
	for index, studio := range anilistModel.Studios {
		studios[index] = &domain.Studio{
			Name: studio,
		}
	}
	if anilistModel.Format != "" {
		format = enums.AnimeFormat(anilistModel.Format)
	}
	if anilistModel.Status != "" {
		status = enums.AnimeStatus(anilistModel.Status)
	}
	if anilistModel.Season != nil && *anilistModel.Season != "" {
		season = enums.AnimeSeason(*anilistModel.Season).Enum()
	}
	if anilistModel.SeasonYear != nil && *anilistModel.SeasonYear > 0 {
		seasonYear = new(int64)
		*seasonYear = int64(*anilistModel.SeasonYear)
	}
	if anilistModel.Banner != nil && *anilistModel.Banner != "" {
		banner = &domain.Banner{
			Name: *anilistModel.Banner,
			Uri:  *anilistModel.Banner,
		}
	}

	return &domain.Anime{
		ID:             anilistModel.ID,
		IDMal:          anilistModel.IDMAL,
		Title:          anilistModel.Title,
		TitleEnglish:   anilistModel.TitleEnglish,
		TitleNative:    anilistModel.TitleJapanese,
		TitleRomaji:    anilistModel.TitleRomaji,
		StartDate:      anilistModel.StartDate,
		EndDate:        anilistModel.EndDate,
		Description:    anilistModel.Description,
		NextAiringDate: anilistModel.NextAiringDate,
		Score:          int64(anilistModel.Score),
		Poster: domain.Poster{
			Name: anilistModel.Poster,
			Uri:  anilistModel.Poster,
		},
		Colour:  anilistModel.Colour,
		Banner:  banner,
		Genres:  genres,
		Studios: studios,
		Format:  format,
		Status:  status,
		Year:    seasonYear,
		Season:  season,
	}
}

func NewAnimeMediaService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeMediaService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &animeMediaService{
		apis:    apis.New(),
		querier: querier,
		cacher:  encodedCacher,
	}
}
