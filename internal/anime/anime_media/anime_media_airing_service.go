package animemedia

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/domain"
)

func (s *animeMediaService) Airing(ctx context.Context) ([]*domain.Anime, error) {
	anilistAiring, err := s.apis.AniList.GetAiring(ctx)
	if err != nil {
		return nil, err
	}
	result := make([]*domain.Anime, len(anilistAiring))
	for index, item := range anilistAiring {
		result[index] = anilistMediaToDomainAnime(*item)
	}
	return result, nil
}
