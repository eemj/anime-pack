package anime

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/apis/models"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
)

func (s *animeService) CreateFromThirdParty(ctx context.Context, media models.Media) (*domain.Anime, error) {
	filter, err := s.Filter(ctx, params.FilterAnimeParams{
		ID: &media.ID,
	})
	if err != nil {
		return nil, err
	}
	if len(filter) > 0 {
		return filter[0], nil
	}
	anime, err := s.Create(ctx, apiModelToInsertParam(media))
	if err != nil {
		return nil, err
	}
	return anime, nil
}
