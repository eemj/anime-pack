package animeformat

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeFormatService interface {
	List(ctx context.Context) (enums.AnimeFormats, error)
}

type animeFormatService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// List implements AnimeFormatService.
func (f *animeFormatService) List(ctx context.Context) (enums.AnimeFormats, error) {
	cachedValue, err := encoded.Get[enums.AnimeFormats](ctx, f.cacher, cachekeys.AnimeFormats)
	if err != nil {
		return nil, err
	}
	if cachedValue != nil {
		return cachedValue, nil
	}
	rows, err := f.querier.ListAnimeReviewFormat(ctx)
	if err != nil {
		return nil, err
	}
	result := make(enums.AnimeFormats, len(rows))
	for index := range rows {
		result[index] = &rows[index]
	}
	err = encoded.Set(
		ctx,
		f.cacher,
		cachekeys.AnimeFormats,
		result,
		cacher.DefaultTTL,
	)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func NewAnimeFormatService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeFormatService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeFormatService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
