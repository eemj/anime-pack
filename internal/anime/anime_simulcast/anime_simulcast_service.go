package animesimulcast

import (
	"context"
	"sort"
	"time"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeSimulcastService interface {
	List(ctx context.Context) (domain.AnimeSimulcasts, error)
	Refresh(ctx context.Context) error
}

type animeSimulcastService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Refresh implements AnimeSimulcastService.
func (s *animeSimulcastService) Refresh(ctx context.Context) error {
	err := s.cacher.Delete(ctx, cachekeys.Simulcasts)
	if err != nil {
		return err
	}
	_, err = s.List(ctx)
	if err != nil {
		return err
	}
	return nil
}

// List implements AnimeSimulcastService.
func (s *animeSimulcastService) List(ctx context.Context) (domain.AnimeSimulcasts, error) {
	result, err := encoded.Get[domain.AnimeSimulcasts](ctx, s.cacher, cachekeys.Simulcasts)
	if err != nil {
		return nil, err
	}

	if result == nil {
		// Fetch the new simulcast
		rows, err := s.querier.ListAnimeSimulcast(ctx)
		if err != nil {
			if err == pgx.ErrNoRows {
				return domain.AnimeSimulcasts{}, nil
			}
			return nil, err
		}
		flatItems := make(domain.AnimeSimulcastFlatItems, len(rows))
		for index, row := range rows {
			flatItems[index] = domain.AnimeSimulcastFlat(*row)
		}
		result = flatItems.Unflatten()
		result.Prefixify()
		err = encoded.Set(
			ctx,
			s.cacher,
			cachekeys.Simulcasts,
			result,
			utils.TillEndOfDay(),
		)
		if err != nil {
			return nil, err
		}
	}

	// arrange the time frames, episodes that have been cast already
	// we need to add a week (post-cache operation).
	utcNow := time.Now().UTC()
	for _, item := range result {
		if item.NextAiringDate.Before(utcNow) {
			item.NextAiringDate = item.NextAiringDate.AddDate(0, 0, 7)
		}
	}

	// sort by NextAiringDate descending
	sort.Stable(domain.AnimeSimulcastsSortByNextAiringDate(result))

	return result, nil
}

func NewAnimeSimulcastService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeSimulcastService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.MsgPack{})

	return &animeSimulcastService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
