package animelatest

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/internal/pager"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type AnimeLatestService interface {
	Filter(ctx context.Context, arg params.FilterAnimeLatestParams) (*pager.PageSet[domain.AnimeLatestItems], error)
}

type animeLatestService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Filter implements AnimeLatestService.
func (s *animeLatestService) Filter(ctx context.Context, arg params.FilterAnimeLatestParams) (*pager.PageSet[domain.AnimeLatestItems], error) {
	err := arg.Validate()
	if err != nil {
		return nil, err
	}
	count, err := s.querier.CountAnimeLatest(ctx, database.CountAnimeLatestParams{
		Statuses:  arg.Statuses,
		Formats:   arg.Formats,
		Favourite: arg.Favourite,
	})
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return &pager.PageSet[domain.AnimeLatestItems]{
			Page:     arg.Page,
			Elements: arg.Elements,
			Count:    0,
			Data:     domain.AnimeLatestItems{},
		}, nil
	}
	offset, limit := pager.CalculateLimitOffset(arg.Page, arg.Elements)
	rows, err := s.querier.FilterAnimeLatest(ctx, database.FilterAnimeLatestParams{
		Statuses:    arg.Statuses,
		Formats:     arg.Formats,
		Favourite:   arg.Favourite,
		OrderBy:     arg.OrderBy,
		OrderByDesc: enums.OrderByDirectionIsDescending(arg.OrderByDesc),
		Offset:      offset,
		Limit:       limit,
	})
	if err != nil {
		return nil, err
	}
	items := make(domain.AnimeLatestFlatItems, len(rows))
	for index, row := range rows {
		items[index] = domain.AnimeLatestFlat(*row)
	}
	result := items.Unflatten()
	return &pager.PageSet[domain.AnimeLatestItems]{
		Page:     arg.Page,
		Elements: arg.Elements,
		Count:    count,
		Data:     result,
	}, nil
}

func NewAnimeLatestService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) AnimeLatestService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &animeLatestService{
		cacher:  encodedCacher,
		querier: querier,
	}
}
