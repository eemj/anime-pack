package anime

import (
	"context"

	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"go.uber.org/zap"
)

// Delete implements AnimeService.
func (s *animeService) Delete(ctx context.Context, arg params.DeleteAnimeParams) (int64, error) {
	// Retrieve to make sure that this anime exists.
	anime, err := s.querier.FirstAnime(ctx, database.FirstAnimeParams{
		ID: &arg.ID,
	})
	if err != nil {
		return 0, nil
	}
	if anime == nil { // If it does not exist, return 0 rowsAffected with an empty error.
		return 0, nil
	}

	rowsAffected, err := s.associations.AnimeGenre().Delete(ctx, params.DeleteAnimeGenreParams{
		AnimeID: arg.ID,
	})
	if err != nil {
		return 0, err
	}
	s.L(ctx).Debug("delete `anime_genre`", zap.Int64("rows_affected", rowsAffected))

	rowsAffected, err = s.associations.AnimeStudio().Delete(ctx, params.DeleteAnimeStudioParams{
		AnimeID: arg.ID,
	})
	if err != nil {
		return 0, err
	}
	s.L(ctx).Debug("delete `anime_studio`", zap.Int64("rows_affected", rowsAffected))

	rowsAffected, err = s.associations.Preferences().Delete(ctx, params.DeletePreferencesParams{
		AnimeIDs: []int64{anime.ID},
	})
	if err != nil {
		return 0, err
	}
	s.L(ctx).Debug("delete `preferences`", zap.Int64("rows_affected", rowsAffected))

	rowsAffected, err = s.querier.DeleteAnime(ctx, database.DeleteAnimeParams(arg))
	if err != nil {
		return 0, err
	}
	s.L(ctx).Debug("delete `anime`", zap.Int64("rows_affected", rowsAffected))

	rowsAffected, err = s.associations.Image().Poster().Delete(ctx, params.DeletePosterParams{
		ID: anime.PosterID,
	})
	if err != nil {
		return 0, err
	}
	s.L(ctx).Debug("delete `poster`", zap.Int64("rows_affected", rowsAffected))

	if anime.BannerID != nil && *anime.BannerID > 0 {
		rowsAffected, err = s.associations.Image().Banner().Delete(ctx, params.DeleteBannerParams{
			ID: *anime.BannerID,
		})
		if err != nil {
			return 0, err
		}
		s.L(ctx).Debug("delete `banner`", zap.Int64("rows_affected", rowsAffected))
	}

	return 0, nil
}
