package anime

import (
	"bytes"
	"context"
	"strings"

	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/enums"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	"gitlab.com/eemj/anime-pack/pkg/utils"
	"go.uber.org/zap"
)

func (s *animeService) UpdateReleasing(ctx context.Context) (err error) {
	paginatedReviewedAnime, err := s.associations.AnimeReview().Filter(ctx, params.FilterAnimeReviewParams{
		Statuses:    []enums.AnimeStatus{enums.AnimeStatusRELEASING},
		OrderBy:     enums.AnimeOrderByID.String(),
		OrderByDesc: enums.OrderByDirectionDESCENDING.String(),
		Elements:    100,
		Page:        1,
	})
	if err != nil {
		return err
	}

	simulcasts, err := s.associations.AnimeSimulcast().List(ctx)
	if err != nil {
		return err
	}

	airingMedia, err := s.associations.AnimeMedia().Airing(ctx)
	if err != nil {
		return err
	}

	// Check against the third party.
	for _, anime := range paginatedReviewedAnime.Data {
		// Attempt to find the anime in the airing list.
		var foundMedia *domain.Anime
		for _, media := range airingMedia {
			if media.ID == anime.ID {
				foundMedia = media
				break
			}
		}
		// We didn't find the anime, possibly it's no longer listed as airing
		// on our third party provider service.
		if foundMedia == nil {
			foundMedia, err = s.associations.AnimeMedia().Filter(ctx, params.FilterAnimeMediaParams{
				AnilistID: &anime.ID,
			})
			if err != nil {
				return err
			}
		}
		// Compare and update
		if err = s.compareAndUpdate(ctx, simulcasts, foundMedia, anime); err != nil {
			return err
		}
	}

	// Check against our database now.
	for _, media := range airingMedia {
		anime, err := s.associations.AnimeReview().First(ctx, params.FirstAnimeReviewParams{ID: media.ID})
		if err != nil {
			return err
		}
		if anime == nil { // Anime is not in our system.
			continue
		}
		err = s.compareAndUpdate(ctx, simulcasts, media, anime)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *animeService) compareAndUpdate(
	ctx context.Context,
	simulcasts domain.AnimeSimulcasts,
	animeMedia, currentAnime *domain.Anime,
) error {
	updateParams := params.UpdateAnimeParams{
		ID: currentAnime.ID,
	}

	// Check if the anime is in our simulcasts, temporary solution till
	// we'll create a release manager microservice that maps out
	// the crunchyroll relations into the respective anime..
	var simulcast *domain.AnimeSimulcast
	for _, currentSimulcast := range simulcasts {
		if currentSimulcast.ID == currentAnime.ID {
			simulcast = currentSimulcast
			break
		}
	}

	// Overwrite the media status to releasing, since it's available
	// in our simulcast..
	if simulcast != nil {
		animeMedia.Status = enums.AnimeStatusRELEASING
	}

	// Compare statuses
	if currentAnime.Status != animeMedia.Status {
		updateParams.Status = &animeMedia.Status
	}

	// Compare english title
	if (currentAnime.TitleEnglish == nil && animeMedia.TitleEnglish != nil) ||
		(currentAnime.TitleEnglish != nil && animeMedia.TitleEnglish == nil) ||
		(currentAnime.TitleEnglish != nil && animeMedia.TitleEnglish != nil && *currentAnime.TitleEnglish != *animeMedia.TitleEnglish) {
		updateParams.TitleEnglish = animeMedia.TitleEnglish
	}

	// Compare native title
	if (currentAnime.TitleNative == nil && animeMedia.TitleNative != nil) ||
		(currentAnime.TitleNative != nil && animeMedia.TitleNative == nil) ||
		(currentAnime.TitleNative != nil && animeMedia.TitleNative != nil && *currentAnime.TitleNative != *animeMedia.TitleNative) {
		updateParams.TitleNative = animeMedia.TitleNative
	}

	// Compare romaji title
	if (currentAnime.TitleRomaji == nil && animeMedia.TitleRomaji != nil) ||
		(currentAnime.TitleRomaji != nil && animeMedia.TitleRomaji == nil) ||
		(currentAnime.TitleRomaji != nil && animeMedia.TitleRomaji != nil && *currentAnime.TitleRomaji != *animeMedia.TitleRomaji) {
		updateParams.TitleRomaji = animeMedia.TitleRomaji
	}

	var (
		previousPosterID  *int64
		previousBannerID  *int64
		posterBytes       []byte
		posterNeedsUpdate = !strings.EqualFold(currentAnime.Poster.Uri, animeMedia.Poster.Uri)
		bannerNeedsUpdate = (animeMedia.Banner == nil && animeMedia.Banner != nil)
		colourNeedsUpdate = (currentAnime.Colour != nil && *currentAnime.Colour == "") &&
			(animeMedia.Colour != nil && *animeMedia.Colour == "") &&
			animeMedia.Poster.Uri != ""
	)

	// Compare posters
	if posterNeedsUpdate {
		posterBytes, err := s.associations.Image().Processor().ReadImage(ctx, animeMedia.Poster.Uri)
		if err != nil {
			return err
		}

		// Create the new poster.
		image, err := s.associations.Image().Poster().CreateFromReader(ctx, params.CreateFromReaderImageParams{
			Reader: bytes.NewReader(posterBytes),
			Uri:    animeMedia.Poster.Uri,
		})
		if err != nil {
			return err
		}

		previousPosterID = &currentAnime.PosterID
		updateParams.PosterID = &image.ID
	}

	// Compare banners
	if bannerNeedsUpdate {
		// Buffer the banner
		bannerBytes, err := s.associations.Image().Processor().ReadImage(ctx, animeMedia.Banner.Uri)
		if err != nil {
			return err
		}

		// Create the new banner.
		image, err := s.associations.Image().Banner().CreateFromReader(ctx, params.CreateFromReaderImageParams{
			Reader: bytes.NewReader(bannerBytes),
			Uri:    animeMedia.Banner.Uri,
		})
		if err != nil {
			return err
		}

		previousBannerID = currentAnime.BannerID
		updateParams.BannerID = &image.ID
	}

	// Compare next airing dates
	if currentAnime.NextAiringDate != nil && animeMedia.NextAiringDate != nil &&
		!currentAnime.NextAiringDate.Equal(*animeMedia.NextAiringDate) {
		updateParams.NextAiringDate = animeMedia.NextAiringDate
	}

	// Compare descriptions
	if !strings.EqualFold(animeMedia.Description, currentAnime.Description) {
		updateParams.Description = &animeMedia.Description
	}

	// Compare the colours.
	if !utils.IsEmpty(animeMedia.Colour) && currentAnime.Colour != animeMedia.Colour {
		updateParams.Colour = animeMedia.Colour
	}

	if colourNeedsUpdate {
		// We need to be smart here if the posterBytes aren't 0, there has been a poster update
		// and we can leverage those bytes instead of re-retrieving.
		if len(posterBytes) == 0 {
			newPosterBytes, err := s.associations.Image().Processor().ReadImage(ctx, animeMedia.Poster.Uri)
			if err != nil {
				return err
			}
			posterBytes = newPosterBytes
		}

		s.L(ctx).Debug(
			"update, processing dominant colour",
			zap.String("image_url", animeMedia.Poster.Name),
		)

		colourHex := s.associations.Image().Processor().DominantColour(ctx, bytes.NewReader(posterBytes))

		s.L(ctx).Debug(
			"update, processed dominant colour",
			zap.String("image_url", animeMedia.Poster.Uri),
			zap.Stringp("hex", colourHex),
		)

		if colourHex != nil {
			currentAnime.Colour = colourHex
		}
	}

	// Compare the score
	if currentAnime.Score != animeMedia.Score {
		updateParams.Score = &animeMedia.Score
	}

	// Compare the start date
	if (currentAnime.StartDate == nil && animeMedia.StartDate != nil) ||
		(currentAnime.StartDate != nil && animeMedia.StartDate == nil) ||
		(currentAnime.StartDate != nil && animeMedia.StartDate != nil && !currentAnime.StartDate.Equal(*animeMedia.StartDate)) {
		updateParams.StartDate = animeMedia.StartDate
	}

	// Compare the end date
	if (currentAnime.EndDate == nil && animeMedia.EndDate != nil) ||
		(currentAnime.EndDate != nil && animeMedia.EndDate == nil) ||
		(currentAnime.EndDate != nil && animeMedia.EndDate != nil && !currentAnime.EndDate.Equal(*animeMedia.EndDate)) {
		updateParams.EndDate = animeMedia.EndDate
	}

	if updateParams.IsEmpty() {
		s.L(ctx).Debug("no updates for anime media", zap.Int64("id", animeMedia.ID))
		return nil
	}

	s.L(ctx).Debug("update", zap.Int64("anime_id", animeMedia.ID), zap.Any("params", updateParams))

	rowsAffected, err := s.Update(ctx, updateParams)
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		s.L(ctx).Warn(
			"update `anime` unexpected rows affected",
			zap.Int64("rows_affected", rowsAffected),
		)
	}

	// After the update, to not violate the foreign key delete the posters and banners.
	{
		if previousPosterID != nil && *previousPosterID > 0 {
			// Remove the previous poster since we've replace it for this currentAnime.
			rowsAffected, err := s.associations.Image().Poster().Delete(ctx, params.DeletePosterParams{
				ID: *previousPosterID,
			})
			if err != nil {
				return err
			}
			if rowsAffected == 0 {
				s.L(ctx).Warn(
					"delete `poster` unexpected rows affected",
					zap.Int64("rows_affected", rowsAffected),
				)
			}
		}

		if previousBannerID != nil && *previousBannerID > 0 {
			// Remove the previous banner since we've replace it for this currentAnime.
			rowsAffected, err := s.associations.Image().Banner().Delete(ctx, params.DeleteBannerParams{
				ID: *previousBannerID,
			})
			if err != nil {
				return err
			}
			if rowsAffected == 0 {
				s.L(ctx).Warn(
					"delete `banner` unexpected rows affected",
					zap.Int64("rows_affected", rowsAffected),
				)
			}
		}
	}

	// Since we updated, bust this cache key to have the new entry.
	err = s.cacher.Delete(ctx, cachekeys.Anime(&currentAnime.ID))
	if err != nil {
		return err
	}

	return nil
}
