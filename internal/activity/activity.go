package activity

import (
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

type ActivityService interface {
	Progress() (
		merger queue.Items,
		data protocol.DataActivityProgress,
		err error,
	)

	Difference(prev, next queue.Items) (differences protocol.DataActivityDifferences)
}

type activityService struct {
	queue  *queue.Queue
	mapper queue.Mapper[protocol.DataActivityProgress]
}

func NewActivityService(queue *queue.Queue) ActivityService {
	return &activityService{
		queue:  queue,
		mapper: newMapper(),
	}
}

func (a *activityService) Difference(prev, next queue.Items) (
	differences protocol.DataActivityDifferences,
) {
	return Differentiate(prev, next)
}

func (a *activityService) Progress() (merger queue.Items, data protocol.DataActivityProgress, err error) {
	active, inactive := a.queue.ActiveItems(), a.queue.InactiveItems()
	data, err = a.mapper.Map(active, inactive)
	if err != nil {
		return
	}
	merger = merge(active, inactive)
	return merger, data, err
}
