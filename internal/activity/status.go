package activity

import (
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

type mapper struct{}

func (m *mapper) add(
	data protocol.DataActivityProgress,
	anime domain.AnimeBase,
	progress *protocol.DataActivityProgressItem,
) {
	if _, exists := data[anime.Title]; !exists {
		data[anime.Title] = struct {
			ID            int64                                                            `json:"d"`
			ReleaseGroups map[string]map[string]map[int]*protocol.DataActivityProgressItem `json:"rg"`
		}{
			ID:            anime.ID,
			ReleaseGroups: make(map[string]map[string]map[int]*protocol.DataActivityProgressItem),
		}
	}

	if _, exists := data[anime.Title].ReleaseGroups[anime.ReleaseGroup]; !exists {
		data[anime.Title].ReleaseGroups[anime.ReleaseGroup] = make(
			map[string]map[int]*protocol.DataActivityProgressItem,
		)
	}

	if _, exists := data[anime.Title].ReleaseGroups[anime.ReleaseGroup][anime.Episode]; !exists {
		data[anime.Title].ReleaseGroups[anime.ReleaseGroup][anime.Episode] = make(
			map[int]*protocol.DataActivityProgressItem,
		)
	}

	data[anime.Title].ReleaseGroups[anime.ReleaseGroup][anime.Episode][anime.Height] = progress
}

func newMapper() queue.Mapper[protocol.DataActivityProgress] {
	return &mapper{}
}

func (m *mapper) Map(
	active queue.Items,
	inactive queue.Items,
) (out protocol.DataActivityProgress, err error) {
	progress := make(protocol.DataActivityProgress)

	for anime, value := range active {
		m.add(
			progress,
			anime,
			fromQueueProgress(value.Progress),
		)
	}

	for anime := range inactive {
		m.add(progress, anime, nil)
	}

	return progress, nil
}
