package activity_test

import (
	"testing"

	"gitlab.com/eemj/anime-pack/internal/activity"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

type TestDifferentiateScenario struct {
	Name                string
	Previous            queue.Items
	Next                queue.Items
	ExpectedDifferences protocol.DataActivityDifferences
}

func generateAnimeBase(episode string) domain.AnimeBase {
	return domain.AnimeBase{
		ID:           21,
		Title:        "One Piece",
		Height:       100,
		ReleaseGroup: "HorribleSubs",
		Episode:      episode,
	}
}

func generateProtocolAnime(episode string) protocol.Anime {
	return protocol.Anime{
		ID:            21,
		Title:         "One Piece",
		QualityHeight: 100,
		ReleaseGroup:  "HorribleSubs",
		Episode:       episode,
	}
}

func compareDifferences(t *testing.T, scenarioName string, actual, expected protocol.DataActivityDifferences) {
	actualLength := len(actual)
	expectedLength := len(expected)

	if actualLength != expectedLength {
		t.Fatalf(
			"(%s) actual vs expected length differs %d != %d",
			scenarioName,
			actualLength,
			expectedLength,
		)
	}

	for _, currentActual := range actual {
		var foundExpected *protocol.DataActivityDifference

		for _, currentExpected := range expected {
			if currentExpected.Anime.Equal(currentActual.Anime) {
				foundExpected = currentExpected
				break
			}
		}

		if foundExpected == nil {
			t.Fatalf("(%s) expected not found for <%+v>", scenarioName, currentActual)
		}

		if foundExpected.Token != currentActual.Token {
			t.Fatalf(
				"(%s) - %s actual vs expected token differs %s != %s",
				scenarioName,
				foundExpected.Anime.String(),
				string(currentActual.Token),
				string(foundExpected.Token),
			)
		}
	}
}

func TestDifferentiate(t *testing.T) {
	scenarios := []TestDifferentiateScenario{
		{
			Name: "stale queue, but a sudden additions",
			Previous: queue.Items{
				generateAnimeBase("1"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Download), Percentage: 99, Speed: 1}},
			},
			Next: queue.Items{
				generateAnimeBase("1"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Upload), Percentage: 1, Speed: 2}},
				generateAnimeBase("2"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("3"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("4"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("5"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("6"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("7"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("8"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("9"): &queue.State{Progress: queue.Progress{}},
			},
			ExpectedDifferences: protocol.DataActivityDifferences{
				{Token: protocol.DifferenceToken_Modify, Anime: generateProtocolAnime("1")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("2")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("3")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("4")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("5")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("6")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("7")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("8")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("9")},
			},
		},

		{
			Name: "activity queue, but a sudden remove wave",
			Previous: queue.Items{
				generateAnimeBase("1"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Upload), Percentage: 1, Speed: 2}},
				generateAnimeBase("7"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("8"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("9"): &queue.State{Progress: queue.Progress{}},
			},
			Next: queue.Items{
				generateAnimeBase("7"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Download), Percentage: 1, Speed: 1}},
				generateAnimeBase("8"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Download), Percentage: 1, Speed: 1}},
				generateAnimeBase("9"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Download), Percentage: 1, Speed: 1}},
			},
			ExpectedDifferences: protocol.DataActivityDifferences{
				{Token: protocol.DifferenceToken_Remove, Anime: generateProtocolAnime("1")},
				{Token: protocol.DifferenceToken_Modify, Anime: generateProtocolAnime("7")},
				{Token: protocol.DifferenceToken_Modify, Anime: generateProtocolAnime("8")},
				{Token: protocol.DifferenceToken_Modify, Anime: generateProtocolAnime("9")},
			},
		},

		{
			Name: "activity queue, with a bit of all differences",
			Previous: queue.Items{
				generateAnimeBase("1"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Upload), Percentage: 1, Speed: 2}},
				generateAnimeBase("7"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("9"): &queue.State{Progress: queue.Progress{}},
			},
			Next: queue.Items{
				generateAnimeBase("1"): &queue.State{Progress: queue.Progress{Type: string(protocol.Progress_Upload), Percentage: 1.1, Speed: 3}},
				generateAnimeBase("7"): &queue.State{Progress: queue.Progress{}},
				generateAnimeBase("8"): &queue.State{Progress: queue.Progress{}},
			},
			ExpectedDifferences: protocol.DataActivityDifferences{
				{Token: protocol.DifferenceToken_Modify, Anime: generateProtocolAnime("1")},
				{Token: protocol.DifferenceToken_Remove, Anime: generateProtocolAnime("9")},
				{Token: protocol.DifferenceToken_Add, Anime: generateProtocolAnime("8")},
			},
		},
	}

	for _, scenario := range scenarios {
		differences := activity.Differentiate(scenario.Previous, scenario.Next)

		compareDifferences(t, scenario.Name, differences, scenario.ExpectedDifferences)
	}
}
