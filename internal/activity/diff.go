package activity

import (
	"gitlab.com/eemj/anime-pack/internal/queue"
	"gitlab.com/eemj/anime-pack/pkg/websocket/protocol"
)

type DifferenceState struct {
	MergedItems queue.Items
	Progress    *protocol.DataActivityProgress
	Differences protocol.DataActivityDifferences
}

func merge[K comparable, V any](prev, next map[K]V) (merged map[K]V) {
	merged = make(map[K]V, len(prev)+len(next))
	for key, value := range prev {
		merged[key] = value
	}
	for key, value := range next {
		merged[key] = value
	}
	return merged
}

func fromQueueProgress(progress queue.Progress) *protocol.DataActivityProgressItem {
	// Instead of having this logic on the frontend, we'll cater for it here,
	// if a type is empty and percentage is 0, we'll return nil
	//
	// When this scenario happens, the item is in the active queue but we're waiting for the IRC
	// bot to respond to our DCC query.
	if progress.Type == "" && progress.Percentage == 0 {
		return nil
	}

	return &protocol.DataActivityProgressItem{
		Type:            protocol.ProgressType(progress.Type),
		Percentage:      progress.Percentage,
		Speed:           progress.Speed,
		CurrentFilesize: progress.CurrentFilesize,
		Filesize:        progress.Filesize,
		CurrentDuration: progress.CurrentDuration,
		Duration:        progress.Duration,
	}
}

func Differentiate(prev, next queue.Items) protocol.DataActivityDifferences {
	differences := make(protocol.DataActivityDifferences, 0)
	merged := merge(prev, next)

	for key := range merged {
		var (
			prevState, prevExists = prev[key]
			nextState, nextExists = next[key]
		)

		var token protocol.DifferenceToken
		switch {
		case prevExists && !nextExists:
			token = protocol.DifferenceToken_Remove
		case !prevExists && nextExists:
			token = protocol.DifferenceToken_Add
		default:
			token = protocol.DifferenceToken_Modify
		}

		difference := &protocol.DataActivityDifference{
			Token: token,
			Anime: protocol.Anime{
				ID:            key.ID,
				Title:         key.Title,
				Episode:       key.Episode,
				ReleaseGroup:  key.ReleaseGroup,
				QualityHeight: key.Height,
			},
		}

		var (
			nextProgress queue.Progress
			prevProgress queue.Progress
		)

		if nextState != nil {
			difference.Progress = fromQueueProgress(nextState.Progress)
			nextProgress = nextState.Progress
		}

		if prevState != nil {
			prevProgress = prevState.Progress
		}

		// Same percentage as the previous no point in sending the difference.
		if token == protocol.DifferenceToken_Modify && prevProgress.Equal(nextProgress) {
			continue
		}

		differences = append(differences, difference)
	}

	return differences
}
