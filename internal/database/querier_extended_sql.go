package database

const (
	deleteAllSimulcast = `delete from simulcast`

	deleteUnusedTitleAnime = `delete from title_anime where title_id in (
		select title.id
		from title
		left join title_episode on title.id = title_episode.title_id
		group by title.id
		having count(title_episode.id) = 0
	);`

	deleteUnusedTitleEpisode = `delete from title_episode where id IN (
		select title_episode.id
		from title_episode
		left join xdcc ON title_episode.id = title_episode_id
		where xdcc is null
	);`

	deleteUnusedTitle = `delete from title where id in (
		select title.id
		from title
		left join title_episode on title.id = title_episode.title_id
		group by title.id
		having count(title_episode.id) = 0
	);`
)
