package database

import (
	"context"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type QuerierExtended interface {
	Querier

	Alive(ctx context.Context) bool
	ReplaceAllSimulcast(ctx context.Context, arg []Simulcast) (err error)
	DeleteUnusedTitles(ctx context.Context) (rowsAffected int64, err error)
}

type QueriesExtended struct {
	*Queries

	pool *pgxpool.Pool
}

// Alive implements QuerierExtended.
func (s *QueriesExtended) Alive(ctx context.Context) bool {
	return s.pool.Ping(ctx) == nil
}

// DeleteUnusedTitles implements QuerierExtended.
func (e *QueriesExtended) DeleteUnusedTitles(ctx context.Context) (rowsAffected int64, err error) {
	conn, err := e.pool.Acquire(ctx)
	if err != nil {
		return 0, err
	}
	defer conn.Release()

	result, err := conn.Exec(ctx, deleteUnusedTitleEpisode)
	if err != nil {
		return 0, err
	}
	rowsAffected += result.RowsAffected()

	result, err = conn.Exec(ctx, deleteUnusedTitleAnime)
	if err != nil {
		return 0, err
	}
	rowsAffected += result.RowsAffected()

	result, err = conn.Exec(ctx, deleteUnusedTitle)
	if err != nil {
		return 0, err
	}
	rowsAffected += result.RowsAffected()

	return rowsAffected, nil
}

// ReplaceAllSimulcast implements QuerierExtended.
func (e *QueriesExtended) ReplaceAllSimulcast(ctx context.Context, items []Simulcast) (err error) {
	txn, err := e.pool.Acquire(ctx)
	if err != nil {
		return err
	}
	defer txn.Release()

	tx, err := txn.BeginTx(ctx, pgx.TxOptions{})
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, deleteAllSimulcast)
	if err != nil {
		return err
	}

	_, err = tx.CopyFrom(
		ctx,
		pgx.Identifier{"simulcast"},
		[]string{"title_id", "weekday", "hour", "minute", "second"},
		pgx.CopyFromSlice(len(items), func(i int) ([]any, error) {
			return []any{
				items[i].TitleID,
				items[i].Weekday,
				items[i].Hour,
				items[i].Minute,
				items[i].Second,
			}, nil
		}),
	)
	if err != nil {
		return err
	}
	return tx.Commit(ctx)
}

var _ QuerierExtended = &QueriesExtended{}

func NewQuerierExtended(pgxpool *pgxpool.Pool) QuerierExtended {
	return &QueriesExtended{
		pool:    pgxpool,
		Queries: New(pgxpool),
	}
}
