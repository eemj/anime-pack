// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: xdcc.sql

package database

import (
	"context"
	"time"
)

const countXDCC = `-- name: CountXDCC :one
select count(1)
from xdcc
where
    (id = coalesce($1, id)) and
    (size = coalesce($2, size)) and
    (filename = coalesce($3, filename)) and
    (escaped_filename = coalesce($4, escaped_filename)) and
    (bot_id = coalesce($5, bot_id)) and
    (release_group_id = coalesce($6, release_group_id)) and
    (title_episode_id = coalesce($7, title_episode_id)) and
    (is_deleted = coalesce($8, is_deleted))
`

type CountXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
}

func (q *Queries) CountXDCC(ctx context.Context, arg CountXDCCParams) (int64, error) {
	row := q.db.QueryRow(ctx, countXDCC,
		arg.ID,
		arg.Size,
		arg.Filename,
		arg.EscapedFilename,
		arg.BotID,
		arg.ReleaseGroupID,
		arg.TitleEpisodeID,
		arg.IsDeleted,
	)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const deleteXDCC = `-- name: DeleteXDCC :execrows
delete from xdcc
where
    (id = any($1::bigint[]) or array_length($1::bigint[], 1) is null) and
    (title_episode_id = any($2::bigint[]) or array_length($2::bigint[], 1) is null)
`

type DeleteXDCCParams struct {
	IDs             []int64
	TitleEpisodeIDs []int64
}

func (q *Queries) DeleteXDCC(ctx context.Context, arg DeleteXDCCParams) (int64, error) {
	result, err := q.db.Exec(ctx, deleteXDCC, arg.IDs, arg.TitleEpisodeIDs)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected(), nil
}

const filterXDCC = `-- name: FilterXDCC :many
select
    xdcc.id,
    xdcc.pack,
    xdcc.size,
    xdcc.filename,
    xdcc.escaped_filename,
    xdcc.bot_id,
    xdcc.quality_id,
    xdcc.release_group_id,
    xdcc.title_episode_id,
    xdcc.created_at,
    xdcc.updated_at,
    xdcc.deleted_at,
    xdcc.is_deleted,
    video.id as video_id,
    bot.name as bot_name,
    release_group.name as release_group_name,
    quality.height as quality_height,
    title_episode.title_id as title_episode_title_id,
    title_episode.episode_id as title_episode_episode_id,
    episode.name as episode_name,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year
from xdcc
join bot on xdcc.bot_id = bot.id
join release_group on xdcc.release_group_id = release_group.id
join title_episode on xdcc.title_episode_id = title_episode.id
left join quality on xdcc.quality_id = quality.id
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
left join video on video.xdcc_id = xdcc.id
where
    (xdcc.id = coalesce($1, xdcc.id)) and
    (xdcc.size = coalesce($2, xdcc.size)) and
    (xdcc.filename = coalesce($3, xdcc.filename)) and
    (escaped_filename = coalesce($4, escaped_filename)) and
    (xdcc.bot_id = coalesce($5, xdcc.bot_id)) and
    (xdcc.release_group_id = coalesce($6, xdcc.release_group_id)) and
    (xdcc.title_episode_id = coalesce($7, xdcc.title_episode_id)) and
    (xdcc.is_deleted = coalesce($8, xdcc.is_deleted)) and
    (title_episode.title_id = coalesce($9, title_episode.title_id)) and
    (title.reviewed = coalesce($10::bool, title.reviewed)) and
    (title.is_deleted = coalesce($11::bool, title.is_deleted)) and
    (case when $12::bool then video is null else true end) and
    (case
        when $13::bool then case
            when $14::bigint is null then xdcc.quality_id is null
            else xdcc.quality_id = $14::bigint
        end
        else true
    end)
`

type FilterXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
	TitleID         *int64
	TitleReviewed   *bool
	TitleIsDeleted  *bool
	WithoutVideo    bool
	WithQualityID   bool
	QualityID       *int64
}

type FilterXDCCRow struct {
	ID                    int64
	Pack                  int64
	Size                  int64
	Filename              string
	EscapedFilename       string
	BotID                 int64
	QualityID             *int64
	ReleaseGroupID        int64
	TitleEpisodeID        int64
	CreatedAt             time.Time
	UpdatedAt             time.Time
	DeletedAt             *time.Time
	IsDeleted             bool
	VideoID               *int64
	BotName               string
	ReleaseGroupName      string
	QualityHeight         *int64
	TitleEpisodeTitleID   int64
	TitleEpisodeEpisodeID int64
	EpisodeName           string
	TitleName             string
	TitleSeasonNumber     *string
	TitleYear             *int
}

func (q *Queries) FilterXDCC(ctx context.Context, arg FilterXDCCParams) ([]*FilterXDCCRow, error) {
	rows, err := q.db.Query(ctx, filterXDCC,
		arg.ID,
		arg.Size,
		arg.Filename,
		arg.EscapedFilename,
		arg.BotID,
		arg.ReleaseGroupID,
		arg.TitleEpisodeID,
		arg.IsDeleted,
		arg.TitleID,
		arg.TitleReviewed,
		arg.TitleIsDeleted,
		arg.WithoutVideo,
		arg.WithQualityID,
		arg.QualityID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*FilterXDCCRow{}
	for rows.Next() {
		var i FilterXDCCRow
		if err := rows.Scan(
			&i.ID,
			&i.Pack,
			&i.Size,
			&i.Filename,
			&i.EscapedFilename,
			&i.BotID,
			&i.QualityID,
			&i.ReleaseGroupID,
			&i.TitleEpisodeID,
			&i.CreatedAt,
			&i.UpdatedAt,
			&i.DeletedAt,
			&i.IsDeleted,
			&i.VideoID,
			&i.BotName,
			&i.ReleaseGroupName,
			&i.QualityHeight,
			&i.TitleEpisodeTitleID,
			&i.TitleEpisodeEpisodeID,
			&i.EpisodeName,
			&i.TitleName,
			&i.TitleSeasonNumber,
			&i.TitleYear,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const firstXDCC = `-- name: FirstXDCC :one
select
    xdcc.id,
    xdcc.pack,
    xdcc.size,
    xdcc.filename,
    xdcc.escaped_filename,
    xdcc.bot_id,
    xdcc.quality_id,
    xdcc.release_group_id,
    xdcc.title_episode_id,
    xdcc.created_at,
    xdcc.updated_at,
    xdcc.deleted_at,
    xdcc.is_deleted,
    video.id as video_id,
    bot.name as bot_name,
    release_group.name as release_group_name,
    quality.height as quality_height,
    title_episode.title_id as title_episode_title_id,
    title_episode.episode_id as title_episode_episode_id,
    episode.name as episode_name,
    title.name as title_name,
    title.season_number as title_season_number,
    title.year as title_year
from xdcc
join bot on xdcc.bot_id = bot.id
join release_group on xdcc.release_group_id = release_group.id
join title_episode on xdcc.title_episode_id = title_episode.id
left join quality on xdcc.quality_id = quality.id
join title on title_episode.title_id = title.id
join episode on title_episode.episode_id = episode.id
left join video on video.xdcc_id = xdcc.id
where
    (xdcc.id = coalesce($1, xdcc.id)) and
    (xdcc.size = coalesce($2, xdcc.size)) and
    (xdcc.filename = coalesce($3, xdcc.filename)) and
    (escaped_filename = coalesce($4, escaped_filename)) and
    (xdcc.bot_id = coalesce($5, xdcc.bot_id)) and
    (xdcc.release_group_id = coalesce($6, xdcc.release_group_id)) and
    (xdcc.title_episode_id = coalesce($7, xdcc.title_episode_id)) and
    (xdcc.is_deleted = coalesce($8, xdcc.is_deleted)) and
    (title_episode.title_id = coalesce($9, title_episode.title_id)) and
    (case when $10::bool then video is null else true end)
limit 1
`

type FirstXDCCParams struct {
	ID              *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	IsDeleted       *bool
	TitleID         *int64
	WithoutVideo    bool
}

type FirstXDCCRow struct {
	ID                    int64
	Pack                  int64
	Size                  int64
	Filename              string
	EscapedFilename       string
	BotID                 int64
	QualityID             *int64
	ReleaseGroupID        int64
	TitleEpisodeID        int64
	CreatedAt             time.Time
	UpdatedAt             time.Time
	DeletedAt             *time.Time
	IsDeleted             bool
	VideoID               *int64
	BotName               string
	ReleaseGroupName      string
	QualityHeight         *int64
	TitleEpisodeTitleID   int64
	TitleEpisodeEpisodeID int64
	EpisodeName           string
	TitleName             string
	TitleSeasonNumber     *string
	TitleYear             *int
}

func (q *Queries) FirstXDCC(ctx context.Context, arg FirstXDCCParams) (*FirstXDCCRow, error) {
	row := q.db.QueryRow(ctx, firstXDCC,
		arg.ID,
		arg.Size,
		arg.Filename,
		arg.EscapedFilename,
		arg.BotID,
		arg.ReleaseGroupID,
		arg.TitleEpisodeID,
		arg.IsDeleted,
		arg.TitleID,
		arg.WithoutVideo,
	)
	var i FirstXDCCRow
	err := row.Scan(
		&i.ID,
		&i.Pack,
		&i.Size,
		&i.Filename,
		&i.EscapedFilename,
		&i.BotID,
		&i.QualityID,
		&i.ReleaseGroupID,
		&i.TitleEpisodeID,
		&i.CreatedAt,
		&i.UpdatedAt,
		&i.DeletedAt,
		&i.IsDeleted,
		&i.VideoID,
		&i.BotName,
		&i.ReleaseGroupName,
		&i.QualityHeight,
		&i.TitleEpisodeTitleID,
		&i.TitleEpisodeEpisodeID,
		&i.EpisodeName,
		&i.TitleName,
		&i.TitleSeasonNumber,
		&i.TitleYear,
	)
	return &i, err
}

const softDeleteXDCC = `-- name: SoftDeleteXDCC :execrows
update xdcc
set deleted_at = timezone('utc'::text, now())
where
    (id = any($1::bigint[]) or array_length($1::bigint[], 1) is null) and
    (title_episode_id = any($2::bigint[]) or array_length($2::bigint[], 1) is null)
`

type SoftDeleteXDCCParams struct {
	IDs             []int64
	TitleEpisodeIDs []int64
}

func (q *Queries) SoftDeleteXDCC(ctx context.Context, arg SoftDeleteXDCCParams) (int64, error) {
	result, err := q.db.Exec(ctx, softDeleteXDCC, arg.IDs, arg.TitleEpisodeIDs)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected(), nil
}

const updateXDCC = `-- name: UpdateXDCC :execrows
update xdcc
set 
    pack = coalesce($1, pack),
    size = coalesce($2, size),
    filename = coalesce($3, filename),
    escaped_filename = coalesce($4, escaped_filename),
    bot_id = coalesce($5, bot_id),
    quality_id = coalesce($6, quality_id),
    release_group_id = coalesce($7, release_group_id),
    title_episode_id = coalesce($8, title_episode_id)
where id = $9
`

type UpdateXDCCParams struct {
	Pack            *int64
	Size            *int64
	Filename        *string
	EscapedFilename *string
	BotID           *int64
	QualityID       *int64
	ReleaseGroupID  *int64
	TitleEpisodeID  *int64
	ID              int64
}

func (q *Queries) UpdateXDCC(ctx context.Context, arg UpdateXDCCParams) (int64, error) {
	result, err := q.db.Exec(ctx, updateXDCC,
		arg.Pack,
		arg.Size,
		arg.Filename,
		arg.EscapedFilename,
		arg.BotID,
		arg.QualityID,
		arg.ReleaseGroupID,
		arg.TitleEpisodeID,
		arg.ID,
	)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected(), nil
}
