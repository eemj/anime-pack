// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: anime_genre.sql

package database

import (
	"context"
)

const countAnimeGenre = `-- name: CountAnimeGenre :one
select count(1)
from anime_genre
where
    (anime_id = coalesce($1, anime_id)) and
    (genre_id = coalesce($2, genre_id))
`

type CountAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

func (q *Queries) CountAnimeGenre(ctx context.Context, arg CountAnimeGenreParams) (int64, error) {
	row := q.db.QueryRow(ctx, countAnimeGenre, arg.AnimeID, arg.GenreID)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const deleteAnimeGenre = `-- name: DeleteAnimeGenre :execrows
delete from anime_genre
where
    (anime_id = $1) and
    (genre_id = coalesce($2, genre_id))
`

type DeleteAnimeGenreParams struct {
	AnimeID int64
	GenreID *int64
}

func (q *Queries) DeleteAnimeGenre(ctx context.Context, arg DeleteAnimeGenreParams) (int64, error) {
	result, err := q.db.Exec(ctx, deleteAnimeGenre, arg.AnimeID, arg.GenreID)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected(), nil
}

const filterAnimeGenre = `-- name: FilterAnimeGenre :many
select
    anime_genre.anime_id,
    anime_genre.genre_id,
    genre.name as genre_name
from anime_genre
join genre on anime_genre.anime_id = genre.id
where
    (anime_genre.anime_id = coalesce($1, anime_genre.anime_id)) and
    (anime_genre.genre_id = coalesce($2, anime_genre.genre_id))
`

type FilterAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

type FilterAnimeGenreRow struct {
	AnimeID   int64
	GenreID   int64
	GenreName string
}

func (q *Queries) FilterAnimeGenre(ctx context.Context, arg FilterAnimeGenreParams) ([]*FilterAnimeGenreRow, error) {
	rows, err := q.db.Query(ctx, filterAnimeGenre, arg.AnimeID, arg.GenreID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*FilterAnimeGenreRow{}
	for rows.Next() {
		var i FilterAnimeGenreRow
		if err := rows.Scan(&i.AnimeID, &i.GenreID, &i.GenreName); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const firstAnimeGenre = `-- name: FirstAnimeGenre :one
select
    anime_genre.anime_id,
    anime_genre.genre_id,
    genre.name as genre_name
from anime_genre
join genre on anime_genre.anime_id = genre.id
where
    (anime_genre.anime_id = coalesce($1, anime_genre.anime_id)) and
    (anime_genre.genre_id = coalesce($2, anime_genre.genre_id))
limit 1
`

type FirstAnimeGenreParams struct {
	AnimeID *int64
	GenreID *int64
}

type FirstAnimeGenreRow struct {
	AnimeID   int64
	GenreID   int64
	GenreName string
}

func (q *Queries) FirstAnimeGenre(ctx context.Context, arg FirstAnimeGenreParams) (*FirstAnimeGenreRow, error) {
	row := q.db.QueryRow(ctx, firstAnimeGenre, arg.AnimeID, arg.GenreID)
	var i FirstAnimeGenreRow
	err := row.Scan(&i.AnimeID, &i.GenreID, &i.GenreName)
	return &i, err
}
