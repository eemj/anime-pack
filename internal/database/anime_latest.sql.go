// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.26.0
// source: anime_latest.sql

package database

import (
	"context"
	"time"

	"gitlab.com/eemj/anime-pack/internal/domain/enums"
)

const countAnimeLatest = `-- name: CountAnimeLatest :one
select count(1)
from vw_xdcc_title_episode as title_episode
         join title on title_episode.title_id = title.id and not title.is_deleted and title.reviewed
         join title_anime on title_anime.title_id = title.id
         join anime on title_anime.anime_id = anime.id
         left join preferences on anime.id = preferences.anime_id
where
  -- Filters
    (anime.status = any ($1::anime_status[]) or array_length($1::anime_status[], 1) is null)
  and (anime.format = any ($2::anime_format[]) or array_length($2::anime_format[], 1) is null)
  and (preferences.favourite = case when $3::bool is null then preferences.favourite else $3::bool end)
`

type CountAnimeLatestParams struct {
	Statuses  []enums.AnimeStatus
	Formats   []enums.AnimeFormat
	Favourite *bool
}

func (q *Queries) CountAnimeLatest(ctx context.Context, arg CountAnimeLatestParams) (int64, error) {
	row := q.db.QueryRow(ctx, countAnimeLatest, arg.Statuses, arg.Formats, arg.Favourite)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const filterAnimeLatest = `-- name: FilterAnimeLatest :many
select anime.id                 as anime_id,
       anime.id_mal             as anime_id_mal,
       anime.title              as anime_title,
       anime.title_english      as anime_title_english,
       anime.title_native       as anime_title_native,
       anime.title_romaji       as anime_title_romaji,
       anime.colour             as anime_colour,
       anime.format             as anime_format,
       anime.status             as anime_status,
       poster.id                as poster_id,
       poster.name              as poster_name,
       poster.hash              as poster_hash,
       episode.name             as episode_name,
       title_episode.id         as title_episode_id,
       title_episode.created_at as title_episode_created_at,
       preferences.favourite    as preferences_favourite
from vw_xdcc_title_episode as title_episode
         join title on title_episode.title_id = title.id and not title.is_deleted and title.reviewed
         join episode on title_episode.episode_id = episode.id
         join title_anime on title.id = title_anime.title_id
         join anime on title_anime.anime_id = anime.id
         join poster on anime.poster_id = poster.id
         left join preferences on anime.id = preferences.anime_id
where
  -- Filters
    (anime.status = any ($1::anime_status[]) or array_length($1::anime_status[], 1) is null)
  and (anime.format = any ($2::anime_format[]) or array_length($2::anime_format[], 1) is null)
  and (preferences.favourite = case when $3::bool is null then preferences.favourite else $3::bool end)
order by case when ($4::text = 'id' and $5::bool = true) then anime.id end desc,
         case when ($4::text = 'id' and $5::bool = false) then anime.id end asc,
         case when ($4::text = 'createdAt' and $5::bool = true) then title_episode.created_at end desc,
         case when ($4::text = 'createdAt' and $5::bool = false) then title_episode.created_at end asc,
         case when ($4::text = 'score' and $5::bool = true) then anime.score end desc,
         case when ($4::text = 'score' and $5::bool = false) then anime.score end asc
limit $7::bigint
offset $6::bigint
`

type FilterAnimeLatestParams struct {
	Statuses    []enums.AnimeStatus
	Formats     []enums.AnimeFormat
	Favourite   *bool
	OrderBy     string
	OrderByDesc bool
	Offset      int64
	Limit       int64
}

type FilterAnimeLatestRow struct {
	AnimeID               int64
	AnimeIDMal            *int64
	AnimeTitle            string
	AnimeTitleEnglish     *string
	AnimeTitleNative      *string
	AnimeTitleRomaji      *string
	AnimeColour           *string
	AnimeFormat           enums.AnimeFormat
	AnimeStatus           enums.AnimeStatus
	PosterID              int64
	PosterName            string
	PosterHash            string
	EpisodeName           string
	TitleEpisodeID        int64
	TitleEpisodeCreatedAt time.Time
	PreferencesFavourite  *bool
}

func (q *Queries) FilterAnimeLatest(ctx context.Context, arg FilterAnimeLatestParams) ([]*FilterAnimeLatestRow, error) {
	rows, err := q.db.Query(ctx, filterAnimeLatest,
		arg.Statuses,
		arg.Formats,
		arg.Favourite,
		arg.OrderBy,
		arg.OrderByDesc,
		arg.Offset,
		arg.Limit,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*FilterAnimeLatestRow{}
	for rows.Next() {
		var i FilterAnimeLatestRow
		if err := rows.Scan(
			&i.AnimeID,
			&i.AnimeIDMal,
			&i.AnimeTitle,
			&i.AnimeTitleEnglish,
			&i.AnimeTitleNative,
			&i.AnimeTitleRomaji,
			&i.AnimeColour,
			&i.AnimeFormat,
			&i.AnimeStatus,
			&i.PosterID,
			&i.PosterName,
			&i.PosterHash,
			&i.EpisodeName,
			&i.TitleEpisodeID,
			&i.TitleEpisodeCreatedAt,
			&i.PreferencesFavourite,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
