package pipeline

import (
	"context"
	"crypto/rand"
	"fmt"
	"io"
	"testing"
	"time"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

func TestPipelineUpload_ProgressPercentage(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	p := &pipeline{
		cancelFunc: cancel,
		stream:     make(chan *grpc_runner_v1.StreamRequest, 32),
		operation:  make(chan *grpc_runner_v1.OperationRequest, 32),
	}

	errc := make(chan error)
	imageSize := int64(split * 16)
	videoSize := int64(split * 48)

	go func() {
		imageStream := io.LimitReader(rand.Reader, imageSize)
		videoStream := io.LimitReader(rand.Reader, videoSize)
		tick := (100 * time.Millisecond)

		if err := p.process(ctx, imageStream, float64(imageSize), grpc_runner_v1.StreamType_IMAGE, tick); err != nil {
			errc <- fmt.Errorf("failed to process image stream due to '%v'", err)
			return
		}

		if err := p.process(ctx, videoStream, float64(videoSize), grpc_runner_v1.StreamType_VIDEO, tick); err != nil {
			errc <- fmt.Errorf("failed to process image stream due to '%v'", err)
			return
		}

		close(p.operation)
	}()

	for {
		select {
		case err := <-errc:
			t.Fatal(err)
		case <-p.stream: // do nothing
		case operationRequest, ok := <-p.operation:
			if !ok {
				return
			}

			if operationRequest.Operation == grpc_runner_v1.Operation_PROGRESS {
				streamType := (grpc_runner_v1.StreamType)(0)

				if imageSize == int64(operationRequest.GetProgressRequest().GetFilesize()) {
					streamType = grpc_runner_v1.StreamType_IMAGE
				} else {
					streamType = grpc_runner_v1.StreamType_VIDEO
				}

				// An image is supposed to be less or equal to the divider
				if streamType == grpc_runner_v1.StreamType_IMAGE {
					if percentage := operationRequest.GetProgressRequest().GetPercentage(); percentage > divider {
						t.Fatalf(
							"image unexpected percentage. expected <='%d', got '%0.8f'",
							divider,
							percentage,
						)
					}
				} else if streamType == grpc_runner_v1.StreamType_VIDEO {
					if percentage := operationRequest.GetProgressRequest().GetPercentage(); percentage <= divider && percentage > 100 {
						t.Fatalf(
							"video unexpected percentage. expected >'%d' & <'%d', got '%0.8f'",
							divider,
							divider,
							percentage,
						)
					}
				}
			}
		}
	}
}
