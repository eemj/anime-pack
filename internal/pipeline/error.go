package pipeline

const (
	// ErrPipelineIsAlreadyActive is thrown when the Run() method is called during an active pipeline.
	ErrPipelineIsAlreadyActive = Error("pipeline is currently active, unable to run")

	// ErrCannotSetupWhilstPipelineIsActive is thrown when the Setup() method is called whilst the pipeline is active.
	ErrCannotSetupWhilstPipelineIsActive = Error("cannot setup whilst pipeline is active")
)

// Error is a custom error interface for our pipeline.
// It was implemented to have constant errors.
type Error string

func (e Error) Error() string {
	return string(e)
}
