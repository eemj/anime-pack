package pipeline

import (
	"sync"

	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

type Options struct {
	rwmutex sync.RWMutex

	address          string
	size             int
	videoSource      string
	videoDestination string
	imageDestination string
	transcodeOptions ffmpeg.TranscodeOptions
}

func NewOptions(
	size int,
	address,
	videoSource,
	videoDestination,
	imageDestination string,
	transcodeOptions ffmpeg.TranscodeOptions,
) *Options {
	return &Options{
		size:             size,
		address:          address,
		videoSource:      videoSource,
		videoDestination: videoDestination,
		imageDestination: imageDestination,
		transcodeOptions: transcodeOptions,
	}
}

func (o *Options) Clone() *Options {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()

	transcodeOptions := ffmpeg.TranscodeOptions{
		InputOptions:  make([]string, len(o.transcodeOptions.InputOptions)),
		OutputOptions: make([]string, len(o.transcodeOptions.OutputOptions)),
	}

	copy(transcodeOptions.InputOptions, o.transcodeOptions.InputOptions)
	copy(transcodeOptions.OutputOptions, o.transcodeOptions.OutputOptions)

	return &Options{
		address:          o.address,
		size:             o.size,
		videoSource:      o.videoSource,
		videoDestination: o.videoDestination,
		imageDestination: o.imageDestination,
		transcodeOptions: transcodeOptions,
	}
}

func (o *Options) IsEmpty() bool {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()

	return utils.IsEmpty(o.size) &&
		utils.IsEmpty(o.address) &&
		utils.IsEmpty(o.videoSource) &&
		utils.IsEmpty(o.videoDestination) &&
		utils.IsEmpty(o.imageDestination)
}

func (o *Options) Replace(options *Options) {
	o.rwmutex.Lock()
	defer o.rwmutex.Unlock()

	o.size = options.size
	o.address = options.address
	o.videoSource = options.videoSource
	o.videoDestination = options.videoDestination
	o.imageDestination = options.imageDestination
	o.transcodeOptions = options.transcodeOptions
}

func (o *Options) GetSize() int {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.size
}

func (o *Options) GetAddress() string {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.address
}

func (o *Options) GetVideoSource() string {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.videoSource
}

func (o *Options) GetVideoDestination() string {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.videoDestination
}

func (o *Options) GetImageDestination() string {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.imageDestination
}

func (o *Options) GetTranscodeOptions() ffmpeg.TranscodeOptions {
	o.rwmutex.RLock()
	defer o.rwmutex.RUnlock()
	return o.transcodeOptions
}
