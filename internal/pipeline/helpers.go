package pipeline

import (
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

func (p *pipeline) progress(progresses ...*grpc_runner_v1.ProgressRequest) {
	if len(progresses) == 0 {
		return
	}
	for _, progress := range progresses {
		if progress == nil {
			continue
		}
		p.operation <- &grpc_runner_v1.OperationRequest{
			Operation: grpc_runner_v1.Operation_PROGRESS,
			Request: &grpc_runner_v1.OperationRequest_ProgressRequest{
				ProgressRequest: progress,
			},
		}
	}
}

func (p *pipeline) metadata(metadata *grpc_runner_v1.MetadataRequest) {
	if metadata != nil {
		p.operation <- &grpc_runner_v1.OperationRequest{
			Operation: grpc_runner_v1.Operation_METADATA,
			Request: &grpc_runner_v1.OperationRequest_MetadataRequest{
				MetadataRequest: metadata,
			},
		}
	}
}
