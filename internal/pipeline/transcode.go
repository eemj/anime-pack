package pipeline

import (
	"context"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg"
)

func (p *pipeline) Transcode(ctx context.Context) (err error) {
	transcodeProgress, errc := ffmpeg.Transcode(
		ctx,
		p.options.GetVideoSource(),
		p.options.GetVideoDestination(),
		p.options.GetTranscodeOptions(),
	)

	for {
		select {
		case progress := <-transcodeProgress:
			p.progress(&grpc_runner_v1.ProgressRequest{
				Type:            grpc_runner_v1.ProgressRequest_TRANSCODE,
				Percentage:      progress.Percentage,
				Speed:           progress.Speed,
				CurrentDuration: &progress.CurrentDuration,
				Duration:        &progress.Duration,
			})
		case err := <-errc:
			return err
		}
	}
}
