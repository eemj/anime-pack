package pipeline

import (
	"context"
	"os"
	"sync"
	"sync/atomic"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

type Pipeline interface {
	// Setup will set the options if the pipeline isn't running.
	Setup(options *Options) (err error)

	// Run combines the other flow methods
	// in the following order: Download -> Transcode -> Metadata -> Upload
	Run(ctx context.Context) (err error)
	Close() (err error)
	Active() bool

	// Methods
	Download(ctx context.Context) (err error)
	Transcode(ctx context.Context) (err error)
	Metadata(ctx context.Context) (err error)
	Upload(ctx context.Context) (err error)
	Retry(ctx context.Context, response *grpc_runner_v1.StreamResponse) (err error)

	// Events
	Stream() <-chan *grpc_runner_v1.StreamRequest
	Operation() <-chan *grpc_runner_v1.OperationRequest
	Done() <-chan struct{}
}

type pipeline struct {
	rwmutex    sync.RWMutex // rwmutex targets cancelFunc property
	options    *Options
	active     int32
	operation  chan *grpc_runner_v1.OperationRequest
	stream     chan *grpc_runner_v1.StreamRequest
	done       chan struct{}
	cancelFunc context.CancelFunc
	wg         sync.WaitGroup
}

func (p *pipeline) Setup(options *Options) (err error) {
	if p.Active() {
		return ErrCannotSetupWhilstPipelineIsActive
	}

	if p.options.IsEmpty() {
		p.options.Replace(options)
	}

	return
}

// Active returns true if the pipeline is currently running.
func (p *pipeline) Active() bool {
	return atomic.LoadInt32(&p.active) == 1
}

// Run will run all the stages on the provided options.
func (p *pipeline) Run(ctx context.Context) (err error) {
	if p.Active() {
		return ErrPipelineIsAlreadyActive
	}

	ctx = p.wrapContext(ctx)
	defer p.Close()

	atomic.SwapInt32(&p.active, 1)

	for _, stage := range []func(context.Context) error{
		p.Download,
		p.Transcode,
		p.Metadata,
		p.Upload,
	} {
		select {
		// There's a possibility that the user Canceled the context,
		// therefore it's important to cater for that scenario by not
		// running the next step.
		case <-ctx.Done():
			// Return whatever exception the ctx contains.
			return ctx.Err()
		default:
		}

		if err = p.await(ctx, stage); err != nil {
			return
		}
	}

	return
}

// await wraps the stage (Download, Transcode, Metadata, Upload) around wait groups.
// When await is called, it'll internally increment the WaitGroup by 1 and when the stage
// finishes the execution, it'll get marked as Done().
func (p *pipeline) await(
	ctx context.Context, // context that will get passed in the stage.
	fn func(context.Context) error,
) (err error) {
	p.wg.Add(1)
	defer p.wg.Done()
	err = fn(ctx)
	return
}

func (p *pipeline) paths() []string {
	return []string{
		p.options.GetVideoSource(),
		p.options.GetVideoDestination(),
		p.options.GetImageDestination(),
	}
}

// Close should be defer'd always, to ensure that the files have been deleted.
func (p *pipeline) Close() (err error) {
	// Keep a copy of the paths since we're resetting the options.
	paths := p.paths()

	p.rwmutex.Lock()
	if p.cancelFunc != nil {
		p.cancelFunc() // terminate the awaited tasks
		p.cancelFunc = nil
	}
	p.rwmutex.Unlock()

	p.options.Replace(&Options{})

	p.wg.Wait() // Wait for the pipeline stage to finish

	atomic.SwapInt32(&p.active, 0)

	// We'll remove the files.
	for _, path := range paths {
		// If the exception is not that the file does not exist,
		// return the exception. Always take into consideration that
		// the user could have deleted the files before calling this Close() method.
		if err := os.Remove(path); err != nil && !os.IsNotExist(err) {
			return err
		}
	}

	// Don't have the done send block,
	// if there are no listener so be it.
	select {
	case p.done <- struct{}{}:
	default:
	}

	return
}

// wrapContext will wrap the passed context so that it's cancellable internally, so
// when the Close() function get's called, we'll call the `context.CancelFunc` and
// terminate all the context listeners.
//
// If the pipeline is currently active, it'll return the source context.
func (p *pipeline) wrapContext(ctx context.Context) (wrapped context.Context) {
	if p.Active() {
		return ctx
	}

	p.rwmutex.Lock()
	defer p.rwmutex.Unlock()
	wrapped, p.cancelFunc = context.WithCancel(ctx)
	return
}

// Done sends a value when a pipeline closes.
func (p *pipeline) Done() <-chan struct{} { return p.done }

func New() Pipeline {
	return &pipeline{
		options:   new(Options),
		operation: make(chan *grpc_runner_v1.OperationRequest, 1),
		stream:    make(chan *grpc_runner_v1.StreamRequest, 1),
		done:      make(chan struct{}, 1),
	}
}

func NewWithOptions(options *Options) Pipeline {
	pipeline := New()
	pipeline.Setup(options)
	return pipeline
}
