package pipeline

import (
	"context"
	"hash/adler32"
	"io"
	"os"
	"time"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

const (
	// split refers to how many chunks are going to get split and sent
	// to the grpc server. currently it's set to 256KB.
	// split = 262144
	split = 262144

	// divider refers to the percentage divided for the video and the
	// image stream.
	divider = 25
)

// cloneBytes will clone the source byte array into a fixed destination byte array.
func cloneBytes(source []byte) []byte {
	dest := make([]byte, len(source))
	copy(dest, source)
	return dest
}

func (p *pipeline) Retry(ctx context.Context, response *grpc_runner_v1.StreamResponse) (err error) {
	var fs *os.File
	if response.Stream == grpc_runner_v1.StreamType_IMAGE {
		fs, err = os.Open(p.options.GetImageDestination())
	} else {
		fs, err = os.Open(p.options.GetVideoDestination())
	}
	if err != nil {
		return err
	}
	defer fs.Close()

	_, err = fs.Seek(response.Start, 0)
	if err != nil {
		return err
	}

	chunks := make([]byte, int(response.End-response.Start))
	n, err := fs.Read(chunks)
	if err != nil {
		return err
	}

	bytes := cloneBytes(chunks[:n])

	p.stream <- &grpc_runner_v1.StreamRequest{
		Stream:  response.Stream,
		Start:   response.Start,
		End:     response.End,
		Size:    int64(n),
		Content: bytes,
		Adler32: adler32.Checksum(bytes),
	}

	return nil
}

func streamProgress(stream grpc_runner_v1.StreamType, percentage, written, speed, size float64) *grpc_runner_v1.ProgressRequest {
	request := &grpc_runner_v1.ProgressRequest{
		Type:            grpc_runner_v1.ProgressRequest_UPLOAD,
		Speed:           written - speed,
		CurrentFilesize: &written,
		Filesize:        &size,
	}

	switch stream {
	case grpc_runner_v1.StreamType_IMAGE:
		request.Percentage = (percentage / 100) * divider
	case grpc_runner_v1.StreamType_VIDEO:
		request.Percentage = divider + ((percentage / 100) * (100 - divider))
	default:
		request.Percentage = percentage
	}

	return request
}

func (p *pipeline) process(
	ctx context.Context,
	reader io.Reader,
	size float64,
	stream grpc_runner_v1.StreamType,
	tickDuration time.Duration,
) (err error) {
	var (
		written, speed, percentage float64

		chunks = make([]byte, split)
		ticker = time.NewTicker(tickDuration)
	)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
			percentage = (written / size) * 100
			p.progress(streamProgress(stream, percentage, written, speed, size))
			speed = written
		default:
		}

		n, err := reader.Read(chunks)
		if err != nil {
			if err == io.EOF {
				p.progress(streamProgress(stream, 100, written, speed, size))
				return nil
			}

			return err
		}

		bytes := cloneBytes(chunks[:n])

		// Send out the chunk.
		p.stream <- &grpc_runner_v1.StreamRequest{
			Stream: stream,
			Start:  int64(written),
			End:    int64(int(written) + n),
			Size:   int64(n),

			// The byte slice needs to get cloned here, as it's sent concurrently and by the time
			// it's sent to the channel, the loop would've already read the next chunk.
			Content: bytes,

			Adler32: adler32.Checksum(bytes),
		}

		// Increment written size
		written += float64(n)
	}
}

func (p *pipeline) streamer(ctx context.Context, stream grpc_runner_v1.StreamType) (err error) {
	var fs *os.File
	if stream == grpc_runner_v1.StreamType_IMAGE {
		fs, err = os.Open(p.options.GetImageDestination())
	} else {
		fs, err = os.Open(p.options.GetVideoDestination())
	}
	if err != nil {
		return err
	}
	defer fs.Close()

	stat, err := fs.Stat()
	if err != nil {
		return err
	}

	return p.process(ctx, fs, float64(stat.Size()), stream, time.Second)
}

func (p *pipeline) Stream() <-chan *grpc_runner_v1.StreamRequest {
	return p.stream
}
