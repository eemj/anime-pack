package pipeline

import (
	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

func (p *pipeline) Operation() <-chan *grpc_runner_v1.OperationRequest { return p.operation }
