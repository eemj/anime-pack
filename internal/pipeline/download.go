package pipeline

import (
	"context"
	"os"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/pkg/dcc"
)

func (p *pipeline) Download(ctx context.Context) (err error) {
	fs, err := os.OpenFile(
		p.options.GetVideoSource(),
		os.O_CREATE|os.O_RDWR,
		os.ModePerm,
	)
	if err != nil {
		return
	}

	defer fs.Close()

	d := dcc.NewDCC(
		p.options.GetAddress(),
		p.options.GetSize(),
		fs,
	)

	dccProgress, errc := d.Run(ctx)

	for {
		select {
		case progress := <-dccProgress:
			p.progress(&grpc_runner_v1.ProgressRequest{
				Type:            grpc_runner_v1.ProgressRequest_DOWNLOAD,
				Percentage:      progress.Percentage,
				Speed:           progress.Speed,
				CurrentFilesize: &progress.CurrentFileSize,
				Filesize:        &progress.FileSize,
			})
		case err := <-errc:
			return err
		}
	}
}
