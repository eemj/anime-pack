package pipeline

import (
	"context"
	"errors"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
)

func (p *pipeline) Upload(ctx context.Context) error {
	return errors.Join(
		p.streamer(ctx, grpc_runner_v1.StreamType_IMAGE),
		p.streamer(ctx, grpc_runner_v1.StreamType_VIDEO),
	)
}
