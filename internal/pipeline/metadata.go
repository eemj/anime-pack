package pipeline

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"

	grpc_runner_v1 "gitlab.com/eemj/anime-pack/api/grpc_runner/v1"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/datatype"
	"gitlab.com/eemj/anime-pack/pkg/ffmpeg/operation"
	"gitlab.com/eemj/anime-pack/pkg/utils"
)

func calculateMetadata(path string) (string, int64, error) {
	fs, err := os.Open(path)
	if err != nil {
		return "", 0, err
	}
	defer fs.Close()

	stat, err := fs.Stat()
	if err != nil {
		return "", 0, err
	}

	var (
		size = stat.Size()
		sha  = sha256.New()
	)

	_, err = io.Copy(sha, fs)
	if err != nil {
		return "", 0, err
	}

	hash := hex.EncodeToString(sha.Sum(nil))

	return hash, size, nil
}

func metadataProgress(percentage float64) *grpc_runner_v1.ProgressRequest {
	return &grpc_runner_v1.ProgressRequest{
		Type:       grpc_runner_v1.ProgressRequest_METADATA,
		Percentage: percentage,
	}
}

func (p *pipeline) createThumbnail(ctx context.Context, duration float64) (err error) {
	if err = operation.GenerateThumbnail(
		ctx,
		p.options.GetVideoSource(),
		p.options.GetImageDestination(),
		duration,
	); err != nil {
		return
	}

	// The duration might not be in-line with the video stream,
	// which could have generated an empty/non-existant image.
	stat, err := os.Stat(p.options.GetImageDestination())

	if (err != nil && os.IsNotExist(err)) || (err == nil && stat.Size() == 0) {
		streams, err := operation.GetStreams(ctx, p.options.GetVideoSource())
		if err != nil {
			return err
		}

		videoDuration := float64(0)

		// We'll get the maximum duration for the video stream
		for _, stream := range streams {
			if stream.CodecType == datatype.Codec_Video {
				_duration := stream.Tags.Duration.Unwrap().Seconds()
				_durationEng := stream.Tags.DurationEng.Unwrap().Seconds()

				if _duration != 0 && videoDuration < _duration {
					videoDuration = _duration
				}

				if _durationEng != 0 && videoDuration < _durationEng {
					videoDuration = _durationEng
				}
			}
		}

		return operation.GenerateThumbnail(
			ctx,
			p.options.GetVideoSource(),
			p.options.GetImageDestination(),
			videoDuration,
		)
	}

	return
}

func (p *pipeline) Metadata(ctx context.Context) (err error) {
	p.progress(metadataProgress(5))

	duration, err := operation.GetDuration(ctx, p.options.GetVideoSource())
	if err != nil {
		return err
	}

	p.progress(metadataProgress(10))

	if err = p.createThumbnail(ctx, duration); err != nil {
		return err
	}

	p.progress(metadataProgress(25))

	// Calculate sizes
	videoSha, videoSize, err := calculateMetadata(p.options.GetVideoDestination())
	if err != nil {
		return err
	}

	p.progress(metadataProgress(45))

	// Calculate hashes
	imageSha, imageSize, err := calculateMetadata(p.options.GetImageDestination())
	if err != nil {
		return err
	}

	p.progress(metadataProgress(60))

	// Calculate video dimensions
	videoWidth, videoHeight, err := operation.GetVideoDimensions(ctx, p.options.GetVideoDestination())
	if err != nil {
		return err
	}

	p.progress(metadataProgress(80))

	fs, err := os.Open(p.options.GetVideoDestination())
	if err != nil {
		return err
	}
	defer fs.Close()

	p.progress(metadataProgress(90))

	videoCrc, err := utils.ChecksumCRC32(fs)
	if err != nil {
		return err
	}

	p.progress(metadataProgress(100))

	p.metadata(&grpc_runner_v1.MetadataRequest{
		Duration:    duration,
		VideoSha256: videoSha,
		ImageSha256: imageSha,
		VideoCrc32:  videoCrc,
		VideoSize:   videoSize,
		ImageSize:   imageSize,
		Video: &grpc_runner_v1.MetadataVideo{
			Sha256:   videoSha,
			Crc32:    videoCrc,
			Size:     videoSize,
			Duration: duration,
			Dimension: &grpc_runner_v1.MetadataVideoDimension{
				Height: videoHeight,
				Width:  videoWidth,
			},
		},
		Image: &grpc_runner_v1.MetadataImage{
			Sha256: imageSha,
			Size:   imageSize,
		},
	})

	return nil
}
