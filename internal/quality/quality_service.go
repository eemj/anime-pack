package quality

import (
	"context"

	"github.com/jackc/pgx/v5"
	"gitlab.com/eemj/anime-pack/internal/cachekeys"
	"gitlab.com/eemj/anime-pack/internal/database"
	"gitlab.com/eemj/anime-pack/internal/domain"
	"gitlab.com/eemj/anime-pack/internal/domain/params"
	pgxbulk "gitlab.com/eemj/anime-pack/internal/pgx_bulk"
	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/encoded"
)

type QualityService interface {
	Create(ctx context.Context, arg params.InsertQualityParamsItem) (*domain.Quality, error)
	CreateMany(ctx context.Context, arg params.InsertQualityParams) ([]*domain.Quality, error)
	First(ctx context.Context, arg params.FirstQualityParams) (*domain.Quality, error)
	Filter(ctx context.Context, arg params.FilterQualityParams) ([]*domain.Quality, error)
	Count(ctx context.Context, arg params.CountQualityParams) (int64, error)
	Update(ctx context.Context, arg params.UpdateQualityParams) (int64, error)
}

type qualityService struct {
	querier database.QuerierExtended
	cacher  *encoded.EncodedCacher
}

// Count implements Service.
func (s *qualityService) Count(ctx context.Context, arg params.CountQualityParams) (int64, error) {
	count, err := s.querier.CountQuality(ctx, database.CountQualityParams{
		Height: arg.Height,
		ID:     arg.ID,
	})
	if err != nil && err == pgx.ErrNoRows {
		return 0, nil
	}
	return count, err
}

// Create implements Service.
func (s *qualityService) Create(ctx context.Context, arg params.InsertQualityParamsItem) (*domain.Quality, error) {
	cacheKey := cachekeys.Quality(nil, &arg.Height)
	if cacheKey != "" {
		cacheValue, err := encoded.Get[*domain.Quality](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cacheValue != nil {
			return cacheValue, nil
		}
	}
	row, err := s.querier.InsertQuality(ctx, database.InsertQualityParams(arg))
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := &domain.Quality{
		ID:     row.ID,
		Height: row.Height,
	}
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// CreateMany implements Service.
func (s *qualityService) CreateMany(ctx context.Context, arg params.InsertQualityParams) ([]*domain.Quality, error) {
	args := make([]database.InsertManyQualityParams, len(arg.Items))
	for index, item := range arg.Items {
		args[index] = database.InsertManyQualityParams{
			Height: item.Height,
		}
	}
	bulk := s.querier.InsertManyQuality(ctx, args)
	defer bulk.Close()
	return pgxbulk.QueryResultMap[
		*database.InsertManyQualityRow,
		*domain.Quality,
	](bulk, func(row *database.InsertManyQualityRow) *domain.Quality {
		return &domain.Quality{
			ID:     row.ID,
			Height: row.Height,
		}
	})
}

func mapEntitesToDomain(entities ...*database.Quality) []*domain.Quality {
	result := make([]*domain.Quality, len(entities))
	for index, entity := range entities {
		result[index] = &domain.Quality{
			ID:     entity.ID,
			Height: entity.Height,
		}
	}
	return result
}

// Filter implements Service.
func (s *qualityService) Filter(ctx context.Context, arg params.FilterQualityParams) ([]*domain.Quality, error) {
	entities, err := s.querier.FilterQuality(ctx, database.FilterQualityParams{
		Height: arg.Height,
		ID:     arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return []*domain.Quality{}, nil
		}
		return nil, err
	}
	return mapEntitesToDomain(entities...), nil
}

// First implements Service.
func (s *qualityService) First(ctx context.Context, arg params.FirstQualityParams) (*domain.Quality, error) {
	cacheKey := cachekeys.Quality(arg.ID, arg.Height)
	if cacheKey != "" {
		cacheValue, err := encoded.Get[*domain.Quality](ctx, s.cacher, cacheKey)
		if err != nil {
			return nil, err
		}
		if cacheValue != nil {
			return cacheValue, nil
		}
	}

	entity, err := s.querier.FirstQuality(ctx, database.FirstQualityParams{
		Height: arg.Height,
		ID:     arg.ID,
	})
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	result := mapEntitesToDomain(entity)[0]
	if cacheKey != "" {
		err = encoded.Set(ctx, s.cacher, cacheKey, result, cacher.DefaultTTL)
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

// Update implements Service.
func (s *qualityService) Update(ctx context.Context, arg params.UpdateQualityParams) (int64, error) {
	return s.querier.UpdateQuality(ctx, database.UpdateQualityParams{
		ID:     arg.ID,
		Height: arg.Height,
	})
}

func NewQualityService(
	querier database.QuerierExtended,
	cacher cacher.Cacher,
) QualityService {
	encodedCacher := encoded.NewEncodedCacher(cacher, &codec.JSON{})

	return &qualityService{
		querier: querier,
		cacher:  encodedCacher,
	}
}
